<?php /* Smarty version 2.6.30, created on 2017-01-30 13:46:08
         compiled from shop_list.tpl */ ?>
<?php require_once(SMARTY_CORE_DIR . 'core.load_plugins.php');
smarty_core_load_plugins(array('plugins' => array(array('modifier', 'oxmultilangassign', 'shop_list.tpl', 1, false),array('modifier', 'oxaddslashes', 'shop_list.tpl', 140, false),array('function', 'oxmultilang', 'shop_list.tpl', 44, false),)), $this); ?>
<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => "headitem.tpl", 'smarty_include_vars' => array('title' => ((is_array($_tmp='GENERAL_ADMIN_TITLE')) ? $this->_run_mod_handler('oxmultilangassign', true, $_tmp) : smarty_modifier_oxmultilangassign($_tmp)),'box' => 'list')));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>
<?php $this->assign('where', $this->_tpl_vars['oView']->getListFilter()); ?>

<?php $this->assign('readonly', ""); ?>
<?php if ($this->_tpl_vars['readonly']): ?>
    <?php $this->assign('readonly', 'readonly disabled'); ?>
<?php endif; ?>

<script type="text/javascript">
<!--
function editThis(sID)
{
    var oForm = top.navigation.adminnav.document.getElementById( "search" );
    if (oForm) {
        // passing this info about active view and tab to nav frame
        var oInputElement = document.createElement( 'input' );
        oInputElement.setAttribute( 'name', 'listview');
        oInputElement.setAttribute( 'type', 'hidden' );
        oInputElement.value = "<?php echo $this->_tpl_vars['oViewConf']->getActiveClassName(); ?>
";
        oForm.appendChild( oInputElement );

        var oInputElement = document.createElement( 'input' );
        oInputElement.setAttribute( 'name', 'actedit');
        oInputElement.setAttribute( 'type', 'hidden' );
        oInputElement.value = "<?php echo $this->_tpl_vars['actedit']; ?>
";
        oForm.appendChild( oInputElement );

        var oInputElement = document.createElement( 'input' );
        oInputElement.setAttribute( 'name', 'editview');
        oInputElement.setAttribute( 'type', 'hidden' );
        oInputElement.value = top.oxid.admin.getClass( sID );
        oForm.appendChild( oInputElement );

        // selecting shop
        top.navigation.adminnav.selectShop( sID );
    }
}

function deleteThis(sID)
{
    var currentshop = <?php echo $this->_tpl_vars['oxid']; ?>
;
    var newshop = (sID == currentshop)?1:currentshop;

    blCheck = confirm("<?php echo smarty_function_oxmultilang(array('ident' => 'SHOP_LIST_YOUWANTTODELETE'), $this);?>
");
    if( blCheck == true)
    {   var oSearch = top.basefrm.list.document.getElementById( "search" );
        oSearch.delshopid.value = sID;
        oSearch.fnc.value = 'deleteentry';
        oSearch.actedit.value = 0;
        oSearch.submit();

        var oTransfer = top.basefrm.edit.document.getElementById( "transfer" );
        oTransfer.oxid.value = newshop;
        oTransfer.actshop.value = newshop;
        oTransfer.cl.value='<?php echo $this->_tpl_vars['default_edit']; ?>
';
        oTransfer.updatenav.value = 1;

        //forcing edit frame to reload after submit
        top.forceReloadingEditFrame();
    }
}

window.onload = function ()
{
    <?php if ($this->_tpl_vars['updatenav']): ?>
    var oTransfer = top.basefrm.edit.document.getElementById( "transfer" );
    oTransfer.updatenav.value = 1;
    oTransfer.cl.value = '<?php echo $this->_tpl_vars['default_edit']; ?>
';
    <?php endif; ?>
    top.reloadEditFrame();
}
//-->
</script>

<form name="search" id="search" action="<?php echo $this->_tpl_vars['oViewConf']->getSelfLink(); ?>
" method="post">
<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => "_formparams.tpl", 'smarty_include_vars' => array('cl' => 'shop_list','lstrt' => $this->_tpl_vars['lstrt'],'actedit' => $this->_tpl_vars['actedit'],'oxid' => $this->_tpl_vars['oxid'],'fnc' => "",'language' => $this->_tpl_vars['actlang'],'editlanguage' => $this->_tpl_vars['actlang'],'delshopid' => "",'updatenav' => "")));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>
<div id="liste">
    <table cellspacing="0" cellpadding="0" border="0" width="100%">
    <colgroup>
        
            <col width="98%"><col width="2%">
        
    </colgroup>
    <tr class="listitem">
        
            <td valign="top" class="listfilter first" colspan="2">
                <div class="r1"><div class="b1">
                <div class="find">
                    <input class="listedit" type="submit" name="submitit" value="<?php echo smarty_function_oxmultilang(array('ident' => 'GENERAL_SEARCH'), $this);?>
">
                </div>
                <input class="listedit" type="text" size="60" maxlength="128" name="where[oxshops][oxname]" value="<?php echo $this->_tpl_vars['where']['oxshops']['oxname']; ?>
">
                </div></div>
            </td>
        
    </tr>
    <tr>
        
            <td class="listheader first" height="15" colspan="2">&nbsp;<a href="Javascript:top.oxid.admin.setSorting( document.search, 'oxshops', 'oxname', 'asc');document.search.submit();" class="listheader"><?php echo smarty_function_oxmultilang(array('ident' => 'GENERAL_DESCRIPTION'), $this);?>
</a></td>
        
    </tr>

    <?php $this->assign('blWhite', ""); ?>
    <?php $this->assign('_cnt', 0); ?>
    <?php $_from = $this->_tpl_vars['mylist']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['listitem']):
?>
    <?php $this->assign('_cnt', $this->_tpl_vars['_cnt']+1); ?>
    <tr id="row.<?php echo $this->_tpl_vars['_cnt']; ?>
">
        
            <?php if ($this->_tpl_vars['listitem']->blacklist == 1): ?>
                <?php $this->assign('listclass', 'listitem3'); ?>
            <?php else: ?>
                <?php $this->assign('listclass', "listitem".($this->_tpl_vars['blWhite'])); ?>
            <?php endif; ?>
            <?php if ($this->_tpl_vars['listitem']->getId() == $this->_tpl_vars['oxid']): ?>
                <?php $this->assign('listclass', 'listitem4'); ?>
            <?php endif; ?>
            <td valign="top" class="<?php echo $this->_tpl_vars['listclass']; ?>
" height="15"><div class="listitemfloating">&nbsp;<a href="Javascript:editThis('<?php echo $this->_tpl_vars['listitem']->oxshops__oxid->value; ?>
');" class="<?php echo $this->_tpl_vars['listclass']; ?>
"><?php if ($this->_tpl_vars['listitem']->oxshops__oxid->value == '1'): ?><b><?php endif; ?><?php echo $this->_tpl_vars['listitem']->oxshops__oxname->value; ?>
<?php if ($this->_tpl_vars['listitem']->oxshops__oxid->value == '1'): ?></b><?php endif; ?>&nbsp;(<?php echo $this->_tpl_vars['listitem']->oxshops__oxid->value; ?>
)</a></div></td>
            <td class="<?php echo $this->_tpl_vars['listclass']; ?>
">
            <?php if (! $this->_tpl_vars['listitem']->isOx() && ! $this->_tpl_vars['listitem']->isparent && ! $this->_tpl_vars['readonly'] && $this->_tpl_vars['listitem']->oxshops__oxid->value != 1): ?>
            <a href="Javascript:deleteThis('<?php echo $this->_tpl_vars['listitem']->oxshops__oxid->value; ?>
');" class="delete" id="del.<?php echo $this->_tpl_vars['_cnt']; ?>
" <?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => "help.tpl", 'smarty_include_vars' => array('helpid' => 'item_delete')));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>></a>
            <?php endif; ?>
            </td>
        
    </tr>
    <?php if ($this->_tpl_vars['blWhite'] == '2'): ?>
        <?php $this->assign('blWhite', ""); ?>
    <?php else: ?>
        <?php $this->assign('blWhite', '2'); ?>
    <?php endif; ?>
    <?php endforeach; endif; unset($_from); ?>
    <?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => "pagenavisnippet.tpl", 'smarty_include_vars' => array('colspan' => '2')));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>
    </table>
    </form>
</div>

<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => "pagetabsnippet.tpl", 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>
</form>

<script type="text/javascript">
if (parent.parent != null && parent.parent.setTitle )
{   parent.parent.sShopTitle   = "<?php echo ((is_array($_tmp=$this->_tpl_vars['actshopobj']->oxshops__oxname->getRawValue())) ? $this->_run_mod_handler('oxaddslashes', true, $_tmp) : smarty_modifier_oxaddslashes($_tmp)); ?>
";
    parent.parent.sMenuItem    = "<?php echo smarty_function_oxmultilang(array('ident' => 'SHOP_LIST_MENUITEM'), $this);?>
";
    parent.parent.sMenuSubItem = "<?php echo smarty_function_oxmultilang(array('ident' => 'SHOP_LIST_MENUSUBITEM'), $this);?>
";
    parent.parent.sWorkArea    = "<?php echo $this->_tpl_vars['_act']; ?>
";
    parent.parent.setTitle();
}
</script>
</body>
</html>