<?php /* Smarty version 2.6.30, created on 2017-01-30 13:46:31
         compiled from include/article_list_actions.tpl */ ?>
<?php require_once(SMARTY_CORE_DIR . 'core.load_plugins.php');
smarty_core_load_plugins(array('plugins' => array(array('block', 'oxhasrights', 'include/article_list_actions.tpl', 6, false),)), $this); ?>
<?php if (! $this->_tpl_vars['readonly']): ?>
    <?php if ($this->_tpl_vars['listitem']->blIsDerived && ! $this->_tpl_vars['oViewConf']->isMultiShop()): ?>
        <a href="Javascript:top.oxid.admin.unassignThis('<?php echo $this->_tpl_vars['listitem']->oxarticles__oxid->value; ?>
');" class="unasign" id="una.<?php echo $this->_tpl_vars['_cnt']; ?>
" title="" <?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => "help.tpl", 'smarty_include_vars' => array('helpid' => 'item_unassign')));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>></a>
    <?php endif; ?>
    <?php if (! $this->_tpl_vars['listitem']->blIsDerived): ?>
        <?php $this->_tag_stack[] = array('oxhasrights', array('object' => $this->_tpl_vars['listitem'],'right' => @RIGHT_DELETE)); $_block_repeat=true;smarty_block_oxhasrights($this->_tag_stack[count($this->_tag_stack)-1][1], null, $this, $_block_repeat);while ($_block_repeat) { ob_start(); ?>
            <a href="Javascript:top.oxid.admin.deleteThis('<?php echo $this->_tpl_vars['listitem']->oxarticles__oxid->value; ?>
');" class="delete" id="del.<?php echo $this->_tpl_vars['_cnt']; ?>
" title="" <?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => "help.tpl", 'smarty_include_vars' => array('helpid' => 'item_delete')));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>></a>
        <?php $_block_content = ob_get_contents(); ob_end_clean(); $_block_repeat=false;echo smarty_block_oxhasrights($this->_tag_stack[count($this->_tag_stack)-1][1], $_block_content, $this, $_block_repeat); }  array_pop($this->_tag_stack); ?>
    <?php endif; ?>
<?php endif; ?>