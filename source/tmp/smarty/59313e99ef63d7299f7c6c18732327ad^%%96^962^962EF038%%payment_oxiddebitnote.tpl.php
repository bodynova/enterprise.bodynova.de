<?php /* Smarty version 2.6.30, created on 2017-01-30 14:07:41
         compiled from page/checkout/inc/payment_oxiddebitnote.tpl */ ?>
<?php require_once(SMARTY_CORE_DIR . 'core.load_plugins.php');
smarty_core_load_plugins(array('plugins' => array(array('function', 'oxmultilang', 'page/checkout/inc/payment_oxiddebitnote.tpl', 9, false),)), $this); ?>
<?php $this->assign('dynvalue', $this->_tpl_vars['oView']->getDynValue()); ?>
<dl>
    <dt>
        <input id="payment_<?php echo $this->_tpl_vars['sPaymentID']; ?>
" type="radio" name="paymentid" value="<?php echo $this->_tpl_vars['sPaymentID']; ?>
" <?php if ($this->_tpl_vars['oView']->getCheckedPaymentId() == $this->_tpl_vars['paymentmethod']->oxpayments__oxid->value): ?>checked<?php endif; ?>>
        <label for="payment_<?php echo $this->_tpl_vars['sPaymentID']; ?>
"><b><?php echo $this->_tpl_vars['paymentmethod']->oxpayments__oxdesc->value; ?>
</b></label>
    </dt>
    <dd class="<?php if ($this->_tpl_vars['oView']->getCheckedPaymentId() == $this->_tpl_vars['paymentmethod']->oxpayments__oxid->value): ?>activePayment<?php endif; ?>">
        <div class="form-group">
            <label class="req control-label col-lg-3"><?php echo smarty_function_oxmultilang(array('ident' => 'BANK'), $this);?>
</label>
            <div class="col-lg-9">
                <input id="payment_<?php echo $this->_tpl_vars['sPaymentID']; ?>
_1" class="form-control js-oxValidate js-oxValidate_notEmpty" type="text" size="20" maxlength="64" name="dynvalue[lsbankname]" value="<?php echo $this->_tpl_vars['dynvalue']['lsbankname']; ?>
" required="required">
            </div>
        </div>
        <div class="form-group">
            <label class="req control-label col-lg-3"><?php echo smarty_function_oxmultilang(array('ident' => 'BANK_CODE'), $this);?>
</label>
            <div class="col-lg-9">
                <input type="text" class="form-control js-oxValidate js-oxValidate_notEmpty" size="20" maxlength="64" name="dynvalue[lsblz]" value="<?php echo $this->_tpl_vars['dynvalue']['lsblz']; ?>
" required="required">
            </div>
        </div>
        <div class="form-group">
            <label class="req control-label col-lg-3"><?php echo smarty_function_oxmultilang(array('ident' => 'BANK_ACCOUNT_NUMBER'), $this);?>
</label>
            <div class="col-lg-9">
                <input type="text" class="form-control js-oxValidate js-oxValidate_notEmpty" size="20" maxlength="64" name="dynvalue[lsktonr]" value="<?php echo $this->_tpl_vars['dynvalue']['lsktonr']; ?>
" required="required">
            </div>
        </div>
        <div class="form-group">
            <label class="req control-label col-lg-3"><?php echo smarty_function_oxmultilang(array('ident' => 'BANK_ACCOUNT_HOLDER'), $this);?>
</label>
            <div class="col-lg-9">
                <input type="text" class="form-control js-oxValidate js-oxValidate_notEmpty" size="20" maxlength="64" name="dynvalue[lsktoinhaber]" value="<?php if ($this->_tpl_vars['dynvalue']['lsktoinhaber']): ?><?php echo $this->_tpl_vars['dynvalue']['lsktoinhaber']; ?>
<?php else: ?><?php echo $this->_tpl_vars['oxcmp_user']->oxuser__oxfname->value; ?>
 <?php echo $this->_tpl_vars['oxcmp_user']->oxuser__oxlname->value; ?>
<?php endif; ?>" required="required">
            </div>
        </div>

        
            <?php if ($this->_tpl_vars['paymentmethod']->oxpayments__oxlongdesc->value): ?>
                <div class="alert alert-info col-lg-offset-3 desc">
                    <?php echo $this->_tpl_vars['paymentmethod']->oxpayments__oxlongdesc->getRawValue(); ?>

                </div>
            <?php endif; ?>
        
    </dd>
</dl>