<?php /* Smarty version 2.6.30, created on 2017-01-30 13:45:28
         compiled from include/login_messages.tpl */ ?>
<?php require_once(SMARTY_CORE_DIR . 'core.load_plugins.php');
smarty_core_load_plugins(array('plugins' => array(array('function', 'oxmultilang', 'include/login_messages.tpl', 2, false),)), $this); ?>
<?php if (( $this->_tpl_vars['oViewConf']->isStagingMode() )): ?>
    <div class="notify"><?php echo smarty_function_oxmultilang(array('ident' => 'LOGIN_STAGINGMODE_NOTIFY'), $this);?>
</div>
<?php endif; ?>
<?php if (( $this->_tpl_vars['oViewConf']->hasDemoKey() )): ?>
    <div class="notify"><?php echo smarty_function_oxmultilang(array('ident' => 'LOGIN_DEMOMODE_NOTIFY'), $this);?>
</div>
<?php endif; ?>
<?php $this->assign('sShopValidationMessage', $this->_tpl_vars['oView']->getShopValidationMessage()); ?>
<?php if (( $this->_tpl_vars['sShopValidationMessage'] )): ?>
    <?php if (( $this->_tpl_vars['oView']->isGracePeriodExpired() )): ?>
        <div class="notify"><?php echo smarty_function_oxmultilang(array('ident' => 'SHOP_LICENSE_ERROR_GRACE_EXPIRED'), $this);?>
</div>
    <?php else: ?>
        <div class="notify"><?php echo smarty_function_oxmultilang(array('ident' => "SHOP_LICENSE_ERROR_".($this->_tpl_vars['sShopValidationMessage'])), $this);?>
</div>
    <?php endif; ?>
<?php endif; ?>