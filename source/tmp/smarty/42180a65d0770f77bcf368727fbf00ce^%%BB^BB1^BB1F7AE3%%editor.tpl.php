<?php /* Smarty version 2.6.30, created on 2017-01-30 13:46:33
         compiled from include/editor.tpl */ ?>
<?php require_once(SMARTY_CORE_DIR . 'core.load_plugins.php');
smarty_core_load_plugins(array('plugins' => array(array('block', 'oxhasrights', 'include/editor.tpl', 2, false),array('function', 'oxmultilang', 'include/editor.tpl', 4, false),)), $this); ?>
<?php if ($this->_tpl_vars['checkrights']): ?>
    <?php $this->_tag_stack[] = array('oxhasrights', array('object' => $this->_tpl_vars['edit'],'field' => $this->_tpl_vars['checkrights'],'readonly' => $this->_tpl_vars['readonly'],'right' => $this->_tpl_vars['right'])); $_block_repeat=true;smarty_block_oxhasrights($this->_tag_stack[count($this->_tag_stack)-1][1], null, $this, $_block_repeat);while ($_block_repeat) { ob_start(); ?>
        <?php echo $this->_tpl_vars['editor']; ?>

        <div class="messagebox"><?php echo smarty_function_oxmultilang(array('ident' => 'EDITOR_PLAINTEXT_HINT'), $this);?>
</div>
    <?php $_block_content = ob_get_contents(); ob_end_clean(); $_block_repeat=false;echo smarty_block_oxhasrights($this->_tag_stack[count($this->_tag_stack)-1][1], $_block_content, $this, $_block_repeat); }  array_pop($this->_tag_stack); ?>
<?php else: ?>
    <?php echo $this->_tpl_vars['editor']; ?>

    <div class="messagebox"><?php echo smarty_function_oxmultilang(array('ident' => 'EDITOR_PLAINTEXT_HINT'), $this);?>
</div>
<?php endif; ?>