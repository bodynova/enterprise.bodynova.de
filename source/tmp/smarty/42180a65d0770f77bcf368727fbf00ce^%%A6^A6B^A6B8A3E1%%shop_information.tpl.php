<?php /* Smarty version 2.6.30, created on 2017-01-30 13:46:09
         compiled from include/shop_information.tpl */ ?>
<?php require_once(SMARTY_CORE_DIR . 'core.load_plugins.php');
smarty_core_load_plugins(array('plugins' => array(array('function', 'oxmultilang', 'include/shop_information.tpl', 7, false),)), $this); ?>
<input type="hidden" name="ismultistore" value=0><br>

<?php if ($this->_tpl_vars['edit']->oxshops__oxismultishop->value): ?>
    <tr>
        <td class="edittext" colspan=2 style="height:15px">
            <img src="<?php echo $this->_tpl_vars['oViewConf']->getImageUrl(); ?>
/checkmark.gif" hspace="0" vspace="0" border="0" align="middle" alt="" width="7" height="5">
            <?php echo smarty_function_oxmultilang(array('ident' => 'SHOP_MAIN_ISMULTISHOP'), $this);?>

        </td>
    </tr>
<?php endif; ?>

<?php if ($this->_tpl_vars['edit']->oxshops__oxissupershop->value): ?>
    <tr>
        <td class="edittext" colspan=2 style="height:15px">
            <img src="<?php echo $this->_tpl_vars['oViewConf']->getImageUrl(); ?>
/checkmark.gif" hspace="0" vspace="0" border="0" align="middle" alt="" width="7" height="5">
            <?php echo smarty_function_oxmultilang(array('ident' => 'SHOP_MAIN_ISSUPERSHOP'), $this);?>

        </td>
    </tr>
<?php endif; ?>

<tr>
    <td class="edittext" style="height:15px">
        <?php echo smarty_function_oxmultilang(array('ident' => 'SHOP_MAIN_SHOPPARENT'), $this);?>

    </td>
    <td class="edittext">
        <?php if ($this->_tpl_vars['edit']->oxshops__oxparentid->value): ?><?php echo $this->_tpl_vars['parentName']; ?>
(<?php echo $this->_tpl_vars['edit']->oxshops__oxparentid->value; ?>
)<?php else: ?>--<?php endif; ?>
    </td>
</tr>

<tr>
    <td class="edittext" style="height:15px">
        <?php echo smarty_function_oxmultilang(array('ident' => 'SHOP_MAIN_ID'), $this);?>

    </td>
    <td class="edittext">
        <?php echo $this->_tpl_vars['oxid']; ?>

    </td>
</tr>