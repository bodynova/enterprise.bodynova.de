<?php /* Smarty version 2.6.30, created on 2017-01-30 15:16:48
         compiled from shop_mall.tpl */ ?>
<?php require_once(SMARTY_CORE_DIR . 'core.load_plugins.php');
smarty_core_load_plugins(array('plugins' => array(array('modifier', 'oxmultilangassign', 'shop_mall.tpl', 1, false),array('function', 'oxinputhelp', 'shop_mall.tpl', 52, false),array('function', 'oxmultilang', 'shop_mall.tpl', 55, false),)), $this); ?>
<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => "headitem.tpl", 'smarty_include_vars' => array('title' => ((is_array($_tmp='GENERAL_ADMIN_TITLE')) ? $this->_run_mod_handler('oxmultilangassign', true, $_tmp) : smarty_modifier_oxmultilangassign($_tmp)))));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>

<script type="text/javascript">
<!--
function editThis( sID )
{
    var oTransfer = top.basefrm.edit.document.getElementById( "transfer" );
    oTransfer.oxid.value = '';
    oTransfer.cl.value = top.oxid.admin.getClass( sID );

    //forcing edit frame to reload after submit
    top.forceReloadingEditFrame();

    var oSearch = top.basefrm.list.document.getElementById( "search" );
    oSearch.oxid.value = sID;
    oSearch.updatenav.value = 1;
    oSearch.submit();
}
//-->
</script>

<?php $this->assign('readonly', ""); ?>
<?php if ($this->_tpl_vars['readonly']): ?>
    <?php $this->assign('readonly', ' readonly disabled '); ?>
<?php endif; ?>

<form name="transfer" id="transfer" action="<?php echo $this->_tpl_vars['oViewConf']->getSelfLink(); ?>
" method="post">
    <?php echo $this->_tpl_vars['oViewConf']->getHiddenSid(); ?>

    <input type="hidden" name="oxid" value="<?php echo $this->_tpl_vars['oxid']; ?>
">
    <input type="hidden" name="cl" value="shop_mall">
    <input type="hidden" name="fnc" value="">
    <input type="hidden" name="actshop" value="<?php echo $this->_tpl_vars['oViewConf']->getActiveShopId(); ?>
">
    <input type="hidden" name="updatenav" value="">
    <input type="hidden" name="editlanguage" value="<?php echo $this->_tpl_vars['editlanguage']; ?>
">
</form>

<form name="myedit1" id="myedit1" action="<?php echo $this->_tpl_vars['oViewConf']->getSelfLink(); ?>
" method="post">
<?php echo $this->_tpl_vars['oViewConf']->getHiddenSid(); ?>

<input type="hidden" name="cl" value="shop_mall">
<input type="hidden" name="fnc" value="save">
<input type="hidden" name="oxid" value="<?php echo $this->_tpl_vars['oxid']; ?>
">
<input type="hidden" name="editval[oxshops__oxid]" value="<?php echo $this->_tpl_vars['oxid']; ?>
">

<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => "include/update_views_notice.tpl", 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>

    <table border="0" width="98%">
    
    <?php if ($this->_tpl_vars['oxid'] != '1'): ?>
        <tr>
         <td valign="top" class="conftext">
            <input type=text class="confinput" style="width:270" name=confstrs[sMallShopURL] value="<?php echo $this->_tpl_vars['confstrs']['sMallShopURL']; ?>
" <?php echo $this->_tpl_vars['readonly']; ?>
>
            <?php echo smarty_function_oxinputhelp(array('ident' => 'HELP_SHOP_MALL_SHOPURL'), $this);?>

         </td>
         <td valign="top" class="conftext" width="100%">
           <?php echo smarty_function_oxmultilang(array('ident' => 'SHOP_MALL_SHOPURL'), $this);?>

         </td>
        </tr>


        <tr>
         <td valign="top" class="conftext2">
            <input type=text class="confinput" style="width:270" name=confstrs[sMallSSLShopURL] value="<?php echo $this->_tpl_vars['confstrs']['sMallSSLShopURL']; ?>
" <?php echo $this->_tpl_vars['readonly']; ?>
>
            <?php echo smarty_function_oxinputhelp(array('ident' => 'HELP_SHOP_MALL_SHOPSSLSHOPURL'), $this);?>

         </td>
         <td valign="top" class="conftext2" width="100%">
           <?php echo smarty_function_oxmultilang(array('ident' => 'SHOP_MALL_SHOPSSLSHOPURL'), $this);?>

         </td>
        </tr>

        <tr>
         <td valign="top" class="conftext">
            <input type=hidden name=confbools[blNativeImages] value=false>
            <input type=checkbox name=confbools[blNativeImages] value=true  <?php if (( $this->_tpl_vars['confbools']['blNativeImages'] )): ?>checked<?php endif; ?> <?php echo $this->_tpl_vars['readonly']; ?>
>
            <?php echo smarty_function_oxinputhelp(array('ident' => 'HELP_SHOP_MALL_NATIVEIMAGES'), $this);?>

         </td>
         <td valign="top" class="conftext" width="100%">
            <?php echo smarty_function_oxmultilang(array('ident' => 'SHOP_MALL_NATIVEIMAGES'), $this);?>

         </td>
        </tr>
    <?php else: ?>
        <tr>
         <td valign="top" class="conftext">
            <select name=confstrs[iMallMode] class="confinput" style="width: 270" <?php echo $this->_tpl_vars['readonly']; ?>
>
                <option value=0 <?php if (( $this->_tpl_vars['confstrs']['iMallMode'] == 0 )): ?>selected<?php endif; ?>><?php echo smarty_function_oxmultilang(array('ident' => 'SHOP_MALL_NOSTARTSITE'), $this);?>

                <option value=1 <?php if (( $this->_tpl_vars['confstrs']['iMallMode'] == 1 )): ?>selected<?php endif; ?>><?php echo smarty_function_oxmultilang(array('ident' => 'SHOP_MALL_STARTSITE'), $this);?>

            </select>
            <?php echo smarty_function_oxinputhelp(array('ident' => 'HELP_SHOP_MALL_MALLMODE'), $this);?>

         </td>
         <td valign="top" class="conftext" width="100%">
           <?php echo smarty_function_oxmultilang(array('ident' => 'SHOP_MALL_MALLMODE'), $this);?>

         </td>
        </tr>
    <?php endif; ?>
        <tr>
         <td valign="top" class="conftext2">
            <input type=hidden name=confbools[blSeparateNumbering] value=false>
            <input type=checkbox name=confbools[blSeparateNumbering] value=true  <?php if (( $this->_tpl_vars['confbools']['blSeparateNumbering'] )): ?>checked<?php endif; ?> <?php echo $this->_tpl_vars['readonly']; ?>
>
            <?php echo smarty_function_oxinputhelp(array('ident' => 'HELP_SHOP_MALL_SEPARATENUMBERING'), $this);?>

         </td>
         <td valign="top" class="conftext2" width="100%">
            <?php echo smarty_function_oxmultilang(array('ident' => 'SHOP_MALL_SEPARATENUMBERING'), $this);?>

         </td>
        </tr>

        <tr>
         <td valign="top" class="conftext">

            <input type=text class="confinput" style="width:70" name=confstrs[iMallPriceAddition] value="<?php if ($this->_tpl_vars['confstrs']['iMallPriceAddition']): ?><?php echo $this->_tpl_vars['confstrs']['iMallPriceAddition']; ?>
<?php else: ?>0<?php endif; ?>" <?php echo $this->_tpl_vars['readonly']; ?>
>
            <select class="confinput" name=confbools[blMallPriceAdditionPercent] <?php echo $this->_tpl_vars['readonly']; ?>
>
              <option value=true <?php if ($this->_tpl_vars['confbools']['blMallPriceAdditionPercent']): ?>selected<?php endif; ?>>%</option>
              <option value=false <?php if (! $this->_tpl_vars['confbools']['blMallPriceAdditionPercent']): ?>selected<?php endif; ?>><?php echo $this->_tpl_vars['defCur']; ?>
</option>
            </select>
            <?php echo smarty_function_oxinputhelp(array('ident' => 'HELP_SHOP_MALL_PRICEADDITION'), $this);?>

         </td>
         <td valign="top" class="conftext" width="100%">
            <?php echo smarty_function_oxmultilang(array('ident' => 'SHOP_MALL_PRICEADDITION'), $this);?>

         </td>
        </tr>

        <tr>
         <td valign="top" class="conftext2">
            <input type=hidden name=confbools[blMallCustomPrice] value=false>
            <input type=checkbox name=confbools[blMallCustomPrice] value=true  <?php if (( $this->_tpl_vars['confbools']['blMallCustomPrice'] )): ?>checked<?php endif; ?> <?php echo $this->_tpl_vars['readonly']; ?>
>
            <?php echo smarty_function_oxinputhelp(array('ident' => 'HELP_SHOP_MALL_MALLCUSTOMPRICE'), $this);?>

         </td>
         <td valign="top" class="conftext2" width="100%">
            <?php echo smarty_function_oxmultilang(array('ident' => 'SHOP_MALL_MALLCUSTOMPRICE'), $this);?>

         </td>
        </tr>

     <?php if ($this->_tpl_vars['oxid'] == '1'): ?>
        <tr>
         <td valign="top" class="conftext">
            <input type=hidden name=confbools[blMallUsers] value=false>
            <input type=checkbox name=confbools[blMallUsers] value=true  <?php if (( $this->_tpl_vars['confbools']['blMallUsers'] )): ?>checked<?php endif; ?> <?php echo $this->_tpl_vars['readonly']; ?>
>
            <?php echo smarty_function_oxinputhelp(array('ident' => 'HELP_SHOP_MALL_MALLUSERS'), $this);?>

         </td>
         <td valign="top" class="conftext" width="100%">
            <?php echo smarty_function_oxmultilang(array('ident' => 'SHOP_MALL_MALLUSERS'), $this);?>

         </td>
        </tr>
    <?php endif; ?>

        <tr>
         <td valign="top" class="conftext">
         <input type="submit" class="confinput" name="save" value="<?php echo smarty_function_oxmultilang(array('ident' => 'GENERAL_SAVE'), $this);?>
" onClick="Javascript:document.myedit1.fnc.value='save'" <?php echo $this->_tpl_vars['readonly']; ?>
>
         </td>
         <td valign="top" class="conftext" width="100%">
         </td>
        </tr>
    
    </table>
</form>

<?php if ($this->_tpl_vars['showInheritanceUpdate']): ?>

    <hr>

<?php echo smarty_function_oxmultilang(array('ident' => 'SHOP_MALL_MALLINHERITANCE'), $this);?>

<br><br>

<form name="myedit2" id="myedit2" action="<?php echo $this->_tpl_vars['oViewConf']->getSelfLink(); ?>
" method="post">
<?php echo $this->_tpl_vars['oViewConf']->getHiddenSid(); ?>

<input type="hidden" name="cl" value="shop_mall">
<input type="hidden" name="fnc" value="changeInheritance">
<input type="hidden" name="oxid" value="<?php echo $this->_tpl_vars['oxid']; ?>
">
<input type="hidden" name="editval[oxshops__oxid]" value="<?php echo $this->_tpl_vars['oxid']; ?>
">

     <table>

        
            <tr>
             <td valign="top" class="conftext">
                <input type=hidden name=confbools[blMallInherit_oxarticles] value=false>
                <input type=checkbox name=confbools[blMallInherit_oxarticles] value=true  <?php if (( $this->_tpl_vars['confbools']['blMallInherit_oxarticles'] )): ?>checked<?php endif; ?> <?php echo $this->_tpl_vars['readonly']; ?>
>
             </td>
             <td valign="top" class="conftext" width="100%">
                <?php echo smarty_function_oxmultilang(array('ident' => 'SHOP_MALL_MALLINHERIT_OXARTICLES'), $this);?>

             </td>
            </tr>

            <tr>
             <td valign="top" class="conftext">
                <input type=hidden name=confbools[blMallInherit_oxattribute] value=false>
                <input type=checkbox name=confbools[blMallInherit_oxattribute] value=true  <?php if (( $this->_tpl_vars['confbools']['blMallInherit_oxattribute'] )): ?>checked<?php endif; ?> <?php echo $this->_tpl_vars['readonly']; ?>
>
             </td>
             <td valign="top" class="conftext" width="100%">
                <?php echo smarty_function_oxmultilang(array('ident' => 'SHOP_MALL_MALLINHERIT_OXATTRIBUTES'), $this);?>

             </td>
            </tr>

            <tr>
             <td valign="top" class="conftext">
                <input type=hidden name=confbools[blMallInherit_oxdiscount] value=false>
                <input type=checkbox name=confbools[blMallInherit_oxdiscount] value=true  <?php if (( $this->_tpl_vars['confbools']['blMallInherit_oxdiscount'] )): ?>checked<?php endif; ?> <?php echo $this->_tpl_vars['readonly']; ?>
>
             </td>
             <td valign="top" class="conftext" width="100%">
                <?php echo smarty_function_oxmultilang(array('ident' => 'SHOP_MALL_MALLINHERIT_OXDISCOUNT'), $this);?>

             </td>
            </tr>

            <tr>
             <td valign="top" class="conftext">
                <input type=hidden name=confbools[blMallInherit_oxdelivery] value=false>
                <input type=checkbox name=confbools[blMallInherit_oxdelivery] value=true  <?php if (( $this->_tpl_vars['confbools']['blMallInherit_oxdelivery'] )): ?>checked<?php endif; ?> <?php echo $this->_tpl_vars['readonly']; ?>
>
             </td>
             <td valign="top" class="conftext" width="100%">
                <?php echo smarty_function_oxmultilang(array('ident' => 'SHOP_MALL_MALLINHERIT_OXDELIVERY'), $this);?>

             </td>
            </tr>

            <tr>
             <td valign="top" class="conftext">
                <input type=hidden name=confbools[blMallInherit_oxlinks] value=false>
                <input type=checkbox name=confbools[blMallInherit_oxlinks] value=true  <?php if (( $this->_tpl_vars['confbools']['blMallInherit_oxlinks'] )): ?>checked<?php endif; ?> <?php echo $this->_tpl_vars['readonly']; ?>
>
             </td>
             <td valign="top" class="conftext" width="100%">
                <?php echo smarty_function_oxmultilang(array('ident' => 'SHOP_MALL_MALLINHERIT_OXLINKS'), $this);?>

             </td>
            </tr>

            <tr>
             <td valign="top" class="conftext">
                <input type=hidden name=confbools[blMallInherit_oxvoucherseries] value=false>
                <input type=checkbox name=confbools[blMallInherit_oxvoucherseries] value=true  <?php if (( $this->_tpl_vars['confbools']['blMallInherit_oxvoucherseries'] )): ?>checked<?php endif; ?> <?php echo $this->_tpl_vars['readonly']; ?>
>
             </td>
             <td valign="top" class="conftext" width="100%">
                <?php echo smarty_function_oxmultilang(array('ident' => 'SHOP_MALL_MALLINHERIT_OXVOUCHERSERIES'), $this);?>

             </td>
            </tr>

            <tr>
             <td valign="top" class="conftext">
                <input type=hidden name=confbools[blMallInherit_oxnews] value=false>
                <input type=checkbox name=confbools[blMallInherit_oxnews] value=true  <?php if (( $this->_tpl_vars['confbools']['blMallInherit_oxnews'] )): ?>checked<?php endif; ?> <?php echo $this->_tpl_vars['readonly']; ?>
>
             </td>
             <td valign="top" class="conftext" width="100%">
                <?php echo smarty_function_oxmultilang(array('ident' => 'SHOP_MALL_MALLINHERIT_OXNEWS'), $this);?>

             </td>
            </tr>

            <tr>
             <td valign="top" class="conftext">
                <input type=hidden name=confbools[blMallInherit_oxselectlist] value=false>
                <input type=checkbox name=confbools[blMallInherit_oxselectlist] value=true  <?php if (( $this->_tpl_vars['confbools']['blMallInherit_oxselectlist'] )): ?>checked<?php endif; ?> <?php echo $this->_tpl_vars['readonly']; ?>
>
             </td>
             <td valign="top" class="conftext" width="100%">
                <?php echo smarty_function_oxmultilang(array('ident' => 'SHOP_MALL_MALLINHERIT_OXSELECTLIST'), $this);?>

             </td>
            </tr>

            <tr>
             <td valign="top" class="conftext">
                <input type=hidden name=confbools[blMallInherit_oxvendor] value=false>
                <input type=checkbox name=confbools[blMallInherit_oxvendor] value=true  <?php if (( $this->_tpl_vars['confbools']['blMallInherit_oxvendor'] )): ?>checked<?php endif; ?> <?php echo $this->_tpl_vars['readonly']; ?>
>
             </td>
             <td valign="top" class="conftext" width="100%">
                <?php echo smarty_function_oxmultilang(array('ident' => 'SHOP_MALL_MALLINHERIT_OXVENDOR'), $this);?>

             </td>
            </tr>

            <tr>
             <td valign="top" class="conftext">
                <input type=hidden name=confbools[blMallInherit_oxmanufacturers] value=false>
                <input type=checkbox name=confbools[blMallInherit_oxmanufacturers] value=true  <?php if (( $this->_tpl_vars['confbools']['blMallInherit_oxmanufacturers'] )): ?>checked<?php endif; ?> <?php echo $this->_tpl_vars['readonly']; ?>
>
             </td>
             <td valign="top" class="conftext" width="100%">
                <?php echo smarty_function_oxmultilang(array('ident' => 'SHOP_MALL_MALLINHERIT_OXMANUFACTURER'), $this);?>

             </td>
            </tr>

            <tr>
             <td valign="top" class="conftext">
                <input type=hidden name=confbools[blMallInherit_oxwrapping] value=false>
                <input type=checkbox name=confbools[blMallInherit_oxwrapping] value=true  <?php if (( $this->_tpl_vars['confbools']['blMallInherit_oxwrapping'] )): ?>checked<?php endif; ?> <?php echo $this->_tpl_vars['readonly']; ?>
>
             </td>
             <td valign="top" class="conftext" width="100%">
                <?php echo smarty_function_oxmultilang(array('ident' => 'SHOP_MALL_MALLINHERIT_OXWRAPPING'), $this);?>

             </td>
            </tr>
            <tr>
             <td valign="top" class="conftext" colspan=2>
             <input type="submit" class="confinput" name="save" value="<?php echo smarty_function_oxmultilang(array('ident' => 'SHOP_MALL_SAVE_INHERITANCE'), $this);?>
" <?php echo $this->_tpl_vars['readonly']; ?>
>
             </td>
            </tr>
        

     </table>
</form>
    <?php endif; ?>

<?php if ($this->_tpl_vars['oViewConf']->isMultiShop()): ?>
<hr>
<br>
<form name="myedit3" id="myedit3" action="<?php echo $this->_tpl_vars['oViewConf']->getSelfLink(); ?>
" method="post">
<?php echo $this->_tpl_vars['oViewConf']->getHiddenSid(); ?>

<input type="hidden" name="cl" value="shop_mall">
<input type="hidden" name="fnc" value="changeInheritance">
<input type="hidden" name="oxid" value="<?php echo $this->_tpl_vars['oxid']; ?>
">
<input type="hidden" name="editval[oxshops__oxid]" value="<?php echo $this->_tpl_vars['oxid']; ?>
">

     <table>
        
            <tr>
             <td valign="top" class="conftext">
                <input type=hidden name=confbools[blMultishopInherit_oxcategories] value=false>
                <input type=checkbox name=confbools[blMultishopInherit_oxcategories] value=true  <?php if (( $this->_tpl_vars['confbools']['blMultishopInherit_oxcategories'] )): ?>checked<?php endif; ?> <?php echo $this->_tpl_vars['readonly']; ?>
>
             </td>
             <td valign="top" class="conftext" width="100%">
                <?php echo smarty_function_oxmultilang(array('ident' => 'SHOP_MALL_MALLINHERIT_OXCATEGORIES'), $this);?>

             </td>
            </tr>
            <tr>
             <td valign="top" class="conftext" colspan=2>
             <input type="submit" class="confinput" name="save" value="<?php echo smarty_function_oxmultilang(array('ident' => 'SHOP_MALL_SAVE_INHERITANCE'), $this);?>
" <?php echo $this->_tpl_vars['readonly']; ?>
>
             </td>
            </tr>
        

     </table>
</form>
    <?php endif; ?>

    <?php if ($this->_tpl_vars['showViewUpdate']): ?>
      <hr>
      <form name="regerateviews" id="regerateviews" action="<?php echo $this->_tpl_vars['oViewConf']->getSelfLink(); ?>
" method="post">
        <?php echo $this->_tpl_vars['oViewConf']->getHiddenSid(); ?>

        <input type="hidden" name="cl" value="shop_mall">
        <input type="hidden" name="fnc" value="updateViews">
        <input type="hidden" name="oxid" value="<?php echo $this->_tpl_vars['oxid']; ?>
">
        <br><?php echo smarty_function_oxmultilang(array('ident' => 'SHOP_MALL_UPDATEVIEWSINFO'), $this);?>
<br><br>
        <input class="confinput" type="Submit" value="<?php echo smarty_function_oxmultilang(array('ident' => 'SHOP_MALL_UPDATEVIEWSNOW'), $this);?>
" onClick="return confirm('<?php echo smarty_function_oxmultilang(array('ident' => 'SHOP_MALL_UPDATEVIEWSCONFIRM'), $this);?>
')" <?php echo $this->_tpl_vars['readonly']; ?>
>
    <?php endif; ?>

</form>

<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => "bottomnaviitem.tpl", 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>
<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => "bottomitem.tpl", 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>