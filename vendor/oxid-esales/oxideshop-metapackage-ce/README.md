OXID eShop metapackage
======================

This repository contains OXID eShop dependencies.

More information how to setup Shop:

  - http://oxid-eshop-developer-documentation.readthedocs.io/en/latest/getting_started/eshop_installation.html#eshop-installation-via-composer
  - http://oxid-eshop-developer-documentation.readthedocs.io/en/latest/getting_started/resources/ce/composer.html

