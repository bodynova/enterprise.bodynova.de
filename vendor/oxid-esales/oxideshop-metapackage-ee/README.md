OXID eShop EE metapackage
=========================

This repository contains OXID eShop EE dependencies.

Information how to setup Shop:

  - http://oxid-eshop-developer-documentation.readthedocs.io/en/latest/getting_started/eshop_installation.html#eshop-installation-via-composer
  - http://oxid-eshop-developer-documentation.readthedocs.io/en/latest/getting_started/resources/ee/composer.html
