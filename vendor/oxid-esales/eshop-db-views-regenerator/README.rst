OXID eShop views regenerator
============================

This component provides eShop the way of creating/recreating the views structure
from console command. The script should be accessible through the oe-eshop-facts
component. Information on how to use ``oe-eshop-db_views_regenerate`` script together
with ``oe-eshop-facts`` can be found in the following
`README <https://github.com/OXID-eSales/eshop-facts/blob/master/README.rst>`__.

Alternative way of running
--------------------------

ESHOP_BOOTSTRAP_PATH='source/bootstrap.php' vendor/bin/oe-eshop-db_views_regenerate