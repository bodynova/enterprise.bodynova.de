<?php
/**
 * This Software is the property of OXID eSales and is protected
 * by copyright law - it is NOT Freeware.
 *
 * Any unauthorized use of this software without a valid license key
 * is a violation of the license agreement and will be prosecuted by
 * civil and criminal law.
 *
 * @link          http://www.oxid-esales.com
 * @copyright (C) OXID eSales AG 2003-2016
 * @version       OXID eShop PE
 */

namespace OxidEsales\EshopProfessional;

/**
 * @inheritdoc
 */
class ClassMap extends \OxidEsales\EshopCommunity\Core\Edition\ClassMap
{

    /**
     * @inheritdoc
     */
    public function getOverridableMap()
    {
        return [
            'oxsystemeventhandler'      => \OxidEsales\Eshop\Core\SystemEventHandler::class,
            'oxexpirationemailbuilder'  => \OxidEsales\Eshop\Core\ExpirationEmailBuilder::class,
            'oxonlinelicensecheck'      => \OxidEsales\Eshop\Core\OnlineLicenseCheck::class,
            'oxmodulecache'             => \OxidEsales\Eshop\Core\Module\ModuleCache::class,
            'oxmoduleinstaller'         => \OxidEsales\Eshop\Core\Module\ModuleInstaller::class,
        ];
    }

    /**
     * @inheritdoc
     */
    public function getNotOverridableMap()
    {
        return [
            'oxserial'             => \OxidEsales\EshopProfessional\Core\Serial::class,
            'oxadmindetails'       => \OxidEsales\EshopProfessional\Application\Controller\Admin\AdminDetailsController::class,
            'oxadminview'          => \OxidEsales\EshopProfessional\Application\Controller\Admin\AdminController::class,
            'ajaxlistcomponent'    => \OxidEsales\EshopProfessional\Application\Controller\Admin\ListComponentAjax::class,
            'article_list'         => \OxidEsales\EshopProfessional\Application\Controller\Admin\ArticleList::class,
            'oxshopidcalculator'   => \OxidEsales\EshopProfessional\Core\ShopIdCalculator::class,
            'oxutilsobject'        => \OxidEsales\EshopProfessional\Core\UtilsObject::class,
            'oxonlinelicensecheck' => \OxidEsales\EshopProfessional\Core\OnlineLicenseCheck::class,
            'oxsystemeventhandler' => \OxidEsales\EshopProfessional\Core\SystemEventHandler::class,
        ];
    }
}
