<?php
/**
 * This Software is the property of OXID eSales and is protected
 * by copyright law - it is NOT Freeware.
 *
 * Any unauthorized use of this software without a valid license key
 * is a violation of the license agreement and will be prosecuted by
 * civil and criminal law.
 *
 * @link      http://www.oxid-esales.com
 * @copyright (C) OXID eSales AG 2003-2016
 * @version   OXID eShop PE
 */

namespace OxidEsales\EshopProfessional\Core;

use OxidEsales\Eshop\Core\Edition\EditionSelector;
use OxidEsales\Eshop\Core\Edition\EditionPathProvider;
use OxidEsales\Eshop\Core\Edition\EditionRootPathProvider;

/**
 * @inheritdoc
 */
class UtilsView extends \OxidEsales\EshopCommunity\Core\UtilsView
{
    /**
     * @inheritdoc
     */
    public function getTemplateDirs()
    {
        $pathSelector = $this->getProfessionalPathSelector();
        $editionTemplatesDirectory = $this->addActiveThemeId($pathSelector->getViewsDirectory());
        $this->setTemplateDir($editionTemplatesDirectory);

        return parent::getTemplateDirs();
    }

    /**
    * @inheritdoc
    */
    protected function _fillCommonSmartyProperties($smarty)
    {
        parent::_fillCommonSmartyProperties($smarty);
        array_unshift($smarty->plugins_dir, $this->getProfessionalPathSelector()->getSmartyPluginsDirectory());
    }

    /**
     * Create Path provider objects to get path.
     *
     * @return EditionPathProvider
     */
    protected function getProfessionalPathSelector()
    {
        return new EditionPathProvider(new EditionRootPathProvider(new EditionSelector(EditionSelector::PROFESSIONAL)));
    }
}
