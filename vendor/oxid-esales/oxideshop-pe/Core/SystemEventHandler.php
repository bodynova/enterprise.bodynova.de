<?php
/**
 * This Software is the property of OXID eSales and is protected
 * by copyright law - it is NOT Freeware.
 *
 * Any unauthorized use of this software without a valid license key
 * is a violation of the license agreement and will be prosecuted by
 * civil and criminal law.
 *
 * @link      http://www.oxid-esales.com
 * @copyright (C) OXID eSales AG 2003-2016
 * @version   OXID eShop PE
 */

namespace OxidEsales\EshopProfessional\Core;

use \oxRegistry;

/**
 * @inheritdoc
 */
class SystemEventHandler extends \OxidEsales\EshopCommunity\Core\SystemEventHandler
{
    /**
     * @inheritdoc
     */
    protected function _isSendingShopDataEnabled()
    {
        return true;
    }

    /**
     * Check if shop valid.
     * Redirect offline if not valid.
     */
    protected function _validateOffline()
    {
        if ($this->_needValidateShop() && !$this->_getConfig()->getSerial()->validateShop()) {
            oxRegistry::getUtils()->redirectOffline();
        }
    }

    /**
     * Performance - run these checks only each 5 times statistically.
     *
     * @return bool
     */
    private function _needValidateShop()
    {
        $oConfig = $this->_getConfig();

        return !$oConfig->isProductiveMode() || !((oxRegistry::get("oxUtilsDate")->getTime()) % 5) || $oConfig->getConfigParam('blShopStopped');
    }
}
