<?php
/**
 * This Software is the property of OXID eSales and is protected
 * by copyright law - it is NOT Freeware.
 *
 * Any unauthorized use of this software without a valid license key
 * is a violation of the license agreement and will be prosecuted by
 * civil and criminal law.
 *
 * @link      http://www.oxid-esales.com
 * @copyright (C) OXID eSales AG 2003-2016
 * @version   OXID eShop PE
 */

namespace OxidEsales\EshopProfessional\Core;

/**
 * @inheritdoc
 */
class SystemRequirements extends \OxidEsales\EshopCommunity\Core\SystemRequirements
{
    /**
     * Checks if Zend Optimizer extension is loaded
     *
     * @deprecated since 6.0 (2015-09-17); Zend Guard or Server is not required any more.
     *
     * @return integer
     */
    public function checkZendOptimizer()
    {
        /**
         * Just for displaying "green" light, because if ZEND Optimizer/Guard loader is not
         * installed you will see some info screen instead of default setup.
         */
        return 2;
    }

    /**
     * @inheritdoc
     */
    public function getRequiredModules()
    {
        return parent::getRequiredModules();
    }
}
