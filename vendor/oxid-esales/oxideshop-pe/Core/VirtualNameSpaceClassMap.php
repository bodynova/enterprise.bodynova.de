<?php
/**
 * This Software is the property of OXID eSales and is protected
 * by copyright law - it is NOT Freeware.
 *
 * Any unauthorized use of this software without a valid license key
 * is a violation of the license agreement and will be prosecuted by
 * civil and criminal law.
 *
 * @link      http://www.oxid-esales.com
 * @copyright (C) OXID eSales AG 2003-2016
 * @version   OXID eShop PE
 */

namespace OxidEsales\EshopProfessional\Core;

/**
 * This file holds the mapping of classes from the virtual namespace to the concrete classes of each edition.
 * Each edition has its own map file. The map files will be merged like this: CE <- PE <- EE
 * So the mapping to a concrete class will be overwritten, if a class exists in a different edition.
 *
 * @inheritdoc
 */
class VirtualNameSpaceClassMap extends \OxidEsales\EshopCommunity\Core\Edition\ClassMap
{

    /**
     * Return mapping of classes from the virtual namespace to concrete classes.
     *
     * @return array Map of classes in the virtual namespace to concrete classes
     */
    public function getOverridableMap()
    {
        return [
            'OxidEsales\Eshop\Application\Component\BasketComponent' => \OxidEsales\EshopProfessional\Application\Component\BasketComponent::class,
            'OxidEsales\Eshop\Application\Component\UserComponent' => \OxidEsales\EshopProfessional\Application\Component\UserComponent::class,
            'OxidEsales\Eshop\Application\Component\Widget\ArticleBox' => \OxidEsales\EshopProfessional\Application\Component\Widget\ArticleBox::class,
            'OxidEsales\Eshop\Application\Component\Widget\ArticleDetails' => \OxidEsales\EshopProfessional\Application\Component\Widget\ArticleDetails::class,
            'OxidEsales\Eshop\Application\Component\Widget\MiniBasket' => \OxidEsales\EshopProfessional\Application\Component\Widget\MiniBasket::class,
            'OxidEsales\Eshop\Application\Component\Widget\Rating' => \OxidEsales\EshopProfessional\Application\Component\Widget\Rating::class,
            'OxidEsales\Eshop\Application\Component\Widget\Review' => \OxidEsales\EshopProfessional\Application\Component\Widget\Review::class,
            'OxidEsales\Eshop\Application\Component\Widget\ServiceMenu' => \OxidEsales\EshopProfessional\Application\Component\Widget\ServiceMenu::class,
            'OxidEsales\Eshop\Application\Controller\AccountController' => \OxidEsales\EshopProfessional\Application\Controller\AccountController::class,
            'OxidEsales\Eshop\Application\Controller\AccountNoticeListController' => \OxidEsales\EshopProfessional\Application\Controller\AccountNoticeListController::class,
            'OxidEsales\Eshop\Application\Controller\Admin\ActionsArticleAjax' => \OxidEsales\EshopProfessional\Application\Controller\Admin\ActionsArticleAjax::class,
            'OxidEsales\Eshop\Application\Controller\Admin\ActionsGroupsAjax' => \OxidEsales\EshopProfessional\Application\Controller\Admin\ActionsGroupsAjax::class,
            'OxidEsales\Eshop\Application\Controller\Admin\ActionsList' => \OxidEsales\EshopProfessional\Application\Controller\Admin\ActionsList::class,
            'OxidEsales\Eshop\Application\Controller\Admin\ActionsMain' => \OxidEsales\EshopProfessional\Application\Controller\Admin\ActionsMain::class,
            'OxidEsales\Eshop\Application\Controller\Admin\ActionsMainAjax' => \OxidEsales\EshopProfessional\Application\Controller\Admin\ActionsMainAjax::class,
            'OxidEsales\Eshop\Application\Controller\Admin\ActionsOrderAjax' => \OxidEsales\EshopProfessional\Application\Controller\Admin\ActionsOrderAjax::class,
            'OxidEsales\Eshop\Application\Controller\Admin\AdminController' => \OxidEsales\EshopProfessional\Application\Controller\Admin\AdminController::class,
            'OxidEsales\Eshop\Application\Controller\Admin\AdminDetailsController' => \OxidEsales\EshopProfessional\Application\Controller\Admin\AdminDetailsController::class,
            'OxidEsales\Eshop\Application\Controller\Admin\AdminListController' => \OxidEsales\EshopProfessional\Application\Controller\Admin\AdminListController::class,
            'OxidEsales\Eshop\Application\Controller\Admin\AdminView' => \OxidEsales\EshopProfessional\Application\Controller\Admin\AdminView::class,
            'OxidEsales\Eshop\Application\Controller\Admin\ArticleAccessoriesAjax' => \OxidEsales\EshopProfessional\Application\Controller\Admin\ArticleAccessoriesAjax::class,
            'OxidEsales\Eshop\Application\Controller\Admin\ArticleAttributeAjax' => \OxidEsales\EshopProfessional\Application\Controller\Admin\ArticleAttributeAjax::class,
            'OxidEsales\Eshop\Application\Controller\Admin\ArticleCrosssellingAjax' => \OxidEsales\EshopProfessional\Application\Controller\Admin\ArticleCrosssellingAjax::class,
            'OxidEsales\Eshop\Application\Controller\Admin\ArticleExtend' => \OxidEsales\EshopProfessional\Application\Controller\Admin\ArticleExtend::class,
            'OxidEsales\Eshop\Application\Controller\Admin\ArticleExtendAjax' => \OxidEsales\EshopProfessional\Application\Controller\Admin\ArticleExtendAjax::class,
            'OxidEsales\Eshop\Application\Controller\Admin\ArticleList' => \OxidEsales\EshopProfessional\Application\Controller\Admin\ArticleList::class,
            'OxidEsales\Eshop\Application\Controller\Admin\ArticleMain' => \OxidEsales\EshopProfessional\Application\Controller\Admin\ArticleMain::class,
            'OxidEsales\Eshop\Application\Controller\Admin\ArticleOverview' => \OxidEsales\EshopProfessional\Application\Controller\Admin\ArticleOverview::class,
            'OxidEsales\Eshop\Application\Controller\Admin\ArticlePictures' => \OxidEsales\EshopProfessional\Application\Controller\Admin\ArticlePictures::class,
            'OxidEsales\Eshop\Application\Controller\Admin\ArticleSelectionAjax' => \OxidEsales\EshopProfessional\Application\Controller\Admin\ArticleSelectionAjax::class,
            'OxidEsales\Eshop\Application\Controller\Admin\ArticleStock' => \OxidEsales\EshopProfessional\Application\Controller\Admin\ArticleStock::class,
            'OxidEsales\Eshop\Application\Controller\Admin\AttributeCategoryAjax' => \OxidEsales\EshopProfessional\Application\Controller\Admin\AttributeCategoryAjax::class,
            'OxidEsales\Eshop\Application\Controller\Admin\AttributeMainAjax' => \OxidEsales\EshopProfessional\Application\Controller\Admin\AttributeMainAjax::class,
            'OxidEsales\Eshop\Application\Controller\Admin\AttributeOrderAjax' => \OxidEsales\EshopProfessional\Application\Controller\Admin\AttributeOrderAjax::class,
            'OxidEsales\Eshop\Application\Controller\Admin\CategoryMain' => \OxidEsales\EshopProfessional\Application\Controller\Admin\CategoryMain::class,
            'OxidEsales\Eshop\Application\Controller\Admin\CategoryMainAjax' => \OxidEsales\EshopProfessional\Application\Controller\Admin\CategoryMainAjax::class,
            'OxidEsales\Eshop\Application\Controller\Admin\CategoryOrderAjax' => \OxidEsales\EshopProfessional\Application\Controller\Admin\CategoryOrderAjax::class,
            'OxidEsales\Eshop\Application\Controller\Admin\ContentList' => \OxidEsales\EshopProfessional\Application\Controller\Admin\ContentList::class,
            'OxidEsales\Eshop\Application\Controller\Admin\CountryList' => \OxidEsales\EshopProfessional\Application\Controller\Admin\CountryList::class,
            'OxidEsales\Eshop\Application\Controller\Admin\CountryMain' => \OxidEsales\EshopProfessional\Application\Controller\Admin\CountryMain::class,
            'OxidEsales\Eshop\Application\Controller\Admin\DiscountArticlesAjax' => \OxidEsales\EshopProfessional\Application\Controller\Admin\DiscountArticlesAjax::class,
            'OxidEsales\Eshop\Application\Controller\Admin\DiscountCategoriesAjax' => \OxidEsales\EshopProfessional\Application\Controller\Admin\DiscountCategoriesAjax::class,
            'OxidEsales\Eshop\Application\Controller\Admin\DiscountGroupsAjax' => \OxidEsales\EshopProfessional\Application\Controller\Admin\DiscountGroupsAjax::class,
            'OxidEsales\Eshop\Application\Controller\Admin\DiscountMainAjax' => \OxidEsales\EshopProfessional\Application\Controller\Admin\DiscountMainAjax::class,
            'OxidEsales\Eshop\Application\Controller\Admin\DiscountUsersAjax' => \OxidEsales\EshopProfessional\Application\Controller\Admin\DiscountUsersAjax::class,
            'OxidEsales\Eshop\Application\Controller\Admin\DynamicExportBaseController' => \OxidEsales\EshopProfessional\Application\Controller\Admin\DynamicExportBaseController::class,
            'OxidEsales\Eshop\Application\Controller\Admin\LanguageList' => \OxidEsales\EshopProfessional\Application\Controller\Admin\LanguageList::class,
            'OxidEsales\Eshop\Application\Controller\Admin\LanguageMain' => \OxidEsales\EshopProfessional\Application\Controller\Admin\LanguageMain::class,
            'OxidEsales\Eshop\Application\Controller\Admin\ListComponentAjax' => \OxidEsales\EshopProfessional\Application\Controller\Admin\ListComponentAjax::class,
            'OxidEsales\Eshop\Application\Controller\Admin\LoginController' => \OxidEsales\EshopProfessional\Application\Controller\Admin\LoginController::class,
            'OxidEsales\Eshop\Application\Controller\Admin\ManufacturerMainAjax' => \OxidEsales\EshopProfessional\Application\Controller\Admin\ManufacturerMainAjax::class,
            'OxidEsales\Eshop\Application\Controller\Admin\ModuleConfiguration' => \OxidEsales\EshopProfessional\Application\Controller\Admin\ModuleConfiguration::class,
            'OxidEsales\Eshop\Application\Controller\Admin\ModuleSortList' => \OxidEsales\EshopProfessional\Application\Controller\Admin\ModuleSortList::class,
            'OxidEsales\Eshop\Application\Controller\Admin\NavigationController' => \OxidEsales\EshopProfessional\Application\Controller\Admin\NavigationController::class,
            'OxidEsales\Eshop\Application\Controller\Admin\NavigationTree' => \OxidEsales\EshopProfessional\Application\Controller\Admin\NavigationTree::class,
            'OxidEsales\Eshop\Application\Controller\Admin\NewsMainAjax' => \OxidEsales\EshopProfessional\Application\Controller\Admin\NewsMainAjax::class,
            'OxidEsales\Eshop\Application\Controller\Admin\OrderList' => \OxidEsales\EshopProfessional\Application\Controller\Admin\OrderList::class,
            'OxidEsales\Eshop\Application\Controller\Admin\OrderMain' => \OxidEsales\EshopProfessional\Application\Controller\Admin\OrderMain::class,
            'OxidEsales\Eshop\Application\Controller\Admin\PaymentMain' => \OxidEsales\EshopProfessional\Application\Controller\Admin\PaymentMain::class,
            'OxidEsales\Eshop\Application\Controller\Admin\PriceAlarmList' => \OxidEsales\EshopProfessional\Application\Controller\Admin\PriceAlarmList::class,
            'OxidEsales\Eshop\Application\Controller\Admin\PriceAlarmMain' => \OxidEsales\EshopProfessional\Application\Controller\Admin\PriceAlarmMain::class,
            'OxidEsales\Eshop\Application\Controller\Admin\SelectListMainAjax' => \OxidEsales\EshopProfessional\Application\Controller\Admin\SelectListMainAjax::class,
            'OxidEsales\Eshop\Application\Controller\Admin\ShopConfiguration' => \OxidEsales\EshopProfessional\Application\Controller\Admin\ShopConfiguration::class,
            'OxidEsales\Eshop\Application\Controller\Admin\ShopController' => \OxidEsales\EshopProfessional\Application\Controller\Admin\ShopController::class,
            'OxidEsales\Eshop\Application\Controller\Admin\ShopLicense' => \OxidEsales\EshopProfessional\Application\Controller\Admin\ShopLicense::class,
            'OxidEsales\Eshop\Application\Controller\Admin\ShopList' => \OxidEsales\EshopProfessional\Application\Controller\Admin\ShopList::class,
            'OxidEsales\Eshop\Application\Controller\Admin\ShopMain' => \OxidEsales\EshopProfessional\Application\Controller\Admin\ShopMain::class,
            'OxidEsales\Eshop\Application\Controller\Admin\ShopSeo' => \OxidEsales\EshopProfessional\Application\Controller\Admin\ShopSeo::class,
            'OxidEsales\Eshop\Application\Controller\Admin\SystemInfoController' => \OxidEsales\EshopProfessional\Application\Controller\Admin\SystemInfoController::class,
            'OxidEsales\Eshop\Application\Controller\Admin\UserGroupList' => \OxidEsales\EshopProfessional\Application\Controller\Admin\UserGroupList::class,
            'OxidEsales\Eshop\Application\Controller\Admin\UserGroupMain' => \OxidEsales\EshopProfessional\Application\Controller\Admin\UserGroupMain::class,
            'OxidEsales\Eshop\Application\Controller\Admin\UserList' => \OxidEsales\EshopProfessional\Application\Controller\Admin\UserList::class,
            'OxidEsales\Eshop\Application\Controller\Admin\UserMain' => \OxidEsales\EshopProfessional\Application\Controller\Admin\UserMain::class,
            'OxidEsales\Eshop\Application\Controller\Admin\VendorMainAjax' => \OxidEsales\EshopProfessional\Application\Controller\Admin\VendorMainAjax::class,
            'OxidEsales\Eshop\Application\Controller\ArticleDetailsController' => \OxidEsales\EshopProfessional\Application\Controller\ArticleDetailsController::class,
            'OxidEsales\Eshop\Application\Controller\ArticleListController' => \OxidEsales\EshopProfessional\Application\Controller\ArticleListController::class,
            'OxidEsales\Eshop\Application\Controller\BasketController' => \OxidEsales\EshopProfessional\Application\Controller\BasketController::class,
            'OxidEsales\Eshop\Application\Controller\ClearCookiesController' => \OxidEsales\EshopProfessional\Application\Controller\ClearCookiesController::class,
            'OxidEsales\Eshop\Application\Controller\CompareController' => \OxidEsales\EshopProfessional\Application\Controller\CompareController::class,
            'OxidEsales\Eshop\Application\Controller\FrontendController' => \OxidEsales\EshopProfessional\Application\Controller\FrontendController::class,
            'OxidEsales\Eshop\Application\Controller\OrderController' => \OxidEsales\EshopProfessional\Application\Controller\OrderController::class,
            'OxidEsales\Eshop\Application\Controller\OxidStartController' => \OxidEsales\EshopProfessional\Application\Controller\OxidStartController::class,
            'OxidEsales\Eshop\Application\Controller\PaymentController' => \OxidEsales\EshopProfessional\Application\Controller\PaymentController::class,
            'OxidEsales\Eshop\Application\Controller\RecommListController' => \OxidEsales\EshopProfessional\Application\Controller\RecommListController::class,
            'OxidEsales\Eshop\Application\Controller\RssController' => \OxidEsales\EshopProfessional\Application\Controller\RssController::class,
            'OxidEsales\Eshop\Application\Controller\SearchController' => \OxidEsales\EshopProfessional\Application\Controller\SearchController::class,
            'OxidEsales\Eshop\Application\Controller\StartController' => \OxidEsales\EshopProfessional\Application\Controller\StartController::class,
            'OxidEsales\Eshop\Application\Controller\ThankYouController' => \OxidEsales\EshopProfessional\Application\Controller\ThankYouController::class,
            'OxidEsales\Eshop\Application\Controller\UserController' => \OxidEsales\EshopProfessional\Application\Controller\UserController::class,
            'OxidEsales\Eshop\Application\Controller\WishListController' => \OxidEsales\EshopProfessional\Application\Controller\WishListController::class,
            'OxidEsales\Eshop\Application\Controller\WrappingController' => \OxidEsales\EshopProfessional\Application\Controller\WrappingController::class,
            'OxidEsales\Eshop\Application\Model\Actions' => \OxidEsales\EshopProfessional\Application\Model\Actions::class,
            'OxidEsales\Eshop\Application\Model\AmountPriceList' => \OxidEsales\EshopProfessional\Application\Model\AmountPriceList::class,
            'OxidEsales\Eshop\Application\Model\Article' => \OxidEsales\EshopProfessional\Application\Model\Article::class,
            'OxidEsales\Eshop\Application\Model\ArticleList' => \OxidEsales\EshopProfessional\Application\Model\ArticleList::class,
            'OxidEsales\Eshop\Application\Model\Attribute' => \OxidEsales\EshopProfessional\Application\Model\Attribute::class,
            'OxidEsales\Eshop\Application\Model\BasketItem' => \OxidEsales\EshopProfessional\Application\Model\BasketItem::class,
            'OxidEsales\Eshop\Application\Model\Category' => \OxidEsales\EshopProfessional\Application\Model\Category::class,
            'OxidEsales\Eshop\Application\Model\CategoryList' => \OxidEsales\EshopProfessional\Application\Model\CategoryList::class,
            'OxidEsales\Eshop\Application\Model\Content' => \OxidEsales\EshopProfessional\Application\Model\Content::class,
            'OxidEsales\Eshop\Application\Model\ContentList' => \OxidEsales\EshopProfessional\Application\Model\ContentList::class,
            'OxidEsales\Eshop\Application\Model\Contract\ArticleInterface' => \OxidEsales\EshopProfessional\Application\Model\Contract\ArticleInterface::class,
            'OxidEsales\Eshop\Application\Model\Contract\CacheConnectorInterface' => \OxidEsales\EshopProfessional\Application\Model\Contract\CacheConnectorInterface::class,
            'OxidEsales\Eshop\Application\Model\Country' => \OxidEsales\EshopProfessional\Application\Model\Country::class,
            'OxidEsales\Eshop\Application\Model\Delivery' => \OxidEsales\EshopProfessional\Application\Model\Delivery::class,
            'OxidEsales\Eshop\Application\Model\DeliverySet' => \OxidEsales\EshopProfessional\Application\Model\DeliverySet::class,
            'OxidEsales\Eshop\Application\Model\Discount' => \OxidEsales\EshopProfessional\Application\Model\Discount::class,
            'OxidEsales\Eshop\Application\Model\Groups' => \OxidEsales\EshopProfessional\Application\Model\Groups::class,
            'OxidEsales\Eshop\Application\Model\Links' => \OxidEsales\EshopProfessional\Application\Model\Links::class,
            'OxidEsales\Eshop\Application\Model\Manufacturer' => \OxidEsales\EshopProfessional\Application\Model\Manufacturer::class,
            'OxidEsales\Eshop\Application\Model\MediaUrl' => \OxidEsales\EshopProfessional\Application\Model\MediaUrl::class,
            'OxidEsales\Eshop\Application\Model\News' => \OxidEsales\EshopProfessional\Application\Model\News::class,
            'OxidEsales\Eshop\Application\Model\NewsSubscribed' => \OxidEsales\EshopProfessional\Application\Model\NewsSubscribed::class,
            'OxidEsales\Eshop\Application\Model\Object2Category' => \OxidEsales\EshopProfessional\Application\Model\Object2Category::class,
            'OxidEsales\Eshop\Application\Model\Order' => \OxidEsales\EshopProfessional\Application\Model\Order::class,
            'OxidEsales\Eshop\Application\Model\OrderArticle' => \OxidEsales\EshopProfessional\Application\Model\OrderArticle::class,
            'OxidEsales\Eshop\Application\Model\Rating' => \OxidEsales\EshopProfessional\Application\Model\Rating::class,
            'OxidEsales\Eshop\Application\Model\RecommendationList' => \OxidEsales\EshopProfessional\Application\Model\RecommendationList::class,
            'OxidEsales\Eshop\Application\Model\Review' => \OxidEsales\EshopProfessional\Application\Model\Review::class,
            'OxidEsales\Eshop\Application\Model\SelectList' => \OxidEsales\EshopProfessional\Application\Model\SelectList::class,
            'OxidEsales\Eshop\Application\Model\Shop' => \OxidEsales\EshopProfessional\Application\Model\Shop::class,
            'OxidEsales\Eshop\Application\Model\ShopList' => \OxidEsales\EshopProfessional\Application\Model\ShopList::class,
            'OxidEsales\Eshop\Application\Model\ShopViewValidator' => \OxidEsales\EshopProfessional\Application\Model\ShopViewValidator::class,
            'OxidEsales\Eshop\Application\Model\SimpleVariant' => \OxidEsales\EshopProfessional\Application\Model\SimpleVariant::class,
            'OxidEsales\Eshop\Application\Model\User' => \OxidEsales\EshopProfessional\Application\Model\User::class,
            'OxidEsales\Eshop\Application\Model\Vendor' => \OxidEsales\EshopProfessional\Application\Model\Vendor::class,
            'OxidEsales\Eshop\Application\Model\VoucherSerie' => \OxidEsales\EshopProfessional\Application\Model\VoucherSerie::class,
            'OxidEsales\Eshop\ClassMap' => \OxidEsales\EshopProfessional\ClassMap::class,
            'OxidEsales\Eshop\Core\AdminLogSqlDecorator' => \OxidEsales\EshopProfessional\Core\AdminLogSqlDecorator::class,
            'OxidEsales\Eshop\Core\Base' => \OxidEsales\EshopProfessional\Core\Base::class,
            'OxidEsales\Eshop\Core\Config' => \OxidEsales\EshopProfessional\Core\Config::class,
            'OxidEsales\Eshop\Core\Controller\BaseController' => \OxidEsales\EshopProfessional\Core\Controller\BaseController::class,
            'OxidEsales\Eshop\Core\DatabaseProvider' => \OxidEsales\EshopProfessional\Core\DatabaseProvider::class,
            'OxidEsales\Eshop\Core\Database\Adapter\Doctrine\Database' => \OxidEsales\EshopProfessional\Core\Database\Adapter\Doctrine\Database::class,
            'OxidEsales\Eshop\Core\DbMetaDataHandler' => \OxidEsales\EshopProfessional\Core\DbMetaDataHandler::class,
            'OxidEsales\Eshop\Core\DebugInfo' => \OxidEsales\EshopProfessional\Core\DebugInfo::class,
            'OxidEsales\Eshop\Core\ExpirationEmailBuilder' => \OxidEsales\EshopProfessional\Core\ExpirationEmailBuilder::class,
            'OxidEsales\Eshop\Core\GenericImport\GenericImport' => \OxidEsales\EshopProfessional\Core\GenericImport\GenericImport::class,
            'OxidEsales\Eshop\Core\GenericImport\ImportObject\Article' => \OxidEsales\EshopProfessional\Core\GenericImport\ImportObject\Article::class,
            'OxidEsales\Eshop\Core\GenericImport\ImportObject\Article2Category' => \OxidEsales\EshopProfessional\Core\GenericImport\ImportObject\Article2Category::class,
            'OxidEsales\Eshop\Core\GenericImport\ImportObject\OrderArticle' => \OxidEsales\EshopProfessional\Core\GenericImport\ImportObject\OrderArticle::class,
            'OxidEsales\Eshop\Core\GenericImport\ImportObject\User' => \OxidEsales\EshopProfessional\Core\GenericImport\ImportObject\User::class,
            'OxidEsales\Eshop\Core\Header' => \OxidEsales\EshopProfessional\Core\Header::class,
            'OxidEsales\Eshop\Core\Language' => \OxidEsales\EshopProfessional\Core\Language::class,
            'OxidEsales\Eshop\Core\Model\BaseModel' => \OxidEsales\EshopProfessional\Core\Model\BaseModel::class,
            'OxidEsales\Eshop\Core\Model\MultiLanguageModel' => \OxidEsales\EshopProfessional\Core\Model\MultiLanguageModel::class,
            'OxidEsales\Eshop\Core\Module\ModuleCache' => \OxidEsales\EshopProfessional\Core\Module\ModuleCache::class,
            'OxidEsales\Eshop\Core\Module\ModuleInstaller' => \OxidEsales\EshopProfessional\Core\Module\ModuleInstaller::class,
            'OxidEsales\Eshop\Core\OnlineLicenseCheck' => \OxidEsales\EshopProfessional\Core\OnlineLicenseCheck::class,
            'OxidEsales\Eshop\Core\Output' => \OxidEsales\EshopProfessional\Core\Output::class,
            'OxidEsales\Eshop\Core\Serial' => \OxidEsales\EshopProfessional\Core\Serial::class,
            'OxidEsales\Eshop\Core\Session' => \OxidEsales\EshopProfessional\Core\Session::class,
            'OxidEsales\Eshop\Core\ShopControl' => \OxidEsales\EshopProfessional\Core\ShopControl::class,
            'OxidEsales\Eshop\Core\ShopIdCalculator' => \OxidEsales\EshopProfessional\Core\ShopIdCalculator::class,
            'OxidEsales\Eshop\Core\SystemEventHandler' => \OxidEsales\EshopProfessional\Core\SystemEventHandler::class,
            'OxidEsales\Eshop\Core\SystemRequirements' => \OxidEsales\EshopProfessional\Core\SystemRequirements::class,
            'OxidEsales\Eshop\Core\TableViewNameGenerator' => \OxidEsales\EshopProfessional\Core\TableViewNameGenerator::class,
            'OxidEsales\Eshop\Core\Utils' => \OxidEsales\EshopProfessional\Core\Utils::class,
            'OxidEsales\Eshop\Core\UtilsCount' => \OxidEsales\EshopProfessional\Core\UtilsCount::class,
            'OxidEsales\Eshop\Core\UtilsObject' => \OxidEsales\EshopProfessional\Core\UtilsObject::class,
            'OxidEsales\Eshop\Core\UtilsUrl' => \OxidEsales\EshopProfessional\Core\UtilsUrl::class,
            'OxidEsales\Eshop\Core\UtilsView' => \OxidEsales\EshopProfessional\Core\UtilsView::class,
            'OxidEsales\Eshop\Core\ViewConfig' => \OxidEsales\EshopProfessional\Core\ViewConfig::class,
            'OxidEsales\Eshop\Core\ViewHelper\JavaScriptRenderer' => \OxidEsales\EshopProfessional\Core\ViewHelper\JavaScriptRenderer::class,
            'OxidEsales\Eshop\Core\ViewHelper\StyleRenderer' => \OxidEsales\EshopProfessional\Core\ViewHelper\StyleRenderer::class,
            'OxidEsales\Eshop\Core\VirtualNameSpaceClassMap' => \OxidEsales\EshopProfessional\Core\VirtualNameSpaceClassMap::class,
            'OxidEsales\Eshop\Core\WidgetControl' => \OxidEsales\EshopProfessional\Core\WidgetControl::class,

        ];
    }

    /**
     * Returns class map, of classes which can't be extended by modules.
     * There are no use cases for virtual namespaces in not overridable classes at the moment.
     * This function will return always an empty array.
     *
     * @return array  Maps a class from the virtual namespace to a concrete class
     */
    public function getNotOverridableMap()
    {
        return [];
    }
}
