<?php
/**
 * This Software is the property of OXID eSales and is protected
 * by copyright law - it is NOT Freeware.
 *
 * Any unauthorized use of this software without a valid license key
 * is a violation of the license agreement and will be prosecuted by
 * civil and criminal law.
 *
 * @link      http://www.oxid-esales.com
 * @copyright (C) OXID eSales AG 2003-2016
 * @version   OXID eShop PE
 */

namespace OxidEsales\EshopProfessional\Application\Controller\Admin;

use OxidEsales\Eshop\Core\Serial;

/**
 * @inheritdoc
 */
class LoginController extends \OxidEsales\EshopCommunity\Application\Controller\Admin\LoginController
{
    /**
     * Returns shop validation error message.
     *
     * @return string
     */
    public function getShopValidationMessage()
    {
        $error = '';
        $serial = $this->getConfig()->getSerial();
        if ($serial->isGracePeriodStarted()) {
            $serial->validateShop();
            if (!$serial->isShopValid()) {
                $error = $serial->getValidationMessage();
            }
        }

        return $error;
    }

    /**
     * Returns whether shop grace period expired.
     *
     * @return bool
     */
    public function isGracePeriodExpired()
    {
        $serial = $this->getConfig()->getSerial();

        return $serial->isGracePeriodExpired();
    }
}
