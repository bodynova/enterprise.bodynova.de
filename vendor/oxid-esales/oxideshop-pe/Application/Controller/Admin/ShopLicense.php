<?php
/**
 * This Software is the property of OXID eSales and is protected
 * by copyright law - it is NOT Freeware.
 *
 * Any unauthorized use of this software without a valid license key
 * is a violation of the license agreement and will be prosecuted by
 * civil and criminal law.
 *
 * @link      http://www.oxid-esales.com
 * @copyright (C) OXID eSales AG 2003-2016
 * @version   OXID eShop PE
 */

namespace OxidEsales\EshopProfessional\Application\Controller\Admin;

use oxRegistry;
use oxAdminDetails;
use oxDb;

/**
 * @inheritdoc
 */
class ShopLicense extends \Shop_License
{
    /**
     * @inheritdoc
     */
    private $versionCheckLink = 'http://admin.oxid-esales.com/PE/onlinecheck.php';

    /**
     * @inheritdoc
     */
    public function init()
    {
        oxRegistry::getSession()->setVariable("actshop", $this->getEditObjectId());
        parent::init();
    }

    /**
     * @inheritdoc
     */
    public function render()
    {
        $templateName = parent::render();

        // Gather serials
        $config = $this->getConfig();
        $serialsList = $config->getConfigParam('aSerials');

        $this->_aViewData["oxserials"] = $serialsList;

        $serial = $config->getSerial(true);
        $serial->disableCacheModules();

        //renew module template data
        $serialList = array();

        $this->_aViewData['aSerial'] = $serialList;

        return $templateName;
    }

    /**
     * Saves shop Licensing data.
     *
     * @return null
     */
    public function save()
    {
        $config = $this->getConfig();

        if ($this->_canUpdate()) {
            oxAdminDetails::save();

            $language = oxRegistry::getLang();
            $parameters = oxRegistry::getConfig()->getRequestParameter("editval");
            //saving serial number
            $serialToAdd = trim($parameters['oxnewserial']);

            $serial = oxNew("oxserial");
            $serial->disableCacheModules();

            //if serial already exists we skip it
            $serialsList = $config->getConfigParam('aSerials');
            if (is_array($serialsList) && in_array($serialToAdd, $serialsList)) {
                return;
            }

            //perform online license key check
            $olc = $this->getOnlineLicenseCheck();
            if (!$olc->validateNewSerial($serialToAdd)) {
                $this->_aViewData['error'] = $language->translateString($olc->getErrorMessage());

                return;
            }

            if ($serial->isValidSerial($serialToAdd) && (!is_array($serialsList) || !in_array($serialToAdd, $serialsList))) {
                //processing error messages
                if (!$serial->isStackable($serialToAdd) && count($serialsList)) {
                    $this->_aViewData['error'] = $language->translateString('nonstackable_serial_detected');
                }

                //so we have four case of behabiour with adding new serial
                //1. New serial is stackable so we just add it, but no demo stackables are allowed on non demo serials and vice versa
                //2. New serial is non demo serial and existing serial is demo then we delete old and leave new one (in spite if new is stackable or not)
                //3. New serial is not stackable and there are no existing serial so we just add the new one.
                //4. New serial is not stackable and existing serial is not demo (or any other case) so we just not add it
                //      as it may be Eric's mistake or wrong serial

                //echo "existing serial: $myConfig->sSerialNr";
                $serialNr = $config->getConfigParam('sSerialNr');
                if ($serial->isStackable($serialToAdd) && !($serial->detectVersion($serialNr) == 1 xor $serial->detectVersion($serialToAdd) == 1)) {
                    //1.
                    $serialsList[] = $serialToAdd;
                    $this->_aViewData['message'] = $language->translateString('serial_added');
                    $this->_aViewData['error'] = '';
                }

                if ($serial->detectVersion($serialNr) == 1 && $serial->detectVersion($serialToAdd) != 1) {
                    //2.
                    $serialsList = array($serialToAdd);
                    $this->_aViewData['message'] = $language->translateString('serial_updated');
                    $this->_aViewData['error'] = '';
                }

                if (!$serial->isStackable($serialToAdd) && !count($serialsList)) {
                    //3.
                    $serialsList = array($serialToAdd);
                }

                //4. Do nothing
                $config->setConfigParam('aSerials', $serialsList);

                $this->updateShopSerial();
            } else {
                $this->_aViewData['error'] = $language->translateString('invalid_serial');
            }

            $config->saveShopConfVar("arr", "aSerials", $config->getConfigParam('aSerials'), $config->getBaseShopId());
        }
    }

    /**
     * OLC dependency getter
     *
     * @return oxOnlineLicenseCheck
     */
    public function getOnlineLicenseCheck()
    {
        $curl = oxNew('oxCurl');
        $emailBuilder = oxNew('oxOnlineServerEmailBuilder');
        $simpleXml = oxNew('oxSimpleXml');
        $licenseCaller = oxNew('oxOnlineLicenseCheckCaller', $curl, $emailBuilder, $simpleXml);
        $userCounter = oxNew('oxUserCounter');
        $serverManager = oxNew('oxServersManager');

        $onlineLicenseCheck = oxNew("oxOnlineLicenseCheck", $licenseCaller);
        $onlineLicenseCheck->setServersManager($serverManager);
        $onlineLicenseCheck->setUserCounter($userCounter);

        return $onlineLicenseCheck;
    }

    /**
     * Delete serial
     */
    public function deleteSerial()
    {
        $config = $this->getConfig();
        if ($this->_canUpdate()) {
            $newSerialsList = array();
            $serialsList = $config->getConfigParam('aSerials');
            $delSerial = oxRegistry::getConfig()->getRequestParameter("serial");
            foreach ($serialsList as $serial) {
                if ($serial != $delSerial) {
                    $newSerialsList[] = $serial;
                }
            }

            $config->saveShopConfVar("arr", "aSerials", $newSerialsList, $config->getBaseShopId());
            $this->updateShopSerial();
        }
    }

    /**
     * Updates serial
     *
     * @return null
     */
    public function updateShopSerial()
    {
        if (!$this->_canUpdate()) {
            return;
        }

        $config = $this->getConfig();

        //it summs all aSerials from config and then inserts the sum to oxshops
        //and caluclates maxdays maxarticles and maxshops
        $serialsList = $config->getConfigParam('aSerials');

        $shopSerial = '';
        $maxDays = 0;
        $maxArticles = 0;
        $maxShops = 0;

        if (is_array($serialsList) && count($serialsList)) {
            $serial = oxNew('oxserial');
            $serial->disableCacheModules();
            $shopSerial = $serialsList[0];
            $maxDays = $serial->getMaxDays($serialsList[0]);
            $maxArticles = $serial->getMaxArticles($serialsList[0]);
            $maxShops = $serial->getMaxShops($serialsList[0]);

            for ($i = 1; $i < count($serialsList); $i++) {
                $maxDays += $serial->getMaxDays($serialsList[$i]);
                $maxArticles += $serial->getMaxArticles($serialsList[$i]);
                $maxShops += $serial->getMaxShops($serialsList[$i]);
                $shopSerial = $serial->addSerial($shopSerial, $serialsList[$i]);
            }
        }

        //saving config max values
        $baseShopId = $config->getBaseShopId();
        $config->saveShopConfVar('str', 'IMD', $maxDays, $baseShopId);
        $config->saveShopConfVar('str', 'IMA', $maxArticles, $baseShopId);
        $config->saveShopConfVar('str', 'IMS', $maxShops, $baseShopId);

        //setting the shop serial
        $oldSerial = $config->getConfigParam('sSerialNr');
        if ($oldSerial != $shopSerial) {
            //storing
            $config->getSerial()->sSerial = $shopSerial;
            $config->setConfigParam('sSerialNr', $shopSerial);

            oxRegistry::getSession()->setVariable($baseShopId . 'oxserial', $shopSerial);

            //saving for all shops
            $db = oxDb::getDb();
            $db->Execute("update oxshops set oxserial = " . $db->quote($shopSerial));
            //setting to unexpired shop again

            $config->saveShopConfVar('bool', 'sShopVar', '', $baseShopId);
        }
        $this->_aViewData['isdemoversion'] = $config->detectVersion() == 1;
    }
}
