<?php
/**
 * This Software is the property of OXID eSales and is protected
 * by copyright law - it is NOT Freeware.
 *
 * Any unauthorized use of this software without a valid license key
 * is a violation of the license agreement and will be prosecuted by
 * civil and criminal law.
 *
 * @link      http://www.oxid-esales.com
 * @copyright (C) OXID eSales AG 2003-2016
 * @version   OXID eShop PE
 */

namespace OxidEsales\EshopProfessional\Application\Controller\Admin;

/**
 * @inheritdoc
 */
class ArticleAccessoriesAjax extends \OxidEsales\EshopCommunity\Application\Controller\Admin\ArticleAccessoriesAjax
{

}
