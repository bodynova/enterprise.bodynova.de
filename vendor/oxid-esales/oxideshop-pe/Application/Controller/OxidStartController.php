<?php
/**
 * This Software is the property of OXID eSales and is protected
 * by copyright law - it is NOT Freeware.
 *
 * Any unauthorized use of this software without a valid license key
 * is a violation of the license agreement and will be prosecuted by
 * civil and criminal law.
 *
 * @link      http://www.oxid-esales.com
 * @copyright (C) OXID eSales AG 2003-2016
 * @version   OXID eShop PE
 */

namespace OxidEsales\EshopProfessional\Application\Controller;

/**
 * Encapsulates methods for application initialization.
 */
class OxidStartController extends \OxidEsales\EshopCommunity\Application\Controller\OxidStartController
{
    /**
     * Creates and starts session object, sets default currency.
     */
    public function pageStart()
    {
        $config = $this->getConfig();

        $shopId = $config->getBaseShopId();
        $config->setConfigParam('IMS', $config->getShopConfVar('IMS', $shopId));
        $config->setConfigParam('IMD', $config->getShopConfVar('IMD', $shopId));
        $config->setConfigParam('IMA', $config->getShopConfVar('IMA', $shopId));
        $config->setConfigParam('aSerials', $config->getShopConfVar('aSerials', $shopId));
        $config->setConfigParam('sBackTag', $config->getShopConfVar('sBackTag', $shopId));
        $config->setConfigParam('sTagList', $config->getShopConfVar('sTagList', $shopId));

        parent::pageStart();
    }
}
