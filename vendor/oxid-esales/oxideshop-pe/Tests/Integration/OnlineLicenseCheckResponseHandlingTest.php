<?php
/**
 * This Software is the property of OXID eSales and is protected
 * by copyright law - it is NOT Freeware.
 *
 * Any unauthorized use of this software without a valid license key
 * is a violation of the license agreement and will be prosecuted by
 * civil and criminal law.
 *
 * @link      http://www.oxid-esales.com
 * @copyright (C) OXID eSales AG 2003-2016
 * @version   OXID eShop PE
 */

namespace OxidEsales\EshopProfessional\Tests\Integration;

use \oxRegistry;

class OnlineLicenseCheckResponseHandlingTest extends \oxUnitTestCase
{
    public function testRequestHandlingWithNegativeResponse()
    {
        $config = oxRegistry::getConfig();
        $config->setConfigParam('blShopStopped', false);
        $config->setConfigParam('sShopVar', '');

        $xml = '<?xml version="1.0" encoding="utf-8"?>'."\n";
        $xml .= '<olc>';
        $xml .=   '<code>1</code>';
        $xml .=   '<message>NACK</message>';
        $xml .= '</olc>'."\n";

        $curl = $this->getMock('oxCurl', array('execute'));
        $curl->expects($this->any())->method('execute')->will($this->returnValue($xml));
        /** @var oxCurl $curl */

        $emailBuilder = oxNew('oxOnlineServerEmailBuilder');
        $simpleXml = oxNew('oxSimpleXml');
        $licenseCaller = oxNew('oxOnlineLicenseCheckCaller', $curl, $emailBuilder, $simpleXml);

        $userCounter = oxNew('oxUserCounter');
        $licenseCheck = oxNew('oxOnlineLicenseCheck', $licenseCaller, $userCounter);

        $licenseCheck->validateShopSerials();

        $this->assertTrue($config->getConfigParam('blShopStopped'));
        $this->assertEquals('unlc', $config->getConfigParam('sShopVar'));
    }
}