<?php
/**
 * This Software is the property of OXID eSales and is protected
 * by copyright law - it is NOT Freeware.
 *
 * Any unauthorized use of this software without a valid license key
 * is a violation of the license agreement and will be prosecuted by
 * civil and criminal law.
 *
 * @link      http://www.oxid-esales.com
 * @copyright (C) OXID eSales AG 2003-2016
 * @version   OXID eShop PE
 */

namespace OxidEsales\EshopProfessional\Tests\Unit\Setup;

use OxidEsales\EshopCommunity\Setup\Core;
use PDO;
use stdClass;

class SetupDatabaseTest extends \OxidEsales\TestingLibrary\UnitTestCase
{
        /** @var array Queries will be logged here. */
        private $loggedQueries;

        /**
         * Resets logged queries.
         */
        protected function setUp()
        {
                parent::setUp();
                $this->loggedQueries = new stdClass();
        }

        /**
         * Testing SetupDb::writeSerial()
         */
        public function testWriteSerial()
        {
                $baseShopId = "testShopId";

                $utils = $this->getMock("Utilities", array("generateUid"));
                $utils->expects($this->any())->method("generateUid")->will($this->returnValue("testId"));

                $setup = $this->getMock("Setup", array("getShopId"));
                $setup->expects($this->any())->method("getShopId")->will($this->returnValue($baseShopId));

                $serial = $this->getMock("oxSerial", array("getMaxDays", "getMaxArticles", "getMaxShops"));
                $serial->expects($this->any())->method("getMaxDays")->will($this->returnValue(1));
                $serial->expects($this->any())->method("getMaxArticles")->will($this->returnValue(1));
                $serial->expects($this->any())->method("getMaxShops")->will($this->returnValue(1));

                $connection = $this->createConnectionMock();
                $iAt = 0;
                $database = $this->getMock(get_class($this->getDatabase()), array("getInstance", "execSql", 'getConnection'));
                $database->expects($this->at($iAt++))->method("getInstance")->with($this->equalTo("Utilities"))->will($this->returnValue($utils));
                $database->expects($this->at($iAt++))->method("getInstance")->with($this->equalTo("Setup"))->will($this->returnValue($setup));
                $database->expects($this->any())->method("getConnection")->will($this->returnValue($connection));

                $database->writeSerial($serial, "testSerial");

                $expectedQueries = array(
                    "update oxshops set oxserial = 'testSerial' where oxid = '$baseShopId'",
                    "delete from oxconfig where oxvarname = 'aSerials'",
                    "delete from oxconfig where oxvarname = 'sTagList'",
                    "delete from oxconfig where oxvarname = 'IMD'",
                    "delete from oxconfig where oxvarname = 'IMA'",
                    "delete from oxconfig where oxvarname = 'IMS'",
                );

                $this->assertEquals($expectedQueries, $this->getLoggedQueries());
        }

        /**
         * Logs exec queries instead of executing them.
         * Prepared statements will be executed as usual and will not be logged.
         *
         * @return PDO
         */
        protected function createConnectionMock()
        {
                $config = $this->getConfig();
                $dsn = sprintf('mysql:host=%s', $config->getConfigParam('dbHost'));
                $pdoMock = $this->getMock('PDO', array('exec'), array(
                    $dsn,
                    $config->getConfigParam('dbUser'),
                    $config->getConfigParam('dbPwd')));

                $loggedQueries = $this->loggedQueries;
                $pdoMock->expects($this->any())
                    ->method('exec')
                    ->will($this->returnCallback(function ($query) use ($loggedQueries) {
                            $loggedQueries->queries[] = $query;
                    }));

                return $pdoMock;
        }

        /**
         * Returns logged queries when mocked connection is used.
         *
         * @return array
         */
        protected function getLoggedQueries()
        {
                return $this->loggedQueries->queries;
        }

        /**
         * @return Core
         */
        protected function getDatabase()
        {
                $core = new Core();
                return $core->getInstance('Database');
        }
}
