<?php
/**
 * This Software is the property of OXID eSales and is protected
 * by copyright law - it is NOT Freeware.
 *
 * Any unauthorized use of this software without a valid license key
 * is a violation of the license agreement and will be prosecuted by
 * civil and criminal law.
 *
 * @link      http://www.oxid-esales.com
 * @copyright (C) OXID eSales AG 2003-2016
 * @version   OXID eShop PE
 */

namespace OxidEsales\EshopProfessional\Tests\Unit\Setup;

use Exception;
use OxidEsales\EshopCommunity\Setup\Core;
use OxidEsales\EshopProfessional\Setup\Controller;
use OxidEsales\EshopProfessional\Setup\Setup;

class SetupControllerTest extends \OxidEsales\TestingLibrary\UnitTestCase
{
    /**
     * Testing controller::serial()
     */
    public function testSerial()
    {
        $setup = $this->getMock(get_class($this->getSetup()), array("getDefaultSerial"));
        $setup->expects($this->once())->method("getDefaultSerial");

        $view = $this->getMock("ViewStub", array("setTitle", "setViewParam"));
        $view->expects($this->once())->method("setTitle")->with($this->equalTo("STEP_5_TITLE"));
        $view->expects($this->once())->method("setViewParam")->with($this->equalTo("sLicense"));

        $controller = $this->getMock(get_class($this->getController()), array("getView", "getInstance"));
        $controller->expects($this->at(0))->method("getView")->will($this->returnValue($view));
        $controller->expects($this->at(1))->method("getInstance")->with($this->equalTo("Setup"))->will($this->returnValue($setup));
        $this->assertEquals("serial.php", $controller->serial());
    }

    /**
     * Testing controller::serialSave()
     */
    public function testSerialSaveBadSerial()
    {
        $setup = $this->getMock(get_class($this->getSetup()), array("setSerial"));
        $setup->expects($this->once())->method("setSerial")->will($this->throwException(new Exception));

        $setupUtilities = $this->getMock("Utilities", array("getRequestVar"));
        $setupUtilities->expects($this->once())->method("getRequestVar");

        $view = $this->getMock("ViewStub", array("setTitle", "setMessage"));
        $view->expects($this->once())->method("setTitle")->with($this->equalTo("STEP_5_1_TITLE"));
        $view->expects($this->once())->method("setMessage");

        $controller = $this->getMock(get_class($this->getController()), array("getView", "getInstance"));
        $controller->expects($this->at(0))->method("getView")->will($this->returnValue($view));
        $controller->expects($this->at(1))->method("getInstance")->with($this->equalTo("Utilities"))->will($this->returnValue($setupUtilities));
        $controller->expects($this->at(2))->method("getInstance")->with($this->equalTo("Setup"))->will($this->returnValue($setup));
        $this->assertEquals("default.php", $controller->serialSave());
    }

    /**
     * Testing controller::serialSave()
     */
    public function testSerialSave()
    {
        $setup = $this->getMock(get_class($this->getSetup()), array("setSerial"));
        $setup->expects($this->once())->method("setSerial");

        $utilities = $this->getMock("Utilities", array("getRequestVar"));
        $utilities->expects($this->once())->method("getRequestVar");

        $view = $this->getMock("ViewStub", array("setTitle", "setMessage"));
        $view->expects($this->once())->method("setTitle")->with($this->equalTo("STEP_5_1_TITLE"));
        $view->expects($this->once())->method("setMessage");

        $controller = $this->getMock(get_class($this->getController()), array("getView", "getInstance"));
        $controller->expects($this->at(0))->method("getView")->will($this->returnValue($view));
        $controller->expects($this->at(1))->method("getInstance")->with($this->equalTo("Utilities"))->will($this->returnValue($utilities));
        $controller->expects($this->at(2))->method("getInstance")->with($this->equalTo("Setup"))->will($this->returnValue($setup));
        $this->assertEquals("default.php", $controller->serialSave());
    }


    /**
     * @return Controller
     */
    protected function getController()
    {
        $core = new Core();
        return $core->getInstance('Controller');
    }

    /**
     * @return Setup
     */
    protected function getSetup()
    {
        $core = new Core();
        return $core->getInstance('Setup');
    }
}
