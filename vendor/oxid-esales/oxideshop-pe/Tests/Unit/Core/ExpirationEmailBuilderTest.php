<?php
/**
 * This Software is the property of OXID eSales and is protected
 * by copyright law - it is NOT Freeware.
 *
 * Any unauthorized use of this software without a valid license key
 * is a violation of the license agreement and will be prosecuted by
 * civil and criminal law.
 *
 * @link      http://www.oxid-esales.com
 * @copyright (C) OXID eSales AG 2003-2016
 * @version   OXID eShop PE
 */

namespace OxidEsales\EshopProfessional\Tests\Unit\Core;

use \oxTestModules;
use \oxRegistry;

class ExpirationEmailBuilderTest extends \oxUnitTestCase
{
    public function providerGetEmail()
    {
        return array(
            array(0.5, oxRegistry::getLang()->translateString('SHOP_LICENSE_ERROR_GRACE_WILL_EXPIRE', null, true)),
            array(1, oxRegistry::getLang()->translateString('SHOP_LICENSE_ERROR_GRACE_WILL_EXPIRE', null, true)),
            array(7, oxRegistry::getLang()->translateString('SHOP_LICENSE_ERROR_shop_unlicensed', null, true)),
            array(100, oxRegistry::getLang()->translateString('SHOP_LICENSE_ERROR_shop_unlicensed', null, true)),
        );
    }

    /**
     * @param $sLeftDaysTillExpiration
     * @param $sExpectedEmailBody
     *
     * @dataProvider providerGetEmail
     */
    public function testGetEmailAndCheckIfBodyWasSetCorrectly($sLeftDaysTillExpiration, $sExpectedEmailBody)
    {
        $oExpirationEmailBuilder = oxNew('oxExpirationEmailBuilder');
        $oExpirationEmail = $oExpirationEmailBuilder->build($sLeftDaysTillExpiration);

        $this->assertSame(
            $sExpectedEmailBody,
            $oExpirationEmail->getBody(),
            'Email content is not that as it should be.'
        );
    }
}
