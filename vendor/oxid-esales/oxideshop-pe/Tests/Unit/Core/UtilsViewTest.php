<?php
/**
 * This Software is the property of OXID eSales and is protected
 * by copyright law - it is NOT Freeware.
 *
 * Any unauthorized use of this software without a valid license key
 * is a violation of the license agreement and will be prosecuted by
 * civil and criminal law.
 *
 * @link      http://www.oxid-esales.com
 * @copyright (C) OXID eSales AG 2003-2016
 * @version   OXID eShop PE
 */

namespace OxidEsales\EshopProfessional\Tests\Unit\Core;

use OxidEsales\Eshop\Core\Edition\EditionPathProvider;
use OxidEsales\Eshop\Core\Edition\EditionRootPathProvider;
use OxidEsales\Eshop\Core\Edition\EditionSelector;

class UtilsViewTest extends \oxUnitTestCase
{
    public function testGetEditionTemplateDirs()
    {
        if ($this->getTestConfig()->getShopEdition() !== 'PE') {
            $this->markTestSkipped('This test is for Professional edition only.');
        }

        $config = $this->getConfig();
        $shopPath = rtrim($config->getConfigParam('sShopDir'), '/') . '/';

        $professionalPathProvider = new EditionPathProvider(new EditionRootPathProvider(new EditionSelector(EditionSelector::PROFESSIONAL)));
        $dirs = array(
            $professionalPathProvider->getViewsDirectory() . 'azure/tpl/',
            $shopPath . 'Application/views/azure/tpl/',
            $shopPath . 'out/azure/tpl/',
        );

        $utilsView = $this->getMock('oxUtilsView', array('isAdmin'));
        $utilsView->expects($this->any())->method('isAdmin')->will($this->returnValue(false));
        $this->assertEquals($dirs, $utilsView->getTemplateDirs());
    }

    public function testGetEditionTemplateDirsForAdmin()
    {
        if ($this->getTestConfig()->getShopEdition() !== 'PE') {
            $this->markTestSkipped('This test is for Professional edition only.');
        }

        $config = $this->getConfig();
        $shopPath = rtrim($config->getConfigParam('sShopDir'), '/') . '/';

        $professionalPathProvider = new EditionPathProvider(new EditionRootPathProvider(new EditionSelector(EditionSelector::PROFESSIONAL)));
        $dirs = array(
            $professionalPathProvider->getViewsDirectory() . 'admin/tpl/',
            $shopPath . 'Application/views/admin/tpl/',
        );

        $utilsView = $this->getMock('oxUtilsView', array('isAdmin'));
        $utilsView->expects($this->any())->method('isAdmin')->will($this->returnValue(true));
        $this->assertEquals($dirs, $utilsView->getTemplateDirs());
    }

    /**
     * oxUtilsView::setTemplateDir() test case
     *
     * @return null
     */
    public function testSetTemplateDir()
    {
        if ($this->getTestConfig()->getShopEdition() != 'PE') {
            $this->markTestSkipped('This test is for Professional edition only.');
        }

        $config = $this->getConfig();
        $shopPath = rtrim($config->getConfigParam('sShopDir'), '/') . '/';

        $professionalPathProvider = new EditionPathProvider(new EditionRootPathProvider(new EditionSelector(EditionSelector::PROFESSIONAL)));
        $dirs = array(
            'testDir1',
            'testDir2',
            $professionalPathProvider->getViewsDirectory() . 'azure/tpl/',
            $shopPath . 'Application/views/azure/tpl/',
            $shopPath . 'out/azure/tpl/',
        );

        //
        $utilsView = $this->getMock("oxUtilsView", array("isAdmin"));
        $utilsView->expects($this->any())->method('isAdmin')->will($this->returnValue(false));
        $utilsView->setTemplateDir("testDir1");
        $utilsView->setTemplateDir("testDir2");
        $utilsView->setTemplateDir("testDir1");

        $this->assertEquals($dirs, $utilsView->getTemplateDirs());
    }

    /**
     * Testing smarty config data setter
     */
    // demo mode
    public function testFillCommonSmartyPropertiesANDSmartyCompileCheckDemoShop()
    {
        if ($this->getTestConfig()->getShopEdition() != 'PE') {
            $this->markTestSkipped('This test is for Professional edition only.');
        }

        $config = oxNew('oxConfig');

        $config->setConfigParam('iDebug', 1);
        $config->setConfigParam('blDemoShop', 1);

        $shopPath = rtrim($config->getConfigParam('sShopDir'), '/') . '/';
        $professionalPathProvider = new EditionPathProvider(new EditionRootPathProvider(new EditionSelector(EditionSelector::PROFESSIONAL)));
        $templatesDirs = array(
            $professionalPathProvider->getViewsDirectory() . 'azure/tpl/',
            $shopPath . 'Application/views/azure/tpl/',
            $shopPath . 'out/azure/tpl/',
        );

        $vfsStreamWrapper = $this->getVfsStreamWrapper();
        $vfsStreamWrapper->createStructure(array('tmp_directory' => array()));
        $compileDirectory = $vfsStreamWrapper->getRootPath().'tmp_directory';
        $config->setConfigParam('sCompileDir', $compileDirectory);


        $smarty = $this->getMock('\Smarty', array('register_resource', 'register_prefilter'));
        $smarty->expects($this->once())->method('register_resource')
            ->with(
                $this->equalTo('ox'),
                $this->equalTo(
                    array(
                        'ox_get_template',
                        'ox_get_timestamp',
                        'ox_get_secure',
                        'ox_get_trusted',
                    )
                )
            );
        $smarty->expects($this->once())->method('register_prefilter')
            ->with($this->equalTo('smarty_prefilter_oxblock'));

        $utilsView = oxNew('oxUtilsView');
        $utilsView->setConfig($config);
        $utilsView->UNITfillCommonSmartyProperties($smarty);
        $utilsView->UNITsmartyCompileCheck($smarty);

        $check = array('php_handling'      => 2,
            'security'          => true,
            'php_handling'      => SMARTY_PHP_REMOVE,
            'left_delimiter'    => '[{',
            'right_delimiter'   => '}]',
            'caching'           => false,
            'compile_dir'       => $compileDirectory . "/smarty/",
            'cache_dir'         => $compileDirectory . "/smarty/",
            'template_dir'      => $templatesDirs,
            'debugging'         => true,
            'compile_check'     => true,
            'security_settings' => array(
                'PHP_HANDLING'        => false,
                'IF_FUNCS'            =>
                    array(
                        0  => 'array',
                        1  => 'list',
                        2  => 'isset',
                        3  => 'empty',
                        4  => 'count',
                        5  => 'sizeof',
                        6  => 'in_array',
                        7  => 'is_array',
                        8  => 'true',
                        9  => 'false',
                        10 => 'null',
                        11 => 'XML_ELEMENT_NODE',
                        12 => 'is_int',
                    ),
                'INCLUDE_ANY'         => false,
                'PHP_TAGS'            => false,
                'MODIFIER_FUNCS'      =>
                    array(
                        0 => 'count',
                        1 => 'round',
                        2 => 'floor',
                        3 => 'trim',
                        4 => 'implode',
                        5 => 'is_array',
                        6 => 'getimagesize',
                    ),
                'ALLOW_CONSTANTS'     => true,
                'ALLOW_SUPER_GLOBALS' => true,
            )
        );

        foreach ($check as $varName => $varValue) {
            $this->assertTrue(isset($smarty->$varName));
            $this->assertEquals($varValue, $smarty->$varName, $varName);
        }
    }

    // non demo mode
    public function testFillCommonSmartyPropertiesANDSmartyCompileCheck()
    {
        if ($this->getTestConfig()->getShopEdition() != 'PE') {
            $this->markTestSkipped('This test is for Professional edition only.');
        }

        $config = oxNew('oxConfig');

        $config->setConfigParam('iDebug', 1);
        $config->setConfigParam('blDemoShop', 0);

        $shopPath = rtrim($config->getConfigParam('sShopDir'), '/') . '/';
        $professionalPathProvider = new EditionPathProvider(new EditionRootPathProvider(new EditionSelector(EditionSelector::PROFESSIONAL)));
        $templatesDirs = array(
            $professionalPathProvider->getViewsDirectory() . 'azure/tpl/',
            $shopPath . 'Application/views/azure/tpl/',
            $shopPath . 'out/azure/tpl/',
        );

        $vfsStreamWrapper = $this->getVfsStreamWrapper();
        $vfsStreamWrapper->createStructure(array('tmp_directory' => array()));
        $compileDirectory = $vfsStreamWrapper->getRootPath().'tmp_directory';
        $config->setConfigParam('sCompileDir', $compileDirectory);

        $professionalPathProvider = new EditionPathProvider(new EditionRootPathProvider(new EditionSelector(EditionSelector::PROFESSIONAL)));
        $plugins = array(
            $professionalPathProvider->getSmartyPluginsDirectory(),
            $this->getConfigParam('sCoreDir') . 'Smarty/Plugin',
            'plugins'
        );

        $check = array(
            'security'        => false,
            'php_handling'    => (int) $config->getConfigParam('iSmartyPhpHandling'),
            'left_delimiter'  => '[{',
            'right_delimiter' => '}]',
            'caching'         => false,
            'compile_dir'     => $compileDirectory . "/smarty/",
            'cache_dir'       => $compileDirectory . "/smarty/",
            'template_dir'    => $templatesDirs,
            'debugging'       => true,
            'compile_check'   => true,
            'plugins_dir'     => $plugins
        );

        $smarty = $this->getMock('\Smarty', array('register_resource'));
        $smarty->expects($this->once())->method('register_resource');

        $utilsView = oxNew('oxUtilsView');
        $utilsView->setConfig($config);
        $utilsView->UNITfillCommonSmartyProperties($smarty);
        $utilsView->UNITsmartyCompileCheck($smarty);

        foreach ($check as $varName => $varValue) {
            $this->assertTrue(isset($smarty->$varName));
            $this->assertEquals($varValue, $smarty->$varName, $varName);
        }
    }
}
