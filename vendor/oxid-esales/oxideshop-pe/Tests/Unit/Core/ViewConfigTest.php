<?php
/**
 * This Software is the property of OXID eSales and is protected
 * by copyright law - it is NOT Freeware.
 *
 * Any unauthorized use of this software without a valid license key
 * is a violation of the license agreement and will be prosecuted by
 * civil and criminal law.
 *
 * @link      http://www.oxid-esales.com
 * @copyright (C) OXID eSales AG 2003-2016
 * @version   OXID eShop PE
 */

namespace OxidEsales\EshopProfessional\Tests\Unit\Core;

//use OxidEsales\EshopProfessional\Core\Utils;

class ViewConfigTest extends \oxUnitTestCase
{
    /**
     * Checks if shop licenze is in staging mode
     */
    public function testHasDemoKey()
    {
        $oConfig = $this->getMock("oxConfig", array("hasDemoKey"));
        $oConfig->expects($this->once())->method("hasDemoKey")->will($this->returnValue(true));

        $oViewConfig = $this->getMock('oxViewConfig', array('getConfig'));
        $oViewConfig->expects($this->any())->method('getConfig')->will($this->returnValue($oConfig));

        $this->assertTrue($oViewConfig->hasDemoKey());
    }
}
