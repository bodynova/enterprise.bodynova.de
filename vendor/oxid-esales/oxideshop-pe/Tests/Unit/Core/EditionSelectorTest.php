<?php
/**
 * This Software is the property of OXID eSales and is protected
 * by copyright law - it is NOT Freeware.
 *
 * Any unauthorized use of this software without a valid license key
 * is a violation of the license agreement and will be prosecuted by
 * civil and criminal law.
 *
 * @link      http://www.oxid-esales.com
 * @copyright (C) OXID eSales AG 2003-2016
 * @version   OXID eShop PE
 */

namespace OxidEsales\EshopProfessional\Tests\Unit\Core;

use OxidEsales\Eshop\Core\Edition\EditionSelector;

class EditionSelectorTest extends \oxUnitTestCase
{
    public function testCheckActiveEdition()
    {
        if ($this->getTestConfig()->getShopEdition() !== 'PE') {
            $this->markTestSkipped('This test is for Professional editions only.');
        }

        $editionSelector = new EditionSelector();

        $this->assertSame('PE', $editionSelector->getEdition());
        $this->assertTrue($editionSelector->isProfessional());
        $this->assertFalse($editionSelector->isCommunity());
        $this->assertFalse($editionSelector->isEnterprise());
    }
}
