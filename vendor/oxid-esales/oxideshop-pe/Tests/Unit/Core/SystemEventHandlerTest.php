<?php
/**
 * This Software is the property of OXID eSales and is protected
 * by copyright law - it is NOT Freeware.
 *
 * Any unauthorized use of this software without a valid license key
 * is a violation of the license agreement and will be prosecuted by
 * civil and criminal law.
 *
 * @link      http://www.oxid-esales.com
 * @copyright (C) OXID eSales AG 2003-2016
 * @version   OXID eShop PE
 */

namespace OxidEsales\EshopProfessional\Tests\Unit\Core;

require_once TEST_LIBRARY_HELPERS_PATH . 'oxUtilsHelper.php';

use OxidEsales\Eshop\Core\Registry;
use OxidEsales\EshopProfessional\Core\SystemEventHandler;
use \oxRegistry;
use \oxOnlineLicenseCheck;
use \oxUtilsHelper;
use \oxUtilsDate;

/**
 * @covers SystemEventHandler
 */
class SystemEventHandlerTest extends \oxUnitTestCase
{
    /**
     * @return null|void
     */
    public function setUp()
    {
        parent::setUp();
        $this->getConfig()->saveShopConfVar('str', 'sOnlineLicenseNextCheckTime', null);
        $this->getConfig()->saveShopConfVar('str', 'sOnlineLicenseCheckTime', null);
        $this->getConfig()->setConfigParam('blLoadDynContents', true);
    }

    public function testShopInformationSendingWhenSendingIsNotAllowedInEnterpriseEdition()
    {
        $this->_prepareCurrentTime(1400000000);
        $this->getConfig()->setConfigParam('blLoadDynContents', false);

        $oSystemEventHandler = new SystemEventHandler();

        $oOnlineLicenseCheck = $this->getMock("oxOnlineLicenseCheck", array(), array(), '', false);
        $oOnlineLicenseCheck->expects($this->once())->method("validateShopSerials");

        /** @var oxOnlineLicenseCheck $oOnlineLicenseCheck */
        $oSystemEventHandler->setOnlineLicenseCheck($oOnlineLicenseCheck);

        $oSystemEventHandler->onShopStart();
    }

    public function testAppInitUnlicensed()
    {
        oxUtilsHelper::$sRedirectUrl = null;

        oxAddClassModule("oxUtilsHelper", "oxutils");
        $this->setConfigParam('redirected', 1);

        $oSerial = $this->getMock('oxserial');
        $oSerial->expects($this->any())->method('validateShop')->will($this->returnValue(false));
        /** @var oxSerial $oSerial */

        $oConfig = $this->getMock('oxconfig');
        $oConfig->expects($this->once())->method('getSerial')->will($this->returnValue($oSerial));

        $oSystemEventHandler = $this->getMock('\OxidEsales\EshopProfessional\Core\SystemEventHandler', array("_getConfig", '_sendShopInformation'));
        $oSystemEventHandler->expects($this->any())->method('_getConfig')->will($this->returnValue($oConfig));
        $oSystemEventHandler->expects($this->any())->method('_sendShopInformation');

        $oUtils = $this->getMock('oxUtils', array('showOfflinePage'));
        $oUtils->expects($this->once())->method('showOfflinePage');
        Registry::set('oxUtils', $oUtils);

        /** @var SystemEventHandler $oSystemEventHandler */
        $oSystemEventHandler->onShopStart();
    }

    public function testAppInitLicensed()
    {
        oxUtilsHelper::$sRedirectUrl = null;

        oxAddClassModule("oxUtilsHelper", "oxutils");
        $this->setConfigParam('redirected', 1);

        $oSerial = $this->getMock('oxserial');
        $oSerial->expects($this->any())->method('validateShop')->will($this->returnValue(true));
        /** @var oxSerial $oSerial */

        $oConfig = $this->getMock('oxconfig');
        $oConfig->expects($this->once())->method('getSerial')->will($this->returnValue($oSerial));

        $oSystemEventHandler = $this->getMock('\OxidEsales\EshopProfessional\Core\SystemEventHandler', array("_getConfig", '_sendShopInformation'));
        $oSystemEventHandler->expects($this->any())->method('_getConfig')->will($this->returnValue($oConfig));
        $oSystemEventHandler->expects($this->any())->method('_sendShopInformation');
        /** @var SystemEventHandler $oSystemEventHandler */
        $oSystemEventHandler->onShopStart();

        $this->assertNull(oxUtilsHelper::$sRedirectUrl);
    }

    /**
     * @param int $iCurrentTime
     */
    private function _prepareCurrentTime($iCurrentTime)
    {
        $oUtilsDate = $this->getMock('oxUtilsDate', array('getTime'));
        $oUtilsDate->expects($this->any())->method('getTime')->will($this->returnValue($iCurrentTime));
        /** @var oxUtilsDate $oUtils */
        oxRegistry::set('oxUtilsDate', $oUtilsDate);
    }
}
