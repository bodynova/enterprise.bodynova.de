<?php
/**
 * This Software is the property of OXID eSales and is protected
 * by copyright law - it is NOT Freeware.
 *
 * Any unauthorized use of this software without a valid license key
 * is a violation of the license agreement and will be prosecuted by
 * civil and criminal law.
 *
 * @link      http://www.oxid-esales.com
 * @copyright (C) OXID eSales AG 2003-2016
 * @version   OXID eShop PE
 */

namespace OxidEsales\EshopProfessional\Tests\Unit\Application\Controller\Admin;


class LoginTest extends \OxidEsales\TestingLibrary\UnitTestCase
{
    public function testGetShopValidationMessage_NoNoticeWhenGraceNotStartedAndSerialValid()
    {
        $oSerial = $this->getMock('oxSerial', array('isGracePeriodStarted', 'isShopValid'));
        $oSerial->expects($this->any())->method('isGracePeriodStarted')->will($this->returnValue(false));
        $oSerial->expects($this->any())->method('isShopValid')->will($this->returnValue(true));

        $oConfig = $this->getMock('oxConfig', array('getSerial'));
        $oConfig->expects($this->any())->method('getSerial')->will($this->returnValue($oSerial));

        $oView = oxNew('Login');
        $oView->setConfig($oConfig);

        $this->assertEquals('', $oView->getShopValidationMessage());
    }

    public function testGetShopValidationMessage_NoNoticeWhenGraceStartedAndSerialValid()
    {
        $oSerial = $this->getMock('oxSerial', array('isGracePeriodStarted', 'isShopValid'));
        $oSerial->expects($this->any())->method('isGracePeriodStarted')->will($this->returnValue(true));
        $oSerial->expects($this->any())->method('isShopValid')->will($this->returnValue(true));

        $oConfig = $this->getMock('oxConfig', array('getSerial'));
        $oConfig->expects($this->any())->method('getSerial')->will($this->returnValue($oSerial));

        $oView = oxNew('Login');
        $oView->setConfig($oConfig);

        $this->assertEquals('', $oView->getShopValidationMessage());
    }

    public function testGetShopValidationMessage_NoticeWhenGraceStartedSerialInvalid()
    {
        $oSerial = $this->getMock('oxSerial', array('isGracePeriodStarted', 'isGracePeriodExpired', 'isShopValid', 'getValidationMessage'));
        $oSerial->expects($this->any())->method('isGracePeriodStarted')->will($this->returnValue(true));
        $oSerial->expects($this->any())->method('isGracePeriodExpired')->will($this->returnValue(false));
        $oSerial->expects($this->any())->method('isShopValid')->will($this->returnValue(false));
        $oSerial->expects($this->any())->method('getValidationMessage')->will($this->returnValue('validation_message'));

        $oConfig = $this->getMock('oxConfig', array('getSerial'));
        $oConfig->expects($this->any())->method('getSerial')->will($this->returnValue($oSerial));
        $this->getConfig()->setConfigParam('blShopStopped', false);

        $oView = oxNew('Login');
        $oView->setConfig($oConfig);

        $this->assertEquals('validation_message', $oView->getShopValidationMessage());
    }

    public function testIsGracePeriodExpired_NotExpired()
    {
        $oSerial = $this->getMock('oxSerial', array('isGracePeriodExpired'));
        $oSerial->expects($this->any())->method('isGracePeriodExpired')->will($this->returnValue(true));

        $oConfig = $this->getMock('oxConfig', array('getSerial'));
        $oConfig->expects($this->any())->method('getSerial')->will($this->returnValue($oSerial));

        $oView = oxNew('Login');
        $oView->setConfig($oConfig);

        $this->assertEquals(true, $oView->isGracePeriodExpired());
    }

    public function testIsGracePeriodExpired_Expired()
    {
        $oSerial = $this->getMock('oxSerial', array('isGracePeriodExpired'));
        $oSerial->expects($this->any())->method('isGracePeriodExpired')->will($this->returnValue(false));

        $oConfig = $this->getMock('oxConfig', array('getSerial'));
        $oConfig->expects($this->any())->method('getSerial')->will($this->returnValue($oSerial));

        $oView = oxNew('Login');
        $oView->setConfig($oConfig);

        $this->assertEquals(false, $oView->isGracePeriodExpired());
    }

    /**
     * When serial is correct, do nothing
     */
    public function testGetShopValidationMessage_GraceResetWhenGraceStartedAndShopValid()
    {
        $oConfig = $this->getMock('oxConfig', array('getSerial'));
        $oConfig->init();
        $oConfig->setConfigParam('sBackTag', time());

        $oSerial = $this->getMock('oxSerial', array('isShopValid', 'getConfig'));
        $oSerial->expects($this->any())->method('isShopValid')->will($this->returnValue(true));
        $oSerial->expects($this->any())->method('getConfig')->will($this->returnValue($oConfig));

        $oConfig->expects($this->any())->method('getSerial')->will($this->returnValue($oSerial));

        $oView = oxNew('Login');
        $oView->setConfig($oConfig);

        $oView->getShopValidationMessage();

        $this->assertEquals('', $oConfig->getConfigParam('sBackTag'));
    }

    /**
     * When serial is correct, do nothing
     */
    public function testGetShopValidationMessage_GraceNotChangedWhenGraceStartedAndShopNotValid()
    {
        $sTime = time();

        $oConfig = $this->getMock('oxConfig', array('getSerial'));
        $oConfig->init();
        $oConfig->setConfigParam('sBackTag', $sTime);

        $oSerial = $this->getMock('oxSerial', array('isShopValid', 'getConfig'));
        $oSerial->expects($this->any())->method('isShopValid')->will($this->returnValue(false));
        $oSerial->expects($this->any())->method('getConfig')->will($this->returnValue($oConfig));

        $oConfig->expects($this->any())->method('getSerial')->will($this->returnValue($oSerial));

        $oView = oxNew('Login');
        $oView->setConfig($oConfig);

        $oView->getShopValidationMessage();

        $this->assertEquals($sTime, $oConfig->getConfigParam('sBackTag'));
    }
}
