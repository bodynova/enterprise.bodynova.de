<?php
/**
 * This Software is the property of OXID eSales and is protected
 * by copyright law - it is NOT Freeware.
 *
 * Any unauthorized use of this software without a valid license key
 * is a violation of the license agreement and will be prosecuted by
 * civil and criminal law.
 *
 * @link      http://www.oxid-esales.com
 * @copyright (C) OXID eSales AG 2003-2016
 * @version   OXID eShop PE
 */

namespace OxidEsales\EshopProfessional\Setup\En;

use OxidEsales\EshopProfessional\Setup\Messages;

/**
 * @inheritdoc
 */
class MessagesData extends Messages
{
    /**
     * @inheritdoc
     */
    public function getMessages()
    {
        return array(
            'STEP_0_ERROR_URL' => 'http://www.oxid-esales.com/en/products/professional-edition/system-requirements',
        );
    }
}
