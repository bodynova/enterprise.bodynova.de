<?php
/**
 * This Software is the property of OXID eSales and is protected
 * by copyright law - it is NOT Freeware.
 *
 * Any unauthorized use of this software without a valid license key
 * is a violation of the license agreement and will be prosecuted by
 * civil and criminal law.
 *
 * @link      http://www.oxid-esales.com
 * @copyright (C) OXID eSales AG 2003-2016
 * @version   OXID eShop PE
 */

namespace OxidEsales\EshopProfessional\Setup;

use Exception;

/** Setup controller class. */
class Controller extends \OxidEsales\EshopCommunity\Setup\Controller
{
    /**
     * Renders serial input fields
     *
     * @return string
     */
    public function serial()
    {
        $view = $this->getView();
        $view->setTitle('STEP_5_TITLE');
        $view->setViewParam('sLicense', $this->getInstance('Setup')->getDefaultSerial());

        return 'serial.php';
    }

    /**
     * Saves serial key
     *
     * @return string
     */
    public function serialSave()
    {
        try {
            $view = $this->getView();
            $licence = (string) $this->getInstance('Utilities')->getRequestVar('sLicence', 'post');
            $view->setMessage($this->getInstance('Setup')->setSerial(trim($licence)));
        } catch (Exception $exception) {
            $view->setMessage($exception->getMessage());
        }

        $view->setTitle('STEP_5_1_TITLE');

        return 'default.php';
    }

    /**
     * @inheritdoc
     */
    protected function onDirsWriteSetStep($setup)
    {
        $setup->setNextStep($setup->getStep('STEP_SERIAL'));
    }
}
