OXID eShop PE metapackage
=========================

This repository contains OXID eShop PE dependencies.

Information how to setup Shop:

  - https://github.com/OXID-eSales/developer_documentation/blob/master-install_eshop_via_composer/getting_started/eshop_installation.rst#steps-for-installing-oxid-eshop-compilation-via-composer
  - https://github.com/OXID-eSales/developer_documentation/blob/master/getting_started/resources/pe/composer.json
