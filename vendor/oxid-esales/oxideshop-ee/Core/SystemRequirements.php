<?php
/**
 * This Software is the property of OXID eSales and is protected
 * by copyright law - it is NOT Freeware.
 *
 * Any unauthorized use of this software without a valid license key
 * is a violation of the license agreement and will be prosecuted by
 * civil and criminal law.
 *
 * @link      http://www.oxid-esales.com
 * @copyright (C) OXID eSales AG 2003-2016
 * @version   OXID eShop EE
 */

namespace OxidEsales\EshopEnterprise\Core;

use oxDb;

/**
 * @inheritdoc
 */
class SystemRequirements extends \OxidEsales\EshopProfessional\Core\SystemRequirements
{
    /**
     * Checks if ZEND Platform Version 3.5 or Zend Server with Data Cache is installed
     *
     * @deprecated since 6.0 (2015-09-17); Zend Guard or Server is not required any more.
     *
     * @return integer
     */
    public function checkZendPlatformOrServer()
    {
        if (function_exists('output_cache_get')) {
            return 2;
        }
        if (function_exists('zend_disk_cache_fetch')) {
            return 2;
        }
        if (function_exists('zend_shm_cache_fetch')) {
            return 2;
        }

        return 1;
    }

    /**
     * Return minimum memory limit by edition.
     *
     * @return string
     */
    protected function _getMinimumMemoryLimit()
    {
        $requiredMinimalMemory = '32M';

        return $requiredMinimalMemory;
    }

    /**
     * Return recommend memory limit by edition.
     *
     * @return string
     */
    protected function _getRecommendMemoryLimit()
    {
        $recommendedMemory = '60M';

        return $recommendedMemory;
    }

    /**
     * @inheritdoc
     *
     * @deprecated since 6.0 (2015-09-17); Zend Guard or Server is not required any more.     *
     */
    public function getRequiredModules()
    {
        return parent::getRequiredModules();
    }


    /**
     * @inheritdoc
     */
    public function checkMysqlVersion($sVersion = null)
    {
        $iModStat = parent::checkMysqlVersion($sVersion);

        if ($sVersion === null) {
            $aRez = oxDb::getDb()->getAll("SHOW VARIABLES LIKE 'version'");
            foreach ($aRez as $aRecord) {
                $sVersion = $aRecord[1];
                break;
            }
        }

        /**
         * There is a bug in MySQL 5.6,* which under certain conditions affects OXID eShop Enterprise Edition.
         * See https://bugs.mysql.com/bug.php?id=79203
         */
        if (version_compare($sVersion, '5.5.999999', '>') && version_compare($sVersion, '5.7.0', '<')) {
            $iModStat = 0;
        }

        return $iModStat;
    }
}
