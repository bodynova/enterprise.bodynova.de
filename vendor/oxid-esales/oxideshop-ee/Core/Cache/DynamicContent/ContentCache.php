<?php
/**
 * This Software is the property of OXID eSales and is protected
 * by copyright law - it is NOT Freeware.
 *
 * Any unauthorized use of this software without a valid license key
 * is a violation of the license agreement and will be prosecuted by
 * civil and criminal law.
 *
 * @link      http://www.oxid-esales.com
 * @copyright (C) OXID eSales AG 2003-2016
 * @version   OXID eShop EE
 */
namespace OxidEsales\EshopEnterprise\Core\Cache\DynamicContent;

use \oxRegistry;
use \oxDb;
use \oxSystemComponentException;
use OxidEsales\Eshop\Application\Model\Contract\CacheBackendInterface;

/**
 * Partial content caching class
 */
class ContentCache extends \OxidEsales\EshopProfessional\Core\Base
{
    /** @var array All available backends, sorted by priority. */
    protected $_aAllBackends = null;

    /** @var array */
    protected $_aSystemBackends = array(
        'ZS_SHM'  => '\OxidEsales\EshopEnterprise\Core\Cache\DynamicContent\Connector\ZendShmCacheConnector',
        'ZS_DISK' => '\OxidEsales\EshopEnterprise\Core\Cache\DynamicContent\Connector\ZendDiskCacheConnector',
        'OXID'    => '\OxidEsales\EshopEnterprise\Core\Cache\DynamicContent\Connector\DefaultCacheConnector',
    );

    /**
     * Cache life time in seconds
     *
     * @var int
     */
    protected $_iCacheLifetime = null;

    /**
     * Cacheable classes names array
     *
     * @var array
     */
    protected $_aCachableClasses = null;

    /**
     * Cache backend
     *
     * @var CacheBackendInterface
     */
    protected $_oBackend = null;

    /**
     * placeholder for session id in cleared data
     */
    const SESSION_ID_PLACEHOLDER = '[__SESSION_ID_PLACEHOLDER__]';

    /**
     * placeholder for session stoken in cleared data
     */
    const SESSION_STOKEN_PLACEHOLDER = '[__SESSION_STOKEN_PLACEHOLDER__]';

    /**
     * placeholder for force_sid=SID in urls
     */
    const SESSION_FULL_ID_PLACEHOLDER = '[__SESSION_FULL_ID_PLACEHOLDER__]';

    /**
     * placeholder for force_sid=SID in urls, add an (&) before original placeholder
     */
    const SESSION_FULL_ID_AMP_PLACEHOLDER = '[__SESSION_FULL_ID_AMP_PLACEHOLDER__]';

    /**
     * placeholder for force_sid=SID in urls, add an (?) before original placeholder
     */
    const SESSION_FULL_ID_QUE_PLACEHOLDER = '[__SESSION_FULL_ID_QUE_PLACEHOLDER__]';

    /**
     * fetch backend object
     *
     * @return CacheBackendInterface
     */
    protected function _getBackend()
    {
        if ($this->_oBackend) {
            return $this->_oBackend;
        }
        $sBackend = $this->getSelectedBackend();
        $aBackends = $this->_getAllBackends();

        $this->_oBackend = oxNew($aBackends[$sBackend]);
        $this->_oBackend->cacheSetTTL($this->getCacheLifeTime());

        return $this->_oBackend;
    }

    /**
     * retrieve all installed backends
     *
     * @return array
     */
    protected function _getAllBackends()
    {
        if (isset($this->_aAllBackends)) {
            return $this->_aAllBackends;
        }
        $aUserBackends = $this->getConfig()->getConfigParam('aUserCacheBackends');
        if (is_array($aUserBackends) && count($aUserBackends)) {
            $this->_aAllBackends = array_merge($aUserBackends, $this->_aSystemBackends);
        } else {
            $this->_aAllBackends = $this->_aSystemBackends;
        }

        return $this->_aAllBackends;
    }

    /**
     * find the selected backend for usage in shop
     *
     * @return string
     */
    public function getSelectedBackend()
    {
        $sBackend = $this->getConfig()->getConfigParam('sCacheBackend');
        if ($sBackend && $this->isBackendAvailable($sBackend)) {
            return $sBackend;
        }

        return $this->_getFirstSuitedBackend();
    }

    /**
     * find first suited backend and return its id
     *
     * @throws oxSystemComponentException
     *
     * @return string
     */
    protected function _getFirstSuitedBackend()
    {
        foreach (array_keys($this->_getAllBackends()) as $sBackend) {
            if ($this->isBackendAvailable($sBackend)) {
                return $sBackend;
            }
        }

        $e = oxNew("oxSystemComponentException");
        $e->setComponent('oxCache');
        throw $e;
    }

    /**
     * check if backend is available
     *
     * @param string $sBackend backend id
     *
     * @return bool
     */
    public function isBackendAvailable($sBackend)
    {
        $aBackends = $this->_getAllBackends();
        $sClass = $aBackends[$sBackend];
        if ($sClass && class_exists($sClass) && in_array('OxidEsales\EshopEnterprise\Application\Model\Contract\CacheBackendInterface', class_implements($sClass))) {
            return call_user_func(array($sClass, 'isAvailable'));
        }

        return false;
    }

    /**
     * return array of available backend ids
     *
     * @return array
     */
    public function getAvailableBackends()
    {
        $aRet = array();
        foreach (array_keys($this->_getAllBackends()) as $sBackend) {
            if ($this->isBackendAvailable($sBackend)) {
                $aRet[] = $sBackend;
            }
        }

        return $aRet;
    }

    /**
     * Cache lifetime setter
     *
     * @param int $iCacheLifeTime cache lifetime
     */
    public function setCacheLifetime($iCacheLifeTime = 360)
    {
        if (isset($iCacheLifeTime)) {
            $this->_iCacheLifetime = $iCacheLifeTime;
        }
    }

    /**
     * Returns cache lifetime value
     *
     * @return int
     */
    public function getCacheLifeTime()
    {
        if (!isset($this->_iCacheLifetime)) {
            $this->setCacheLifetime(oxRegistry::getConfig()->getConfigParam('iCacheLifeTime'));
        }

        return $this->_iCacheLifetime;
    }

    /**
     * Cacheable classes setter
     *
     * @param array $aCachableClasses cacheable classes
     */
    public function setCachableClasses($aCachableClasses)
    {
        $this->_aCachableClasses = $aCachableClasses;
    }

    /**
     * Cacheable classes setter
     *
     * @return null
     */
    public function getCachableClasses()
    {
        if (!isset($this->_iCacheLifetime)) {
            $this->setCachableClasses(oxRegistry::getConfig()->getConfigParam('aCachableClasses'));
        }

        return $this->_aCachableClasses;
    }

    /**
     * Returns, whether a given class is cachable
     *
     * @param string $sViewName a class name
     *
     * @return bool
     */
    public function isViewCacheable($sViewName)
    {
        $blCachable = false;
        $aClasses = $this->getCachableClasses();
        if (is_array($aClasses)) {
            $blCachable = in_array($sViewName, $aClasses);
        }

        return $blCachable;
    }

    /**
     * Adds an ID and a content to the cache.
     *
     * @param string $sCacheId a cache ID
     * @param string $sContent a content
     * @param string $sResetOn a reset (date?)
     */
    public function put($sCacheId, $sContent, $sResetOn = '')
    {
        $sId = md5($sCacheId);
        $sContent = $this->_cleanSensitiveData($sContent);

        $oCacheBackend = $this->_getBackend();
        $oCacheBackend->cacheRemoveKey($sId);
        $oCacheBackend->cachePut($sId, $sContent);

        $this->_addId($sCacheId, getStr()->strlen($sContent), $sResetOn);
    }

    /**
     * Retrieves a cache entry by it's id.
     *
     * @param string $sCacheId an id for the information to be retrieved
     *
     * @return mixed
     */
    public function get($sCacheId)
    {
        $sContent = false;
        $sId = $this->getCacheId($sCacheId);

        if ($sId && ($sContent = $this->_getBackend()->cacheGet($sId))) {
            $this->_addHit($sCacheId);
        }

        return $sContent;
    }

    /**
     * Returns cache id if it exists and is not expired and zend caching is on.
     *
     * @param string $sCacheId caching contents id
     *
     * @return string
     */
    public function getCacheId($sCacheId)
    {
        $sId = md5($sCacheId);

        // We force reading from master to prevent issues with slow replications or open transactions (see ESDEV-3804).
        return oxDb::getMaster()->getOne("select oxid from `oxcache` where oxid='$sId' and oxexpire > " . time());
    }

    /**
     * Removes a cache entry by a given id.
     *
     * @param string $sCacheId a id to be removed from cache
     */
    public function remove($sCacheId)
    {
        $sId = md5($sCacheId);
        $this->_getBackend()->cacheRemoveKey($sId);
        $this->_removeId($sCacheId);
    }

    /**
     * Adds an object to cachem by serialising it.
     *
     * @param string $sCacheId an identifier
     * @param object $oObject  an object to be added to cache
     * @param string $sResetOn a reset (date?)
     */
    public function putObject($sCacheId, $oObject, $sResetOn = '')
    {
        $this->put($sCacheId, serialize($oObject), $sResetOn);
    }

    /**
     * Retrieves a cached object by it's id, unserialising the object
     *
     * @param string $sCacheId an id of the cached object
     *
     * @return mixed
     */
    public function getObject($sCacheId)
    {
        $oObject = $this->get($sCacheId);
        if ($oObject !== false) {
            $oObject = unserialize($oObject);
        }

        return $oObject;
    }

    /**
     * Deleted db and file cache content
     *
     * @param array $blResetFileCache reset file cache
     */
    public function reset($blResetFileCache = true)
    {
        if ($blResetFileCache) {
            oxRegistry::getUtils()->oxResetFileCache();
        }
        if ($this->isActive()) {
            oxDb::getDb()->execute("delete from oxcache");
            $this->_getBackend()->cacheClear();
        }
    }

    /**
     * returns true if the content cache was activated by configuration
     * it should be used to check if the admin has enabled that feature before performing
     * operation on the cache
     *
     * @return bool
     */
    public function isActive()
    {
        return (bool) oxRegistry::getConfig()->getConfigParam('blUseContentCaching');
    }


    /**
     * Resets cache according to special reset conditions passed by params
     *
     * @param array $aResetOn reset conditions array
     * @param bool  $blUseAnd reset precise level ( AND's conditions SQL )
     */
    public function resetOn($aResetOn, $blUseAnd = false)
    {
        $sResetConditions = '';
        $sSep = $blUseAnd ? ' and ' : ' or ';
        // We force reading from master to prevent issues with slow replications or open transactions (see ESDEV-3804).
        $masterDb = oxDb::getMaster();
        foreach ($aResetOn as $sKey => $sVal) {
            if ($sResetConditions) {
                $sResetConditions .= $sSep;
            }
            $sResetConditions .= " oxreseton like " . $masterDb->quote("%|$sVal=$sKey%") . " ";
        }

        $oCacheBackend = $this->_getBackend();

        $sQ = "select oxid from oxcache where oxexpire > " . time() . " and ( $sResetConditions ) ";

        // We force reading from master to prevent issues with slow replications or open transactions (see ESDEV-3804).
        $oRs = $masterDb->select($sQ, false);
        if ($oRs != false && $oRs->count() > 0) {
            while (!$oRs->EOF) {
                $oCacheBackend->cacheRemoveKey($oRs->fields[0]);
                $oRs->fetchRow();
            }

            $sQ = "delete from oxcache where oxexpire > " . time() . " and ( $sResetConditions ) ";
            $masterDb->execute($sQ);
        }
    }

    /**
     * Returns the total size of the cached entries in db
     *
     * @param bool $blExpired flag, whether expired entries are included
     *
     * @return int
     */
    public function getTotalCacheSize($blExpired = false)
    {
        $sShopID = $this->getConfig()->getShopId();
        $sSelect = "select sum(oxsize) from `oxcache` where oxexpire ";
        $sSelect .= ($blExpired) ? " <= " : " > ";
        $sSelect .= time() . " and oxshopid = '$sShopID'";

        // We force reading from master to prevent issues with slow replications or open transactions (see ESDEV-3804).
        return oxDb::getMaster()->getOne($sSelect);
    }

    /**
     * Returns the amount of cached entries in db
     *
     * @param bool $blExpired flag, whether expired entries are included
     *
     * @return int
     */
    public function getTotalCacheCount($blExpired = false)
    {
        $sShopID = $this->getConfig()->getShopId();
        $sSelect = "select count(oxid) from `oxcache` where oxexpire";
        $sSelect .= ($blExpired) ? " <= " : " > ";
        $sSelect .= time() . " and oxshopid = '$sShopID'";

        // We force reading from master to prevent issues with slow replications or open transactions (see ESDEV-3804).
        return oxDb::getMaster()->getOne($sSelect);
    }

    /**
     * Returns the total hit count of all entries in db
     *
     * @param bool $blExpired flag, whether expired entries are included
     *
     * @return int
     */
    public function getTotalCacheHits($blExpired = false)
    {
        $sShopID = $this->getConfig()->getShopId();

        $sSelect = "select sum(oxhits) from `oxcache` where oxexpire";
        $sSelect .= ($blExpired) ? " <= " : " > ";
        $sSelect .= time() . " and oxshopid = '$sShopID'";

        // We force reading from master to prevent issues with slow replications or open transactions (see ESDEV-3804).
        return oxDb::getMaster()->getOne($sSelect);
    }

    /**
     * Callback function which processes dynamic parts of html. If all needed data is
     * set and processed correctly new generated html code is returned. On case of
     * missing template to render - initial dada is returned
     *
     * @param array $aMathes regular expression matchers
     *
     * @return string
     */
    protected function _processDynContent($aMathes)
    {
        $oSmarty = oxRegistry::get("oxUtilsView")->getSmarty();

        $aDynParams = array();
        $aAllParts = explode(' ', trim($aMathes[1]));

        // processing dyn params
        foreach ($aAllParts as $sOnePart) {
            list ($sName, $sValue) = explode('=', $sOnePart);

            $sName = trim($sName);
            if ($sName) {
                $aDynParams[$sName] = base64_decode(trim($sValue));
            }
        }

        if (isset($aDynParams['type'])) {

            if (isset($aDynParams['aid']) && $aDynParams['aid']) {
                // Compare links
                if ($aDynParams['type'] == 'compare') {
                    $aCompareItems = oxRegistry::getSession()->getVariable('aFiltcompproducts');


                    $oSmarty->assign('_compare_aid', $aDynParams['aid']);

                    if (isset($aCompareItems[$aDynParams['aid']])) {
                        $oSmarty->assign('_compare_in_list', true);
                    } else {
                        $oSmarty->assign('_compare_in_list', false);
                    }
                    unset($aDynParams['aid']);
                    unset($aDynParams['in_list']);
                }
            }
        }

        //Assign auto parameters
        $sPrefix = '_';
        if (isset($aDynParams['type'])) {
            $sPrefix .= $aDynParams['type'] . '_';
        }

        foreach ($aDynParams as $sKey => $sVal) {
            if ($sKey != 'type' && $sKey != 'file') {
                $oSmarty->assign($sPrefix . $sKey, $sVal);
            }
        }

        if (isset($aDynParams['file']) && $aDynParams['file']) {
            $sRes = $oSmarty->fetch($aDynParams['file']);

            return $sRes;
        }

        // returning unchanged
        return $aMathes[0];
    }

    /**
     * Searches for URL's and adds force_sid placeholder where they're missing
     *
     * @param string $sContent conten to cache
     *
     * @return string
     */
    protected function _appendSidPlaceholder($sContent)
    {
        $aUrls = array();
        $aUpdatedUrls = array();

        $sShopUrl = $this->getConfig()->getShopUrl();
        $sShopSslUrl = $this->getConfig()->getSslShopUrl();

        if (!empty($sShopSslUrl) && $sShopUrl != $sShopSslUrl) {
            $sShopUrlPattern = "(" . preg_quote($sShopUrl, "@") . "|" . preg_quote($sShopSslUrl, "@") . ")";
        } else {
            $sShopUrlPattern = "(" . preg_quote($sShopUrl, "@") . ")";
        }

        // regexp pattern for urls in anchors
        $sHrefPattern = '@(<a.*href=")(' . $sShopUrlPattern . '[^"#]*)(#|")@i';

        // getting all urls
        if (preg_match_all($sHrefPattern, $sContent, $aUrls)) {

            foreach ($aUrls[2] as $sKey => $sUrl) {
                // removing force_sid=... if there is one
                $sUrl = oxRegistry::get("oxUtilsUrl")->cleanUrl($sUrl, array("force_sid"));

                // removing parameter separator from url end
                $sUrl = preg_replace("/(\?|&(amp;)?)$/", "", $sUrl);

                // adding force_sid placeholder to url end
                if (strstr($sUrl, "?")) {
                    // there are already some parameters - using placeholder with &amp;
                    $sUrl .= self::SESSION_FULL_ID_AMP_PLACEHOLDER;
                } else {
                    // no parameters - using placeholder with ?
                    $sUrl .= self::SESSION_FULL_ID_QUE_PLACEHOLDER;
                }

                // formating full updated html <a> tag
                $aUpdatedUrls[] = $aUrls[1][$sKey] . $sUrl . $aUrls[4][$sKey];
            }

            $sContent = str_replace($aUrls[0], $aUpdatedUrls, $sContent);

        }

        return $sContent;
    }

    /**
     * remove user sensitive data for caching content
     *
     * @param string $sContent content to cache
     *
     * @return string
     */
    protected function _cleanSensitiveData($sContent)
    {
        $sSid = $this->getSession()->getId();
        if ($sSid && $sSid != 'x') {
            // adding a force_sid placeholder (if needed)
            $sContent = $this->_appendSidPlaceholder($sContent);

            // searching for already defined sids and stokes and adding sid placeholders
            $aSearch = array();
            $aReplace = array();

            // searching for SID in other places (input values and so on..)
            $aSearch[] = '#(^|[^a-zA-Z0-9])' . preg_quote($sSid, '#') . '([^a-zA-Z0-9]|$)#';
            $aReplace[] = '\1' . self::SESSION_ID_PLACEHOLDER . '\2';

            if (($sToken = oxRegistry::getSession()->getVariable('sess_stoken'))) {
                $aSearch[] = '#(^|[^a-zA-Z0-9])' . preg_quote($sToken, '#') . '([^a-zA-Z0-9]|$)#';
                $aReplace[] = '\1' . self::SESSION_STOKEN_PLACEHOLDER . '\2';
            }

            return preg_replace($aSearch, $aReplace, $sContent);
        }

        return $sContent;
    }

    /**
     * Renders dynamic contents within cached contents.
     *
     * @param string $sOutput static contents
     *
     * @return string
     */
    public function processCache($sOutput)
    {
        $mySession = $this->getSession();

        $sSid = $mySession->getId();
        $sToken = '';
        $sForceSid = '';
        $sForceSidAmp = '';
        $sForceSidQue = '';
        if ($sSid != '') {
            $sToken = $mySession->getSessionChallengeToken();
        }
        if ($sSid && $mySession->isSidNeeded()) {
            $sForceSid = "force_sid=$sSid";
            $sForceSidAmp = "&amp;force_sid=$sSid";
            $sForceSidQue = "?force_sid=$sSid";
        }

        // replacing cleared data
        $sOutput = str_replace(
            array(
                 self::SESSION_ID_PLACEHOLDER,
                 self::SESSION_STOKEN_PLACEHOLDER,
                 self::SESSION_FULL_ID_PLACEHOLDER,
                 self::SESSION_FULL_ID_AMP_PLACEHOLDER,
                 self::SESSION_FULL_ID_QUE_PLACEHOLDER
            ),
            array(
                 $sSid,
                 $sToken,
                 $sForceSid,
                 $sForceSidAmp,
                 $sForceSidQue
            ),
            $sOutput
        );

        oxRegistry::get("oxUtilsView")->getSmarty()->assign('_render4cache', '0');

        return preg_replace_callback("/<oxid_dynamic>(.*)<\/oxid_dynamic>/mUs", array($this, '_processDynContent'), $sOutput);
    }

    /**
     * Adds an entry to the db oxcache table.
     *
     * @param string $sCacheId an id to be added to cache
     * @param int    $iSize    the length of content to be added
     * @param string $sResetOn a reset (date?)
     */
    protected function _addId($sCacheId, $iSize = 0, $sResetOn = '')
    {
        $sShopID = $this->getConfig()->getShopId();
        $sId = md5($sCacheId);
        $iOffset = $this->getCacheLifeTime();

        $oDb = oxDb::getDb();
        $oDb->execute("replace into `oxcache` ( `oxid`,`oxexpire`,`oxreseton`,`oxsize`, `oxhits`, `oxshopid` ) values ( '$sId', " . (time() + $iOffset) . ", " . $oDb->quote($sResetOn) . ", " . $oDb->quote($iSize) . ", '0', '$sShopID')");
    }

    /**
     * Increments the hit counter in the db entry,  when a certain id is retrieved from cache.
     * This method as inserting to the table on every call decreases caching efficiency.
     * However this functionality could be enabled using "blShowCacheHits" config option if needed.
     *
     * @param string $sCacheId an id of the cache entry, whose hit counter is to be incremented
     */
    protected function _addHit($sCacheId)
    {
        //if ($this->getConfig()->getParam("blShowCacheHits")) {
        if (!$this->getConfig()->getActiveShop()->oxshops__oxproductive->value) {
            $sShopID = $this->getConfig()->getShopId();
            $sId = md5($sCacheId);

            oxDb::getDb()->execute("update `oxcache` set `oxhits` = `oxhits`+1 where oxid='$sId' and oxshopid = '$sShopID'");
        }
    }

    /**
     * Removes an id from db oxcache table
     *
     * @param string $sCacheId identifier of the entry to be removed
     */
    protected function _removeId($sCacheId)
    {
        $sShopID = $this->getConfig()->getShopId();
        $sId = md5($sCacheId);
        oxDb::getDb()->execute("delete from `oxcache` where oxid='$sId' and oxshopid = '$sShopID'");
    }
}
