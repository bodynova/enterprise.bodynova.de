<?php
/**
 * This Software is the property of OXID eSales and is protected
 * by copyright law - it is NOT Freeware.
 *
 * Any unauthorized use of this software without a valid license key
 * is a violation of the license agreement and will be prosecuted by
 * civil and criminal law.
 *
 * @link      http://www.oxid-esales.com
 * @copyright (C) OXID eSales AG 2003-2016
 * @version   OXID eShop EE
 */
namespace OxidEsales\EshopEnterprise\Core\Cache\DynamicContent\Connector;

use OxidEsales\EshopEnterprise\Application\Model\Contract\CacheBackendInterface;

/**
 * Zend Server Data Cache disk storage cache backend
 */
class ZendDiskCacheConnector implements CacheBackendInterface
{
    /** @var int Time to live. */
    protected $_iTtl = 0;

    /**
     * Sets cache ttl in seconds.
     *
     * @param int $iTimeToLive cache timeout value in seconds
     *
     * @return null
     */
    public function cacheSetTTL($iTimeToLive)
    {
        $this->_iTtl = $iTimeToLive;
    }

    /**
     * Returns cache data. If data is not found - returns false
     *
     * @param string $sId cache id
     *
     * @return mixed
     */
    public function cacheGet($sId)
    {
        return zend_disk_cache_fetch($sId);
    }

    /**
     * Stores cache data, returns storing status
     *
     * @param string $sId      cache id
     * @param string $sContent cache data
     *
     * @return mixed
     */
    public function cachePut($sId, $sContent)
    {
        return zend_disk_cache_store($sId, $sContent, $this->_iTtl);
    }

    /**
     * Removes cache according to cache key, returns removal status
     *
     * @param string $sId cache key
     *
     * @return null
     */
    public function cacheRemoveKey($sId)
    {
        return zend_disk_cache_delete($sId);
    }

    /**
     * Removes all cache entries if possible by backend
     */
    public function cacheClear()
    {
        zend_disk_cache_clear();
    }

    /**
     * check if this backend is available
     *
     * @return bool
     */
    public static function isAvailable()
    {
        return function_exists('zend_disk_cache_fetch');
    }
}
