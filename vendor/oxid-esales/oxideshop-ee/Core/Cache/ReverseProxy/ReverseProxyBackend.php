<?php
/**
 * This Software is the property of OXID eSales and is protected
 * by copyright law - it is NOT Freeware.
 *
 * Any unauthorized use of this software without a valid license key
 * is a violation of the license agreement and will be prosecuted by
 * civil and criminal law.
 *
 * @link      http://www.oxid-esales.com
 * @copyright (C) OXID eSales AG 2003-2016
 * @version   OXID eShop EE
 */
namespace OxidEsales\EshopEnterprise\Core\Cache\ReverseProxy;

use OxidEsales\Eshop\Core\Cache\Generic\Connector\ReverseProxyCacheConnector;
use OxidEsales\Eshop\Core\Config;
use \oxRegistry;

/**
 *  Provides an interface to reverse proxy cache to invalidate cached urls.
 *  Sore url and action in pool. Execute action in one goal.
 */
class ReverseProxyBackend extends \OxidEsales\EshopProfessional\Core\Base
{
    /**
     * Cached urls pool with actions: url|action
     *
     * @var array
     */
    protected $_aUrlPool = array();

    /**
     * Reverse Proxy access object
     *
     * @var ReverseProxyCacheConnector
     */
    protected $_oConnector = null;

    /**
     * Reverse proxy status by configs and status
     *
     * @var bool
     */
    protected $_blIsActive = null;

    /**
     * Reverse proxy status by configs
     *
     * @var bool
     */
    protected $_blIsEnabled = null;

    /**
     * If we need flush all reverse proxy cache we set true, and do not execute action from pool
     *
     * @var bool
     */
    protected $_blFlush = false;

    /**
     * Reverse proxy capable du esi.
     *
     * @var bool
     */
    private $_blReverseProxyCapableDoEsi = null;

    /**
     * Reverse proxy environment key
     *
     * @var array
     */
    private $_aEnvKey = array();

    /** @var  ReverseProxyUrlPartsToFlush */
    private $urlPartsToFlush;

    /** @var  ReverseProxyUrlGenerator */
    private $urlGenerator;

    /**
     * Returns URL pool
     *
     * @return array $_aUrlPool
     */
    public function getUrlPool()
    {
        return $this->_aUrlPool;
    }

    /**
     * Return reverse proxy connector
     *
     * @return ReverseProxyCacheConnector $_oConnector
     */
    public function getConnector()
    {
        if ($this->_oConnector === null) {
            $oConfig = $this->_getConfig();
            $curl = oxNew('oxCurl');
            $oConnector = oxNew("oxReverseProxyConnector", $curl);
            $oConnector->setReverseProxyUrl($oConfig->getShopUrl());
            $this->setConnector($oConnector);
        }

        return $this->_oConnector;
    }

    /**
     * Setting reverse proxy connector
     *
     * @param ReverseProxyCacheConnector $oConnector - connector to reverse proxy
     */
    public function setConnector($oConnector)
    {
        $this->_oConnector = $oConnector;
    }

    /**
     * Sets object which forms list of hosts for flushing.
     *
     * @param ReverseProxyUrlPartsToFlush $urlPartsToFlush
     */
    public function setUrlPartsToFlush($urlPartsToFlush)
    {
        $this->urlPartsToFlush = $urlPartsToFlush;
    }

    /**
     * Returns object which forms list of hosts for flushing.
     *
     * @return ReverseProxyUrlPartsToFlush
     */
    public function getUrlPartsToFlush()
    {
        if ($this->urlPartsToFlush === null) {
            $urlPartsToFlush = oxNew('oxReverseProxyUrlPartsToFlush');
            $this->setUrlPartsToFlush($urlPartsToFlush);
        }

        return $this->urlPartsToFlush;
    }

    /**
     * Sets object which forms URL.
     *
     * @param ReverseProxyUrlGenerator $urlGenerator
     */
    public function setUrlGenerator($urlGenerator)
    {
        $this->urlGenerator = $urlGenerator;
    }

    /**
     * Returns object which forms URL.
     *
     * @return ReverseProxyUrlGenerator
     */
    public function getUrlGenerator()
    {
        if ($this->urlGenerator === null) {
            $urlGenerator = oxNew('oxReverseProxyUrlGenerator');
            $this->setUrlGenerator($urlGenerator);
        }

        return $this->urlGenerator;
    }

    /**
     * Adds url to url pool.
     *
     * @param mixed $mUrls - urls
     */
    public function set($mUrls)
    {
        $aUrlPool = $this->getUrlPool();

        if (!is_array($mUrls)) {
            $aUrlPool[] = $mUrls;
        } else {
            $aUrlPool = array_merge($aUrlPool, $mUrls);
        }

        $this->_aUrlPool = array_unique($aUrlPool);
    }


    /**
     * Set mark for invalidation all remote proxy contents
     */
    public function setFlush()
    {
        $this->_blFlush = true;
    }

    /**
     * Sets different shop host to connector
     *
     * @param string $sHost shop host
     */
    public function setShopHost($sHost)
    {
        $this->getConnector()->setShopHost($sHost);
    }

    /**
     * return true if all pages are prepare for flush, otherwise false
     *
     * @return bool
     */
    public function isFlushSet()
    {
        return $this->_blFlush;
    }

    /**
     * Invalidates URL collected to URLs pool
     */
    public function execute()
    {
        $connector = $this->getConnector();
        $urlPartsToFlush = $this->getUrlPartsToFlush();
        $urlGenerator = $this->getUrlGenerator();
        $connector->setShopHost($urlPartsToFlush->getHost());

        if ($this->isFlushSet()) {
            $sUrl = $urlGenerator->prependPathToUrlQuery(
                $urlPartsToFlush->getPath(),
                $urlGenerator::REGEXP_MATCH_EVERYTHING
            );
            $connector->invalidate($sUrl);
        } elseif (!empty($this->_aUrlPool)) {
            foreach ($this->_aUrlPool as $sUrl) {
                $sUrl = $urlGenerator->prependPathToUrlQuery($urlPartsToFlush->getPath(), $sUrl);
                $connector->invalidate($sUrl);
            }
            // clear url pool once it is flushed
            $this->_aUrlPool = array();
        }
    }

    /**
     * Return shop config from registry
     *
     * @return Config
     */
    protected function _getConfig()
    {
        return oxRegistry::getConfig();
    }

    /**
     * Set if Reverse Proxy capable do esi.
     *
     * @param bool $blReverseProxyCapableDoEsi if Reverse Proxy capable do esi.
     */
    public function setReverseProxyCapableDoEsi($blReverseProxyCapableDoEsi)
    {
        $this->_blReverseProxyCapableDoEsi = (bool) $blReverseProxyCapableDoEsi;
    }

    /**
     * Set if Reverse Proxy capable do esi.
     *
     * @param bool $blReverseProxyCapableDoEsi if Reverse Proxy capable do esi.
     *
     * @deprecated since 5.1.0 (2013-10-21); typo fix use setReverseProxyCapableDoEsi()
     */
    public function setReverseProxyCapabaleDoEsi($blReverseProxyCapableDoEsi)
    {
        $this->setReverseProxyCapableDoEsi($blReverseProxyCapableDoEsi);
    }

    /**
     * Get if Reverse Proxy capable do esi.
     *
     * @return bool
     */
    public function isReverseProxyCapableDoEsi()
    {
        if ($this->_blReverseProxyCapableDoEsi === null) {
            $this->_blReverseProxyCapableDoEsi = isset($_SERVER["HTTP_SURROGATE_CAPABILITY"])
                                                 && strpos($_SERVER["HTTP_SURROGATE_CAPABILITY"], 'varnish=ESI') !== false;
        }

        return $this->_blReverseProxyCapableDoEsi;
    }

    /**
     * Get if Reverse Proxy capable do esi.
     *
     * @deprecated since 5.1.0 (2013-10-21); typo fix use isReverseProxyCapableDoEsi()
     *
     * @return bool
     */
    public function isReverseProxyCapabaleDoEsi()
    {
        return $this->isReverseProxyCapableDoEsi();
    }

    /**
     * Force Reverse Proxy to be inactive.
     */
    public function setNotActive()
    {
        $this->_blIsActive = false;
    }

    /**
     * Return if Reverse Proxy is active: is enabled and capable to parse ESI tags.
     *
     * @return boolean
     */
    public function isActive()
    {
        if ($this->_blIsActive === null) {
            $blReverseProxyCapableDoEsi = $this->isReverseProxyCapableDoEsi();

            $this->_blIsActive = $blReverseProxyCapableDoEsi && $this->isEnabled();
        }

        return $this->_blIsActive;
    }

    /**
     * Return if Reverse Proxy is enabled with config parameters and serial.
     *
     * @return boolean
     */
    public function isEnabled()
    {
        if ($this->_blIsEnabled === null) {
            $oConfig = $this->_getConfig();
            $oSerial = $oConfig->getSerial();

            $this->_blIsEnabled = $oSerial->isFlagEnabled('reverse_proxy')
                                  && (bool) $oConfig->getConfigParam('blReverseProxyActive');
        }

        return $this->_blIsEnabled;
    }

    /**
     * Returns $blOnModuleEventDoNotFlushCache config value
     *
     * @return boolean
     */
    public function manualFlushForModules()
    {
        return (bool) ($this->_getConfig()->getConfigParam("blOnModuleEventDoNotFlushCache"));
    }

    /**
     * Adds additional value to reverse proxy environment key.
     * If value is empty or equal to default value, no value
     * will be added. Second optional parameter defines config
     * option name according which comparison will be done.
     *
     * @param mixed  $sValue  parameter value
     * @param string $sPrefix prefix for value
     */
    protected function _addListDisplayTypeToEnvironmentKey($sValue, $sPrefix)
    {
        if (!empty($sValue)) {
            // checking if this is config parameter value
            // if value is not same as default in oxConfig table, add it to key
            if ($sValue != $this->getConfig()->getConfigParam("sDefaultListDisplayType")) {
                $this->_addParamToEnvironmentKey($sValue, $sPrefix);
            }
        }
    }

    /**
     * Sets articles per page parameter to environment key if needed.
     *
     * @param int    $iValue  articles per page value
     * @param string $sLdType display list type
     * @param string $sPrefix prefix for value
     *
     * @return null
     */
    protected function _addNrOfCatArticlesToEnvironmentKey($iValue, $sLdType, $sPrefix)
    {
        $oConfig = $this->getConfig();
        $iValue = (int) $iValue;

        if (empty($iValue)) {
            return;
        }

        $iDefaultValue = ($oConfig->getConfigParam('iNrofCatArticles')) ? $oConfig->getConfigParam('iNrofCatArticles') : 10;

        // getting default value according list type
        if ($sLdType == "grid") {
            $aNrOfCatArticles = $oConfig->getConfigParam('aNrofCatArticlesInGrid');
        } else {
            $aNrOfCatArticles = $oConfig->getConfigParam('aNrofCatArticles');
        }

        if (!empty($aNrOfCatArticles)) {
            $iDefaultValue = $aNrOfCatArticles[0];
        }

        if (!empty($iValue) && $iValue != $iDefaultValue) {
            $this->_addParamToEnvironmentKey($iValue, $sPrefix);
        }
    }

    /**
     * Adds additional value to reverse proxy environment key.
     * If value is empty, no value. Custom prefix is added to value to
     * make it unique.
     *
     * @param mixed  $sValue  parameter value
     * @param string $sPrefix prefix for value
     */
    protected function _addParamToEnvironmentKey($sValue, $sPrefix)
    {
        if (!empty($sValue)) {
            if (is_array($sValue) && !empty($sValue)) {
                $sValue = serialize($sValue);
            }

            $this->_aEnvKey[] = $sPrefix . $sValue;
        }
    }

    /**
     * Return hashed environment key. If no parameters are in key,
     * null will be returned.
     *
     * @return string
     */
    protected function _getEnvironmentKey()
    {
        if (is_array($this->_aEnvKey) && !empty($this->_aEnvKey)) {
            return md5(implode('|', $this->_aEnvKey));
        }
    }

    /**
     * Set sorting parameters to environment key.
     * Gets sorting from url, if possible, if not, get it from session
     */
    protected function _setSortingToEnvKey()
    {

        $oConfig = oxRegistry::getConfig();
        $sSortBy = $oConfig->getRequestParameter('listorderby');
        $sSortDir = $oConfig->getRequestParameter('listorder');

        if ($sSortBy && $sSortDir) {
            $aSorting = array();
            $aSorting['sortby'] = $sSortBy;
            $aSorting['sortdir'] = $sSortDir;
        } else {
            $aSorting = oxRegistry::getSession()->getVariable("aSorting");
        }
        $this->_addParamToEnvironmentKey($aSorting, "#5#");
    }

    /**
     * Set environment key to cookie with values from session.
     *
     * @return void
     */
    public function setEnvironmentKey()
    {
        if ($this->isAdmin()) {
            return;
        }

        $oConfig = $this->getConfig();

        if ($oConfig->getShopCurrency() > 0) {
            $this->_addParamToEnvironmentKey($oConfig->getShopCurrency(), "#1#");
        }

        // Language is not used in environment key as it is already included in url.
        // The exception is start page as it does not contain language id in url. In this case
        // reverse proxy must be informed to handle language or cache key.
        $sShopUrl = $oConfig->getShopUrl();
        $sCurrentUrl = oxRegistry::get("oxUtilsUrl")->getCurrentUrl();
        $oUtilsServer = oxRegistry::get("oxUtilsServer");
        $oSession = oxRegistry::getSession();

        if ($sShopUrl == $sCurrentUrl) {
            $oReverseProxyHeader = oxNew('oxReverseProxyHeader');
            $oReverseProxyHeader->setVaryLanguageHeader();
            $oReverseProxyHeader->sendHeader();
        }

        $this->_addListDisplayTypeToEnvironmentKey($oSession->getVariable("ldtype"), "#3#");
        $this->_addNrOfCatArticlesToEnvironmentKey($oSession->getVariable("_artperpage"), $oSession->getVariable("ldtype"), "#4#");
        $this->_setSortingToEnvKey();
        $this->_addParamToEnvironmentKey($oSession->getVariable("session_attrfilter"), "#6#");

        if ($this->getUser()) {
            $this->_addParamToEnvironmentKey(1, "#7#");
        }

        if ($oSession->getVariable("_newitem") || $oSession->getVariable("blAddedNewItem") || ($oSession->getBasket() && $oSession->getBasket()->getProductsCount())) {
            $this->_addParamToEnvironmentKey(1, "#8#");
        }

        if ($oSession->getVariable("aFiltcompproducts")) {
            $this->_addParamToEnvironmentKey(1, "#9#");
        }

        $sEnvKey = $this->_getEnvironmentKey();

        // checking if environment key is not empty. Only if not, then setting it.
        if (!empty($sEnvKey)) {
            $oUtilsServer->setOxCookie('oxenv_key', $sEnvKey);
        } else {
            // checking if exists previously set oxenv_key cookie - if yes, remove it, as no key is needed.
            //
            // After logout environment key of logged in user remains then all pages are cached as not logged in user.
            // When user logs in he gets cached page as not logged in user.
            if ($oUtilsServer->getOxCookie('oxenv_key')) {
                $oUtilsServer->setOxCookie('oxenv_key', '', 1);
            }
        }
    }
}
