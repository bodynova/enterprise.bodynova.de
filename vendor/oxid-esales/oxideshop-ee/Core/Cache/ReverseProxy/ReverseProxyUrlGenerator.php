<?php
/**
 * This Software is the property of OXID eSales and is protected
 * by copyright law - it is NOT Freeware.
 *
 * Any unauthorized use of this software without a valid license key
 * is a violation of the license agreement and will be prosecuted by
 * civil and criminal law.
 *
 * @link      http://www.oxid-esales.com
 * @copyright (C) OXID eSales AG 2003-2016
 * @version   OXID eShop EE
 */
namespace OxidEsales\EshopEnterprise\Core\Cache\ReverseProxy;

use \oxRegistry;
use \oxDb;

/**
 * Url generator for reverse proxy cache invalidation
 */
class ReverseProxyUrlGenerator
{
    /** Separates URL parts. */
    const URL_SEPARATOR = '/';

    /** Constant defines regexp match for anything ir URL. */
    const REGEXP_MATCH_EVERYTHING = '.*';

    /**
     * Url pool
     *
     * @var array
     */
    protected $_aUrlPool = array();

    /**
     * Widget names array
     *
     * @var array
     */
    protected $_aWidgets = array();

    /**
     * Static page names array
     *
     * @var array
     */
    protected $_aStaticPages = array();

    /**
     * Static page names array
     *
     * @var array
     */
    protected $_aDynamicPages = array();

    /**
     * Page names array
     *
     * @var array
     */
    protected $_aPages = array();

    /**
     * Objects names array
     *
     * @var array
     */
    protected $_aObjects = array();

    /**
     * Return generated Url array
     *
     * @return array
     */
    public function getUrlPool()
    {
        return $this->_aUrlPool;
    }

    /**
     * Set generated Url array
     *
     * @param array $aUrlPool array of urls
     */
    public function setUrlPool($aUrlPool)
    {
        $this->_aUrlPool = $aUrlPool;
    }

    /**
     * Set passed Url
     *
     * @param string $sUrl url
     */
    public function setUrl($sUrl)
    {
        $this->_aUrlPool[] = $sUrl;
    }

    /**
     * Set widget name and parameters to generator
     *
     * @param string $sWidgetName widget name
     * @param array  $aParams     array of parameter
     */
    public function setWidget($sWidgetName, $aParams = null)
    {
        if (is_array($aParams)) {
            ksort($aParams);
        }
        $this->_aWidgets[] = array(strtolower($sWidgetName), $aParams);
    }

    /**
     * Returns all set widget names to generator
     *
     * @return array
     */
    public function getWidgets()
    {
        return $this->_aWidgets;
    }

    /**
     * Set static page name to generator
     *
     * @param string $sPageName page name
     * @param array  $aParams   parameters
     */
    public function setStaticPage($sPageName, $aParams = null)
    {
        $this->_aStaticPages[] = array($sPageName, $aParams);
    }

    /**
     * Set dynamic page name to generator
     *
     * @param string $sPageName page name
     * @param array  $aParams   parameters
     */
    public function setDynamicPage($sPageName, $aParams = null)
    {
        $this->_aDynamicPages[] = array($sPageName, $aParams);
    }

    /**
     * Set page name to generator
     *
     * @param string $sPageName page name
     * @param array  $aParams   parameters
     */
    public function setPage($sPageName, $aParams = null)
    {
        $this->_aPages[] = array($sPageName, $aParams);
    }

    /**
     * Returns array of set page names
     *
     * @return array
     */
    public function getPages()
    {
        return $this->_aPages;
    }

    /**
     * Returns array of set static page names
     *
     * @return array
     */
    public function getStaticPages()
    {
        return $this->_aStaticPages;
    }

    /**
     * Returns array of set dynamic page names
     *
     * @return array
     */
    public function getDynamicPages()
    {
        return $this->_aDynamicPages;
    }

    /**
     * Set object and its id to generator
     *
     * @param string $sObjectName object name
     * @param string $sOxId       object id
     */
    public function setObject($sObjectName, $sOxId = null)
    {
        $this->_aObjects[] = array($sObjectName, $sOxId);
    }

    /**
     * Returns set objects names and id
     *
     * @return array
     */
    public function getObjects()
    {
        return $this->_aObjects;
    }

    /**
     * Method prepends URL path with URL query.
     * For example: "eshop3.*Kuyichi-Jeans-Sugar.html"
     *
     * @param string $urlPath Path of URL to connect with URL query.
     * @param string $urlQuery Query of URL to connect with URL path.
     *
     * @return string
     */
    public function prependPathToUrlQuery($urlPath, $urlQuery)
    {
        $prependedUrl = $urlQuery;

        $urlPath = trim($urlPath, self::URL_SEPARATOR);
        $urlQuery = trim($urlQuery, self::URL_SEPARATOR);

        if ($urlQuery === '' && $urlPath) {
            $prependedUrl = self::URL_SEPARATOR . $urlPath . self::URL_SEPARATOR;
        }
        if ($urlQuery && $urlPath) {
            $prependedUrl = $this->concatQueryAndPath($urlPath, $urlQuery);
        }

        return $prependedUrl;
    }

    /**
     * Generate url from all set parameters
     */
    protected function _generate()
    {
        $this->_generateWidgetUrls();
        $this->_generateStaticPageUrls();
        $this->_generateDynamicPageUrls();
        $this->_generatePageUrls();
        $this->_generateObjectUrls();
    }

    /**
     * Generate url for widgets
     */
    protected function _generateWidgetUrls()
    {
        $aWidgets = $this->getWidgets();

        foreach ($aWidgets as $aWidget) {
            $sWidget = $aWidget[0];
            $sUrl = '/widget.php?';
            $aParams = array("cl" => $sWidget);
            if (isset($aWidget[1]) && is_array($aWidget[1])) {
                // append all parameters provided
                $aParams = array_merge($aWidget[1], $aParams);
                ksort($aParams);
            }
            foreach ($aParams as $sKey => $sVal) {
                $sUrl .= self::REGEXP_MATCH_EVERYTHING . $sKey . '=' . $sVal;
            }
            $sUrl .= self::REGEXP_MATCH_EVERYTHING;
            $this->_aUrlPool[] = $sUrl;
        }
    }

    /**
     * Generate url for static pages
     */
    protected function _generateStaticPageUrls()
    {
        if (count($this->getStaticPages())) {
            if (oxRegistry::getUtils()->seoIsActive()) {

                $oDb = oxDb::getDb();
                $sSql = "SELECT CONCAT( '". self::URL_SEPARATOR ."', `oxseourl`, '". self::REGEXP_MATCH_EVERYTHING ."' )
                    FROM `oxseo`
                    WHERE 1 AND `oxtype` = 'static' AND ";

                foreach ($this->getStaticPages() as $aPage) {
                    if ($aPage[0] && $aPage[0] != "") {
                        $aSqlWhereParts[] = "( `oxstdurl`='index.php?cl=" . $aPage[0] . "' )";
                    }
                    if ($aPage[1]) {
                        $aSqlWhereParts[] = "( `oxobjectid`='" . $aPage[1] . "' )";
                    }
                }
                $sSql .= ' (' . implode(' OR ', $aSqlWhereParts) . ') ';

                $aUrls = $oDb->getCol($sSql);
                if (count($aUrls)) {
                    $this->_aUrlPool = array_merge($this->_aUrlPool, $aUrls);
                }

            } else {
                foreach ($this->getStaticPages() as $aPage) {
                    if ($aPage[0]) {
                        $this->_aUrlPool[] = '/index.php?cl=' . $aPage[0] . self::REGEXP_MATCH_EVERYTHING;
                    }
                }
            }
        }
    }

    /**
     * Generate url for dynamic pages
     */
    protected function _generateDynamicPageUrls()
    {
        if (count($this->getDynamicPages())) {
            if (oxRegistry::getUtils()->seoIsActive()) {

                $oDb = oxDb::getDb();
                $sSql = "SELECT CONCAT( '". self::URL_SEPARATOR ."', `oxseourl`, '". self::REGEXP_MATCH_EVERYTHING ."' ) FROM `oxseo` WHERE 1 AND `oxtype` = 'dynamic' AND ";

                foreach ($this->getDynamicPages() as $aPage) {
                    if ($aPage[0] && $aPage[0] != "") {
                        $aSqlWhereParts[] = "( `oxstdurl` like '%?cl=" . $aPage[0] . "%' )";
                    }
                    if ($aPage[1]) {
                        $aSqlWhereParts[] = "( `oxobjectid`='" . $aPage[1] . "' )";
                    }
                }
                $sSql .= ' (' . implode(' OR ', $aSqlWhereParts) . ') ';

                $aUrls = $oDb->getCol($sSql);
                if (count($aUrls)) {
                    $this->_aUrlPool = array_merge($this->_aUrlPool, $aUrls);
                }

            } else {
                foreach ($this->getDynamicPages() as $aPage) {
                    if ($aPage[0]) {
                        $this->_aUrlPool[] = '/index.php?cl=' . $aPage[0] . self::REGEXP_MATCH_EVERYTHING;
                    }
                }
            }
        }
    }

    /**
     * Generate url for static pages
     */
    protected function _generatePageUrls()
    {
        if (count($this->getPages())) {
            foreach ($this->getPages() as $aPage) {
                $this->_aUrlPool[] = '/index.php?.*cl=' . $aPage[0] . self::REGEXP_MATCH_EVERYTHING;
            }
        }
    }

    /**
     * Generate url for objects
     */
    protected function _generateObjectUrls()
    {
        if (count($this->getObjects())) {
            if (oxRegistry::getUtils()->seoIsActive()) {

                $oDb = oxDb::getDb();
                $sSql = "SELECT CONCAT( '". self::URL_SEPARATOR ."', `oxseourl`, '.*' ) FROM `oxseo` WHERE 1 AND ";

                foreach ($this->getObjects() as $aObject) {
                    $sObjSql = '( `oxtype`=' . $oDb->quote($aObject[0]);
                    if ($aObject[1]) {
                        $sObjSql .= ' AND `oxobjectid`=' . $oDb->quote($aObject[1]);
                    }
                    $sObjSql .= ' )';
                    $aSqlWhereParts[] = $sObjSql;
                }
                $sSql .= ' (' . implode(' OR ', $aSqlWhereParts) . ') ';

                $aUrls = $oDb->getCol($sSql);
                if (count($aUrls)) {
                    $this->_aUrlPool = array_merge($this->_aUrlPool, $aUrls);
                }
            } else {
                foreach ($this->getObjects() as $aObject) {
                    $aAttributes = $this->_getObjectUrlAtributes($aObject[0]);
                    if (!is_null($aAttributes)) {
                        $sUrl = '/index.php?.*cl=' . $aAttributes[0] . self::REGEXP_MATCH_EVERYTHING;
                        if ($aObject[1]) {
                            $sUrl .= $aAttributes[1] . '=' . $aObject[1] . self::REGEXP_MATCH_EVERYTHING;
                        }

                        $this->_aUrlPool[] = $sUrl;
                    }
                }
            }
        }
    }

    /**
     * Return with object related information page controller name and variable name
     *
     * @param string $sObjectName object name
     *
     * @return array
     */
    protected function _getObjectUrlAtributes($sObjectName)
    {
        $aAttributes = null;

        switch ($sObjectName) {
            case 'oxarticle':
                $aAttributes = array('details', 'anid');
                break;
            case 'oxcontent':
                $aAttributes = array('content', 'oxcid');
                break;
            case 'oxcategory':
                $aAttributes = array('alist', 'cnid');
                break;
            case 'oxmanufacturer':
                $aAttributes = array('manufacturerlist', 'mnid');
                break;
            case 'oxvendor':
                $aAttributes = array('vendorlist', 'cnid');
                break;
        }

        return $aAttributes;
    }

    /**
     * Returns generated url
     *
     * @return null
     */
    public function getUrls()
    {
        $this->_generate();

        return $this->getUrlPool();
    }

    /**
     * Set start page for url generation
     */
    public function setStartPage()
    {
        $this->setStaticPage('start');
        $this->setUrl(self::URL_SEPARATOR);
    }

    /**
     * Set all detail pages urls
     */
    public function setDetails()
    {
        $this->setObject('oxArticle');
    }

    /**
     * Set all lists pages urls
     */
    public function setLists()
    {
        $this->setObject('oxCategory');
        $this->setObject('oxManufacturer');
        $this->setObject('oxVendor');
    }

    /**
     * Concatenates URL query and URL path.
     *
     * @param string $urlPath
     * @param string $urlQuery
     *
     * @return string
     */
    private function concatQueryAndPath($urlPath, $urlQuery)
    {
        $prependedUrl = $urlPath . self::REGEXP_MATCH_EVERYTHING;
        if ($urlQuery !== self::REGEXP_MATCH_EVERYTHING) {
            $prependedUrl .= $urlQuery;
        }

        return $prependedUrl;
    }
}
