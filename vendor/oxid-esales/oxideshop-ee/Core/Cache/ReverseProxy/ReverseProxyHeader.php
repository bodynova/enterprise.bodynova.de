<?php
/**
 * This Software is the property of OXID eSales and is protected
 * by copyright law - it is NOT Freeware.
 *
 * Any unauthorized use of this software without a valid license key
 * is a violation of the license agreement and will be prosecuted by
 * civil and criminal law.
 *
 * @link      http://www.oxid-esales.com
 * @copyright (C) OXID eSales AG 2003-2016
 * @version   OXID eShop EE
 */
namespace OxidEsales\EshopEnterprise\Core\Cache\ReverseProxy;

use OxidEsales\Eshop\Core\Registry;

/**
 * Reverse Proxy HTTP headers formator.
 * Collects HTTP headers for Reverse Proxy and form HTTP header.
 */
class ReverseProxyHeader extends \OxidEsales\EshopProfessional\Core\Header
{
    /**
     * Set cache age.
     *
     * @param int $iCacheAge seconds how long should be cached.
     */
    public function setCacheAge($iCacheAge)
    {
        if (isset($iCacheAge)) {
            $sHeader = "Cache-Control: s-maxage=" . $iCacheAge . ";";
            $this->setHeader($sHeader);
        }
    }

    /**
     * Set that there must be no stale date.
     */
    public function setNoStaleData()
    {
        $sHeader = "x-no-stale-data: 1;";
        $this->setHeader($sHeader);
    }

    /**
     * Set surogate header for reverse proxy to handle esi tags.
     */
    public function setSurrogateHeader()
    {
        $sHeader = 'Surrogate-Control: content="ESI/1.0;"';
        $this->setHeader($sHeader);
    }

    /**
     * Set Vary: X-UA-Language header for reverse proxy. This makes reverse proxy
     * to detect laguage from user browser and us it to cache page.
     */
    public function setVaryLanguageHeader()
    {
        $lang = Registry::getLang();
        $languangeIds = $lang->getActiveShopLanguageIds();
        // only send vary header on multi language shops because it causes cache misses
        if (count($languangeIds) > 1) {
            $header = "Vary: X-UA-Language";
            $this->setHeader($header);
        }
    }
}
