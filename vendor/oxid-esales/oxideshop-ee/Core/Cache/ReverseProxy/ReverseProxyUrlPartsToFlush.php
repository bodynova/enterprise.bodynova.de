<?php
/**
 * This Software is the property of OXID eSales and is protected
 * by copyright law - it is NOT Freeware.
 *
 * Any unauthorized use of this software without a valid license key
 * is a violation of the license agreement and will be prosecuted by
 * civil and criminal law.
 *
 * @link      http://www.oxid-esales.com
 * @copyright (C) OXID eSales AG 2003-2016
 * @version   OXID eShop EE
 */
namespace OxidEsales\EshopEnterprise\Core\Cache\ReverseProxy;

use \oxRegistry;

/**
 * Class forms URL parts which needs to be flushed.
 */
class ReverseProxyUrlPartsToFlush
{
    /** Used to match all hosts in URL. */
    const REGEXP_HOSTS_MATCHES = '.*';

    /**
     * Returns Shop URL host which should be flushed.
     *
     * @return string
     */
    public function getHost()
    {
        $config = oxRegistry::getConfig();

        $shopHost = static::REGEXP_HOSTS_MATCHES;
        if ($config->getConfigParam('reverseProxyFlushStrategy') === 1) {
            $oxUtilsUrl = oxRegistry::get('oxUtilsUrl');
            $shopHost = $oxUtilsUrl->getActiveShopHost();
        }

        return $shopHost;
    }

    /**
     * Returns Shop URL path which should be flushed.
     *
     * @return string
     */
    public function getPath()
    {
        $urlPath = '';
        $config = oxRegistry::getConfig();
        if ($config->getConfigParam('reverseProxyFlushStrategy') === 1) {
            $utilsUrl = oxRegistry::get('oxUtilsUrl');
            $urlPath = $utilsUrl->getActiveShopUrlPath();
        }

        return $urlPath;
    }
}
