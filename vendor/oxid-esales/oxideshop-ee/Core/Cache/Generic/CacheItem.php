<?php
/**
 * This Software is the property of OXID eSales and is protected
 * by copyright law - it is NOT Freeware.
 *
 * Any unauthorized use of this software without a valid license key
 * is a violation of the license agreement and will be prosecuted by
 * civil and criminal law.
 *
 * @link      http://www.oxid-esales.com
 * @copyright (C) OXID eSales AG 2003-2016
 * @version   OXID eShop EE
 */
namespace OxidEsales\EshopEnterprise\Core\Cache\Generic;

/**
 * Cached data class
 */
class CacheItem
{
    /** @var mixed Data storage */
    protected $_data = null;

    /**
     * Set data to cache item.
     *
     * @param mixed $mData data
     */
    public function setData($mData)
    {
        $this->_data = $mData;
    }

    /**
     * Get data from item.
     *
     * @return mixed
     */
    public function getData()
    {
        return $this->_data;
    }
}
