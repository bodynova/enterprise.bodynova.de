<?php
/**
 * This Software is the property of OXID eSales and is protected
 * by copyright law - it is NOT Freeware.
 *
 * Any unauthorized use of this software without a valid license key
 * is a violation of the license agreement and will be prosecuted by
 * civil and criminal law.
 *
 * @link      http://www.oxid-esales.com
 * @copyright (C) OXID eSales AG 2003-2016
 * @version   OXID eShop EE
 */
namespace OxidEsales\EshopEnterprise\Core\Cache\Generic;

use OxidEsales\Eshop\Application\Model\Contract\CacheConnectorInterface;
use \oxRegistry;

/**
 * Generic Cache.
 */
class Cache extends \OxidEsales\EshopEnterprise\Core\Base
{
    /** @var array Object pool. */
    protected $_aPool = array();

    /** @var CacheConnectorInterface Cache connector. */
    protected $_oConnector;

    /** @var array Cache connectors. */
    protected $_aConnectors = array(
        'oxMemcachedCacheConnector',
        'oxZendShmCacheConnector',
        'oxZendDiskCacheConnector',
        'oxFileCacheConnector',
    );

    /** @var bool Cache enabled flag. */
    protected $_blActive = null;

    /** @var int Cache TTL. */
    protected $_iTTL = null;

    /**
     * Check if cache is active.
     *
     * @return bool
     */
    public function isActive()
    {
        if ($this->_blActive === null) {
            $oConfig = oxRegistry::getConfig();
            $blActive = (bool) $oConfig->getConfigParam('blCacheActive');
            if ($blActive) {
                $oConnector = $this->_getConnector();
                $blActive = $oConnector && $oConnector->isAvailable();
            }

            $this->_blActive = $blActive;
        }

        return $this->_blActive;
    }

    /**
     * Register cache connector.
     *
     * @param CacheConnectorInterface $oConnector cache connector
     */
    public function registerConnector(\OxidEsales\EshopCommunity\Application\Model\Contract\CacheConnectorInterface $oConnector)
    {
        $this->_setConnector($oConnector);
        $this->_poolFlush();
    }

    /**
     * Get available cache connectors.
     *
     * @return array
     */
    public function getAvailableConnectors()
    {
        $aConnectors = array();
        $utilsObject = oxRegistry::get('oxUtilsObject');
        foreach ($this->_aConnectors as $sConnector) {
            $sRealConnectorClass = $utilsObject->getClassName(strtolower($sConnector));
            if (call_user_func(array($sRealConnectorClass, "isAvailable"))) {
                $aConnectors[] = $sConnector;
            }
        }

        return $aConnectors;
    }

    /**
     * Store single or multiple items.
     *
     * @param array|string    $mKey   key or array of cache items with keys.
     * @param CacheItem|int $mValue value or cache TTL (if mKey is array )
     * @param int             $iTTL   cache TTL
     */
    public function set($mKey, $mValue = null, $iTTL = null)
    {
        $blArray = is_array($mKey);
        if ($blArray && is_int($mValue)) {
            $iTTL = $mValue;
        }

        if ($this->_getConnector()) {
            $this->_getConnector()->set($mKey, $mValue, $this->_getTTL($iTTL));
        }

        $this->_poolSet($mKey, $mValue);
    }

    /**
     * Retrieve single or multiple cache items.
     *
     * @param mixed $mKey key or array of keys (if mKey is array)
     *
     * @return CacheItem|array[string]oxCacheItem
     */
    public function get($mKey)
    {
        $blArray = is_array($mKey);
        $aMissingKeys = array();
        $mPoolValue = $this->_poolGet($mKey, $aMissingKeys);

        if ($blArray) {
            $mKey = $aMissingKeys;
        }

        if ((count($aMissingKeys) || !$mPoolValue) && $this->_getConnector()) {
            $mValue = $this->_getConnector()->get($mKey);
            if ($blArray) {
                foreach ($mValue as $sKey => $oCacheItem) {
                    if ($oCacheItem) {
                        $mPoolValue[$sKey] = $oCacheItem;
                    }
                }
                $mValue = $mPoolValue;
                $this->_poolSet($mValue);
            } else {
                if ($mValue) {
                    $this->_poolSet($mKey, $mValue);
                }
            }
        } else {
            $mValue = $mPoolValue;
        }

        return $mValue;
    }

    /**
     * Invalidate single or multiple items.
     *
     * @param mixed $mKey key or array of keys (if mKey is array)
     */
    public function invalidate($mKey)
    {
        if ($this->_getConnector()) {
            $this->_getConnector()->invalidate($mKey);
        }
        $this->_poolInvalidate($mKey);
    }

    /**
     * Invalidate all items in the cache.
     */
    public function flush()
    {
        if ($this->_getConnector()) {
            $this->_getConnector()->flush();
        }
        $this->_poolFlush();
    }

    /**
     * Set cache connector.
     *
     * @param CacheConnectorInterface $oConnector cache connector
     */
    protected function _setConnector(\OxidEsales\EshopCommunity\Application\Model\Contract\CacheConnectorInterface $oConnector)
    {
        if ($oConnector->isAvailable()) {
            $this->_oConnector = $oConnector;
            $this->_blActive = null;
        } else {
            $this->_oConnector = null;
            $this->_blActive = false;
        }
    }

    /**
     * Get cache connector.
     *
     * @return CacheConnectorInterface
     */
    protected function _getConnector()
    {
        if (!$this->_blActive && !$this->_oConnector) {
            $this->_registerDefaultConnector();
        }

        return $this->_oConnector;
    }

    /**
     * Get TTL
     *
     * @param int $iTTL cache TTL
     *
     * @return int
     */
    protected function _getTTL($iTTL = null)
    {
        if (!is_null($iTTL)) {
            return $iTTL;
        }
        if (is_null($iTTL) && is_null($this->_iTTL)) {
            $oConfig = oxRegistry::getConfig();
            $iDefaultCacheTTL = $oConfig->getConfigParam('iDefaultCacheTTL');
            if ($iDefaultCacheTTL) {
                $this->_iTTL = $iDefaultCacheTTL;
            } else {
                $this->_iTTL = 0;
            }
        }

        return $this->_iTTL;
    }

    /**
     * Register default cache connector.
     */
    protected function _registerDefaultConnector()
    {
        $oConfig = oxRegistry::getConfig();
        $sDefaultConnector = $oConfig->getConfigParam('sDefaultCacheConnector');
        $utilsObject = oxRegistry::get('oxUtilsObject');
        $sDefaultConnector = $utilsObject->getClassName(strtolower($sDefaultConnector));

        if ($sDefaultConnector && call_user_func(array($sDefaultConnector, "isAvailable"))) {
            /** @var CacheConnectorInterface $oConnector */
            $oConnector = oxNew($sDefaultConnector);
            $this->_setConnector($oConnector);
        } else {
            // No available default connector.
            $this->_oConnector = null;
            $this->_blActive = false;
        }
    }

    /**
     * Clean object pool.
     */
    protected function _poolFlush()
    {
        $this->_aPool = array();
    }

    /**
     * Set single or multiple items to object pool.
     *
     * @param array|string     $mKey   cache key
     * @param CacheItem|null $mValue data
     */
    protected function _poolSet($mKey, $mValue = null)
    {
        if (is_array($mKey)) {
            $this->_aPool = array_merge($this->_aPool, $mKey);
        } else {
            $this->_aPool[$mKey] = $mValue;
        }
    }

    /**
     * Get single or multiple objects from pool.
     *
     * @param mixed $mKey          cache key or array of keys.
     * @param array &$aMissingKeys array missing of keys.
     *
     * @return CacheItem|array
     */
    protected function _poolGet($mKey, &$aMissingKeys = null)
    {
        $blArray = is_array($mKey);

        if (!$blArray) {
            $mKey = array($mKey);
        }

        $aKeys = array_values($mKey);
        $aItems = array_intersect_key($this->_aPool, array_flip($aKeys));

        if (count($aItems) < count($aKeys)) {
            $aMissingKeys = array_diff($aKeys, array_keys($aItems));
        }

        if (!$blArray) {
            if (count($aItems)) {
                $aItems = reset($aItems);
            } else {
                $aItems = null;
            }
        }

        return $aItems;
    }

    /**
     * Invalidate single or multiple object from pool.
     *
     * @param mixed $mKey cache key or array of keys.
     */
    protected function _poolInvalidate($mKey)
    {
        $blArray = is_array($mKey);
        if (!$blArray) {
            $mKey = array($mKey);
        }

        $aKeys = array_values($mKey);
        $this->_aPool = array_diff_key($this->_aPool, array_flip($aKeys));
    }
}
