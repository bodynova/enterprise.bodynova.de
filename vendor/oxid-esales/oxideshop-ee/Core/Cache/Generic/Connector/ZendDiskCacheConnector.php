<?php
/**
 * This Software is the property of OXID eSales and is protected
 * by copyright law - it is NOT Freeware.
 *
 * Any unauthorized use of this software without a valid license key
 * is a violation of the license agreement and will be prosecuted by
 * civil and criminal law.
 *
 * @link      http://www.oxid-esales.com
 * @copyright (C) OXID eSales AG 2003-2016
 * @version   OXID eShop EE
 */
namespace OxidEsales\EshopEnterprise\Core\Cache\Generic\Connector;

use OxidEsales\EshopEnterprise\Application\Model\Contract\CacheConnectorInterface;
use OxidEsales\Eshop\Core\Cache\Generic\CacheItem;
use \oxException;

/**
 * oxZendDiskCacheConnector class - Zend Server disk cache
 *
 */
class ZendDiskCacheConnector implements CacheConnectorInterface
{
    /**
     * Zend disk cache connector.
     *
     * @throws oxException
     */
    public function __construct()
    {
        if (!self::isAvailable()) {
            throw oxNew('oxException', EXCEPTION_NOZENDDISKCACHE);
        }
    }

    /**
     * Check if connector is available.
     *
     * @return bool
     */
    public static function isAvailable()
    {
        return function_exists('zend_disk_cache_fetch');
    }

    /**
     * Store single or multiple items.
     *
     * @param array|string    $mKey   key or array of cache items with keys.
     * @param CacheItem|int $mValue value or cache TTL (if mKey is array )
     * @param int             $iTTL   cache TTL
     *
     * @return null
     */
    public function set($mKey, $mValue = null, $iTTL = 0)
    {
        $blArray = is_array($mKey);
        if (!$blArray) {
            $mKey = array($mKey => $mValue);
        } elseif (is_int($mValue)) {
            $iTTL = $mValue;
        }

        foreach ($mKey as $sKey => $mValue) {
            zend_disk_cache_store($sKey, $mValue, $iTTL);
        }
    }

    /**
     * Retrieve single or multiple cache items.
     *
     * @param array|string $mKey key or array of keys (if mKey is array)
     *
     * @return CacheItem|array[string]oxCacheItem
     */
    public function get($mKey)
    {
        $blArray = is_array($mKey);
        if (!$blArray) {
            $mKey = array($mKey);
        }

        $mValue = array();
        foreach ($mKey as $sKey) {
            $mData = zend_disk_cache_fetch($sKey);
            if ($mData !== false) {
                $mValue[$sKey] = $mData;
            }
        }

        if (!$blArray) {
            if (count($mValue)) {
                $mValue = reset($mValue);
            } else {
                $mValue = null;
            }
        }

        return $mValue;
    }

    /**
     * Invalidate single or multiple items.
     *
     * @param array|string $mKey key or array of keys (if mKey is array)
     *
     * @return null
     */
    public function invalidate($mKey)
    {
        $blArray = is_array($mKey);
        if (!$blArray) {
            $mKey = array($mKey);
        }

        foreach ($mKey as $sKey) {
            zend_disk_cache_delete($sKey);
        }
    }

    /**
     * Invalidate all items in the cache.
     *
     * @return null
     */
    public function flush()
    {
        zend_disk_cache_clear();
    }
}
