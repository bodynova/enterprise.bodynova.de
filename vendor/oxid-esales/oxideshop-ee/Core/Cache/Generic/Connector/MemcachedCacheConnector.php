<?php
/**
 * This Software is the property of OXID eSales and is protected
 * by copyright law - it is NOT Freeware.
 *
 * Any unauthorized use of this software without a valid license key
 * is a violation of the license agreement and will be prosecuted by
 * civil and criminal law.
 *
 * @link      http://www.oxid-esales.com
 * @copyright (C) OXID eSales AG 2003-2016
 * @version   OXID eShop EE
 */
namespace OxidEsales\EshopEnterprise\Core\Cache\Generic\Connector;

use OxidEsales\EshopEnterprise\Application\Model\Contract\CacheConnectorInterface;
use \oxRegistry;
use \oxException;
use \Memcached;
use OxidEsales\Eshop\Core\Cache\Generic\CacheItem;

/**
 * Memcached connector for generic cache.
 */
class MemcachedCacheConnector implements CacheConnectorInterface
{

    /** @var Memcached Memcached object. */
    protected $_oMemcached;

    /**
     * Memcached cache connector.
     *
     * @throws oxException
     */
    public function __construct()
    {
        if (!self::isAvailable()) {
            throw oxNew('oxException', EXCEPTION_NOMEMCACHED);
        }

        $this->_oMemcached = new Memcached();
        $this->_oMemcached->addServers($this->_getParsedServers());
    }

    /**
     * Check if connector is available.
     *
     * @return bool
     */
    public static function isAvailable()
    {
        $oConfig = oxRegistry::getConfig();
        $oSerial = $oConfig->getSerial();

        return extension_loaded('Memcached') && $oSerial->isFlagEnabled('memcached_connector');
    }

    /**
     * Store single or multiple items.
     *
     * @param array|string    $mKey   key or array of cache items with keys.
     * @param CacheItem|int $mValue value or cache TTL (if mKey is array )
     * @param int             $iTTL   cache TTL
     *
     * @return null
     */
    public function set($mKey, $mValue = null, $iTTL = 0)
    {
        $blArray = is_array($mKey);
        if ($blArray && is_int($mValue)) {
            $iTTL = $mValue;
        }

        $iExpires = $iTTL ? (time() + $iTTL) : 0;
        if ($blArray) {
            $this->_oMemcached->setMulti($mKey, $iExpires);
        } else {
            $this->_oMemcached->set($mKey, $mValue, $iExpires);
        }
    }

    /**
     * Retrieve single or multiple cache items.
     *
     * @param array|string $mKey key or array of keys (if mKey is array)
     *
     * @return CacheItem|array[string]oxCacheItem
     */
    public function get($mKey)
    {
        $blArray = is_array($mKey);
        if ($blArray) {
            $mValue = $this->_oMemcached->getMulti($mKey);
        } else {
            $mValue = $this->_oMemcached->get($mKey);
        }

        if ($mValue !== false) {
            return $mValue;
        }
    }

    /**
     * Invalidate single or multiple items.
     *
     * @param array|string $mKey key or array of keys (if mKey is array)
     *
     * @return null
     */
    public function invalidate($mKey)
    {
        $blArray = is_array($mKey);
        if (!$blArray) {
            $mKey = array($mKey);
        }

        foreach ($mKey as $sKey) {
            $this->_oMemcached->delete($sKey);
        }
    }

    /**
     * Invalidate all items in the cache.
     */
    public function flush()
    {
        $this->_oMemcached->flush();
    }

    /**
     * Get parsed list of memcached servers, because in config they are stored in array("host@port@weight", ...)
     * aditionally works with preparsed 2 level array(array("host","port","weight"),...).
     *
     * @return array
     */
    protected function _getParsedServers()
    {
        $oConfig = oxRegistry::getConfig();
        $aMemcachedServers = $oConfig->getConfigParam('aMemcachedServers');

        $aServers = array();
        if (is_array($aMemcachedServers)) {
            foreach ($aMemcachedServers as $aServer) {
                if (!is_array($aServer)) {
                    $aServer = explode('@', $aServer);
                }
                if (is_array($aServer)) {
                    foreach ($aServer as &$sValue) {
                        $sValue = trim($sValue);
                    }
                    $aServers[] = $aServer;
                }
            }
        } else {
            // Defaults
            $aServers = array(array('localhost', '11211', '100'));
        }

        return $aServers;
    }
}
