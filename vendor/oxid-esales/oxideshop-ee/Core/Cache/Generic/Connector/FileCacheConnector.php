<?php
/**
 * This Software is the property of OXID eSales and is protected
 * by copyright law - it is NOT Freeware.
 *
 * Any unauthorized use of this software without a valid license key
 * is a violation of the license agreement and will be prosecuted by
 * civil and criminal law.
 *
 * @link      http://www.oxid-esales.com
 * @copyright (C) OXID eSales AG 2003-2016
 * @version   OXID eShop EE
 */
namespace OxidEsales\EshopEnterprise\Core\Cache\Generic\Connector;

use OxidEsales\EshopEnterprise\Application\Model\Contract\CacheConnectorInterface;
use \oxRegistry;
use OxidEsales\Eshop\Core\Cache\Generic\CacheItem;

/**
 * File connector for generic cache.
 */
class FileCacheConnector implements CacheConnectorInterface
{
    /** @var string Cache directory */
    protected $_sCacheDir;

    /** @var string Default cache directory ( sCacheDir = getShopBasePath + sDefaultCacheDir). */
    protected $_sDefaultCacheDir = 'cache';

    /** @var int Cache default expires time. */
    protected $_iDefaultExpires = 157784630; // 5 years

    /**
     * Check if connector is available.
     *
     * @return bool
     */
    public static function isAvailable()
    {
        return true;
    }

    /**
     * Cache directory getter.
     *
     * @return string
     *
     * @return null
     */
    public function getCacheDir()
    {
        if (!$this->_sCacheDir) {
            $oConfig = oxRegistry::getConfig();
            $sCacheDir = $oConfig->getConfigParam('sCacheDir');
            if (!$sCacheDir) {
                // Defaults
                $sCacheDir = $this->_sDefaultCacheDir;
            }

            // Add shop base path if needed
            if (!is_dir($sCacheDir)) {
                $sCacheDir = getShopBasePath() . $sCacheDir;
            }
            $this->_sCacheDir = $sCacheDir;
        }

        return $this->_sCacheDir;
    }

    /**
     * Cache directory setter.
     *
     * @param string $sCacheDir directory path from base shop dir
     *
     * @return null
     */
    public function setCacheDir($sCacheDir)
    {
        $this->_sCacheDir = $sCacheDir;
    }

    /**
     * Store single or multiple items.
     *
     * @param array|string    $mKey   key or array of cache items with keys.
     * @param CacheItem|int $mValue value or cache TTL (if mKey is array )
     * @param int             $iTTL   cache TTL
     *
     * @return null
     */
    public function set($mKey, $mValue = null, $iTTL = 0)
    {
        $blArray = is_array($mKey);
        if (!$blArray) {
            $mKey = array($mKey => $mValue);
        } elseif (is_int($mValue)) {
            $iTTL = $mValue;
        }

        $iExpires = $iTTL ? (time() + $iTTL) : (time() + $this->_iDefaultExpires);
        foreach ($mKey as $sKey => $oCacheItem) {
            $this->_writeFile($this->_getFilePath($sKey), $oCacheItem, $iExpires);
        }
    }

    /**
     * Retrieve single or multiple cache items.
     *
     * @param array|string $mKey key or array of keys (if mKey is array)
     *
     * @return CacheItem|array[string]oxCacheItem
     *
     * @return null
     */
    public function get($mKey)
    {
        $blArray = is_array($mKey);
        if (!$blArray) {
            $mKey = array($mKey);
        }

        $mValue = array();
        foreach ($mKey as $sKey) {
            $sFilePath = $this->_getFilePath($sKey);
            if (file_exists($sFilePath)) {
                if (filemtime($sFilePath) > time()) {
                    $sContent = $this->_readFile($sFilePath);
                    $mValue[$sKey] = unserialize($sContent);
                } else {
                    $this->_deleteFile($sFilePath);
                }
            }
        }

        if (!$blArray) {
            if (count($mValue)) {
                $mValue = reset($mValue);
            } else {
                $mValue = null;
            }
        }

        return $mValue;
    }

    /**
     * Invalidate single or multiple items.
     *
     * @param string|array $mKey key or array of keys (if mKey is array)
     *
     * @return null
     */
    public function invalidate($mKey)
    {
        $blArray = is_array($mKey);
        if (!$blArray) {
            $mKey = array($mKey);
        }

        foreach ($mKey as $sKey) {
            $this->_deleteFile($this->_getFilePath($sKey));
        }
    }

    /**
     * Invalidate all items in the cache.
     *
     * @return null
     */
    public function flush()
    {
        $aFiles = glob($this->_getPath() . '*.cache');

        if (is_array($aFiles)) {
            foreach ($aFiles as $sFile) {
                unlink($sFile);
            }
        }
    }

    /**
     * Create file path
     *
     * @param string $sKey key
     *
     * @return string
     */
    protected function _getFilePath($sKey)
    {
        return $this->getCacheDir() . '/' . strtolower($sKey) . '.cache';
    }

    /**
     * Returns full path to cache dir
     *
     * @return string
     */
    protected function _getPath()
    {
        return $this->getCacheDir() . '/';
    }

    /**
     * Reads and returns cache file contents
     *
     * @param string $sFilePath cache file path
     *
     * @return string
     */
    protected function _readFile($sFilePath)
    {
        $sContent = file_get_contents($sFilePath);

        return $sContent;
    }

    /**
     * Reads and returns cache file contents
     *
     * @param string $sFilePath cache file path
     *
     * @return string
     */
    protected function _deleteFile($sFilePath)
    {
        return @unlink($sFilePath);
    }

    /**
     * Releases file lock and returns release state
     *
     * @param string      $sFilePath  file path
     * @param CacheItem $oCacheItem cache data
     * @param int         $iExpires   expire time
     *
     * @return null
     */
    protected function _writeFile($sFilePath, $oCacheItem, $iExpires)
    {
        $rHandle = fopen($sFilePath, "w");
        flock($rHandle, LOCK_EX);
        fwrite($rHandle, serialize($oCacheItem));
        flock($rHandle, LOCK_UN);
        fclose($rHandle);

        touch($sFilePath, $iExpires);
    }
}
