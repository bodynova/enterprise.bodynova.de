<?php
/**
 * This Software is the property of OXID eSales and is protected
 * by copyright law - it is NOT Freeware.
 *
 * Any unauthorized use of this software without a valid license key
 * is a violation of the license agreement and will be prosecuted by
 * civil and criminal law.
 *
 * @link      http://www.oxid-esales.com
 * @copyright (C) OXID eSales AG 2003-2016
 * @version   OXID eShop EE
 */
namespace OxidEsales\EshopEnterprise\Core\Cache\Generic\Connector;

use \oxCurl;
use \oxRegistry;

/**
 * Access Reverse Proxy over network
 */
class ReverseProxyCacheConnector
{
    /** @var string Reverse proxy urls. */
    protected $_sReverseProxyUrl = '';

    /** @var string Shop host. */
    protected $_sShopHost = '';

    /** @var oxCurl */
    private $curl;

    /**
     * @param oxCurl $curl
     */
    public function __construct($curl)
    {
        $this->curl = $curl;
    }

    /**
     * Return set reverse proxy url
     *
     * @return string
     */
    public function getReverseProxyUrl()
    {
        return $this->_sReverseProxyUrl;
    }

    /**
     * Set reverse proxy url
     *
     * @param string $sReverseProxyUrl shop url
     */
    public function setReverseProxyUrl($sReverseProxyUrl)
    {
        $this->_sReverseProxyUrl = $sReverseProxyUrl;
    }

    /**
     * Retun setted shop host.
     *
     * @return string
     */
    public function getShopHost()
    {
        return $this->_sShopHost;
    }

    /**
     * Set shop host
     *
     * @param string $sShopHost shop host
     */
    public function setShopHost($sShopHost)
    {
        $this->_sShopHost = $sShopHost;
    }

    /**
     * Build and call CURL request.
     *
     * @param string $sRequestMethod Request method (PURGE, REFRESH, BAN).
     * @param string $sUrl           URL to be called.
     * @param array  $aHeaders       Additional headers.
     */
    protected function _callCurl($sRequestMethod, $sUrl, $aHeaders = array())
    {
        $oCurl = $this->getCurl();
        $oCurl->setMethod("GET");
        $oCurl->setUrl($sUrl);
        $oCurl->setHeader($aHeaders);
        $oCurl->setOption('CURLOPT_FRESH_CONNECT', true);
        $oCurl->setOption('CURLOPT_CUSTOMREQUEST', $sRequestMethod);
        $oCurl->setOption('CURLOPT_HEADER', false);
        $oCurl->setOption('CURLOPT_NOBODY', true);
        $oCurl->execute();
    }

    /**
     * Calls PURGE method on single URL.
     * Reverse proxy flushes objects with similar URL.
     *
     * @param string $sUrl     URL to be invalidated.
     * @param array  $aHeaders Additional headers.
     */
    public function purge($sUrl, $aHeaders = array())
    {
        $this->_callCurl('PURGE', $sUrl, $aHeaders);
    }

    /**
     * Calls BAN method on single URL.
     * Reverse proxy flushes objects with exact URL.
     *
     * @param string $sUrl     URL to be invalidated.
     * @param array  $aHeaders Additional headers.
     */
    public function ban($sUrl, $aHeaders = array())
    {
        $this->_callCurl('BAN', $sUrl, $aHeaders);
    }

    /**
     * Calls "Refresh" method on single URL. Regexp as parameter is not accepted in this case.
     *
     * @param string $sUrl URL to be refreshed.
     */
    public function refresh($sUrl)
    {
        $this->_callCurl('REFRESH', $sUrl);
    }

    /**
     * Calls "ban" method on URL or URL patter.
     *
     * @param string $sUri URI to be banned (without host name). String or regexp
     *
     * @return null
     */
    public function invalidate($sUri)
    {
        // if URI is not set, do nothing
        if (!$sUri) {
            return;
        }

        $sHost = $this->getShopHost();

        $aHeaders = array(
            'x-ban-url: ' . $sUri,
            'x-ban-host: ' . $sHost,
        );

        if ($this->isUrlToBan($sUri)) {
            $this->ban($this->getReverseProxyUrl(), $aHeaders);
        } else {
            $this->purge($this->getReverseProxyUrl(), $aHeaders);
        }
    }

    /**
     * Invalidates ALL contents
     *
     * @return null
     */
    public function flush()
    {
        $this->invalidate("/");

        return $this->invalidate(".*");
    }

    /**
     * @return oxCurl
     */
    protected function getCurl()
    {
        return $this->curl;
    }

    /**
     * Checks if given URL part should be banned in varnish.
     *
     * @param $urlToBan
     * @return bool
     */
    private function isUrlToBan($urlToBan)
    {
        $result = true;

        $urlToBan = trim($urlToBan, '/');
        if ($urlToBan) {
            $result = $this->isHomePage($urlToBan);
        }

        return $result;
    }

    /**
     * Checks if given URL part should be interpreted as home page.
     *
     * @param $urlToBan
     * @return bool
     */
    private function isHomePage($urlToBan)
    {
        $result = false;

        $oxUtilsUrl = oxRegistry::get('oxUtilsUrl');
        $activeShopUrlPath = trim($oxUtilsUrl->getActiveShopUrlPath(), '/');

        if ($urlToBan === $activeShopUrlPath) {
            $result = true;
        }

        return $result;
    }
}
