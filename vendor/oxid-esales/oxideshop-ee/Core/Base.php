<?php
/**
 * This Software is the property of OXID eSales and is protected
 * by copyright law - it is NOT Freeware.
 *
 * Any unauthorized use of this software without a valid license key
 * is a violation of the license agreement and will be prosecuted by
 * civil and criminal law.
 *
 * @link      http://www.oxid-esales.com
 * @copyright (C) OXID eSales AG 2003-2016
 * @version   OXID eShop EE
 */

namespace OxidEsales\EshopEnterprise\Core;

use oxDb;

/**
 * @inheritdoc
 */
class Base extends \OxidEsales\EshopProfessional\Core\Base
{
    /**
     * @inheritdoc
     */
    public function setAdminMode($isAdmin)
    {
        // resetting rights object
        self::$_oRights = null;

        parent::setAdminMode($isAdmin);
    }

    /**
     * Rights manager getter
     *
     * @return \OxidEsales\EshopEnterprise\Application\Model\Rights|\OxidEsales\EshopEnterprise\Core\AdminRights
     */
    public function getRights()
    {
        $rightsRolesConfiguration = (int) $this->getConfig()->getConfigParam('blUseRightsRoles');
        if ($rightsRolesConfiguration && self::$_oRights === null) {
            self::$_oRights = false;
            if ($this->isAdmin() && ($rightsRolesConfiguration & 1)) {
                // checking if back-end RR control is on
                self::$_oRights = oxNew('oxadminrights');
                self::$_oRights->load();
            } elseif (!$this->isAdmin() && $rightsRolesConfiguration & 2) {
                if (oxDb::getDb()->getOne("select 1 from oxobjectrights")) {
                    // checking if front-end RR control is on
                    self::$_oRights = oxNew('oxrights');
                    self::$_oRights->load();
                }
            }
        }

        return self::$_oRights;
    }

    /**
     * Rights manager setter
     *
     * @param \OxidEsales\EshopEnterprise\Application\Model\Rights $rights rights manager object
     */
    public function setRights($rights)
    {
        self::$_oRights = $rights;
    }
}
