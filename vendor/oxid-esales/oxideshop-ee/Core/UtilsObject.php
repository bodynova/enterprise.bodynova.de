<?php
/**
 * This Software is the property of OXID eSales and is protected
 * by copyright law - it is NOT Freeware.
 *
 * Any unauthorized use of this software without a valid license key
 * is a violation of the license agreement and will be prosecuted by
 * civil and criminal law.
 *
 * @link      http://www.oxid-esales.com
 * @copyright (C) OXID eSales AG 2003-2016
 * @version   OXID eShop EE
 */

namespace OxidEsales\EshopEnterprise\Core;

use oxDb;

/**
 * @inheritdoc
 */
class UtilsObject extends \OxidEsales\EshopProfessional\Core\UtilsObject
{
    /**
     * Checks if item with oxid from table $tableName is derived from parent shop
     * Returns true if article is derived
     *
     * @deprecated on b-dev (2015-07-27); Use oxBase::isDerived();
     *
     * @param string $objectId  oxid
     * @param string $tableName table name
     *
     * @return bool
     */
    public function isDerivedFromParentShop($objectId, $tableName)
    {
        $database = oxDb::getDb();
        $isDerived = false;
        $query = "select oxshopid from $tableName where oxid = " . $database->quote($objectId);
        $shopId = $database->getOne($query);
        $currentShopId = $this->getShopIdCalculator()->getShopId();
        if ($currentShopId && $shopId && $shopId != $currentShopId) {
            //now check if that shop is really a parent shop of current shop
            $query = "select oxid from oxshops where oxid = '$currentShopId' and oxparentid = '$shopId'";
            $isDerived = (bool) $database->getOne($query);
        }

        return $isDerived;
    }
}
