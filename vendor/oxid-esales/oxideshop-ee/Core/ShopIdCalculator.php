<?php
/**
 * This Software is the property of OXID eSales and is protected
 * by copyright law - it is NOT Freeware.
 *
 * Any unauthorized use of this software without a valid license key
 * is a violation of the license agreement and will be prosecuted by
 * civil and criminal law.
 *
 * @link      http://www.oxid-esales.com
 * @copyright (C) OXID eSales AG 2003-2016
 * @version   OXID eShop EE
 */

namespace OxidEsales\EshopEnterprise\Core;

use oxUtilsServer;

/**
 * @inheritdoc
 */
class ShopIdCalculator extends \OxidEsales\EshopProfessional\Core\ShopIdCalculator
{
    /**
     * @var int $_iShopId the active shop
     * stored as member variable so it does not has to be fetched from database
     */
    private $_iShopId;

    /**
     * @inheritdoc
     */
    public function getShopId()
    {
        if (isset($this->_iShopId)) {
            return $this->_iShopId;
        }
        $iShopId = false;

        if (!$iShopId && isset($_POST['shp'])) {
            $iShopId = (int) $_POST['shp'];
        }

        if (!$iShopId && isset($_GET['shp'])) {
            $iShopId = (int) $_GET['shp'];
        }

        if (!$iShopId && isset($_POST['actshop'])) {
            $iShopId = (int) $_POST['actshop'];
        }

        if (!$iShopId && isset($_GET['actshop'])) {
            $iShopId = (int) $_GET['actshop'];
        }

        if (!$iShopId) {
            $aShopUrlMap = $this->_getShopUrlMap();
            if (is_array($aShopUrlMap) && count($aShopUrlMap)) {
                // TODO: check posibility to use oxregistry
                $oUtilsServer = new oxUtilsServer();
                foreach ($aShopUrlMap as $sUrl => $iShp) {
                    if ($sUrl && $oUtilsServer->isCurrentUrl($sUrl)) {
                        $iShopId = $iShp;
                    }
                }
            }
        }

        if (!$iShopId) {
            $iShopId = 1;
        }
        $this->_iShopId = $iShopId;
        return $iShopId;
    }
}
