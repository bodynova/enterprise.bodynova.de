<?php

namespace OxidEsales\EshopEnterprise\Core;

/**
 * @inheritdoc
 */
class Serial extends \OxidEsales\EshopProfessional\Core\Serial
{
    /**
     * @inheritdoc
     */
    protected function isInvalidBetaSerial($sSerial)
    {
        return false;
    }

    /**
     * @inheritdoc
     */
    protected function _isCorrectMandateAmount()
    {
        $blCorrectAmount = true;

        $oConfig = $this->getConfig();

        $iMandateCount = $oConfig->getMandateCount();
        $iMaxMandates = $oConfig->getConfigParam('IMS');
        if ($iMandateCount > $iMaxMandates) {
            $blCorrectAmount = false;
            $this->_sValidationCode = 'incorrect_mandate_amount';
        }

        return $blCorrectAmount;
    }
}
