<?php
/**
 * This Software is the property of OXID eSales and is protected
 * by copyright law - it is NOT Freeware.
 *
 * Any unauthorized use of this software without a valid license key
 * is a violation of the license agreement and will be prosecuted by
 * civil and criminal law.
 *
 * @link      http://www.oxid-esales.com
 * @copyright (C) OXID eSales AG 2003-2016
 * @version   OXID eShop EE
 */
namespace OxidEsales\EshopEnterprise\Core;

use OxidEsales\Eshop\Core\Registry;
use OxidEsales\Eshop\Core\Database\Adapter\Doctrine\Database as DatabaseAdapter;
use oxRegistry;
use OxidEsales\EshopCommunity\Core\Exception\StandardException;

/**
 * Database connection class
 */
class DatabaseProvider extends \OxidEsales\EshopProfessional\Core\DatabaseProvider
{

    /**
     * A singleton instance of this class or a sub class of this class
     *
     * @var null|DatabaseProvider
     */
    protected static $instance = null;

    /**
     * @inheritdoc
     */
    protected function onPostConnect()
    {
        parent::onPostConnect();

        /**
         * If this is a master-slave connection, but the shop has no master-slave license, force the connection to pick
         * the master. The connection will never pick the slave from this point.
         */
        $isMasterSlaveConnection = static::$db->isMasterSlaveConnection();
        if (!$isMasterSlaveConnection) {
            return;
        }

        $hasMasterSlaveLicense = false;
        try {
            $hasMasterSlaveLicense = $this->isMasterSlaveLicense();
        } catch (\Exception $e) {
            //@TODO: this is a temporary solution, need to refactor this place
            // When migration from CE to EE currently the EE classes are already used before
            // the migration from CE to PE is done. So when Database::isMasterSlaveLicense() is called
            // we might get an exception because oxshops.oxserial is not yet present.
            // For now we catch, log and swallow here.
            $standard = new StandardException;
            $message = 'Error during Database::isMasterSlaveLicense() - ' . $e->getMessage();
            Registry::getUtils()->writeToLog($message, $standard->getLogFileName());
        }

        if ($isMasterSlaveConnection &&
            !$hasMasterSlaveLicense
        ) {
            static::$db->forceMasterConnection();
        }

        /**
         * If this is a master-slave connection, commands executed in parent::onPostConnect() may force the connection
         * to pick the master. As the connection would stay on the master from this point, the connection will be forced
         * to the slave here.
         */
        if ($isMasterSlaveConnection && $hasMasterSlaveLicense) {
            static::$db->forceSlaveConnection();
        }
    }

    /**
     * @inheritdoc
     */
    protected function createDatabase()
    {
        /** Call to fetchConfigFile redirects to setup wizard, if shop has not been configured. */
        $configFile = $this->fetchConfigFile();

        /** Validate the configuration file */
        $this->validateConfigFile($configFile);

        /** Set config file to be able to read shop configuration within the class */
        $this->setConfigFile($configFile);

        /** @var array $connectionParameters Parameters needed for the database connection */
        $connectionParameters = $this->getConnectionParameters();

        $databaseAdapter = new DatabaseAdapter();
        $databaseAdapter->setConnectionParameters($connectionParameters);
        $databaseAdapter->connect();

        return $databaseAdapter;
    }

    /**
     * @inheritdoc
     *
     * @return array
     */
    protected function getConnectionParameters()
    {
        $connectionParameters = parent::getConnectionParameters();
        $slaveConnectionParameters = $this->getSlaveConnectionParameters($connectionParameters);

        if (count($slaveConnectionParameters) > 0) {
            $connectionParameters['slaves'] = $slaveConnectionParameters;
        }

        return $connectionParameters;
    }

    /*
     * Get the parameters from config.inc.php file for slave configuration.
     *
     * @param array $connectionParameters
     * @return array
     */
    protected function getSlaveConnectionParameters($connectionParameters)
    {
        $slaveConnectionParameters = array();
        $slaveHostsConfigParameter = $this->getConfigParam('aSlaveHosts');

        if (!is_array($slaveHostsConfigParameter) || count($slaveHostsConfigParameter) === 0) {
            return $slaveConnectionParameters;
        }

        for ($i = 0; $i < count($slaveHostsConfigParameter); $i++) {
            $slaveConnectionParameters[] = array(
                'databaseHost'     => $slaveHostsConfigParameter[$i],
                'databasePort'     => $connectionParameters['default']['databasePort'],
                'databaseName'     => $connectionParameters['default']['databaseName'],
                'databaseUser'     => $connectionParameters['default']['databaseUser'],
                'databasePassword' => $connectionParameters['default']['databasePassword'],
            );
        }

        return $slaveConnectionParameters;
    }

    /**
     * @inheritdoc
     * The result will be fetched from the eShop file cache.
     *
     * @param string $tableName
     *
     * @return array
     */
    protected function fetchTableDescription($tableName)
    {
        $utils = Registry::getUtils();
        $fields = $utils->fromFileCache("tbdsc_" . $tableName);

        if (!$fields) {
            $fields = self::getDb()->metaColumns($tableName);
            $utils->toFileCache("tbdsc_" . $tableName, $fields);
        }

        return $fields;
    }

    /**
     * Returns true, if the shop is allowed to use master-slave for database connection
     *
     * @return bool|null
     */
    private function isMasterSlaveLicense() {
        $config = Registry::getConfig();
        $isMasterSlaveLicense = $config->getSerial(true)->isFlagEnabled('master_slave');

        return $isMasterSlaveLicense;
    }

}
