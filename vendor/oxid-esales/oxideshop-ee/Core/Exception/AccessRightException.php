<?php
/**
 * This Software is the property of OXID eSales and is protected
 * by copyright law - it is NOT Freeware.
 *
 * Any unauthorized use of this software without a valid license key
 * is a violation of the license agreement and will be prosecuted by
 * civil and criminal law.
 *
 * @link      http://www.oxid-esales.com
 * @copyright (C) OXID eSales AG 2003-2016
 * @version   OXID eShop EE
 */
namespace OxidEsales\EshopEnterprise\Core\Exception;

/**
 * Exception class thrown when a violation of access rights is commited, e.g.:
 * - no rights to view area
 */
class AccessRightException extends \oxException
{

    /** @var string To what object the access was denied */
    protected $_sObjectName = null;

    /**
     * Class name of the object which caused the RR exception
     *
     * @param string $sObjectName Class name of the object
     */
    public function setObjectName($sObjectName)
    {
        $this->_sObjectName = $sObjectName;
    }

    /**
     * Class name of the object that caused the RR exception
     *
     * @return string
     */
    public function getObjectName()
    {
        return $this->_sObjectName;
    }

    /**
     * Get string dump
     * Overrides oxException::getString()
     *
     * @return string
     */
    public function getString()
    {
        return __CLASS__ . '-' . parent::getString() . " Faulty Object --> " . $this->_sObjectName . "\n";
    }

    /**
     * Override of Exception::getValues()
     *
     * @return array
     */
    public function getValues()
    {
        $aRes = parent::getValues();
        $aRes['object'] = $this->getObjectName();

        return $aRes;
    }
}
