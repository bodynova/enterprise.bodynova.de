<?php
/**
 * This Software is the property of OXID eSales and is protected
 * by copyright law - it is NOT Freeware.
 *
 * Any unauthorized use of this software without a valid license key
 * is a violation of the license agreement and will be prosecuted by
 * civil and criminal law.
 *
 * @link      http://www.oxid-esales.com
 * @copyright (C) OXID eSales AG 2003-2016
 * @version   OXID eShop EE
 */
namespace OxidEsales\EshopEnterprise\Core;

use \oxCategory;

/**
 * Class oxCategory2ShopRelations
 *
 * @internal Do not make a module extension for this class.
 * @see      http://wiki.oxidforge.org/Tutorials/Core_OXID_eShop_classes:_must_not_be_extended
 */
class Category2ShopRelations extends Element2ShopRelations
{

    /**
     * Adds category to shop or list of shops.
     * Also adds category objects and subcategories to list of shops.
     *
     * @param oxCategory $oCategory Category object to add to shop.
     */
    public function addObjectToShop($oCategory)
    {
        parent::addObjectToShop($oCategory);
        $this->_addCategoryDependenciesToShop($oCategory->getId());
    }

    /**
     * Removes category from shop or list of shops.
     * Also removes category objects and subcategories from list of shops.
     *
     * @param oxCategory $oCategory Category object to add to shop.
     */
    public function removeObjectFromShop($oCategory)
    {
        parent::removeObjectFromShop($oCategory);
        $this->_removeCategoryDependenciesFromShop($oCategory->getId());
    }

    /**
     * Adds category dependencies to shop.
     *
     * @param string $sCategoryId Category ID.
     */
    private function _addCategoryDependenciesToShop($sCategoryId)
    {
        $this->_addCategoryObjectsToShop($sCategoryId);
        $this->_addSubCategoriesToShop($sCategoryId);
    }

    /**
     * Adds category objects to shop.
     *
     * @param string $sCategoryId Category ID.
     */
    private function _addCategoryObjectsToShop($sCategoryId)
    {
        $oElement2ShopRelations = oxNew('oxElement2ShopRelations', 'oxobject2category');
        $oElement2ShopRelations->setShopIds($this->getShopIds());

        $oCategory = oxNew('oxCategory');

        $aCategoryObjectIds = $oCategory->getCategoryObjectIds($sCategoryId);
        foreach ($aCategoryObjectIds as $sCategoryObjectId) {
            $oElement2ShopRelations->addToShop($sCategoryObjectId);
        }
    }

    /**
     * Adds subcategories to shop.
     *
     * @param string $sCategoryId Category ID.
     */
    private function _addSubCategoriesToShop($sCategoryId)
    {
        $oCategory = oxNew('oxCategory');

        $aSubCategoryIds = $oCategory->getFieldFromSubCategories('oxid', $sCategoryId);
        foreach ($aSubCategoryIds as $sSubCategoryId) {
            $this->addToShop($sSubCategoryId);
            $this->_addCategoryObjectsToShop($sSubCategoryId);
        }
    }

    /**
     * Removes category dependencies from shop.
     *
     * @param string $sCategoryId Category ID.
     */
    private function _removeCategoryDependenciesFromShop($sCategoryId)
    {
        $this->_removeCategoryObjectsFromShop($sCategoryId);
        $this->_removeSubCategoriesFromShop($sCategoryId);
    }

    /**
     * Removes category objects from shop.
     *
     * @param string $sCategoryId Category ID.
     */
    private function _removeCategoryObjectsFromShop($sCategoryId)
    {
        $oElement2ShopRelations = oxNew('oxElement2ShopRelations', 'oxobject2category');
        $oElement2ShopRelations->setShopIds($this->getShopIds());

        $oCategory = oxNew('oxCategory');

        $aCategoryObjectIds = $oCategory->getCategoryObjectIds($sCategoryId);
        foreach ($aCategoryObjectIds as $sCategoryObjectId) {
            $oElement2ShopRelations->removeFromShop($sCategoryObjectId);
        }
    }

    /**
     * Removes subcategories from shop.
     *
     * @param string $sCategoryId Category ID.
     */
    private function _removeSubCategoriesFromShop($sCategoryId)
    {
        $oCategory = oxNew('oxCategory');

        $aSubCategoryIds = $oCategory->getFieldFromSubCategories('oxid', $sCategoryId);
        foreach ($aSubCategoryIds as $sSubCategoryId) {
            $this->removeFromShop($sSubCategoryId);
            $this->_removeCategoryObjectsFromShop($sSubCategoryId);
        }
    }
}
