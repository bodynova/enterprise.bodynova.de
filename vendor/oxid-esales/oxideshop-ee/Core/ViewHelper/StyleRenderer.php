<?php
/**
 * This Software is the property of OXID eSales and is protected
 * by copyright law - it is NOT Freeware.
 *
 * Any unauthorized use of this software without a valid license key
 * is a violation of the license agreement and will be prosecuted by
 * civil and criminal law.
 *
 * @link      http://www.oxid-esales.com
 * @copyright (C) OXID eSales AG 2003-2016
 * @version   OXID eShop EE
 */

namespace OxidEsales\EshopEnterprise\Core\ViewHelper;

use oxRegistry;

/**
 * Class for preparing JavaScript.
 */
class StyleRenderer extends \OxidEsales\EshopProfessional\Core\ViewHelper\StyleRenderer
{
    /**
     * Returns whether rendering of scripts should be forced.
     *
     * @param bool $forceRender
     *
     * @return bool
     */
    protected function shouldForceRender($forceRender)
    {
        $reverseProxyBackend = oxRegistry::get('oxReverseProxyBackend');
        return parent::shouldForceRender($forceRender) ?: $reverseProxyBackend->isActive();
    }
}
