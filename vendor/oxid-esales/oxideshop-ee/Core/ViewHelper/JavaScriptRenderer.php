<?php
/**
 * This Software is the property of OXID eSales and is protected
 * by copyright law - it is NOT Freeware.
 *
 * Any unauthorized use of this software without a valid license key
 * is a violation of the license agreement and will be prosecuted by
 * civil and criminal law.
 *
 * @link      http://www.oxid-esales.com
 * @copyright (C) OXID eSales AG 2003-2016
 * @version   OXID eShop EE
 */
namespace OxidEsales\EshopEnterprise\Core\ViewHelper;

use oxRegistry;

/**
 * Class for preparing JavaScript.
 */
class JavaScriptRenderer extends \OxidEsales\EshopProfessional\Core\ViewHelper\JavaScriptRenderer
{
    /**
     * Returns if it is ajax request.
     *
     * @return bool
     */
    protected function isAjaxRequest()
    {
        $reverseProxyBackend = oxRegistry::get('oxReverseProxyBackend');
        return $reverseProxyBackend->isActive() ? false : parent::isAjaxRequest();
    }

    /**
     * Returns whether rendering of scripts should be forced.
     *
     * @param bool $forceRender
     * @param bool $isAjaxRequest
     *
     * @return bool
     */
    protected function shouldForceRender($forceRender, $isAjaxRequest)
    {
        $reverseProxyBackend = oxRegistry::get('oxReverseProxyBackend');
        return parent::shouldForceRender($forceRender, $isAjaxRequest) ?: $reverseProxyBackend->isActive();
    }

    /**
     * Returns files list for rendering.
     * Adds widgets handler to the list when reverse proxy is active.
     *
     * @param array  $files
     * @param string $widget
     *
     * @return array
     */
    protected function prepareFilesForRendering($files, $widget)
    {
        $files = parent::prepareFilesForRendering($files, $widget);
        $reverseProxyBackend = oxRegistry::get('oxReverseProxyBackend');
        if (!$widget && $reverseProxyBackend->isActive()) {
            $config = oxRegistry::getConfig();
            $files[3][] = $config->getResourceUrl("js/widgets/oxwidgetshandler.js", $config->isAdmin());
            $files[3] = array_unique($files[3]);
        }

        return $files;
    }
}
