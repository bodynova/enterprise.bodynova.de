<?php
/**
 * This Software is the property of OXID eSales and is protected
 * by copyright law - it is NOT Freeware.
 *
 * Any unauthorized use of this software without a valid license key
 * is a violation of the license agreement and will be prosecuted by
 * civil and criminal law.
 *
 * @link      http://www.oxid-esales.com
 * @copyright (C) OXID eSales AG 2003-2016
 * @version   OXID eShop EE
 */

namespace OxidEsales\EshopEnterprise\Core;

use \oxWidgetControl;
use \oxRegistry;

/**
 * @inheritdoc
 */
class WidgetControl extends \OxidEsales\EshopProfessional\Core\WidgetControl
{
    /**
     * Disallow setting env_key cookies for widget controller
     *
     * @var bool
     */
    protected $_blAllowEnvKeySetting = false;

    /**
     * Disallow invalidating cache for for widget controller.
     *
     * @var bool
     */
    protected $_blAllowCacheInvalidating = false;

    /**
     * Create object and ensure that params have correct value.
     */
    public function __construct()
    {
        // Force to handle in shop controller as this is first call to it if reverse proxy active.
        if (oxRegistry::get('oxReverseProxyBackend')->isActive()) {
            $this->_blHandlerSet = null;
            $this->_blMainTasksExecuted = null;
        }

        parent::__construct();
    }

    /**
     * This function is only executed one time here we perform checks if we
     * only need once per session. There is no need to execute it if there
     * is views chain as parent view already executed it.
     *
     * @return null
     */
    protected function _runOnce()
    {
        $shopConfig = $this->getConfig();
        $reverseProxyBackend = oxRegistry::get('oxReverseProxyBackend');
        if ($reverseProxyBackend->isActive() && !$shopConfig->hasActiveViewsChain()) {
            parent::_runOnce();
        }
    }
}