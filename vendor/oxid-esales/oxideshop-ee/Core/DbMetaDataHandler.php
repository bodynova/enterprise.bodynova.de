<?php
/**
 * This Software is the property of OXID eSales and is protected
 * by copyright law - it is NOT Freeware.
 *
 * Any unauthorized use of this software without a valid license key
 * is a violation of the license agreement and will be prosecuted by
 * civil and criminal law.
 *
 * @link      http://www.oxid-esales.com
 * @copyright (C) OXID eSales AG 2003-2016
 * @version   OXID eShop EE
 */

namespace OxidEsales\EshopEnterprise\Core;

/**
 * Class for handling database related operations
 */
class DbMetaDataHandler extends \OxidEsales\EshopProfessional\Core\DbMetaDataHandler
{
    /**
     * Mark view tables as invalid.
     *
     * @param string $tableName
     *
     * @return bool
     */
    protected function validateTableName($tableName)
    {
        $result = parent::validateTableName($tableName);
        if ($result) {
            $result = strpos($tableName, "oxv_") !== 0;
        }

        return $result;
    }
}
