<?php
/**
 * This Software is the property of OXID eSales and is protected
 * by copyright law - it is NOT Freeware.
 *
 * Any unauthorized use of this software without a valid license key
 * is a violation of the license agreement and will be prosecuted by
 * civil and criminal law.
 *
 * @link      http://www.oxid-esales.com
 * @copyright (C) OXID eSales AG 2003-2016
 * @version   OXID eShop EE
 */
namespace OxidEsales\EshopEnterprise\Core\GenericImport;

use Exception;
use oxDb;
use OxidEsales\Eshop\Core\GenericImport\ImportObject\ImportObject;
use OxidEsales\Eshop\Core\Config;
use oxRegistry;

/**
 * Manager of Administrators rights.
 */
class GenericImport extends \OxidEsales\EshopProfessional\Core\GenericImport\GenericImport
{
    /**
     * Checks if user has sufficient rights.
     *
     * @param ImportObject $importObject  Data type object
     * @param boolean      $isWriteAction Check for write permissions
     *
     * @throws Exception
     */
    protected function checkAccess($importObject, $isWriteAction)
    {
        parent::checkAccess($importObject, $isWriteAction);

        /** @var Config $config */
        $config = oxRegistry::getConfig();
        static $accessCache;

        // add R&R check for access
        if ($config->blUseRightsRoles) {
            $accessMode = ((boolean) $isWriteAction) ? '2' : '1';
            $typeClass = get_class($importObject);

            if (!isset($accessCache[$typeClass][$accessMode])) {
                $database = oxDb::getDb();

                //create list of user/group id's
                $userIds = array($database->quote($this->userId));
                $userGroupsQuery = 'SELECT oxgroupsid ' .
                    'FROM oxobject2group ' .
                    //"WHERE oxshopid = '{$this->_iShopID}' ".
                    "WHERE oxshopid = '{$config->getShopId()}' " .
                    "AND oxobjectid ='{$this->userId}'";

                $result = $database->select($userGroupsQuery);
                if ($result != false && $result->count() > 0) {
                    while (!$result->EOF) {
                        $userIds[] = $database->quote($result->fields[0]);
                        $result->fetchRow();
                    }
                }

                $rightFields = $importObject->getRightFields();
                foreach ($rightFields as $key => $field) {
                    $rightFields[$key] = $database->quote($field);
                }

                //check user rights...
                $query = 'SELECT count(*) ' .
                    'FROM oxfield2role as rr , oxrolefields as rf, oxobject2role as ro, oxroles as rt ' .
                    "WHERE rr.OXIDX < {$accessMode} " .
                    'AND rr.oxroleid = ro.oxroleid  ' .
                    'AND rt.oxid = ro.oxroleid ' .
                    'AND rt.oxactive = 1 ' .
                    //"AND rt.oxshopid = '{$this->_iShopID}'".
                    "AND rt.oxshopid = '{$config->getShopId()}'" .
                    'AND rf.oxparam IN (' . implode(',', $rightFields) . ') ' .
                    'AND ro.oxobjectid IN (' . implode(',', $userIds) . ') ' .
                    'AND rr.oxfieldid=rf.oxid';

                $accessLevel = $database->getOne($query);
                $accessCache[$typeClass][$accessMode] = $accessLevel;
            } else {
                $accessLevel = $accessCache[$typeClass][$accessMode];
            }

            if ($accessLevel) {
                throw new Exception(self::ERROR_USER_NO_RIGHTS);
            }
        }
    }
}
