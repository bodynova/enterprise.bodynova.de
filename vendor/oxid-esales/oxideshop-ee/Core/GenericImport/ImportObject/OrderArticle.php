<?php
/**
 * This Software is the property of OXID eSales and is protected
 * by copyright law - it is NOT Freeware.
 *
 * Any unauthorized use of this software without a valid license key
 * is a violation of the license agreement and will be prosecuted by
 * civil and criminal law.
 *
 * @link      http://www.oxid-esales.com
 * @copyright (C) OXID eSales AG 2003-2016
 * @version   OXID eShop EE
 */

namespace OxidEsales\EshopEnterprise\Core\GenericImport\ImportObject;

use Exception;
use oxBase;
use OxidEsales\Eshop\Core\GenericImport\GenericImport;
use oxRegistry;

/**
 * Import object for Order Articles.
 */
class OrderArticle extends \OxidEsales\EshopProfessional\Core\GenericImport\ImportObject\OrderArticle
{
    /** @var string Database table name. */
    protected $tableName = 'oxorderarticles';

    /** @var string Shop object name. */
    protected $shopObjectName = 'oxorderarticle';

    /**
     * Check for write access for id.
     *
     * @param oxBase $shopObject Loaded shop object
     * @param array  $data       Fields to be written, null for default
     *
     * @throws Exception on now access
     */
    public function checkWriteAccess($shopObject, $data = null)
    {
        if ($shopObject->oxorderarticles__oxordershopid->value != oxRegistry::getConfig()->getShopId()) {
            throw new Exception(GenericImport::ERROR_USER_NO_RIGHTS);
        }

        parent::checkWriteAccess($shopObject, $data);
    }

    /**
     * Returns formed order shop id, which should be set to data array.
     *
     * @param string $currentShopId
     *
     * @return string
     */
    protected function getOrderShopId($currentShopId)
    {
        return $currentShopId;
    }
}
