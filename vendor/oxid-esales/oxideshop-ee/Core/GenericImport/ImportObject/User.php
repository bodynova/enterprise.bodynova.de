<?php
/**
 * This Software is the property of OXID eSales and is protected
 * by copyright law - it is NOT Freeware.
 *
 * Any unauthorized use of this software without a valid license key
 * is a violation of the license agreement and will be prosecuted by
 * civil and criminal law.
 *
 * @link      http://www.oxid-esales.com
 * @copyright (C) OXID eSales AG 2003-2016
 * @version   OXID eShop EE
 */

namespace OxidEsales\EshopEnterprise\Core\GenericImport\ImportObject;

use Exception;
use oxBase;
use oxRegistry;

/**
 * Import object for Users.
 */
class User extends \OxidEsales\EshopProfessional\Core\GenericImport\ImportObject\User
{
    /**
     * Basic access check for writing data, checks for same shopid, should be overridden if field oxshopid does not
     * exist.
     *
     * @param oxBase $shopObject Loaded shop object
     * @param array  $data       Fields to be written, null for default
     *
     * @throws Exception on now access
     */
    public function checkWriteAccess($shopObject, $data = null)
    {
        $config = oxRegistry::getConfig();

        if (!$config->getConfigParam('blMallUsers')) {
            parent::checkWriteAccess($shopObject, $data);
        }
    }
}
