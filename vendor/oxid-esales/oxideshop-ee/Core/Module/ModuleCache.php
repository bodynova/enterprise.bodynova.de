<?php
/**
 * This Software is the property of OXID eSales and is protected
 * by copyright law - it is NOT Freeware.
 *
 * Any unauthorized use of this software without a valid license key
 * is a violation of the license agreement and will be prosecuted by
 * civil and criminal law.
 *
 * @link      http://www.oxid-esales.com
 * @copyright (C) OXID eSales AG 2003-2016
 * @version   OXID eShop EE
 */
namespace OxidEsales\EshopEnterprise\Core\Module;

/**
 * @inheritdoc
 */
class ModuleCache extends \OxidEsales\EshopProfessional\Core\Module\ModuleCache
{
    /**
     * Try to flush reverse proxy cache.
     *
     * @return null
     */
    public function executeDependencyEvent()
    {
        $oCache = $this->_getReverseProxyBackend();
        if ($oCache->isEnabled() && !$oCache->manualFlushForModules()) {
            $oCache->setFlush();
            $oCache->execute();
        }
    }

    /**
     * returns ReverseProxyBackend from Registry.
     *
     * @return \OxidEsales\EshopEnterprise\Core\Cache\ReverseProxy\ReverseProxyBackend
     */
    protected function _getReverseProxyBackend()
    {
        return \oxRegistry::get('oxReverseProxyBackend');
    }
}
