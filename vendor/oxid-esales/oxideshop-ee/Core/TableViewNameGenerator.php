<?php
/**
 * This Software is the property of OXID eSales and is protected
 * by copyright law - it is NOT Freeware.
 *
 * Any unauthorized use of this software without a valid license key
 * is a violation of the license agreement and will be prosecuted by
 * civil and criminal law.
 *
 * @link      http://www.oxid-esales.com
 * @copyright (C) OXID eSales AG 2003-2016
 * @version   OXID eShop EE
 */
namespace OxidEsales\EshopEnterprise\Core;

/**
 * @inheritdoc
 */
class TableViewNameGenerator extends \OxidEsales\EshopProfessional\Core\TableViewNameGenerator
{

    /**
     * if user does not want to use views he could specify that in config.inc.php
     * this could be used for example for performance reasons when there exists only one shop
     */
    protected function getViewSuffix($table, $languageId, $shopId, $isMultiLang)
    {
        $config = $this->getConfig();

        $viewSuffix = '';
        if ($shopId != -1 && $config->isMall() && in_array($table, $config->getConfigParam('aMultiShopTables'))) {
            $viewSuffix .= "_{$shopId}";
        }

        return $viewSuffix . parent::getViewSuffix($table, $languageId, $shopId, $isMultiLang);
    }
}
