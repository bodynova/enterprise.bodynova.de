<?php
/**
 * This Software is the property of OXID eSales and is protected
 * by copyright law - it is NOT Freeware.
 *
 * Any unauthorized use of this software without a valid license key
 * is a violation of the license agreement and will be prosecuted by
 * civil and criminal law.
 *
 * @link      http://www.oxid-esales.com
 * @copyright (C) OXID eSales AG 2003-2016
 * @version   OXID eShop EE
 */

namespace OxidEsales\EshopEnterprise\Core;

use \oxRegistry;

/**
 * @inheritdoc
 */
class Session extends \OxidEsales\EshopProfessional\Core\Session
{
    /**
     * Return Shop IR parameter for Url.
     *
     * @return string
     */
    protected function getShopUrlId()
    {
        $myConfig = $this->getConfig();
        $shopUrlId = '&amp;shp=' . $myConfig->getShopId();

        return $shopUrlId;
    }

    /**
     * Do not ses headers to browser if session starts cache for reverse proxy.
     *
     * @return bool
     */
    protected function needToSetHeaders()
    {
        $blSetNoCache = parent::needToSetHeaders();

        if ($this->_getReverseProxyBackend()->isActive()) {
            $blSetNoCache = false;
        }

        return $blSetNoCache;
    }

    /**
     * Disable caching when add Session ID to URL
     * if Reverse Proxy cache content.
     */
    protected function sidToUrlEvent()
    {
        parent::sidToUrlEvent();

        if ($this->_getReverseProxyBackend()->isActive()) {
            $this->getConfig()->getActiveView()->setIsCacheable(false);
        }
    }

    /**
     * returns ReverseProxyBackend from Registry
     *
     * @return Cache\ReverseProxy\ReverseProxyBackend
     */
    protected function _getReverseProxyBackend()
    {
        return oxRegistry::get('oxReverseProxyBackend');
    }
}