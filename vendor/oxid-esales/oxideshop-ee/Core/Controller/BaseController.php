<?php
/**
 * This Software is the property of OXID eSales and is protected
 * by copyright law - it is NOT Freeware.
 *
 * Any unauthorized use of this software without a valid license key
 * is a violation of the license agreement and will be prosecuted by
 * civil and criminal law.
 *
 * @link      http://www.oxid-esales.com
 * @copyright (C) OXID eSales AG 2003-2016
 * @version   OXID eShop EE
 */

namespace OxidEsales\EshopEnterprise\Core\Controller;

use oxDb;
use oxRegistry;

/**
 * @inheritdoc
 */
class BaseController extends \OxidEsales\EshopProfessional\Core\Controller\BaseController
{
    /**
     * Sets if page should be cached if user is logged in
     *
     * @var bool
     */
    protected $_blCacheForUser = true;

    /**
     * Sets if page should be cached
     *
     * @var bool
     */
    protected $_blCachePage = true;

    /**
     * Allow invalidating cache for shop control, but not for widget controller.
     *
     * @var bool
     */
    protected $_blAllowCacheInvalidating = true;

    /**
     * Returns if shop is mall
     *
     * @return bool
     */
    public function isMall()
    {
        return true;
    }

    /**
     * @inheritdoc
     */
    public function executeFunction($functionName)
    {
        if ($functionName && !self::$_blExecuted) {
            if (method_exists($this, $functionName)) {
                // once again checking if user has enough rights to exec. preferred action
                if (($rights = $this->getRights())) {
                    $rights->processView($this, $functionName);
                }
            }
        }

        parent::executeFunction($functionName);
    }

    /**
     * Returns if view should be cached
     *
     * @return bool
     */
    public function isCacheable()
    {
        if (!$this->_blCacheForUser && $this->getUser()) {
            return false;
        }

        return $this->_blCachePage;
    }

    /**
     * Returns if view should be cached
     *
     * @param bool $cache set/unset to cache page
     */
    public function setIsCacheable($cache)
    {
        $this->_blCachePage = $cache;
    }

    /**
     * Returns time to live in seconds
     *
     * @return integer
     */
    public function getCacheLifeTime()
    {
        $timeToLive = $this->getConfig()->getConfigParam("iLayoutCacheLifeTime");
        if (empty($timeToLive)) {
            $timeToLive = 3600;
        }

        $now = date('Y-m-d H:i:s', oxRegistry::get("oxUtilsDate")->getTime());
        $query = array(
            "SELECT UNIX_TIMESTAMP(MIN(`oxactivefrom`)) - UNIX_TIMESTAMP('" . $now . "') FROM `oxdiscount` WHERE `oxactivefrom` != '0000-00-00 00:00:00' AND `oxactivefrom` > '" . $now . "'",
            "SELECT UNIX_TIMESTAMP(MIN(`oxupdatepricetime`)) - UNIX_TIMESTAMP('" . $now . "') FROM `oxarticles` WHERE `oxupdatepricetime` != '0000-00-00 00:00:00' AND `oxupdatepricetime` > '" . $now . "'",
            "SELECT UNIX_TIMESTAMP(MIN(`oxupdatepricetime`)) - UNIX_TIMESTAMP('" . $now . "') FROM `oxfield2shop` WHERE `oxupdatepricetime` != '0000-00-00 00:00:00' AND `oxupdatepricetime` > '" . $now . "'",
            "SELECT UNIX_TIMESTAMP(MIN(`oxactiveto`)) - UNIX_TIMESTAMP('" . $now . "') FROM `oxactions` WHERE `oxactiveto` != '0000-00-00 00:00:00' AND `oxactiveto` > '" . $now . "'"
        );

        if ($this->getConfig()->getConfigParam('blUseTimeCheck')) {
            $query[] = "SELECT UNIX_TIMESTAMP(MIN(`oxactivefrom`)) - UNIX_TIMESTAMP('" . $now . "') FROM `oxarticles` WHERE `oxactivefrom` != '0000-00-00 00:00:00' AND `oxactivefrom` > '" . $now . "'";
        }

        $database = oxDb::getDb();
        foreach ($query as $oneQuery) {
            $timeToLiveDatabaseValue = $database->getOne($oneQuery);
            if (!empty($timeToLiveDatabaseValue) && $timeToLiveDatabaseValue < $timeToLive) {
                $timeToLive = $timeToLiveDatabaseValue;
            }
        }

        //for private sales, if basket can be reserved
        if ($this->getConfig()->getConfigParam('blPsBasketReservationEnabled')) {
            $timeout = $this->getConfig()->getConfigParam('iPsBasketReservationTimeout');
            $timeToLiveDatabaseValue = $database->getOne("SELECT UNIX_TIMESTAMP( '" . $now . "' ) - MIN(`oxupdate`) FROM `oxuserbaskets` WHERE `oxupdate` != '0' AND `oxupdate` < UNIX_TIMESTAMP( '" . $now . "' ) AND `oxtitle` = 'reservations'");
            if (!empty($timeToLiveDatabaseValue)) {
                $timeToLiveDatabaseValue = $timeout - $timeToLiveDatabaseValue;
                if ($timeToLiveDatabaseValue > 0 && $timeToLiveDatabaseValue < $timeToLive) {
                    $timeToLive = $timeToLiveDatabaseValue;
                }
            }
        }

        return $timeToLive;
    }

    /**
     * returns ReverseProxyBackend from Registry
     *
     * @return \OxidEsales\EshopEnterprise\Core\Cache\ReverseProxy\ReverseProxyBackend
     */
    protected function _getReverseProxyBackend()
    {
        return oxRegistry::get('oxReverseProxyBackend');
    }

    /**
     * Sets allow cache invalidating.
     *
     * @param bool $allowInvalidation Cache invalidating
     */
    public function setAllowCacheInvalidating($allowInvalidation)
    {
        $this->_blAllowCacheInvalidating = $allowInvalidation;
    }

    /**
     * Gets allow cache invalidating.
     *
     * @return bool
     */
    public function getAllowCacheInvalidating()
    {
        return $this->_blAllowCacheInvalidating;
    }

    /**
     * @inheritdoc
     */
    protected function onExecuteNewAction()
    {
        parent::onExecuteNewAction();

        $reverseProxyCache = $this->_getReverseProxyBackend();
        if ($reverseProxyCache->isEnabled()) {

            //invalidating proxy cache
            $reverseProxyCache->execute();
        }
    }
}
