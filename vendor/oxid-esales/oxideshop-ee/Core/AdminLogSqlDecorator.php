<?php
/**
 * This Software is the property of OXID eSales and is protected
 * by copyright law - it is NOT Freeware.
 *
 * Any unauthorized use of this software without a valid license key
 * is a violation of the license agreement and will be prosecuted by
 * civil and criminal law.
 *
 * @link      http://www.oxid-esales.com
 * @copyright (C) OXID eSales AG 2003-2016
 * @version   OXID eShop EE
 */

namespace OxidEsales\EshopEnterprise\Core;

use oxRegistry;

/**
 * @inheritdoc
 */
class AdminLogSqlDecorator extends \OxidEsales\EshopProfessional\Core\AdminLogSqlDecorator
{
    /**
     * @inheritdoc
     */
    public function prepareSqlForLogging($originalSql)
    {
        $userId = $this->getUserId();
        $shopConfig = oxRegistry::getConfig();

        $shopId = $shopConfig->getShopId();
        $sessionId = oxRegistry::getSession()->getId();
        $className = $shopConfig->getActiveView()->getClassName();
        $classMethod = $shopConfig->getActiveView()->getFncName();

        $preparedSql = "insert into {$this->table} (oxuserid, oxshopid, oxsessid, oxclass, oxfnc, oxsql)
          values ('{$userId}', '{$shopId}', '{$sessionId}', '{$className}', '{$classMethod}', " . $this->quote($originalSql) . ")";

        return $preparedSql;
    }
}
