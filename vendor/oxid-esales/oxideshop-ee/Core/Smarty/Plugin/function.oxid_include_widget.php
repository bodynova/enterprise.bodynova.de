<?php
/**
 * This Software is the property of OXID eSales and is protected
 * by copyright law - it is NOT Freeware.
 *
 * Any unauthorized use of this software without a valid license key
 * is a violation of the license agreement and will be prosecuted by
 * civil and criminal law.
 *
 * @link      http://www.oxid-esales.com
 * @copyright (C) OXID eSales AG 2003-2016
 * @version   OXID eShop EE
 */

/**
 * Smarty function
 * -------------------------------------------------------------
 * Purpose: set params and render widget
 * Use [{oxid_include_dynamic file="..."}] instead of include
 * -------------------------------------------------------------
 *
 * @param array  $params params
 * @param Smarty $smarty clever simulation of a method
 *
 * @return string
 */
function smarty_function_oxid_include_widget($params, &$smarty)
{
    $config = oxRegistry::getConfig();
    $class = strtolower($params['cl']);
    $reverseProxyBackend = oxRegistry::get('oxReverseProxyBackend');
    $shouldReturnEsi = $params['skipESIforUser'] && $smarty->oxobject->getUser() ? false : $reverseProxyBackend->isActive();

    if ($shouldReturnEsi) {
        $params['cl'] = $class;
        unset($params['skipESIforUser']);
        unset($params["_object"]);

        // setting widget parent views param
        if (!empty($params["_parent"])) {
            $params["oxwparent"] = $params["_parent"];
            unset($params["_parent"]);
        }
        
        // setting navigation parameters
        $navigationParams = $params["_navurlparams"] ? rawurldecode($params["_navurlparams"]) : "";
        unset($params["_navurlparams"]);

        //parse params string to params array and merge
        $utilsUrl = oxNew("oxUtilsUrl");
        $params = array_merge($params, $utilsUrl->stringToParamsArray($navigationParams));

        // sorting alphabetically
        ksort($params);

        return "<esi:include src='" . $config->getWidgetUrl(null, null, $params) . "'/>";
    }
    unset($params['cl']);

    $aParentViews = null;

    if (!empty($params["_parent"])) {
        $aParentViews = explode("|", $params["_parent"]);
        unset($params["_parent"]);
    }

    $oShopControl = oxRegistry::get('oxWidgetControl');

    return $oShopControl->start($class, null, $params, $aParentViews);
}
