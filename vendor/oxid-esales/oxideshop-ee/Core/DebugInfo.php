<?php
/**
 * This Software is the property of OXID eSales and is protected
 * by copyright law - it is NOT Freeware.
 *
 * Any unauthorized use of this software without a valid license key
 * is a violation of the license agreement and will be prosecuted by
 * civil and criminal law.
 *
 * @link      http://www.oxid-esales.com
 * @copyright (C) OXID eSales AG 2003-2016
 * @version   OXID eShop EE
 */

namespace OxidEsales\EshopEnterprise\Core;

use \oxRegistry;

/**
 * Debug information formatter.
 */
class DebugInfo extends \OxidEsales\EshopProfessional\Core\DebugInfo
{
    /**
     * Form that reverse proxy is active.
     *
     * @return string
     */
    public function formatReverseProxyActive()
    {
        $oReverseProxyCache = oxRegistry::get('oxReverseProxyBackend');

        $sLog = '';
        $sLog .= '<div>Reverse proxy is ';
        $sLog .= $oReverseProxyCache->isActive() ? 'active.' : 'not active.';
        $sLog .= '</div>';

        return $sLog;
    }

    /**
     * format cache info
     *
     * @param boolean $blIsCache  can cache
     * @param boolean $blIsCached is taken from cache
     * @param string  $sViewID    view id
     *
     * @return string
     */
    public function formatContentCaching($blIsCache = false, $blIsCached = false, $sViewID = null)
    {
        $sLog = '';
        if (oxRegistry::getConfig()->getConfigParam('blUseContentCaching')) {
            $sLog .= 'Content cache : ';

            if ($blIsCache) {
                if ($blIsCached) {
                    $sLog .= ' Cache Hit ';
                } else {
                    $sLog .= ' Cache Miss ';
                }

                $sLog .= ' [' . $sViewID . '] ';
            } else {
                $sLog .= ' Not Cachable ';
            }
            $sLog .= '<br />';
        }

        return $sLog;
    }
}
