<?php
/**
 * This Software is the property of OXID eSales and is protected
 * by copyright law - it is NOT Freeware.
 *
 * Any unauthorized use of this software without a valid license key
 * is a violation of the license agreement and will be prosecuted by
 * civil and criminal law.
 *
 * @link      http://www.oxid-esales.com
 * @copyright (C) OXID eSales AG 2003-2016
 * @version   OXID eShop EE
 */
namespace OxidEsales\EshopEnterprise\Core;

use \oxArticle;

/**
 * Class oxArticle2ShopRelations
 * Handles differently shop relation class with oxarticle element.
 * After un/assigning product to shop, un/assigns all variants of this product too.
 *
 * @internal Do not make a module extension for this class.
 * @see      http://wiki.oxidforge.org/Tutorials/Core_OXID_eShop_classes:_must_not_be_extended
 */
class Article2ShopRelations extends Element2ShopRelations
{
    /**
     * Adds article to shop or list of shops.
     * And updates according to it his variants inheritance.
     *
     * @param oxArticle $oElement element object to add shop id to.
     */
    public function addObjectToShop($oElement)
    {
        parent::addObjectToShop($oElement);

        if ($oElement->getVariantsCount()) {
            $this->updateVariantInheritance($oElement);
        }
    }

    /**
     * Gives an object of item and removes it from shop or list of shops.
     * And updates according to it his variants inheritance.
     *
     * @param oxArticle $oElement element object to add shop id to.
     */
    public function removeObjectFromShop($oElement)
    {
        parent::removeObjectFromShop($oElement);
        $this->updateVariantInheritance($oElement);
    }

    /**
     * Updates product inheritance information according to parent product inheritance information.
     *
     * @param string $sVariantId Id of given variant.
     * @param string $sParentId  Id of given variants parent product.
     */
    public function updateInheritanceFromParent($sVariantId, $sParentId)
    {
        // clear all previous inheritance information
        $this->removeFromAllShops($sVariantId);
        // copy inheritance information from parent item
        $this->copyInheritance($sParentId, $sVariantId);
    }

    /**
     * Takes variant id list of given article.
     * Adds/removes variants to/from shop according to parent article.
     *
     * @param oxArticle $oElement oxArticles object to get it's variants.
     */
    public function updateVariantInheritance($oElement)
    {
        $aVariantIds = $oElement->getVariantIds(false);
        $sElementId = $oElement->getId();
        foreach ($aVariantIds as $sId) {
            $this->updateInheritanceFromParent($sId, $sElementId);
        }
    }
}
