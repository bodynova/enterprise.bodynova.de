<?php
/**
 * This Software is the property of OXID eSales and is protected
 * by copyright law - it is NOT Freeware.
 *
 * Any unauthorized use of this software without a valid license key
 * is a violation of the license agreement and will be prosecuted by
 * civil and criminal law.
 *
 * @link      http://www.oxid-esales.com
 * @copyright (C) OXID eSales AG 2003-2016
 * @version   OXID eShop EE
 */

namespace OxidEsales\EshopEnterprise\Core;

use \oxRegistry;

/**
 * @inheritdoc
 */
class Language extends \OxidEsales\EshopProfessional\Core\Language
{
    /**
     * @inheritdoc
     */
    protected function getCustomThemeLanguageFiles($language)
    {
        $config = $this->getConfig();
        $theme = $config->getConfigParam("sTheme");
        $customTheme = $config->getConfigParam("sCustomTheme");
        $shopId = $config->getShopId();
        $applicationDirectory = $config->getAppDir();
        $languageAbbreviation = oxRegistry::getLang()->getLanguageAbbr($language);

        $languageFiles = parent::getCustomThemeLanguageFiles($language);

        if ($customTheme) {
            $shopPath = $applicationDirectory . 'views/' . $customTheme . '/' . $shopId . '/' . $languageAbbreviation;
            $languageFiles[] = $shopPath . "/lang.php";
            $languageFiles = $this->_appendLangFile($languageFiles, $shopPath);
        } elseif ($theme) {
            // theme shop languages
            $shopPath = $applicationDirectory . 'views/' . $theme . '/' . $shopId . '/' . $languageAbbreviation;
            $languageFiles[] = $shopPath . "/lang.php";
            $languageFiles = $this->_appendLangFile($languageFiles, $shopPath);
        }

        return $languageFiles;
    }

    /**
     * @inheritdoc
     */
    public function getMultiLangTables()
    {
        $tables = parent::getMultiLangTables();
        $tables[] = "oxfield2shop";

        return $tables;
    }

    /**
     * @inheritdoc
     */
    protected function _getLanguageIdsFromDatabase($shopId = null)
    {
        $languages = $this->_getConfigLanguageValues('aLanguageParams', $shopId);

        if (empty($languages)) {
            $languages = $this->_getConfigLanguageValues('aLanguages', $shopId);
        }

        return $languages;
    }
}
