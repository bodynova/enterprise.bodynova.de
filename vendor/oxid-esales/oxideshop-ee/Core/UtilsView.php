<?php
/**
 * This Software is the property of OXID eSales and is protected
 * by copyright law - it is NOT Freeware.
 *
 * Any unauthorized use of this software without a valid license key
 * is a violation of the license agreement and will be prosecuted by
 * civil and criminal law.
 *
 * @link      http://www.oxid-esales.com
 * @copyright (C) OXID eSales AG 2003-2016
 * @version   OXID eShop EE
 */

namespace OxidEsales\EshopEnterprise\Core;

use OxidEsales\EshopCommunity\Core\Edition\EditionSelector;
use OxidEsales\EshopCommunity\Core\Edition\EditionPathProvider;
use OxidEsales\EshopCommunity\Core\Edition\EditionRootPathProvider;

/**
 * @inheritdoc
 */
class UtilsView extends \OxidEsales\EshopProfessional\Core\UtilsView
{
    /**
     * @inheritdoc
     */
    public function getTemplateDirs()
    {
        $pathSelector = $this->getEnterprisePathSelector();
        $editionTemplatesDirectory = $this->addActiveThemeId($pathSelector->getViewsDirectory());
        $this->setTemplateDir($editionTemplatesDirectory);

        return parent::getTemplateDirs();
    }

    /**
     * @inheritdoc
     */
    protected function _smartyCompileCheck($smarty)
    {
        parent::_smartyCompileCheck($smarty);
        $config = $this->getConfig();
        if ($config->isProductiveMode()) {
            // override in any case
            $smarty->compile_check = false;
        }
    }

    /**
     * @inheritdoc
     */
    protected function _fillCommonSmartyProperties($smarty)
    {
        parent::_fillCommonSmartyProperties($smarty);
        array_unshift($smarty->plugins_dir, $this->getEnterprisePathSelector()->getSmartyPluginsDirectory());
    }

    /**
     * Create Path provider objects to get path.
     *
     * @return EditionPathProvider
     */
    private function getEnterprisePathSelector()
    {
        return new EditionPathProvider(new EditionRootPathProvider(new EditionSelector(EditionSelector::ENTERPRISE)));
    }
}
