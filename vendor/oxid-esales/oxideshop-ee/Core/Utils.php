<?php
/**
 * This Software is the property of OXID eSales and is protected
 * by copyright law - it is NOT Freeware.
 *
 * Any unauthorized use of this software without a valid license key
 * is a violation of the license agreement and will be prosecuted by
 * civil and criminal law.
 *
 * @link      http://www.oxid-esales.com
 * @copyright (C) OXID eSales AG 2003-2016
 * @version   OXID eShop EE
 */

namespace OxidEsales\EshopEnterprise\Core;

use \oxRegistry;

/**
 * @inheritdoc
 */
class Utils extends \OxidEsales\EshopProfessional\Core\Utils
{
    /**
     * @inheritdoc
     */
    public function resetTemplateCache($aTemplates)
    {
        parent::resetTemplateCache($aTemplates);

        //reset output cache
        $oCache = oxNew('oxCache');
        $oCache->reset(false);
    }

    /**
     * @inheritdoc
     */
    protected function prepareToExit()
    {
        parent::prepareToExit();

        // Add environment key and surrogate header as it might be ESI tags already formed.
        // For example if 404 error or redirect after adding to basket.
        $oReverseProxyBackend = oxRegistry::get('oxReverseProxyBackend');
        if ($oReverseProxyBackend->isEnabled()) {
            $oReverseProxyBackend->setEnvironmentKey();
            $oReverseProxyHeader = oxNew('oxReverseProxyHeader');
            $oActView = $this->getConfig()->getActiveView();
            if ($oActView->isCacheable()) {
                $oReverseProxyHeader->setCacheAge($oActView->getCacheLifeTime());
            } else {
                $oReverseProxyHeader->setNonCacheable();
            }
            $oReverseProxyHeader->setSurrogateHeader();
            $oReverseProxyHeader->sendHeader();
        } elseif ($this->isSearchEngine() || $oReverseProxyBackend->isReverseProxyCapableDoEsi()) {
            $oHeader = oxNew('oxHeader');
            $oHeader->setNonCacheable();
            $oHeader->sendHeader();
        }
    }


    /**
     * @inheritdoc
     */
    public function getEditionCacheFilePrefix()
    {
        return 'ee';
    }
}