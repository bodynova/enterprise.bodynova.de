<?php
/**
 * This Software is the property of OXID eSales and is protected
 * by copyright law - it is NOT Freeware.
 *
 * Any unauthorized use of this software without a valid license key
 * is a violation of the license agreement and will be prosecuted by
 * civil and criminal law.
 *
 * @link      http://www.oxid-esales.com
 * @copyright (C) OXID eSales AG 2003-2016
 * @version   OXID eShop EE
 */

namespace OxidEsales\EshopEnterprise\Core;

use oxDb;
use oxException;
use OxidEsales\Eshop\Application\Controller\FrontendController;
use OxidEsales\Eshop\Core\Cache\DynamicContent\ContentCache;
use OxidEsales\EshopEnterprise\Core\Exception\AccessRightException;
use oxRegistry;

/**
 * @inheritdoc
 */
class ShopControl extends \OxidEsales\EshopProfessional\Core\ShopControl
{
    /** @var bool Allow setting env_key cookies for shopcontrol, but not for widget controller. */
    protected $_blAllowEnvKeySetting = true;

    /** @var bool Allow invalidating cache for shopcontrol, but not for widget controller. */
    protected $_blAllowCacheInvalidating = true;

    /** @var bool Is current view loaded from cache */
    protected $viewLoadedFromCache = false;

    /**
     * Sets whether to allow cache invalidation
     *
     * @param bool $allowCacheInvalidating
     */
    public function setAllowCacheInvalidating($allowCacheInvalidating)
    {
        $this->_blAllowCacheInvalidating = $allowCacheInvalidating;
    }

    /**
     * Returns whether cache invalidation is allowed
     *
     * @return bool
     */
    public function getAllowCacheInvalidating()
    {
        return $this->_blAllowCacheInvalidating;
    }

    /**
     * Forms output and returns it.
     * If possible takes from cache, if not - renders page and stores it to cache.
     *
     * @param FrontendController $view
     *
     * @return string
     */
    protected function formOutput($view)
    {
        $output = $this->getFromCache($view);

        $this->viewLoadedFromCache = $output === false;

        if ($this->viewLoadedFromCache) {
            $output = parent::formOutput($view);
            $this->addToCache($view, $output);
        }

        $this->setReverseProxyParameters();

        return $this->processOutput($view, $output);
    }

    /**
     * Checks if current view output is cached. If so - returns it, if not - returns false.
     *
     * @param FrontendController $view
     *
     * @return string|false
     */
    protected function getFromCache($view)
    {
        $output = false;
        if ($view->getIsCallForCache()) {
            $errors = $this->_getErrors($view->getClassName());
            if (is_array($errors) && count($errors)) {
                $view->setIsCallForCache(false);
                $view->initCacheableComponents();
            } else {
                // now we are ready for cache check
                $viewId = $view->getViewId();
                $cacheManager = $this->_getCacheManager();
                $output = $cacheManager->get($viewId);

                // If no cache found - must initiate cacheable components to render template correctly.
                if ($output === false) {
                    $view->initCacheableComponents();
                } else {
                    // get Smarty is important here as it sets template directory correct
                    $smarty = oxRegistry::get("oxUtilsView")->getSmarty();

                    // rendering non cacheable components ...
                    $view->renderNonCacheableComponents();

                    // setting template variables
                    $viewData = $view->getViewData();
                    foreach (array_keys($viewData) as $viewName) {
                        $smarty->assign_by_ref($viewName, $viewData[$viewName]);
                    }
                }
            }
        }

        return $output;
    }

    /**
     * Sending headers for proxy cache.
     * Always send env_key as it is need for getting different cache on next click.
     * Do not use reverse proxy functionality if search engine or need to add sid on links.
     */
    protected function setReverseProxyParameters()
    {
        $utils = oxRegistry::getUtils();
        $reverseProxyCache = $this->_getReverseProxyBackend();
        if ($this->_blAllowEnvKeySetting && $reverseProxyCache->isEnabled()) {
            $reverseProxyCache->setEnvironmentKey();
        }
        if ($reverseProxyCache->isActive()) {
            $session = $this->getSession();
            if ($utils->isSearchEngine() || ($session->isSidNeeded() && !$this->isAdmin())) {
                $reverseProxyCache->setNotActive();
            }
        }
    }

    /**
     * Adds content to cache.
     *
     * @param FrontendController $view
     * @param string         $output
     */
    protected function addToCache($view, $output)
    {
        if ($view->getIsCallForCache()) {
            $this->_getCacheManager()->put($view->getViewId(), $output, $view->getViewResetId());
        }
    }

    /**
     * Changes cached content session information.
     *
     * @param FrontendController $view
     * @param string         $output
     *
     * @return string
     */
    protected function processOutput($view, $output)
    {
        if ($view->getIsCallForCache()) {
            $output = $this->_getCacheManager()->processCache($output);
        }

        return $output;
    }

    /**
     * When reverse proxy cache is active sends correct headers.
     *
     * @param FrontendController $view
     */
    protected function sendAdditionalHeaders($view)
    {
        $utils = oxRegistry::getUtils();
        $reverseProxyCache = $this->_getReverseProxyBackend();
        if ($reverseProxyCache->isEnabled()) {
            if ($this->getAllowCacheInvalidating() || $view->getAllowCacheInvalidating()) {
                //invalidating proxy cache
                $reverseProxyCache->execute();
            }

            $reverseProxyHeader = oxNew('oxReverseProxyHeader');
            if ($view->isCacheable()) {
                $reverseProxyHeader->setCacheAge($view->getCacheLifeTime());
            } else {
                $reverseProxyHeader->setNonCacheable();
            }
            $reverseProxyHeader->setSurrogateHeader();
            $reverseProxyHeader->sendHeader();

        } elseif ($utils->isSearchEngine() || $reverseProxyCache->isReverseProxyCapableDoEsi()) {
            $header = oxNew('oxHeader');
            $header->setNonCacheable();
            $header->sendHeader();
        }
    }

    /**
     * Checking if user has enough rights to execute preferred functionality
     * Initializes non cacheable components if view itself is cached.
     *
     * @param FrontendController $view
     */
    protected function onViewCreation($view)
    {
        if (($rights = $this->getRights())) {
            $rights->processView($view);
        }

        if ($this->getConfig()->getConfigParam('blUseContentCaching') && !$this->isAdmin()) {
            // lets look for cached data
            $cacheManager = $this->_getCacheManager();
            if ($cacheManager->isViewCacheable($view->getClassName()) && $view->canCache()) {
                // initializing non cacheable components
                $view->setIsCallForCache(true);
                $view->initNonCacheableComponents();
            }
        }
    }

    /**
     * @inheritdoc
     */
    protected function _render($view)
    {
        if ($view->getIsCallForCache()) {
            $smarty = oxRegistry::get("oxUtilsView")->getSmarty();
            $smarty->assign('_render4cache', '1');
        }

        return parent::_render($view);
    }

    /**
     * return cache manager instance
     *
     * @return ContentCache
     */
    protected function _getCacheManager()
    {
        if (!$this->_oCache) {
            $this->_oCache = oxNew('oxCache');
        }

        return $this->_oCache;
    }

    /**
     * returns ReverseProxyBackend from Registry
     *
     * @return \OxidEsales\EshopEnterprise\Core\Cache\ReverseProxy\ReverseProxyBackend
     */
    protected function _getReverseProxyBackend()
    {
        return oxRegistry::get('oxReverseProxyBackend');
    }


    /**
     * @inheritdoc
     */
    protected function formMonitorMessage($view)
    {
        $debugInfo = oxNew('oxDebugInfo');
        $viewId = $view->getViewId();

        $message = $debugInfo->formatContentCaching($view->getIsCallForCache(), $this->viewLoadedFromCache, $viewId);
        $message .= $debugInfo->formatReverseProxyActive();

        $message .= parent::formMonitorMessage($view);

        return $message;
    }

    /**
     * Catching other not caught exceptions.
     *
     * @param oxException $exception
     */
    protected function _handleBaseException($exception)
    {
        if ($exception instanceof \OxidEsales\EshopEnterprise\Core\Exception\AccessRightException) {
            $this->_handleAccessRightsException($exception);
        }

        $exception->debugOut();

        if ($this->_isDebugMode()) {
            oxRegistry::get("oxUtilsView")->addErrorToDisplay($exception);
            $this->_process('exceptionError', 'displayExceptionError');
        }
    }

    /**
     * R&R handling -> redirect to error msg, also, can call _process again, specifying error handler view class.
     *
     * @param oxException $exception Exception
     */
    protected function _handleAccessRightsException($exception)
    {
        oxRegistry::getUtils()->redirect($this->getConfig()->getShopHomeUrl() . 'cl=content&tpl=err_accessdenied.tpl', true, 302);
    }

    /**
     * Returns which controller should be loaded at shop start.
     * Check whether we have to display mall start screen or not.
     *
     * @return string
     */
    protected function _getFrontendStartController()
    {
        if ($this->getConfig()->isMall()) {
            $class = $this->_getFrontendMallStartController();
        } else {
            $class = parent::_getFrontendStartController();
        }

        return $class;
    }

    /**
     * Returns start controller class name for frontend mall.
     * If no class specified, we need to change back to base shop
     *
     * @return string
     */
    protected function _getFrontendMallStartController()
    {
        $activeShopsCount = $this->fetchActiveShopCount();

        $mallShopUrl = $this->getConfig()->getConfigParam('sMallShopURL');

        $class = 'start';
        if ($activeShopsCount && $activeShopsCount > 1 && $this->getConfig()->getConfigParam('iMallMode') != 0 && !$mallShopUrl) {
            $class = 'mallstart';
        }

        return $class;
    }

    /**
     * Fetch the number of active shops.
     *
     * @return false|string The number of active shops.
     */
    protected function fetchActiveShopCount()
    {
        return oxDb::getDb()->getOne('select count(*) from oxshops where oxactive = 1');
    }
}
