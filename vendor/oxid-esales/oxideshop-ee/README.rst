OXID eShop Enterprise Edition
=============================

The repository contains OXID eShop Enterprise Edition source code.

Installation
------------

Enterprise edition goes as a composer dependency for the OXID eShop.
Professional Edition package has to be added as a dependency required as well.

Run the following commands to install Enterprise Edition:

.. code ::

    git clone https://github.com/OXID-eSales/oxideshop_ce.git oxideshop
    cd oxideshop
    composer config repositories.oxid-esales/oxideshop-ee vcs https://github.com/OXID-eSales/oxideshop_ee
    composer config repositories.oxid-esales/oxideshop-pe vcs https://github.com/OXID-eSales/oxideshop_pe
    composer require oxid-esales/oxideshop-pe:dev-master
    composer require oxid-esales/oxideshop-ee:dev-master
    
In case Community or Professional edition was configured earlier, the database should 
be upgraded to Enterprise edition too. Please run the ``vendor/bin/reset-shop``, which will 
be available after ``composer install``. The script will install edition database structure 
by your configured edition. Note that your previously installed database will be deleted.

IDE code completion
-------------------

You can easily enable code completion in your IDE by installing `this script <https://github.com/OXID-eSales/eshop-ide-helper>`__ and generating it as described.
