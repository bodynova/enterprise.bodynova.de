<?php
/**
 * This Software is the property of OXID eSales and is protected
 * by copyright law - it is NOT Freeware.
 *
 * Any unauthorized use of this software without a valid license key
 * is a violation of the license agreement and will be prosecuted by
 * civil and criminal law.
 *
 * @link      http://www.oxid-esales.com
 * @copyright (C) OXID eSales AG 2003-2016
 * @version   OXID eShop EE
 */

namespace OxidEsales\EshopEnterprise;

/**
 * @inheritdoc
 */
class ClassMap extends \OxidEsales\EshopCommunity\Core\Edition\ClassMap
{

    /**
     * @inheritdoc
     */
    public function getOverridableMap()
    {
        return [
            'oxadminrights'                       => \OxidEsales\Eshop\Core\AdminRights::class,
            'oxarticle2shoprelations'             => \OxidEsales\Eshop\Core\Article2ShopRelations::class,
            'oxcache'                             => \OxidEsales\Eshop\Core\Cache\DynamicContent\ContentCache::class,
            'oxcachebackenddefault'               => \OxidEsales\Eshop\Core\Cache\DynamicContent\Connector\DefaultCacheConnector::class,
            'oxcachebackendzsdisk'                => \OxidEsales\Eshop\Core\Cache\DynamicContent\Connector\ZendDiskCacheConnector::class,
            'oxcachebackendzsshm'                 => \OxidEsales\Eshop\Core\Cache\DynamicContent\Connector\ZendShmCacheConnector::class,
            'oxcachebackend'                      => \OxidEsales\Eshop\Core\Cache\Generic\Cache::class,
            'oxcacheitem'                         => \OxidEsales\Eshop\Core\Cache\Generic\CacheItem::class,
            'oxfilecacheconnector'                => \OxidEsales\Eshop\Core\Cache\Generic\Connector\FileCacheConnector::class,
            'oxmemcachedcacheconnector'           => \OxidEsales\Eshop\Core\Cache\Generic\Connector\MemcachedCacheConnector::class,
            'oxreverseproxyconnector'             => \OxidEsales\Eshop\Core\Cache\Generic\Connector\ReverseProxyCacheConnector::class,
            'oxzenddiskcacheconnector'            => \OxidEsales\Eshop\Core\Cache\Generic\Connector\ZendDiskCacheConnector::class,
            'oxzendshmcacheconnector'             => \OxidEsales\Eshop\Core\Cache\Generic\Connector\ZendShmCacheConnector::class,
            'oxreverseproxybackend'               => \OxidEsales\Eshop\Core\Cache\ReverseProxy\ReverseProxyBackend::class,
            'oxreverseproxyheader'                => \OxidEsales\Eshop\Core\Cache\ReverseProxy\ReverseProxyHeader::class,
            'oxreverseproxyurlgenerator'          => \OxidEsales\Eshop\Core\Cache\ReverseProxy\ReverseProxyUrlGenerator::class,
            'oxreverseproxyurlpartstoflush'       => \OxidEsales\Eshop\Core\Cache\ReverseProxy\ReverseProxyUrlPartsToFlush::class,
            'oxcategory2shoprelations'            => \OxidEsales\Eshop\Core\Category2ShopRelations::class,
            'oxelement2shoprelations'             => \OxidEsales\Eshop\Core\Element2ShopRelations::class,
            'oxelement2shoprelationsdbgateway'    => \OxidEsales\Eshop\Core\Element2ShopRelationsDbGateway::class,
            'oxelement2shoprelationssqlgenerator' => \OxidEsales\Eshop\Core\Element2ShopRelationsSqlGenerator::class,
            'account_noticelist'                  => \OxidEsales\Eshop\Application\Controller\AccountNoticeList::class,
            'admin_beroles'                       => \OxidEsales\Eshop\Application\Controller\Admin\AdminBackEndRoles::class,
            'admin_feroles'                       => \OxidEsales\Eshop\Application\Controller\Admin\AdminFrontEndRoles::class,
            'admin_mall'                          => \OxidEsales\Eshop\Application\Controller\Admin\AdminMall::class,
            'adminlinks_mall'                     => \OxidEsales\Eshop\Application\Controller\Admin\AdminLinksMall::class,
            'article_mall'                        => \OxidEsales\Eshop\Application\Controller\Admin\ArticleMall::class,
            'article_rights_visible_ajax'         => \OxidEsales\Eshop\Application\Controller\Admin\ArticleRightsVisibleAjax::class,
            'article_rights_buyable_ajax'         => \OxidEsales\Eshop\Application\Controller\Admin\ArticleRightsBuyableAjax::class,
            'article_rights'                      => \OxidEsales\Eshop\Application\Controller\Admin\ArticleRights::class,
            'attribute_mall'                      => \OxidEsales\Eshop\Application\Controller\Admin\AttributeMall::class,
            'oxfield2shop'                        => \OxidEsales\Eshop\Application\Model\Field2Shop::class,
            'oxrights'                            => \OxidEsales\Eshop\Application\Model\Rights::class,
            'oxrole'                              => \OxidEsales\Eshop\Application\Model\Role::class,
            'oxshoprelations'                     => \OxidEsales\Eshop\Application\Model\ShopRelations::class,
            'discount_mall'                       => \OxidEsales\Eshop\Application\Controller\Admin\DiscountMall::class,
            'delivery_mall'                       => \OxidEsales\Eshop\Application\Controller\Admin\DeliveryMall::class,
            'deliveryset_mall'                    => \OxidEsales\Eshop\Application\Controller\Admin\DeliverySetMall::class,
            'category_mall'                       => \OxidEsales\Eshop\Application\Controller\Admin\CategoryMall::class,
            'category_rights'                     => \OxidEsales\Eshop\Application\Controller\Admin\CategoryRights::class,
            'category_rights_buyable_ajax'        => \OxidEsales\Eshop\Application\Controller\Admin\CategoryRightsBuyableAjax::class,
            'category_rights_visible_ajax'        => \OxidEsales\Eshop\Application\Controller\Admin\CategoryRightsVisibleAjax::class,
            'oxelement2shoprelationsservice'      => \OxidEsales\Eshop\Application\Component\Service\Element2ShopRelationsService::class,
            'news_mall'                           => \OxidEsales\Eshop\Application\Controller\Admin\NewsMall::class,
            'manufacturer_mall'                   => \OxidEsales\Eshop\Application\Controller\Admin\ManufacturerMall::class,
            'roles_femain'                        => \OxidEsales\Eshop\Application\Controller\Admin\RolesFrontendMain::class,
            'roles_feuser'                        => \OxidEsales\Eshop\Application\Controller\Admin\RolesFrontendUser::class,
            'selectlist_mall'                     => \OxidEsales\Eshop\Application\Controller\Admin\SelectListMall::class,
            'shop_cache'                          => \OxidEsales\Eshop\Application\Controller\Admin\ShopCache::class,
            'roles_begroups_ajax'                 => \OxidEsales\Eshop\Application\Controller\Admin\RolesBackendGroupsAjax::class,
            'roles_belist'                        => \OxidEsales\Eshop\Application\Controller\Admin\RolesBackendList::class,
            'roles_bemain'                        => \OxidEsales\Eshop\Application\Controller\Admin\RolesBackendMain::class,
            'roles_beobject'                      => \OxidEsales\Eshop\Application\Controller\Admin\RolesBackendObject::class,
            'roles_beuser'                        => \OxidEsales\Eshop\Application\Controller\Admin\RolesBackendUser::class,
            'roles_beuser_ajax'                   => \OxidEsales\Eshop\Application\Controller\Admin\RolesBackendUserAjax::class,
            'roles_fegroups_ajax'                 => \OxidEsales\Eshop\Application\Controller\Admin\RolesFrontendGroupsAjax::class,
            'roles_felist'                        => \OxidEsales\Eshop\Application\Controller\Admin\RolesFrontendList::class,
            'shop_mall'                           => \OxidEsales\Eshop\Application\Controller\Admin\ShopMall::class,
            'vendor_mall'                         => \OxidEsales\Eshop\Application\Controller\Admin\VendorMall::class,
            'voucherserie_mall'                   => \OxidEsales\Eshop\Application\Controller\Admin\VoucherSerieMall::class,
            'wrapping_mall'                       => \OxidEsales\Eshop\Application\Controller\Admin\WrappingMall::class,
            'mallstart'                           => \OxidEsales\Eshop\Application\Controller\MallStartController::class,
            'oxldap'                              => \OxidEsales\Eshop\Core\LDAP::class,
        ];
    }

    /**
     * @inheritdoc
     */
    public function getNotOverridableMap()
    {
        return [
            'oxadminview'                         => \OxidEsales\EshopEnterprise\Application\Controller\Admin\AdminController::class,
            'oxelement2shoprelationsservice'      => \OxidEsales\EshopEnterprise\Application\Component\Service\Element2ShopRelationsService::class,
            'oxshoprelations'                     => \OxidEsales\EshopEnterprise\Application\Model\ShopRelations::class,
            'oxarticle2shoprelations'             => \OxidEsales\EshopEnterprise\Core\Article2ShopRelations::class,
            'oxcategory2shoprelations'            => \OxidEsales\EshopEnterprise\Core\Category2ShopRelations::class,
            'oxelement2shoprelations'             => \OxidEsales\EshopEnterprise\Core\Element2ShopRelations::class,
            'oxelement2shoprelationsdbgateway'    => \OxidEsales\EshopEnterprise\Core\Element2ShopRelationsDbGateway::class,
            'oxelement2shoprelationssqlgenerator' => \OxidEsales\EshopEnterprise\Core\Element2ShopRelationsSqlGenerator::class,
            'oxserial'                            => \OxidEsales\EshopEnterprise\Core\Serial::class,
            'oxadmindetails'                      => \OxidEsales\EshopEnterprise\Application\Controller\Admin\AdminDetailsController::class,
            'ajaxlistcomponent'                   => \OxidEsales\EshopEnterprise\Application\Controller\Admin\ListComponentAjax::class,
            'article_list'                        => \OxidEsales\EshopEnterprise\Application\Controller\Admin\ArticleList::class,
            'oxshopidcalculator'                  => \OxidEsales\EshopEnterprise\Core\ShopIdCalculator::class,
            'oxutilsobject'                       => \OxidEsales\EshopEnterprise\Core\UtilsObject::class,
            'oxaccessrightexception'              => \OxidEsales\EshopEnterprise\Core\Exception\AccessRightException::class,

            'oxicachebackend'                     => \OxidEsales\EshopEnterprise\Application\Model\Contract\CacheBackendInterface::class,
            'oxicacheconnector'                   => \OxidEsales\EshopEnterprise\Application\Model\Contract\CacheConnectorInterface::class,
        ];
    }
}
