<?php
/**
 * This Software is the property of OXID eSales and is protected
 * by copyright law - it is NOT Freeware.
 *
 * Any unauthorized use of this software without a valid license key
 * is a violation of the license agreement and will be prosecuted by
 * civil and criminal law.
 *
 * @link      http://www.oxid-esales.com
 * @copyright (C) OXID eSales AG 2003-2016
 * @version   OXID eShop EE
 */

namespace OxidEsales\EshopEnterprise\Setup;

/**
 * @inheritdoc
 */
class Setup extends \OxidEsales\EshopProfessional\Setup\Setup
{
    /**
     * @inheritdoc
     */
    public function getDefaultSerial()
    {
        return 'EF7FV-B9TA8-3R3SD-MZNU4-7NWM3-AN7AU';
    }

    /**
     * @inheritdoc
     */
    public function getShopId()
    {
        return '1';
    }
}
