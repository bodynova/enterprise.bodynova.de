<?php
/**
 * This Software is the property of OXID eSales and is protected
 * by copyright law - it is NOT Freeware.
 *
 * Any unauthorized use of this software without a valid license key
 * is a violation of the license agreement and will be prosecuted by
 * civil and criminal law.
 *
 * @link      http://www.oxid-esales.com
 * @copyright (C) OXID eSales AG 2003-2016
 * @version   OXID eShop EE
 */

namespace OxidEsales\EshopEnterprise\Application\Model;

use oxRegistry;

/**
 * This class handles relations between shops.
 *
 * For example: article (or other elements) are inherited by shop from parent shop.
 *
 * @internal Do not make a module extension for this class.
 * @see      http://wiki.oxidforge.org/Tutorials/Core_OXID_eShop_classes:_must_not_be_extended
 */
class ShopRelations
{
    /** @var string Shop Id. */
    private $_sShopId = null;

    /** @var string Element type (table name to add to shop). */
    private $_sElementType = null;

    /** @var bool Multi Shop flag. */
    private $_blIsMultiShopType = false;

    /**
     * Constructor class.
     *
     * @param string $shopId Shop id
     */
    public function __construct($shopId)
    {
        $this->setShopId($shopId);
    }

    /**
     * Sets shop ID.
     *
     * @param string $shopId Shop ID.
     */
    public function setShopId($shopId)
    {
        $this->_sShopId = $shopId;
    }

    /**
     * Gets shop ID.
     *
     * @return string
     */
    public function getShopId()
    {
        return $this->_sShopId;
    }

    /**
     * Sets multi shop flag.
     *
     * @param bool $isMultiShopType Is multi shop flag.
     */
    public function setIsMultiShopType($isMultiShopType)
    {
        $this->_blIsMultiShopType = $isMultiShopType;
    }

    /**
     * Gets multi shop flag.
     *
     * @return bool
     */
    public function isMultiShopType()
    {
        return $this->_blIsMultiShopType;
    }

    /**
     * Sets Element type - table name of element.
     *
     * @param string $elementType Type (table name) of Element to add to shop
     */
    public function setElementType($elementType)
    {
        $this->_sElementType = $elementType;
    }

    /**
     * Gets Element type - table name of element.
     *
     * @return string
     */
    public function getElementType()
    {
        return $this->_sElementType;
    }

    /**
     * Checks if given element is inherited from parent shop.
     *
     * @param string $elementName The name of element to check.
     *
     * @return bool
     */
    public function isShopElementInherited($elementName)
    {
        if ($this->isMultiShopType()) {
            $isInherited = $this->_isMultiShopElementInherited($elementName);
        } else {
            $isInherited = $this->_isSubShopElementInherited($elementName);
        }

        return $isInherited;
    }

    /**
     * Checks if given element is inherited from parent shop.
     *
     * @param string $elementName The name of element to check.
     *
     * @return bool
     */
    protected function _isSubShopElementInherited($elementName)
    {
        $isInherited = false;
        if ($elementName == 'oxcategories') {
            return $isInherited;
        }

        $shopId = $this->getShopId();

        $variableValue = oxRegistry::getConfig()->getShopConfVar('blMallInherit_' . strtolower($elementName), $shopId);
        if (isset($variableValue)) {
            $isInherited = $variableValue;
        }

        return $isInherited;
    }

    /**
     * Checks if given element is inherited from all shop.
     * For Multi shops all elements are inherited by default except of categories.
     * If element name is oxcategories it checks for the option.
     *
     * @param string $elementName The name of element to check.
     *
     * @return bool
     */
    protected function _isMultiShopElementInherited($elementName)
    {
        $isInherited = true;

        if ($elementName == 'oxcategories') {
            $isInherited = false;
            $shopId = $this->getShopId();
            $variableValue = oxRegistry::getConfig()->getShopConfVar('blMultishopInherit_oxcategories', $shopId);
            if (isset($variableValue)) {
                $isInherited = $variableValue;
            }
        }

        return $isInherited;
    }
}
