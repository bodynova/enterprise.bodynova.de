<?php
/**
 * This Software is the property of OXID eSales and is protected
 * by copyright law - it is NOT Freeware.
 *
 * Any unauthorized use of this software without a valid license key
 * is a violation of the license agreement and will be prosecuted by
 * civil and criminal law.
 *
 * @link      http://www.oxid-esales.com
 * @copyright (C) OXID eSales AG 2003-2016
 * @version   OXID eShop EE
 */

namespace OxidEsales\EshopEnterprise\Application\Model;

use oxRegistry;
use oxArticle;

/**
 * @inheritdoc
 */
class AmountPriceList extends \OxidEsales\EshopProfessional\Application\Model\AmountPriceList
{
    /**
     * Load category list data
     *
     * @param oxArticle $article Article
     */
    public function load($article)
    {
        $this->setArticle($article);

        $genericCache = $this->_getCacheBackend();
        $data = $genericCache->isActive() ? $this->_loadFromCache() : $this->_loadFromDb();

        $this->assignArray($data);
    }

    /**
     * Generate cache key for category tree for current shop and language
     *
     * @return string
     */
    public function getCacheKey()
    {
        return 'oxArticle_' . $this->getArticle()->getId() . '_oxAmountPrice_' . $this->getConfig()->getShopId();
    }

    /**
     * Generate cache keys for dependent cached data.
     *
     * @param string $articleId article id
     * @param array  $shopIds   shop ids array
     *
     * @return string
     */
    public function getCacheKeys($articleId, $shopIds = null)
    {
        $cacheKeys = array();
        $shopIds = $shopIds ? $shopIds : $this->getConfig()->getShopIds();

        foreach ($shopIds as $shopId) {
            $cacheKeys[] = 'oxArticle_' . $articleId . '_oxAmountPrice_' . $shopId;
        }

        return $cacheKeys;
    }


    /**
     * Execute cache dependencies
     *
     * @param string $articleId article id
     *
     * @return null
     */
    public function executeDependencyEvent($articleId)
    {
        $genericCache = $this->_getCacheBackend();

        if ($genericCache->isActive()) {
            $genericCache->invalidate($this->getCacheKeys($articleId));
        }
    }

    /**
     * Load data from cache
     *
     * @return array
     */
    protected function _loadFromCache()
    {
        $genericCache = $this->_getCacheBackend();
        $cacheKey = $this->getCacheKey();
        $cacheItem = $genericCache->get($cacheKey);

        if ($cacheItem) {
            $data = $cacheItem->getData();
        } else {
            $data = $this->_loadFromDb();
            $cacheItem = oxNew('oxCacheItem');
            $cacheItem->setData($data);
            $genericCache->set($cacheKey, $cacheItem);
        }

        return $data;
    }

    /**
     * returns Generic\Cache from Registry
     *
     * @return \OxidEsales\Eshop\Core\Cache\Generic\Cache
     */
    protected function _getCacheBackend()
    {
        return oxRegistry::get('oxCacheBackend');
    }
}
