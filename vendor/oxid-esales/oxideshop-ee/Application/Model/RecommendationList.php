<?php
/**
 * This Software is the property of OXID eSales and is protected
 * by copyright law - it is NOT Freeware.
 *
 * Any unauthorized use of this software without a valid license key
 * is a violation of the license agreement and will be prosecuted by
 * civil and criminal law.
 *
 * @link      http://www.oxid-esales.com
 * @copyright (C) OXID eSales AG 2003-2016
 * @version   OXID eShop EE
 */

namespace OxidEsales\EshopEnterprise\Application\Model;

/**
 * @inheritdoc
 */
class RecommendationList extends \OxidEsales\EshopProfessional\Application\Model\RecommendationList
{
    /**
     * Execute cache dependencies
     */
    public function executeDependencyEvent()
    {
        // @deprecated since v5.3 (2016-06-17); Listmania will be moved to an own module.
        // Proxy cache dependencies.
        $cache = $this->_getReverseProxyBackend();
        if ($cache->isEnabled()) {
            $proxyCacheUrls = oxNew('oxReverseProxyUrlGenerator');

            // Widgets.
            $proxyCacheUrls->setWidget('oxwrecommendation');
            // Recommendation lists.
            $proxyCacheUrls->setDynamicPage('recommlist', $this->getId());

            $cache->set($proxyCacheUrls->getUrls());
        }
        // END deprecated
    }

    /**
     * @inheritdoc
     */
    public function removeArticle($oxId)
    {
        if ($oxId) {
            $this->executeDependencyEvent();
        }
        parent::removeArticle($oxId);
    }

    /**
     * @inheritdoc
     */
    public function addArticle($oxId, $description)
    {
        if ($oxId) {
            $this->executeDependencyEvent();
        }

        return parent::addArticle($oxId, $description);
    }

    /**
     * @inheritdoc
     */
    protected function onSave()
    {
        parent::onSave();
        $this->executeDependencyEvent();
    }

    /**
     * @inheritdoc
     */
    protected function onDelete()
    {
        parent::onDelete();
        $this->executeDependencyEvent();
    }

    /**
     * returns ReverseProxyBackend from Registry
     *
     * @return \OxidEsales\Eshop\Core\Cache\ReverseProxy\ReverseProxyBackend
     */
    protected function _getReverseProxyBackend()
    {
        return \oxRegistry::get('oxReverseProxyBackend');
    }
}
