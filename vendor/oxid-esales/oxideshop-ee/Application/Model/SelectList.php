<?php
/**
 * This Software is the property of OXID eSales and is protected
 * by copyright law - it is NOT Freeware.
 *
 * Any unauthorized use of this software without a valid license key
 * is a violation of the license agreement and will be prosecuted by
 * civil and criminal law.
 *
 * @link      http://www.oxid-esales.com
 * @copyright (C) OXID eSales AG 2003-2016
 * @version   OXID eShop EE
 */

namespace OxidEsales\EshopEnterprise\Application\Model;

use OxidEsales\Eshop\Core\Cache\ReverseProxy\ReverseProxyBackend;
use oxRegistry;
use oxDb;

/**
 * @inheritdoc
 */
class SelectList extends \OxidEsales\EshopProfessional\Application\Model\SelectList
{
    /**
     * Returns ReverseProxyBackend from Registry.
     *
     * @return ReverseProxyBackend
     */
    protected function _getReverseProxyBackend()
    {
        return oxRegistry::get('oxReverseProxyBackend');
    }

    /**
     * Returns article ids assigned to discount.
     *
     * @return array
     */
    public function getArticleIds()
    {
        return oxDb::getDb()->getCol("SELECT `oxobjectid` FROM `oxobject2selectlist` WHERE `oxselnid` = '" . $this->getId() . "'");
    }


    /**
     * Execute cache dependencies.
     */
    public function executeDependencyEvent()
    {
        // proxy cache dependencies
        $cache = $this->_getReverseProxyBackend();
        if ($cache->isEnabled()) {
            $proxyCacheUrls = oxNew('oxReverseProxyUrlGenerator');
            $articleIds = $this->getArticleIds();
            foreach ($articleIds as $articleId) {
                $proxyCacheUrls->setWidget('oxwarticlebox', array('anid' => $articleId));
                $proxyCacheUrls->setWidget('oxwarticledetails', array('anid' => $articleId));
            }
            $cache->set($proxyCacheUrls->getUrls());
        }
    }

    /**
     * Save this Object to database, insert or update as needed.
     *
     * @return string|bool
     */
    public function save()
    {
        $isSaved = parent::save();
        $this->executeDependencyEvent();

        return $isSaved;
    }
}
