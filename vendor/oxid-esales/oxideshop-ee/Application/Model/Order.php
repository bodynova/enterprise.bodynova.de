<?php
/**
 * This Software is the property of OXID eSales and is protected
 * by copyright law - it is NOT Freeware.
 *
 * Any unauthorized use of this software without a valid license key
 * is a violation of the license agreement and will be prosecuted by
 * civil and criminal law.
 *
 * @link      http://www.oxid-esales.com
 * @copyright (C) OXID eSales AG 2003-2016
 * @version   OXID eShop EE
 */

namespace OxidEsales\EshopEnterprise\Application\Model;

/**
 * @inheritdoc
 */
class Order extends \OxidEsales\EshopProfessional\Application\Model\Order
{
    /**
     * @inheritdoc
     */
    public function assign($dbRecord)
    {
        if (!$this->canRead()) {
            return false;
        }

        parent::assign($dbRecord);
    }

    /**
     * @inheritdoc
     */
    protected function _setUser($user)
    {
        parent::_setUser($user);
        $this->oxorder__oxbillustidstatus = clone $user->oxuser__oxustidstatus;
    }

    /**
     * @inheritdoc
     */
    public function delete($oxid = null)
    {
        if ($oxid) {
            if (!$this->load($oxid)) {
                // such order does not exist
                return false;
            }
        } elseif (!$oxid) {
            $oxid = $this->getId();
        }

        if (!$this->canDelete($oxid)) {
            return false;
        }

        return parent::delete($oxid);
    }

    /**
     * @inheritdoc
     */
    public function getOrderUser()
    {
        parent::getOrderUser();

        if ($this->_oUser && $this->_isLoaded) {
            $this->_oUser->oxuser__oxustidstatus = clone $this->oxorder__oxbillustidstatus;
        }

        return $this->_oUser;
    }
}
