<?php
/**
 * This Software is the property of OXID eSales and is protected
 * by copyright law - it is NOT Freeware.
 *
 * Any unauthorized use of this software without a valid license key
 * is a violation of the license agreement and will be prosecuted by
 * civil and criminal law.
 *
 * @link      http://www.oxid-esales.com
 * @copyright (C) OXID eSales AG 2003-2016
 * @version   OXID eShop EE
 */

namespace OxidEsales\EshopEnterprise\Application\Model;

use \oxRegistry;

/**
 * @inheritdoc
 */
class Vendor extends \OxidEsales\EshopProfessional\Application\Model\Vendor
{
    /**
     * @inheritdoc
     */
    public function delete($oxid = null)
    {
        $this->executeDependencyEvent();

        return parent::delete($oxid);
    }

    /**
     * returns ReverseProxyBackend from Registry
     *
     * @return \OxidEsales\Eshop\Core\Cache\ReverseProxy\ReverseProxyBackend
     */
    protected function _getReverseProxyBackend()
    {
        return oxRegistry::get('oxReverseProxyBackend');
    }

    /**
     * Execute cache dependencies
     *
     * @return null
     */
    public function executeDependencyEvent()
    {
        // proxy cache dependencies
        $cache = $this->_getReverseProxyBackend();
        if ($cache->isEnabled()) {
            $proxyUrlGenerator = oxNew('oxReverseProxyUrlGenerator');

            //widgets
            $proxyUrlGenerator->setWidget('oxwvendorlist');

            //business object urls
            $proxyUrlGenerator->setObject('oxvendor', $this->getId());

            $cache->set($proxyUrlGenerator->getUrls());
        }
    }

    /**
     * Save this Object to database, insert or update as needed.
     *
     * @return mixed
     */
    public function save()
    {
        $parentResult = parent::save();
        $this->executeDependencyEvent();

        return $parentResult;
    }
}
