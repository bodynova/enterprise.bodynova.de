<?php
/**
 * This Software is the property of OXID eSales and is protected
 * by copyright law - it is NOT Freeware.
 *
 * Any unauthorized use of this software without a valid license key
 * is a violation of the license agreement and will be prosecuted by
 * civil and criminal law.
 *
 * @link      http://www.oxid-esales.com
 * @copyright (C) OXID eSales AG 2003-2016
 * @version   OXID eShop EE
 */

namespace OxidEsales\EshopEnterprise\Application\Model;

/**
 * @inheritdoc
 */
class Object2Category extends \OxidEsales\EshopProfessional\Application\Model\Object2Category
{
    /**
     * Adds object to related map for inherited subshops
     */
    protected function _addElement2ShopRelations()
    {
        $aShopIds = $this->_getInheritanceGroup();
        if (count($aShopIds) > 1) {
            array_shift($aShopIds);
            $oElement2ShopRelations = $this->_getElement2ShopRelations();
            $oElement2ShopRelations->setShopIds($aShopIds);
            $oElement2ShopRelations->addObjectToShop($this);
        }
    }

    /**
     * Returns a list of subshop ids, including the parent.
     * If it is a supershop it checks for all subshops that assigned article and category is in.
     * If not it checks for inherited subshops and if the config option for the bulk inheritance is on.
     * The items are considered inherited in case it is inherited directly to the subshop, recursive check is performed
     * for subsequent subshops.
     *
     * @return array
     */
    protected function _getInheritanceGroup()
    {
        $oShop = $this->getConfig()->getActiveShop();
        if ($oShop->isSuperShop()) {
            $aShopIds = $this->_getInheritanceGroupForSuperShop();
        } else {
            $sCoreTable = $this->getCoreTableName();
            $aShopIds = $oShop->getInheritanceGroup($sCoreTable);
        }

        return $aShopIds;
    }

    /**
     * Returns a id's list of subshop, in which assigned product and category are.
     *
     * @return array
     */
    protected function _getInheritanceGroupForSuperShop()
    {
        $aProductShopIds = $this->_getProductShopIds();
        $aCategoryShopIds = $this->_getCategoryShopIds();

        $aShopIds = array_intersect($aProductShopIds, $aCategoryShopIds);

        return $aShopIds;
    }

    /**
     * Returns shop id's of assigned product
     *
     * @return array
     */
    protected function _getProductShopIds()
    {
        $oArticle = oxNew('oxArticle');
        $oArticle->load($this->getProductId());
        $aProductShopIds = $oArticle->getItemAssignedShopIds();

        return $aProductShopIds;
    }

    /**
     * Returns shop id's of assigned category
     *
     * @return array
     */
    protected function _getCategoryShopIds()
    {
        $oCategory = oxNew('oxCategory');
        $oCategory->load($this->getCategoryId());
        $aCategoryShopIds = $oCategory->getItemAssignedShopIds();

        return $aCategoryShopIds;
    }
}
