<?php
/**
 * This Software is the property of OXID eSales and is protected
 * by copyright law - it is NOT Freeware.
 *
 * Any unauthorized use of this software without a valid license key
 * is a violation of the license agreement and will be prosecuted by
 * civil and criminal law.
 *
 * @link      http://www.oxid-esales.com
 * @copyright (C) OXID eSales AG 2003-2016
 * @version   OXID eShop EE
 */

namespace OxidEsales\EshopEnterprise\Application\Model\Contract;

/**
 * Cache backend interface
 * defines basic cache operations
 */
interface CacheBackendInterface
{

    /**
     * sets cache ttl in seconds
     *
     * @param int $timeToLive cache timeout value in seconds
     *
     * @return null
     */
    public function cacheSetTTL($timeToLive);

    /**
     * Returns cache data. If data is not found - returns false
     *
     * @param string $id cache id
     *
     * @return mixed
     */
    public function cacheGet($id);

    /**
     * Stores cache data, returns storing status
     *
     * @param string $id      cache id
     * @param string $content cache data
     *
     * @return mixed
     */
    public function cachePut($id, $content);

    /**
     * Removes cache according to cache key, returns removal status
     *
     * @param string $id cache key
     *
     * @return null
     */
    public function cacheRemoveKey($id);

    /**
     * Removes all cache entries if possible by backend
     *
     * @return null
     */
    public function cacheClear();

    /**
     * check if this backend is available
     *
     * @return bool
     */
    public static function isAvailable();
}
