<?php
/**
 * This Software is the property of OXID eSales and is protected
 * by copyright law - it is NOT Freeware.
 *
 * Any unauthorized use of this software without a valid license key
 * is a violation of the license agreement and will be prosecuted by
 * civil and criminal law.
 *
 * @link      http://www.oxid-esales.com
 * @copyright (C) OXID eSales AG 2003-2016
 * @version   OXID eShop EE
 */

namespace OxidEsales\EshopEnterprise\Application\Model;

use oxRegistry;

/**
 * @inheritdoc
 */
class Manufacturer extends \OxidEsales\EshopProfessional\Application\Model\Manufacturer
{
    /**
     * @inheritdoc
     * Call cache flushing event.
     */
    public function delete($sOXID = null)
    {
        $this->executeDependencyEvent();

        return parent::delete($sOXID);
    }

    /**
     * @inheritdoc
     * Call cache flushing event.
     */
    public function save()
    {
        $this->executeDependencyEvent();
        $blSaved = parent::save();

        return $blSaved;
    }

    /**
     * Set pages to be flushed to cache.
     *
     * @return null
     */
    public function executeDependencyEvent()
    {
        // proxy cache dependencies
        $oCache = $this->_getReverseProxyBackend();
        if ($oCache->isEnabled()) {
            $oProxyCacheUrls = oxNew('oxReverseProxyUrlGenerator');

            //widgets
            $oProxyCacheUrls->setWidget('oxwmanufacturerlist');

            //pages
            $oProxyCacheUrls->setStartPage();

            //business object urls
            $oProxyCacheUrls->setObject('oxmanufacturer', $this->getId());

            $oCache->set($oProxyCacheUrls->getUrls());
        }
    }

    /**
     * Returns ReverseProxyBackend from Registry.
     *
     * @return \OxidEsales\Eshop\Core\Cache\ReverseProxy\ReverseProxyBackend
     */
    protected function _getReverseProxyBackend()
    {
        return oxRegistry::get('oxReverseProxyBackend');
    }
}
