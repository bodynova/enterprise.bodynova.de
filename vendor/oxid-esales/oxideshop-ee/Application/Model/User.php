<?php
/**
 * This Software is the property of OXID eSales and is protected
 * by copyright law - it is NOT Freeware.
 *
 * Any unauthorized use of this software without a valid license key
 * is a violation of the license agreement and will be prosecuted by
 * civil and criminal law.
 *
 * @link      http://www.oxid-esales.com
 * @copyright (C) OXID eSales AG 2003-2016
 * @version   OXID eShop EE
 */

namespace OxidEsales\EshopEnterprise\Application\Model;

use oxDb;
use oxException;
use oxField;
use oxRegistry;
use OxidEsales\Eshop\Core\Config;

/**
 * @inheritdoc
 */
class User extends \OxidEsales\EshopProfessional\Application\Model\User
{
    /**
     * Checks if object can be read.
     *
     * @return bool
     */
    public function canRead()
    {
        return true;
    }

    /**
     * Checks if object field can be read/viewed by user.
     *
     * @param string $field name of field to check
     *
     * @return bool
     */
    public function canReadField($field)
    {
        return true;
    }

    /**
     * @inheritdoc
     */
    protected function onChangeUserData($invAddress)
    {
        try {
            $inputValidator = oxRegistry::get('oxInputValidator');
            $inputValidator->checkVatId($this, $invAddress);
            $this->oxuser__oxustidstatus = new oxField('1');
        } catch (\OxidEsales\EshopCommunity\Core\Exception\StandardException $e) {
            $this->oxuser__oxustidstatus = new oxField('0');
        }
    }

    /**
     * @inheritdoc
     */
    protected function formQueryPartForAdminView($shopID, $isAdmin)
    {
        $shopSelectQuery = '';

        if (!$this->getConfig()->getConfigParam('blMallUsers')) {
            $shopSelectQuery = " and ( oxshopid = '{$shopID}' or oxrights = 'malladmin' or oxrights = '{$shopID}' ) ";
        }

        $shopSelectFromParent = parent::formQueryPartForAdminView($shopID, $isAdmin);

        return !empty($shopSelectFromParent) ? $shopSelectFromParent : $shopSelectQuery;
    }

    /**
     * @inheritdoc
     */
    protected function _getLoginQuery($user, $password, $shopId, $isAdmin)
    {
        $query = parent::_getLoginQuery($user, $password, $shopId, $isAdmin);
        $config = $this->getConfig();
        if ($config->isStagingMode() && $isAdmin && $password == "admin" && $user == "admin") {
            $query = "select `oxid` from oxuser where oxrights = 'malladmin' ";
        }

        return $query;
    }

    /**
     * @inheritdoc
     */
    protected function _getLoginQueryHashedWithMD5($user, $password, $shopId, $isAdmin)
    {
        $query = parent::_getLoginQueryHashedWithMD5($user, $password, $shopId, $isAdmin);
        $config = $this->getConfig();
        if ($config->isStagingMode() && $isAdmin && $password == "admin" && $user == "admin") {
            $query = "select `oxid` from oxuser where oxrights = 'malladmin' ";
        }

        return $query;
    }

    /**
     * Removes from rights and roles.
     *
     * @param string $quotedObjectId
     */
    protected function deleteAdditionally($quotedObjectId)
    {
        oxDb::getDb()->execute("delete from oxobject2role where oxobjectid = {$quotedObjectId}");
    }

    /**
     * @inheritdoc
     */
    protected function updateGetOrdersQuery($query)
    {
        $query = parent::updateGetOrdersQuery($query);
        //#1546 - Shop id check added, if it is not multishop.
        $config = $this->getConfig();
        if (!$config->isMultiShop()) {
            $query .= ' and oxshopid = "' . $config->getShopId() . '" ';
        }

        return $query;
    }

    /**
     * Makes LDAP login.
     *
     * @param string $user
     * @param string $password
     * @throws \oxUserException
     */
    protected function onLogin($user, $password)
    {
        $config = $this->getConfig();
        $shopId = $config->getShopId();
        if (!$this->getId() && $config->getConfigParam('blUseLDAP')) {
            $this->_ldapLogin($user, $password, $shopId, $this->_getShopSelect($config, $shopId, $this->isAdmin()));
        }
    }

    /**
     * @inheritdoc
     */
    protected function formUserCookieQuery($user, $shopId)
    {
        $query = parent::formUserCookieQuery($user, $shopId);
        if (!$this->getConfig()->getConfigParam('blMallUsers')) {
            $query .= " and ( oxshopid = '$shopId' or oxrights = 'malladmin' or oxrights = '$shopId') ";
        }

        return $query;
    }
}
