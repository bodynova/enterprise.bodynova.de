<?php
/**
 * This Software is the property of OXID eSales and is protected
 * by copyright law - it is NOT Freeware.
 *
 * Any unauthorized use of this software without a valid license key
 * is a violation of the license agreement and will be prosecuted by
 * civil and criminal law.
 *
 * @link      http://www.oxid-esales.com
 * @copyright (C) OXID eSales AG 2003-2016
 * @version   OXID eShop EE
 */

namespace OxidEsales\EshopEnterprise\Application\Model;

use oxRegistry;

/**
 * @inheritdoc
 *
 * @deprecated since v.5.3.0 (2016-06-17); The Admin Menu: Customer Info -> News feature will be moved to a module in v6.0.0
 */
class News extends \OxidEsales\EshopProfessional\Application\Model\News
{
    /**
     * @inheritdoc
     * Check if has rights to work with this object.
     */
    public function assign($dbRecord)
    {
        if (!$this->canRead()) {
            return false;
        }

        parent::assign($dbRecord);
    }

    /**
     * @inheritdoc
     * Call cache flushing event.
     */
    public function delete($sOxid = null)
    {
        if (!$sOxid) {
            $sOxid = $this->getId();
        }
        if (!$sOxid) {
            return false;
        }

        $result = parent::delete($sOxid);

        $this->executeDependencyEvent();

        return $result;
    }

    /**
     * @inheritdoc
     * Call cache flushing event.
     */
    protected function _update()
    {
        parent::_update();
        $this->executeDependencyEvent();
    }

    /**
     * @inheritdoc
     * Call cache flushing event.
     */
    protected function _insert()
    {
        $result = parent::_insert();

        $this->executeDependencyEvent();

        return $result;
    }

    /**
     * Set pages to be flushed to cache.
     */
    public function executeDependencyEvent()
    {
        // proxy cache dependencies
        $oCache = $this->_getReverseProxyBackend();
        if ($oCache->isEnabled()) {
            $oProxyCacheUrls = oxNew('oxReverseProxyUrlGenerator');

            //pages
            $oProxyCacheUrls->setStartPage();
            $oProxyCacheUrls->setStaticPage('news');

            $oCache->set($oProxyCacheUrls->getUrls());
        }
    }

    /**
     * Returns ReverseProxyBackend from Registry.
     *
     * @return \OxidEsales\Eshop\Core\Cache\ReverseProxy\ReverseProxyBackend
     */
    protected function _getReverseProxyBackend()
    {
        return oxRegistry::get('oxReverseProxyBackend');
    }
}
