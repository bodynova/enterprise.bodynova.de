<?php
/**
 * This Software is the property of OXID eSales and is protected
 * by copyright law - it is NOT Freeware.
 *
 * Any unauthorized use of this software without a valid license key
 * is a violation of the license agreement and will be prosecuted by
 * civil and criminal law.
 *
 * @link      http://www.oxid-esales.com
 * @copyright (C) OXID eSales AG 2003-2016
 * @version   OXID eShop EE
 */

namespace OxidEsales\EshopEnterprise\Application\Model;

use \oxDb;

/**
 * @inheritdoc
 */
class NewsSubscribed extends \OxidEsales\EshopProfessional\Application\Model\NewsSubscribed
{
    /**
     * Is mall user
     *
     * @var bool
     */
    protected $_blMallUsers = false;

    /**
     * Class constructor, initiates parent constructor (parent::oxBase()).
     */
    public function __construct()
    {
        $this->setMallUsers($this->getConfig()->getConfigParam('blMallUsers'));
        parent::__construct();
    }

    /**
     * Mall user status setter
     *
     * @param bool $blMallUsers Is mall user
     */
    public function setMallUsers($blMallUsers)
    {
        $this->_blMallUsers = $blMallUsers;
    }

    /**
     * @inheritdoc
     */
    protected function getSubscribedUserIdByEmail($email)
    {
        $database = oxDb::getDb();
        $quotedEmail = $database->quote($email);
        $quotedShopId = $database->quote($this->getConfig()->getShopId());

        if ($this->_blMallUsers) {
            $userOxid = $database->getOne("select oxid from oxnewssubscribed where oxemail = {$quotedEmail} and oxshopid = $quotedShopId");
        } else {
            $userOxid = $database->getOne("select oxnewssubscribed.oxid from oxnewssubscribed left join oxuser on oxnewssubscribed.oxuserid=oxuser.oxid where oxnewssubscribed.oxemail = $quotedEmail and oxuser.oxshopid = $quotedShopId and oxnewssubscribed.oxshopid = $quotedShopId");
        }

        return $userOxid;
    }
}
