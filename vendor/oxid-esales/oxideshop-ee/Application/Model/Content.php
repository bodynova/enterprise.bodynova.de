<?php
/**
 * This Software is the property of OXID eSales and is protected
 * by copyright law - it is NOT Freeware.
 *
 * Any unauthorized use of this software without a valid license key
 * is a violation of the license agreement and will be prosecuted by
 * civil and criminal law.
 *
 * @link      http://www.oxid-esales.com
 * @copyright (C) OXID eSales AG 2003-2016
 * @version   OXID eShop EE
 */

namespace OxidEsales\EshopEnterprise\Application\Model;

use \oxDb;
use \oxRegistry;

/**
 * @inheritdoc
 */
class Content extends \OxidEsales\EshopProfessional\Application\Model\Content
{
    /**
     * returns oxCacheBackend from Registry
     *
     * @return oxCacheBackend
     */
    protected function _getCacheBackend()
    {
        return oxRegistry::get('oxCacheBackend');
    }

    /**
     * returns ReverseProxyBackend from Registry
     *
     * @return \OxidEsales\Eshop\Core\Cache\ReverseProxy\ReverseProxyBackend
     */
    protected function _getReverseProxyBackend()
    {
        return oxRegistry::get('oxReverseProxyBackend');
    }

    /**
     * Load data from cache
     *
     * @param string $sOXID id
     *
     * @return array
     */
    protected function _loadFromCache($sOXID)
    {
        $oCache = $this->_getCacheBackend();
        $sKey = $this->getCacheKey($sOXID);
        $oCacheItem = $oCache->get($sKey);

        if ($oCacheItem) {
            $aData = $oCacheItem->getData();
        } else {
            $aData = $this->_loadFromDb($sOXID);
            $oCacheItem = oxNew('oxCacheItem');
            $oCacheItem->setData($aData);
            $oCache->set($sKey, $oCacheItem);
        }

        return $aData;
    }

    /**
     * Generate cache key
     *
     * @param string $sLoadId load id
     *
     * @return string
     */
    public function getCacheKey($sLoadId = null)
    {
        if (!$sLoadId) {
            // When load indent don't set load it from db, because on savewe can change it
            $sLoadId = oxDb::getDb()->getOne("SELECT `oxloadid` FROM `oxcontents` WHERE `oxid` = " . oxDb::getDb()->quote($this->getId()));
        }

        return 'oxContent_' . $sLoadId . '_' . $this->getConfig()->getShopId() . '_' . oxRegistry::getLang()->getLanguageAbbr($this->getLanguage());
    }

    /**
     * @inheritdoc
     */
    public function loadByIdent($sLoadId)
    {
        $oCache = $this->_getCacheBackend();

        if ($oCache->isActive()) {
            $aData = $this->_loadFromCache($sLoadId);
            if ($aData) {
                $this->assign($aData);
                return true;
            }
        } else {
            return parent::loadByIdent($sLoadId);
        }

        return false;
    }

    /**
     * Execute cache dependencies.
     */
    public function executeDependencyEvent()
    {
        $this->_updateSelfDependencies();
        $this->_updateContentListDependencies();
    }

    /**
     * Execute cache dependencies by self.
     */
    public function _updateSelfDependencies()
    {
        $oCache = $this->_getCacheBackend();
        if ($oCache->isActive()) {
            //invalidate cache
            $oCache->invalidate($this->getCacheKey());
        }

        // proxy cache dependencies
        $oCache = $this->_getReverseProxyBackend();
        if ($oCache->isEnabled()) {
            $oProxyCacheUrls = oxNew('oxReverseProxyUrlGenerator');

            //widgets
            $oProxyCacheUrls->setWidget('oxwinformationlist');
            $oProxyCacheUrls->setWidget('oxwCategoryTree');

            //business object urls
            $oProxyCacheUrls->setObject('oxcontent', $this->getId());

            $oCache->set($oProxyCacheUrls->getUrls());
        }
    }

    /**
     * Execute cache dependencies.
     */
    public function _updateContentListDependencies()
    {
        $oCache = $this->_getCacheBackend();
        if ($oCache->isActive()) {
            //category tree cache dependency
            $oList = oxNew('oxContentList');
            $oList->executeDependencyEvent($this->getType());
        }
    }

    /**
     * @inheritdoc
     */
    public function assign($dbRecord)
    {
        if (!$this->canRead()) {
            return false;
        } else {
            return parent::assign($dbRecord);
        }
    }

    /**
     * @inheritdoc
     */
    public function delete($oxid = null)
    {
        $this->executeDependencyEvent();

        return parent::delete($oxid);
    }

    /**
     * @inheritdoc
     */
    public function save()
    {
        $this->executeDependencyEvent();

        return parent::save();
    }

}
