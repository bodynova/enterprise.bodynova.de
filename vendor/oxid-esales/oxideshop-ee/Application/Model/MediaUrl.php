<?php
/**
 * This Software is the property of OXID eSales and is protected
 * by copyright law - it is NOT Freeware.
 *
 * Any unauthorized use of this software without a valid license key
 * is a violation of the license agreement and will be prosecuted by
 * civil and criminal law.
 *
 * @link      http://www.oxid-esales.com
 * @copyright (C) OXID eSales AG 2003-2016
 * @version   OXID eShop EE
 */

namespace OxidEsales\EshopEnterprise\Application\Model;

use oxRegistry;

/**
 * @inheritdoc
 */
class MediaUrl extends \OxidEsales\EshopProfessional\Application\Model\MediaUrl
{
    /**
     * @inheritdoc
     * Call cache flushing event.
     */
    public function save()
    {
        $blSaved = parent::save();
        $this->executeDependencyEvent();

        return $blSaved;
    }

    /**
     * @inheritdoc
     * Call cache flushing event.
     */
    public function delete($sOXID = null)
    {
        $result = parent::delete($sOXID);

        $this->executeDependencyEvent();

        return $result;
    }

    /**
     * Set pages to be flushed to cache.
     */
    public function executeDependencyEvent()
    {
        $oCache = $this->_getReverseProxyBackend();
        if ($oCache->isEnabled()) {
            $oProxyCacheUrls = oxNew('oxReverseProxyUrlGenerator');
            $oProxyCacheUrls->setWidget('oxwarticledetails', array('anid' => $this->getObjectId()));
            $oCache->set($oProxyCacheUrls->getUrls());
        }
    }

    /**
     * Returns ReverseProxyBackend from Registry.
     *
     * @return \OxidEsales\Eshop\Core\Cache\ReverseProxy\ReverseProxyBackend
     */
    protected function _getReverseProxyBackend()
    {
        return oxRegistry::get('oxReverseProxyBackend');
    }
}
