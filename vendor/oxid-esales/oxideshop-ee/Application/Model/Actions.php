<?php
/**
 * This Software is the property of OXID eSales and is protected
 * by copyright law - it is NOT Freeware.
 *
 * Any unauthorized use of this software without a valid license key
 * is a violation of the license agreement and will be prosecuted by
 * civil and criminal law.
 *
 * @link      http://www.oxid-esales.com
 * @copyright (C) OXID eSales AG 2003-2016
 * @version   OXID eShop EE
 */

namespace OxidEsales\EshopEnterprise\Application\Model;

use oxRegistry;

/**
 * @inheritdoc
 */
class Actions extends \OxidEsales\EshopProfessional\Application\Model\Actions
{
    /**
     * @inheritdoc
     */
    public function addArticle($articleId)
    {
        parent::addArticle($articleId);

        $this->executeDependencyEvent();
    }

    /**
     * @inheritdoc
     */
    public function removeArticle($articleId)
    {
        $removedArticles = parent::removeArticle($articleId);

        $this->executeDependencyEvent();

        return $removedArticles;
    }

    /**
     * @inheritdoc
     */
    public function delete($articleId = null)
    {
        $articleId = $articleId ? $articleId : $this->getId();
        if (!$articleId || !$this->canDelete($articleId)) {
            return false;
        }
        $isDeleted = parent::delete($articleId);

        $this->executeDependencyEvent();

        return $isDeleted;
    }

    /**
     * @inheritdoc
     */
    public function start()
    {
        parent::start();

        $this->executeDependencyEvent();
    }

    /**
     * @inheritdoc
     */
    public function stop()
    {
        parent::stop();

        $this->executeDependencyEvent();
    }

    /**
     * @inheritdoc
     */
    public function save()
    {
        $isSaved = parent::save();
        $this->executeDependencyEvent();

        return $isSaved;
    }

    /**
     * Execute cache dependencies.
     *
     * @return null
     */
    public function executeDependencyEvent()
    {
        $reverseProxyCache = $this->_getReverseProxyBackend();
        if ($reverseProxyCache->isEnabled()) {
            $urlGenerator = oxNew('oxReverseProxyUrlGenerator');
            $urlGenerator->setStartPage();
            // action="oxtop5"
            $urlGenerator->setWidget('oxwactions', array('action' => $this->getId()));
            $reverseProxyCache->set($urlGenerator->getUrls());
        }
    }

    /**
     * returns ReverseProxyBackend from Registry
     *
     * @return \OxidEsales\Eshop\Core\Cache\ReverseProxy\ReverseProxyBackend
     */
    protected function _getReverseProxyBackend()
    {
        return oxRegistry::get('oxReverseProxyBackend');
    }
}
