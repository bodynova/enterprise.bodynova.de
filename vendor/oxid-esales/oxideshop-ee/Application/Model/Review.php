<?php
/**
 * This Software is the property of OXID eSales and is protected
 * by copyright law - it is NOT Freeware.
 *
 * Any unauthorized use of this software without a valid license key
 * is a violation of the license agreement and will be prosecuted by
 * civil and criminal law.
 *
 * @link      http://www.oxid-esales.com
 * @copyright (C) OXID eSales AG 2003-2016
 * @version   OXID eShop EE
 */

namespace OxidEsales\EshopEnterprise\Application\Model;

use OxidEsales\Eshop\Core\Cache\ReverseProxy\ReverseProxyBackend;

/**
 * @inheritdoc
 */
class Review extends \OxidEsales\EshopProfessional\Application\Model\Review
{
    /**
     * Resets reviewable product cache (non admin mode) and
     * executes parent _resetCache method.
     *
     * @param string $reviewId
     */
    protected function _resetCache($reviewId = null)
    {
        // Customer wrote product review, review affects only product
        // details page( "reset by conditions").
        if (!$this->isAdmin()) {
            if ($this->oxreviews__oxtype->value == 'oxarticle') {
                $cache = oxNew('oxcache');
                $cache->resetOn(array($this->oxreviews__oxobjectid->value => 'anid'));
            }
        }
        parent::_resetCache($reviewId);
    }

    /**
     * Returns ReverseProxyBackend from Registry.
     *
     * @return ReverseProxyBackend
     */
    protected function _getReverseProxyBackend()
    {
        return \oxRegistry::get('oxReverseProxyBackend');
    }

    /**
     * Execute cache dependencies.
     */
    public function executeDependencyEvent()
    {
        $cache = $this->_getReverseProxyBackend();
        if ($cache->isEnabled()) {
            $proxyCacheUrls = oxNew('oxReverseProxyUrlGenerator');
            $proxyCacheUrls->setWidget('oxwReview', array($this->_getObjectKey() => $this->getObjectId()));
            $cache->set($proxyCacheUrls->getUrls());
        }
    }

    /**
     * Save this Object to database, insert or update as needed.
     *
     * @return string|bool
     */
    public function save()
    {
        $isSaved = parent::save();
        $this->executeDependencyEvent();

        return $isSaved;
    }

    /**
     * Delete Object from database.
     *
     * @param string $oxId Object ID (default null)
     *
     * @return bool
     */
    public function delete($oxId = null)
    {
        if (!$oxId) {
            $oxId = $this->getId();
        }
        if (!$oxId) {
            return false;
        }
        $isDeleted = parent::delete($oxId);
        $this->executeDependencyEvent();

        return $isDeleted;
    }

    /**
     * Returns object key used for caching.
     *
     * @return string
     */
    protected function _getObjectKey()
    {
        // @deprecated since v5.3 (2016-06-17); Listmania will be moved to an own module.
        switch ($this->getObjectType()) {
            case 'oxarticle':
                return 'anid';
            case 'oxrecommlist':
                return 'recommid';
        }
        // END deprecated
    }
}
