<?php
/**
 * This Software is the property of OXID eSales and is protected
 * by copyright law - it is NOT Freeware.
 *
 * Any unauthorized use of this software without a valid license key
 * is a violation of the license agreement and will be prosecuted by
 * civil and criminal law.
 *
 * @link      http://www.oxid-esales.com
 * @copyright (C) OXID eSales AG 2003-2016
 * @version   OXID eShop EE
 */

namespace OxidEsales\EshopEnterprise\Application\Model;

use \oxField;
use \oxDb;

/**
 * @inheritdoc
 */
class Groups extends \OxidEsales\EshopProfessional\Application\Model\Groups
{
    /**
     * Creating new group ...
     *
     * @return resource
     */
    protected function _insert()
    {
        // calculating group RR index
        $this->oxgroups__oxrrid = new oxField((( int ) oxDb::getDb()->getOne('select max( oxrrid ) from oxgroups') + 1));

        return parent::_insert();
    }

    /**
     * Checks if object can be read
     *
     * @return bool
     */
    public function canRead()
    {
        return true;
    }

    /**
     * Checks if object field can be read/viewed by user
     *
     * @param string $fieldName name of field to check
     *
     * @return bool
     */
    public function canReadField($fieldName)
    {
        return true;
    }

    /**
     * Deletes user group from database. Returns true/false, according to deleting status.
     *
     * @param string $oxid Object ID (default null)
     *
     * @return bool
     */
    public function delete($oxid = null)
    {
        if (!$oxid) {
            $oxid = $this->getId();
        }
        if (!$oxid) {
            return false;
        }

        if (!$this->canDelete($oxid)) {
            return false;
        }

        $rrid = false;
        if (!$this->getId()) {
            $oGroup = oxNew('oxgroups');
            if ($oGroup->load($oxid)) {
                $rrid = $oGroup->oxgroups__oxrrid->value;
            }
        } else {
            $rrid = $this->oxgroups__oxrrid->value;
        }

        $database = oxDb::getDb();

        // deleting R&R info
        $sDelete = "delete from oxobject2role where oxobject2role.oxobjectid = " . $database->quote($oxid);
        $database->execute($sDelete);

        if ($rrid !== false) {
            $offset = ( int ) ($rrid / 31);
            $bitMap = 1 << ($rrid % 31);

            $sql = "update oxobjectrights set oxgroupidx = oxgroupidx & ~$bitMap where oxoffset = $offset and oxgroupidx & $bitMap ";
            $database->execute($sql);

            // removing cleared
            $sql = 'delete from oxobjectrights where oxgroupidx = 0';
            $database->execute($sql);
        }

        $parentResult = parent::delete($oxid);
        return $parentResult;
    }
}
