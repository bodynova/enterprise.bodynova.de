<?php
/**
 * This Software is the property of OXID eSales and is protected
 * by copyright law - it is NOT Freeware.
 *
 * Any unauthorized use of this software without a valid license key
 * is a violation of the license agreement and will be prosecuted by
 * civil and criminal law.
 *
 * @link      http://www.oxid-esales.com
 * @copyright (C) OXID eSales AG 2003-2016
 * @version   OXID eShop EE
 */

namespace OxidEsales\EshopEnterprise\Application\Model;

use \oxRegistry;

/**
 * @inheritdoc
 */
class Country extends \OxidEsales\EshopProfessional\Application\Model\Country
{
    /**
     * returns ReverseProxyBackend from Registry
     *
     * @return \OxidEsales\Eshop\Core\Cache\ReverseProxy\ReverseProxyBackend
     */
    protected function _getReverseProxyBackend()
    {
        return oxRegistry::get('oxReverseProxyBackend');
    }

    /**
     * Execute cache dependencies
     *
     * @return null
     */
    public function executeDependencyEvent()
    {
        // proxy cache dependencies
        $cache = $this->_getReverseProxyBackend();
        if ($cache->isEnabled()) {
            $proxyCacheUrlsGenerator = oxNew('oxReverseProxyUrlGenerator');

            //pages
            $proxyCacheUrlsGenerator->setStaticPage('register');

            $cache->set($proxyCacheUrlsGenerator->getUrls());
        }
    }

    /**
     * Save this Object to database, insert or update as needed.
     *
     * @return mixed
     */
    public function save()
    {
        $wasSaved = parent::save();
        $this->executeDependencyEvent();

        return $wasSaved;
    }

    /**
     * delete Object from database
     *
     * @param string $oxid Object ID (default null)
     *
     * @return mixed
     */
    public function delete($oxid = null)
    {
        if (!$oxid) {
            $oxid = $this->getId();
        }
        if (!$oxid) {
            return false;
        }
        $wasDeleted = parent::delete($oxid);
        $this->executeDependencyEvent();

        return $wasDeleted;
    }
}
