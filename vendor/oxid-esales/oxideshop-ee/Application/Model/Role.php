<?php
/**
 * This Software is the property of OXID eSales and is protected
 * by copyright law - it is NOT Freeware.
 *
 * Any unauthorized use of this software without a valid license key
 * is a violation of the license agreement and will be prosecuted by
 * civil and criminal law.
 *
 * @link      http://www.oxid-esales.com
 * @copyright (C) OXID eSales AG 2003-2016
 * @version   OXID eShop EE
 */

namespace OxidEsales\EshopEnterprise\Application\Model;

use oxBase;
use oxDb;

/**
 * Roles manager.
 */
class Role extends \OxidEsales\EshopProfessional\Core\Model\BaseModel
{
    /**
     * Current class name.
     *
     * @var string
     */
    protected $_sClassName = 'oxroles';

    /**
     * Executes parent constructor, oxroles::init().
     */
    public function __construct()
    {
        parent::__construct();
        if (!$this->getConfig()->getSerial()->validateShop()) {
            return;
        }
        $this->init('oxroles');
    }

    /**
     * Deletes object information from DB, returns true on success.
     *
     * @param string $oxId Object ID (default null).
     *
     * @return bool
     */
    public function delete($oxId = null)
    {
        if (!$oxId) {
            $oxId = $this->getId();
        }
        if (!$oxId) {
            return false;
        }

        if ($isDeleted = parent::delete($oxId)) {
            $database = oxDb::getDb();
            $quotedOxId = $database->quote($oxId);

            // assigned fields
            $database->execute("delete from oxfield2role where oxroleid = $quotedOxId ");

            // assigned users/groups
            $database->execute("delete from oxobject2role where oxroleid = $quotedOxId ");

            // assigned users/groups
            $database->execute("delete from oxobjectrights where oxobjectid = $quotedOxId ");
        }

        return $isDeleted;
    }
}
