<?php
/**
 * This Software is the property of OXID eSales and is protected
 * by copyright law - it is NOT Freeware.
 *
 * Any unauthorized use of this software without a valid license key
 * is a violation of the license agreement and will be prosecuted by
 * civil and criminal law.
 *
 * @link      http://www.oxid-esales.com
 * @copyright (C) OXID eSales AG 2003-2016
 * @version   OXID eShop EE
 */

namespace OxidEsales\EshopEnterprise\Application\Model;

use \oxRegistry;
use \oxDb;

/**
 * @inheritdoc
 */
class Category extends \OxidEsales\EshopProfessional\Application\Model\Category
{
    /**
     * @inheritdoc
     */
    public function load($oxid)
    {
        $cache = $this->_getCacheBackend();

        if ($cache->isActive()) {
            $categoryData = $this->_loadFromCache($oxid);
        } else {
            return parent::load($oxid);
        }

        if ($categoryData) {
            $this->assign($categoryData);
            return true;
        }

        return false;
    }

    /**
     * returns oxCacheBackend from Registry
     *
     * @return oxCacheBackend
     */
    protected function _getCacheBackend()
    {
        return oxRegistry::get('oxCacheBackend');
    }

    /**
     * returns ReverseProxyBackend from Registry
     *
     * @return \OxidEsales\Eshop\Core\Cache\ReverseProxy\ReverseProxyBackend
     */
    protected function _getReverseProxyBackend()
    {
        return oxRegistry::get('oxReverseProxyBackend');
    }

    /**
     * Load data from cache
     *
     * @param string $sOXID id
     *
     * @return array
     */
    protected function _loadFromCache($sOXID)
    {
        $oCache = $this->_getCacheBackend();
        $sKey = $this->getCacheKey($sOXID);
        $oCacheItem = $oCache->get($sKey);

        if ($oCacheItem) {
            $aData = $oCacheItem->getData();
        } else {
            $aData = $this->_loadFromDb($sOXID);
            $oCacheItem = oxNew('oxCacheItem');
            $oCacheItem->setData($aData);
            $oCache->set($sKey, $oCacheItem);
        }

        return $aData;
    }

    /**
     * Generate cache key
     *
     * @param string $sOXID id
     *
     * @return string
     */
    public function getCacheKey($sOXID = null)
    {
        if (!$sOXID) {
            $sOXID = $this->getId();
        }

        return 'oxCategory_' . $sOXID . '_' . $this->getConfig()->getShopId() . '_' . oxRegistry::getLang()->getLanguageAbbr($this->getLanguage());
    }


    /**
     * Generate cache keys for dependent cached data
     *
     * @param array $aCategoryIds category ids array
     * @param array $aLanguages   lang ids array
     * @param array $aShops       shop ids array
     *
     * @return array
     */
    public function getCacheKeys($aCategoryIds, $aLanguages = null, $aShops = null)
    {
        $aKeys = array();
        $aLanguages = $aLanguages ? $aLanguages : oxRegistry::getLang()->getLanguageIds();
        $aShops = $aShops ? $aShops : $this->getConfig()->getShopIds();

        foreach ($aCategoryIds as $sCategoryId) {
            foreach ($aShops as $sShopId) {
                foreach ($aLanguages as $sLanguageId) {
                    $aKeys[] = 'oxCategory_' . $sCategoryId . '_' . $sShopId . '_' . $sLanguageId;
                }
            }
        }

        return $aKeys;
    }


    /**
     * Execute cache dependencies
     *
     * @param array $aCategoryIds    array of category ids
     * @param bool  $blFlushArticles do articles need to be flushed
     */
    public function executeDependencyEvent($aCategoryIds = null, $blFlushArticles = true)
    {
        if (is_null($aCategoryIds)) {
            $aCategoryIds[] = $this->getId();
        }

        $oCache = $this->_getCacheBackend();
        if ($oCache->isActive()) {
            //invalidate cache
            $oCache->invalidate($this->getCacheKeys($aCategoryIds));
        }

        // proxy cache dependencies
        $oCache = $this->_getReverseProxyBackend();
        if ($oCache->isEnabled()) {
            $oProxyCacheUrls = oxNew('oxReverseProxyUrlGenerator');

            //widgets
            $oProxyCacheUrls->setWidget('oxwcategorytree');

            //pages
            $oProxyCacheUrls->setStartPage();

            foreach ($aCategoryIds as $sCategoryId) {

                //business object urls
                $oProxyCacheUrls->setObject('oxcategory', $sCategoryId);

                if ($blFlushArticles) {
                    // also flush widgets for all articles which are in this category
                    $oArtList = oxNew('oxArticleList');
                    $oArtList->loadCategoryArticles($sCategoryId, null);

                    foreach ($oArtList as $sKey => $oArticle) {
                        // article widgets
                        $oProxyCacheUrls->setWidget('oxwarticlebox', array('anid' => $oArticle->getId()));
                        $oProxyCacheUrls->setWidget('oxwarticledetails', array('anid' => $oArticle->getId()));
                    }
                }
            }

            $oCache->set($oProxyCacheUrls->getUrls());
        }

        $this->_updateDependencies();
    }

    /**
     * Execute cache dependencies
     */
    public function _updateDependencies()
    {
        $oCache = $this->_getCacheBackend();
        if ($oCache->isActive()) {
            //category tree cache dependency
            $oCategoryList = oxNew('oxCategoryList');
            $oCategoryList->executeDependencyEvent();
        }
    }

    /**
     * @inheritdoc
     */
    public function delete($oxid = null)
    {
        if (!$this->getId()) {
            $this->load($oxid);
        }

        if (!$this->canDelete($oxid)) {
            return false;
        }

        $parentResult = parent::delete($oxid);

        $this->executeDependencyEvent();

        return $parentResult;
    }

    /**
     * @inheritdoc
     */
    public function getAdditionalSqlFilter($forceCoreTable = null)
    {
        $parentConditions = parent::getAdditionalSqlFilter($forceCoreTable);
        $result = $parentConditions . $this->_getSqlRightsSnippet($forceCoreTable);

        return $result;
    }

    /**
     * Checks if user rights allows to view/open current category
     *
     * @param string $sCategoryId category id (optional)
     *
     * @return bool
     */
    public function canView($sCategoryId = null)
    {
        if ($this->isAdmin() || !$this->getRights()) {
            return true;
        }

        $myConfig = $this->getConfig();

        if (!$this->canDo($sCategoryId, 1)) {
            return false;
        }

        // additionally checking for parent categories
        $oDb = oxDb::getDb(oxDb::FETCH_MODE_ASSOC);
        if ($sCategoryId) {
            $rs = $this->fetchCategoryTreeInformation($sCategoryId);
            if ($rs != false && $rs->count() > 0) {
                $iLeft = (int) $rs->fields['oxleft'];
                $iRight = (int) $rs->fields['oxright'];
                $sRootId = $rs->fields['oxrootid'];
            } else {
                return false;
            }
        } else {
            $iLeft = (int) $this->oxcategories__oxleft->value;
            $iRight = (int) $this->oxcategories__oxright->value;
            $sRootId = $this->oxcategories__oxrootid->value;
        }
        $sQ = "select oxid from oxcategories where oxleft < $iLeft and oxright > $iRight and oxrootid = " . $oDb->quote($sRootId) . " ";
        $rs = $oDb->selectLimit($sQ, 1000);
        if ($rs != false && $rs->count() > 0) {
            while (!$rs->EOF) {
                if (!$this->canDo($rs->fields['oxid'], 1)) {
                    return false;
                }
                $rs->fetchRow();
            }
        }

        return true;
    }

    /**
     * Fetch the category tree information.
     *
     * @param string $categoryId The id of the category for which we want the tree information.
     *
     * @return array An associative array with the left, right and rootId of the given category.
     */
    protected function fetchCategoryTreeInformation($categoryId)
    {
        $database = oxDb::getDb(oxDb::FETCH_MODE_ASSOC);

        $query = "select oxleft, oxright, oxrootid from oxcategories where oxid = " . $database->quote($categoryId);

        return $database->selectLimit($query, 1);
    }

    /**
     * @inheritdoc
     */
    public function assignViewableRecord($sSelect)
    {
        $oCategoryList = oxNew("oxCategoryList");
        $oCategoryList->selectString($sSelect);
        foreach ($oCategoryList as $oCategory) {
            if ($oCategory->canView()) {
                //$this = $oCategory;
                $aRecord = array();
                //assigning data to this
                foreach ($this->_aFieldNames as $sFieldName => $sVal) {
                    $sLongName = $this->_getFieldLongName($sFieldName);
                    $aRecord[$sFieldName] = $oCategory->$sLongName;
                    if ($aRecord[$sFieldName] instanceof \OxidEsales\EshopCommunity\Core\Field) {
                        $aRecord[$sFieldName] = $aRecord[$sFieldName]->getRawValue();
                    }
                    $this->assign($aRecord);
                }

                return true;
            }
        }

        return false;
    }

    /**
     * @inheritdoc
     */
    protected function _insert()
    {
        if (!$this->canInsert()) {
            return false;
        }
        $this->executeDependencyEvent();

        return parent::_insert();
    }

    /**
     * @inheritdoc
     */
    protected function _update()
    {
        if (!$this->canUpdate()) {
            return false;
        }

        $parentResult = parent::_update();

        if ($parentResult!==false) {
            $this->executeDependencyEvent();
        }

        return $parentResult;
    }

    /**
     * Gets shop relations object.
     *
     * @return oxCategory2ShopRelations
     */
    protected function _getElement2ShopRelations()
    {
        if (is_null($this->_oElement2ShopRelations)) {
            $this->_oElement2ShopRelations = oxNew('oxCategory2ShopRelations', $this->getCoreTableName());
        }

        return $this->_oElement2ShopRelations;
    }

    /**
     * Returns a list of subshop ids, including the parent, where oxcategory elements are inherited in bulk via config
     * option from the current shop.
     * If it is subcategory then it returns a list of subshop ids where parent category is assigned to.
     *
     * @return array
     */
    protected function _getInheritanceGroup()
    {
        $oParentCategory = $this->getParentCategory();

        if (is_null($oParentCategory)) {
            // this is parent category
            $aShopIds = parent::_getInheritanceGroup();
        } else {
            // this is subcategory
            $oElement2ShopRelations = $this->_getElement2ShopRelations();
            $sParentCategoryId = $oParentCategory->getId();

            $aShopIds = $oElement2ShopRelations->getItemAssignedShopIds($sParentCategoryId);
        }

        return $aShopIds;
    }

    /**
     * Gets category object IDs from oxobject2category.
     *
     * @param string $sCategoryId Category ID.
     *
     * @return array
     */
    public function getCategoryObjectIds($sCategoryId)
    {
        $sTable = 'oxobject2category';

        $sSql = "SELECT `oxid` FROM `{$sTable}` WHERE `oxcatnid` = ?";
        $aResult = oxDb::getDb()->getCol($sSql, array($sCategoryId));

        return $aResult;
    }
}
