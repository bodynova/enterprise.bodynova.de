<?php
/**
 * This Software is the property of OXID eSales and is protected
 * by copyright law - it is NOT Freeware.
 *
 * Any unauthorized use of this software without a valid license key
 * is a violation of the license agreement and will be prosecuted by
 * civil and criminal law.
 *
 * @link      http://www.oxid-esales.com
 * @copyright (C) OXID eSales AG 2003-2016
 * @version   OXID eShop EE
 */

namespace OxidEsales\EshopEnterprise\Application\Model;

/**
 * @inheritdoc
 */
class Attribute extends \OxidEsales\EshopProfessional\Application\Model\Attribute
{
    /**
     * @inheritdoc
     */
    protected function canDeleteAttribute($oxId)
    {
        $canDelete = parent::canDeleteAttribute($oxId);
        if (!$canDelete || !$this->canDelete($oxId)) {
            $canDelete = false;
        }

        return $canDelete;
    }

    /**
     * Returns ReverseProxyBackend from Registry.
     *
     * @return \OxidEsales\Eshop\Core\Cache\ReverseProxy\ReverseProxyBackend
     */
    protected function _getReverseProxyBackend()
    {
        return \oxRegistry::get('oxReverseProxyBackend');
    }

    /**
     * Returns article ids assigned to attribute.
     *
     * @return array
     */
    public function getArticleIds()
    {
        $sViewName = getViewName('oxobject2attribute');

        return \oxDb::getDb()->getCol("SELECT `oxobjectid` FROM $sViewName WHERE `oxattrid` = '" . $this->getId() . "' and `oxvalue` != '' ");
    }

    /**
     * Execute cache dependencies.
     */
    public function executeDependencyEvent()
    {
        // proxy cache dependencies
        $cache = $this->_getReverseProxyBackend();
        if ($cache->isEnabled()) {
            $proxyCacheUrls = oxNew('oxReverseProxyUrlGenerator');

            $articleIds = $this->getArticleIds();

            foreach ($articleIds as $articleId) {
                $proxyCacheUrls->setWidget('oxwarticledetails', array('anid' => $articleId));
            }

            $cache->set($proxyCacheUrls->getUrls());
        }
    }

    /**
     * @inheritdoc
     */
    public function save()
    {
        $isSaved = parent::save();
        $this->executeDependencyEvent();

        return $isSaved;
    }
}
