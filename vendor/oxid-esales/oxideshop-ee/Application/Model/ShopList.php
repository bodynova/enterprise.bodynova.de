<?php
/**
 * This Software is the property of OXID eSales and is protected
 * by copyright law - it is NOT Freeware.
 *
 * Any unauthorized use of this software without a valid license key
 * is a violation of the license agreement and will be prosecuted by
 * civil and criminal law.
 *
 * @link      http://www.oxid-esales.com
 * @copyright (C) OXID eSales AG 2003-2016
 * @version   OXID eShop EE
 */

namespace OxidEsales\EshopEnterprise\Application\Model;

use oxDb;

/**
 * @inheritdoc
 */
class ShopList extends \OxidEsales\EshopProfessional\Application\Model\ShopList
{
    /**
     * Loads the list of subshops for given parent.
     * Multishop should be removed from mall tabs.
     *
     * @param int $shopId Parent id
     */
    public function loadSubShopList($shopId)
    {
        if (!$shopId) {
            $shopId = $this->getConfig()->getShopId();
        }

        $database = oxDb::getDb();
        $query = "select * from oxshops where oxismultishop = 0 and oxparentid = " . $database->quote($shopId);

        $this->selectString($query);
    }

    /**
     * Loads the list of subshops for given parent.
     * Multishop should be removed from mall tabs.
     *
     * @param int $shopId Parent id
     */
    public function loadSuperShopList($shopId)
    {
        if (!$shopId) {
            $shopId = $this->getConfig()->getShopId();
        }

        $database = oxDb::getDb();
        $query = "select * from oxshops where oxismultishop = 0 and  oxid <> " . $database->quote($shopId) . " ";

        $this->selectString($query);
    }
}
