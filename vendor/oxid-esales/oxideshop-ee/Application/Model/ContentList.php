<?php
/**
 * This Software is the property of OXID eSales and is protected
 * by copyright law - it is NOT Freeware.
 *
 * Any unauthorized use of this software without a valid license key
 * is a violation of the license agreement and will be prosecuted by
 * civil and criminal law.
 *
 * @link      http://www.oxid-esales.com
 * @copyright (C) OXID eSales AG 2003-2016
 * @version   OXID eShop EE
 */

namespace OxidEsales\EshopEnterprise\Application\Model;

use \OxRegistry;

/**
 * @inheritdoc
 */
class ContentList extends \OxidEsales\EshopProfessional\Application\Model\ContentList
{
    /**
     * @inheritdoc
     */
    protected function _load($type)
    {
        $cache = $this->_getCacheBackend();
        if ($cache->isActive()) {
            $itemData = $this->_loadFromCache($type);
            $this->assignArray($itemData);
        } else {
            parent::_load($type);
        }
    }

    /**
     * Returns oxCacheBackend from Registry
     *
     * @return oxCacheBackend
     */
    protected function _getCacheBackend()
    {
        return OxRegistry::get('oxCacheBackend');
    }

    /**
     * Load data from cache
     *
     * @param integer $type - type of content
     *
     * @return array
     */
    protected function _loadFromCache($type)
    {
        $cache = $this->_getCacheBackend();
        $key = $this->getCacheKey($type);
        $cacheItem = $cache->get($key);

        if ($cacheItem) {
            $itemData = $cacheItem->getData();
        } else {
            $itemData = $this->_loadFromDb($type);
            $cacheItem = oxNew('oxCacheItem');
            $cacheItem->setData($itemData);
            $cache->set($key, $cacheItem);
        }

        return $itemData;
    }

    /**
     * Generate cache key for category tree for current shop and language
     *
     * @param integer $type - type of content
     *
     * @return string
     */
    public function getCacheKey($type)
    {
        return $sKey = 'oxContentList_' . $type . '_' . $this->getConfig()->getShopId() . '_' . oxRegistry::getLang()->getLanguageAbbr();
    }

    /**
     * Generate cache key
     *
     * @param integer $type      type of content
     * @param array   $languages lang id array
     * @param array   $shopIds     shop ids array
     *
     * @return array
     */
    public function getCacheKeys($type, $languages = null, $shopIds = null)
    {
        $keys = array();
        $languages = $languages ? $languages : oxRegistry::getLang()->getLanguageIds();
        $shopIds = $shopIds ? $shopIds : $this->getConfig()->getShopIds();

        foreach ($shopIds as $oneShopId) {
            foreach ($languages as $oneLanguageId) {
                $keys[] = 'oxContentList_' . $type . '_' . $oneShopId . '_' . $oneLanguageId;
            }
        }

        return $keys;
    }

    /**
     * Execute cache dependencies
     *
     * @param integer $type - type of content
     */
    public function executeDependencyEvent($type)
    {
        $cache = $this->_getCacheBackend();
        if ($cache->isActive()) {
            $cache->invalidate($this->getCacheKeys($type));
        }

        if ($type == self::TYPE_CATEGORY_MENU) {
            $this->_updateCategoryTreeDependencies();
        }
    }

    /**
     * Execute cache dependencies
     */
    public function _updateCategoryTreeDependencies()
    {
        $cache = $this->_getCacheBackend();
        if ($cache->isActive()) {
            //category tree cache dependency
            $categoryList = oxNew('oxCategoryList');
            $categoryList->executeDependencyEvent();
        }
    }


}
