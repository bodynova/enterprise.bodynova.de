<?php
/**
 * This Software is the property of OXID eSales and is protected
 * by copyright law - it is NOT Freeware.
 *
 * Any unauthorized use of this software without a valid license key
 * is a violation of the license agreement and will be prosecuted by
 * civil and criminal law.
 *
 * @link      http://www.oxid-esales.com
 * @copyright (C) OXID eSales AG 2003-2016
 * @version   OXID eShop EE
 */

namespace OxidEsales\EshopEnterprise\Application\Model;

use oxArticle;
use oxDb;
use oxField;
use oxI18n;
use OxidEsales\Eshop\Application\Model\Contract\ArticleInterface;
use OxidEsales\Eshop\Core\Article2ShopRelations;
use OxidEsales\Eshop\Core\Cache\Generic\Cache;
use OxidEsales\Eshop\Core\Cache\ReverseProxy\ReverseProxyBackend;
use oxRegistry;
use oxPrice;

/**
 * @inheritdoc
 */
class Article extends \OxidEsales\EshopProfessional\Application\Model\Article implements ArticleInterface
{
    /**
     * 'Stock Changed' dependency event flag.
     *
     * @var int
     */
    const DEPENDENCY_EVENT_STOCK_CHANGED = 1;

    /**
     * 'Article deleted' dependency event flag.
     *
     * @var int
     */
    const DEPENDENCY_EVENT_ARTICLE_DELETED = 2;

    /**
     * Load article even in other non active shops
     *
     * @var boolean
     */
    protected $_blDisableShopCheck = true;

    /**
     * Maximum number of similar products which forces to reset full cache
     */
    protected $_iMaxSimilarForCacheReset = 100;

    /**
     * Checks if current rights allows specified action
     *
     * @param string $articleId ID of the product to be checked
     * @param int    $action    Action to check
     *
     * @return bool
     */
    public function canDo($articleId = null, $action = 1)
    {
        if (!parent::canDo($articleId, $action)) {
            return false;
        }

        $database = oxDb::getDb();

        $parentArticleId = $this->oxarticles__oxparentid->value;
        if ($articleId) {
            $parentArticleId = $database->getOne("select oxparentid from oxarticles where oxid = " . $database->quote($articleId));
        }

        if ($parentArticleId && !$this->canDo($parentArticleId, $action)) {
            return false;
        }

        return true;
    }

    /**
     * @inheritdoc
     */
    public function canUpdateField($field)
    {
        $canUpdate = true;

        if ($this->isDerived()) {
            $config = $this->getConfig();
            $canUpdate = false;
            $multiShopFields = $config->getConfigParam('aMultishopArticleFields');

            if (!$config->getConfigParam('blMallCustomPrice')) {
                return false;
            } elseif (in_array(strtoupper($field), $multiShopFields)) {
                $canUpdate = true;
            }
        }

        return $canUpdate ? parent::canUpdateField($field) : $canUpdate;
    }

    /**
     * Checks if any field can be updated from aMultishopArticleFields
     *
     * @return bool
     */
    public function canUpdateAnyField()
    {
        if ($this->_blCanUpdateAnyField === null) {
            $canUpdateAnyField = false;
            $config = $this->getConfig();
            $multiShopFields = $config->getConfigParam('aMultishopArticleFields');

            if (is_array($multiShopFields)) {
                foreach ($multiShopFields as $field) {
                    $canUpdateAnyField = $this->canUpdateField($field);
                    if ($canUpdateAnyField) {
                        break;
                    }
                }
            } else {
                // if aMultishopArticleFields array is empty, using previous implementation
                $canUpdateAnyField = $this->canUpdateField('oxprice');
            }
            $this->_blCanUpdateAnyField = $canUpdateAnyField;
        }

        return $this->_blCanUpdateAnyField;
    }

    /**
     * Checks if user rights allows to view/open current product
     *
     * @param string $articleId product id (optional)
     *
     * @return bool
     */
    public function canView($articleId = null)
    {
        $rights = $this->getRights();
        if ($this->isAdmin() || !$rights) {
            return true;
        }

        return $this->canDo($articleId, 1);
    }

    /**
     * Checks if user rights allows to buy current product
     *
     * @param string $articleId product id (optional)
     *
     * @return bool
     */
    public function canBuy($articleId = null)
    {
        $rights = $this->getRights();
        if ($this->isAdmin() || !$rights) {
            return true;
        }

        return $this->canDo($articleId, 2);
    }

    /**
     * @inheritdoc
     */
    public function isVisible()
    {
        if (!$this->canView()) {
            return false;
        }

        return parent::isVisible();
    }

    /**
     * @inheritdoc
     */
    public function assign($aRecord)
    {
        if (!$this->canRead()) {
            return false;
        }

        $result = parent::assign($aRecord);

        $this->_assignAccessRights();

        return $result;
    }

    /**
     * @inheritdoc
     */
    public function delete($sOXID = null)
    {
        $sOXID = $sOXID ? $sOXID : $this->getId();

        if (!$sOXID) {
            return false;
        }

        if (!$this->canDelete($sOXID)) {
            return false;
        }

        $this->executeDependencyEvent(self::DEPENDENCY_EVENT_ARTICLE_DELETED);

        // delete self
        return parent::delete($sOXID);
    }

    /**
     * Checks if derived update is allowed:
     *  if - blMallCustomPrice = true, current shop id != product shop id, setupped aMultishopArticleFields - returns true
     *  else - returns parent::allowDerivedUpdate()
     * Task #1579 - impossible to edit price for inherited / assigned products
     *
     * @return bool
     */
    public function allowDerivedUpdate()
    {
        $config = $this->getConfig();
        if ($config->getConfigParam('blMallCustomPrice') && $this->oxarticles__oxshopid->value &&
            $config->getShopId() != $this->oxarticles__oxshopid->value &&
            is_array($config->getConfigParam('aMultishopArticleFields'))
        ) {
            return true;
        }

        return parent::allowDerivedUpdate();
    }

    /**
     * @inheritdoc
     */
    public function onChange($action = null, $articleId = null, $parentArticleId = null)
    {
        parent::onChange($action, $articleId, $parentArticleId);

        if (!isset($articleId)) {
            if ($this->getId()) {
                $articleId = $this->getId();
            }
            if (!isset ($articleId)) {
                $articleId = $this->oxarticles__oxid->value;
            }
        }

        $config = $this->getConfig();

        // cache control ..
        $this->_resetCache($articleId);
        if ($action === ACTION_UPDATE_STOCK) {
            $this->executeDependencyEvent(self::DEPENDENCY_EVENT_STOCK_CHANGED);
        } elseif ($action === ACTION_DELETE) {
            $this->executeDependencyEvent(self::DEPENDENCY_EVENT_ARTICLE_DELETED);
        } else {
            $this->executeDependencyEvent();
        }

        if ($config->getConfigParam('blUseContentCaching')) {
            $genericCache = oxNew('oxCache');
            $genericCache->reset();
        }
    }

    /**
     * Checks for VPE info which applies changes on passed amount
     *
     * @param double $amount Amount
     *
     * @return double
     */
    public function checkForVpe($amount)
    {
        $database = oxDb::getDb();
        $vpe = $database->getOne('select oxvpe from oxarticles where oxid = ' . $database->quote($this->getId()));
        if ($vpe > 1) {
            // change amount
            $amount = ceil(($amount / $vpe));
            $amount = $amount * $vpe;
        }

        return $amount;
    }

    /**
     * @inheritdoc
     */
    public function getLongDescription()
    {
        if ($this->_oLongDesc === null) {
            if (!$this->canReadField('oxlongdesc')) {
                $this->_oLongDesc = new oxField();
                return $this->_oLongDesc;
            }
        }

        return parent::getLongDescription();
    }

    /**
     * @inheritdoc
     */
    public function setArticleLongDesc($longDescription)
    {
        if (!$this->canUpdateField('oxlongdesc')) {
            return false;
        }

        return parent::setArticleLongDesc($longDescription);
    }

    /**
     * Execute cache dependencies
     *
     * @param int $dependencyEvent event name
     *
     * @return null
     */
    public function executeDependencyEvent($dependencyEvent = null)
    {
        if ($dependencyEvent == self::DEPENDENCY_EVENT_STOCK_CHANGED) {
            $this->executeDependencyEventAfterStockChanges();
        } else {
            $this->_updateSelfDependency();
            $this->_updateParentDependency();

            if ($dependencyEvent == self::DEPENDENCY_EVENT_ARTICLE_DELETED || $this->hasSortingFieldsChanged()) {
                $this->_updateCategoryDependency();
            }
        }
    }

    /**
     * @inheritdoc
     */
    public function addToRatingAverage($rating)
    {
        parent::addToRatingAverage($rating);

        $this->_updateSelfDependency();
    }

    /**
     * @inheritdoc
     */
    public function getVendorId($forceReload = false)
    {
        $vendorId = parent::getVendorId();
        if ($vendorId) {
            if (!$forceReload && isset(self::$_aArticleVendors[$this->getId()])) {
                return self::$_aArticleVendors[$this->getId()];
            }
            $database = oxDb::getDb();
            $vendorView = getViewName('oxvendor');
            $vendorId = $database->getOne("select oxid from $vendorView where oxid = ?", array($vendorId));
            self::$_aArticleVendors[$this->getId()] = $vendorId;
        }

        return $vendorId;
    }

    /**
     * @inheritdoc
     */
    public function getManufacturerId($forceReload = false)
    {
        $manufacturerId = parent::getManufacturerId();
        if ($manufacturerId) {
            if (!$forceReload && isset(self::$_aArticleManufacturers[$this->getId()])) {
                return self::$_aArticleManufacturers[$this->getId()];
            }
            $oDb = oxDb::getDb();
            $manufacturersView = getViewName('oxmanufacturers');
            $manufacturerId = $oDb->getOne("select oxid from $manufacturersView where oxid = ?", array($manufacturerId));
            self::$_aArticleManufacturers[$this->getId()] = $manufacturerId;
        }

        return $manufacturerId;
    }

    /**
     * Execute cache dependencies:
     * Invalidates generic cache;
     * Flush cache when stock status has changed, otherwise nothing in views will change;
     * Runs article parent dependency event.
     */
    public function executeDependencyEventAfterStockChanges()
    {
        $genericCache = $this->_getCacheBackend();

        if ($genericCache->isActive()) {
            $genericCache->invalidate($this->getCacheKeys());
        }

        //
        if ($this->_isStockStatusChanged()) {
            $this->_updateArticleWidgetsDependency();

            // article either becomes not visible or visible
            if (!$this->isVisible() || $this->_isVisibilityChanged()) {
                $this->_updateOtherWidgetsDependency();
                $this->_updatePagesDependency();
                $this->_updateOtherPagesDependency();
                $this->_updateArticleObjectDependency();
                $this->_updateCategoryObjectsDependency();
            }
        }

        $article = $this->getParentArticle();
        if ($article) {
            $article->executeDependencyEvent(self::DEPENDENCY_EVENT_STOCK_CHANGED);
        }
    }

    /**
     * Generate cache key
     *
     * @param string $articleId id
     *
     * @return string
     */
    public function getCacheKey($articleId = null)
    {
        if (!$articleId) {
            $articleId = $this->getId();
        }

        return 'oxArticle_' . $articleId . '_' . $this->getConfig()->getShopId() . '_' . oxRegistry::getLang()->getLanguageAbbr($this->getLanguage());
    }

    /**
     * Generate cache keys for dependent cached data.
     *
     * @param array $languages lang id array
     * @param array $shopIds   shop ids array
     *
     * @return string
     */
    public function getCacheKeys($languages = null, $shopIds = null)
    {
        $cacheKeys = array();
        $languages = $languages ? $languages : oxRegistry::getLang()->getLanguageIds();
        $shopIds = $shopIds ? $shopIds : $this->getConfig()->getShopIds();

        foreach ($shopIds as $shopId) {
            foreach ($languages as $languageId) {
                $cacheKeys[] = 'oxArticle_' . $this->getId() . '_' . $shopId . '_' . $languageId;
            }
        }

        return $cacheKeys;
    }

    /**
     * Returns current article vendor object. If $blShopCheck = false, then
     * vendor loading will fallback to oxI18n object and blReadOnly parameter
     * will be set to true if vendor is not assigned to current shop
     *
     * @return oxi18n
     */
    public function _createMultilanguageVendorObject()
    {
        $vendor = parent::_createMultilanguageVendorObject();
        $vendor->setForceCoreTableUsage(true);

        return $vendor;
    }

    /**
     * @inheritdoc
     */
    public function unassignFromShop($shopIds)
    {
        $result = parent::unassignFromShop($shopIds);

        if (!is_array($shopIds)) {
            $shopIds = array($shopIds);
        }

        //remove unused oxfield2shop values
        $field2Shop = $this->_getField2Shop();
        foreach ($shopIds as $sShopId) {
            $field2Shop->cleanMultishopFields($sShopId);
        }

        return $result;
    }

    /**
     * @inheritdoc
     */
    public function updateManufacturerBeforeLoading($oManufacturer)
    {
        parent::updateManufacturerBeforeLoading($oManufacturer);

        $oManufacturer->setForceCoreTableUsage(true);
    }

    /**
     * @inheritdoc
     */
    protected function _selectCategoryIds($query, $field)
    {
        $results = parent::_selectCategoryIds($query, $field);

        $category = oxNew('oxcategory');

        foreach ($results as $key => $categoryId) {
            if (!$category->canView($categoryId)) {
                unset($results[$key]);
            }
        }

        return $results;
    }

    /**
     * @inheritdoc
     */
    protected function _saveArtLongDesc()
    {
        if (in_array("oxlongdesc", $this->_aSkipSaveFields)) {
            return;
        }
        if ($this->isDerived() && !$this->canUpdateField("oxlongdesc")) {
            return;
        }
        parent::_saveArtLongDesc();
    }

    /**
     * @inheritdoc
     */
    protected function _skipSaveFields()
    {
        parent::_skipSaveFields();
        $config = $this->getConfig();

        // Also remove the fields which are not saved for subshops (these are defined in $myConfg->aMultishopArticleFields array).

        $shopId = $config->getShopID();
        $multiShopArticleFields = $config->getConfigParam('aMultishopArticleFields');

        // Removing multishop fields which should be saved before some another way.
        if ($config->getConfigParam('blMallCustomPrice') && $this->oxarticles__oxshopid->value && $shopId != $this->oxarticles__oxshopid->value && is_array($multiShopArticleFields)) {
            $languagesCount = count(oxRegistry::getLang()->getLanguageIds());
            foreach ($multiShopArticleFields as $field) {
                $field = strtolower($field);
                $this->_aSkipSaveFields[] = $field;
                if ($this->isMultilingualField($field)) {
                    for ($ii = 1; $ii < $languagesCount; ++$ii) {
                        $this->_aSkipSaveFields[] = $field . "_$ii";
                    }
                }
            }
        }
    }

    /**
     * Sets shop specific article information from oxfield2shop table
     * (default are oxprice, oxpricea, oxpriceb, oxpricec
     * (specified in oxConfig::aMultishopArticleFields param))
     *
     * @param oxArticle $article Article object
     */
    protected function _setShopValues($article)
    {
        $config = $this->getConfig();
        $shopId = $config->getShopID();
        $multishopArticleFields = $config->getConfigParam('aMultishopArticleFields');
        if ($config->getConfigParam('blMallCustomPrice') && $shopId != $article->oxarticles__oxshopid->value && is_array($multishopArticleFields)) {
            $field2Shop = oxNew("oxField2Shop");
            $field2Shop->setEnableMultilang($this->_blEmployMultilanguage);
            $field2Shop->setLanguage($this->getLanguage());
            $field2Shop->setProductData($this);
        }
    }

    /**
     * @inheritdoc
     */
    protected function _insert()
    {
        $result = parent::_insert();

        //copy parent subshop assignments for a new variant
        if ($this->isVariant() && $this->getParentArticle() !== false) {
            $element2ShopRelations = $this->_getElement2ShopRelations();
            $element2ShopRelations->updateInheritanceFromParent($this->getId(), $this->getParentId());
        }

        return $result;
    }

    /**
     * @inheritdoc
     */
    protected function _update()
    {
        if (!$this->canUpdate()) {
            return false;
        }

        $this->setUpdateSeo(true);
        $this->_setUpdateSeoOnFieldChange('oxtitle');

        $config = $this->getConfig();
        //saving custom subshop fields
        $shopId = $config->getShopId();
        $multishopArticleFields = $config->getConfigParam('aMultishopArticleFields');
        if ($config->getConfigParam('blMallCustomPrice') && $this->oxarticles__oxshopid->value && $shopId != $this->oxarticles__oxshopid->value && is_array($multishopArticleFields)) {
            $field2Shop = oxNew("oxField2Shop");
            $field2Shop->setEnableMultilang($this->_blEmployMultilanguage);
            $field2Shop->setLanguage($this->getLanguage());

            return $field2Shop->saveProductData($this);
        }

        $result = parent::_update();

        $this->executeDependencyEvent();

        return $result;
    }

    /**
     * @inheritdoc
     */
    protected function _deleteRecords($articleId)
    {
        parent::_deleteRecords($articleId);

        $database = oxDb::getDb();

        return $database->execute('delete from oxfield2shop where oxartid = ? ', array($articleId));
    }

    /**
     * Return sub shop variant min price
     *
     * @return null
     */
    protected function _getShopVarMinPrice()
    {
        return $this->_getSubShopVarMinPrice();
    }

    /**
     * Return sub shop variant max price
     *
     * @return null
     */
    protected function _getShopVarMaxPrice()
    {
        return $this->_getSubShopVarMaxPrice();
    }

    /**
     * Return sub shop variant min price.
     *
     * @return double|null
     */
    protected function _getSubShopVarMinPrice()
    {
        $myConfig = $this->getConfig();
        $sShopId = $myConfig->getShopId();
        if ($this->getConfig()->getConfigParam('blMallCustomPrice') && $sShopId != $this->oxarticles__oxshopid->value) {
            $sPriceSuffix = $this->_getUserPriceSufix();
            $sSql = 'SELECT ';
            if ($sPriceSuffix != '' && $this->getConfig()->getConfigParam('blOverrideZeroABCPrices')) {
                $sSql .= 'MIN(IF(`oxfield2shop`.`oxprice' . $sPriceSuffix . '` = 0, `oxfield2shop`.`oxprice`, `oxfield2shop`.`oxprice' . $sPriceSuffix . '`)) AS `varminprice` ';
            } else {
                $sSql .= 'MIN(`oxfield2shop`.`oxprice' . $sPriceSuffix . '`) AS `varminprice` ';
            }
            $sSql .= ' FROM ' . getViewName('oxfield2shop') . ' AS oxfield2shop
                        INNER JOIN ' . $this->getViewName(true) . ' AS oxarticles ON `oxfield2shop`.`oxartid` = `oxarticles`.`oxid`
                        WHERE ' . $this->getSqlActiveSnippet(true) . '
                            AND ( `oxarticles`.`oxparentid` = ' . oxDb::getDb()->quote($this->getId()) . ' )
                            AND ( `oxfield2shop`.`oxshopid` = ' . oxDb::getDb()->quote($sShopId) . ' )';
            $dPrice = oxDb::getDb()->getOne($sSql);
        }

        return $dPrice;
    }

    /**
     * Return sub shop variant max price.
     *
     * @return double|null
     */
    protected function _getSubShopVarMaxPrice()
    {
        $myConfig = $this->getConfig();
        $sShopId = $myConfig->getShopId();
        if ($this->getConfig()->getConfigParam('blMallCustomPrice') && $sShopId != $this->oxarticles__oxshopid->value) {
            $sPriceSuffix = $this->_getUserPriceSufix();
            $sSql = 'SELECT ';
            if ($sPriceSuffix != '' && $this->getConfig()->getConfigParam('blOverrideZeroABCPrices')) {
                $sSql .= 'MAX(IF(`oxfield2shop`.`oxprice' . $sPriceSuffix . '` = 0, `oxfield2shop`.`oxprice`, `oxfield2shop`.`oxprice' . $sPriceSuffix . '`)) AS `varmaxprice` ';
            } else {
                $sSql .= 'MAX(`oxfield2shop`.`oxprice' . $sPriceSuffix . '`) AS `varmaxprice` ';
            }
            $sSql .= ' FROM ' . getViewName('oxfield2shop') . ' AS oxfield2shop
                        INNER JOIN ' . $this->getViewName(true) . ' AS oxarticles ON `oxfield2shop`.`oxartid` = `oxarticles`.`oxid`
                        WHERE ' . $this->getSqlActiveSnippet(true) . '
                            AND ( `oxarticles`.`oxparentid` = ' . oxDb::getDb()->quote($this->getId()) . ' )
                            AND ( `oxfield2shop`.`oxshopid` = ' . oxDb::getDb()->quote($sShopId) . ' )';
            $dPrice = oxDb::getDb()->getOne($sSql);
        }

        return $dPrice;
    }

    /**
     * @inheritdoc
     */
    protected function _loadData($articleId)
    {
        $genericCache = $this->_getCacheBackend();

        return $genericCache->isActive() ? $this->_loadFromCache($articleId) : parent::_loadData($articleId);
    }

    /**
     * @inheritdoc
     */
    protected function _getModifiedAmountPrice($amount)
    {
        $price = parent::_getModifiedAmountPrice($amount);

        $price = $this->_modifyMallPrice($price);

        return $price;
    }

    /**
     * Returns SQL select string with checks if items is accessible by R&R config
     *
     * @param bool $forceCoreTable forces core table usage (optional)
     *
     * @return string
     */
    protected function _createSqlActiveSnippet($forceCoreTable)
    {
        $querySnippet = parent::_createSqlActiveSnippet($forceCoreTable);

        return $querySnippet . $this->_getSqlRightsSnippet($forceCoreTable);
    }

    /**
     * @inheritdoc
     */
    protected function _calculateVarMinPrice()
    {
        $dPrice = parent::_calculateVarMinPrice();

        return $this->_modifyMallPrice($dPrice);
    }

    /**
     * @inheritdoc
     */
    protected function _prepareModifiedPrice($dPrice)
    {
        $dPrice = parent::_prepareModifiedPrice($dPrice);
        $dPrice = $this->_modifyMallPrice($dPrice);

        return $dPrice;
    }

    /**
     * Returns oxCacheBackend from Registry
     *
     * @return Cache
     */
    protected function _getCacheBackend()
    {
        return oxRegistry::get('oxCacheBackend');
    }

    /**
     * returns oxReverseProxyBackend from Registry
     *
     * @return ReverseProxyBackend
     */
    protected function _getReverseProxyBackend()
    {
        return oxRegistry::get('oxReverseProxyBackend');
    }

    /**
     * @inheritdoc
     */
    protected function _loadFromDb($articleId)
    {
        $forceCoreTableUsage = $this->getForceCoreTableUsage();
        $this->_forceCoreTableUsageForSharedBasket();

        $data = parent::_loadFromDb($articleId);

        $this->setForceCoreTableUsage($forceCoreTableUsage);

        return $data;
    }

    /**
     * Sets forcing of core table usage for creating table view name when shared basket is enabled.
     */
    private function _forceCoreTableUsageForSharedBasket()
    {
        if ($this->getConfig()->getConfigParam('blMallSharedBasket')) {
            $this->setForceCoreTableUsage(true);
        }
    }

    /**
     * Load data from cache
     *
     * @param string $articleId id
     *
     * @return array
     */
    protected function _loadFromCache($articleId)
    {
        $genericCache = $this->_getCacheBackend();
        $cacheKey = $this->getCacheKey($articleId);
        $cacheItem = $genericCache->get($cacheKey);

        if ($cacheItem) {
            $data = $cacheItem->getData();
        } else {
            $data = $this->_loadFromDb($articleId);
            $cacheItem = oxNew('oxCacheItem');
            $cacheItem->setData($data);
            $genericCache->set($cacheKey, $cacheItem);
        }

        return $data;
    }

    /**
     * Resets article cache
     *
     * @param string $articleId Article ID
     *
     * @return null
     */
    protected function _resetCache($articleId = null)
    {
        $config = $this->getConfig();
        // if is not admin and caching enabled, try to clear it
        if (!$this->isAdmin() && $config->getConfigParam('blUseContentCaching')) {

            // stock control check
            $config = $this->getConfig();
            $articleId = $articleId ? $articleId : $this->getId();

            $invalidArticles = array();
            $invalidCategories = array();

            if ($articleId && $config->getConfigParam('blUseStock')) {

                // if active product id does not match reset product id..
                if ($articleId != $this->getId()) {
                    $product = oxNew('oxarticle');
                    $product->setSkipAssign(true);
                    $product->load($articleId);
                    $product->_assignStock();

                    return $product->_resetCache();
                }

                $contentCache = oxNew('oxCache');
                $database = oxDb::getDb();
                // Fixing oxarticle unit test testResetCache().
                // Fetch mode is global so it might be easly lost to no assoc.
                $database->setFetchMode(oxDb::FETCH_MODE_ASSOC);

                $articleIdQuoted = $database->quote($articleId);

                // choosing parent id
                $query = "select oxparentid from oxarticles where oxid=$articleIdQuoted ";
                $parentId = $this->getId() ? $this->oxarticles__oxparentid->value : $database->getOne($query);

                // if article belongs to some action - deleting all cache
                $parentIdQuoted = $parentId ? $database->quote($parentId) : $articleIdQuoted;
                $query = 'select 1 from oxactions2article where oxartid=' . $parentIdQuoted;
                if ($database->getOne($query)) {
                    $contentCache->reset();

                    return;
                }

                // this check can only be performed on loaded article ..
                if (($articleId = $this->getId()) && $this->oxarticles__oxstockflag->value != 4) {
                    $articleIdQuoted = $database->quote($articleId);

                    // Stock control is ON

                    // GREEN light
                    $stockStatus = 0;

                    $stock = $this->_blNotBuyableParent ? $this->oxarticles__oxvarstock->value : $this->oxarticles__oxstock->value;

                    // ORANGE light
                    if ($stock <= $config->getConfigParam('sStockWarningLimit') && $stock > 0) {
                        $stockStatus = 1;
                    }

                    // RED light
                    if ($stock <= 0) {
                        $stockStatus = -1;
                    }

                    // checking initial with current (set by user) stock status ..
                    if ($this->_iStockStatus != $stockStatus) {

                        // similar articles ...
                        if ($config->getConfigParam('bl_perfLoadSimilar')) {

                            $query = "from oxobject2attribute where oxobjectid != $articleIdQuoted and oxattrid
                                   in ( select oxattrid from oxobject2attribute where oxobjectid = $articleIdQuoted ) group by oxobjectid ";

                            // resetting cache fully if similar article count > 100
                            if ((( int ) $database->getOne("select count(*) from ( select count(oxobjectid) $query ) as _cnt")) > $this->_iMaxSimilarForCacheReset) {
                                $contentCache->reset();

                                return;
                            }
                            $queryResult = $database->select("select oxobjectid $query");
                            if ($queryResult != false && $queryResult->count() > 0) {
                                while (!$queryResult->EOF) {
                                    $resetOn[$queryResult->fields['oxobjectid']] = 'anid';
                                    $queryResult->fetchRow();
                                    $invalidArticles[] = $queryResult->fields['oxobjectid'];
                                }
                            }
                        }

                        // resetting self
                        $resetOn[$articleId] = 'anid';

                        // resetting parent if available
                        if ($parentId) {
                            $resetOn[$parentId] = 'anid';
                            $invalidArticles[] = $parentId;
                        }

                        // reset cache for article categories
                        $categoryIds = $this->getCategoryIds();
                        foreach ($categoryIds as $categoryId) {
                            $resetOn[$categoryId] = 'cid';
                            $invalidCategories[] = $categoryId;
                        }

                        $query = '';
                        if ($config->getConfigParam('bl_perfLoadCrossselling')) {
                            $query = "select oxaccessoire2article.oxarticlenid as _oxid from oxaccessoire2article
                                   where oxaccessoire2article.oxobjectid=$articleIdQuoted ";
                        }

                        if ($config->getConfigParam('bl_perfLoadAccessoires')) {
                            $query .= $query ? ' union ' : '';
                            $query .= "select oxobject2article.oxobjectid as _oxid from oxobject2article where oxobject2article.oxarticlenid=$articleIdQuoted
                                    union select oxobject2article.oxarticlenid as _oxid from oxobject2article where oxobject2article.oxobjectid=$articleIdQuoted group by _oxid";
                        }

                        if ($query) {
                            $queryResult = $database->select($query);
                            if ($queryResult != false && $queryResult->count() > 0) {
                                while (!$queryResult->EOF) {
                                    $resetOn[$queryResult->fields['_oxid']] = 'anid';
                                    $queryResult->fetchRow();
                                    $invalidArticles[] = $queryResult->fields['_oxid'];
                                }
                            }
                        }

                        // resetting ..
                        $contentCache->resetOn($resetOn);

                    }
                }
            }
        }

        parent::_resetCache($articleId);
    }

    /**
     * Execute cache dependencies for parent
     */
    protected function _updateParentDependency()
    {
        $article = $this->getParentArticle();
        if ($article) {
            $article->executeDependencyEvent();
        }
    }

    /**
     * Execute cache dependencies by self
     */
    protected function _updateSelfDependency()
    {
        $genericCache = $this->_getCacheBackend();

        if ($genericCache->isActive()) {
            //invalidate cache
            $genericCache->invalidate($this->getCacheKeys());
        }

        $this->_updateArticleWidgetsDependency();
        $this->_updateOtherWidgetsDependency();
        $this->_updatePagesDependency();

        // flush only when stock status has changed, otherwise nothing in views will change
        if ($this->_isStockStatusChanged()) {
            $this->_updateOtherPagesDependency();
        }
    }

    /**
     * Execute cache dependencies with categories
     */
    protected function _updateCategoryDependency()
    {
        //category tree cache dependency
        $this->setForceCoreTableUsage(true);
        $categoriesIds = $this->getCategoryIds(false, true);
        $this->setForceCoreTableUsage(false);

        $category = oxNew('oxCategory');
        $category->executeDependencyEvent($categoriesIds, false);
    }

    /**
     * Execute cache dependencies for article widgets.
     */
    protected function _updateArticleWidgetsDependency()
    {
        $reverseProxyBackend = $this->_getReverseProxyBackend();

        if ($reverseProxyBackend->isEnabled()) {
            $reverseProxyUrl = oxNew('oxReverseProxyUrlGenerator');

            $reverseProxyUrl->setWidget('oxwarticlebox', array('anid' => $this->getId()));
            $reverseProxyUrl->setWidget('oxwarticledetails', array('anid' => $this->getId()));

            $reverseProxyBackend->set($reverseProxyUrl->getUrls());
        }
    }

    /**
     * Execute cache dependencies for other widgets.
     */
    protected function _updateOtherWidgetsDependency()
    {
        $reverseProxyBackend = $this->_getReverseProxyBackend();

        if ($reverseProxyBackend->isEnabled()) {
            $reverseProxyUrl = oxNew('oxReverseProxyUrlGenerator');

            // @deprecated since v5.3 (2016-06-17); Listmania will be moved to an own module.
            $reverseProxyUrl->setWidget('oxwrecommendation');
            // END deprecated
            $reverseProxyBackend->set($reverseProxyUrl->getUrls());
        }
    }

    /**
     * Execute cache dependencies for pages.
     */
    protected function _updatePagesDependency()
    {
        $reverseProxyBackend = $this->_getReverseProxyBackend();

        if ($reverseProxyBackend->isEnabled()) {
            $reverseProxyUrl = oxNew('oxReverseProxyUrlGenerator');

            $reverseProxyUrl->setStartPage();
            $reverseProxyUrl->setPage('search');

            $reverseProxyBackend->set($reverseProxyUrl->getUrls());
        }
    }

    /**
     * Execute cache dependencies for other pages.
     */
    protected function _updateOtherPagesDependency()
    {
        $reverseProxyBackend = $this->_getReverseProxyBackend();

        if ($reverseProxyBackend->isEnabled()) {
            $reverseProxyUrl = oxNew('oxReverseProxyUrlGenerator');

            // @deprecated since v5.3 (2016-06-17); Listmania will be moved to an own m
            $reverseProxyUrl->setDynamicPage('recommlist');
            // END deprecated
            $reverseProxyUrl->setPage('wishlist');

            $reverseProxyBackend->set($reverseProxyUrl->getUrls());
        }
    }

    /**
     * Execute cache dependencies for article object.
     */
    protected function _updateArticleObjectDependency()
    {
        $reverseProxyBackend = $this->_getReverseProxyBackend();

        if ($reverseProxyBackend->isEnabled()) {
            $reverseProxyUrl = oxNew('oxReverseProxyUrlGenerator');

            $reverseProxyUrl->setObject('oxarticle', $this->getId());

            $reverseProxyBackend->set($reverseProxyUrl->getUrls());
        }
    }

    /**
     * Execute cache dependencies for category objects.
     */
    protected function _updateCategoryObjectsDependency()
    {
        $reverseProxyBackend = $this->_getReverseProxyBackend();

        if ($reverseProxyBackend->isEnabled()) {
            $reverseProxyUrl = oxNew('oxReverseProxyUrlGenerator');

            $categoryIds = $this->getCategoryIds(false, true);
            foreach ($categoryIds as $categoryId) {
                $reverseProxyUrl->setObject('oxcategory', $categoryId);
            }

            $reverseProxyBackend->set($reverseProxyUrl->getUrls());
        }
    }

    /**
     * Modifies price according to special sub shop addition
     *
     * @param double $price Modifiable price
     *
     * @return double
     */
    protected function _modifyMallPrice(&$price)
    {
        $config = $this->getConfig();
        // mall add price stuff
        // MALL ON
        if ($config->isMall() && !$this->isAdmin()) {
            //adding shop addition
            if ($config->getConfigParam('iMallPriceAddition')) {
                if ($config->getConfigParam('blMallPriceAdditionPercent')) {
                    $price += oxPrice::percent($price, $config->getConfigParam('iMallPriceAddition'));
                } else {
                    $price += $config->getConfigParam('iMallPriceAddition');
                }
            }
        }

        return $price;
    }

    /**
     * Assigns rr to article object
     */
    protected function _assignAccessRights()
    {
        if (!$this->isAdmin() && $this->getRights()) {
            // R&R: checking if access rights allows to buy this product
            if (!$this->_blNotBuyable) {
                $this->setBuyableState($this->canBuy());
            }
        }
    }

    /**
     * Gets shop relations object.
     *
     * @return Article2ShopRelations
     */
    protected function _getElement2ShopRelations()
    {
        if (is_null($this->_oElement2ShopRelations)) {
            $this->_oElement2ShopRelations = oxNew('oxArticle2ShopRelations', $this->getCoreTableName());
        }

        return $this->_oElement2ShopRelations;
    }

    /**
     * Returns Field2Shop object
     *
     * @return Field2Shop
     */
    protected function _getField2Shop()
    {
        return oxNew('oxField2Shop');
    }

    /**
     * @inheritdoc
     */
    protected function updateVariantsBaseObject($baseObject, $forceCoreTableUsage = null)
    {
        parent::updateVariantsBaseObject($baseObject, $forceCoreTableUsage);

        $baseObject->setForceCoreTableUsage($forceCoreTableUsage);
    }
}
