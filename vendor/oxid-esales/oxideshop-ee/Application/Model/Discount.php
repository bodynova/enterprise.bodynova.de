<?php
/**
 * This Software is the property of OXID eSales and is protected
 * by copyright law - it is NOT Freeware.
 *
 * Any unauthorized use of this software without a valid license key
 * is a violation of the license agreement and will be prosecuted by
 * civil and criminal law.
 *
 * @link      http://www.oxid-esales.com
 * @copyright (C) OXID eSales AG 2003-2016
 * @version   OXID eShop EE
 */

namespace OxidEsales\EshopEnterprise\Application\Model;

use oxRegistry;

/**
 * @inheritdoc
 */
class Discount extends \OxidEsales\EshopProfessional\Application\Model\Discount
{
    /**
     * @inheritdoc
     */
    public function delete($sOXID = null)
    {
        if (!$sOXID) {
            $sOXID = $this->getId();
        }

        if (!$this->canDelete($sOXID)) {
            return false;
        }

        $this->executeDependencyEvent();

        return parent::delete($sOXID);
    }

    /**
     * @inheritdoc
     */
    public function save()
    {
        $this->executeDependencyEvent();

        return parent::save();
    }

    /**
     * Execute cache dependencies
     */
    public function executeDependencyEvent()
    {
        // proxy cache dependencies
        $oCache = $this->_getReverseProxyBackend();
        if ($oCache->isEnabled() && $this->isForAmount(0)) {
            $oProxyCacheUrls = oxNew('oxReverseProxyUrlGenerator');

            $aArticleIds = $this->getArticleIds();
            $aCategoryIds = $this->getCategoryIds();
            //pages
            if ($aArticleIds || $aCategoryIds) {
                if ($aArticleIds) {
                    foreach ($aArticleIds as $sId) {
                        $oProxyCacheUrls->setWidget('oxwarticlebox', array('anid' => $sId));
                        $oProxyCacheUrls->setWidget('oxwarticledetails', array('anid' => $sId));
                    }
                }
                if ($aCategoryIds) {
                    $oCategory = oxNew('oxCategory');
                    $oCategory->executeDependencyEvent($aCategoryIds);
                }
            } else {
                $oProxyCacheUrls->setStartPage();
                $oProxyCacheUrls->setLists();
                $oProxyCacheUrls->setPage('search');
            }
            $oCache->set($oProxyCacheUrls->getUrls());
        }
    }

    /**
     * returns ReverseProxyBackend from Registry
     *
     * @return \OxidEsales\Eshop\Core\Cache\ReverseProxy\ReverseProxyBackend
     */
    protected function _getReverseProxyBackend()
    {
        return oxRegistry::get('oxReverseProxyBackend');
    }
}
