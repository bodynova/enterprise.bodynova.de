<?php
/**
 * This Software is the property of OXID eSales and is protected
 * by copyright law - it is NOT Freeware.
 *
 * Any unauthorized use of this software without a valid license key
 * is a violation of the license agreement and will be prosecuted by
 * civil and criminal law.
 *
 * @link      http://www.oxid-esales.com
 * @copyright (C) OXID eSales AG 2003-2016
 * @version   OXID eShop EE
 */

namespace OxidEsales\EshopEnterprise\Application\Model;

/**
 * @inheritdoc
 */
class Rating extends \OxidEsales\EshopProfessional\Application\Model\Rating
{
    /**
     * Returns ReverseProxyBackend from Registry.
     *
     * @return \OxidEsales\Eshop\Core\Cache\ReverseProxy\ReverseProxyBackend
     */
    protected function _getReverseProxyBackend()
    {
        return \oxRegistry::get('oxReverseProxyBackend');
    }

    /**
     * Execute cache dependencies.
     */
    public function executeDependencyEvent()
    {
        // proxy cache dependencies
        $cache = $this->_getReverseProxyBackend();
        if ($cache->isActive()) {
            $proxyCacheUrls = oxNew('oxReverseProxyUrlGenerator');

            //widgets
            $proxyCacheUrls->setWidget('oxwRating', array($this->_getObjectKey() => $this->getObjectId()));

            $cache->set($proxyCacheUrls->getUrls());
        }
    }

    /**
     * Save this Object to database, insert or update as needed.
     *
     * @return bool
     */
    public function save()
    {
        $isSaved = parent::save();
        $this->executeDependencyEvent();

        return $isSaved;
    }

    /**
     * Returns object key used for caching.
     *
     * @return string
     */
    protected function _getObjectKey()
    {
        // @deprecated since v5.3 (2016-06-17); Listmania will be moved to an own module.
        $objectKey = '';
        switch ($this->getObjectType()) {
            case 'oxarticle':
                $objectKey = 'anid';
                break;
            case 'oxrecommlist':
                $objectKey = 'recommid';
                break;
        }

        return $objectKey;
        // END deprecated
    }
}
