<?php
/**
 * This Software is the property of OXID eSales and is protected
 * by copyright law - it is NOT Freeware.
 *
 * Any unauthorized use of this software without a valid license key
 * is a violation of the license agreement and will be prosecuted by
 * civil and criminal law.
 *
 * @link      http://www.oxid-esales.com
 * @copyright (C) OXID eSales AG 2003-2016
 * @version   OXID eShop EE
 */

namespace OxidEsales\EshopEnterprise\Application\Model;

use oxRegistry;
use OxidEsales\Eshop\Core\Cache\Generic\Cache;

/**
 * @inheritdoc
 */
class CategoryList extends \OxidEsales\EshopProfessional\Application\Model\CategoryList
{
    /**
     * @inheritdoc
     */
    protected function getActivityFieldsSql($tableName)
    {
        return ",not ($tableName.oxactive " . $this->_getSqlRightsSnippet($tableName) . ") as oxppremove";
    }

    /**
     * Execute cache dependencies
     */
    public function executeDependencyEvent()
    {
        $oCache = $this->_getCacheBackend();
        if ($oCache->isActive()) {
            $oCache->invalidate($this->getCacheKeys());
        }
    }

    /**
     * @inheritdoc
     */
    public function load()
    {
        $oCache = $this->_getCacheBackend();
        if ($oCache->isActive()) {
            $aData = $this->_loadFromCache();
            $this->assignArray($aData);
        } else {
            parent::load();
        }
    }

    /**
     * Returns SQL select string with checks if items is accessible by R&R config
     *
     * @param string $sTable (optional) table name
     *
     * @return string
     */
    protected function _getSqlRightsSnippet($sTable = null)
    {
        if (!$sTable) {
            $sTable = $this->getBaseObject()->getViewName();
        }
        $sQ = '';

        // R&R: user access
        if (!$this->isAdmin() && ($oRights = $this->getRights())) {
            $sQ .= " and ( ( ";
            $sQ .= "( select oxobjectrights.oxobjectid from oxobjectrights where oxobjectrights.oxobjectid = $sTable.oxid and oxobjectrights.oxaction = 1 limit 1 ) is null ";
            $aGroupIdx = $oRights->getUserGroupIndex();
            if (is_array($aGroupIdx) && count($aGroupIdx)) {
                $sSel = "";
                $iCnt = 0;
                foreach ($aGroupIdx as $iOffset => $iBitMap) {
                    if ($iCnt) {
                        $sSel .= " | ";
                    }
                    $sSel .= " ( oxobjectrights.oxgroupidx & $iBitMap and oxobjectrights.oxoffset = $iOffset ) ";
                    $iCnt++;
                }

                $sQ .= ") or (";
                $sQ .= "( select oxobjectrights.oxobjectid from oxobjectrights where oxobjectrights.oxobjectid = $sTable.oxid and oxobjectrights.oxaction = 1 and $sSel limit 1 ) is not null ";
            }

            $sQ .= " ) ) ";
        }

        return $sQ;
    }

    /**
     * returns oxCacheBackend from Registry
     *
     * @return Cache
     */
    protected function _getCacheBackend()
    {
        return oxRegistry::get('oxCacheBackend');
    }

    /**
     * Load data from cache
     *
     * @return array
     */
    protected function _loadFromCache()
    {
        $oCache = $this->_getCacheBackend();
        $sKey = $this->getCacheKey();
        $oCacheItem = $oCache->get($sKey);

        if ($oCacheItem) {
            $aData = $oCacheItem->getData();
        } else {
            $aData = $this->_loadFromDb();
            $oCacheItem = oxNew('oxCacheItem');
            $oCacheItem->setData($aData);
            $oCache->set($sKey, $oCacheItem);
        }

        return $aData;
    }

    /**
     * Generate cache key for category tree for current shop and language
     *
     * @return string
     */
    public function getCacheKey()
    {
        return $sKey = 'oxCategoryTree_' . $this->getConfig()->getShopId() . '_' . oxRegistry::getLang()->getLanguageAbbr();
    }

    /**
     * Generate cache key
     *
     * @param array $aLanguages lang id array
     * @param array $aShops     shop ids array
     *
     * @return array
     */
    public function getCacheKeys($aLanguages = null, $aShops = null)
    {
        $aKeys = array();
        $aLanguages = $aLanguages ? $aLanguages : oxRegistry::getLang()->getLanguageIds();
        $aShops = $aShops ? $aShops : $this->getConfig()->getShopIds();

        foreach ($aShops as $sShopId) {
            foreach ($aLanguages as $sLanguageId) {
                $aKeys[] = 'oxCategoryTree_' . $sShopId . '_' . $sLanguageId;
            }
        }

        return $aKeys;
    }

    /**
     * @inheritdoc
     */
    protected function onUpdateCategoryTree()
    {
        $oCache = oxNew("oxCache");
        $oCache->reset();
    }

    /**
     * @inheritdoc
     */
    protected function getInitialUpdateCategoryTreeCondition($verbose = false)
    {
        $condition = parent::getInitialUpdateCategoryTreeCondition();

        $this->_aUpdateInfo[] = "*** <b>SHOP : {$this->_sShopID}</b><br><br>";
        if ($verbose) {
            echo current($this->_aUpdateInfo);
        }
        $condition .= " and oxshopid = '$this->_sShopID'";

        return $condition;
    }
}
