<?php
/**
 * This Software is the property of OXID eSales and is protected
 * by copyright law - it is NOT Freeware.
 *
 * Any unauthorized use of this software without a valid license key
 * is a violation of the license agreement and will be prosecuted by
 * civil and criminal law.
 *
 * @link      http://www.oxid-esales.com
 * @copyright (C) OXID eSales AG 2003-2016
 * @version   OXID eShop EE
 */

namespace OxidEsales\EshopEnterprise\Application\Component\Widget;

use oxRegistry;

/**
 * @inheritdoc
 */
class MiniBasket extends \OxidEsales\EshopProfessional\Application\Component\Widget\MiniBasket
{
    /**
     * Returns if view should be cached
     *
     * @return bool
     */
    public function isCacheable()
    {
        $sType = oxRegistry::getConfig()->getRequestParameter('nocookie');

        if ($sType) {
            return true;
        } else {
            return false;
        }
    }
}
