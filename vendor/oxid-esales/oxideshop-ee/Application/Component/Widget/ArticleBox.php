<?php
/**
 * This Software is the property of OXID eSales and is protected
 * by copyright law - it is NOT Freeware.
 *
 * Any unauthorized use of this software without a valid license key
 * is a violation of the license agreement and will be prosecuted by
 * civil and criminal law.
 *
 * @link      http://www.oxid-esales.com
 * @copyright (C) OXID eSales AG 2003-2016
 * @version   OXID eShop EE
 */

namespace OxidEsales\EshopEnterprise\Application\Component\Widget;

use oxRegistry;

/**
 * @inheritdoc
 */
class ArticleBox extends \OxidEsales\EshopProfessional\Application\Component\Widget\ArticleBox
{
    /**
     * Do not cache if user is logged in
     *
     * @var bool
     */
    protected $_blCacheForUser = false;

    /**
     * @inheritdoc
     */
    protected function updateDynamicParameters($dynamicParameters)
    {
        $dynamicParameters = parent::updateDynamicParameters($dynamicParameters);
        $reverseProxyBackend = oxRegistry::get('oxReverseProxyBackend');

        if ($reverseProxyBackend->isActive()) {
            $dynamicParameters = str_replace("&amp;", "&", $dynamicParameters);
        }

        return $dynamicParameters;
    }
}
