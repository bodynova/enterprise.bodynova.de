<?php
/**
 * This Software is the property of OXID eSales and is protected
 * by copyright law - it is NOT Freeware.
 *
 * Any unauthorized use of this software without a valid license key
 * is a violation of the license agreement and will be prosecuted by
 * civil and criminal law.
 *
 * @link      http://www.oxid-esales.com
 * @copyright (C) OXID eSales AG 2003-2016
 * @version   OXID eShop EE
 */

namespace OxidEsales\EshopEnterprise\Application\Component\Widget;

/**
 * @inheritdoc
 */
class ArticleDetails extends \OxidEsales\EshopProfessional\Application\Component\Widget\ArticleDetails
{
    /**
     * Do not cache if user is logged in
     *
     * @var bool
     */
    protected $_blCacheForUser = false;

    /**
     * @inheritdoc
     */
    public function loadVariantInformation()
    {
        if ($this->_aVariantList === null) {
            $this->_aVariantList = parent::loadVariantInformation();

            $this->_aVariantList = $this->_checkVariantAccessRights($this->_aVariantList);
        }

        return $this->_aVariantList;
    }

    /**
     * R&R: sets if variant is buyble
     *
     * @param array $variantList variant list
     *
     * @return null
     */
    protected function _checkVariantAccessRights($variantList)
    {
        if ($this->getRights()) {
            if (is_array($variantList)) {
                foreach ($variantList as $variantId => $variant) {
                    // viewable ?
                    if (!$variant->canView()) {
                        // removing variant
                        unset($variantList[$variantId]);
                        continue;
                    }
                }
            }
        }

        return $variantList;
    }
}
