<?php
/**
 * This Software is the property of OXID eSales and is protected
 * by copyright law - it is NOT Freeware.
 *
 * Any unauthorized use of this software without a valid license key
 * is a violation of the license agreement and will be prosecuted by
 * civil and criminal law.
 *
 * @link      http://www.oxid-esales.com
 * @copyright (C) OXID eSales AG 2003-2016
 * @version   OXID eShop EE
 */

namespace OxidEsales\EshopEnterprise\Application\Component;

use oxRegistry;
use oxUtils;

/**
 * @inheritdoc
 */
class BasketComponent extends \OxidEsales\EshopProfessional\Application\Component\BasketComponent
{
    /**
     * @inheritdoc
     */
    public function toBasket($sProductId = null, $dAmount = null, $aSel = null, $aPersParam = null, $blOverride = false)
    {
        $redirectUrl = parent::toBasket($sProductId, $dAmount, $aSel, $aPersParam, $blOverride);

        if (!is_null($redirectUrl)) {
            $this->_flushPrivateSalesCategories();
        }

        return $redirectUrl;
    }

    /**
     * @inheritdoc
     */
    public function executeUserChoice()
    {
        $this->_flushPrivateSalesCategories();
        return parent::executeUserChoice();
    }

    /**
     * Flushes Private sales messages in cached category list
     * Called when adding items to basket and private sales messages in different categories are cached.
     */
    protected function _flushPrivateSalesCategories()
    {
        if (oxRegistry::getConfig()->getConfigParam("blBasketExcludeEnabled")) {
            $oCache = $this->_getReverseProxyBackend();

            if ($oCache->isEnabled()) {
                $oProxyCacheUrls = oxNew('oxReverseProxyUrlGenerator');
                $oProxyCacheUrls->setObject('oxCategory');
                $oCache->set($oProxyCacheUrls->getUrls());
            }
        }
    }

    /**
     * @inheritdoc
     */
    protected function emptyBasket($oBasket)
    {
        parent::emptyBasket($oBasket);

        if ($oParent = $this->getParent()) {
            $oParent->setAllowCacheInvalidating(true);
        }
    }
}
