<?php
/**
 * This Software is the property of OXID eSales and is protected
 * by copyright law - it is NOT Freeware.
 *
 * Any unauthorized use of this software without a valid license key
 * is a violation of the license agreement and will be prosecuted by
 * civil and criminal law.
 *
 * @link      http://www.oxid-esales.com
 * @copyright (C) OXID eSales AG 2003-2016
 * @version   OXID eShop EE
 */

namespace OxidEsales\EshopEnterprise\Application\Component;

/**
 * @inheritdoc
 */

class UserComponent extends \OxidEsales\EshopProfessional\Application\Component\UserComponent
{
    /**
     * @inheritdoc
     */
    public function __construct()
    {
        parent::__construct();
        $this->_aAllowedClasses = array_merge($this->_aAllowedClasses, array('mallstart'));
    }


    /**
     * @inheritdoc
     */
    protected function _afterLogin($user)
    {
        $parentResult = parent::_afterLogin($user);

        // resetting rights ..
        $this->setRights(null);

        return $parentResult;
    }

    /**
     * @inheritdoc
     */
    protected function resetPermissions()
    {
        parent::resetPermissions();

        // resetting rights
        $this->setRights(null);
    }

    /**
     * @inheritdoc
     */
    protected function configureUserBeforeCreation($user)
    {
        $user = parent::configureUserBeforeCreation($user);
        $user->setUseMaster();

        return $user;
    }

}
