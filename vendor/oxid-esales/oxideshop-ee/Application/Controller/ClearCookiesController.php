<?php
/**
 * This Software is the property of OXID eSales and is protected
 * by copyright law - it is NOT Freeware.
 *
 * Any unauthorized use of this software without a valid license key
 * is a violation of the license agreement and will be prosecuted by
 * civil and criminal law.
 *
 * @link      http://www.oxid-esales.com
 * @copyright (C) OXID eSales AG 2003-2016
 * @version   OXID eShop EE
 */

namespace OxidEsales\EshopEnterprise\Application\Controller;

/**
 * CMS - loads pages and displays it
 */
class ClearCookiesController extends \OxidEsales\EshopProfessional\Application\Controller\ClearCookiesController
{
    /**
     * Clears all cookies
     */
    protected function _removeCookies()
    {
        parent::_removeCookies();
        $oReverseProxyCache = $this->_getReverseProxyBackend();
        if ($oReverseProxyCache->isActive()) {
            $oUtilsServer = \oxRegistry::get("oxUtilsServer");
            $oUtilsServer->setOxCookie('oxenv_key', '', time() - 10000, '/');
        }
    }
}
