<?php
/**
 * This Software is the property of OXID eSales and is protected
 * by copyright law - it is NOT Freeware.
 *
 * Any unauthorized use of this software without a valid license key
 * is a violation of the license agreement and will be prosecuted by
 * civil and criminal law.
 *
 * @link      http://www.oxid-esales.com
 * @copyright (C) OXID eSales AG 2003-2016
 * @version   OXID eShop EE
 */

namespace OxidEsales\EshopEnterprise\Application\Controller;

use oxRegistry;

/**
 * Current user notice list manager.
 * When user is logged in in this manager window he can modify
 * his notice list status - remove articles from notice list or
 * store them to shopping basket, view detail information.
 * OXID eShop -> MY ACCOUNT -> Newsletter.
 */
class AccountNoticeList extends \OxidEsales\EshopProfessional\Application\Controller\AccountNoticeListController
{

    /**
     * Unset anid from navigation parameters because it overrides required view parameters
     * for wish list while reverse proxy is active.
     *
     * @return array
     */
    public function getNavigationParams()
    {
        $aParams = parent::getNavigationParams();

        $oRPBackend = oxRegistry::get('oxReverseProxyBackend');
        if ($oRPBackend->isActive()) {
            unset($aParams['anid']);
        }

        return $aParams;
    }
}
