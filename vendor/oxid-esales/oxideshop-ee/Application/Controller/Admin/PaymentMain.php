<?php
/**
 * This Software is the property of OXID eSales and is protected
 * by copyright law - it is NOT Freeware.
 *
 * Any unauthorized use of this software without a valid license key
 * is a violation of the license agreement and will be prosecuted by
 * civil and criminal law.
 *
 * @link      http://www.oxid-esales.com
 * @copyright (C) OXID eSales AG 2003-2016
 * @version   OXID eShop EE
 */

namespace OxidEsales\EshopEnterprise\Application\Controller\Admin;

/**
 * @inheritdoc
 */
class PaymentMain extends \OxidEsales\EshopProfessional\Application\Controller\Admin\PaymentMain
{
    /**
     * @inheritdoc
     */
    public function save()
    {
        if (!$this->getConfig()->getConfigParam('blAllowSharedEdit')) {
            return;
        }
        parent::save();
    }

    /**
     * @inheritdoc
     */
    public function saveinnlang()
    {
        if (!$this->getConfig()->getConfigParam('blAllowSharedEdit')) {
            return;
        }
        parent::saveinnlang();
    }

    /**
     * @inheritdoc
     */
    public function delFields()
    {
        if (!$this->getConfig()->getConfigParam('blAllowSharedEdit')) {
            return;
        }
        parent::delFields();
    }

    /**
     * @inheritdoc
     */
    public function addField()
    {
        if (!$this->getConfig()->getConfigParam('blAllowSharedEdit')) {
            return;
        }
        parent::addField();
    }
}
