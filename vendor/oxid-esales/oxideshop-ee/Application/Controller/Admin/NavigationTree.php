<?php
/**
 * This Software is the property of OXID eSales and is protected
 * by copyright law - it is NOT Freeware.
 *
 * Any unauthorized use of this software without a valid license key
 * is a violation of the license agreement and will be prosecuted by
 * civil and criminal law.
 *
 * @link      http://www.oxid-esales.com
 * @copyright (C) OXID eSales AG 2003-2016
 * @version   OXID eShop EE
 */

namespace OxidEsales\EshopEnterprise\Application\Controller\Admin;

/**
 * @inheritdoc
 */
class NavigationTree extends \OxidEsales\EshopProfessional\Application\Controller\Admin\NavigationTree
{
    /**
     * @inheritdoc
     */
    protected function _getMenuFiles()
    {
        $filesToLoad = parent::_getMenuFiles();

        $dynamicPagesMenuPath = null;
        $config = $this->getConfig();
        $isLoadedDynamicContents = $config->getConfigParam('blLoadDynContents');
        $sShopCountry = $config->getConfigParam('sShopCountry');
        if (!$isLoadedDynamicContents && $sShopCountry) {
            $dynamicPagesMenuLanguage = $this->_getDynMenuLang();
            $dynamicPagesMenuPath = $this->_getDynMenuUrl($dynamicPagesMenuLanguage, $isLoadedDynamicContents);

        }
        if (!is_null($dynamicPagesMenuPath)) {
            $filesToLoad[] = $dynamicPagesMenuPath;
        }

        return $filesToLoad;
    }

    /**
     * Process cache contents and return the result
     * deprecated, as cache files are cleared from session data, which is only added
     * after loading the cache by _sessionizeLocalUrls().
     *
     * @param string $cacheContents Initial cached string.
     *
     * @see self::_sessionizeLocalUrls()
     *
     * @return string
     */
    protected function _processCachedFile($cacheContents)
    {
        $cacheContents = parent::_processCachedFile($cacheContents);
        return getStr()->preg_replace("/shp\=\d+/", "shp=" . $this->getConfig()->getShopId(), $cacheContents);
    }

    /**
     * Process navigation tree for rights and roles.
     */
    protected function onGettingDomXml()
    {
        parent::onGettingDomXml();
        if (($rights = $this->getRights())) {
            $rights->processNaviTree($this->_oDom);
        }
    }
}
