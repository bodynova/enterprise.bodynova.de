<?php
/**
 * This Software is the property of OXID eSales and is protected
 * by copyright law - it is NOT Freeware.
 *
 * Any unauthorized use of this software without a valid license key
 * is a violation of the license agreement and will be prosecuted by
 * civil and criminal law.
 *
 * @link      http://www.oxid-esales.com
 * @copyright (C) OXID eSales AG 2003-2016
 * @version   OXID eShop EE
 */

namespace OxidEsales\EshopEnterprise\Application\Controller\Admin;

/**
 * @inheritdoc
 */
class VendorMainAjax extends \OxidEsales\EshopProfessional\Application\Controller\Admin\VendorMainAjax
{
    /**
     * @inheritdoc
     */
    protected function onVendorActionArticleUpdateConditions($articleIds)
    {
        $parentResult = parent::onVendorActionArticleUpdateConditions($articleIds);
        $oConfig = $this->getConfig();

        $result = $parentResult . " and oxshopid='" . $oConfig->getShopId() . "' ";

        return $result;
    }

    /**
     * @inheritdoc
     */
    protected function onVendorAction($vendorOxid)
    {
        parent::onVendorAction($vendorOxid);

        $vendor = oxNew('oxvendor');
        $vendor->load($vendorOxid);
        $vendor->executeDependencyEvent();
    }
}
