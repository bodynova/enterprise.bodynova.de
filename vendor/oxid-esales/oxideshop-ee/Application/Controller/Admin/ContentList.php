<?php
/**
 * This Software is the property of OXID eSales and is protected
 * by copyright law - it is NOT Freeware.
 *
 * Any unauthorized use of this software without a valid license key
 * is a violation of the license agreement and will be prosecuted by
 * civil and criminal law.
 *
 * @link      http://www.oxid-esales.com
 * @copyright (C) OXID eSales AG 2003-2016
 * @version   OXID eShop EE
 */

namespace OxidEsales\EshopEnterprise\Application\Controller\Admin;

/**
 * Include parent class.
 **/
class ContentList extends \OxidEsales\EshopProfessional\Application\Controller\Admin\ContentList
{
    /**
     * @inheritdoc
     */
    protected function _prepareWhereQuery($aWhere, $sqlFull)
    {
        $sql = parent::_prepareWhereQuery($aWhere, $sqlFull);
        $viewName = getviewName("oxcontents");

        //load only for active shop
        $sql .= " and {$viewName}.oxshopid = '" . $this->getConfig()->getShopId() . "'";

        return $sql;
    }
}
