<?php
/**
 * This Software is the property of OXID eSales and is protected
 * by copyright law - it is NOT Freeware.
 *
 * Any unauthorized use of this software without a valid license key
 * is a violation of the license agreement and will be prosecuted by
 * civil and criminal law.
 *
 * @link      http://www.oxid-esales.com
 * @copyright (C) OXID eSales AG 2003-2016
 * @version   OXID eShop EE
 */

namespace OxidEsales\EshopEnterprise\Application\Controller\Admin;

use oxRegistry;

/**
 * @inheritdoc
 */
class AdminController extends \OxidEsales\EshopProfessional\Application\Controller\Admin\AdminController
{
    /**
     * @inheritdoc
     */
    public function __construct()
    {
        parent::__construct();

        $config = $this->getConfig();
        if ($shop = $this->_getEditShop($config->getShopId())) {
            $config->setSerial($shop->oxshops__oxserial->value);
        }
    }

    /**
     * @inheritdoc
     */
    public function init()
    {
        parent::init();

        $config = $this->getConfig();

        $this->_aViewData['allowSharedEdit'] = $config->getConfigParam('blAllowSharedEdit');
        $this->_aViewData['malladmin'] = $config->getConfigParam('blAllowSharedEdit');
    }

    /**
     * @inheritdoc
     */
    public function resetContentCache($forceReset = null)
    {
        parent::resetContentCache($forceReset);

        $needCleanupOnLogout = $this->getConfig()->getConfigParam('blClearCacheOnLogout');

        if (!$needCleanupOnLogout || $forceReset) {
            $this->resetCaches();
        }
    }

    /**
     * @inheritdoc
     */
    protected function _allowAdminEdit($userId)
    {
        if (!$userId || $userId == -1) {
            return true;
        }

        $user = oxNew('oxuser');
        $user->load($userId);
        $userRights = $user->oxuser__oxrights->value;
        $userShopId = $user->oxuser__oxshopid->value;

        if (!isset(self::$_sAuthUserRights)) {
            $authUser = oxNew('oxuser');
            $authUser->loadAdminUser();
            self::$_sAuthUserRights = $authUser->oxuser__oxrights->value;
        }

        $isMallAdmin = oxRegistry::getSession()->getVariable('malladmin');

        //allow edit if editable user is not admin, is from the same shop, or we are malladmin
        if ($userRights != 'user' && self::$_sAuthUserRights != $userShopId && !$isMallAdmin) {
            return false;
        }

        //mall admin is not allowed to be edited by shop admin
        if ($userRights == 'malladmin' && !$isMallAdmin) {
            return false;
        }

        return parent::_allowAdminEdit($userId);
    }

    /**
     * Resets cache.
     */
    protected function _resetContentCache()
    {
        parent::_resetContentCache();
        $cache = oxNew('oxCache');
        $cache->reset();
    }

    /**
     * @inheritdoc
     */
    private function resetCaches()
    {
        //reset output cache
        $cache = oxNew('oxCache');
        $cache->reset(false);
    }
}
