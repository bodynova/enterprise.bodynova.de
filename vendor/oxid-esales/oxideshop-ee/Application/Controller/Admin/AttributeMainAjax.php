<?php
/**
 * This Software is the property of OXID eSales and is protected
 * by copyright law - it is NOT Freeware.
 *
 * Any unauthorized use of this software without a valid license key
 * is a violation of the license agreement and will be prosecuted by
 * civil and criminal law.
 *
 * @link      http://www.oxid-esales.com
 * @copyright (C) OXID eSales AG 2003-2016
 * @version   OXID eShop EE
 */

namespace OxidEsales\EshopEnterprise\Application\Controller\Admin;

/**
 * @inheritdoc
 */
class AttributeMainAjax extends \OxidEsales\EshopProfessional\Application\Controller\Admin\AttributeMainAjax
{
    /**
     * @inheritdoc
     */
    public function removeAttrArticle()
    {
        $categoriesIds = $this->_getActionIds('oxobject2attribute.oxid');
        $objectToAttributeView = $this->_getViewName('oxobject2attribute');
        if (\oxRegistry::getConfig()->getRequestParameter('all')) {
            $query = "SELECT $objectToAttributeView.`oxobjectid` " . $this->_getQuery();
        } else {
            $chosenCategories = implode(", ", \oxDb::getDb()->quoteArray($categoriesIds));
            $query = "SELECT `oxobject2attribute`.`oxobjectid` FROM `oxobject2attribute` " .
                "WHERE `oxobject2attribute`.`oxid` in (" . $chosenCategories . ") ";
        }

        $articleIds = \oxDb::getDb()->getCol($query);

        foreach ($articleIds as $articleId) {
            $article = oxNew("oxArticle");
            $article->setId($articleId);
            $article->executeDependencyEvent();
        }

        parent::removeAttrArticle();
    }

    /**
     * @inheritdoc
     */
    protected function onArticleAddToAttributeList($articleId)
    {
        $article = oxNew("oxArticle");
        $article->load($articleId);
        $article->executeDependencyEvent();
    }
}
