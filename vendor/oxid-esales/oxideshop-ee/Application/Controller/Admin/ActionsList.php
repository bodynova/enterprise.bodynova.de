<?php
/**
 * This Software is the property of OXID eSales and is protected
 * by copyright law - it is NOT Freeware.
 *
 * Any unauthorized use of this software without a valid license key
 * is a violation of the license agreement and will be prosecuted by
 * civil and criminal law.
 *
 * @link      http://www.oxid-esales.com
 * @copyright (C) OXID eSales AG 2003-2016
 * @version   OXID eShop EE
 */

namespace OxidEsales\EshopEnterprise\Application\Controller\Admin;

/**
 * @inheritdoc
 */
class ActionsList extends \OxidEsales\EshopProfessional\Application\Controller\Admin\ActionsList
{
    /**
     * @inheritdoc
     */
    protected function _prepareWhereQuery($aWhere, $sqlFull)
    {
        $sQ = parent::_prepareWhereQuery($aWhere, $sqlFull);

        $sTable = getViewName("oxactions");
        $iShopId = $this->getConfig()->getShopId();
        $sQ .= " and ( {$sTable}.oxtype = 0 or ( {$sTable}.oxtype != 0 and {$sTable}.oxshopid = '{$iShopId}') ) ";

        return $sQ;
    }
}
