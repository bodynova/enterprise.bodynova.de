<?php
/**
 * This Software is the property of OXID eSales and is protected
 * by copyright law - it is NOT Freeware.
 *
 * Any unauthorized use of this software without a valid license key
 * is a violation of the license agreement and will be prosecuted by
 * civil and criminal law.
 *
 * @link      http://www.oxid-esales.com
 * @copyright (C) OXID eSales AG 2003-2016
 * @version   OXID eShop EE
 */

namespace OxidEsales\EshopEnterprise\Application\Controller\Admin;

/**
 * @inheritdoc
 */
class DiscountCategoriesAjax extends \OxidEsales\EshopProfessional\Application\Controller\Admin\DiscountCategoriesAjax
{
    /**
     * @inheritdoc
     */
    public function removeDiscCat()
    {
        parent::removeDiscCat();

        $discount = oxNew('oxDiscount');
        $discount->setId($this->getConfig()->getRequestParameter('oxid'));
        $discount->executeDependencyEvent();
    }

    /**
     * @inheritdoc
     */
    public function addDiscCat()
    {
        parent::addDiscCat();

        $discountId = $this->getConfig()->getRequestParameter('synchoxid');
        $discount = oxNew('oxDiscount');
        $discount->setId($discountId);
        $discount->executeDependencyEvent();

        $categoryIds = $this->_getActionIds('oxcategories.oxid');
        $category = oxNew('oxCategory');
        $category->executeDependencyEvent($categoryIds);
    }
}
