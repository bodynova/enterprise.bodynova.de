<?php
/**
 * This Software is the property of OXID eSales and is protected
 * by copyright law - it is NOT Freeware.
 *
 * Any unauthorized use of this software without a valid license key
 * is a violation of the license agreement and will be prosecuted by
 * civil and criminal law.
 *
 * @link      http://www.oxid-esales.com
 * @copyright (C) OXID eSales AG 2003-2016
 * @version   OXID eShop EE
 */

namespace OxidEsales\EshopEnterprise\Application\Controller\Admin;

/**
 * @inheritdoc
 */
class ActionsMainAjax extends \OxidEsales\EshopProfessional\Application\Controller\Admin\ActionsMainAjax
{
    /**
     * @inheritdoc
     */
    public function removeArtFromAct()
    {
        parent::removeArtFromAct();

        $oPromotion = oxNew("oxActions");
        $oPromotion->executeDependencyEvent();
    }


    /**
     * @inheritdoc
     */
    public function setSorting()
    {
        parent::setSorting();

        $oPromotion = oxNew("oxActions");
        $oPromotion->executeDependencyEvent();
    }

    /**
     * @inheritdoc
     */
    public function addArtToAct()
    {
        $articleAdded = parent::addArtToAct();
        if ($articleAdded) {
            $oPromotion = oxNew("oxActions");
            $oPromotion->executeDependencyEvent();
        }

        return $articleAdded;
    }
}
