<?php
/**
 * This Software is the property of OXID eSales and is protected
 * by copyright law - it is NOT Freeware.
 *
 * Any unauthorized use of this software without a valid license key
 * is a violation of the license agreement and will be prosecuted by
 * civil and criminal law.
 *
 * @link      http://www.oxid-esales.com
 * @copyright (C) OXID eSales AG 2003-2016
 * @version   OXID eShop EE
 */

namespace OxidEsales\EshopEnterprise\Application\Controller\Admin;

/**
 * @inheritdoc
 */
class ArticleMain extends \OxidEsales\EshopProfessional\Application\Controller\Admin\ArticleMain
{
    /**
     * Copies article (with all parameters) to this shop.
     */
    public function cloneArticle()
    {
        if ($sOldId = $this->getEditObjectId()) {
            $sNewId = $this->copyArticle();

            $sNewArticle = oxNew("oxArticle");
            $sNewArticle->load($sNewId);
            $sNewArticle->oxarticles__oxshopid->setValue($this->getConfig()->getShopId());
            $sNewArticle->save();

            $this->setEditObjectId($sNewId);
        }
    }

    /**
     * @inheritdoc
     */
    protected function updateArticle($article, $oxId)
    {
        $article = parent::updateArticle($article, $oxId);
        // Set access field properties to prevent derived articles for editing.
        if ($article->isDerived()) {
            $this->_aViewData["readonly"] = true;
        }

        return $article;
    }

    /**
     * @inheritdoc
     */
    protected function formQueryForCopyingToCategory($newArticleId, $sUid, $sCatId, $sTime)
    {
        $database = \oxDb::getDb();
        $config = $this->getConfig();
        $shopId = $config->getShopId();
        $sql = "insert into oxobject2category (oxid, oxobjectid, oxcatnid, oxshopid, oxtime) " .
            "VALUES (" . $database->quote($sUid) . ", " . $database->quote($newArticleId) . ", " .
            $database->quote($sCatId) . ", " . $database->quote($shopId) . ", " . $database->quote($sTime) . ") ";

        return $sql;
    }

    /**
     * @inheritdoc
     */
    protected function updateBase($base)
    {
        $base = parent::updateBase($base);
        $config = $this->getConfig();
        $shopId = $config->getShopId();
        $base->oxobject2category__oxshopid = new \oxField($shopId);

        return $base;
    }
}
