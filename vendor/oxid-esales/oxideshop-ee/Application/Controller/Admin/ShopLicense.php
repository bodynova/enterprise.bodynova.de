<?php
/**
 * This Software is the property of OXID eSales and is protected
 * by copyright law - it is NOT Freeware.
 *
 * Any unauthorized use of this software without a valid license key
 * is a violation of the license agreement and will be prosecuted by
 * civil and criminal law.
 *
 * @link      http://www.oxid-esales.com
 * @copyright (C) OXID eSales AG 2003-2016
 * @version   OXID eShop EE
 */

namespace OxidEsales\EshopEnterprise\Application\Controller\Admin;

/**
 * @inheritdoc
 */
class ShopLicense extends \OxidEsales\EshopProfessional\Application\Controller\Admin\ShopLicense
{
    /**
     * @inheritdoc
     */
    private $versionCheckLink = 'http://admin.oxid-esales.com/EE/onlinecheck.php';

    /**
     * @inheritdoc
     */
    public function render()
    {
        $templateName = parent::render();

        //TV2007-02-27
        $allowedMandateCount = 0;
        $config = $this->getConfig();
        $serialsList = $config->getConfigParam('aSerials');

        $serial = $config->getSerial(true);
        $serial->disableCacheModules();

        if (is_array($serialsList)) {
            foreach ($serialsList as $serialKey) {
                $allowedMandateCount += $serial->getMaxShops($serialKey);
            }
        }
        if ($allowedMandateCount > 100000000) {
            $allowedMandateCount = 'unlimited';
        }
        $this->_aViewData['iAllowedMandateCount'] = $allowedMandateCount;
        $this->_aViewData['iUsedMandateCount'] = $config->getMandateCount();

        return $templateName;
    }
}
