<?php
/**
 * This Software is the property of OXID eSales and is protected
 * by copyright law - it is NOT Freeware.
 *
 * Any unauthorized use of this software without a valid license key
 * is a violation of the license agreement and will be prosecuted by
 * civil and criminal law.
 *
 * @link      http://www.oxid-esales.com
 * @copyright (C) OXID eSales AG 2003-2016
 * @version   OXID eShop EE
 */

namespace OxidEsales\EshopEnterprise\Application\Controller\Admin;

use oxCategory;

/**
 * Admin article categories mall manager.
 * There is possibility to change category inheritanve.
 * Admin Menu: Manage Products -> Categories -> Mall.
 */
class CategoryMall extends AdminMall
{
    /**
     * DB table used for multiple shops we are going to deal with
     */
    protected $_sMallTable = 'oxcategories';

    /**
     * Class name of object to load
     */
    protected $_sObjectClassName = 'oxcategory';

    /**
     * @var oxCategory $_oCategory category object
     */
    protected $_oCategory = null;

    /**
     * Loads article category data, passes it to Smarty engine, returns
     * name of template file 'category_mall_nonparent.tpl'.
     *
     * @return string
     */
    public function render()
    {
        $template = parent::render();
        $category = $this->_getSelectedItem();
        $this->_aViewData['edit'] = $category;
        if ($category->oxcategories__oxparentid->value != 'oxrootid') {
            $template = 'category_mall_nonparent.tpl';
        }

        return $template;
    }

    /**
     * Assigns record information in multiple shop field
     */
    public function assignToSubshops()
    {
        $category = $this->_getSelectedItem();
        if (!is_null($category) && $category->oxcategories__oxparentid->value == 'oxrootid') {
            parent::assignToSubshops();
        }
    }

    /**
     * Loads selected item using oxBase
     *
     * @return oxCategory
     */
    protected function _getSelectedItem()
    {
        if (is_null($this->_oCategory)) {
            $categoryId = $this->getEditObjectId();
            $this->_oCategory = oxNew('oxCategory');
            $this->_oCategory->load($categoryId);
        }

        return $this->_oCategory;
    }
}
