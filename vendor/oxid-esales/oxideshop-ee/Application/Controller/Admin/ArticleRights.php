<?php
/**
 * This Software is the property of OXID eSales and is protected
 * by copyright law - it is NOT Freeware.
 *
 * Any unauthorized use of this software without a valid license key
 * is a violation of the license agreement and will be prosecuted by
 * civil and criminal law.
 *
 * @link      http://www.oxid-esales.com
 * @copyright (C) OXID eSales AG 2003-2016
 * @version   OXID eShop EE
 */
namespace OxidEsales\EshopEnterprise\Application\Controller\Admin;

use oxRegistry;

/**
 * Admin article categories thumbnail manager.
 * Category thumbnail manager (Previews assigned pictures).
 * Admin Menu: Manage Products -> Categories -> Thumbnail.
 */
class ArticleRights extends \OxidEsales\EshopProfessional\Application\Controller\Admin\AdminDetailsController
{
    /**
     * Loads category object, passes it to Smarty engine and returns name
     * of template file "category_pictures.tpl".
     *
     * @return string
     */
    public function render()
    {
        parent::render();

        $article = oxNew("oxArticle");
        $this->_aViewData["edit"] = $article;

        $articleId = $this->getEditObjectId();
        if ($articleId != "-1" && isset($articleId)) {
            // load object
            $article->load($articleId);

            //disable for derived articles
            if ($article->isDerived()) {
                $this->_aViewData['readonly'] = true;
            }
        }

        // 1 - visible articles
        // 2 - buyable articles
        $whatArticlesToDisplay = oxRegistry::getConfig()->getRequestParameter("aoc");

        if ($whatArticlesToDisplay == 1) {
            $articleRightsVisibleAjax = oxNew('article_rights_visible_ajax');
            $this->_aViewData['oxajax'] = $articleRightsVisibleAjax->getColumns();

            return "popups/article_rights_visible.tpl";
        } else if ($whatArticlesToDisplay == 2) {
            $articleRightsBuyableAjax = oxNew('article_rights_buyable_ajax');
            $this->_aViewData['oxajax'] = $articleRightsBuyableAjax->getColumns();

            return "popups/article_rights_buyable.tpl";
        }

        return "article_rights.tpl";
    }
}
