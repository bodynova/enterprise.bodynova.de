<?php
/**
 * This Software is the property of OXID eSales and is protected
 * by copyright law - it is NOT Freeware.
 *
 * Any unauthorized use of this software without a valid license key
 * is a violation of the license agreement and will be prosecuted by
 * civil and criminal law.
 *
 * @link      http://www.oxid-esales.com
 * @copyright (C) OXID eSales AG 2003-2016
 * @version   OXID eShop EE
 */
namespace OxidEsales\EshopEnterprise\Application\Controller\Admin;

/**
 * @inheritdoc
 */
class NavigationController extends \OxidEsales\EshopProfessional\Application\Controller\Admin\NavigationController
{
    /**
     * @inheritdoc
     */
    public function chshp()
    {
        parent::chshp();

        if ($rights = $this->getRights()) {
            $navigation = $this->getNavigation();
            $listView = $this->_aViewData['listview'];

            // checking list view
            $rightsIndex = $rights->getViewRightsIndex($navigation->getClassId($listView));
            if ($rightsIndex !== null && $rightsIndex < RIGHT_VIEW) {
                $this->_aViewData['listview'] = null;
                $this->_aViewData['editview'] = null;
                $this->_aViewData['actedit'] = null;
            } else {
                $navigation->markNodeActive($listView);
                // checking edit view
                $rightsIndex = $rights->getViewRightsIndex($navigation->getClassId($this->_aViewData['editview']));
                if ($rightsIndex !== null && $rightsIndex < RIGHT_VIEW) {
                    $this->_aViewData['editview'] = $navigation->getActiveTab($listView, 0);
                    $this->_aViewData['actedit'] = 0;
                }
            }
        }
    }
}
