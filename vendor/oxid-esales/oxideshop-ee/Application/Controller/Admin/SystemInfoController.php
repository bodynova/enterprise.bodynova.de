<?php
/**
 * This Software is the property of OXID eSales and is protected
 * by copyright law - it is NOT Freeware.
 *
 * Any unauthorized use of this software without a valid license key
 * is a violation of the license agreement and will be prosecuted by
 * civil and criminal law.
 *
 * @link      http://www.oxid-esales.com
 * @copyright (C) OXID eSales AG 2003-2016
 * @version   OXID eShop EE
 */

namespace OxidEsales\EshopEnterprise\Application\Controller\Admin;

/**
 * @inheritdoc
 */
class SystemInfoController extends \OxidEsales\EshopProfessional\Application\Controller\Admin\SystemInfoController
{
    /**
     * @inheritdoc
     */
    protected function isClassVariableVisible($varName)
    {
        $result = parent::isClassVariableVisible($varName);

        // only if visible by parent, check for more conditions
        if ($result) {
            $skipNames = array(
                'oActView',
                'oRRoles',
            );
            $result = !in_array($varName, $skipNames);
        }

        return $result;
    }
}
