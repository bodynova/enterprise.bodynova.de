<?php
/**
 * This Software is the property of OXID eSales and is protected
 * by copyright law - it is NOT Freeware.
 *
 * Any unauthorized use of this software without a valid license key
 * is a violation of the license agreement and will be prosecuted by
 * civil and criminal law.
 *
 * @link      http://www.oxid-esales.com
 * @copyright (C) OXID eSales AG 2003-2016
 * @version   OXID eShop EE
 */

namespace OxidEsales\EshopEnterprise\Application\Controller\Admin;

use oxRegistry;
use oxDb;

/**
 * Admin shop system setting manager.
 * Collects shop system settings, updates it on user submit, etc.
 * Admin Menu: Main Menu -> Core Settings -> System.
 */
class ShopMall extends \OxidEsales\EshopCommunity\Application\Controller\Admin\ShopConfiguration
{
    /**
     * Executes parent method parent::render() and returns name of template
     * file "shop_mall.tpl".
     *
     * @return string
     */
    public function render()
    {
        parent::render();

        $config = $this->getConfig();
        $currencies = $config->getCurrencyArray();

        $this->_aViewData['defCur'] = $currencies[0]->name;

        //preventing edit for anyone except malladmin
        if (!oxRegistry::getSession()->getVariable("malladmin")) {
            $this->_aViewData["readonly"] = true;
        }

        //sometimes we may want to prevent showing regenerate views button
        //but up to 2006-10-22 (2.7.0.2) it is not used anywhere
        $showUpdateViews = $config->getConfigParam('blShowUpdateViews');
        $this->_aViewData['showViewUpdate'] = (isset($showUpdateViews) && !$showUpdateViews) ? false : true;
        $this->_aViewData['showInheritanceUpdate'] = false;

        $db = oxDb::getDb();
        $selectShopParentQuery = "select oxparentid, oxismultishop from oxshops where oxid = " . $db->quote($this->getEditObjectId());
        $shopParent = $db->select($selectShopParentQuery);
        if ($shopParent && $shopParent->count() > 0) {
            if ($shopParent->fields[0] && !$shopParent->fields[1]) {
                $this->_aViewData['showInheritanceUpdate'] = true;
            }
        }

        return "shop_mall.tpl";
    }

    /**
     * Saves changed shop configuration parameters.
     *
     * @return bool
     */
    public function save()
    {
        $saveSuccess = false;
        //preventing edit for anyone except malladmin
        if (oxRegistry::getSession()->getVariable("malladmin")) {
            $saveSuccess = parent::save();
        }

        return $saveSuccess;
    }

    /**
     * Performs full view update
     */
    public function updateViews()
    {
        //preventing edit for anyone except malladmin
        if (oxRegistry::getSession()->getVariable("malladmin")) {
            $metaData = oxNew('oxDbMetaDataHandler');
            $this->_aViewData["blViewSuccess"] = $metaData->updateViews();
        }
    }

    /**
     * Changes inheritance information
     */
    public function changeInheritance()
    {
        $config = $this->getConfig();
        $multiShopTables = $config->getConfigParam('aMultiShopTables');
        $editShopId = $config->getRequestParameter("oxid");


        //Saving config vars
        $confBools = $config->getRequestParameter("confbools");
        if (isset($confBools['blMallInherit_oxdelivery'])) {
            //copying delivery parameteres to deliveryset parameters
            $confBools['blMallInherit_oxdeliveryset'] = $confBools['blMallInherit_oxdelivery'];
        }

        //detect only changed values
        $elementWhiteList = $this->_getChangedMultishopTableValues($confBools, $editShopId);

        $_REQUEST['confbools'] = $confBools;
        $_GET['confbools'] = $confBools;
        $_POST['confbools'] = $confBools;

        //save the config values
        $this->save();

        //updating shop inheritance information according to saved config options
        $shop = $this->_getEditShop($editShopId);
        $shop->setMultiShopTables($multiShopTables);
        $shop->updateInheritance($elementWhiteList);

        //regenerating shop views
        $mallInherit = array();
        foreach ($multiShopTables as $tableName) {
            $mallInherit[$tableName] = $config->getShopConfVar('blMallInherit_' . $tableName, $shop->sOXID);
        }
        $shop->generateViews(false, $mallInherit);

        //remove unused oxfield2shop values
        $field2Shop = oxNew('oxfield2shop');
        $field2Shop->cleanMultishopFields($shop->sOXID);
    }

    /**
     * Filters only changed values from $aConfBools. And returns changed mulitshop table name array for given shop.
     *
     * @param array $confBools  New Conf var array
     * @param int   $editShopId Shop id
     *
     * @return array
     */
    protected function _getChangedMultishopTableValues($confBools, $editShopId)
    {
        $changed = array();

        $config = $this->getConfig();
        $multiShopTables = $config->getConfigParam('aMultiShopTables');

        foreach ($multiShopTables as $table) {
            $confVarName = $this->_getMultishopInheritConfigVarName($table);
            if (isset($confBools[$confVarName])) {
                $confVar = $confBools[$confVarName] == "true";
                $savedVarValue = $config->getShopConfVar($confVarName, $editShopId);
                if ($confVar != $savedVarValue) {
                    $changed[] = $table;
                }
            }
        }

        return $changed;

    }

    /**
     * Returns "Inherit all" config var name for given table
     *
     * @param string $tableName Table name
     *
     * @return string
     */
    protected function _getMultishopInheritConfigVarName($tableName)
    {
        $confVarName = 'blMallInherit_' . $tableName;

        if ($tableName == "oxcategories") {
            //categories table is an exception
            return "blMultishopInherit_oxcategories";
        }

        return $confVarName;
    }
}
