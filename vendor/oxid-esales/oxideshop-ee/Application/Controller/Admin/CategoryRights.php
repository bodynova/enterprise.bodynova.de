<?php
/**
 * This Software is the property of OXID eSales and is protected
 * by copyright law - it is NOT Freeware.
 *
 * Any unauthorized use of this software without a valid license key
 * is a violation of the license agreement and will be prosecuted by
 * civil and criminal law.
 *
 * @link      http://www.oxid-esales.com
 * @copyright (C) OXID eSales AG 2003-2016
 * @version   OXID eShop EE
 */

namespace OxidEsales\EshopEnterprise\Application\Controller\Admin;

use oxRegistry;

/**
 * Admin article categories thumbnail manager.
 * Category thumbnail manager (Previews assigned pictures).
 * Admin Menu: Manage Products -> Categories -> Thumbnail.
 */
class CategoryRights extends \OxidEsales\EshopProfessional\Application\Controller\Admin\AdminDetailsController
{
    /**
     * Loads category object, passes it to Smarty engine and returns name
     * of template file "category_pictures.tpl".
     *
     * @return string
     */
    public function render()
    {
        parent::render();

        $this->_aViewData['edit'] = $category = oxNew('oxCategory');

        $oxId = $this->getEditObjectId();
        if ($oxId != "-1" && isset($oxId)) {
            // load object
            $category->load($oxId);

            //Disable editing for derived items
            if ($category->blIsDerived) {
                $this->_aViewData['readonly'] = true;
            }
        }

        $aoc = oxRegistry::getConfig()->getRequestParameter("aoc");
        if ($aoc == 1) {
            $oCategoryRightVisibleAjax = oxNew('category_rights_visible_ajax');
            $this->_aViewData['oxajax'] = $oCategoryRightVisibleAjax->getColumns();

            return "popups/category_rights_visible.tpl";
        } elseif ($aoc == 2) {
            $categoryRightBuyableAjax = oxNew('category_rights_buyable_ajax');
            $this->_aViewData['oxajax'] = $categoryRightBuyableAjax->getColumns();

            return "popups/category_rights_buyable.tpl";
        }

        return "category_rights.tpl";
    }
}
