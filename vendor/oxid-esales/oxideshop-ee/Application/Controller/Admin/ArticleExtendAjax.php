<?php
/**
 * This Software is the property of OXID eSales and is protected
 * by copyright law - it is NOT Freeware.
 *
 * Any unauthorized use of this software without a valid license key
 * is a violation of the license agreement and will be prosecuted by
 * civil and criminal law.
 *
 * @link      http://www.oxid-esales.com
 * @copyright (C) OXID eSales AG 2003-2016
 * @version   OXID eShop EE
 */

namespace OxidEsales\EshopEnterprise\Application\Controller\Admin;

/**
 * @inheritdoc
 */
class ArticleExtendAjax extends \OxidEsales\EshopProfessional\Application\Controller\Admin\ArticleExtendAjax
{
    /**
     * @inheritdoc
     */
    public function onCategoriesRemoval($categoriesToRemove, $oxId)
    {
        $category = oxNew('oxCategory');
        $category->executeDependencyEvent($categoriesToRemove);

        $article = oxNew('oxArticle');
        $article->setId($oxId);
        $article->executeDependencyEvent();
    }

    /**
     * @inheritdoc
     */
    protected function updateQueryForRemovingArticleFromCategory($query)
    {
        $shopID = $this->getConfig()->getShopId();
        $query .= " oxshopid = " . \oxDb::getDb()->quote($shopID) . " and";

        return $query;
    }

    /**
     * @inheritdoc
     */
    protected function onCategoriesAdd($categories)
    {
        $category = oxNew('oxCategory');
        $category->executeDependencyEvent($categories);
    }

    /**
     * @inheritdoc
     */
    protected function formQueryToEmbedForUpdatingTime()
    {
        $shopId = $this->getConfig()->getShopId();
        $sqlShopFilter = "and oxshopid = {$shopId}";

        return $sqlShopFilter;
    }

    /**
     * @inheritdoc
     */
    protected function formQueryToEmbedForSettingCategoryAsDefault()
    {
        $shopId = $this->getConfig()->getShopId();
        $sqlShopFilter = "and oxshopid = {$shopId}";

        return $sqlShopFilter;
    }
}
