<?php
/**
 * This Software is the property of OXID eSales and is protected
 * by copyright law - it is NOT Freeware.
 *
 * Any unauthorized use of this software without a valid license key
 * is a violation of the license agreement and will be prosecuted by
 * civil and criminal law.
 *
 * @link      http://www.oxid-esales.com
 * @copyright (C) OXID eSales AG 2003-2016
 * @version   OXID eShop EE
 */

namespace OxidEsales\EshopEnterprise\Application\Controller\Admin;

use \oxRegistry;

/**
 * @inheritdoc
 */
class ListComponentAjax extends \OxidEsales\EshopProfessional\Application\Controller\Admin\ListComponentAjax
{
    /**
     * @inheritdoc
     */
    public function processRequest($function = null)
    {
        parent::processRequest($function);

        if ($function) {
            $reverseProxyCache = oxRegistry::get('oxReverseProxyBackend');
            $reverseProxyCache->execute();
        }
    }

    /**
     * @inheritdoc
     */
    protected function _resetContentCache()
    {
        parent::_resetContentCache();

        $cache = oxNew('oxCache');
        $cache->reset();
    }

    /**
     * @inheritdoc
     */
    protected function _resetCaches()
    {
        parent::_resetCaches();

        $cache = oxNew('oxCache');
        $cache->reset(false);
    }
}
