<?php
/**
 * This Software is the property of OXID eSales and is protected
 * by copyright law - it is NOT Freeware.
 *
 * Any unauthorized use of this software without a valid license key
 * is a violation of the license agreement and will be prosecuted by
 * civil and criminal law.
 *
 * @link      http://www.oxid-esales.com
 * @copyright (C) OXID eSales AG 2003-2016
 * @version   OXID eShop EE
 */

namespace OxidEsales\EshopEnterprise\Application\Controller\Admin;

/**
 * @inheritdoc
 */
class UserList extends \OxidEsales\EshopProfessional\Application\Controller\Admin\UserList
{
    /**
     * @inheritdoc
     */
    public function _prepareWhereQuery($conditionList, $queryString)
    {
        $generatedQuery = parent::_prepareWhereQuery($conditionList, $queryString);

        // malladmin stuff
        $user = oxNew('oxuser');
        $user->loadAdminUser();
        if ($user->oxuser__oxrights->value != "malladmin") {
            $generatedQuery .= " and oxuser.oxrights != 'malladmin'";
        }

        if (!$this->getConfig()->getConfigParam('blMallUsers')) {
            $generatedQuery .= " and oxshopid = '" . $this->getConfig()->getShopId() . "' ";
        }

        return $generatedQuery;
    }

    /**
     * Builds and returns SQL query string for user list loading
     *
     * @param mixed $oListObject list main object
     *
     * @return string
     */
    protected function _buildSelectString($oListObject = null)
    {
        $oBase = oxNew('oxBase');
        $oBase->init($oListObject->getCoreTableName());

        if ($this->getConfig()->getConfigParam('blMallUsers')) {
            $oBase->setDisableShopCheck(true);
        }

        return $oBase->buildSelectString(null);
    }
}
