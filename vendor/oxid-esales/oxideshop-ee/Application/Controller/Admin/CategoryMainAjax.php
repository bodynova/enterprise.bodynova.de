<?php
/**
 * This Software is the property of OXID eSales and is protected
 * by copyright law - it is NOT Freeware.
 *
 * Any unauthorized use of this software without a valid license key
 * is a violation of the license agreement and will be prosecuted by
 * civil and criminal law.
 *
 * @link      http://www.oxid-esales.com
 * @copyright (C) OXID eSales AG 2003-2016
 * @version   OXID eShop EE
 */

namespace OxidEsales\EshopEnterprise\Application\Controller\Admin;

use oxDb;
use oxRegistry;

/**
 * @inheritdoc
 */
class CategoryMainAjax extends \OxidEsales\EshopProfessional\Application\Controller\Admin\CategoryMainAjax
{
    /**
     * @inheritdoc
     */
    public function addArticle()
    {
        parent::addArticle();
        $config = $this->getConfig();
        $categoryID = $config->getRequestEscapedParameter('synchoxid');

        $category = oxNew('oxCategory');
        $category->executeDependencyEvent(array($categoryID));
    }

    /**
     * @inheritdoc
     */
    protected function getRemoveCategoryArticlesQueryFilter($categoryID, $prodIds)
    {
        $db = oxDb::getDb();
        $where = parent::getRemoveCategoryArticlesQueryFilter($categoryID, $prodIds);

        $shopID = $this->getConfig()->getShopId();
        $where .= " and oxshopid = " . $db->quote($shopID);

        return $where;
    }

    /**
     * @inheritdoc
     */
    protected function getUpdateOxTimeQueryShopFilter()
    {
        $shopFilterQuery = parent::getUpdateOxTimeQueryShopFilter();

        $shopId = $this->getConfig()->getShopId();
        $shopFilterQuery .= " and oxshopid = {$shopId} ";

        return $shopFilterQuery;
    }

    /**
     * @inheritdoc
     */
    protected function getUpdateOxTimeSqlWhereFilter()
    {
        $shopFilterQuery = parent::getUpdateOxTimeSqlWhereFilter();

        $shopId = $this->getConfig()->getShopId();
        $shopFilterQuery .= "and oxobject2category.oxshopid = {$shopId} ";

        return $shopFilterQuery;
    }

    /**
     * Flush category dependencies when removing articles.
     */
    public function removeArticle()
    {
        parent::removeArticle();

        $categoryID = oxRegistry::getConfig()->getRequestParameter('oxid');

        $category = oxNew('oxCategory');
        $category->executeDependencyEvent(array($categoryID));
    }

    /**
     * @inheritdoc
     */
    protected function removeCategoryArticles($articles, $categoryID)
    {
        parent::removeCategoryArticles($articles, $categoryID);

        $article = oxNew('oxArticle');
        foreach ($articles as $articleId) {
            $article->setId($articleId);
            $article->executeDependencyEvent();
        }
    }
}
