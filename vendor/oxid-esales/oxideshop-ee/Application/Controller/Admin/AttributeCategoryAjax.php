<?php
/**
 * This Software is the property of OXID eSales and is protected
 * by copyright law - it is NOT Freeware.
 *
 * Any unauthorized use of this software without a valid license key
 * is a violation of the license agreement and will be prosecuted by
 * civil and criminal law.
 *
 * @link      http://www.oxid-esales.com
 * @copyright (C) OXID eSales AG 2003-2016
 * @version   OXID eShop EE
 */

namespace OxidEsales\EshopEnterprise\Application\Controller\Admin;

use \oxRegistry;
use \oxDb;

/**
 * @inheritdoc
 */
class AttributeCategoryAjax extends \OxidEsales\EshopProfessional\Application\Controller\Admin\AttributeCategoryAjax
{
    /**
     * @inheritdoc
     */
    public function removeCatFromAttr()
    {
        $aChosenCat = $this->_getActionIds('oxcategory2attribute.oxid');

        if (oxRegistry::getConfig()->getRequestParameter('all')) {
            $sSelectSql = "SELECT `oxcategory2attribute`.`oxobjectid` " . $this->_getQuery();
        } else {
            $sChosenCategories = implode(", ", oxDb::getDb()->quoteArray($aChosenCat));
            $sSelectSql = "SELECT `oxcategory2attribute`.`oxobjectid` FROM `oxcategory2attribute` " .
                "WHERE `oxcategory2attribute`.`oxid` in (" . $sChosenCategories . ") ";
        }
        $aCategoryIds = oxDb::getDb()->getCol($sSelectSql);
        $this->_resetCache($aCategoryIds);

        parent::removeCatFromAttr();
    }

    /**
     * @inheritdoc
     */
    public function addCatToAttr()
    {
        parent::addCatToAttr();

        $aAddCategory = $this->_getActionIds('oxcategories.oxid');
        $this->_resetCache($aAddCategory);
    }

    /**
     * Reset category related cache
     *
     * @param array $aCategoryIds array of category ids
     */
    protected function _resetCache($aCategoryIds)
    {
        $oCategory = oxNew('oxCategory');
        $oCategory->executeDependencyEvent($aCategoryIds);
    }
}
