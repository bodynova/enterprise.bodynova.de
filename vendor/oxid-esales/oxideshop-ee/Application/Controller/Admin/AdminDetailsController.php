<?php
/**
 * This Software is the property of OXID eSales and is protected
 * by copyright law - it is NOT Freeware.
 *
 * Any unauthorized use of this software without a valid license key
 * is a violation of the license agreement and will be prosecuted by
 * civil and criminal law.
 *
 * @link      http://www.oxid-esales.com
 * @copyright (C) OXID eSales AG 2003-2016
 * @version   OXID eShop EE
 */

namespace OxidEsales\EshopEnterprise\Application\Controller\Admin;

/**
 * @inheritdoc
 */
class AdminDetailsController extends \OxidEsales\EshopProfessional\Application\Controller\Admin\AdminDetailsController
{
    /**
     * @inheritdoc
     */
    public function render()
    {
        $parentResult = parent::render();

        // generate help link
        $config = $this->getConfig();
        $documentationDirectory = $config->getConfigParam('sShopDir') . '/documentation/admin';
        if (!is_dir($documentationDirectory)) {
            $languageId = $this->getDocumentationLanguageId();
            $shopVersion = str_replace('EE.', '', $this->_sShopVersion);
            $documentationDirectory = "http://docu.oxid-esales.com/EE/{$shopVersion}/" . $languageId . '/admin';
            $this->_aViewData['sHelpURL'] = $documentationDirectory;
        }

        return $parentResult;
    }
}
