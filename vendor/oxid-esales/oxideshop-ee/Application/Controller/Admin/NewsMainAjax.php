<?php
/**
 * This Software is the property of OXID eSales and is protected
 * by copyright law - it is NOT Freeware.
 *
 * Any unauthorized use of this software without a valid license key
 * is a violation of the license agreement and will be prosecuted by
 * civil and criminal law.
 *
 * @link      http://www.oxid-esales.com
 * @copyright (C) OXID eSales AG 2003-2016
 * @version   OXID eShop EE
 */

namespace OxidEsales\EshopEnterprise\Application\Controller\Admin;

/**
 * @inheritdoc
 *
 * @deprecated since v.5.3.0 (2016-06-17); The Admin Menu: Customer Info -> News feature will be moved to a module in v6.0.0
 *             
 */
class NewsMainAjax extends \OxidEsales\EshopProfessional\Application\Controller\Admin\NewsMainAjax
{
    /**
     * Adds user group for viewing some news.
     */
    public function addGroupToNews()
    {
        parent::addGroupToNews();

        $newsId = $this->getConfig()->getRequestParameter('synchoxid');
        if ($newsId && $newsId != "-1") {
            $news = oxNew("oxNews");
            $news->executeDependencyEvent();
        }
    }
}
