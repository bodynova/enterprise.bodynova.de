<?php
/**
 * This Software is the property of OXID eSales and is protected
 * by copyright law - it is NOT Freeware.
 *
 * Any unauthorized use of this software without a valid license key
 * is a violation of the license agreement and will be prosecuted by
 * civil and criminal law.
 *
 * @link      http://www.oxid-esales.com
 * @copyright (C) OXID eSales AG 2003-2016
 * @version   OXID eShop EE
 */

namespace OxidEsales\EshopEnterprise\Application\Controller\Admin;

use \oxRegistry;

/**
 * @inheritdoc
 */
class ArticleAccessoriesAjax extends \OxidEsales\EshopProfessional\Application\Controller\Admin\ArticleAccessoriesAjax
{
    /**
     * @inheritdoc
     */
    public function removeArticleAcc()
    {
        parent::removeArticleAcc();

        $article = oxNew("oxArticle");
        $article->setId(oxRegistry::getConfig()->getRequestParameter('oxid'));
        $article->executeDependencyEvent();
    }

    /**
     * @inheritdoc
     */
    protected function onArticleAccessoryRelationChange($article)
    {
        parent::onArticleAccessoryRelationChange($article);

        $article->executeDependencyEvent();
    }
}
