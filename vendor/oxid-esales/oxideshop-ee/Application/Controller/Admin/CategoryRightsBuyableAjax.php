<?php
/**
 * This Software is the property of OXID eSales and is protected
 * by copyright law - it is NOT Freeware.
 *
 * Any unauthorized use of this software without a valid license key
 * is a violation of the license agreement and will be prosecuted by
 * civil and criminal law.
 *
 * @link      http://www.oxid-esales.com
 * @copyright (C) OXID eSales AG 2003-2016
 * @version   OXID eShop EE
 */

namespace OxidEsales\EshopEnterprise\Application\Controller\Admin;

use ajaxListComponent;
use oxRegistry;
use oxDb;
use oxUtilsObject;

/**
 * Class manages category rights to buy
 */
class CategoryRightsBuyableAjax extends \OxidEsales\EshopCommunity\Application\Controller\Admin\ListComponentAjax
{
    /**
     * Columns array
     *
     * @var array
     */
    protected $_aColumns = array('container1' => array( // field , table,  visible, multilanguage, ident
        array('oxtitle', 'oxgroups', 1, 0, 0),
        array('oxid', 'oxgroups', 0, 0, 0),
        array('oxrrid', 'oxgroups', 0, 0, 1),
    ),
                                 'container2' => array(
                                     array('oxtitle', 'oxgroups', 1, 0, 0),
                                     array('oxid', 'oxgroups', 0, 0, 0),
                                     array('oxrrid', 'oxgroups', 0, 0, 1),
                                 )
    );

    /**
     * Returns SQL query for data to fetc
     *
     * @return string
     */
    protected function _getQuery()
    {
        // looking for table/view
        $groupTable = $this->_getViewName('oxgroups');
        $action = 2;
        $db = oxDb::getDb();

        $artId = oxRegistry::getConfig()->getRequestParameter('oxid');
        $synchArtId = oxRegistry::getConfig()->getRequestParameter('synchoxid');

        // category selected or not ?
        if (!$artId) {
            $sqlAdd = " from $groupTable where 1 ";
        } else {
            // fetching article RR view index
            $sqlAdd = " from $groupTable, oxobjectrights where ";
            $sqlAdd .= " oxobjectrights.oxobjectid = " . $db->quote($artId) . " and ";
            $sqlAdd .= " oxobjectrights.oxoffset = ( $groupTable.oxrrid div 31 ) and ";
            $sqlAdd .= " oxobjectrights.oxgroupidx & ( 1 << ( $groupTable.oxrrid mod 31 ) ) and oxobjectrights.oxaction = $action ";
        }

        if ($synchArtId && $synchArtId != $artId) {
            $sqlAdd = " from $groupTable left join oxobjectrights on ";
            $sqlAdd .= " oxobjectrights.oxoffset = ($groupTable.oxrrid div 31) and ";
            $sqlAdd .= " oxobjectrights.oxgroupidx & ( 1 << ( $groupTable.oxrrid mod 31 ) ) and oxobjectrights.oxobjectid=" . $db->quote($synchArtId) . " and oxobjectrights.oxaction = $action ";
            $sqlAdd .= " where oxobjectrights.oxobjectid != " . $db->quote($synchArtId) . " or ( oxobjectid is null )";
        }

        return $sqlAdd;
    }

    /**
     * Removing article from Buy Article group list
     */
    public function removeGroupFromCatBuy()
    {
        $config = $this->getConfig();

        $groups = $this->_getActionIds('oxgroups.oxrrid');
        $oxId = oxRegistry::getConfig()->getRequestParameter('oxid');

        $range = oxRegistry::getConfig()->getRequestParameter('oxrrapplyrange');
        $action = 2;

        // removing all
        if (oxRegistry::getConfig()->getRequestParameter('all')) {
            $groupTable = $this->_getViewName('oxgroups');
            $groups = $this->_getAll($this->_addFilter("select $groupTable.oxrrid " . $this->_getQuery()));
        }

        if ($oxId != "-1" && isset($oxId) && count($groups)) {
            $indexes = array();
            foreach ($groups as $RRIdx) {
                $offset = ( int ) ($RRIdx / 31);
                $bitMap = 1 << ($RRIdx % 31);

                // summing indexes
                if (!isset($indexes[$offset])) {
                    $indexes[$offset] = $bitMap;
                } else {
                    $indexes[$offset] = $indexes [$offset] | $bitMap;
                }
            }

            $cat = oxNew("oxCategory");
            $cat->load($oxId);

            $shopID = $config->getShopID();
            $db = oxDb::getDb();

            $object2categoryViewName = $this->_getViewName('oxobject2category');

            // iterating through indexes and applying to (sub)categories R&R
            foreach ($indexes as $offset => $idx) {
                // processing category
                $sqlQuery = "update oxobjectrights set oxgroupidx = oxgroupidx & ~$idx where oxobjectid = '$oxId' and oxoffset = $offset and oxaction = $action ";
                $db->Execute($sqlQuery);

                // processing articles
                $sqlQuery = "update oxobjectrights set oxgroupidx = oxgroupidx & ~$idx where oxaction = $action and oxoffset = $offset and oxobjectid in ( ";
                $sqlQuery .= "select oxobject2category.oxobjectid from  $object2categoryViewName as oxobject2category where oxobject2category.oxcatnid='$oxId' ) ";
                $db->Execute($sqlQuery);

                if ($range) {
                    // processing subcategories
                    $sqlQuery = "update oxobjectrights, oxcategories ";
                    $sqlQuery .= "inner join oxcategories2shop as t2s on t2s.oxmapobjectid = oxcategories.oxmapid where t2s.oxshopid = {$db->quote($shopID)} ";
                    $sqlQuery .= "set oxobjectrights.oxgroupidx = oxobjectrights.oxgroupidx & ~$idx where oxobjectrights.oxoffset = $offset and oxobjectrights.oxaction = $action ";
                    $sqlQuery .= "and oxobjectrights.oxobjectid = oxcategories.oxid and ";
                    $sqlQuery .= "oxcategories.oxleft > " . $cat->oxcategories__oxleft->value . " and oxcategories.oxright < " . $cat->oxcategories__oxright->value . " and ";
                    $sqlQuery .= "oxcategories.oxrootid=" . $db->quote($cat->oxcategories__oxrootid->value) . " ";
                    $db->Execute($sqlQuery);

                    // processing articles
                    $sqlQuery = "update oxobjectrights set oxobjectrights.oxgroupidx = oxobjectrights.oxgroupidx & ~$idx ";
                    $sqlQuery .= "where oxobjectrights.oxaction = $action and oxobjectrights.oxobjectid in ( ";
                    $sqlQuery .= "select oxobject2category.oxobjectid from $object2categoryViewName as oxobject2category ";
                    $sqlQuery .= "left join oxcategories on oxcategories.oxid = oxobject2category.oxcatnid ";
                    $sqlQuery .= "inner join oxcategories2shop as t2s on t2s.oxmapobjectid = oxcategories.oxmapid where t2s.oxshopid = {$db->quote($shopID)} ";
                    $sqlQuery .= "where oxcategories.oxrootid = " . $db->quote($cat->oxcategories__oxrootid->value) . " and ";
                    $sqlQuery .= "oxcategories.oxleft > " . $cat->oxcategories__oxleft->value . " and ";
                    $sqlQuery .= "oxcategories.oxright < " . $cat->oxcategories__oxright->value . " ) ";
                    $db->Execute($sqlQuery);
                }
            }

            // removing cleared
            $sqlQuery = "delete from oxobjectrights where oxgroupidx = 0";
            $db->Execute($sqlQuery);

            $this->flushCategoryDependencies($oxId);
        }
    }

    /**
     * Adding article to Buy Article group list
     */
    public function addGroupToCatBuy()
    {
        $config = $this->getConfig();

        $groups = $this->_getActionIds('oxgroups.oxrrid');
        $oxId = oxRegistry::getConfig()->getRequestParameter('synchoxid');

        $range = oxRegistry::getConfig()->getRequestParameter('oxrrapplyrange');
        $action = 2;

        // adding
        if (oxRegistry::getConfig()->getRequestParameter('all')) {
            $groupTable = $this->_getViewName('oxgroups');
            $groups = $this->_getAll($this->_addFilter("select $groupTable.oxrrid " . $this->_getQuery()));
        }

        if ($oxId != "-1" && isset($oxId) && count($groups)) {
            $indexes = array();
            foreach ($groups as $RRIdx) {
                $offset = ( int ) ($RRIdx / 31);
                $bitMap = 1 << ($RRIdx % 31);

                // summing indexes
                if (!isset($indexes[$offset])) {
                    $indexes[$offset] = $bitMap;
                } else {
                    $indexes[$offset] = $indexes [$offset] | $bitMap;
                }
            }

            $category = oxNew("oxCategory");
            $category->load($oxId);

            $shopID = $config->getShopID();
            $object2categortyViewName = $this->_getViewName('oxobject2category');
            $utilsObject = oxUtilsObject::getInstance();
            $db = oxDb::getDb();

            // iterating through indexes and applying to (sub)categories R&R
            foreach ($indexes as $offset => $idx) {
                // processing category
                $sqlQuery = "insert into oxobjectrights (oxid, oxobjectid, oxgroupidx, oxoffset, oxaction) ";
                $sqlQuery .= "values (" . $db->quote($utilsObject->generateUID()) . ", '$oxId', $idx, $offset,  $action ) on duplicate key update oxgroupidx = (oxgroupidx | $idx ) ";
                $db->Execute($sqlQuery);

                // processing articles
                $sqlQuery = "insert into oxobjectrights (oxid, oxobjectid, oxgroupidx, oxoffset, oxaction) ";
                $sqlQuery .= "select md5( concat( a.oxobjectid, oxobject2category.oxid) ), oxobject2category.oxobjectid, a.oxgroupidx, a.oxoffset, a.oxaction ";
                $sqlQuery .= "from $object2categortyViewName as oxobject2category left join oxobjectrights a on oxobject2category.oxcatnid=a.oxobjectid where oxobject2category.oxcatnid='$oxId' and a.oxaction = $action ";
                $sqlQuery .= "on duplicate key update oxobjectrights.oxgroupidx = (oxobjectrights.oxgroupidx | a.oxgroupidx ) ";
                $db->Execute($sqlQuery);

                if ($range) {
                    // processing subcategories
                    $sqlQuery = "insert into oxobjectrights (oxid, oxobjectid, oxgroupidx, oxoffset, oxaction) ";
                    $sqlQuery .= "select " . $db->quote($utilsObject->generateUID()) . ", oxcategories.oxid, $idx, $offset, $action from oxcategories ";
                    $sqlQuery .= "inner join oxcategories2shop as t2s on t2s.oxmapobjectid = oxcategories.oxmapid where t2s.oxshopid = {$db->quote($shopID)} ";
                    $sqlQuery .= "where oxcategories.oxleft > " . $category->oxcategories__oxleft->value . " and oxcategories.oxright < " . $category->oxcategories__oxright->value . " and ";
                    $sqlQuery .= "oxcategories.oxrootid=" . $db->quote($category->oxcategories__oxrootid->value) . " ";
                    $sqlQuery .= "on duplicate key update oxgroupidx = (oxgroupidx | $idx ) ";
                    $db->Execute($sqlQuery);

                    // processing articles
                    $sqlQuery = "insert into oxobjectrights (oxid, oxobjectid, oxgroupidx, oxoffset, oxaction) ";
                    $sqlQuery .= "select md5( concat( a.oxobjectid, oxobject2category.oxid, a.oxaction, a.oxoffset) ), oxobject2category.oxobjectid, a.oxgroupidx, a.oxoffset, a.oxaction ";
                    $sqlQuery .= "from $object2categortyViewName oxobject2category ";
                    $sqlQuery .= "left join oxcategories on oxcategories.oxid = oxobject2category.oxcatnid ";
                    $sqlQuery .= "left join oxobjectrights a on a.oxobjectid = '$oxId' ";
                    $sqlQuery .= "where oxcategories.oxrootid = " . $db->quote($category->oxcategories__oxrootid->value) . " and ";
                    $sqlQuery .= "oxcategories.oxleft > " . $category->oxcategories__oxleft->value . " and  ";
                    $sqlQuery .= "oxcategories.oxright < " . $category->oxcategories__oxright->value . " and a.oxaction = $action ";
                    $sqlQuery .= "on duplicate key update oxobjectrights.oxgroupidx = (oxobjectrights.oxgroupidx | a.oxgroupidx )";
                    $db->Execute($sqlQuery);
                }
            }

            $this->flushCategoryDependencies($oxId);
        }
    }

    /**
     * @param string $categoryId
     */
    protected function flushCategoryDependencies($categoryId)
    {
        $category = oxNew('oxCategory');
        $category->executeDependencyEvent(array($categoryId));
    }
}
