<?php
/**
 * This Software is the property of OXID eSales and is protected
 * by copyright law - it is NOT Freeware.
 *
 * Any unauthorized use of this software without a valid license key
 * is a violation of the license agreement and will be prosecuted by
 * civil and criminal law.
 *
 * @link      http://www.oxid-esales.com
 * @copyright (C) OXID eSales AG 2003-2016
 * @version   OXID eShop EE
 */
namespace OxidEsales\EshopEnterprise\Application\Controller\Admin;

/**
 * Admin order manager.
 * Sets template, that arranges two other templates ("roles_list.tpl"
 * and "roles_main.tpl") to frame.
 * Admin Menu: Users -> Rights and Roles.
 */
class AdminFrontEndRoles extends \OxidEsales\EshopProfessional\Application\Controller\Admin\AdminView
{
    /**
     * Current class template name.
     *
     * @var string
     */
    protected $_sThisTemplate = 'admin_feroles.tpl';
}
