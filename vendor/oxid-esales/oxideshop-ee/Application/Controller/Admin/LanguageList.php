<?php
/**
 * This Software is the property of OXID eSales and is protected
 * by copyright law - it is NOT Freeware.
 *
 * Any unauthorized use of this software without a valid license key
 * is a violation of the license agreement and will be prosecuted by
 * civil and criminal law.
 *
 * @link      http://www.oxid-esales.com
 * @copyright (C) OXID eSales AG 2003-2016
 * @version   OXID eShop EE
 */

namespace OxidEsales\EshopEnterprise\Application\Controller\Admin;

/**
 * @inheritdoc
 */
class LanguageList extends \OxidEsales\EshopProfessional\Application\Controller\Admin\LanguageList
{
    /**
     * @inheritdoc
     */
    public function deleteEntry()
    {
        $config = $this->getConfig();

        if (!$config->getConfigParam('blAllowSharedEdit')) {
            return;
        }

        parent::deleteEntry();

        $languageData['params'] = $config->getConfigParam('aLanguageParams');

        $languageId = $this->getEditObjectId();
        $baseLanguageId = (int) $languageData['params'][$languageId]['baseId'];

        // resetting all multilanguage DB fields with deleted lang id to default value
        $this->_resetMultiLangDbFields($baseLanguageId);
    }

    /**
     * @inheritdoc
     */
    public function render()
    {
        if (!$this->getConfig()->getConfigParam('blAllowSharedEdit')) {
            $this->_aViewData['readonly'] = true;
        }

        return parent::render();
    }
}
