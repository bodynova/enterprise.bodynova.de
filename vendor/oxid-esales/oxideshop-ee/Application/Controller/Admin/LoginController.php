<?php
/**
 * This Software is the property of OXID eSales and is protected
 * by copyright law - it is NOT Freeware.
 *
 * Any unauthorized use of this software without a valid license key
 * is a violation of the license agreement and will be prosecuted by
 * civil and criminal law.
 *
 * @link      http://www.oxid-esales.com
 * @copyright (C) OXID eSales AG 2003-2016
 * @version   OXID eShop EE
 */

namespace OxidEsales\EshopEnterprise\Application\Controller\Admin;

/**
 * @inheritdoc
 */
class LoginController extends \OxidEsales\EshopProfessional\Application\Controller\Admin\LoginController
{
    /**
     * @inheritdoc
     */
    protected function setShopConfigParameters()
    {
        $config = $this->getConfig();

        $baseShop = oxNew("oxShop");
        $baseShop->load($config->getBaseShopId());

        $serial = $baseShop->oxshops__oxserial->value;
        $config->setConfigParam('sSerialNr', $serial);
        $version = str_replace("EE.", "", $baseShop->oxshops__oxversion->value);

        $this->getViewConfig()->setViewConfigParam('sShopVersion', $version);
    }
}
