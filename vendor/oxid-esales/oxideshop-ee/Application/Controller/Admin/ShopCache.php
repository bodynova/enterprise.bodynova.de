<?php
/**
 * This Software is the property of OXID eSales and is protected
 * by copyright law - it is NOT Freeware.
 *
 * Any unauthorized use of this software without a valid license key
 * is a violation of the license agreement and will be prosecuted by
 * civil and criminal law.
 *
 * @link      http://www.oxid-esales.com
 * @copyright (C) OXID eSales AG 2003-2016
 * @version   OXID eShop EE
 */

namespace OxidEsales\EshopEnterprise\Application\Controller\Admin;

use oxRegistry;

/**
 * Admin shop TERMS manager.
 * Collects shop TERMS information, updates it on user submit, etc.
 * Admin Menu: Main Menu -> Core Settings -> Terms.
 */
class ShopCache extends \OxidEsales\EshopProfessional\Application\Controller\Admin\ShopConfiguration
{
    /**
     * There is Reverse Proxy header which indicate that Reverse Proxy is between.
     *
     * @var bool
     */
    private $_blIsReverseProxyHeader = null;

    /**
     * Executes parent method parent::render(), creates oxshop object and
     * passes it's data to Smarty engine, returns name of template file
     * "shop_cache.tpl".
     *
     * @return string
     */
    public function render()
    {
        parent::render();

        $cache = oxNew('oxCache');

        $totalValidCacheCount = $cache->getTotalCacheCount();
        $totalExpiredCacheCount = $cache->getTotalCacheCount(true);
        $totalCacheCount = $totalValidCacheCount + $totalExpiredCacheCount;

        // Cache Hit/Miss Ratio.
        $totalValidCacheHitCount = $cache->getTotalCacheHits();
        $totalValidCacheMissCount = $totalValidCacheCount;

        if ($totalValidCacheHitCount + $totalValidCacheMissCount > 0) {
            $totalValidCacheHitRatio = $totalValidCacheHitCount /
                ($totalValidCacheHitCount + $totalValidCacheMissCount);
            $totalValidCacheMissRatio = $totalValidCacheMissCount /
                ($totalValidCacheHitCount + $totalValidCacheMissCount);

            $totalValidCacheHitPercent = $totalValidCacheHitRatio * 100;
            $totalValidCacheMissPercent = $totalValidCacheMissRatio * 100;
        } else {
            $totalValidCacheHitCount = 0;
            $totalValidCacheMissCount = 0;

            $totalValidCacheHitRatio = 0;
            $totalValidCacheMissRatio = 0;
            $totalValidCacheHitPercent = 0;
            $totalValidCacheMissPercent = 0;
        }

        $this->_aViewData["TotalValidCacheHitCount"] = $totalValidCacheHitCount;
        $this->_aViewData["TotalValidCacheMissCount"] = $totalValidCacheMissCount;

        $this->_aViewData["TotalValidCacheHitRatio"] = round($totalValidCacheHitRatio, 2);
        $this->_aViewData["TotalValidCacheMissRatio"] = round($totalValidCacheMissRatio, 2);

        $this->_aViewData["TotalValidCacheHitPercent"] = round($totalValidCacheHitPercent, 2);
        $this->_aViewData["TotalValidCacheMissPercent"] = round($totalValidCacheMissPercent, 2);

        $this->_aViewData["TotalValidCacheCount"] = $totalValidCacheCount;
        $this->_aViewData["TotalExpiredCacheCount"] = $totalExpiredCacheCount;
        $this->_aViewData["TotalCacheCount"] = $totalCacheCount;


        $totalValidCacheSize = $cache->getTotalCacheSize();
        $totalExpiredCacheSize = $cache->getTotalCacheSize(true);

        if (!$totalValidCacheSize) {
            $totalValidCacheSize = 0;
        }
        if (!$totalExpiredCacheSize) {
            $totalExpiredCacheSize = 0;
        }

        $totalCacheSize = $totalValidCacheSize + $totalExpiredCacheSize;

        $this->_aViewData["TotalValidCacheSize"] = $totalValidCacheSize;
        $this->_aViewData["TotalExpiredCacheSize"] = $totalExpiredCacheSize;
        $this->_aViewData["TotalCacheSize"] = $totalCacheSize;
        $this->_aViewData["ActiveCacheLifetime"] = $cache->getCacheLifeTime();

        $backends = array_flip($cache->getAvailableBackends());
        foreach (array_keys($backends) as $key) {
            $backends[$key] = false;
        }
        $backends[$cache->getSelectedBackend()] = true;
        $this->_aViewData["aCacheBackends"] = $backends;

        $cacheBackend = oxRegistry::get('oxCacheBackend');
        $this->_aViewData["aCacheConnectors"] = $cacheBackend->getAvailableConnectors();

        // Checking if reverse proxy is active.
        $isFlagEnabled = $this->getConfig()->getSerial()->isFlagEnabled('reverse_proxy');
        $this->_aViewData["blReverseProxyisActive"] = $isFlagEnabled;

        $this->_aViewData["sShopHomeURL"] = $this->getConfig()->getShopURL();

        return "shop_cache.tpl";
    }

    /**
     * Flush allt cache.
     */
    public function flushCache()
    {
        $this->flushDefaultCacheBackend();
        $this->flushReverseProxyBackend('all');
        $this->flushContentCache();
    }

    /**
     * Flush content cache.
     */
    public function flushContentCache()
    {
        $this->resetContentCache(true);
    }

    /**
     * Flushes (Invalidates) all reverse proxy cache, or given cache section.
     *
     * @param string $section Section name.
     */
    public function flushReverseProxyBackend($section = null)
    {
        $reverseProxyBackend = oxRegistry::get('oxReverseProxyBackend');
        $reverseProxyUrlGenerator = oxNew('oxReverseProxyUrlGenerator');

        if (is_null($section)) {
            $reverseProxySection = oxRegistry::getConfig()->getRequestParameter("reverseProxySection");
        } else {
            $reverseProxySection = $section;
        }

        switch ($reverseProxySection) {
            case 'start':
                $reverseProxyUrlGenerator->setStartPage();
                break;

            case 'details':
                $reverseProxyUrlGenerator->setDetails();
                break;

            case 'lists':
                // Flush all category, vendor and manufacturer lists.
                $reverseProxyUrlGenerator->setLists();
                // Flush all search page lists.
                $reverseProxyUrlGenerator->setPage('search');
                // Flush all rss lists.
                $reverseProxyUrlGenerator->setDynamicPage('rss');
                // @deprecated since v5.3 (2016-06-17); Listmania will be moved to an own module.
                // Flush all recommendation lists.
                $reverseProxyUrlGenerator->setDynamicPage('recommlist');
                // END deprecated
                break;

            case 'all':
            default:
                $reverseProxyBackend->setFlush();
                break;
        }

        $reverseProxyBackend->set($reverseProxyUrlGenerator->getUrls());
        $reverseProxyBackend->execute();
    }

    /**
     * Flushes (Invalidates) all default cache backend cache.
     */
    public function flushDefaultCacheBackend()
    {
        $reverseProxyBackend = oxRegistry::get('oxCacheBackend');
        $reverseProxyBackend->flush();
    }
}
