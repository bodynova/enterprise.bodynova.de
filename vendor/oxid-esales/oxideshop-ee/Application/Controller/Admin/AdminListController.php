<?php
/**
 * This Software is the property of OXID eSales and is protected
 * by copyright law - it is NOT Freeware.
 *
 * Any unauthorized use of this software without a valid license key
 * is a violation of the license agreement and will be prosecuted by
 * civil and criminal law.
 *
 * @link      http://www.oxid-esales.com
 * @copyright (C) OXID eSales AG 2003-2016
 * @version   OXID eShop EE
 */
namespace OxidEsales\EshopEnterprise\Application\Controller\Admin;

use OxidEsales\Eshop\Application\Model\Shop;
use oxBase;

/**
 * @inheritdoc
 */
class AdminListController extends \OxidEsales\EshopProfessional\Application\Controller\Admin\AdminListController
{
    /**
     * Unassigns entry from current shop
     */
    public function unassignEntry()
    {
        /** @var oxBase $item */
        $item = oxNew($this->_sListClass);
        $item->load($this->getEditObjectId());

        $shop = $this->_getShopObject();
        $shopIds = $shop->getInheritanceGroup($item->getCoreTableName(), $this->_getShopId());

        $shouldUnassign = $item->unassignFromShop($shopIds);

        // #A - we must reset object ID
        if ($shouldUnassign && isset($_POST["oxid"])) {
            $_POST["oxid"] = -1;
        }

        $this->init();
    }

    /**
     * Returns oxShop object
     *
     * @return Shop
     */
    protected function _getShopObject()
    {
        return oxNew('oxShop');
    }

    /**
     * Returns item shop id
     *
     * @return string
     */
    protected function _getShopId()
    {
        return $this->getConfig()->getShopId();
    }
}
