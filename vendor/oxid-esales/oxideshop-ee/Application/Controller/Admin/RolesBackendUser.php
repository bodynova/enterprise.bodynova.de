<?php
/**
 * This Software is the property of OXID eSales and is protected
 * by copyright law - it is NOT Freeware.
 *
 * Any unauthorized use of this software without a valid license key
 * is a violation of the license agreement and will be prosecuted by
 * civil and criminal law.
 *
 * @link      http://www.oxid-esales.com
 * @copyright (C) OXID eSales AG 2003-2016
 * @version   OXID eShop EE
 */
namespace OxidEsales\EshopEnterprise\Application\Controller\Admin;

use oxRegistry;
use oxField;

/**
 * Admin article main actions manager.
 * There is possibility to change actions description, assign articles to
 * this actions, etc.
 * Admin Menu: Manage Products -> actions -> Main.
 */
class RolesBackendUser extends \OxidEsales\EshopProfessional\Application\Controller\Admin\AdminDetailsController
{
    /**
     * Prepares view data, executes parent::render() and returns name of
     * template "roles_beuser.tpl"
     *
     * @return string
     */
    public function render()
    {
        //allow only mall admin to perform this
        if (!$this->getConfig()->getConfigParam('blAllowSharedEdit')) {
            $this->_aViewData['readonly'] = true;
        }

        parent::render();

        $roleId = $this->getEditObjectId();

        $role = oxNew("oxRole");
        $role->load($roleId);
        $this->_aViewData["edit"] = $role;

        // all user groups
        $userGroups = oxNew("oxList");
        $userGroups->init('oxgroups');
        $userGroups->selectString("SELECT * FROM " . getViewName("oxgroups", $this->_iEditLang));

        $rootGroups = oxNew('oxGroups');
        $rootGroups->oxgroups__oxid = new oxField("");
        $rootGroups->oxgroups__oxtitle = new oxField("-- ");

        // Rebuild list as we need the "no value" entry at the first position.
        $newList = array();
        $newList[] = $rootGroups;

        foreach ($userGroups as $value) {
            $newList[$value->oxgroups__oxid->value] = oxNew('oxGroups');
            $newList[$value->oxgroups__oxid->value]->oxgroups__oxid = new oxField($value->oxgroups__oxid->value);
            $newList[$value->oxgroups__oxid->value]->oxgroups__oxtitle = new oxField($value->oxgroups__oxtitle->value);
        }

        $userGroups = $newList;
        $this->_aViewData["allgroups2"] = $userGroups;

        $aoc = oxRegistry::getConfig()->getRequestParameter("aoc");
        if ($aoc == 1) {
            $rolesBackendGroupsAjax = oxNew('Roles_BEgroups_Ajax');
            $this->_aViewData['oxajax'] = $rolesBackendGroupsAjax->getColumns();

            return "popups/roles_begroups.tpl";
        } elseif ($aoc == 2) {
            $rolesBackendUserAjax = oxNew('Roles_BEuser_Ajax');
            $this->_aViewData['oxajax'] = $rolesBackendUserAjax->getColumns();

            return "popups/roles_beuser.tpl";
        }

        return "roles_beuser.tpl";
    }
}
