<?php
/**
 * This Software is the property of OXID eSales and is protected
 * by copyright law - it is NOT Freeware.
 *
 * Any unauthorized use of this software without a valid license key
 * is a violation of the license agreement and will be prosecuted by
 * civil and criminal law.
 *
 * @link      http://www.oxid-esales.com
 * @copyright (C) OXID eSales AG 2003-2016
 * @version   OXID eShop EE
 */
namespace OxidEsales\EshopEnterprise\Application\Controller\Admin;

use oxDb;
use oxField;

/**
 * Class manages back-end user rights
 */
class RolesBackendUserAjax extends ListComponentAjax
{
    /** @var array Columns array. */
    protected $_aColumns = array(
        // field , table,  visible, multilanguage, id
        'container1' => array(
            array('oxusername', 'oxuser', 1, 0, 0),
            array('oxlname', 'oxuser', 0, 0, 0),
            array('oxfname', 'oxuser', 0, 0, 0),
            array('oxstreet', 'oxuser', 0, 0, 0),
            array('oxstreetnr', 'oxuser', 0, 0, 0),
            array('oxcity', 'oxuser', 0, 0, 0),
            array('oxzip', 'oxuser', 0, 0, 0),
            array('oxfon', 'oxuser', 0, 0, 0),
            array('oxbirthdate', 'oxuser', 0, 0, 0),
            array('oxid', 'oxuser', 0, 0, 1),
        ),
        'container2' => array(
            array('oxusername', 'oxuser', 1, 0, 0),
            array('oxlname', 'oxuser', 0, 0, 0),
            array('oxfname', 'oxuser', 0, 0, 0),
            array('oxstreet', 'oxuser', 0, 0, 0),
            array('oxstreetnr', 'oxuser', 0, 0, 0),
            array('oxcity', 'oxuser', 0, 0, 0),
            array('oxzip', 'oxuser', 0, 0, 0),
            array('oxfon', 'oxuser', 0, 0, 0),
            array('oxbirthdate', 'oxuser', 0, 0, 0),
            array('oxid', 'oxobject2role', 0, 0, 1),
        )
    );

    /**
     * Returns SQL query for data to fetc
     *
     * @return string
     */
    protected function _getQuery()
    {
        $config = $this->getConfig();

        // looking for table/view
        $userViewName = $this->_getViewName('oxuser');
        $database = oxDb::getDb();
        $roleId = $config->getRequestParameter('oxid');
        $syncedRoleId = $config->getRequestParameter('synchoxid');

        if (!$roleId) {
            // performance
            $query = " from $userViewName where 1 ";
            if (!$config->getRequestParameter('blMallUsers')) {
                $query .= " and (oxshopid = '" . $config->getShopId() . "' OR oxrights='malladmin')";
            }
        } elseif ($syncedRoleId && $syncedRoleId != $roleId) {
            // selected group ?
            $query = " from oxobject2group inner join $userViewName on $userViewName.oxid = oxobject2group.oxobjectid ";
            $query .= " where oxobject2group.oxgroupsid = " . $database->quote($roleId);
            if (!$config->getRequestParameter('blMallUsers')) {
                $query .= " and $userViewName.oxshopid = '" . $config->getShopId() . "' ";
            }
        } else {
            $query = " from oxobject2role, $userViewName where oxobject2role.oxtype = 'oxuser' and ";
            $query .= " oxobject2role.oxroleid = " . $database->quote($roleId) . " and $userViewName.oxid=oxobject2role.oxobjectid ";
        }

        if ($syncedRoleId && $syncedRoleId != $roleId) {
            // performance
            $query .= " and $userViewName.oxid not in ( select oxobject2role.oxobjectid  from oxobject2role where oxobject2role.oxtype = 'oxuser' and oxobject2role.oxroleid = " . $database->quote($syncedRoleId) . " ) ";
        }

        return $query;
    }

    /**
     * Removes User from R&R
     */
    public function removeUserFromBeroles()
    {
        $removeGroups = $this->_getActionIds('oxobject2role.oxid');
        if ($this->getConfig()->getRequestParameter('all')) {
            $query = $this->_addFilter("delete oxobject2role.* " . $this->_getQuery());
            oxDb::getDb()->Execute($query);
        } elseif ($removeGroups && is_array($removeGroups)) {
            $query = "delete from oxobject2role where oxobject2role.oxid in (" . implode(", ", oxDb::getDb()->quoteArray($removeGroups)) . ") ";
            oxDb::getDb()->Execute($query);
        }
    }

    /**
     * Adds User to R&R
     */
    public function addUserToBeRoles()
    {
        $chosenUsers = $this->_getActionIds('oxuser.oxid');
        $roleId = $this->getConfig()->getRequestParameter('synchoxid');

        if ($this->getConfig()->getRequestParameter('all')) {
            $userViewName = $this->_getViewName('oxuser');
            $chosenUsers = $this->_getAll($this->_addFilter("select $userViewName.oxid " . $this->_getQuery()));
        }
        if ($roleId && $roleId != "-1" && is_array($chosenUsers)) {
            foreach ($chosenUsers as $chosenUser) {
                $objectToRole = oxNew("oxBase");
                $objectToRole->init("oxobject2role");
                $objectToRole->oxobject2role__oxobjectid = new oxField($chosenUser);
                $objectToRole->oxobject2role__oxroleid = new oxField($roleId);
                $objectToRole->oxobject2role__oxtype = new oxField("oxuser");
                $objectToRole->save();
            }
        }
    }
}
