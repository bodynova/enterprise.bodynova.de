<?php
/**
 * This Software is the property of OXID eSales and is protected
 * by copyright law - it is NOT Freeware.
 *
 * Any unauthorized use of this software without a valid license key
 * is a violation of the license agreement and will be prosecuted by
 * civil and criminal law.
 *
 * @link      http://www.oxid-esales.com
 * @copyright (C) OXID eSales AG 2003-2016
 * @version   OXID eShop EE
 */

namespace OxidEsales\EshopEnterprise\Application\Controller\Admin;

/**
 * @inheritdoc
 */
class DiscountUsersAjax extends \OxidEsales\EshopProfessional\Application\Controller\Admin\DiscountUsersAjax
{
    /**
     * @inheritdoc
     */
    public function removeDiscUser()
    {
        $discount = oxNew('oxDiscount');
        $discount->setId($this->getConfig()->getRequestParameter('oxid'));
        $discount->executeDependencyEvent();

        parent::removeDiscUser();
    }

    /**
     * @inheritdoc
     */
    public function addDiscUser()
    {
        $discount = oxNew('oxDiscount');
        $discount->setId($this->getConfig()->getRequestParameter('synchoxid'));
        $discount->executeDependencyEvent();

        parent::addDiscUser();
    }
}
