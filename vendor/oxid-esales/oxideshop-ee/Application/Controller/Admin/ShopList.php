<?php
/**
 * This Software is the property of OXID eSales and is protected
 * by copyright law - it is NOT Freeware.
 *
 * Any unauthorized use of this software without a valid license key
 * is a violation of the license agreement and will be prosecuted by
 * civil and criminal law.
 *
 * @link      http://www.oxid-esales.com
 * @copyright (C) OXID eSales AG 2003-2016
 * @version   OXID eShop EE
 */

namespace OxidEsales\EshopEnterprise\Application\Controller\Admin;

use oxRegistry;
use oxDb;

/**
 * @inheritdoc
 */
class ShopList extends \OxidEsales\EshopProfessional\Application\Controller\Admin\ShopList
{
    /**
     * Sets SQL query parameters (such as sorting),
     * executes parent method parent::Init().
     */
    public function init()
    {
        parent::Init();

        $database = oxDb::getMaster();

        $itemsList = $this->getItemList();
        //setting isparent param
        foreach ($itemsList as $key => $shop) {
            $query = "select oxid from oxshops where oxparentid = " . $database->quote($key);

            $shop->isparent = $database->getOne($query) ? true : false;
        }
    }

    /**
     * Deletes selected shop files from server.
     *
     * @return null
     */
    public function deleteEntry()
    {
        $config = $this->getConfig();
        $shopId = oxRegistry::getConfig()->getRequestParameter("delshopid");

        // We force reading from master to prevent issues with slow replications or open transactions (see ESDEV-3804).
        $masterDb = oxDb::getMaster();

        //do not delete parent shops
        $query = "select oxid from oxshops where oxparentid = " . $masterDb->quote($shopId);
        if ($masterDb->getOne($query)) {
            return;
        }

        // try to remove directories
        $shopId = strtr($shopId, "\\/", "__");
        $target = $config->getConfigParam('sShopDir') . "/Application/views/" . $shopId;

        // Shom message if deleted shop has custom templates.
        if (is_dir($target)) {
            $excetion = oxNew("oxExceptionToDisplay");
            $excetion->setMessage('CUSTOM_TEMPLATE_EXIST_FOR_DELETED_SHOP');
            oxRegistry::get("oxUtilsView")->addErrorToDisplay($excetion, false);
        }

        $delete = oxNew("oxShop");
        $tables = $config->getConfigParam('aMultiShopTables');
        $delete->setMultiShopTables($tables);

        //disabling deletion for derived items
        if ($delete->isDerived()) {
            return;
        }

        $delete->delete($shopId);

        // Task #1476 Administer attributes
        $this->resetContentCache();
        $this->_blUpdateNav = true;

        $config->setShopId($config->getBaseShopId());
        $this->clearItemList();
        $this->init();
    }

    /**
     * Set to view data if update navigation menu.
     */
    protected function updateNavigation()
    {
        parent::updateNavigation();
        $this->_aViewData['updatenav'] = isset($this->_blUpdateNav) ? $this->_blUpdateNav : ($this->getRights() ? oxRegistry::getConfig()->getRequestParameter('updatenav') : 0);
    }
}
