<?php
/**
 * This Software is the property of OXID eSales and is protected
 * by copyright law - it is NOT Freeware.
 *
 * Any unauthorized use of this software without a valid license key
 * is a violation of the license agreement and will be prosecuted by
 * civil and criminal law.
 *
 * @link      http://www.oxid-esales.com
 * @copyright (C) OXID eSales AG 2003-2016
 * @version   OXID eShop EE
 */

namespace OxidEsales\EshopEnterprise\Application\Controller\Admin;

use oxDb;

/**
 * @inheritdoc
 */
class PriceAlarmMain extends \OxidEsales\EshopProfessional\Application\Controller\Admin\PriceAlarmMain
{
    /**
     * @inheritdoc
     */
    protected function getActivePriceAlarmsCount()
    {
        $config = $this->getConfig();

        // #889C - Netto prices in Admin
        $priceIndex = "";
        if ($config->getConfigParam('blEnterNetPrice')) {
            $priceIndex = " * " . (1 + $config->getConfigParam('dDefaultVAT') / 100);
        }

        $articleViewName = getViewName('oxarticles');
        $shopId = $config->getShopID();

        $query = "
            SELECT COUNT(*)
            FROM oxpricealarm, $articleViewName
            WHERE $articleViewName.oxid = oxpricealarm.oxartid
            AND $articleViewName.oxprice$priceIndex <= oxpricealarm.oxprice
            AND oxpricealarm.oxsended = '000-00-00 00:00:00'
            AND oxpricealarm.oxshopid = $shopId";

        return (int) oxDb::getDb()->getOne($query);
    }
}
