<?php
/**
 * This Software is the property of OXID eSales and is protected
 * by copyright law - it is NOT Freeware.
 *
 * Any unauthorized use of this software without a valid license key
 * is a violation of the license agreement and will be prosecuted by
 * civil and criminal law.
 *
 * @link      http://www.oxid-esales.com
 * @copyright (C) OXID eSales AG 2003-2016
 * @version   OXID eShop EE
 */

namespace OxidEsales\EshopEnterprise\Application\Controller\Admin;

use oxDb;
use oxRegistry;
use oxUtilsObject;
use stdClass;

/**
 * Admin article main actions manager.
 * There is possibility to change actions description, assign articles to
 * this actions, etc.
 * Admin Menu: Manage Products -> actions -> Main.
 */
class RolesFrontendMain extends \OxidEsales\EshopProfessional\Application\Controller\Admin\AdminDetailsController
{
    /**
     * Render.
     *
     * @return string
     */
    public function render()
    {
        parent::render();
        $oxId = $this->_aViewData['oxid'] = $this->getEditObjectId();
        if ($oxId != '-1' && $oxId) {
            $database = oxDb::getDb(oxDB::FETCH_MODE_ASSOC);
            $role = oxNew('oxrole');
            $role->load($oxId);
            $this->_aViewData['edit'] = $role;

            // Loading default menu/tabs data and building logical structure.
            $subQuery = "SELECT oxfield2role.oxidx FROM oxfield2role
                WHERE oxfield2role.oxfieldid = oxrolefields.oxid
                AND oxfield2role.oxroleid = " . $database->quote($oxId);

            $query = "select oxrolefields.oxid, oxrolefields.oxname, oxrolefields.oxparam, ($subQuery) as value "
                  . "from oxrolefields ";
            $stringHandler = getStr();

            $fieldsList = array();
            $executionResult = $database->select($query);
            if ($executionResult != false && $executionResult->count() > 0) {
                while (!$executionResult->EOF) {
                    $fieldsList[$executionResult->fields['oxid']] = new stdClass();
                    $fieldsList[$executionResult->fields['oxid']]->id = $executionResult->fields['oxid'];
                    $fieldsList[$executionResult->fields['oxid']]->name = $executionResult->fields['oxname'];
                    $fieldsList[$executionResult->fields['oxid']]->params = $executionResult->fields['oxparam'];
                    $fieldsList[$executionResult->fields['oxid']]->value = $executionResult->fields['value'];

                    // Protecting from deletion special fields.
                    if (!$stringHandler->preg_match('/^ox_/', $fieldsList[$executionResult->fields['oxid']]->id)) {
                        $fieldsList[$executionResult->fields['oxid']]->deletable = true;
                    }
                    $executionResult->fetchRow();
                }
            }

            // Applying defined options.
            $this->_aViewData['shopfnc'] = $fieldsList;
        }

        return 'roles_femain.tpl';
    }

    /**
     * Saving Shop Role parameters.
     */
    public function save()
    {
        parent::save();

        $config = $this->getConfig();

        $rights = oxRegistry::getConfig()->getRequestParameter('menuright');
        $oxId = $this->getEditObjectId();
        $parameters = oxRegistry::getConfig()->getRequestParameter('editval');

        // Checkbox handling.
        if (!isset($parameters['oxroles__oxactive'])) {
            $parameters['oxroles__oxactive'] = 0;
        }

        // Saving role data.
        $role = oxNew('oxrole');
        if ($oxId != '-1') {
            $role->load($oxId);
        } else {
            $parameters['oxroles__oxid'] = null;
            $parameters['oxroles__oxshopid'] = $config->getShopId();
        }

        $role->assign($parameters);
        $role->save();

        $this->setEditObjectId($role->getId());

        // Saving oxfield2role.
        if (is_array($rights) && count($rights)) {
            $database = oxDb::getDb();
            // Saving or updating configuration.
            foreach ($rights as $fieldId => $value) {
                // We only interpret it as "exclusively...".
                if (!$value['right']) {
                    $query = "DELETE FROM oxfield2role "
                        . "WHERE oxfieldid=" . $database->quote($fieldId) . " "
                        . "AND oxroleid = " . $database->quote($role->oxroles__oxid->value);
                } else {
                    $query = "INSERT INTO oxfield2role ( oxfieldid, oxroleid, oxidx ) VALUES ( "
                        . $database->quote($fieldId)
                        . ", " . $database->quote($role->oxroles__oxid->value)
                        . ", " . $database->quote($value['right'])
                        . " ) ON DUPLICATE KEY UPDATE oxidx=" . $database->quote($value['right']);
                }
                $database->execute($query);
            }
        }
    }

    /**
     * Adds field for R&R.
     */
    public function addField()
    {
        $parameter = oxRegistry::getConfig()->getRequestParameter("oxparam");
        $oxId = $this->getEditObjectId();

        // Saving oxfield2role.
        $role = oxNew("oxrole");
        if ($parameter && $role->load($oxId)) {
            $stringHandler = getStr();

            // Parsing input.
            $name = $stringHandler->preg_replace('/&.*/', '', $parameter);
            $escapedParameter = '';
            if (strpos($parameter, '&') !== false) {
                $escapedParameter = $stringHandler->preg_replace('/.*&/', '', $parameter);
            }

            $database = oxDb::getDb();
            $query = "insert into oxrolefields (oxid, oxname, oxparam) values ('"
                . oxUtilsObject::getInstance()->generateUID(). "', "
                . $database->quote($name) . ", "
                . $database->quote($escapedParameter) . " ) ";
            $database->execute($query);
        }
    }

    /**
     * Deletes R&R field.
     */
    public function deleteField()
    {
        // Saving oxfield2role.
        if ($id = oxRegistry::getConfig()->getRequestParameter('oxparam')) {
            $database = oxDb::getDb();
            // Deleting field.
            $query = "delete from oxrolefields where oxid = " . $database->quote($id);
            $database->execute($query);

            // Deleting predefined roles for field.
            $query = "delete from oxfield2role where oxfieldid = " . $database->quote($id);
            $database->execute($query);
        }
    }
}
