<?php
/**
 * This Software is the property of OXID eSales and is protected
 * by copyright law - it is NOT Freeware.
 *
 * Any unauthorized use of this software without a valid license key
 * is a violation of the license agreement and will be prosecuted by
 * civil and criminal law.
 *
 * @link      http://www.oxid-esales.com
 * @copyright (C) OXID eSales AG 2003-2016
 * @version   OXID eShop EE
 */
namespace OxidEsales\EshopEnterprise\Application\Controller\Admin;

use oxDb;
use oxField;

/**
 * Class manages back-end user groups rights
 */
class RolesBackendGroupsAjax extends ListComponentAjax
{
    /** @var array Columns array. */
    protected $_aColumns = array(
        // field , table,  visible, multilanguage, id
        'container1' => array(
            array('oxtitle', 'oxgroups', 1, 0, 0),
            array('oxid', 'oxgroups', 0, 0, 0),
            array('oxid', 'oxgroups', 0, 0, 1),
        ),
        'container2' => array(
            array('oxtitle', 'oxgroups', 1, 0, 0),
            array('oxid', 'oxgroups', 0, 0, 0),
            array('oxid', 'oxobject2role', 0, 0, 1),
        )
    );

    /**
     * Returns SQL query for data to fetc
     *
     * @return string
     */
    protected function _getQuery()
    {
        // looking for table/view
        $groupTable = $this->_getViewName('oxgroups');
        $database = oxDb::getDb();
        $roleId = $this->getConfig()->getRequestParameter('oxid');
        $syncedRoleId = $this->getConfig()->getRequestParameter('synchoxid');

        // category selected or not ?
        if (!$roleId) {
            $query = " FROM $groupTable WHERE 1 ";
        } else {
            $query =
                " FROM oxobject2role, $groupTable
                WHERE oxobject2role.oxtype = 'oxgroups'
                AND oxobject2role.oxroleid = " . $database->quote($roleId) . "
                AND $groupTable.oxid=oxobject2role.oxobjectid ";
        }

        if ($syncedRoleId && $syncedRoleId != $roleId) {
            $query .=
                " AND $groupTable.oxid not in (
                SELECT oxobject2role.oxobjectid
                FROM oxobject2role
                WHERE oxobject2role.oxtype = 'oxgroups'
                AND oxobject2role.oxroleid = " . $database->quote($syncedRoleId) . " ) ";
        }

        return $query;
    }

    /**
     * Removes User group from R&R
     */
    public function removeGroupFromBeRoles()
    {
        $database = oxDb::getDb();
        $groupsToRemove = $this->_getActionIds('oxobject2role.oxid');
        if ($this->getConfig()->getRequestParameter('all')) {
            $query = $this->_addFilter("delete oxobject2role.* " . $this->_getQuery());
            $database->execute($query);

        } elseif ($groupsToRemove && is_array($groupsToRemove)) {
            $groupsToRemoveQuoted = $database->quoteArray($groupsToRemove);
            $query = "DELETE FROM oxobject2role WHERE oxobject2role.oxid IN (" . implode(", ", $groupsToRemoveQuoted) . ") ";
            oxDb::getDb()->execute($query);
        }
    }

    /**
     * Adds User group to R&R
     */
    public function addGroupToBeRoles()
    {
        $chosenCategory = $this->_getActionIds('oxgroups.oxid');
        $roleId = $this->getConfig()->getRequestParameter('synchoxid');
        if ($this->getConfig()->getRequestParameter('all')) {
            $groupViewName = $this->_getViewName('oxgroups');
            $chosenCategory = $this->_getAll($this->_addFilter("select $groupViewName.oxid " . $this->_getQuery()));
        }
        if ($roleId && $roleId != "-1" && is_array($chosenCategory)) {
            foreach ($chosenCategory as $sChosenCat) {
                $objectToRole = oxNew("oxBase");
                $objectToRole->init("oxobject2role");
                $objectToRole->oxobject2role__oxobjectid = new oxField($sChosenCat);
                $objectToRole->oxobject2role__oxroleid = new oxField($roleId);
                $objectToRole->oxobject2role__oxtype = new oxField("oxgroups");
                $objectToRole->save();
            }
        }
    }
}
