<?php
/**
 * This Software is the property of OXID eSales and is protected
 * by copyright law - it is NOT Freeware.
 *
 * Any unauthorized use of this software without a valid license key
 * is a violation of the license agreement and will be prosecuted by
 * civil and criminal law.
 *
 * @link      http://www.oxid-esales.com
 * @copyright (C) OXID eSales AG 2003-2016
 * @version   OXID eShop EE
 */

namespace OxidEsales\EshopEnterprise\Application\Controller\Admin;

/**
 * @inheritdoc
 */
class ManufacturerMainAjax extends \OxidEsales\EshopProfessional\Application\Controller\Admin\ManufacturerMainAjax
{

    /**
     * @inheritdoc
     */
    protected function formManufacturerRemovalQuery($articlesToRemove)
    {
        $query = parent::formManufacturerRemovalQuery($articlesToRemove);
        $query .= " and oxshopid='" . $this->getConfig()->getShopId() . "' ";

        return $query;
    }

    /**
     * @inheritdoc
     */
    public function removeManufacturer()
    {
        parent::removeManufacturer();
        $manufacturerId = $this->getConfig()->getRequestParameter('oxid');

        if ($manufacturerId && $manufacturerId != "-1") {
            $manufacturer = oxNew('oxManufacturer');
            $manufacturer->load($manufacturerId);
            $manufacturer->executeDependencyEvent();
        }
    }

    /**
     * @inheritdoc
     */
    public function addManufacturer()
    {
        parent::addManufacturer();

        $manufacturerId = $this->getConfig()->getRequestParameter('synchoxid');

        if ($manufacturerId && $manufacturerId != "-1") {
            $manufacturer = oxNew('oxManufacturer');
            $manufacturer->load($manufacturerId);
            $manufacturer->executeDependencyEvent();
        }
    }

    /**
     * @inheritdoc
     */
    protected function formArticleToManufacturerAdditionQuery($manufacturerId, $articlesToAdd)
    {
        $query = parent::formArticleToManufacturerAdditionQuery($manufacturerId, $articlesToAdd);
        $query .= " and oxshopid='" . $this->getConfig()->getShopId() . "' ";

        return $query;
    }
}
