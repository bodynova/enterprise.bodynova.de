<?php
/**
 * This Software is the property of OXID eSales and is protected
 * by copyright law - it is NOT Freeware.
 *
 * Any unauthorized use of this software without a valid license key
 * is a violation of the license agreement and will be prosecuted by
 * civil and criminal law.
 *
 * @link      http://www.oxid-esales.com
 * @copyright (C) OXID eSales AG 2003-2016
 * @version   OXID eShop EE
 */

namespace OxidEsales\EshopEnterprise\Application\Controller\Admin;

use oxDb;
use oxRegistry;

/**
 * Class manages article rights to view
 */
class ArticleRightsVisibleAjax extends ListComponentAjax
{
    /**
     * Columns array
     *
     * @var array
     */
    protected $_aColumns = array(
        // field , table,  visible, multilanguage, ident
        'container1' => array(
            array('oxtitle', 'oxgroups', 1, 0, 0),
            array('oxid', 'oxgroups', 0, 0, 0),
            array('oxrrid', 'oxgroups', 0, 0, 1),
        ),
        'container2' => array(
            array('oxtitle', 'oxgroups', 1, 0, 0),
            array('oxid', 'oxgroups', 0, 0, 0),
            array('oxrrid', 'oxgroups', 0, 0, 1),
        )
    );

    /**
     * Returns SQL query for data to fetch
     *
     * @return string
     */
    protected function _getQuery()
    {
        $groupTable = $this->_getViewName('oxgroups');
        $database = oxDb::getDb();

        $action = 1;

        $articleId = oxRegistry::getConfig()->getRequestParameter('oxid');
        $syncedArticleId = oxRegistry::getConfig()->getRequestParameter('synchoxid');

        // category selected or not ?
        if (!$articleId) {
            $query = " from {$groupTable} where 1 ";
        } else {
            // fetching article RR view index
            $query = " from {$groupTable}, oxobjectrights where " .
                " oxobjectrights.oxobjectid = " . $database->quote($articleId) . " and " .
                " oxobjectrights.oxoffset = ({$groupTable}.oxrrid div 31) and " .
                " oxobjectrights.oxgroupidx & (1 << ({$groupTable}.oxrrid mod 31)) " .
                "and oxobjectrights.oxaction = {$action} ";
        }

        if ($syncedArticleId && $syncedArticleId != $articleId) {
            $query = " from {$groupTable} left join oxobjectrights " .
                "on  oxobjectrights.oxoffset = ( {$groupTable}.oxrrid div 31 ) and " .
                " oxobjectrights.oxgroupidx & (1 << ( {$groupTable}.oxrrid mod 31 ) ) " .
                "and oxobjectrights.oxobjectid= " . $database->quote($syncedArticleId) .
                " and oxobjectrights.oxaction = {$action} " .
                " where oxobjectrights.oxobjectid != " . $database->quote($syncedArticleId) .
                " or ( oxobjectid is null ) ";
        }

        return $query;
    }

    /**
     * Removing article from View Article group
     */
    public function removeGroupFromView()
    {
        $removeFromGroups = $this->_getActionIds('oxgroups.oxrrid');
        $articleId = oxRegistry::getConfig()->getRequestParameter('oxid');
        $action = 1;

        // removing all
        if (oxRegistry::getConfig()->getRequestParameter('all')) {
            $groupTable = $this->_getViewName('oxgroups');
            $removeFromGroups = $this->_getAll($this->_addFilter("select $groupTable.oxrrid " . $this->_getQuery()));
        }

        if ($articleId != "-1" && isset($articleId) && is_array($removeFromGroups) && count($removeFromGroups)) {
            $indexes = array();
            foreach ($removeFromGroups as $RRIdx) {
                $offset = ( int ) ($RRIdx / 31);
                $bitMap = 1 << ($RRIdx % 31);

                // summing indexes
                if (!isset($indexes[$offset])) {
                    $indexes[$offset] = $bitMap;
                } else {
                    $indexes[$offset] = $indexes [$offset] | $bitMap;
                }
            }

            $database = oxDb::getDb();
            // iterating through indexes and applying to (sub)categories R&R
            foreach ($indexes as $offset => $index) {
                // processing article
                $query = "update oxobjectrights set oxgroupidx = oxgroupidx & ~{$index} " .
                    "where oxobjectid= " . $database->quote($articleId) .
                    " and oxoffset = {$offset} and oxaction = {$action} ";
                $database->execute($query);
            }

            // removing cleared
            $query = "delete from oxobjectrights where oxgroupidx = 0";
            $database->execute($query);

            $article = oxNew('oxArticle');
            $article->setId($articleId);
            $article->executeDependencyEvent();
        }
    }

    /**
     * Adding article to View Article group list
     */
    public function addGroupToView()
    {
        $addToGroups = $this->_getActionIds('oxgroups.oxrrid');
        $articleId = oxRegistry::getConfig()->getRequestParameter('synchoxid');
        $action = 1;

        // adding
        if (oxRegistry::getConfig()->getRequestParameter('all')) {
            $groupTable = $this->_getViewName('oxgroups');
            $addToGroups = $this->_getAll($this->_addFilter("select $groupTable.oxrrid " . $this->_getQuery()));
        }

        if ($articleId != "-1" && isset($articleId) && is_array($addToGroups) && count($addToGroups)) {
            $indexes = array();
            foreach ($addToGroups as $RRIdx) {
                $offset = ( int ) ($RRIdx / 31);
                $bitMap = 1 << ($RRIdx % 31);

                // summing indexes
                if (!isset($indexes[$offset])) {
                    $indexes[$offset] = $bitMap;
                } else {
                    $indexes[$offset] = $indexes [$offset] | $bitMap;
                }
            }

            // iterating through indexes and applying to (sub)categories R&R
            $utilsObject = oxRegistry::get('oxUtilsObject');
            $database = oxDb::getDb();
            foreach ($indexes as $offset => $sIdx) {
                // processing category
                $query = "insert into oxobjectrights (oxid, oxobjectid, oxgroupidx, oxoffset, oxaction) " .
                    "values ('" . $utilsObject->generateUID() . "', " . oxDb::getDb()->quote($articleId) .
                    ", $sIdx, $offset,  $action ) on duplicate key update oxgroupidx = (oxgroupidx | $sIdx ) ";
                $database->Execute($query);
            }

            $article = oxNew('oxArticle');
            $article->setId($articleId);
            $article->executeDependencyEvent();
        }
    }
}
