<?php
/**
 * This Software is the property of OXID eSales and is protected
 * by copyright law - it is NOT Freeware.
 *
 * Any unauthorized use of this software without a valid license key
 * is a violation of the license agreement and will be prosecuted by
 * civil and criminal law.
 *
 * @link      http://www.oxid-esales.com
 * @copyright (C) OXID eSales AG 2003-2016
 * @version   OXID eShop EE
 */

namespace OxidEsales\EshopEnterprise\Application\Controller\Admin;

use oxRegistry;
use oxDb;

/**
 * @inheritdoc
 */
class SelectListMainAjax extends \OxidEsales\EshopProfessional\Application\Controller\Admin\SelectListMainAjax
{
    /**
     * @inheritdoc
     */
    public function removeArtFromSel()
    {
        if (oxRegistry::getConfig()->getRequestParameter('all')) {
            $sSelectSql = "SELECT `oxobject2selectlist`.`oxobjectid` " . $this->_getQuery();
        } else {
            $aChosenArt = $this->_getActionIds('oxobject2selectlist.oxid');
            $sSelectSql = "SELECT `oxobject2selectlist`.`oxobjectid`
                          FROM `oxobject2selectlist`
                          WHERE `oxobject2selectlist`.`oxid`
                          in (" . implode(", ", oxDb::getDb()->quoteArray($aChosenArt)) . ") ";
        }

        $aArticleIds = oxDb::getDb()->getCol($sSelectSql);

        foreach ($aArticleIds as $sArticleId) {
            $oArticle = oxNew("oxArticle");
            $oArticle->setId($sArticleId);
            $oArticle->executeDependencyEvent();
        }
        parent::removeArtFromSel();
    }

    /**
     * @inheritdoc
     */
    protected function onArticleAddToSelectionList($articleId)
    {
        parent::onArticleAddToSelectionList($articleId);
        $article = oxNew('oxArticle');
        $article->setId($articleId);
        $article->executeDependencyEvent();
    }
}
