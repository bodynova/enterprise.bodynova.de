<?php
/**
 * This Software is the property of OXID eSales and is protected
 * by copyright law - it is NOT Freeware.
 *
 * Any unauthorized use of this software without a valid license key
 * is a violation of the license agreement and will be prosecuted by
 * civil and criminal law.
 *
 * @link      http://www.oxid-esales.com
 * @copyright (C) OXID eSales AG 2003-2016
 * @version   OXID eShop EE
 */

namespace OxidEsales\EshopEnterprise\Application\Controller\Admin;

use oxRegistry;

/**
 * Admin article main actions manager.
 * There is possibility to change actions description, assign articles to
 * this actions, etc.
 * Admin Menu: Manage Products -> actions -> Main.
 */
class RolesFrontendUser extends \OxidEsales\EshopProfessional\Application\Controller\Admin\AdminDetailsController
{
    /**
     * Render.
     *
     * @return string
     */
    public function render()
    {
        parent::render();

        oxRegistry::getConfig()->getRequestParameter('selgroup');
        $oxId = $this->getEditObjectId();

        $role = oxNew('oxrole');
        $role->load($oxId);
        $this->_aViewData['edit'] = $role;

        if (oxRegistry::getConfig()->getRequestParameter('aoc')) {
            $rolesFEGroupsAjax = oxNew('roles_fegroups_ajax');
            $this->_aViewData['oxajax'] = $rolesFEGroupsAjax->getColumns();
            return 'popups/roles_fegroups.tpl';
        }

        return 'roles_feuser.tpl';
    }
}
