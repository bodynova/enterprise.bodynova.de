<?php
/**
 * This Software is the property of OXID eSales and is protected
 * by copyright law - it is NOT Freeware.
 *
 * Any unauthorized use of this software without a valid license key
 * is a violation of the license agreement and will be prosecuted by
 * civil and criminal law.
 *
 * @link      http://www.oxid-esales.com
 * @copyright (C) OXID eSales AG 2003-2016
 * @version   OXID eShop EE
 */

namespace OxidEsales\EshopEnterprise\Application\Controller\Admin;

/**
 * @inheritdoc
 */
class CategoryMain extends \OxidEsales\EshopProfessional\Application\Controller\Admin\CategoryMain
{
    /**
     * @inheritdoc
     */
    protected function _deleteCatPicture(\OxidEsales\EshopCommunity\Application\Model\Category $item, $field)
    {
        if (!($item->canUpdateField($field) && $item->canUpdate())) {
            return;
        }

        parent::_deleteCatPicture($item, $field);
    }

    /**
     * @inheritdoc
     */
    protected function updateCategoryOnSave($category, $params)
    {
        $config = $this->getConfig();

        $category = parent::updateCategoryOnSave($category, $params);

        // if it is not parent category then assign it to inhereded shops
        if ($params["oxcategories__oxparentid"] != 'oxrootid') {
            $config->getActiveShop()->setMultiShopInheritCategories(true);
        }

        return $category;
    }
}
