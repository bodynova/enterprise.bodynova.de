<?php
/**
 * This Software is the property of OXID eSales and is protected
 * by copyright law - it is NOT Freeware.
 *
 * Any unauthorized use of this software without a valid license key
 * is a violation of the license agreement and will be prosecuted by
 * civil and criminal law.
 *
 * @link      http://www.oxid-esales.com
 * @copyright (C) OXID eSales AG 2003-2016
 * @version   OXID eShop EE
 */

namespace OxidEsales\EshopEnterprise\Application\Controller\Admin;

use ajaxListComponent;
use oxRegistry;
use oxDb;
use oxUtilsObject;

/**
 * Class manages category rights to view
 */
class CategoryRightsVisibleAjax extends \OxidEsales\EshopProfessional\Application\Controller\Admin\ListComponentAjax
{
    const NEW_CATEGORY_ID = '-1';
    /**
     * Columns array
     *
     * @var array
     */
    protected $_aColumns = array(
        'container1' => array( // field , table,  visible, multilanguage, ident
            array('oxtitle', 'oxgroups', 1, 0, 0),
            array('oxid', 'oxgroups', 0, 0, 0),
            array('oxrrid', 'oxgroups', 0, 0, 1),
        ),
        'container2' => array(
            array('oxtitle', 'oxgroups', 1, 0, 0),
            array('oxid', 'oxgroups', 0, 0, 0),
            array('oxrrid', 'oxgroups', 0, 0, 1),
        )
    );

    /**
     * Returns SQL query for data to fetch
     *
     * @return string
     */
    protected function _getQuery()
    {
        $RRId = null;
        $action = 1;
        $db = oxDb::getDb();

        // looking for table/view
        $groupTable = $this->_getViewName('oxgroups');

        $catId = oxRegistry::getConfig()->getRequestParameter('oxid');
        $synchCatId = oxRegistry::getConfig()->getRequestParameter('synchoxid');

        // category selected or not ?
        if (!$catId) {
            $queryAdd = " from $groupTable where 1 ";
        } else {
            // fetching category RR view index
            $queryAdd = " from $groupTable, oxobjectrights where ";
            $queryAdd .= " oxobjectrights.oxobjectid = " . $db->quote($catId) . " and ";
            $queryAdd .= " oxobjectrights.oxoffset = ($groupTable.oxrrid div 31) and ";
            $queryAdd .= " oxobjectrights.oxgroupidx & (1 << ($groupTable.oxrrid mod 31)) and oxobjectrights.oxaction = $action ";
        }

        if ($synchCatId && $synchCatId != $catId) {
            $queryAdd = " from $groupTable left join oxobjectrights on ";
            $queryAdd .= " oxobjectrights.oxoffset = ($groupTable.oxrrid div 31) and ";
            $queryAdd .= " oxobjectrights.oxgroupidx & (1 << ($groupTable.oxrrid mod 31)) and oxobjectrights.oxobjectid=" . $db->quote($synchCatId) . " and oxobjectrights.oxaction = $action ";
            $queryAdd .= " where oxobjectrights.oxobjectid != " . $db->quote($synchCatId) . " or (oxobjectid is null)";
        }

        return $queryAdd;
    }

    /**
     * Removing article from View Article group
     */
    public function removeGroupFromCatView()
    {
        $config = $this->getConfig();

        $range = oxRegistry::getConfig()->getRequestParameter('oxrrapplyrange');

        $groups = $this->_getActionIds('oxgroups.oxrrid');
        $categoryId = oxRegistry::getConfig()->getRequestParameter('oxid');

        $action = 1;

        // removing all
        if (oxRegistry::getConfig()->getRequestParameter('all')) {
            $groupTable = $this->_getViewName('oxgroups');
            $groups = $this->_getAll($this->_addFilter("select $groupTable.oxrrid " . $this->_getQuery()));
        }

        if ($categoryId != self::NEW_CATEGORY_ID && isset($categoryId) && is_array($groups) && count($groups)) {
            $indexes = array();
            foreach ($groups as $RRIdx) {
                $offset = ( int ) ($RRIdx / 31);
                $bitMap = 1 << ($RRIdx % 31);

                // summing indexes
                if (!isset($indexes[$offset])) {
                    $indexes[$offset] = $bitMap;
                } else {
                    $indexes[$offset] = $indexes [$offset] | $bitMap;
                }
            }

            $category = oxNew("oxCategory");
            $category->load($categoryId);

            $shopID = $config->getShopID();
            $db = oxDb::getDb();

            $object2categoryViewName = $this->_getViewName('oxobject2category');

            // iterating through indexes and applying to (sub)categories R&R
            foreach ($indexes as $offset => $sIdx) {
                // processing category
                $sqlQuery = "update oxobjectrights set oxgroupidx = oxgroupidx & ~$sIdx where oxobjectid = " . $db->quote($categoryId) . " and oxoffset = $offset and oxaction = $action ";
                $db->Execute($sqlQuery);

                // processing articles
                $sqlQuery = "update oxobjectrights set oxgroupidx = oxgroupidx & ~$sIdx where oxaction = $action and oxoffset = $offset and oxobjectid in ( ";
                $sqlQuery .= "select oxobject2category.oxobjectid from $object2categoryViewName as oxobject2category where oxobject2category.oxcatnid=" . $db->quote($categoryId) . " ) ";
                $db->Execute($sqlQuery);

                if ($range) {
                    // processing subcategories
                    $sqlQuery = "update oxobjectrights, oxcategories ";
                    $sqlQuery .= "inner join oxcategories2shop as t2s on t2s.oxmapobjectid = oxcategories.oxmapid where t2s.oxshopid = {$db->quote($shopID)} ";
                    $sqlQuery .= "set oxobjectrights.oxgroupidx = oxobjectrights.oxgroupidx & ~$sIdx where oxobjectrights.oxoffset = $offset and oxobjectrights.oxaction = $action ";
                    $sqlQuery .= "and oxobjectrights.oxobjectid = oxcategories.oxid and ";
                    $sqlQuery .= "oxcategories.oxleft > " . $category->oxcategories__oxleft->value . " and oxcategories.oxright < " . $category->oxcategories__oxright->value . " and ";
                    $sqlQuery .= "oxcategories.oxrootid= " . $db->quote($category->oxcategories__oxrootid->value) . " ";
                    $db->Execute($sqlQuery);

                    // processing articles
                    $sqlQuery = "update oxobjectrights set oxobjectrights.oxgroupidx = oxobjectrights.oxgroupidx & ~$sIdx ";
                    $sqlQuery .= "where oxobjectrights.oxaction = $action and oxobjectrights.oxobjectid in ( ";
                    $sqlQuery .= "select oxobject2category.oxobjectid from $object2categoryViewName oxobject2category ";
                    $sqlQuery .= "left join oxcategories on oxcategories.oxid = oxobject2category.oxcatnid ";
                    $sqlQuery .= "inner join oxcategories2shop as t2s on t2s.oxmapobjectid = oxcategories.oxmapid where t2s.oxshopid = {$db->quote($shopID)} ";
                    $sqlQuery .= "where oxcategories.oxrootid = " . $db->quote($category->oxcategories__oxrootid->value) . " and ";
                    $sqlQuery .= "oxcategories.oxleft > " . $category->oxcategories__oxleft->value . " and ";
                    $sqlQuery .= "oxcategories.oxright < " . $category->oxcategories__oxright->value . " ";
                    $sqlQuery .= ") ";
                    $db->Execute($sqlQuery);
                }
            }

            // removing cleared
            $sqlQuery = "delete from oxobjectrights where oxgroupidx = 0";
            $db->Execute($sqlQuery);

            $this->flushCategoryDependencies($categoryId);
        }
    }

    /**
     * Adding article to View Article group list
     */
    public function addGroupToCatView()
    {
        $config = $this->getConfig();

        $groups = $this->_getActionIds('oxgroups.oxrrid');
        $categoryId = oxRegistry::getConfig()->getRequestParameter('synchoxid');

        $range = oxRegistry::getConfig()->getRequestParameter('oxrrapplyrange');
        $action = 1;

        // adding
        if (oxRegistry::getConfig()->getRequestParameter('all')) {
            $groupTable = $this->_getViewName('oxgroups');
            $groups = $this->_getAll($this->_addFilter("select $groupTable.oxrrid " . $this->_getQuery()));
        }

        if ($categoryId != self::NEW_CATEGORY_ID && isset($categoryId) && is_array($groups) && count($groups)) {
            $indexes = array();
            foreach ($groups as $RRIdx) {
                $offset = ( int ) ($RRIdx / 31);
                $bitMap = 1 << ($RRIdx % 31);

                // summing indexes
                if (!isset($indexes[$offset])) {
                    $indexes[$offset] = $bitMap;
                } else {
                    $indexes[$offset] = $indexes [$offset] | $bitMap;
                }
            }

            $category = oxNew("oxCategory");
            $category->load($categoryId);

            $shopID = $config->getShopID();
            $object2categoryView = $this->_getViewName('oxobject2category');
            $utilsObject = oxUtilsObject::getInstance();
            $oDb = oxDb::getDb();

            // iterating through indexes and applying to (sub)categories R&R
            foreach ($indexes as $offset => $sIdx) {
                // processing category
                $query = "insert into oxobjectrights (oxid, oxobjectid, oxgroupidx, oxoffset, oxaction) ";
                $query .= "values ( " . $oDb->quote($utilsObject->generateUID()) . ", " . $oDb->quote($categoryId) . ", " . $oDb->quote($sIdx) . ", $offset,  $action ) on duplicate key update oxgroupidx = (oxgroupidx | $sIdx ) ";
                $oDb->Execute($query);

                $query = "insert into oxobjectrights (oxid, oxobjectid, oxgroupidx, oxoffset, oxaction) ";
                $query .= "select md5( concat( a.oxobjectid, oxobject2category.oxid) ), oxobject2category.oxobjectid, a.oxgroupidx, a.oxoffset, a.oxaction ";
                $query .= "from $object2categoryView as oxobject2category left join oxobjectrights a on oxobject2category.oxcatnid=a.oxobjectid where oxobject2category.oxcatnid=" . $oDb->quote($categoryId) . " and a.oxaction = $action ";
                $query .= "on duplicate key update oxobjectrights.oxgroupidx = (oxobjectrights.oxgroupidx | a.oxgroupidx ) ";
                $oDb->Execute($query);

                if ($range) {
                    // processing subcategories
                    $query = "insert into oxobjectrights (oxid, oxobjectid, oxgroupidx, oxoffset, oxaction) ";
                    $query .= "select " . $oDb->quote($utilsObject->generateUID()) . ", oxcategories.oxid, $sIdx, $offset, $action from oxcategories ";
                    $query .= "inner join oxcategories2shop as t2s on t2s.oxmapobjectid = oxcategories.oxmapid where t2s.oxshopid = {$oDb->quote($shopID)} ";
                    $query .= "where oxcategories.oxleft > " . $category->oxcategories__oxleft->value . " and oxcategories.oxright < " . $category->oxcategories__oxright->value . " and ";
                    $query .= "oxcategories.oxrootid=" . $oDb->quote($category->oxcategories__oxrootid->value) . " ";
                    $query .= "on duplicate key update oxgroupidx = (oxgroupidx | $sIdx ) ";
                    $oDb->Execute($query);

                    // processing articles
                    $query = "insert into oxobjectrights (oxid, oxobjectid, oxgroupidx, oxoffset, oxaction) ";
                    $query .= "select md5( concat( a.oxobjectid, oxobject2category.oxid, a.oxaction, a.oxoffset) ), oxobject2category.oxobjectid, a.oxgroupidx, a.oxoffset, a.oxaction ";
                    $query .= "from $object2categoryView as oxobject2category ";
                    $query .= "left join oxcategories on oxcategories.oxid = oxobject2category.oxcatnid ";
                    $query .= "left join oxobjectrights a on a.oxobjectid = " . $oDb->quote($categoryId);
                    $query .= "inner join oxarticles2shop as t2s on t2s.oxmapobjectid = oxarticles.oxmapid where t2s.oxshopid = {$oDb->quote($shopID)} ";
                    $query .= "where oxcategories.oxrootid = " . $oDb->quote($category->oxcategories__oxrootid->value) . " and ";
                    $query .= "oxcategories.oxleft > " . $category->oxcategories__oxleft->value . " and  ";
                    $query .= "oxcategories.oxright < " . $category->oxcategories__oxright->value . " and a.oxaction = $action ";
                    $query .= "on duplicate key update oxobjectrights.oxgroupidx = (oxobjectrights.oxgroupidx | a.oxgroupidx )";
                    $oDb->Execute($query);
                }
            }

            $this->flushCategoryDependencies($categoryId);
        }
    }

    /**
     * @param string $categoryId
     */
    protected function flushCategoryDependencies($categoryId)
    {
        $category = oxNew('oxCategory');
        $category->executeDependencyEvent(array($categoryId));
    }
}
