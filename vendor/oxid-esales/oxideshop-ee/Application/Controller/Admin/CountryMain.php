<?php
/**
 * This Software is the property of OXID eSales and is protected
 * by copyright law - it is NOT Freeware.
 *
 * Any unauthorized use of this software without a valid license key
 * is a violation of the license agreement and will be prosecuted by
 * civil and criminal law.
 *
 * @link      http://www.oxid-esales.com
 * @copyright (C) OXID eSales AG 2003-2016
 * @version   OXID eShop EE
 */

namespace OxidEsales\EshopEnterprise\Application\Controller\Admin;

/**
 * @inheritdoc
 **/
class CountryMain extends \OxidEsales\EshopProfessional\Application\Controller\Admin\CountryMain
{
    /**
     * @inheritdoc
     */
    public function render()
    {
        $myConfig = $this->getConfig();

        if (!$myConfig->getConfigParam('blAllowSharedEdit')) {
            $this->_aViewData['readonly'] = true;
        }

        return parent::render();
    }

    /**
     * @inheritdoc
     */
    public function save()
    {
        $myConfig = $this->getConfig();

        //allow malladmin only to perform this action
        if (!$myConfig->getConfigParam('blAllowSharedEdit')) {
            return;
        }

        return parent::save();
    }

    /**
     * @inheritdoc
     */
    public function saveinnlang()
    {
        $myConfig = $this->getConfig();

        //allow malladmin only
        if (!$myConfig->getConfigParam('blAllowSharedEdit')) {
            return;
        }

        return parent::save();
    }
}
