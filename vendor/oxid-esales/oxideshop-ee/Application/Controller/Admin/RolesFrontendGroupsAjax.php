<?php
/**
 * This Software is the property of OXID eSales and is protected
 * by copyright law - it is NOT Freeware.
 *
 * Any unauthorized use of this software without a valid license key
 * is a violation of the license agreement and will be prosecuted by
 * civil and criminal law.
 *
 * @link      http://www.oxid-esales.com
 * @copyright (C) OXID eSales AG 2003-2016
 * @version   OXID eShop EE
 */
namespace OxidEsales\EshopEnterprise\Application\Controller\Admin;

use oxDb;
use oxRegistry;
use oxUtilsObject;

/**
 * Class manages front-end user groups rights.
 */
class RolesFrontendGroupsAjax extends ListComponentAjax
{
    /** @var array Columns array. */
    protected $_aColumns = array(
        // field , table,  visible, multilanguage, id
        'container1' => array(
            array('oxtitle', 'oxgroups', 1, 0, 0),
            array('oxid', 'oxgroups', 0, 0, 0),
            array('oxrrid', 'oxgroups', 0, 0, 1),
        ),
        'container2' => array(
            array('oxtitle', 'oxgroups', 1, 0, 0),
            array('oxid', 'oxgroups', 0, 0, 0),
            array('oxrrid', 'oxgroups', 0, 0, 1),
        )
    );

    /**
     * Returns SQL query for data to fetch.
     *
     * @return string
     */
    protected function _getQuery()
    {
        // looking for table/view
        $groupsViewName = $this->_getViewName('oxgroups');
        $database = oxDb::getDb();
        $groupId = oxRegistry::getConfig()->getRequestParameter('oxid');
        $syncedGroupId = oxRegistry::getConfig()->getRequestParameter('synchoxid');

        $action = 1;
        if (!$groupId) {
            $query = " from $groupsViewName where 1 ";
        } else {
            $query = " from $groupsViewName, oxobjectrights where ";
            $query .= " oxobjectrights.oxobjectid = " . $database->quote($groupId) . " and ";
            $query .= " oxobjectrights.oxoffset = ($groupsViewName.oxrrid div 31) and ";
            $query .= " oxobjectrights.oxgroupidx & (1 << ($groupsViewName.oxrrid mod 31)) and oxobjectrights.oxaction = $action ";
        }

        if ($syncedGroupId && $syncedGroupId != $groupId) {
            $query = " from $groupsViewName left join oxobjectrights on ";
            $query .= " oxobjectrights.oxoffset = ($groupsViewName.oxrrid div 31) and ";
            $query .= " oxobjectrights.oxgroupidx & (1 << ($groupsViewName.oxrrid mod 31)) and oxobjectrights.oxobjectid=" . $database->quote($syncedGroupId) . " and oxobjectrights.oxaction = $action ";
            $query .= " where oxobjectrights.oxobjectid != " . $database->quote($syncedGroupId) . " or (oxobjectid is null) ";
        }

        return $query;
    }

    /**
     * Removes chosen user group (groups) from delivery list.
     */
    public function removeGroupFromFeRoles()
    {
        $chosenGroups = $this->_getActionIds('oxgroups.oxrrid');
        $groupId = oxRegistry::getConfig()->getRequestParameter('oxid');

        $action = 1;

        // removing all
        if (oxRegistry::getConfig()->getRequestParameter('all')) {
            $groupViewName = $this->_getViewName('oxgroups');
            $chosenGroups = $this->_getAll($this->_addFilter("select $groupViewName.oxrrid " . $this->_getQuery()));
        }

        if (isset($groupId) && $groupId != "-1" && is_array($chosenGroups) && $chosenGroups) {
            $indexes = array();
            foreach ($chosenGroups as $rightsAndRolesIndex) {
                $offset = ( int ) ($rightsAndRolesIndex / 31);
                $bitMap = 1 << ($rightsAndRolesIndex % 31);

                // summing indexes
                if (!isset($indexes[$offset])) {
                    $indexes[$offset] = $bitMap;
                } else {
                    $indexes[$offset] = $indexes [$offset] | $bitMap;
                }
            }

            $database = oxDb::getDb();
            // iterating through indexes and applying to (sub)categories R&R
            foreach ($indexes as $offset => $index) {
                // processing category
                $query = "update oxobjectrights set oxgroupidx = oxgroupidx & ~$index where oxobjectid = " . $database->quote($groupId) . " and oxoffset = $offset and oxaction = $action ";
                $database->execute($query);
            }

            // removing cleared
            $query = "delete from oxobjectrights where oxgroupidx = 0";
            $database->execute($query);
        }
    }

    /**
     * Adds chosen user group (groups) to R&R list.
     */
    public function addGroupToFeroles()
    {
        $chosenCategories = $this->_getActionIds('oxgroups.oxrrid');
        $groupId = oxRegistry::getConfig()->getRequestParameter('synchoxid');

        $action = 1;
        if (oxRegistry::getConfig()->getRequestParameter('all')) {
            $groupViewName = $this->_getViewName('oxgroups');
            $chosenCategories = $this->_getAll($this->_addFilter("select $groupViewName.oxrrid " . $this->_getQuery()));
        }

        if (isset($groupId) && $groupId != "-1" && isset($chosenCategories) && $chosenCategories) {
            $indexes = array();
            foreach ($chosenCategories as $rightsAndRolesIndex) {
                $offset = (int) ($rightsAndRolesIndex / 31);
                $bitMap = 1 << ($rightsAndRolesIndex % 31);

                // summing indexes
                if (!isset($indexes[$offset])) {
                    $indexes[$offset] = $bitMap;
                } else {
                    $indexes[$offset] = $indexes [$offset] | $bitMap;
                }
            }

            $utilsObject = oxUtilsObject::getInstance();
            $database = oxDb::getDb();

            // iterating through indexes and applying to (sub)categories R&R
            foreach ($indexes as $offset => $index) {
                // processing category
                $query = "insert into oxobjectrights (oxid, oxobjectid, oxgroupidx, oxoffset, oxaction) ";
                $query .= "values ('" . $utilsObject->generateUID() . "', " . $database->quote($groupId) . ", " . $database->quote($index) . ", $offset,  $action ) on duplicate key update oxgroupidx = (oxgroupidx | $index ) ";
                $database->Execute($query);
            }
        }
    }
}
