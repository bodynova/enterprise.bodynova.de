<?php
/**
 * This Software is the property of OXID eSales and is protected
 * by copyright law - it is NOT Freeware.
 *
 * Any unauthorized use of this software without a valid license key
 * is a violation of the license agreement and will be prosecuted by
 * civil and criminal law.
 *
 * @link      http://www.oxid-esales.com
 * @copyright (C) OXID eSales AG 2003-2016
 * @version   OXID eShop EE
 */

namespace OxidEsales\EshopEnterprise\Application\Controller;

use oxRegistry;
use oxUBase;

/**
 * @inheritdoc
 */
class ArticleListController extends \OxidEsales\EshopProfessional\Application\Controller\ArticleListController
{
    /**
     * Do not cache if user is logged in
     *
     * @var bool
     */
    protected $_blCacheForUser = false;

    /**
     * @inheritdoc
     */
    protected function generateViewId()
    {
        $categoryId = oxRegistry::getConfig()->getRequestParameter('cnid');
        $activePage = $this->getActPage();
        $articlesPerPage = oxRegistry::getSession()->getVariable('_artperpage');
        $listDisplayType = $this->_getListDisplayType();
        $parentViewId = oxUBase::generateViewId();

        //#1998 - filters and caching
        $filter = oxRegistry::getConfig()->getRequestParameter('attrfilter');
        if (!$filter) {
            $sessionFilter = oxRegistry::getSession()->getVariable('session_attrfilter');
            $languageId = oxRegistry::getLang()->getBaseLanguage();
            if (isset($sessionFilter[$categoryId][$languageId])) {
                $filter = $sessionFilter[$categoryId][$languageId];
            }
        }

        $filterAdd = '';
        if (is_array($filter)) {
            $filterAdd = "|" . md5(serialize($filter));
        }

        // shorten it
        $viewId = $parentViewId . '|' . $categoryId . $filterAdd . '|';
        $viewId .= $activePage . '|' . $articlesPerPage . '|' . $listDisplayType;

        return $viewId;
    }

    /**
     * Checks category rights.
     * If category can not be viewed, null is returned instead.
     *
     * @inheritdoc
     *
     * @return null|\oxCategory
     */
    protected function getCategoryToRender()
    {
        $category = parent::getCategoryToRender();

        if ($category && $this->_blIsCat && !$category->canView()) {
            $category = null;
        }

        return $category;
    }
}
