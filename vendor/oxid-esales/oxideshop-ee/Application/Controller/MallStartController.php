<?php
/**
 * This Software is the property of OXID eSales and is protected
 * by copyright law - it is NOT Freeware.
 *
 * Any unauthorized use of this software without a valid license key
 * is a violation of the license agreement and will be prosecuted by
 * civil and criminal law.
 *
 * @link      http://www.oxid-esales.com
 * @copyright (C) OXID eSales AG 2003-2016
 * @version   OXID eShop EE
 */

namespace OxidEsales\EshopEnterprise\Application\Controller;

use oxRegistry;

/**
 * Starting mall window with list of active shops.
 * Arranges list of running shops in current mall. Shops are sorted by categories
 * (if available). This class is only executed if current license enables shopping
 * mall ability.
 */
class MallStartController extends \OxidEsales\EshopProfessional\Application\Controller\FrontendController
{

    /**
     * Current class template name.
     *
     * @var string
     */
    protected $_sThisTemplate = 'page/shop/mallstart.tpl';

    /**
     * Shop links
     *
     * @var array
     */
    protected $_aShopLinks = null;

    /**
     * Shop default langs
     *
     * @var array
     */
    protected $_aShopDefaultLangs = null;

    /**
     * Shoplist
     *
     * @var object
     */
    protected $_oShopList = null;

    /**
     * Sets id of current session shop and returns "start".
     *
     * @return string
     */
    public function chshp()
    {
        //racalculate basket if changing shops
        $sActShop = oxRegistry::getConfig()->getRequestParameter('shp');
        if ($sActShop && $sActShop != oxRegistry::getSession()->getVariable('actshop')) {
            //performs actions required on shop change
            $this->getConfig()->onShopChange();
        }

        oxRegistry::getSession()->setVariable('actshop', $sActShop);

        return "start";
    }

    /**
     * Template variable getter. Returns shop links
     *
     * @return array
     */
    public function getShopLinks()
    {
        if ($this->_aShopLinks === null) {
            $this->_aShopLinks = false;
            $aShopLinks = array();
            $oShoplist = $this->getShopList();
            foreach ($oShoplist as $sId => $oShop) {
                $aShopLinks[$sId] = $this->getConfig()->getShopConfVar('sMallShopURL', $sId);
            }
            $this->_aShopLinks = $aShopLinks;
        }

        return $this->_aShopLinks;
    }

    /**
     * Template variable getter. Returns shop links
     *
     * @return array
     */
    public function getShopDefaultLangs()
    {
        if ($this->_aShopDefaultLangs === null) {
            $this->_aShopDefaultLangs = false;
            $aShopDefaultLangs = array();
            $oShoplist = $this->getShopList();
            foreach ($oShoplist as $sId => $oShop) {
                $aShopDefaultLangs[$sId] = $this->getConfig()->getShopConfVar('sDefaultLang', $sId);
            }
            $this->_aShopDefaultLangs = $aShopDefaultLangs;
        }

        return $this->_aShopDefaultLangs;
    }

    /**
     * Template variable getter. Returns shoplist
     *
     * @return object
     */
    public function getShopList()
    {
        if ($this->_oShopList === null) {
            $this->_oShopList = false;
            $oShoplist = oxNew('oxshoplist');
            $oShoplist->getList();
            $this->_oShopList = $oShoplist;
        }

        return $this->_oShopList;
    }
}
