<?php
/**
 * This Software is the property of OXID eSales and is protected
 * by copyright law - it is NOT Freeware.
 *
 * Any unauthorized use of this software without a valid license key
 * is a violation of the license agreement and will be prosecuted by
 * civil and criminal law.
 *
 * @link      http://www.oxid-esales.com
 * @copyright (C) OXID eSales AG 2003-2016
 * @version   OXID eShop EE
 */

namespace OxidEsales\EshopEnterprise\Tests\Unit\Model;

use oxField;
use OxidEsales\EshopEnterprise\Core\Cache\DynamicContent\ContentCache;
use OxidEsales\TestingLibrary\UnitTestCase;
use PHPUnit_Framework_MockObject_MockObject as MockObject;

/**
 * Tests utf8 related functionality.
 */
class Utf8Test extends UnitTestCase
{
    public function testOxRoleSaveAndLoad()
    {
        $value = 'agentūЛитовfür';

        $fields = array('oxroles__oxtitle');

        $role = oxNew('oxRole');
        $role->setId('_testRole');
        foreach ($fields as $fieldName) {
            $role->{$fieldName} = new oxField($value);
        }
        $role->save();

        $role = oxNew('oxRole');
        $role->load('_testRole');

        foreach ($fields as $fieldName) {
            $this->assertTrue(strcmp($role->{$fieldName}->value, $value) === 0, "$fieldName (" . $role->{$fieldName}->value . ")");
        }
    }

    public function testCacheProcessing()
    {
        $contentToProcess = "agentūлитовfür <oxid_dynamic><a href=\"someurl.php?cl=comecl&amp;sid=somesid&amp;something=something\" title=\"agentūлитовfür\"></oxid_dynamic> agentūлитовfür";
        $contentWillGet = "agentūлитовfür # agentūлитовfür";

        /** @var ContentCache|MockObject $cache */
        $cache = $this->getMock('oxCache', array('_processDynContent'));
        $cache->expects($this->once())->method('_processDynContent')->will($this->returnValue('#'));
        $this->assertEquals($contentWillGet, $cache->processCache($contentToProcess));
    }
}
