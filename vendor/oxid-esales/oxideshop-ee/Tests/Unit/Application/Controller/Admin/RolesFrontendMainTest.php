<?php
/**
 * This Software is the property of OXID eSales and is protected
 * by copyright law - it is NOT Freeware.
 *
 * Any unauthorized use of this software without a valid license key
 * is a violation of the license agreement and will be prosecuted by
 * civil and criminal law.
 *
 * @link      http://www.oxid-esales.com
 * @copyright (C) OXID eSales AG 2003-2016
 * @version   OXID eShop EE
 */

namespace OxidEsales\EshopEnterprise\Tests\Unit\Application\Controller\Admin;

use OxidEsales\TestingLibrary\UnitTestCase;
use oxDb;
use oxTestModules;
use Exception;

/**
 * Tests for Roles_FEmain class
 */
class RolesFrontendMainTest extends UnitTestCase
{
    /**
     * Tear down the fixture.
     *
     * @return null
     */
    protected function tearDown()
    {
        oxDb::getDb()->execute("delete from oxrolefields where oxid like 'test%'");
        oxDb::getDb()->execute("delete from oxfield2role where oxfieldid like 'test%'");

        parent::tearDown();
    }

    /**
     * Roles_FEmain::Render() test case
     *
     * @return null
     */
    public function testRender()
    {
        // testing..
        $oView = oxNew('Roles_FEmain');
        $this->assertEquals('roles_femain.tpl', $oView->render());
    }

    /**
     * Roles_FEmain::Save() test case
     *
     * @return null
     */
    public function testSave()
    {
        oxTestModules::addFunction('oxrole', 'save', '{ throw new Exception( "save" ); }');
        $this->getConfig()->setConfigParam('blAllowSharedEdit', true);

        // testing..
        try {
            $oView = oxNew('Roles_FEmain');
            $oView->save();
        } catch (Exception $oExcp) {
            $this->assertEquals("save", $oExcp->getMessage(), "error in Roles_FEmain::save()");

            return;
        }
        $this->fail("error in Roles_FEmain::save()");
    }

    /**
     * Roles_FEmain::AddField() test case
     *
     * @return null
     */
    public function testAddField()
    {
        oxTestModules::addFunction('oxUtilsObject', 'generateUID', '{ return "testRecord"; }');
        oxTestModules::addFunction('oxrole', 'load', '{ return true; }');
        $this->setRequestParameter("oxparam", '&a&a&');

        // testing..
        $oView = oxNew('Roles_FEmain');
        $oView->addField();

        $this->assertEquals("1", oxDb::getDb()->getOne("select 1 from oxrolefields where oxid = 'testRecord'"));
    }

    /**
     * Roles_FEmain::DeleteField() test case
     *
     * @return null
     */
    public function testDeleteField()
    {
        $oDb = oxDb::getDb();

        $this->setRequestParameter('oxparam', "testRecord");
        $oDb->execute("insert into oxrolefields (`OXID`, `OXNAME`, `OXPARAM`) values ( 'testRecord', 'testName', 'testParam' )");
        $oDb->execute("insert into oxfield2role (`OXFIELDID`, `OXTYPE`, `OXROLEID`, `OXIDX`) values( 'testRecord', 'oxview', 'testRoleId', '0' )");

        // testing..
        $oView = oxNew('Roles_FEmain');
        $oView->deleteField();

        $this->assertFalse($oDb->getOne("select 1 from oxrolefields where oxid = 'testRecord'"));
        $this->assertFalse($oDb->getOne("select 1 from oxfield2role where oxfieldid = 'testRecord'"));
    }
}
