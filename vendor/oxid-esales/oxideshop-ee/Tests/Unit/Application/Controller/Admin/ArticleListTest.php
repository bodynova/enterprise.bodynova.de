<?php
/**
 * This Software is the property of OXID eSales and is protected
 * by copyright law - it is NOT Freeware.
 *
 * Any unauthorized use of this software without a valid license key
 * is a violation of the license agreement and will be prosecuted by
 * civil and criminal law.
 *
 * @link      http://www.oxid-esales.com
 * @copyright (C) OXID eSales AG 2003-2016
 * @version   OXID eShop EE
 */

namespace OxidEsales\EshopEnterprise\Tests\Unit\Application\Controller\Admin;

/**
 * Class ArticleListTest.
 */
class ArticleListTest extends \OxidEsales\TestingLibrary\UnitTestCase
{
    /**
     * Article_List::UnassignEntry() test case.
     */
    public function testUnassignEntry()
    {
        \oxTestModules::addFunction("oxUtilsServer", "getOxCookie", "{return array(1);}");
        \oxTestModules::addFunction("oxUtils", "checkAccessRights", "{return true;}");
        \oxTestModules::addFunction('oxarticle', 'load', '{ return true; }');

        $oView = $this->getMock("Article_List", array("resetContentCache", "_authorize"));
        $oView->expects($this->any())->method('_authorize')->will($this->returnValue(true));
        $oView->expects($this->once())->method('resetContentCache');
        $oView->unassignEntry();
    }
}
