<?php
/**
 * This Software is the property of OXID eSales and is protected
 * by copyright law - it is NOT Freeware.
 *
 * Any unauthorized use of this software without a valid license key
 * is a violation of the license agreement and will be prosecuted by
 * civil and criminal law.
 *
 * @link      http://www.oxid-esales.com
 * @copyright (C) OXID eSales AG 2003-2016
 * @version   OXID eShop EE
 */

namespace OxidEsales\EshopEnterprise\Tests\Unit\Application\Controller\Admin;

use oxDb;

/**
 * Test oxAdminList module
 */
class AdminListForAdminListTest extends \oxAdminList
{

    /**
     * force _authorize.
     *
     * @return boolean
     */
    protected function _authorize()
    {
        return true;
    }
}

/**
 * Test oxLinks module
 */
class LinksIsDerived extends \oxLinks
{
    /**
     * force isDerived.
     *
     * @return boolean
     */
    public function isDerived()
    {
        return true;
    }
}

/**
 * Class AdminListTest
 * @package OxidEsales\EshopEnterprise\Tests\Unit\Application\Controller\Admin
 */
class AdminListTest extends \OxidEsales\TestingLibrary\UnitTestCase
{
    /**
     * Test deleting
     *
     * @return null
     */
    public function testDeleteEntryIsDerivedEE()
    {
        $this->setRequestParameter('oxid', '_testId');

        // preparing test data
        $oLink = oxNew('oxlinks');
        $oLink->setId('_testId');
        $oLink->save();

        $oAdminList = $this->getProxyClass('\OxidEsales\EshopEnterprise\Tests\Unit\Application\Controller\Admin\AdminListForAdminListTest');
        $oAdminList->setNonPublicVar('_sListClass', '\OxidEsales\EshopEnterprise\Tests\Unit\Application\Controller\Admin\LinksIsDerived');
        $this->assertNull($oAdminList->deleteEntry());

        $this->assertEquals('1', oxDb::getDb()->getOne('select 1 from oxlinks where oxid = "_testId" '));
    }

    /**
     * Test unassinging item from shop
     *
     * @return null
     */
    public function testUnassignEntry()
    {
        $oLink = oxNew('oxLinks');
        $oLink->setId('_testId');
        $oLink->save();

        $this->setRequestParameter('oxid', '_testId');

        $oAdminList = $this->getProxyClass('\OxidEsales\EshopEnterprise\Tests\Unit\Application\Controller\Admin\AdminListForAdminListTest');
        $oAdminList->setNonPublicVar('_sListClass', 'oxLinks');
        $oAdminList->unassignEntry();
    }

    /**
     * Test building sql where with specified "folder" param for table oxorders
     *  If table is oxorder and folder name not specified, takes first member of
     *  orders folders array
     *
     * @return null
     */
    public function testPrepareWhereQueryWithOrderWhenFolderNotSpecified()
    {
        $this->getConfig()->setConfigParam('aOrderfolder', array('Neu' => 1, 'Old' => 2));
        $this->setRequestParameter('folder', '');

        $aWhere['oxtitle'] = '';
        $oAdminList = $this->getProxyClass('order_list');
        $sResultSql = $oAdminList->UNITprepareWhereQuery($aWhere, '');

        $sSql = " and ( oxorder.oxfolder = 'Neu' ) and oxorder.oxshopid = '1'";
        $this->assertEquals($sSql, $sResultSql);
    }
}
