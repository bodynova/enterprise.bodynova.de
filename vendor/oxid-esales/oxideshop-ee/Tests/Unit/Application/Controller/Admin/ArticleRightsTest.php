<?php
/**
 * This Software is the property of OXID eSales and is protected
 * by copyright law - it is NOT Freeware.
 *
 * Any unauthorized use of this software without a valid license key
 * is a violation of the license agreement and will be prosecuted by
 * civil and criminal law.
 *
 * @link      http://www.oxid-esales.com
 * @copyright (C) OXID eSales AG 2003-2016
 * @version   OXID eShop EE
 */
namespace OxidEsales\EshopEnterprise\Tests\Unit\Application\Controller\Admin;

use OxidEsales\TestingLibrary\UnitTestCase;
use oxArticle;

/**
 * Tests for Article_Rights class
 */
class ArticleRightsTest extends UnitTestCase
{

    /**
     * Article_Rights::Render() test case
     *
     * @return null
     */
    public function testRender()
    {
        $oView = oxNew('Article_Rights');
        $sTplName = $oView->render();

        $aViewData = $oView->getViewData();
        $this->assertTrue($aViewData["edit"] instanceof \OxidEsales\EshopCommunity\Application\Model\Article);

        $this->assertEquals('article_rights.tpl', $sTplName);
    }

}
