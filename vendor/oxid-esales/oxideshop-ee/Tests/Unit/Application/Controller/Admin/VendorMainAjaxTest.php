<?php
/**
 * This Software is the property of OXID eSales and is protected
 * by copyright law - it is NOT Freeware.
 *
 * Any unauthorized use of this software without a valid license key
 * is a violation of the license agreement and will be prosecuted by
 * civil and criminal law.
 *
 * @link      http://www.oxid-esales.com
 * @copyright (C) OXID eSales AG 2003-2016
 * @version   OXID eShop EE
 */

namespace OxidEsales\EshopEnterprise\Tests\Unit\Application\Controller\Admin;

use OxidEsales\TestingLibrary\UnitTestCase;
use oxTestModules;

/**
 * Tests for Shop_Cache class.
 */
class VendorMainAjaxTest extends UnitTestCase
{
    /**
     * AttributeMainAjax::removeVendor() test case
     *
     * @return null
     */
    public function testRemoveVendor_resetingCache()
    {
        $this->setRequestParameter("oxid", "_testVendorId");

        $oVendor = $this->getMock("oxVendor", array("load", "executeDependencyEvent"));
        $oVendor->expects($this->once())->method('load')->with($this->equalTo("_testVendorId"));
        $oVendor->expects($this->once())->method('executeDependencyEvent');
        oxTestModules::addModuleObject("oxVendor", $oVendor);

        $oView = $this->getMock("vendor_main_ajax", array("_getActionIds"));
        $oView->expects($this->any())->method('_getActionIds')->will($this->returnValue(array('_testArticle1')));

        $oView->removeVendor();
    }

    /**
     * AttributeMainAjax::addVendor() test case
     *
     * @return null
     */
    public function testAddVendor_resetingCache()
    {
        $this->setRequestParameter("synchoxid", "_testVendorId");

        $oVendor = $this->getMock("oxVendor", array("load", "executeDependencyEvent"));
        $oVendor->expects($this->once())->method('load')->with($this->equalTo("_testVendorId"));
        $oVendor->expects($this->once())->method('executeDependencyEvent');
        oxTestModules::addModuleObject("oxVendor", $oVendor);

        $oView = $this->getMock("vendor_main_ajax", array("_getActionIds", "resetCounter"));
        $oView->expects($this->any())->method('_getActionIds')->will($this->returnValue(array('_testArticle3')));
        $oView->expects($this->any())->method('resetCounter')->with($this->equalTo("vendorArticle"), $this->equalTo("_testVendorId"));

        $oView->addVendor();
    }
}
