<?php
/**
 * This Software is the property of OXID eSales and is protected
 * by copyright law - it is NOT Freeware.
 *
 * Any unauthorized use of this software without a valid license key
 * is a violation of the license agreement and will be prosecuted by
 * civil and criminal law.
 *
 * @link      http://www.oxid-esales.com
 * @copyright (C) OXID eSales AG 2003-2016
 * @version   OXID eShop EE
 */

namespace OxidEsales\EshopEnterprise\Tests\Unit\Application\Controller\Admin;

use \OxidEsales\TestingLibrary\UnitTestCase;
use \oxRegistry;
use \oxDb;
use \oxTestModules;
use \article_variant;

/**
 * Unit test class for ArticleMain.
 */
class ArticleVariant extends UnitTestCase
{
    /**
     * Initialize the fixture.
     *
     * @return null
     */
    protected function setUp()
    {
        parent::setUp();

        oxRegistry::getUtils()->oxResetFileCache();

        $iShopId = $this->getConfig()->getShopId();
        $sTable = 'oxarticles';

        $sSql = "insert into `$sTable` (oxid, oxprice, oxshopid, oxtitle)
                 values ('_testArt', '12', '{$iShopId}', 'testTitle')";
        $this->addToDatabase($sSql, $sTable);
    }

    /**
     * Tear down the fixture.
     *
     * @return null
     */
    protected function tearDown()
    {
        oxDb::getDb()->execute("delete from oxartextends where oxid = (select oxid from oxarticles where oxparentid = '_testArt')");
        oxDb::getDb()->execute("delete from oxarticles2shop where oxmapobjectid in (select oxmapid from oxarticles where oxparentid = '_testArt')");
        oxDb::getDb()->execute("delete from oxarticles where oxid = '_testArt' or oxparentid = '_testArt'");

        self::cleanUpTable('oxarticles');
        self::cleanUpTable('oxartextends');

        parent::tearDown();
    }

    /**
     * Test variant saving.
     *
     * FS#1778 FS#2366
     *
     * @return null
     */
    public function testSaveVariantForTestCases()
    {
        oxTestModules::addFunction('oxSuperCfg', 'getRights', '{return false;}');

        $this->setRequestParameter("oxid", '_testArt');

        $aParams = array(
            'oxarticles__oxvarselect' => 'testVar',
            'oxarticles__oxartnum'    => '_testVar',
            'oxarticles__oxprice'     => '12',
            'oxarticles__oxstock'     => '2',
        );

        $oView = new article_variant();
        $oView->savevariant('-1', $aParams);

        $iShopId = $this->getConfig()->getShopId();

        $sSql = "select count(*) from oxarticles2shop where oxshopid = ? and oxmapobjectid =
            (select oxmapid from oxarticles where oxid = '_testArt')";
        $this->assertEquals('1', oxDb::getDb()->getOne($sSql, array($iShopId)));

        $sSql = "select count(*) from oxarticles2shop where oxshopid = ? and oxmapobjectid =
            (select oxmapid from oxarticles where oxparentid = '_testArt')";
        $this->assertEquals('1', oxDb::getDb()->getOne($sSql, array($iShopId)));
    }
}
