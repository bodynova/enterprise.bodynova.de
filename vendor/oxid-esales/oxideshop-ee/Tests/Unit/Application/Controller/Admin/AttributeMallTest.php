<?php
/**
 * This Software is the property of OXID eSales and is protected
 * by copyright law - it is NOT Freeware.
 *
 * Any unauthorized use of this software without a valid license key
 * is a violation of the license agreement and will be prosecuted by
 * civil and criminal law.
 *
 * @link      http://www.oxid-esales.com
 * @copyright (C) OXID eSales AG 2003-2016
 * @version   OXID eShop EE
 */

namespace OxidEsales\EshopEnterprise\Tests\Unit\Application\Controller\Admin;

use \OxidEsales\TestingLibrary\UnitTestCase;

/**
 * Tests for Attribute_Mall class.
 */
class AttributeMallTest extends UnitTestCase
{
    /**
     * Attribute_Mall::Render() test case.
     */
    public function testConstructor()
    {
        // testing..
        $view = $this->getProxyClass("Attribute_Mall");
        $this->assertEquals('oxattribute', $view->getNonPublicVar("_sMallTable"));
    }
}
