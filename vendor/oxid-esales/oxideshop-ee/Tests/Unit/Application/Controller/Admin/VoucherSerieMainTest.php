<?php
/**
 * This Software is the property of OXID eSales and is protected
 * by copyright law - it is NOT Freeware.
 *
 * Any unauthorized use of this software without a valid license key
 * is a violation of the license agreement and will be prosecuted by
 * civil and criminal law.
 *
 * @link      http://www.oxid-esales.com
 * @copyright (C) OXID eSales AG 2003-2016
 * @version   OXID eShop EE
 */

namespace OxidEsales\EshopEnterprise\Tests\Unit\Application\Controller\Admin;

use OxidEsales\TestingLibrary\UnitTestCase;
use oxTestModules;
use Exception;

/**
 * Tests for Vendor_Mall class
 */
class VoucherSerieMainTest extends UnitTestCase
{
    /**
     * VoucherSerie_Main::Save() test case
     *
     * @return null
     */
    public function testSaveDerived()
    {
        oxTestModules::addFunction('oxvoucherserie', 'save', '{ throw new Exception( "save" ); }');
        oxTestModules::addFunction('oxvoucherserie', 'load', '{ $this->_blIsDerived = true; return true; }');

        // testing..
        try {
            $oView = oxNew('VoucherSerie_Main');
            $oView->save();
        } catch (Exception $oExcp) {
            $this->fail("error in VoucherSerie_Main::save()");
        }
    }
}
