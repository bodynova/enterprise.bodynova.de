<?php
/**
 * This Software is the property of OXID eSales and is protected
 * by copyright law - it is NOT Freeware.
 *
 * Any unauthorized use of this software without a valid license key
 * is a violation of the license agreement and will be prosecuted by
 * civil and criminal law.
 *
 * @link      http://www.oxid-esales.com
 * @copyright (C) OXID eSales AG 2003-2016
 * @version   OXID eShop EE
 */
namespace OxidEsales\EshopEnterprise\Tests\Unit\Application\Controller\Admin;

use OxidEsales\TestingLibrary\UnitTestCase;

/**
 * Tests for Roles_FElist class
 */
class RolesFrontendListTest extends UnitTestCase
{
    /**
     * Roles_FElist::BuildWhere() test case
     *
     * @return null
     */
    public function testBuildWhere()
    {
        // testing..
        $oView = oxNew('Roles_FElist');
        $aWhere = $oView->buildWhere();

        $this->assertTrue(isset($aWhere["oxroles.oxarea"]));
        $this->assertEquals("1", $aWhere["oxroles.oxarea"]);
        $this->assertTrue(isset($aWhere["oxroles.oxshopid"]));
        $this->assertEquals($this->getConfig()->getShopId(), $aWhere["oxroles.oxshopid"]);
    }

    /**
     * Roles_FElist::Render() test case
     *
     * @return null
     */
    public function testRender()
    {
        // testing..
        $oView = oxNew('Roles_FElist');
        $this->assertEquals('roles_felist.tpl', $oView->render());
    }
}
