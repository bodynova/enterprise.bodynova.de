<?php
/**
 * This Software is the property of OXID eSales and is protected
 * by copyright law - it is NOT Freeware.
 *
 * Any unauthorized use of this software without a valid license key
 * is a violation of the license agreement and will be prosecuted by
 * civil and criminal law.
 *
 * @link      http://www.oxid-esales.com
 * @copyright (C) OXID eSales AG 2003-2016
 * @version   OXID eShop EE
 */

namespace OxidEsales\EshopEnterprise\Tests\Unit\Application\Controller\Admin;

/**
 * Unit test class for ArticleMain.
 */
class ArticleMainTest extends \OxidEsales\TestingLibrary\UnitTestCase
{
    /**
     * Setup fixture
     */
    public function setUp()
    {
        $this->addToDatabase("replace into oxcategories set oxid='_testCategory1', oxshopid='1', oxtitle='_testCategory1'", 'oxcategories');
    }

    public function testCloneArticle()
    {
        \oxTestModules::addFunction('oxarticle', 'load', '{ $this->oxarticles__oxshopid = new oxField(); return true; }');
        \oxTestModules::addFunction('oxarticle', 'save', '{ return true; }');
        $this->setRequestParameter("oxid", "testId");

        $articleMain = $this->getMock("Article_Main", array("copyArticle", "setEditObjectId"));
        $articleMain->expects($this->once())->method('copyArticle')->will($this->returnValue("newTestId"));
        $articleMain->expects($this->once())->method('setEditObjectId')->with($this->equalTo("newTestId"));
        $articleMain->cloneArticle();
    }

    /**
     * Tests that when category is assigned to different shop, it gets proper oxobject2category relations.
     */
    public function testAddToCategoryAndAssignToOtherShopGenerateOneEntry()
    {
        $articleMain = oxNew('Article_Main');
        $articleMain->addToCategory('_testCategory1', '_testArticle1');

        $article = oxNew('oxArticle');
        $article->load('_testArticle1');
        $article->assignToShop(2);


        $category = oxNew('oxCategory');
        $category->load('_testCategory1');
        $category->assignToShop(2);

        $count = \oxDb::getDb(\oxDb::FETCH_MODE_ASSOC)->getOne("select count(*) from oxobject2category where OXCATNID = '_testCategory1' AND OXOBJECTID = '_testArticle1' and OXSHOPID = 2");
        $this->assertEquals(1, $count);
    }
}
