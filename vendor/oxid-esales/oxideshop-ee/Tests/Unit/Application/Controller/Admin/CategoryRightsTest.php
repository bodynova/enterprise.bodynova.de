<?php
/**
 * This Software is the property of OXID eSales and is protected
 * by copyright law - it is NOT Freeware.
 *
 * Any unauthorized use of this software without a valid license key
 * is a violation of the license agreement and will be prosecuted by
 * civil and criminal law.
 *
 * @link      http://www.oxid-esales.com
 * @copyright (C) OXID eSales AG 2003-2016
 * @version   OXID eShop EE
*/

/**
 * Tests for Category_Rights class
 */
class Unit_Admin_CategoryRightsTest extends OxidTestCase
{
    /**
     * Category_Rights::Render() test case
     */
    public function testRender()
    {
        oxTestModules::addFunction('oxcategory', 'load', '{ $this->blIsDerived = true; }');
        $this->setRequestParameter("oxid", "testId");

        // testing..
        $view = oxNew('Category_Rights');
        $templateName = $view->render();

        // testing view data
        $viewData = $view->getViewData();
        $this->assertTrue($viewData["edit"] instanceof \OxidEsales\EshopCommunity\Application\Model\Category);
        $this->assertTrue(isset($viewData["readonly"]));
        $this->assertEquals('category_rights.tpl', $templateName);
    }

}
