<?php
/**
 * This Software is the property of OXID eSales and is protected
 * by copyright law - it is NOT Freeware.
 *
 * Any unauthorized use of this software without a valid license key
 * is a violation of the license agreement and will be prosecuted by
 * civil and criminal law.
 *
 * @link      http://www.oxid-esales.com
 * @copyright (C) OXID eSales AG 2003-2016
 * @version   OXID eShop EE
 */

namespace OxidEsales\EshopEnterprise\Tests\Unit\Application\Controller\Admin;

use oxField;
use oxTestModules;

/**
 * Unit test class for CategoryMain.
 */
class CategoryMainTest extends \OxidEsales\TestingLibrary\UnitTestCase
{
    /**
     * Category_Main::_deleteCatPicture() no rights
     */
    public function testDeleteThumbnailNoRights()
    {
        $oItem = $this->getMock("\oxCategory", array("canUpdateField", 'canUpdate', 'isDerived'));
        $oItem->expects($this->once())->method('canUpdateField')->with($this->equalTo('oxthumb'))->will($this->returnValue(true));
        $oItem->expects($this->once())->method('canUpdate')->will($this->returnValue(false));
        $oItem->expects($this->never())->method('isDerived')->will($this->returnValue(false));

        $oItem->oxcategories__oxthumb = new oxField('testThumb.jpg');

        $oPicHandler = $this->getMock("oxUtilsPic", array('safePictureDelete'));
        $oPicHandler->expects($this->never())->method('safePictureDelete');
        $this->addClassExtension(get_class($oPicHandler), 'oxUtilsPic');

        $oView = $this->getProxyClass('Category_Main');
        $oView->UNITdeleteCatPicture($oItem, 'oxthumb');
        $this->assertEquals('testThumb.jpg', $oItem->oxcategories__oxthumb->value);
    }

    /**
     * Category_Main::_deleteCatPicture() derived category
     */
    public function testDeleteThumbnailDerived()
    {
        $oItem = $this->getMock("\oxCategory", array("canUpdateField", 'canUpdate', 'isDerived'));
        $oItem->expects($this->once())->method('canUpdateField')->with($this->equalTo('oxthumb'))->will($this->returnValue(true));
        $oItem->expects($this->once())->method('canUpdate')->will($this->returnValue(true));
        $oItem->expects($this->once())->method('isDerived')->will($this->returnValue(true));

        $oItem->oxcategories__oxthumb = new oxField('testThumb.jpg');

        $oPicHandler = $this->getMock("oxUtilsPic", array('safePictureDelete'));
        $oPicHandler->expects($this->never())->method('safePictureDelete');
        $this->addClassExtension(get_class($oPicHandler), 'oxUtilsPic');

        $oView = $this->getProxyClass('Category_Main');
        $oView->UNITdeleteCatPicture($oItem, 'oxthumb');
        $this->assertEquals('testThumb.jpg', $oItem->oxcategories__oxthumb->value);
    }

    /**
     * Category_Main::Saveinnlang() test case
     */
    public function testSaveIsDerived()
    {
        $this->setRequestParameter("oxid", "testId");
        oxTestModules::addFunction('oxcategory', 'save', '{ return true; }');
        oxTestModules::addFunction('oxcategory', 'isDerived', '{ return true; }');

        // testing..
        $oView = oxNew('Category_Main');
        $oView->save();

        $this->assertNull($oView->getViewDataElement("updatelist"));
    }

    /**
     * Category_Main::Saveinnlang() test case
     */
    public function testSaveinnlangIsDerived()
    {
        $this->setRequestParameter("oxid", "testId");
        oxTestModules::addFunction('oxcategory', 'save', '{ return true; }');
        oxTestModules::addFunction('oxcategory', 'isDerived', '{ return true; }');

        // testing..
        $oView = oxNew('Category_Main');
        $oView->saveinnlang();

        $this->assertNull($oView->getViewDataElement("updatelist"));
    }
}
