<?php
/**
 * This Software is the property of OXID eSales and is protected
 * by copyright law - it is NOT Freeware.
 *
 * Any unauthorized use of this software without a valid license key
 * is a violation of the license agreement and will be prosecuted by
 * civil and criminal law.
 *
 * @link      http://www.oxid-esales.com
 * @copyright (C) OXID eSales AG 2003-2016
 * @version   OXID eShop EE
 */

namespace OxidEsales\EshopEnterprise\Tests\Unit\Application\Controller\Admin;

class AdminLinksMallTest extends \OxidEsales\TestingLibrary\UnitTestCase
{
    public function testRender()
    {
        $oView = $this->getProxyClass("Adminlinks_Mall");
        $this->assertEquals('oxlinks', $oView->getNonPublicVar("_sMallTable"));
    }
}