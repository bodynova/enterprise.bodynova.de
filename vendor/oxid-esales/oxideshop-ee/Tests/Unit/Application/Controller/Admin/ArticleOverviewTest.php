<?php
/**
 * This Software is the property of OXID eSales and is protected
 * by copyright law - it is NOT Freeware.
 *
 * Any unauthorized use of this software without a valid license key
 * is a violation of the license agreement and will be prosecuted by
 * civil and criminal law.
 *
 * @link      http://www.oxid-esales.com
 * @copyright (C) OXID eSales AG 2003-2016
 * @version   OXID eShop EE
 */

namespace OxidEsales\EshopEnterprise\Tests\Unit\Application\Controller\Admin;

class ArticleOverviewTest extends \OxidEsales\TestingLibrary\UnitTestCase
{
    /**
     * Article_Overview::GetVariantsWhereField() test case.
     */
    public function testGetVariantsWhereField()
    {
        $query = " or oxorderarticles.oxartid='1661-01' or oxorderarticles.oxartid='1661-02'";
        $articleOverview = oxNew('Article_Overview');
        $this->assertEquals($query, $articleOverview->UNITgetVariantsWhereField("1661"));
    }
}
