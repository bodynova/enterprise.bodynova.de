<?php
/**
 * This Software is the property of OXID eSales and is protected
 * by copyright law - it is NOT Freeware.
 *
 * Any unauthorized use of this software without a valid license key
 * is a violation of the license agreement and will be prosecuted by
 * civil and criminal law.
 *
 * @link      http://www.oxid-esales.com
 * @copyright (C) OXID eSales AG 2003-2016
 * @version   OXID eShop EE
 */
namespace OxidEsales\EshopEnterprise\Tests\Unit\Application\Controller\Admin;

use OxidEsales\TestingLibrary\UnitTestCase;
use oxDb;

class ArticleRightsBuyableAjaxTest extends UnitTestCase
{
    /**
     * Initialize the fixture.
     *
     * @return null
     */
    protected function setUp()
    {
        parent::setUp();

        $sSql = "insert into oxobjectrights set oxid='_testObjectRightsRemove', " .
                "oxobjectid='_testObjectRemove', oxgroupidx='3', oxaction=2";
        oxDb::getDb()->execute($sSql);
        $sSql = "insert into oxobjectrights set oxid='_testObjectRightsRemoveAll', " .
                "oxobjectid='_testObjectRemoveAll', oxgroupidx='1', oxaction=2";
        oxDb::getDb()->execute($sSql);
    }

    /**
     * Tear down the fixture.
     *
     * @return null
     */
    protected function tearDown()
    {
        oxDb::getDb()->execute("delete from oxobjectrights where oxid='_testObjectRightsRemove'");
        oxDb::getDb()->execute("delete from oxobjectrights where oxid='_testObjectRightsRemoveAll'");

        oxDb::getDb()->execute("delete from oxobjectrights where oxobjectid='_testObjectAdd'");
        oxDb::getDb()->execute("delete from oxobjectrights where oxobjectid='_testObjectAddAll'");

        parent::tearDown();
    }

    /**
     * ArticleRightsBuyableAjax::_getQuery() test case
     *
     * @return null
     */
    public function testGetQuery()
    {
        $oView = oxNew('article_rights_buyable_ajax');
        $this->assertEquals("from oxv_oxgroups_de where 1", trim($oView->UNITgetQuery()));
    }

    /**
     * ArticleRightsBuyableAjax::_getQuery() test case
     *
     * @return null
     */
    public function testGetQueryOxid()
    {
        $sOxid = '_testArticleRightsBuyableOxid';
        $this->setRequestParameter("oxid", $sOxid);

        $oView = oxNew('article_rights_buyable_ajax');
        $sExpectedSql = "from oxv_oxgroups_de, oxobjectrights where  oxobjectrights.oxobjectid = '$sOxid' " .
                        "and  oxobjectrights.oxoffset = (oxv_oxgroups_de.oxrrid div 31) " .
                        "and  oxobjectrights.oxgroupidx & (1 << (oxv_oxgroups_de.oxrrid mod 31)) " .
                        "and oxobjectrights.oxaction = 2";
        $this->assertEquals($sExpectedSql, trim($oView->UNITgetQuery()));
    }

    /**
     * ArticleRightsBuyableAjax::_getQuery() test case
     *
     * @return null
     */
    public function testGetQuerySynchoxid()
    {
        $sSynchoxid = '_testArticleRightsBuyableSynchoxid';
        $this->setRequestParameter("synchoxid", $sSynchoxid);

        $oView = oxNew('article_rights_buyable_ajax');
        $sExpectedSql = "from oxv_oxgroups_de left join oxobjectrights " .
                        "on oxobjectrights.oxoffset = ( oxv_oxgroups_de.oxrrid div 31 ) " .
                        "and oxobjectrights.oxgroupidx & (1 << ( oxv_oxgroups_de.oxrrid mod 31 ) ) " .
                        "and oxobjectrights.oxobjectid= '$sSynchoxid' and oxobjectrights.oxaction = 2  " .
                        "where oxobjectrights.oxobjectid != '$sSynchoxid' or ( oxobjectid is null )";
        $this->assertEquals($sExpectedSql, trim($oView->UNITgetQuery()));
    }

    /**
     * ArticleRightsBuyableAjax::_getQuery() test case
     *
     * @return null
     */
    public function testGetQueryOxidSynchoxid()
    {
        $sOxid = '_testArticleRightsBuyableOxid';
        $this->setRequestParameter("oxid", $sOxid);
        $sSynchoxid = '_testArticleRightsBuyableSynchoxid';
        $this->setRequestParameter("synchoxid", $sSynchoxid);

        $oView = oxNew('article_rights_buyable_ajax');
        $sExpectedSql = "from oxv_oxgroups_de left join oxobjectrights " .
                        "on oxobjectrights.oxoffset = ( oxv_oxgroups_de.oxrrid div 31 ) " .
                        "and oxobjectrights.oxgroupidx & (1 << ( oxv_oxgroups_de.oxrrid mod 31 ) ) " .
                        "and oxobjectrights.oxobjectid= '$sSynchoxid' and oxobjectrights.oxaction = 2  " .
                        "where oxobjectrights.oxobjectid != '$sSynchoxid' or ( oxobjectid is null )";
        $this->assertEquals($sExpectedSql, trim($oView->UNITgetQuery()));
    }

    /**
     * ArticleRightsBuyableAjax::removeGroupFromView() test case
     *
     * @return null
     */
    public function testRemoveGroupFromView()
    {
        $oDb = oxDb::getDb();
        $sOxid = '_testObjectRemove';
        $this->setRequestParameter("oxid", $sOxid);
        $this->setRequestParameter("all", true);

        $oView = $this->getMock("article_rights_buyable_ajax", array("_getActionIds"));
        $oView->expects($this->any())->method('_getActionIds')->will($this->returnValue(array(0, 1)));

        $sSql = "select count(oxid) from oxobjectrights where oxid='_testObjectRightsRemove'";
        $this->assertEquals(1, $oDb->getOne($sSql));
        $oView->removeGroupFromView();
        $this->assertEquals(0, $oDb->getOne($sSql));
    }

    /**
     * ArticleRightsBuyableAjax::removeGroupFromView() test case
     *
     * @return null
     */
    public function testRemoveGroupFromViewAll()
    {
        $oDb = oxDb::getDb();
        $sOxid = '_testObjectRemoveAll';
        $this->setRequestParameter("oxid", $sOxid);
        $this->setRequestParameter("all", true);

        $this->assertEquals(1, $oDb->getOne("select count(oxid) from oxobjectrights where oxobjectid='$sOxid'"));
        $oView = oxNew('article_rights_buyable_ajax');
        $oView->removeGroupFromView();
        $this->assertEquals(0, $oDb->getOne("select count(oxid) from oxobjectrights where oxobjectid='$sOxid'"));
    }

    /**
     * ArticleRightsBuyableAjax::addGroupToView() test case
     *
     * @return null
     */
    public function testaddGroupToView()
    {
        $oDb = oxDb::getDb();
        $sSynchoxid = '_testObjectAdd';
        $this->setRequestParameter("synchoxid", $sSynchoxid);

        $oView = $this->getMock("article_rights_buyable_ajax", array("_getActionIds"));
        $oView->expects($this->any())->method('_getActionIds')->will($this->returnValue(array(0, 1)));

        $sSql = "select count(oxid) from oxobjectrights where oxobjectid='$sSynchoxid'";
        $this->assertEquals(0, $oDb->getOne($sSql));
        $oView->addGroupToView();
        $this->assertEquals(1, $oDb->getOne($sSql));
    }

    /**
     * ArticleRightsBuyableAjax::addGroupToView() test case
     *
     * @return null
     */
    public function testaddGroupToViewAll()
    {
        $oDb = oxDb::getDb();
        $sSynchoxid = '_testObjectAddAll';
        $this->setRequestParameter("synchoxid", $sSynchoxid);
        $this->setRequestParameter("all", true);

        $oView = $this->getMock("article_rights_buyable_ajax", array("_getActionIds"));
        $oView->expects($this->any())->method('_getActionIds')->will($this->returnValue(array(0, 1)));

        $sSql = "select count(oxid) from oxobjectrights where oxobjectid='$sSynchoxid'";
        $this->assertEquals(0, $oDb->getOne($sSql));
        $oView->addGroupToView();
        $sSql = "select oxgroupidx from oxobjectrights where oxobjectid='$sSynchoxid'";
        $this->assertEquals(65535, $oDb->getOne($sSql));
    }
}
