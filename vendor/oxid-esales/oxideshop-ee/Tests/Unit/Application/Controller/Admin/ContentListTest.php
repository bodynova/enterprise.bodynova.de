<?php
/**
 * This Software is the property of OXID eSales and is protected
 * by copyright law - it is NOT Freeware.
 *
 * Any unauthorized use of this software without a valid license key
 * is a violation of the license agreement and will be prosecuted by
 * civil and criminal law.
 *
 * @link      http://www.oxid-esales.com
 * @copyright (C) OXID eSales AG 2003-2016
 * @version   OXID eShop EE
 */

namespace OxidEsales\EshopEnterprise\Tests\Unit\Application\Controller\Admin;

use \OxidEsales\TestingLibrary\UnitTestCase;

/**
 * Tests for Attribute_Mall class.
 */
class ContentListTest extends UnitTestCase
{
    /**
     * Content_List::PrepareWhereQuery() test case
     *
     * @return null
     */
    public function testPrepareWhereQueryUserDefinedFolder()
    {
        $this->setRequestParameter("folder", "testFolder");
        $viewName = getviewName("oxcontents");

        // defining parameters
        $view = oxNew('Content_List');
        $realQuery = $view->UNITprepareWhereQuery(array(), "");

        $sql = " and {$viewName}.oxfolder = 'testFolder'";
        $sql .= " and {$viewName}.oxshopid = '" . $this->getConfig()->getShopId() . "'";

        $this->assertEquals($sql, $realQuery);
    }
}
