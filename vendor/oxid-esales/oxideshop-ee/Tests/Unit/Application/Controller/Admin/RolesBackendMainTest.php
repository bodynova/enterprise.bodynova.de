<?php
/**
 * This Software is the property of OXID eSales and is protected
 * by copyright law - it is NOT Freeware.
 *
 * Any unauthorized use of this software without a valid license key
 * is a violation of the license agreement and will be prosecuted by
 * civil and criminal law.
 *
 * @link      http://www.oxid-esales.com
 * @copyright (C) OXID eSales AG 2003-2016
 * @version   OXID eShop EE
 */
namespace OxidEsales\EshopEnterprise\Tests\Unit\Application\Controller\Admin;

use OxidEsales\TestingLibrary\UnitTestCase;
use oxTestModules;
use Exception;
use DOMDocument;

/**
 * Tests for Roles_BEmain class
 */
class RolesBackendMainTest extends UnitTestCase
{
    /**
     * Roles_BEmain::Render() test case
     *
     * @return null
     */
    public function testRender()
    {
        $oNavTree = $this->getMock("oxnavigationtree", array("getDomXml"));
        $oNavTree->expects($this->once())->method('getDomXml')->will($this->returnValue(new DOMDocument));

        // testing..
        $oView = $this->getMock("Roles_BEmain", array("getNavigation", "getRights"));
        $oView->expects($this->once())->method('getNavigation')->will($this->returnValue($oNavTree));
        $oView->expects($this->once())->method('getRights')->will($this->returnValue(oxNew('oxAdminRights')));
        $this->assertEquals('roles_bemain.tpl', $oView->render());
    }

    /**
     * Roles_BEmain::Save() test case
     *
     * @return null
     */
    public function testSave()
    {
        oxTestModules::addFunction('oxrole', 'save', '{ throw new Exception( "save" ); }');
        $this->getConfig()->setConfigParam('blAllowSharedEdit', true);

        // testing..
        try {
            $oView = oxNew('Roles_BEmain');
            $oView->save();
        } catch (Exception $oExcp) {
            $this->assertEquals("save", $oExcp->getMessage(), "error in Roles_BEmain::save()");

            return;
        }
        $this->fail("error in Roles_BEmain::save()");
    }
}
