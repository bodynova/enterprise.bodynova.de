<?php
/**
 * This Software is the property of OXID eSales and is protected
 * by copyright law - it is NOT Freeware.
 *
 * Any unauthorized use of this software without a valid license key
 * is a violation of the license agreement and will be prosecuted by
 * civil and criminal law.
 *
 * @link      http://www.oxid-esales.com
 * @copyright (C) OXID eSales AG 2003-2016
 * @version   OXID eShop EE
 */
namespace OxidEsales\EshopEnterprise\Tests\Unit\Application\Controller\Admin;

use OxidEsales\TestingLibrary\UnitTestCase;
use oxTestModules;
use Exception;
use DOMDocument;

/**
 * Tests for Roles_BEobject class
 */
class RolesBackendObjectTest extends UnitTestCase
{
    /**
     * Roles_BEobject::Render() test case
     *
     * @return null
     */
    public function testRender()
    {
        $oNavTree = $this->getMock("oxnavigationtree", array("getDomXml"));
        $oNavTree->expects($this->any())->method('getDomXml')->will($this->returnValue(new DOMDocument));

        // testing..
        $oView = $this->getMock("Roles_BEobject", array("getNavigation", "getRights"));
        $oView->expects($this->any())->method('getNavigation')->will($this->returnValue($oNavTree));
        $oView->expects($this->any())->method('getRights')->will($this->returnValue(oxNew('oxAdminRights')));
        $this->assertEquals('roles_beobject.tpl', $oView->render());
    }

    /**
     * Roles_BEobject::Save() test case
     *
     * @return null
     */
    public function testSave()
    {
        oxTestModules::addFunction('oxrole', 'save', '{ throw new Exception( "save" ); }');
        $this->getConfig()->setConfigParam('blAllowSharedEdit', true);

        // testing..
        try {
            $oView = oxNew('Roles_BEobject');
            $oView->save();
        } catch (Exception $oExcp) {
            $this->assertEquals("save", $oExcp->getMessage(), "error in Roles_BEobject::save()");

            return;
        }
        $this->fail("error in Roles_BEobject::save()");
    }
}
