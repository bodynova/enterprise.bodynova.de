<?php
/**
 * This Software is the property of OXID eSales and is protected
 * by copyright law - it is NOT Freeware.
 *
 * Any unauthorized use of this software without a valid license key
 * is a violation of the license agreement and will be prosecuted by
 * civil and criminal law.
 *
 * @link      http://www.oxid-esales.com
 * @copyright (C) OXID eSales AG 2003-2016
 * @version   OXID eShop EE
 */
namespace OxidEsales\EshopEnterprise\Tests\Unit\Application\Controllers\Admin;

use \PHPUnit_Framework_MockObject_MockObject as MockObject;

/**
 * Tests for Admin_FERoles class
 */
class Unit_Admin_AdminFERolesTest extends \oxUnitTestCase
{
    /**
     * Admin_FERoles::Render() test case
     *
     * @return null
     */
    public function testRender()
    {
        $oView = oxNew('Admin_FERoles');
        $this->assertEquals('admin_feroles.tpl', $oView->render());
    }
}
