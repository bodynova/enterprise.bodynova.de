<?php
/**
 * This Software is the property of OXID eSales and is protected
 * by copyright law - it is NOT Freeware.
 *
 * Any unauthorized use of this software without a valid license key
 * is a violation of the license agreement and will be prosecuted by
 * civil and criminal law.
 *
 * @link      http://www.oxid-esales.com
 * @copyright (C) OXID eSales AG 2003-2016
 * @version   OXID eShop EE
 */

namespace OxidEsales\EshopEnterprise\Tests\Unit\Application\Controller\Admin;

use OxidEsales\TestingLibrary\UnitTestCase;
use oxTestModules;
use stdClass;
use oxField;
use Exception;

/**
 * Tests for ShopMain class.
 */
class ShopMainTest extends UnitTestCase
{
    /**
     * Shop_Main::Render() test case
     */
    public function testRenderNewShop()
    {
        // testing..
        $oView = oxNew('Shop_Main');
        $this->assertEquals('shop_main_new.tpl', $oView->render());
    }

    /**
     * Shop_Main::Save() test case with updating inheritance
     *
     * @return null
     */
    public function testSaveUpdateInheritance()
    {
        // testing..
        oxTestModules::addFunction('oxshop', 'updateInheritance', '{ throw new Exception( "updateInheritance" ); }');

        $this->setRequestParameter("oxid", '-1');

        $oUser = new stdClass();

        $oUser->oxuser__oxrights = new oxField('malladmin');

        /** @var Shop_Main|PHPUnit_Framework_MockObject_MockObject $oView */
        $oView = $this->getMock('Shop_Main', array('getUser'));
        $oView->expects($this->once())->method('getUser')->will($this->returnValue($oUser));

        // testing..
        try {
            $oView->save();
        } catch (Exception $oExcp) {
            $this->assertEquals("updateInheritance", $oExcp->getMessage(), 'oxShop::updateInheritance() was expected to be called()');

            return;
        }
        $this->fail('oxShop::updateInheritance() was expected to be called');
    }

    /**
     * Shop_Main::_getNonCopyConfigVars() test case
     */
    public function testGetNonCopyConfigVars()
    {
        $aNonCopyVars = array("aSerials", "IMS", "IMD", "IMA", "sBackTag", "sUtilModule", "aModulePaths", "aModuleFiles", "aModuleVersions", "aModuleTemplates", "aModules", "aDisabledModules");
        $oView = $this->getProxyClass("Shop_Main");
        $aNonCopyVarsRes = $oView->UNITgetNonCopyConfigVars();
        $this->assertEquals(array(), array_diff($aNonCopyVars, $aNonCopyVarsRes));
    }
}
