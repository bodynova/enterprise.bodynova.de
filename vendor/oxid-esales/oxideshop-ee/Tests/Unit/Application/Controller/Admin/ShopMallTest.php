<?php
/**
 * This Software is the property of OXID eSales and is protected
 * by copyright law - it is NOT Freeware.
 *
 * Any unauthorized use of this software without a valid license key
 * is a violation of the license agreement and will be prosecuted by
 * civil and criminal law.
 *
 * @link      http://www.oxid-esales.com
 * @copyright (C) OXID eSales AG 2003-2016
 * @version   OXID eShop EE
 */

namespace OxidEsales\EshopEnterprise\Tests\Unit\Application\Controller\Admin;

use OxidEsales\TestingLibrary\UnitTestCase;
use oxTestModules;
use oxDb;
use oxField;

/**
 * Tests for ShopMall class.
 */
class ShopMallTest extends UnitTestCase
{
    protected $_iActShopId = null;

    /**
     * Initialize the fixture.
     */
    protected function setUp()
    {
        parent::setUp();

        $this->_iActShopId = oxDb::getDb()->getOne('select max( oxid )+1 as oxid from oxshops');

        // creating NON active shop
        $oShop = oxNew('oxBase');
        $oShop->init('oxshops');
        $oShop->setId($this->_iActShopId);
        $oShop->oxshops__oxactive = new oxField(0, oxField::T_RAW);
        $oShop->oxshops__oxparentid = new oxField(1, oxField::T_RAW);
        $oShop->oxshops__oxisinherited = new oxField(0, oxField::T_RAW);
        $oShop->oxshops__oxname = new oxField('testshop', oxField::T_RAW);
        $oShop->save();

        // saving shop's config variable
        $this->getConfig()->saveShopConfVar('bool', 'blMallInherit_oxarticles', 1, $this->_iActShopId);
    }

    /**
     * Tear down the fixture.
     */
    protected function tearDown()
    {
        $oShop = oxNew('oxBase');
        $oShop->init('oxshops');
        $oShop->delete($this->_iActShopId);

        // cleaning up config table
        oxDb::getDb()->execute("delete from oxshops where oxid = '{$this->_iActShopId}'");

        parent::tearDown();
    }

    /**
     * Shop_Main::Render() test case
     */
    public function testRender()
    {
        // testing..
        $oView = oxNew('Shop_Mall');
        $this->assertEquals('shop_mall.tpl', $oView->render());
    }

    /**
     * Shop_Main::Save() test case
     */
    public function testSave()
    {
        oxTestModules::addFunction('oxshop', 'load', '{ return true; }');
        oxTestModules::addFunction('oxshop', 'assign', '{ return true; }');
        oxTestModules::addFunction('oxshop', 'save', '{ return true; }');

        $this->getSession()->setVariable("malladmin", false);

        // testing..
        $oView = $this->getMock("Shop_Mall", array("saveConfVars"));
        $oView->expects($this->never())->method('saveConfVars');
        $oView->save();

        $this->getSession()->setVariable("malladmin", true);

        $oView = $this->getMock("Shop_Mall", array("saveConfVars"));
        $oView->expects($this->once())->method('saveConfVars');
        $oView->save();
    }

    /**
     * Shop_Main::changeInheritance() test case
     */
    public function testChangeInheritance()
    {
        /** @var oxShop|PHPUnit_Framework_MockObject_MockObject $oShop */
        $oShop = $this->getMock('oxShop', array('updateInheritance', 'generateViews'), array(), '', false);
        $oShop->expects($this->once())->method('updateInheritance');
        $oShop->expects($this->once())->method('generateViews');

        /** @var Shop_Mall|PHPUnit_Framework_MockObject_MockObject $oView */
        $oView = $this->getMock("Shop_Mall", array('save', '_getEditShop'));
        $oView->expects($this->once())->method('save');
        $oView->expects($this->once())->method('_getEditShop')->will($this->returnValue($oShop));
        $oView->changeInheritance();
    }
}
