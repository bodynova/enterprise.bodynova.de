<?php
/**
 * This Software is the property of OXID eSales and is protected
 * by copyright law - it is NOT Freeware.
 *
 * Any unauthorized use of this software without a valid license key
 * is a violation of the license agreement and will be prosecuted by
 * civil and criminal law.
 *
 * @link      http://www.oxid-esales.com
 * @copyright (C) OXID eSales AG 2003-2016
 * @version   OXID eShop EE
 */

namespace OxidEsales\EshopEnterprise\Tests\Unit\Application\Controller\Admin;

use \oxTestModules;

class ListComponentAjaxTest extends \OxidEsales\TestingLibrary\UnitTestCase
{
    /**
     * ajaxListComponent::resetContentCache() test case
     *
     * @return null
     */
    public function testResetContentCache()
    {
        $config = $this->getMock("oxConfig", array("getConfigParam"));
        $config->expects($this->any())->method('getConfigParam')->will($this->returnValue(false));

        $ajaxListComponent = $this->getMock('ajaxListComponent', array("getConfig"));
        $ajaxListComponent->expects($this->once())->method('getConfig')->will($this->returnValue($config));

        oxTestModules::addFunction('oxcache', 'reset', '{ throw new Exception( "reset" ); }');

        try {
            $ajaxListComponent->resetContentCache();
        } catch (\Exception $oExcp) {
            $this->assertEquals("reset", $oExcp->getMessage(), "error in ajaxListComponent::resetContentCache()");

            return;
        }
        $this->fail("error in ajaxListComponent::resetContentCache()");
    }
}