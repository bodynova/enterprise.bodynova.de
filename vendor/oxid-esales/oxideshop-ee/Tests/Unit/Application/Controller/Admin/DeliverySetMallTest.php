<?php
/**
 * This Software is the property of OXID eSales and is protected
 * by copyright law - it is NOT Freeware.
 *
 * Any unauthorized use of this software without a valid license key
 * is a violation of the license agreement and will be prosecuted by
 * civil and criminal law.
 *
 * @link      http://www.oxid-esales.com
 * @copyright (C) OXID eSales AG 2003-2016
 * @version   OXID eShop EE
 */

namespace OxidEsales\EshopEnterprise\Tests\Unit\Application\Controller\Admin;

use \OxidEsales\TestingLibrary\UnitTestCase;

/**
 * Tests for DeliverySet_Mall class
 */
class DeliverySetMallTest extends UnitTestCase
{
    /**
     * DeliverySet_Mall::Render() test case
     *
     * @return null
     */
    public function testRender()
    {
        $oView = $this->getProxyClass("DeliverySet_Mall");
        $this->assertEquals('oxdeliveryset', $oView->getNonPublicVar("_sMallTable"));
        $this->assertEquals('admin_mall.tpl', $oView->render());
    }
}
