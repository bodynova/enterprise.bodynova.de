<?php
/**
 * This Software is the property of OXID eSales and is protected
 * by copyright law - it is NOT Freeware.
 *
 * Any unauthorized use of this software without a valid license key
 * is a violation of the license agreement and will be prosecuted by
 * civil and criminal law.
 *
 * @link      http://www.oxid-esales.com
 * @copyright (C) OXID eSales AG 2003-2016
 * @version   OXID eShop EE
 */
namespace OxidEsales\EshopEnterprise\Tests\Unit\Application\Controller\Admin;

use OxidEsales\TestingLibrary\UnitTestCase;
use oxDb;

/**
 * Tests for Roles_Begroups_Ajax class
 */
class RolesFrontendGroupsAjaxTest extends UnitTestCase
{
    /**
     * Initialize the fixture.
     *
     * @return null
     */
    protected function setUp()
    {
        parent::setUp();

        oxDb::getDb()->execute("insert into oxobject2role set oxid='_testRoleRemove1', oxobjectid='_testRoleRemove'");
        oxDb::getDb()->execute("insert into oxobject2role set oxid='_testRoleRemove2', oxobjectid='_testRoleRemove'");

        oxDb::getDb()->execute("insert into oxobject2role set oxid='_testRoleRemoveAll1', oxroleid='_testRoleRemoveAll', oxobjectid='_testGroup1', oxtype = 'oxgroups'");
        oxDb::getDb()->execute("insert into oxobject2role set oxid='_testRoleRemoveAll2', oxroleid='_testRoleRemoveAll', oxobjectid='_testGroup2', oxtype = 'oxgroups'");
        oxDb::getDb()->execute("insert into oxobject2role set oxid='_testRoleRemoveAll3', oxroleid='_testRoleRemoveAll', oxobjectid='_testGroup3', oxtype = 'oxgroups'");

        oxDb::getDb()->execute("insert into oxgroups set oxid='_testGroup1', oxtitle='_testGroup1', oxactive=1");
        oxDb::getDb()->execute("insert into oxgroups set oxid='_testGroup2', oxtitle='_testGroup2', oxactive=1");
        oxDb::getDb()->execute("insert into oxgroups set oxid='_testGroup3', oxtitle='_testGroup3', oxactive=1");
    }

    /**
     * Tear down the fixture.
     *
     * @return null
     */
    protected function tearDown()
    {
        oxDb::getDb()->execute("delete from oxobject2role where oxobjectid='_testRoleRemove'");
        oxDb::getDb()->execute("delete from oxobject2role where oxroleid='_testRoleRemoveAll'");

        oxDb::getDb()->execute("delete from oxobject2role where oxroleid='_testRoleAdd'");
        oxDb::getDb()->execute("delete from oxobject2role where oxroleid='_testRoleAddAll'");

        oxDb::getDb()->execute("delete from oxgroups where oxid='_testGroup1'");
        oxDb::getDb()->execute("delete from oxgroups where oxid='_testGroup2'");
        oxDb::getDb()->execute("delete from oxgroups where oxid='_testGroup3'");

        parent::tearDown();
    }

    /**
     * RolesBeGroupsAjax::_getQuery() test case
     *
     * @return null
     */
    public function testGetQuery()
    {
        $oView = oxNew('roles_begroups_ajax');
        $this->assertEquals("FROM oxv_oxgroups_de WHERE 1", trim($oView->UNITgetQuery()));
    }

    /**
     * RolesBeGroupsAjax::_getQuery() test case
     *
     * @return null
     */
    public function testGetQuerySynchoxid()
    {
        $sSynchoxid = '_testAction';
        $this->setRequestParameter("synchoxid", $sSynchoxid);

        $oView = oxNew('roles_begroups_ajax');
        $expectedQuery =
            "FROM oxv_oxgroups_de WHERE 1  AND oxv_oxgroups_de.oxid not in (
                SELECT oxobject2role.oxobjectid
                FROM oxobject2role
                WHERE oxobject2role.oxtype = 'oxgroups'
                AND oxobject2role.oxroleid = '$sSynchoxid' )";
        $this->assertEquals($expectedQuery, trim($oView->UNITgetQuery()));
    }

    /**
     * RolesBeGroupsAjax::_getQuery() test case
     *
     * @return null
     */
    public function testGetQueryOxid()
    {
        $sOxid = '_testAction';
        $this->setRequestParameter("oxid", $sOxid);

        $oView = oxNew('roles_begroups_ajax');
        $expectedQuery =
            "FROM oxobject2role, oxv_oxgroups_de
                WHERE oxobject2role.oxtype = 'oxgroups'
                AND oxobject2role.oxroleid = '$sOxid'
                AND oxv_oxgroups_de.oxid=oxobject2role.oxobjectid";
        $this->assertEquals($expectedQuery, trim($oView->UNITgetQuery()));
    }

    /**
     * RolesBeGroupsAjax::_getQuery() test case
     *
     * @return null
     */
    public function testGetQueryOxidSynchoxid()
    {
        $sOxid = '_testAction';
        $sSynchoxid = '_testActionSynch';
        $this->setRequestParameter("oxid", $sOxid);
        $this->setRequestParameter("synchoxid", $sSynchoxid);

        $oView = oxNew('roles_begroups_ajax');
        $expectedQuery =
            "FROM oxobject2role, oxv_oxgroups_de
                WHERE oxobject2role.oxtype = 'oxgroups'
                AND oxobject2role.oxroleid = '$sOxid'
                AND oxv_oxgroups_de.oxid=oxobject2role.oxobjectid  AND oxv_oxgroups_de.oxid not in (
                SELECT oxobject2role.oxobjectid
                FROM oxobject2role
                WHERE oxobject2role.oxtype = 'oxgroups'
                AND oxobject2role.oxroleid = '$sSynchoxid' )";
        $this->assertEquals($expectedQuery, trim($oView->UNITgetQuery()));
    }

    /**
     * RolesBeGroupsAjax::removeGroupFromBeroles() test case
     *
     * @return null
     */
    public function testRemoveGroupFromBeroles()
    {
        $oView = $this->getMock("roles_begroups_ajax", array("_getActionIds"));
        $oView->expects($this->any())->method('_getActionIds')->will($this->returnValue(array('_testRoleRemove1', '_testRoleRemove2')));

        $sSql = "select count(oxid) FROM oxobject2role WHERE oxid IN ('_testRoleRemove1', '_testRoleRemove2')";
        $this->assertEquals(2, oxDb::getDb()->getOne($sSql));
        $oView->removeGroupFromBeroles();
        $this->assertEquals(0, oxDb::getDb()->getOne($sSql));
    }

    /**
     * RolesBeGroupsAjax::removeGroupFromBeroles() test case
     *
     * @return null
     */
    public function testRemoveGroupFromBerolesAll()
    {
        $sOxid = '_testRoleRemoveAll';
        $this->setRequestParameter("oxid", $sOxid);
        $this->setRequestParameter("all", true);

        $sSql = "select count(oxid) FROM oxobject2role WHERE oxroleid = '" . $sOxid . "'";
        $oView = oxNew('roles_begroups_ajax');
        $this->assertEquals(3, oxDb::getDb()->getOne($sSql));
        $oView->removeGroupFromBeroles();
        $this->assertEquals(0, oxDb::getDb()->getOne($sSql));
    }

    /**
     * RolesBeGroupsAjax::addGroupToBeroles() test case
     *
     * @return null
     */
    public function testAddGroupToBeroles()
    {
        $sSynchoxid = '_testRoleAdd';
        $this->setRequestParameter("synchoxid", $sSynchoxid);

        $sSql = "select count(oxid) FROM oxobject2role WHERE oxroleid='$sSynchoxid'";
        $this->assertEquals(0, oxDb::getDb()->getOne($sSql));

        $oView = $this->getMock("roles_begroups_ajax", array("_getActionIds"));
        $oView->expects($this->any())->method('_getActionIds')->will($this->returnValue(array('_testRoleAdd1', '_testRoleAdd2')));

        $oView->addGroupToBeroles();
        $this->assertEquals(2, oxDb::getDb()->getOne($sSql));
    }

    /**
     * RolesBeGroupsAjax::addGroupToBeroles() test case
     *
     * @return null
     */
    public function testAddGroupToBerolesAll()
    {
        $sSynchoxid = '_testRoleAddAll';
        $this->setRequestParameter("synchoxid", $sSynchoxid);
        $this->setRequestParameter("all", true);

        //count how much articles gets filtered
        $iCount = oxDb::getDb()->getOne("select count(oxv_oxgroups_de.oxid) from oxv_oxgroups_de where 1  and oxv_oxgroups_de.oxid not in (  select oxobject2role.oxobjectid from oxobject2role where oxobject2role.oxtype = 'oxgroups' and  oxobject2role.oxroleid = '" . $sSynchoxid . "' )");

        $sSql = "select count(oxid) from oxobject2role where oxroleid='$sSynchoxid'";
        $this->assertEquals(0, oxDb::getDb()->getOne($sSql));

        $oView = $this->getMock("roles_begroups_ajax", array("_getActionIds"));
        $oView->expects($this->any())->method('_getActionIds')->will($this->returnValue(array('_testRoleAdd1', '_testRoleAdd2')));

        $oView->addGroupToBeroles();
        $this->assertEquals($iCount, oxDb::getDb()->getOne($sSql));
    }
}
