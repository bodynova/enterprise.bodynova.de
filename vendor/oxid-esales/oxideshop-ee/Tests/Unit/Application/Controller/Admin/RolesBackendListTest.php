<?php
/**
 * This Software is the property of OXID eSales and is protected
 * by copyright law - it is NOT Freeware.
 *
 * Any unauthorized use of this software without a valid license key
 * is a violation of the license agreement and will be prosecuted by
 * civil and criminal law.
 *
 * @link      http://www.oxid-esales.com
 * @copyright (C) OXID eSales AG 2003-2016
 * @version   OXID eShop EE
 */
namespace OxidEsales\EshopEnterprise\Tests\Unit\Application\Controller\Admin;

use OxidEsales\TestingLibrary\UnitTestCase;
use oxTestModules;
use Exception;

/**
 * Tests for Roles_BElist class
 */
class RolesBackendListTest extends UnitTestCase
{
    /**
     * Roles_BElist::Render() test case
     *
     * @return null
     */
    public function testRender()
    {
        // testing..
        $oView = oxNew('Roles_BElist');
        $this->assertEquals('roles_belist.tpl', $oView->render());
    }

    /**
     * Roles_BElist::DeleteEntry() test case
     *
     * @return null
     */
    public function testDeleteEntry()
    {
        $this->getConfig()->setConfigParam("blAllowSharedEdit", true);
        oxTestModules::addFunction('oxrole', 'delete', '{ throw new Exception( "delete" ); }');
        oxTestModules::addFunction('oxrole', 'isDerived', '{ return false; }');

        // testing..
        try {
            $oView = oxNew('Roles_BElist');
            $oView->deleteEntry();
        } catch (Exception $oExcp) {
            $this->assertEquals("delete", $oExcp->getMessage(), "error in Roles_BElist::deleteEntry()");

            return;
        }
        $this->fail("error in Roles_BElist::deleteEntry()");
    }

    /**
     * Roles_BElist::BuildWhere() test case
     *
     * @return null
     */
    public function testBuildWhere()
    {
        // testing..
        $oView = oxNew('Roles_BElist');
        $aWhere = $oView->buildWhere();

        $this->assertTrue(isset($aWhere["oxroles.oxarea"]));
        $this->assertEquals("0", $aWhere["oxroles.oxarea"]);
        $this->assertTrue(isset($aWhere["oxroles.oxshopid"]));
        $this->assertEquals($this->getConfig()->getShopId(), $aWhere["oxroles.oxshopid"]);
    }
}
