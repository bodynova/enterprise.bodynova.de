<?php
/**
 * This Software is the property of OXID eSales and is protected
 * by copyright law - it is NOT Freeware.
 *
 * Any unauthorized use of this software without a valid license key
 * is a violation of the license agreement and will be prosecuted by
 * civil and criminal law.
 *
 * @link      http://www.oxid-esales.com
 * @copyright (C) OXID eSales AG 2003-2016
 * @version   OXID eShop EE
 */
namespace OxidEsales\EshopEnterprise\Tests\Unit\Application\Controller;

use \oxTestModules;

class UserListTest extends \OxidEsales\TestingLibrary\UnitTestCase
{
    /**
     * User_List::BuildSelectString() test case
     *
     * @return null
     */
    public function testBuildSelectString()
    {
        $this->getConfig()->setConfigParam("blMallUsers", true);

        $base = oxNew('oxBase');
        $base->init("oxarticles");
        $base->setDisableShopCheck(true);
        $query = $base->buildSelectString(null);

        // defining parameters
        $listObject = oxNew('oxArticle');

        // testing..
        $view = oxNew('User_List');
        $this->assertEquals($query, $view->UNITbuildSelectString($listObject));
    }

    /**
     * User_List::PrepareWhereQuery() test case
     *
     * @return null
     */
    public function testPrepareWhereQuery()
    {
        $expectedQuery = " and (  oxuser.oxlname testFilter or oxuser.oxlname testFilter  or  oxuser.oxfname testFilter or oxuser.oxfname testFilter )  and oxuser.oxrights != 'malladmin' and oxshopid = '1' ";

        oxTestModules::addFunction('oxUtilsString', 'prepareStrForSearch', '{ return "testUml"; }');

        // defining parameters
        $aWhere['oxuser.oxlname'] = 'testLastName';

        // testing..
        $view = $this->getMock("User_List", array("_isSearchValue", "_processFilter", "_buildFilter"));
        $view->expects($this->any())->method('_isSearchValue')->will($this->returnValue(true));
        $view->expects($this->any())->method('_processFilter')->will($this->returnValue("testValue"));
        $view->expects($this->any())->method('_buildFilter')->will($this->returnValue("testFilter"));
        $this->assertEquals($expectedQuery, $view->UNITprepareWhereQuery($aWhere, ''));
    }
}
