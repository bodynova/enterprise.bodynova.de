<?php
/**
 * This Software is the property of OXID eSales and is protected
 * by copyright law - it is NOT Freeware.
 *
 * Any unauthorized use of this software without a valid license key
 * is a violation of the license agreement and will be prosecuted by
 * civil and criminal law.
 *
 * @link      http://www.oxid-esales.com
 * @copyright (C) OXID eSales AG 2003-2016
 * @version   OXID eShop EE
 */
namespace OxidEsales\EshopEnterprise\Tests\Unit\Application\Controller;

use oxRegistry;

class AccountNoticeListTest extends \OxidEsales\TestingLibrary\UnitTestCase
{

    /**
     * Testing Account_Noticelist::getNavigationParams()
     */
    public function testGetNavigationParamsReverseProxy()
    {
        $blProxyEnabled = oxRegistry::get('oxReverseProxyBackend')->isEnabled();
        if (!$blProxyEnabled) {
            $this->markTestSkipped('Serial is not capable of reverse proxy.');
        }

        $oAccNoticeList = oxNew('Account_Noticelist');

        $oRPBackend = oxRegistry::get('oxReverseProxyBackend');
        $oRPBackend->setReverseProxyCapableDoEsi(true);

        $this->getConfig()->saveShopConfVar('bool', 'blReverseProxyActive', true);
        $this->getConfig()->saveShopConfVar('str', 'iLayoutCacheLifeTime', 3000);

        $this->setRequestParameter('anid', 'testId');

        $aParams = $oAccNoticeList->getNavigationParams();

        $this->assertEquals(null, $aParams['anid'], "Should unset anid navigation parameter");
    }
}
