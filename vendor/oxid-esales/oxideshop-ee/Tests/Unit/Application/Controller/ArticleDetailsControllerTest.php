<?php
/**
 * This Software is the property of OXID eSales and is protected
 * by copyright law - it is NOT Freeware.
 *
 * Any unauthorized use of this software without a valid license key
 * is a violation of the license agreement and will be prosecuted by
 * civil and criminal law.
 *
 * @link      http://www.oxid-esales.com
 * @copyright (C) OXID eSales AG 2003-2016
 * @version   OXID eShop EE
 */
namespace OxidEsales\EshopEnterprise\Tests\Unit\Application\Controller;

use oxField;

class ArticleDetailsControllerTest extends \OxidEsales\TestingLibrary\UnitTestCase
{
    /**
     * Test meta keywords generation.
     *
     * @return null
     */
    public function testMetaKeywords()
    {
        $oProduct = oxNew("oxArticle");
        $oProduct->load("1849");
        $oProduct->oxarticles__oxsearchkeys->value = 'testValue1 testValue2   testValue3 <br> ';

        //building category tree for category "Bar-eqipment"
        $sCatId = '30e44ab8593023055.23928895';

        $oCategoryTree = oxNew('oxCategoryList');
        $oCategoryTree->buildTree($sCatId, false, false, false);

        $oDetails = $this->getMock('Details', array('getProduct', 'getCategoryTree'));
        $oDetails->expects($this->any())->method('getProduct')->will($this->returnValue($oProduct));
        $oDetails->expects($this->any())->method('getCategoryTree')->will($this->returnValue($oCategoryTree));

        $sKeywords = $oProduct->oxarticles__oxtitle->value;

        //adding breadcrumb
        $sKeywords .= ", Party, Bar-Equipment";

        $oView = oxNew("oxUBase");
        $sTestKeywords = $oView->UNITprepareMetaKeyword($sKeywords, true) . ", testvalue1, testvalue2, testvalue3";

        $this->assertEquals($sTestKeywords, $oDetails->UNITprepareMetaKeyword(null));
    }

    public function testCanCache()
    {
        $oObj = oxNew('Details');
        $this->assertTrue($oObj->canCache());
        $this->setRequestParameter('listtype', 'search');
        $this->assertFalse($oObj->canCache());
        $this->setRequestParameter('listtype', '');
        $this->assertTrue($oObj->canCache());
        $this->setRequestParameter('listtype', 'asd');
        $this->assertTrue($oObj->canCache());
    }

    public function testGetViewId_testHash()
    {
        $view = oxNew('Details');

        $baseView = oxNew('oxUBase');
        $baseViewId = $baseView->getViewId();

        $this->setRequestParameter('anid', 'test_anid');
        $this->setRequestParameter('cnid', 'test_cnid');
        $this->setRequestParameter('listtype', 'search');
        $this->setRequestParameter('searchparam', 'test_sparam');
        $this->setRequestParameter('renderPartial', 'test_render');
        $this->setRequestParameter('varselid', 'test_varselid');
        $filters = array('test_cnid' => array(0 => 'test_filters'));
        $this->setSessionParam('session_attrfilter', $filters);

        $expectedViewId = $baseViewId . '|test_anid|search-test_sparam|test_cnid' . serialize('test_filters') . '|test_render|' . serialize('test_varselid');

        $resultViewId = $view->getViewId();
        $this->assertSame($expectedViewId, $resultViewId);
    }

    public function testGetViewResetId()
    {
        $oBaseView = oxNew('oxUBase');
        $sBaseViewId = $oBaseView->GetViewResetID();

        $oProduct = $this->getMock('oxArticle', array('getId'));
        $oProduct->expects($this->once())->method('getId')->will($this->returnValue('test_ID'));
        $oProduct->oxarticles__oxparentid = new oxField('test_parentID');

        $oView = $this->getMock($this->getProxyClassName('Details'), array('getProduct'));
        $oView->expects($this->any())->method('getProduct')->will($this->returnValue($oProduct));

        $sExpected = $sBaseViewId . '|anid=test_ID|anid=test_parentID';
        $this->assertSame($sExpected, $oView->getViewResetId());
    }
}


