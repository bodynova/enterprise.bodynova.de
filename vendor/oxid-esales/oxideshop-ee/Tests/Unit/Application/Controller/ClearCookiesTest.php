<?php
/**
 * This Software is the property of OXID eSales and is protected
 * by copyright law - it is NOT Freeware.
 *
 * Any unauthorized use of this software without a valid license key
 * is a violation of the license agreement and will be prosecuted by
 * civil and criminal law.
 *
 * @link      http://www.oxid-esales.com
 * @copyright (C) OXID eSales AG 2003-2016
 * @version   OXID eShop EE
 */
namespace OxidEsales\EshopEnterprise\Tests\Unit\Application\Controller;

use PHPUnit_Framework_MockObject_MockObject as MockObject;
use oxRegistry;

class ClearCookiesTest extends \OxidEsales\TestingLibrary\UnitTestCase
{

    /**
     * Test view render.
     *
     * @return null
     */
    public function testRender()
    {
        $_SERVER['HTTP_COOKIE'] = "shop=1";

        $oReverseProxy = $this->getMock("oxReverseProxyBackend", array("isActive"));
        $oReverseProxy->expects($this->any())->method("isActive")->will($this->returnValue(true));

        $oView = $this->getMock("clearcookies", array("_getReverseProxyBackend"));
        $oView->expects($this->once())->method("_getReverseProxyBackend")->will($this->returnValue($oReverseProxy));

        $oUtilsServer = $this->getMock('oxUtilsServer', array('setOxCookie'));
        $oUtilsServer->expects($this->at(0))->method('setOxCookie')->with($this->equalTo('shop'));
        $oUtilsServer->expects($this->at(1))->method('setOxCookie')->with($this->equalTo('language'));
        $oUtilsServer->expects($this->at(2))->method('setOxCookie')->with($this->equalTo('displayedCookiesNotification'));
        $oUtilsServer->expects($this->at(3))->method('setOxCookie')->with($this->equalTo('oxenv_key'));
        oxRegistry::set('oxUtilsServer', $oUtilsServer);

        $this->assertEquals('page/info/clearcookies.tpl', $oView->render());
    }
}
