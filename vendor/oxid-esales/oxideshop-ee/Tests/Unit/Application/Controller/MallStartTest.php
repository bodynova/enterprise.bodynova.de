<?php
/**
 * This Software is the property of OXID eSales and is protected
 * by copyright law - it is NOT Freeware.
 *
 * Any unauthorized use of this software without a valid license key
 * is a violation of the license agreement and will be prosecuted by
 * civil and criminal law.
 *
 * @link      http://www.oxid-esales.com
 * @copyright (C) OXID eSales AG 2003-2016
 * @version   OXID eShop EE
 */
namespace OxidEsales\EshopEnterprise\Tests\Unit\Application\Controller;

use OxidEsales\TestingLibrary\UnitTestCase;

/**
 * Testing MallStart class.
 */
class MallStartTest extends UnitTestCase
{
    /**
     * Test get shop links.
     */
    public function testGetShopLinks()
    {
        $mallStart = $this->getProxyClass('MallStart');

        $shopLinks = $mallStart->getShopLinks();
        $this->assertEquals($this->getConfig()->getShopConfVar('sMallShopURL', 1), $shopLinks[1]);
    }

    /**
     * Test get default shop languages.
     */
    public function testGetShopDefaultLangs()
    {
        $mallStart = $this->getProxyClass('MallStart');
        $this->assertEquals(array('1' => '0'), $mallStart->getShopDefaultLangs());
    }

    /**
     * Test get shop list.
     */
    public function testGetShopList()
    {
        $mallStart = $this->getProxyClass('MallStart');
        $shops = $mallStart->getShopList();
        $this->assertEquals('1', $shops[1]->getId());
    }
}
