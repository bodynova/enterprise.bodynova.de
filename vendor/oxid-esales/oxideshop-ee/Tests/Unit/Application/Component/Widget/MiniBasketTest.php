<?php
/**
 * This Software is the property of OXID eSales and is protected
 * by copyright law - it is NOT Freeware.
 *
 * Any unauthorized use of this software without a valid license key
 * is a violation of the license agreement and will be prosecuted by
 * civil and criminal law.
 *
 * @link      http://www.oxid-esales.com
 * @copyright (C) OXID eSales AG 2003-2016
 * @version   OXID eShop EE
 */
namespace OxidEsales\EshopEnterprise\Tests\Unit\Application\Component\Widget;

/**
 * Tests for MiniBasket class
 */
class MiniBasketTest extends \oxUnitTestCase
{
    /**
     * Testing oxwMiniBasket::isCacheable()
     *
     * @return null
     */
    public function testIsNotCacheable()
    {
        $miniBasket = oxNew('oxwMiniBasket');
        $this->assertFalse($miniBasket->isCacheable());
    }

    /**
     * Testing oxwMiniBasket::isCacheable()
     *
     * @return null
     */
    public function testIsCacheable()
    {
        $this->setRequestParameter("nocookie", 1);
        $miniBasket = oxNew('oxwMiniBasket');
        $this->assertTrue($miniBasket->isCacheable());
    }
}
