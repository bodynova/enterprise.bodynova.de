<?php
/**
 * This Software is the property of OXID eSales and is protected
 * by copyright law - it is NOT Freeware.
 *
 * Any unauthorized use of this software without a valid license key
 * is a violation of the license agreement and will be prosecuted by
 * civil and criminal law.
 *
 * @link      http://www.oxid-esales.com
 * @copyright (C) OXID eSales AG 2003-2016
 * @version   OXID eShop EE
 */
namespace OxidEsales\EshopEnterprise\Tests\Unit\Application\Component\Widget;

/**
 * Tests for ArticleDetails class
 */
class ArticleDetailsTest extends \oxUnitTestCase
{
    public function testCheckVariantAccessRights_noRights()
    {
        $variant = $this->getMock('oxArticle', array('canView'));
        $variant->expects($this->atLeastOnce())->method('canView')->will($this->returnValue(false));

        $variants = array();
        $variants[] = $variant;

        $view = $this->getMock($this->getProxyClassName('oxwArticleDetails'), array('getRights'));
        $view->expects($this->once())->method('getRights')->will($this->returnValue(true));
        $resp = $view->UNITcheckVariantAccessRights($variants);

        $this->assertSame(array(), $resp);
    }

    public function testCheckVariantAccessRights_withRights()
    {
        $variant = $this->getMock('oxArticle', array('canView'));
        $variant->expects($this->atLeastOnce())->method('canView')->will($this->returnValue(true));

        $variants = array();
        $variants[] = $variant;

        $view = $this->getMock($this->getProxyClassName('oxwArticleDetails'), array('getRights'));
        $view->expects($this->once())->method('getRights')->will($this->returnValue(true));
        $resp = $view->UNITcheckVariantAccessRights($variants);

        $this->assertSame($variants, $resp);
    }
}
