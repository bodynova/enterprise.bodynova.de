<?php
/**
 * This Software is the property of OXID eSales and is protected
 * by copyright law - it is NOT Freeware.
 *
 * Any unauthorized use of this software without a valid license key
 * is a violation of the license agreement and will be prosecuted by
 * civil and criminal law.
 *
 * @link      http://www.oxid-esales.com
 * @copyright (C) OXID eSales AG 2003-2016
 * @version   OXID eShop EE
 */

namespace OxidEsales\EshopEnterprise\Tests\Unit\Application\Component;

use OxidEsales\TestingLibrary\UnitTestCase;
use \oxTestModules;
use \oxField;
use \oxRegistry;

class UserComponentTest extends UnitTestCase
{
    /**
     * Test view logout().
     *
     * @return null
     */
    public function testLogoutForLoginFeature()
    {
        oxTestModules::addFunction("oxUser", "logout", "{ return true;}");

        $aMockFnc = array('_afterLogout', '_getLogoutLink', 'setRights', 'getParent');

        $oParent = $this->getMock('oxubase', array("isEnabledPrivateSales"));
        $oParent->expects($this->once())->method('isEnabledPrivateSales')->will($this->returnValue(true));

        $oUserView = $this->getMock('oxcmp_user', $aMockFnc);
        $oUserView->expects($this->once())->method('_afterLogout');
        $oUserView->expects($this->any())->method('_getLogoutLink')->will($this->returnValue("testurl"));
        $oUserView->expects($this->any())->method('getParent')->will($this->returnValue($oParent));

        $oUserView->expects($this->once())->method('setRights');

        $this->assertEquals('account', $oUserView->logout());
    }

    /**
     * Test _afterlogin().
     *
     * @return null
     */
    public function testAfterLogin()
    {
        $this->setRequestParameter('blPerfNoBasketSaving', true);
        $oBasket = $this->getMock('oxBasket', array('onUpdate'));
        $oBasket->expects($this->once())->method('onUpdate');

        $oSession = $this->getMock('oxSession', array('getBasket', "regenerateSessionId"));
        $oSession->expects($this->once())->method('getBasket')->will($this->returnValue($oBasket));
        $oSession->expects($this->once())->method('regenerateSessionId');

        $oUser = $this->getMock('oxcmp_user', array('inGroup'));
        $oUser->expects($this->once())->method('inGroup')->will($this->returnValue(false));

        $aMockFnc = array('getSession', "getLoginStatus", 'setRights');

        $oUserView = $this->getMock('oxcmp_user', $aMockFnc);
        $oUserView->expects($this->once())->method('getSession')->will($this->returnValue($oSession));
        $oUserView->expects($this->once())->method('getLoginStatus')->will($this->returnValue(1));

        $oUserView->expects($this->once())->method('setRights');

        $this->assertEquals('payment', $oUserView->UNITafterLogin($oUser));
    }

    /**
     * Test logout.
     *
     * @return null
     */
    public function testLogout()
    {
        oxTestModules::addFunction("oxUtils", "redirect", "{ return true;}");
        oxTestModules::addFunction("oxUser", "logout", "{ return true;}");
        $this->setRequestParameter('redirect', true);
        $blParam = $this->getConfig()->getConfigParam('sSSLShopURL');
        $this->getConfig()->setConfigParam('sSSLShopURL', true);

        $oParent = $this->getMock('oxubase', array("isEnabledPrivateSales"));
        $oParent->expects($this->once())->method('isEnabledPrivateSales')->will($this->returnValue(false));

        $aMockFnc = array('_afterLogout', '_getLogoutLink', 'setRights', 'getParent');

        $oUserView = $this->getMock('oxcmp_user', $aMockFnc);
        $oUserView->expects($this->once())->method('_afterLogout');
        $oUserView->expects($this->once())->method('_getLogoutLink')->will($this->returnValue("testurl"));
        $oUserView->expects($this->any())->method('getParent')->will($this->returnValue($oParent));
        $oUserView->expects($this->once())->method('setRights');

        $oUserView->logout();
        $this->assertEquals(3, $oUserView->getLoginStatus());
        $this->getConfig()->setConfigParam('sSSLShopURL', $blParam);
    }

    /**
     * Test _changeUser_noRedirect()().
     *
     * @return null
     */
    public function testChangeUserNoRedirect()
    {
        $this->setRequestParameter('order_remark', 'TestRemark');
        $this->setRequestParameter('blnewssubscribed', null);
        $aRawVal = array('oxuser__oxfname'     => 'fname',
            'oxuser__oxlname'     => 'lname',
            'oxuser__oxstreetnr'  => 'nr',
            'oxuser__oxstreet'    => 'street',
            'oxuser__oxzip'       => 'zip',
            'oxuser__oxcity'      => 'city',
            'oxuser__oxcountryid' => 'a7c40f631fc920687.20179984');
        $this->setRequestParameter('invadr', $aRawVal);

        $oUser = $this->getMock('oxUser', array('changeUserData', 'getNewsSubscription', 'setNewsSubscription'));
        $oUser->expects($this->once())->method('changeUserData')->with(
            $this->equalTo('test@oxid-esales.com'),
            $this->equalTo(crc32('Test@oxid-esales.com')),
            $this->equalTo(crc32('Test@oxid-esales.com')),
            $this->equalTo($aRawVal),
            null
        );
        $oUser->expects($this->once())->method('getNewsSubscription')->will($this->returnValue(oxNew('oxnewssubscribed')));
        $oUser->expects($this->once())->method('setNewsSubscription')->will($this->returnValue(1));
        $oUser->oxuser__oxusername = new oxField('test@oxid-esales.com');
        $oUser->oxuser__oxpassword = new oxField(crc32('Test@oxid-esales.com'));
        $oBasket = $this->getMock('oxBasket', array('onUpdate'));
        $oBasket->expects($this->once())->method('onUpdate');
        $oSession = $this->getMock('oxSession', array('getBasket', 'checkSessionChallenge'));
        $oSession->expects($this->once())->method('getBasket')->will($this->returnValue($oBasket));
        $oSession->expects($this->once())->method('checkSessionChallenge')->will($this->returnValue(true));

        $aMockFnc = array('getSession', 'getUser', '_getDelAddressData', 'setRights');
        $oUserView = $this->getMock($this->getProxyClassName("oxcmp_user"), $aMockFnc);
        $oUserView->expects($this->once())->method('_getDelAddressData')->will($this->returnValue(null));
        $oUserView->expects($this->any())->method('getSession')->will($this->returnValue($oSession));
        $oUserView->expects($this->once())->method('getUser')->will($this->returnValue($oUser));

        $oUserView->expects($this->once())->method('setRights');

        $this->assertTrue($oUserView->UNITchangeUser_noRedirect());
        $this->assertEquals('TestRemark', oxRegistry::getSession()->getVariable('ordrem'));
        $this->assertEquals(1, $oUserView->getNonPublicVar('_blNewsSubscriptionStatus'));
    }
}
