<?php
/**
 * This Software is the property of OXID eSales and is protected
 * by copyright law - it is NOT Freeware.
 *
 * Any unauthorized use of this software without a valid license key
 * is a violation of the license agreement and will be prosecuted by
 * civil and criminal law.
 *
 * @link      http://www.oxid-esales.com
 * @copyright (C) OXID eSales AG 2003-2016
 * @version   OXID eShop EE
 */
namespace OxidEsales\EshopEnterprise\Tests\Unit\Application\Component;

/**
 * Tests for ArticleDetails class
 */
class BasketComponentTest extends \oxUnitTestCase
{
    public function testExecuteUserChoiceFlushCache()
    {
        $this->setConfigParam("blBasketExcludeEnabled", true);

        $oCache = $this->getMock('oxReverseProxyBackend', array('isEnabled', 'set'));
        $oCache->expects($this->once())->method('isEnabled')->will($this->returnValue(true));
        $oCache->expects($this->once())->method('set');

        $this->setRequestParameter('tobasket', true);

        $oCB = $this->getMock('oxcmp_basket', array('_getReverseProxyBackend', 'getParent'));
        $oCB->expects($this->once())->method('_getReverseProxyBackend')->will($this->returnValue($oCache));

        $this->assertEquals('basket', $oCB->executeuserchoice());
    }
}
