<?php
/**
 * This Software is the property of OXID eSales and is protected
 * by copyright law - it is NOT Freeware.
 *
 * Any unauthorized use of this software without a valid license key
 * is a violation of the license agreement and will be prosecuted by
 * civil and criminal law.
 *
 * @link      http://www.oxid-esales.com
 * @copyright (C) OXID eSales AG 2003-2016
 * @version   OXID eShop EE
 */

namespace OxidEsales\EshopEnterprise\Tests\Unit\Application\Model;

use \oxDb;
use \oxField;
use \oxPrice;

class SimpleVariantTest extends \OxidEsales\TestingLibrary\UnitTestCase
{
    /**
     * Resting if magic getter returns "aSelectlist" value
     *
     * @return null
     */
    public function testAssign()
    {
        $oVariant = $this->getMock('oxSimpleVariant', array('_setShopValues'));
        $oVariant->expects($this->once())->method('_setShopValues');

        $oVariant->assign(array());
    }

    /**
     * Test set shop values
     *
     * @return null
     */
    public function testSetShopValues()
    {
        $oSubj = oxNew('oxSimpleVariant');
        $oSubj->setId("_testVar");
        $oSubj->oxarticles__oxprice = new oxField(20, oxField::T_RAW);

        $oParent = oxNew('oxArticle');
        $oParent->setId("_testArticle");
        $oParent->oxarticles__oxprice = new oxField(10);
        $oSubj->setParent($oParent);

        oxDb::getDB()->execute("insert into oxfield2shop (oxartid, oxprice, oxshopid) values ('_testVar', 25, 1 )");
        $oSubj->oxarticles__oxshopid = new oxField(2, oxField::T_RAW);
        $oSubj->oxarticles__oxprice = new oxField(20, oxField::T_RAW);
        $oSubj->UNITsetShopValues($oSubj);

        oxDb::getDB()->execute('delete from oxfield2shop where oxartid = "_testVar"');

        $this->assertEquals(25, $oSubj->oxarticles__oxprice->value);
    }

    /**
     * oxSimpleVariant::getPrice() - Mall price addition Test case.
     *
     * @return null
     */
    public function testGetPrice_MallAddition()
    {
        $oObject = $this->getMock('oxSimpleVariant', array('_getGroupPrice', 'modifyGroupPrice'));
        $oObject->expects($this->once())->method('_getGroupPrice')->will($this->returnValue(12));
        $oObject->expects($this->once())->method('modifyGroupPrice')->will($this->returnValue(15.17));

        $oCurrPrice = $oObject->getPrice();
        $this->assertTrue($oCurrPrice instanceof \OxidEsales\EshopCommunity\Core\Price, 'Response should be of type oxPrice');
        $this->assertSame(15.17, $oCurrPrice->getBruttoPrice(), 'Incorrect price calculated.');
    }

    /**
     * oxSimpleVariant::modifyGroupPrice() - Test case.
     *
     * @return null
     */
    public function testModifyGroupPrice()
    {
        $this->getConfig()->setConfigParam('iMallPriceAddition', 10.0);
        $this->getConfig()->setConfigParam('blMallInterchangeArticles', true);

        $oProxyObj = oxNew('oxSimpleVariant');
        $oObj = $this->getMock(get_class($oProxyObj), array('isAdmin'));
        $oObj->expects($this->any())->method('isAdmin')->will($this->returnValue(false));

        $this->getConfig()->setConfigParam('blMallPriceAdditionPercent', false);
        $this->assertSame(18.00, $oObj->modifyGroupPrice(8), 'Absolute price addition failed.');

        $this->getConfig()->setConfigParam('blMallPriceAdditionPercent', true);
        $this->assertSame(8.80, $oObj->modifyGroupPrice(8), 'Percental price addition failed.');
    }
}
