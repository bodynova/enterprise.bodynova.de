<?php
/**
 * This Software is the property of OXID eSales and is protected
 * by copyright law - it is NOT Freeware.
 *
 * Any unauthorized use of this software without a valid license key
 * is a violation of the license agreement and will be prosecuted by
 * civil and criminal law.
 *
 * @link      http://www.oxid-esales.com
 * @copyright (C) OXID eSales AG 2003-2016
 * @version   OXID eShop EE
 */

namespace OxidEsales\EshopEnterprise\Tests\Unit\Application\Model;

class AttributeTest extends \OxidEsales\TestingLibrary\UnitTestCase
{
    /**
     * Initialize the fixture.
     */
    protected function setUp()
    {
        parent::setUp();
        $this->_oAttr = oxNew('oxAttribute');
        $this->_oAttr->oxattribute__oxtitle = new \oxField("test", \oxField::T_RAW);
        $this->_oAttr->save();

        // article attribute
        $oNewGroup = new \oxbase();
        $oNewGroup->Init('oxobject2attribute');
        $oNewGroup->oxobject2attribute__oxobjectid = new \oxField("test_oxid", \oxField::T_RAW);
        $oNewGroup->oxobject2attribute__oxattrid = new \oxField($this->_oAttr->getId(), \oxField::T_RAW);
        $oNewGroup->oxobject2attribute__oxvalue = new \oxField("testvalue", \oxField::T_RAW);
        $oNewGroup->Save();
    }

    /**
     * Test delete attribute denied by RR.
     */
    public function testDeleteDeniedByRR()
    {
        $oAttribute = $this->getMock('oxattribute', array('canDelete'));
        $oAttribute->expects($this->once())->method('canDelete')->will($this->returnValue(false));

        $oAttribute->load($this->_oAttr->getId());
        $this->assertFalse($oAttribute->delete());
    }

    /**
     * Test get attribute id.
     */
    public function testGetArticleIds()
    {
        $articles = $this->_oAttr->getArticleIds();
        $this->assertEquals(1, count($articles));
        $this->assertEquals('test_oxid', $articles[0]);
    }
}
