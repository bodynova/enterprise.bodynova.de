<?php
/**
 * This Software is the property of OXID eSales and is protected
 * by copyright law - it is NOT Freeware.
 *
 * Any unauthorized use of this software without a valid license key
 * is a violation of the license agreement and will be prosecuted by
 * civil and criminal law.
 *
 * @link      http://www.oxid-esales.com
 * @copyright (C) OXID eSales AG 2003-2016
 * @version   OXID eShop EE
 */
namespace OxidEsales\EshopEnterprise\Tests\Unit\Application\Model;

use oxActions;
use oxField;

class ActionTest extends \OxidEsales\TestingLibrary\UnitTestCase
{
    /**
     * oxActions::delete() test case
     * Trying to delete denied action by RR (EE only)
     */
    public function testDeleteDeniedByRR()
    {
        $realAction = new oxActions();
        $realAction->oxactions__oxtitle = new oxField("test", oxField::T_RAW);
        $realAction->save();

        $articleId = 'xxx';

        $action = $this->getMock('oxActions', array('canDelete'));
        $action->expects($this->once())->method('canDelete')->will($this->returnValue(false));

        $action->load($realAction->getId());
        $action->addArticle($articleId);
        $this->assertFalse($action->delete());
    }
}
