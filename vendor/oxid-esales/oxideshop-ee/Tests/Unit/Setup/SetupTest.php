<?php
/**
 * This Software is the property of OXID eSales and is protected
 * by copyright law - it is NOT Freeware.
 *
 * Any unauthorized use of this software without a valid license key
 * is a violation of the license agreement and will be prosecuted by
 * civil and criminal law.
 *
 * @link      http://www.oxid-esales.com
 * @copyright (C) OXID eSales AG 2003-2016
 * @version   OXID eShop EE
 */

namespace OxidEsales\EshopEnterprise\Tests\Unit\Setup;

use OxidEsales\EshopCommunity\Setup\Core;
use OxidEsales\EshopEnterprise\Setup\Setup;

class SetupTest extends \OxidEsales\TestingLibrary\UnitTestCase
{
    /**
     * Testing Setup::getDefaultSerial()
     */
    public function testGetDefaultSerial()
    {
        $setup = $this->getSetup();;
        $this->assertEquals('EF7FV-B9TA8-3R3SD-MZNU4-7NWM3-AN7AU', $setup->getDefaultSerial());
    }

    /**
     * Testing Setup::getEdition()
     */
    public function testGetEdition()
    {
        $setup = $this->getSetup();
        $this->assertEquals(2, $setup->getEdition());
    }

    /**
     * @return Setup
     */
    protected function getSetup()
    {
        $core = new Core();
        return $core->getInstance('Setup');
    }
}
