<?php
/**
 * This Software is the property of OXID eSales and is protected
 * by copyright law - it is NOT Freeware.
 *
 * Any unauthorized use of this software without a valid license key
 * is a violation of the license agreement and will be prosecuted by
 * civil and criminal law.
 *
 * @link      http://www.oxid-esales.com
 * @copyright (C) OXID eSales AG 2003-2016
 * @version   OXID eShop EE
 */

namespace OxidEsales\EshopEnterprise\Tests\Unit\Model;

use \OxidEsales\TestingLibrary\UnitTestCase;
use \oxField;
use \oxBase;
use \oxDb;

class GroupsTest extends UnitTestCase
{
    /**
     * Initialize the fixture.
     *
     * @return null
     */
    protected function setUp()
    {
        parent::setUp();
        $group = oxNew('oxgroups');
        $group->setId('testgroup');
        $group->oxgroups__oxtitle = new oxfield('testgroup');
        $group->oxgroups__oxactive = new oxfield(1);
        $group->save();

        // EE only
        $objectToRole = new oxbase();
        $objectToRole->init('oxobject2role');
        $objectToRole->oxobject2role__oxobjectid = new oxfield($group->getId());
        $objectToRole->oxobject2role__oxroleid = new oxfield('testrole');
        $objectToRole->oxobject2role__oxtype = new oxfield('oxgroups');
        $objectToRole->save();

        $offset = ( int ) ($group->oxgroups__oxrrid->value / 31);
        $bitmap = 1 << ($group->oxgroups__oxrrid->value % 31);

        $objectToRights = new oxbase();
        $objectToRights->init('oxobjectrights');
        $objectToRights->oxobjectrights__oxobjectid = new oxfield('xxx');
        $objectToRights->oxobjectrights__oxgroupidx = new oxfield($bitmap);
        $objectToRights->oxobjectrights__oxoffset = new oxfield($offset);
        $objectToRights->oxobjectrights__oxaction = new oxfield(1);
        $objectToRights->save();
    }

    /**
     * Tear down the fixture.
     *
     * @return null
     */
    protected function tearDown()
    {
        $group = oxNew('oxgroups');
        $group->delete('testgroup');
        $group->delete('testgroup2');
        parent::tearDown();
    }

    /**
     * Testing if insert creates correct rights index (only for EE)
     */
    public function testInsert()
    {
        $group = oxNew('oxgroups');
        $group->setId('testgroup2');
        $group->oxgroups__oxtitle = new oxfield('testgroupdesc');
        $group->save(); //no delete needed, as the db is cleaned up automatically

        $this->assertNotNull($group->oxgroups__oxrrid->value);

        $rrid = $group->oxgroups__oxrrid->value;

        //check that the given rrid is unique
        $myDb = oxDb::getDb();
        $res = $myDb->GetAll("select oxid from oxgroups where oxrrid = $rrid ");
        $this->assertEquals(count($res), 1);
    }

    // 3. trying to delete denied action by RR (EE only)
    public function testDeleteDeniedByRR()
    {
        $oGroups = $this->getMock('oxgroups', array('canDelete'));
        $oGroups->expects($this->once())->method('canDelete')->will($this->returnValue(false));

        $this->assertFalse($oGroups->delete('testDelete'));
    }

    public function testCanRead()
    {
        $groups = oxNew('oxgroups');
        $this->assertTrue($groups->canRead());
    }

    public function testDelete()
    {
        $myDB = oxDb::getDb();

        // selecting count from DB
        $group = oxNew('oxgroups');
        $group->Load('testgroup');
        $group->delete();

        $offset = ( int ) ($group->oxgroups__oxrrid->value / 31);
        $bitmap = 1 << ($group->oxgroups__oxrrid->value % 31);

        $this->assertEquals(0, $myDB->getOne("select count(*) from oxobject2role where oxobjectid='testgroup'"));
        $this->assertEquals(0, $myDB->getOne("select count(*) from oxobjectrights where oxoffset = $offset and oxgroupidx & $bitmap "));
    }

    // if this function begins to return false, probably shop does not run at all :)
    public function testCanReadField()
    {
        $oGroups = oxNew('oxgroups');
        $this->assertTrue($oGroups->canReadField('xxx'));
    }
}
