<?php
/**
 * This Software is the property of OXID eSales and is protected
 * by copyright law - it is NOT Freeware.
 *
 * Any unauthorized use of this software without a valid license key
 * is a violation of the license agreement and will be prosecuted by
 * civil and criminal law.
 *
 * @link      http://www.oxid-esales.com
 * @copyright (C) OXID eSales AG 2003-2016
 * @version   OXID eShop EE
 */

namespace OxidEsales\EshopEnterprise\Tests\Unit\Model;

use \OxidEsales\TestingLibrary\UnitTestCase;

class DeliveryTest extends UnitTestCase
{
    /**
     * Trying to delete denied action by RR.
     */
    public function testDeleteDeniedByRR()
    {
        $deliveryMock = $this->getMock('oxdelivery', array('canDelete'));
        $deliveryMock->expects($this->once())->method('canDelete')->will($this->returnValue(false));

        $this->assertFalse($deliveryMock->delete('anyOxid'));
    }
}
