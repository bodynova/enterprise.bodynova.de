<?php
/**
 * This Software is the property of OXID eSales and is protected
 * by copyright law - it is NOT Freeware.
 *
 * Any unauthorized use of this software without a valid license key
 * is a violation of the license agreement and will be prosecuted by
 * civil and criminal law.
 *
 * @link      http://www.oxid-esales.com
 * @copyright (C) OXID eSales AG 2003-2016
 * @version   OXID eShop EE
 */

namespace OxidEsales\EshopEnterprise\Tests\Unit\Model;

use \OxidEsales\TestingLibrary\UnitTestCase;
use \oxField;

class NewsSubscribedTest extends UnitTestCase
{
    private $_oNewsSub = null;

    /**
     * Initialize the fixture.
     *
     * @return null
     */
    protected function setUp()
    {
        parent::setUp();
        $this->_oNewsSub = oxNew("oxnewssubscribed");
        $this->_oNewsSub->setId('_testNewsSubscrId');
        $this->_oNewsSub->oxnewssubscribed__oxuserid = new oxField('_testUserId', oxField::T_RAW);
        $this->_oNewsSub->oxnewssubscribed__oxemail = new oxField('useremail@useremail.nl', oxField::T_RAW);
        $this->_oNewsSub->oxnewssubscribed__oxdboptin = new oxField('1', oxField::T_RAW);
        $this->_oNewsSub->oxnewssubscribed__oxunsubscribed = new oxField('0000-00-00 00:00:00', oxField::T_RAW);
        $this->_oNewsSub->save();
    }

    /**
     * Testing email subscription loader by user email with mall user.
     */
    public function testLoadFromEMailMallUser()
    {
        $oNewsSubscribed = oxNew('oxnewssubscribed');
        $oNewsSubscribed->setMallUsers(true);

        $this->assertTrue($oNewsSubscribed->loadFromEmail('useremail@useremail.nl'));
        $this->assertEquals('_testNewsSubscrId', $oNewsSubscribed->oxnewssubscribed__oxid->value);
    }

    /**
     * Testing subscription loading by userid in different shops
     */
    public function testLoadFromUserIdDifferentShops()
    {
        // assert that user is subscribed in first shop
        $sShopId1 = $this->getConfig()->getShopId();
        $oNewsSubscribed = oxNew('oxnewssubscribed');
        $this->assertTrue($oNewsSubscribed->loadFromUserId('_testUserId'));
        $this->assertEquals($sShopId1, $oNewsSubscribed->oxnewssubscribed__oxshopid->value);
        $this->assertEquals(1, $oNewsSubscribed->oxnewssubscribed__oxdboptin->value);

        // set second shop
        $sShopId2 = $sShopId1 + 1;
        $this->getConfig()->setShopId($sShopId2);

        // assert that user is not subscribed in second shop
        $oNewsSubscribed = oxNew('oxnewssubscribed');
        $this->assertFalse($oNewsSubscribed->loadFromUserId('_testUserId'));

        // subscribe user in second shop
        $oNewsSubscribed->setId('_testNewsSubscrId2');
        $oNewsSubscribed->oxnewssubscribed__oxuserid = new oxField('_testUserId', oxField::T_RAW);
        $oNewsSubscribed->oxnewssubscribed__oxdboptin = new oxField(1, oxField::T_RAW);
        $oNewsSubscribed->save();

        // assert that user is subscribed in second shop
        $oNewsSubscribed = oxNew('oxnewssubscribed');
        $this->assertTrue($oNewsSubscribed->loadFromUserId('_testUserId'));
        $this->assertEquals($sShopId2, $oNewsSubscribed->oxnewssubscribed__oxshopid->value);
        $this->assertEquals(1, $oNewsSubscribed->oxnewssubscribed__oxdboptin->value);

        // unsuscribe user in second shop and assert
        $oNewsSubscribed->oxnewssubscribed__oxdboptin->value = new oxField(0, oxField::T_RAW);
        $oNewsSubscribed->save();
        $this->assertTrue($oNewsSubscribed->loadFromUserId('_testUserId'));
        $this->assertEquals(0, $oNewsSubscribed->oxnewssubscribed__oxdboptin->value);

        // set first shop
        $this->getConfig()->setShopId($sShopId1);

        // check if user is still subscribed in first shop
        $oNewsSubscribed = oxNew('oxnewssubscribed');
        $this->assertTrue($oNewsSubscribed->loadFromUserId('_testUserId'));
        $this->assertEquals($sShopId1, $oNewsSubscribed->oxnewssubscribed__oxshopid->value);
        $this->assertEquals(1, $oNewsSubscribed->oxnewssubscribed__oxdboptin->value);
    }
}
