<?php
/**
 * This Software is the property of OXID eSales and is protected
 * by copyright law - it is NOT Freeware.
 *
 * Any unauthorized use of this software without a valid license key
 * is a violation of the license agreement and will be prosecuted by
 * civil and criminal law.
 *
 * @link      http://www.oxid-esales.com
 * @copyright (C) OXID eSales AG 2003-2016
 * @version   OXID eShop EE
 */

namespace OxidEsales\EshopEnterprise\Tests\Unit\Model;

use Exception;
use oxConnectionException;
use oxException;
use oxField;
use oxRegistry;
use oxTestModules;
use oxUser;
use oxUserException;
use oxUtilsObject;

class UserTest extends \OxidEsales\TestingLibrary\UnitTestCase
{
    protected $_aUsers = array();

    /**
     * Tear down the fixture.
     *
     * @return null
     */
    protected function tearDown()
    {
        foreach ($this->_aUsers as $sUserId => $oUser) {
            /** @var oxUser $oUser */
            $oUser->delete($sUserId);
        }
        parent::tearDown();
    }

    public function testChangeUserDataWithIncorrectVatId()
    {
        $oUser = $this->getMock("oxUser", array("checkValues", "assign", "save", "_setAutoGroups"));

        $oUser->expects($this->once())->method('checkValues')->with('user', 'password', 'password2', 'inv-address', 'del-address');
        $oUser->expects($this->once())->method('assign')->with($this->equalto('inv-address'));
        $oUser->expects($this->once())->method('save')->will($this->returnValue(false));
        $oUser->expects($this->never())->method('_setAutoGroups');

        $oInputValidator = $this->getMock('oxInputValidator');
        $oInputValidator->expects($this->once())->method('checkVatId')->with($this->isInstanceOf('\OxidEsales\EshopCommunity\Application\Model\User'), 'inv-address')->will($this->throwException(new oxException()));
        oxRegistry::set('oxInputValidator', $oInputValidator);

        $oUser->changeUserData('user', 'password', 'password2', 'inv-address', 'del-address');

        $this->assertEquals(0, $oUser->oxuser__oxustidstatus->value);
    }

    /**
     * User creation problems in MALL
     * EE only
     */
    // saving subshop admins details in main shop, users shop id and rights must not be changed
    public function testSubShopAminInMainShop()
    {
        $oUser = $this->createUser();
        $sUserId = $oUser->getId();

        // creating subshop user ..
        $oUser = oxNew('oxBase');
        $oUser->init('oxuser');
        $oUser->load($sUserId);
        $oUser->oxuser__oxshopid = new oxField(2, oxField::T_RAW);
        $oUser->oxuser__oxrights = new oxField('malladmin', oxField::T_RAW);
        $oUser->save();

        // saving user in base shop
        $oUser = oxNew('oxUser');
        $oUser->load($sUserId);
        $oUser->save();

        // now testing for user rights ..
        $this->assertEquals(2, $oUser->oxuser__oxshopid->value, 'changed user shop id');
        $this->assertEquals('malladmin', $oUser->oxuser__oxrights->value, 'changed user rights');
    }

    /**
     * Testing user object saving if birthday is added.
     */
    public function testSaveWithBirthDay()
    {
        $oUser = oxNew('oxUser');
        $oUser->setId($oUser->getId());
        $oUser->oxuser__oxbirthdate = new oxField(array('day' => '12', 'month' => '12', 'year' => '1212'), oxField::T_RAW);
        $oUser->save();
        $this->assertEquals('1212-12-12', $oUser->oxuser__oxbirthdate->value);
    }

    public function testDelete()
    {
        $oDb = $this->getDb();

        $oUser = $this->createUser();
        $sUserId = $oUser->getId();

        $o2r = oxNew('oxBase');
        $o2r->init("oxobject2role");
        $o2r->setId("_testo2r");
        $o2r->oxobject2role__oxobjectid = new oxField($sUserId);
        $o2r->oxobject2role__oxroleid = new oxField($sUserId);
        $o2r->save();

        $oUser = oxNew('oxUser');
        $oUser->load($sUserId);
        $oUser->delete();

        $aWhat['oxobject2role'] = 'oxobjectid';

        // now checking if all related records were deleted
        foreach ($aWhat as $sTable => $sField) {
            $sQ = 'select count(*) from ' . $sTable . ' where ' . $sField . ' = "' . $sUserId . '" ';

            if ($sTable == 'oxremark') {
                $sQ .= " AND oxtype ='o'";
            }

            $iCnt = $oDb->getOne($sQ);
            if ($iCnt > 0) {
                $this->fail($iCnt . ' records were not deleted from "' . $sTable . '" table');
            }
        }
    }

    // Checking order count for random user but emulating non multishop. order count must be 1
    public function testGetOrdersForRandumUserNotMultishop()
    {
        $myConfig = $this->getConfig();

        $oActShop = oxNew('oxShop');
        $oActShop->sShopID = $myConfig->getShopId();
        $oActShop->oxshops__oxismultishop = new oxField(false, oxField::T_RAW);

        $this->getConfig()->setConfigParam('oActShop', $oActShop);

        $oUser = $this->createUser();
        $oOrders = $oUser->getOrders();

        // checking order count
        $this->assertEquals(1, count($oOrders));
    }

    public function testCheckValuesWithConnectionException()
    {
        $oInputValidator = $this->getMock('oxInputValidator');
        $oInputValidator->expects($this->once())->method("checkVatId")->will($this->throwException(new oxConnectionException()));
        oxRegistry::set('oxInputValidator', $oInputValidator);

        $oUser = oxNew('oxUser');
        $oUser->checkValues("X", "X", "X", array(), array());
    }

    /**
     * Testing customer information update function
     */
    // 1. all data "is fine" (emulated), just checking if all necessary methods were called
    public function testChangeUserDataAllDataIsFine()
    {
        $oUser = $this->getMock("oxUser", array("checkValues", "assign", "save", "_setAutoGroups"));
        $oUser->expects($this->once())->method('checkValues');
        $oUser->expects($this->once())->method('assign');
        $oUser->expects($this->once())->method('save')->will($this->returnValue(true));
        $oUser->expects($this->once())->method('_setAutoGroups');

        $oInputValidator = $this->getMock('oxInputValidator');
        $oInputValidator->expects($this->once())->method('checkVatId')->will($this->returnValue(null));
        oxRegistry::set('oxInputValidator', $oInputValidator);

        $oUser->changeUserData(null, null, null, null, null, null, null, null, null, null);

        $this->assertEquals(1, $oUser->oxuser__oxustidstatus->value);
    }

    /**
     * oxuser::login() and oxuser::logout() test
     */
    public function testLoginOxidNotSet()
    {
        $this->getConfig()->setConfigParam('blUseLDAP', 1);
        $this->getConfig()->setConfigParam('blMallUsers', 1);

        $oUser = $this->getMock('oxuser', array('load', '_ldapLogin'));
        $oUser->expects($this->atLeastOnce())->method('load')->will($this->returnValue(true));
        $oUser->expects($this->once())->method('_ldapLogin')->with($this->equalTo(oxADMIN_LOGIN), $this->equalTo(oxADMIN_PASSWD), $this->equalTo($this->getConfig()->getShopId()), $this->equalTo(''));

        try {
            $oUser->login(oxADMIN_LOGIN, oxADMIN_PASSWD);
        } catch (Exception $oExcp) {
            $this->assertEquals('ERROR_MESSAGE_USER_NOVALIDLOGIN', $oExcp->getMessage());

            return;
        }
        $this->fail('exception must be thrown due to problems loading user object');
    }

    public function testCanRead()
    {
        $oUser = oxNew('oxUser');
        $this->assertTrue($oUser->canRead());
    }

    // if this function begins to return false, probably shop does not run at all :)
    public function testCanReadField()
    {
        $oUser = oxNew('oxUser');
        $this->assertTrue($oUser->canReadField('xxx'));
    }

    /**
     * oxUser::_ldapLogin() test case.
     */
    public function testLdapLoginDataMapFails()
    {
        $aLdapParams = array('HOST'      => 'HOST',
            'PORT'      => 'PORT',
            'USERQUERY' => 'USERQUERY',
            'BASEDN'    => 'BASEDN',
            'FILTER'    => 'FILTER',
            'DATAMAP'   => 'DATAMAP');

        $oConfig = $this->getConfig();
        $oConfig->setConfigParam("aLDAPParams", $aLdapParams);


        $oLdap = $this->getMock("oxLDAP", array("login", "mapData"), array(), '', false);
        $oLdap->expects($this->once())->method('login')->with(
            $this->equalTo("testUser"),
            $this->equalTo("testPassword"),
            $this->equalTo("USERQUERY"),
            $this->equalTo("BASEDN"),
            $this->equalTo("FILTER")
        );
        $oLdap->expects($this->once())->method('mapData')->with($this->equalTo('DATAMAP'));
        oxTestModules::addModuleObject("oxLDAP", $oLdap);

        try {
            $oUser = $this->getMock("oxuser", array("setId", "setPassword", "save", "load"));
            $oUser->expects($this->never())->method('setId');
            $oUser->expects($this->never())->method('setPassword');
            $oUser->expects($this->never())->method('save');
            $oUser->expects($this->never())->method('load');
            $oUser->UNITldapLogin("testUser", "testPassword", "testshopid", "");
        } catch (\OxidEsales\EshopCommunity\Core\Exception\UserException $oExcp) {
            return;
        }
        $this->fail("Error while runing testLdapLogin");
    }

    /**
     * oxUser::_ldapLogin() test case.
     */
    public function testLdapLogin()
    {
        $aReturn["OXUSERNAME"] = $this->getDb()->getOne("select oxusername from oxuser where oxid = 'oxdefaultadmin'");
        $aLdapParams = array('HOST'      => 'HOST',
            'PORT'      => 'PORT',
            'USERQUERY' => 'USERQUERY',
            'BASEDN'    => 'BASEDN',
            'FILTER'    => 'FILTER',
            'DATAMAP'   => 'DATAMAP');

        $oConfig = $this->getConfig();
        $oConfig->setConfigParam("aLDAPParams", $aLdapParams);


        $oLdap = $this->getMock("oxLDAP", array("login", "mapData"), array(), '', false);
        $oLdap->expects($this->once())->method('login')->with(
            $this->equalTo("testUser"),
            $this->equalTo("testPassword"),
            $this->equalTo("USERQUERY"),
            $this->equalTo("BASEDN"),
            $this->equalTo("FILTER")
        );
        $oLdap->expects($this->once())->method('mapData')->with($this->equalTo('DATAMAP'))->will($this->returnValue($aReturn));
        oxTestModules::addModuleObject("oxLDAP", $oLdap);

        $oUser = $this->getMock("oxuser", array("setId", "setPassword", "save", "load"));
        $oUser->expects($this->never())->method('setId');
        $oUser->expects($this->never())->method('setPassword');
        $oUser->expects($this->never())->method('save');
        $oUser->expects($this->once())->method('load');
        $oUser->UNITldapLogin("testUser", "testPassword", "testshopid", "");
    }

    /**
     * oxUser::_ldapLogin() test case.
     */
    public function testLdapLoginCreatignNewUser()
    {
        $aReturn["OXUSERNAME"] = "testUser";
        $aLdapParams = array('HOST'      => 'HOST',
            'PORT'      => 'PORT',
            'USERQUERY' => 'USERQUERY',
            'BASEDN'    => 'BASEDN',
            'FILTER'    => 'FILTER',
            'DATAMAP'   => 'DATAMAP');

        $oConfig = $this->getConfig();
        $oConfig->setConfigParam("aLDAPParams", $aLdapParams);


        $oLdap = $this->getMock("oxLDAP", array("login", "mapData"), array(), '', false);
        $oLdap->expects($this->once())->method('login')->with(
            $this->equalTo("testUser"),
            $this->equalTo("testPassword"),
            $this->equalTo("USERQUERY"),
            $this->equalTo("BASEDN"),
            $this->equalTo("FILTER")
        );
        $oLdap->expects($this->once())->method('mapData')->with($this->equalTo('DATAMAP'))->will($this->returnValue($aReturn));
        oxTestModules::addModuleObject("oxLDAP", $oLdap);

        $oUser = $this->getMock("oxuser", array("setId", "setPassword", "save", "load"));
        $oUser->expects($this->once())->method('setId');
        $oUser->expects($this->once())->method('setPassword');
        $oUser->expects($this->once())->method('save');
        $oUser->expects($this->never())->method('load');
        $oUser->UNITldapLogin("testUser", "testPassword", "testshopid", "");
    }

    /**
     * Testing if shopselect is got by ldap.
     */
    public function testLoginLdapGotShopSelectAdmin()
    {
        $this->getConfig()->setConfigParam("blUseLDAP", true);
        $this->getConfig()->getConfigParam("blMallUsers", false);

        $oUser = $this->getMock("oxuser", array("getId"));
        $oUser->expects($this->any())->method('getId ')->will($this->returnValue(null));
        $sShopSelect = " and ( oxrights != 'user' ) ";
        $this->assertEquals(
            $sShopSelect,
            $oUser->_getShopSelect($this->getConfig(), $this->getConfig()->getShopId(), true)
        );
    }

    /**
     * Test case for oxUSer::_getLoginQuery() - staging mode.
     */
    public function testGetLoginQuery_stagingMode_InvalidLogin()
    {
        $oConfig = $this->getMock("oxConfig", array("isStagingMode"));
        $oConfig->expects($this->once())->method('isStagingMode')->will($this->returnValue(true));

        $oUser = $this->getMock("oxUser", array("getConfig"), array(), '', false);
        $oUser->expects($this->any())->method('getConfig')->will($this->returnValue($oConfig));

        $this->assertNotNull($oUser->UNITgetLoginQuery("notadmin", "notadmin", 1, true));
    }


    /**
     * Test case for oxUSer::_getLoginQuery() - staging mode.
     */
    public function testGetLoginQuery_stagingMode()
    {
        $sWhat = "`oxid`";

        $oConfig = $this->getMock("oxConfig", array("isStagingMode"));
        $oConfig->expects($this->once())->method('isStagingMode')->will($this->returnValue(true));

        $oUser = $this->getMock("oxUser", array("getConfig"), array(), '', false);
        $oUser->expects($this->any())->method('getConfig')->will($this->returnValue($oConfig));

        $sQ = "select $sWhat from oxuser where oxrights = 'malladmin' ";

        $this->assertEquals($sQ, $oUser->UNITgetLoginQuery("admin", "admin", 1, true));
    }

    /**
     * Creates user.
     *
     * @param string $sUserName
     * @param int    $iActive
     * @param string $sRights either user or malladmin
     * @param int    $sShopId User shop ID
     *
     * @return oxUser
     */
    protected function createUser($sUserName = null, $iActive = 1, $sRights = 'user', $sShopId = null)
    {
        $oUtils = oxRegistry::getUtils();
        $oDb = $this->getDb();

        $iLastNr = count($this->_aUsers) + 1;

        if (is_null($sShopId)) {
            $sShopId = $this->getConfig()->getShopId();
        }

        $oUser = oxNew('oxUser');
        $oUser->oxuser__oxshopid = new oxField($sShopId, oxField::T_RAW);
        $oUser->oxuser__oxactive = new oxField($iActive, oxField::T_RAW);
        $oUser->oxuser__oxrights = new oxField($sRights, oxField::T_RAW);

        // setting name
        $sUserName = $sUserName ? $sUserName : 'test' . $iLastNr . '@oxid-esales.com';
        $oUser->oxuser__oxusername = new oxField($sUserName, oxField::T_RAW);
        $oUser->oxuser__oxpassword = new oxField(crc32($sUserName), oxField::T_RAW);
        $oUser->oxuser__oxcountryid = new oxField("testCountry", oxField::T_RAW);
        $oUser->save();

        $sUserId = $oUser->getId();
        $sId = oxUtilsObject::getInstance()->generateUID();

        // loading user groups
        $sGroupId = $oDb->getOne('select oxid from oxgroups order by rand() ');
        $sQ = 'insert into oxobject2group (oxid,oxshopid,oxobjectid,oxgroupsid) values ( "' . $sUserId . '", "' . $sShopId . '", "' . $sUserId . '", "' . $sGroupId . '" )';
        $oDb->Execute($sQ);

        $sQ = 'insert into oxorder ( oxid, oxshopid, oxuserid, oxorderdate ) values ( "' . $sId . '", "' . $sShopId . '", "' . $sUserId . '", "' . date('Y-m-d  H:i:s', time() + 3600) . '" ) ';
        $oDb->Execute($sQ);

        // adding article to order
        $sArticleID = $oDb->getOne('select oxid from oxarticles order by rand() ');
        $sQ = 'insert into oxorderarticles ( oxid, oxorderid, oxamount, oxartid, oxartnum ) values ( "' . $sId . '", "' . $sId . '", 1, "' . $sArticleID . '", "' . $sArticleID . '" ) ';
        $oDb->Execute($sQ);

        // adding article to basket
        $sQ = 'insert into oxuserbaskets ( oxid, oxuserid, oxtitle ) values ( "' . $sUserId . '", "' . $sUserId . '", "oxtest" ) ';
        $oDb->Execute($sQ);

        $sArticleID = $oDb->getOne('select oxid from oxarticles order by rand() ');
        $sQ = 'insert into oxuserbasketitems ( oxid, oxbasketid, oxartid, oxamount ) values ( "' . $sUserId . '", "' . $sUserId . '", "' . $sArticleID . '", "1" ) ';
        $oDb->Execute($sQ);

        // creating test address
        $sCountryId = $oDb->getOne('select oxid from oxcountry where oxactive = "1"');
        $sQ = 'insert into oxaddress ( oxid, oxuserid, oxaddressuserid, oxcountryid ) values ( "test_user' . $iLastNr . '", "' . $sUserId . '", "' . $sUserId . '", "' . $sCountryId . '" ) ';
        $oDb->Execute($sQ);

        // creating test executed user payment
        $aDynValue = $this->_aDynPaymentFields;
        $oPayment = oxNew('oxPayment');
        $oPayment->load('oxidcreditcard');
        $oPayment->setDynValues($oUtils->assignValuesFromText($oPayment->oxpayments__oxvaldesc->value, true, true, true));

        $aDynValues = $oPayment->getDynValues();
        while (list($key, $oVal) = each($aDynValues)) {
            $oVal = new oxField($aDynValue[$oVal->name], oxField::T_RAW);
            $oPayment->setDynValue($key, $oVal);
            $aDynVal[$oVal->name] = $oVal->value;
        }

        $sDynValues = '';
        if (isset($aDynVal)) {
            $sDynValues = $oUtils->assignValuesToText($aDynVal);
        }

        $sQ = 'insert into oxuserpayments ( oxid, oxuserid, oxpaymentsid, oxvalue ) values ( "' . $sId . '", "' . $sUserId . '", "oxidcreditcard", "' . $sDynValues . '" ) ';
        $oDb->Execute($sQ);

        $this->_aUsers[$sUserId] = $oUser;

        return $oUser;
    }
}
