<?php
/**
 * This Software is the property of OXID eSales and is protected
 * by copyright law - it is NOT Freeware.
 *
 * Any unauthorized use of this software without a valid license key
 * is a violation of the license agreement and will be prosecuted by
 * civil and criminal law.
 *
 * @link      http://www.oxid-esales.com
 * @copyright (C) OXID eSales AG 2003-2016
 * @version   OXID eShop EE
 */

namespace OxidEsales\EshopEnterprise\Tests\Unit\Model;

use \oxField;

class NewsTest extends \oxUnitTestCase
{
    private $_oNews = null;

    /**
     * Initialize the fixture.
     *
     * @return null
     */
    protected function setUp()
    {
        parent::setUp();
        $oBaseNews = oxNew('oxBase');
        $oBaseNews->init('oxnews');
        $oBaseNews->oxnews__oxshortdesc = new oxField('Test', oxField::T_RAW);
        $oBaseNews->oxnews__oxshortdesc_1 = new oxField('Test_news_1', oxField::T_RAW);
        $oBaseNews->Save();

        $this->_oNews = oxNew('oxnews');
        $this->_oNews->load($oBaseNews->getId());
    }

    // 3. trying to delete denied action by RR (EE only)
    public function testAssignDeniedByRR()
    {
        $oTestNews = $this->getMock('oxNews', array('canRead'));
        $oTestNews->expects($this->once())->method('canRead')->will($this->returnValue(false));

        $oTestNews->load($this->_oNews->getId());
        $this->assertFalse($oTestNews->delete());
    }
}
