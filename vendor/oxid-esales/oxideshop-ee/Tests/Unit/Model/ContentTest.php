<?php
/**
 * This Software is the property of OXID eSales and is protected
 * by copyright law - it is NOT Freeware.
 *
 * Any unauthorized use of this software without a valid license key
 * is a violation of the license agreement and will be prosecuted by
 * civil and criminal law.
 *
 * @link      http://www.oxid-esales.com
 * @copyright (C) OXID eSales AG 2003-2016
 * @version   OXID eShop EE
 */

namespace OxidEsales\EshopEnterprise\Tests\Unit\Model;

use \OxidEsales\TestingLibrary\UnitTestCase;

class ContentTest extends UnitTestCase
{
    public function testAssignDeniedByRR()
    {
        $oContent = $this->getMock('oxcontent', array('canRead'));
        $oContent->expects($this->once())->method('canRead')->will($this->returnValue(false));

        $this->assertFalse($oContent->assign(array()));
    }

    // for second shop (buglist_327)
    public function testLoadByIdentSecondShop()
    {
        $this->getConfig()->setShopId(2);
        $oObj = oxNew('oxContent');
        $this->assertFalse($oObj->loadByIdent('_testLoadId'));
    }
}
