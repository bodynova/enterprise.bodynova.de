<?php
/**
 * This Software is the property of OXID eSales and is protected
 * by copyright law - it is NOT Freeware.
 *
 * Any unauthorized use of this software without a valid license key
 * is a violation of the license agreement and will be prosecuted by
 * civil and criminal law.
 *
 * @link      http://www.oxid-esales.com
 * @copyright (C) OXID eSales AG 2003-2016
 * @version   OXID eShop EE
 */

namespace OxidEsales\EshopEnterprise\Tests\Unit\Model;

use oxField;
use oxDb;


class RoleTest extends \OxidEsales\TestingLibrary\UnitTestCase
{

    /**
     * Initialize the fixture.
     *
     * @return null
     */
    protected function setUp()
    {
        parent::setUp();
        $this->oRole = oxNew('oxRole');
        $this->oRole->oxroles__oxtitle = new oxfield('test');
        $this->oRole->oxroles__oxshopid = new oxfield($this->getConfig()->getBaseShopId());
        $this->oRole->oxroles__oxactive = new oxfield(1);
        $this->oRole->oxroles__oxarea = new oxfield(1);
        $this->oRole->save();

        $sQ = "insert into oxfield2role ( oxfieldid, oxtype, oxroleid, oxidx) values
           ( 'test', 'test', '" . $this->oRole->getId() . "', 1 )";
        oxDb::getDb()->execute($sQ);

        $oO2R = oxNew('oxBase');
        $oO2R->init('oxobject2role');
        $oO2R->oxobject2role__oxobjectid = new oxfield('test');
        $oO2R->oxobject2role__oxroleid = new oxfield($this->oRole->getId());
        $oO2R->oxobject2role__oxtype = new oxfield('test');
        $oO2R->save();

        $oOR = oxNew('oxBase');
        $oOR->init('oxobjectrights');
        $oOR->oxobjectrights__oxobjectid = new oxfield($this->oRole->getId());
        $oOR->oxobjectrights__oxgroupidx = new oxfield(1);
        $oOR->oxobjectrights__oxoffset = new oxfield(0);
        $oOR->oxobjectrights__oxaction = new oxfield(1);
        $oOR->save();
    }

    /**
     * Tear down the fixture.
     *
     * @return null
     */
    protected function tearDown()
    {
        $this->oRole->delete();
        parent::tearDown();
    }

    public function testLoad()
    {
        $oRole = oxNew('oxRole');
        if (!$oRole->load($this->oRole->getId())) {
            $this->fail('can not load roles');
        }
        $this->assertEquals(1, $oRole->oxroles__oxshopid->value);
    }

    public function testFailedLoad()
    {
        $oRole = oxNew('oxRole');
        $this->assertFalse($oRole->load('wrong'));
    }

    public function testDeleteNoId()
    {
        $oRole = oxNew('oxRole');
        $this->assertFalse($oRole->delete());
    }

    public function testDelete()
    {
        $oDb = oxDb::getDb();

        $oRole = oxNew('oxRole');

        $this->assertTrue('1' == $oDb->getOne("select 1 from oxfield2role where oxroleid = '" . $this->oRole->getId() . "' "));
        $this->assertTrue('1' == $oDb->getOne("select 1 from oxobject2role where oxroleid = '" . $this->oRole->getId() . "' "));
        $this->assertTrue('1' == $oDb->getOne("select 1 from oxobjectrights where oxobjectid = '" . $this->oRole->getId() . "' "));

        $this->assertTrue($oRole->delete($this->oRole->getId()));

        $this->assertFalse($oDb->getOne("select 1 from oxfield2role where oxroleid = '" . $this->oRole->getId() . "' "));
        $this->assertFalse($oDb->getOne("select 1 from oxobject2role where oxroleid = '" . $this->oRole->getId() . "' "));
        $this->assertFalse($oDb->getOne("select 1 from oxobjectrights where oxobjectid = '" . $this->oRole->getId() . "' "));
    }
}
