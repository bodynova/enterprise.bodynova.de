<?php
/**
 * This Software is the property of OXID eSales and is protected
 * by copyright law - it is NOT Freeware.
 *
 * Any unauthorized use of this software without a valid license key
 * is a violation of the license agreement and will be prosecuted by
 * civil and criminal law.
 *
 * @link      http://www.oxid-esales.com
 * @copyright (C) OXID eSales AG 2003-2016
 * @version   OXID eShop EE
 */

namespace OxidEsales\EshopEnterprise\Tests\Unit\Model;

use \oxField;

require_once TEST_LIBRARY_HELPERS_PATH . 'oxCacheHelper.php';

class ReviewTest extends \oxUnitTestCase
{
    public function testResetCacheAdminMode()
    {
        $oReview = $this->getMock('oxreview', array('_resetCache'));
        $oReview->expects($this->once())->method('_resetCache')->with($this->equalTo('xxx'));
        $oReview->setAdminMode(true);
        $oReview->UNITresetCache('xxx');
    }

    public function testResetCacheNonAdminMode()
    {
        oxAddClassModule('oxCacheHelper', 'oxCache');

        $oReview = oxNew('oxReview');
        $oReview->setAdminMode(false);
        $oReview->oxreviews__oxtype = new oxField('oxarticle', oxField::T_RAW);
        $oReview->oxreviews__oxobjectid = new oxField('xxx', oxField::T_RAW);
        try {
            $oReview->UNITresetCache();
        } catch (\Exception $oE) {
            $this->assertEquals('a:1:{s:3:"xxx";s:4:"anid";}', $oE->getMessage());

            return;
        }
        $this->fail('error exec. testResetCacheNonAdminMode');
    }

    public function testGetObjectIdAndType()
    {
        // inserting few test records
        $oRev = oxNew('oxReview');
        $oRev->setId('id1');
        $oRev->oxreviews__oxactive = new oxField(1);
        $oRev->oxreviews__oxobjectid = new oxField('xx1');
        $oRev->oxreviews__oxtype = new oxField('oxarticle');
        $oRev->oxreviews__oxtext = new oxField('revtext');
        $oRev->save();

        $oRev = oxNew('oxReview');
        $oRev->setId('id2');
        $oRev->oxreviews__oxactive = new oxField(1);
        $oRev->oxreviews__oxobjectid = new oxField('xx2');
        $oRev->oxreviews__oxtype = new oxField('oxrecommlist');
        $oRev->oxreviews__oxtext = new oxField('revtext');
        $oRev->save();

        $oRev = oxNew('oxReview');
        $oRev->load('id1');
        $this->assertEquals('anid', $oRev->UNITgetObjectKey());

        $oRev = oxNew('oxReview');
        $oRev->load('id2');

        $this->assertEquals('recommid', $oRev->UNITgetObjectKey());
    }
}
