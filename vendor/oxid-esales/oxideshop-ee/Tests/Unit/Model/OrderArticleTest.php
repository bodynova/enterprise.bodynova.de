<?php
/**
 * This Software is the property of OXID eSales and is protected
 * by copyright law - it is NOT Freeware.
 *
 * Any unauthorized use of this software without a valid license key
 * is a violation of the license agreement and will be prosecuted by
 * civil and criminal law.
 *
 * @link      http://www.oxid-esales.com
 * @copyright (C) OXID eSales AG 2003-2016
 * @version   OXID eShop EE
 */

namespace OxidEsales\EshopEnterprise\Tests\Unit\Model;

use \OxidEsales\TestingLibrary\UnitTestCase;
use \oxField;
use \oxDb;

class OrderArticleTest extends UnitTestCase
{
    protected $orderArticle = null;

    /**
     * Initialize the fixture.
     */
    protected function setup()
    {
        parent::setUp();

        $this->orderArticle = oxNew('oxorderarticle');
        $this->orderArticle->setId('_testOrderArticleId');
        $this->orderArticle->oxorderarticles__oxartid = new oxField('_testArticleId', oxField::T_RAW);
        $this->orderArticle->oxorderarticles__oxorderid = new oxField('51', oxField::T_RAW);
        $this->orderArticle->save();
    }

    public function testCheckForVpe()
    {
        $oOrderArticle = oxNew('oxOrderArticle');
        $this->assertEquals(999, $oOrderArticle->checkForVpe(999));
    }

    /**
     * Testing ERP status getter
     */
    public function testGetStatus()
    {
        $oOrderArticle = oxNew('oxorderarticle');
        $oOrderArticle->load('_testOrderArticleId');
        $oOrderArticle = $this->getProxyClass('oxorderarticle');
        $this->assertNull($oOrderArticle->getStatus());
        $this->assertNull($oOrderArticle->getNonPublicVar('_aStatuses'));

        $aParams = array('somekey' => 'somevalue');
        $oOrderArticle->oxorderarticles__oxerpstatus = new oxField(serialize($aParams), oxField::T_RAW);
        $this->assertEquals($aParams, $oOrderArticle->getStatus());
        $this->assertEquals($aParams, $oOrderArticle->getNonPublicVar('_aStatuses'));
    }

    /*
     * Test correct serializing and loading oxerpstatus values
     */
    public function testSerializingValues()
    {
        $aTestArr = array("te\"st", "test2");
        $sParams = serialize($aTestArr);

        $this->orderArticle->oxorderarticles__oxerpstatus = new oxField($sParams, oxField::T_RAW);
        $this->orderArticle->save();

        $oOrderArticle = oxNew('oxorderarticle');
        $oOrderArticle->load('_testOrderArticleId');

        $this->assertEquals($aTestArr, $oOrderArticle->getStatus());
    }

    /*
     * Test _setFieldData - correctly sets data type to T_RAW to oxpersparam and oxerpstatus fields
     * M #275
     */
    public function test_setFieldData()
    {
        $this->orderArticle->oxorderarticles__oxerpstatus = new oxField('" &', oxField::T_RAW);
        $this->orderArticle->save();

        $sSQL = "select * from oxorderarticles where oxid = '_testOrderArticleId' ";
        $rs = oxDb::getDb(oxDB::FETCH_MODE_ASSOC)->select($sSQL);

        $oOrderArticle = oxNew('oxorderarticle');
        $oOrderArticle->assign($rs->fields); // field names are in upercase

        $this->assertEquals('" &', $oOrderArticle->oxorderarticles__oxerpstatus->rawValue);
    }
}
