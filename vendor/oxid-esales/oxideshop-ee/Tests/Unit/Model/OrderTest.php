<?php
/**
 * This Software is the property of OXID eSales and is protected
 * by copyright law - it is NOT Freeware.
 *
 * Any unauthorized use of this software without a valid license key
 * is a violation of the license agreement and will be prosecuted by
 * civil and criminal law.
 *
 * @link      http://www.oxid-esales.com
 * @copyright (C) OXID eSales AG 2003-2016
 * @version   OXID eShop EE
 */

namespace OxidEsales\EshopEnterprise\Tests\Unit\Model;

use \OxidEsales\TestingLibrary\UnitTestCase;
use \oxField;

class OrderTest extends UnitTestCase
{
    /**
     * Trying to delete denied action by RR
     */
    public function testDeleteDeniedByRR()
    {

        $oOrder = $this->getMock('oxOrder', array('canDelete', 'load'));
        $oOrder->expects($this->once())->method('load')->will($this->returnValue(true));
        $oOrder->expects($this->once())->method('canDelete')->will($this->returnValue(false));

        $this->assertFalse($oOrder->delete('_testOrderId'));
    }

    /**
     * Trying to assign denied action by RR
     */
    public function testAssignDeniedByRR()
    {
        $oOrder = $this->getMock('oxorder', array('canRead'));
        $oOrder->expects($this->once())->method('canRead')->will($this->returnValue(false));

        $oOrder->load("_testOrderId");
        $this->assertFalse($oOrder->assign(array()));
    }

    public function testSetUser()
    {
        //load user
        $oUser = oxNew('oxuser');
        $oUser->load("oxdefaultadmin");
        $oUser->oxuser__oxustidstatus = new oxField('5');

        $oOrder = $this->getProxyClass("oxOrder");
        $oOrder->UNITsetUser($oUser);

        $this->assertEquals(5, $oOrder->oxorder__oxbillustidstatus->value);
    }

}
