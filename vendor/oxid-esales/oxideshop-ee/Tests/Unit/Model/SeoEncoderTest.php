<?php
/**
 * This Software is the property of OXID eSales and is protected
 * by copyright law - it is NOT Freeware.
 *
 * Any unauthorized use of this software without a valid license key
 * is a violation of the license agreement and will be prosecuted by
 * civil and criminal law.
 *
 * @link      http://www.oxid-esales.com
 * @copyright (C) OXID eSales AG 2003-2016
 * @version   OXID eShop EE
 */

namespace OxidEsales\EshopEnterprise\Tests\Unit\Model;

use oxDb;
use OxidEsales\TestingLibrary\UnitTestCase;

class SeoEncoderTest extends UnitTestCase
{
    public function testCopyStaticUrlsForOtherThanBaseShop()
    {
        oxDb::getDb()->execute('delete from oxseo where oxshopid != "1" ');

        $oEncoder = oxNew('oxSeoEncoder');
        $oEncoder->copyStaticUrls('2');

        $seoId = oxDb::getDb()->getOne('select oxident from oxseo where oxshopid = "2" and oxlang="1" and oxstdurl="index.php?cl=account_wishlist"');

        // checking if new records are copied
        $this->assertEquals('023abc17c853f9bccc201c5afd549a92', $seoId);
    }
}
