<?php
/**
 * This Software is the property of OXID eSales and is protected
 * by copyright law - it is NOT Freeware.
 *
 * Any unauthorized use of this software without a valid license key
 * is a violation of the license agreement and will be prosecuted by
 * civil and criminal law.
 *
 * @link      http://www.oxid-esales.com
 * @copyright (C) OXID eSales AG 2003-2016
 * @version   OXID eShop EE
 */

namespace OxidEsales\EshopEnterprise\Tests\Unit\Model;

class VoucherSerieTest extends \OxidEsales\TestingLibrary\UnitTestCase
{
    /**
     * Trying to delete denied action by RR
     */
    public function testDeleteDeniedByRR()
    {
        $oSerie = $this->getMock('oxvoucherserie', array('canDelete', 'unsetUserGroups', 'deleteVoucherList'));
        $oSerie->expects($this->once())->method('canDelete')->will($this->returnValue(false));
        $oSerie->expects($this->never())->method('unsetUserGroups');
        $oSerie->expects($this->never())->method('deleteVoucherList');

        $this->assertFalse($oSerie->delete('_testOrderId'));
    }
}
