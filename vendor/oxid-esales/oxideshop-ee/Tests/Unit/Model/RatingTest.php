<?php
/**
 * This Software is the property of OXID eSales and is protected
 * by copyright law - it is NOT Freeware.
 *
 * Any unauthorized use of this software without a valid license key
 * is a violation of the license agreement and will be prosecuted by
 * civil and criminal law.
 *
 * @link      http://www.oxid-esales.com
 * @copyright (C) OXID eSales AG 2003-2016
 * @version   OXID eShop EE
 */

namespace OxidEsales\EshopEnterprise\Tests\Unit\Model;

use \oxField;

class RatingTest extends \oxUnitTestCase
{
    public function testGetObjectIdAndType()
    {
        // inserting few test records
        $rating = oxNew('oxRating');
        $rating->setId('id1');
        $rating->oxratings__oxobjectid = new oxField('xx1');
        $rating->oxratings__oxtype = new oxField('oxarticle');
        $rating->oxratings__oxrating = new oxField(1);
        $rating->save();

        $rating = oxNew('oxRating');
        $rating->setId('id2');
        $rating->oxratings__oxobjectid = new oxField('xx2');
        $rating->oxratings__oxtype = new oxField('oxrecommlist');
        $rating->oxratings__oxrating = new oxField(2);
        $rating->save();

        $rating = oxNew('oxRating');
        $rating->load('id1');
        $this->assertEquals('id1', $rating->getId());
        $this->assertEquals('xx1', $rating->getObjectId());
        $this->assertEquals('oxarticle', $rating->getObjectType());
        $this->assertEquals('anid', $rating->UNITgetObjectKey());

        $rating = oxNew('oxRating');
        $rating->load('id2');
        $this->assertEquals('id2', $rating->getId());
        $this->assertEquals('xx2', $rating->getObjectId());
        $this->assertEquals('oxrecommlist', $rating->getObjectType());
        $this->assertEquals('recommid', $rating->UNITgetObjectKey());
    }
}
