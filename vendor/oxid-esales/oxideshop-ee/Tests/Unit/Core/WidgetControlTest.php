<?php
/**
 * This Software is the property of OXID eSales and is protected
 * by copyright law - it is NOT Freeware.
 *
 * Any unauthorized use of this software without a valid license key
 * is a violation of the license agreement and will be prosecuted by
 * civil and criminal law.
 *
 * @link      http://www.oxid-esales.com
 * @copyright (C) OXID eSales AG 2003-2016
 * @version   OXID eShop EE
 */

namespace OxidEsales\EshopEnterprise\Tests\Unit\Core;

use \oxRegistry;

class WidgetControlTest extends \oxUnitTestCase
{
    /**
     * Testing oxShopControl::_runOnce()
     */
    public function testRunOnce()
    {
        // if _runOnce() will be executed, this param will be set to true
        $this->setSessionParam("blRunOnceExecuted", false);

        $oUtilsStub = $this->getMock('oxUtils', array('showMessageAndExit'));
        oxRegistry::set('oxUtils', $oUtilsStub);

        $oReverseProxyBackend = $this->getMock("stdClass", array("isActive"));
        $oReverseProxyBackend->expects($this->any())->method('isActive')->will($this->returnValue(true));

        OxRegistry::set("oxReverseProxyBackend", $oReverseProxyBackend);

        $oConfig = $this->getMock("oxConfig", array("hasActiveViewsChain", "isProductiveMode"));
        $oConfig->expects($this->any())->method('hasActiveViewsChain')->will($this->returnValue(false));
        $oConfig->expects($this->any())->method('isProductiveMode')->will($this->returnValue(true));

        //, array("getConfig")
        $oControl = $this->getMock('oxWidgetControl', array("getConfig"));
        $oControl->expects($this->any())->method('getConfig')->will($this->returnValue($oConfig));

        $oControl->blSkipForTest = true;
        $oControl->UNITrunOnce();

        $this->assertEquals(true, $this->getSessionParam("blRunOnceExecuted"));
    }

    /**
     * Testing oxShopControl::_runOnce()
     *
     * @return null
     */
    public function testRunOnce_reverseProxyIsNotActive()
    {
        // if _runOnce() will be executed, this param will be set to true
        $this->setSessionParam("blRunOnceExecuted", false);

        $oReverseProxyBackend = $this->getMock("stdClass", array("isActive"));
        $oReverseProxyBackend->expects($this->any())->method('isActive')->will($this->returnValue(false));

        OxRegistry::set("oxReverseProxyBackend", $oReverseProxyBackend);

        $oConfig = $this->getMock("oxConfig", array("hasActiveViewsChain"));
        $oConfig->expects($this->any())->method('hasActiveViewsChain')->will($this->returnValue(false));

        $oControl = $this->getMock('oxWidgetControl', array("getConfig"));
        $oControl->expects($this->any())->method('getConfig')->will($this->returnValue($oConfig));

        $oControl->blSkipForTest = true;
        $oControl->UNITrunOnce();

        $this->assertEquals(false, $this->getSessionParam("blRunOnceExecuted"));
    }

    /**
     * Testing oxShopControl::_runOnce()
     *
     * @return null
     */
    public function testRunOnce_hasActiveViewsChain()
    {
        // if _runOnce() will be executed, this param will be set to true
        $this->setSessionParam("blRunOnceExecuted", false);

        $oReverseProxyBackend = $this->getMock("stdClass", array("isActive"));
        $oReverseProxyBackend->expects($this->any())->method('isActive')->will($this->returnValue(true));

        OxRegistry::set("oxReverseProxyBackend", $oReverseProxyBackend);

        $oConfig = $this->getMock("oxConfig", array("hasActiveViewsChain"));
        $oConfig->expects($this->any())->method('hasActiveViewsChain')->will($this->returnValue(true));

        $oControl = $this->getMock('oxWidgetControl', array("getConfig"));
        $oControl->expects($this->any())->method('getConfig')->will($this->returnValue($oConfig));

        $oControl->blSkipForTest = true;
        $oControl->UNITrunOnce();

        $this->assertEquals(false, $this->getSessionParam("blRunOnceExecuted"));
    }

}