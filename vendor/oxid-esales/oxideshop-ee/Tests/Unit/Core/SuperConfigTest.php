<?php
/**
 * This Software is the property of OXID eSales and is protected
 * by copyright law - it is NOT Freeware.
 *
 * Any unauthorized use of this software without a valid license key
 * is a violation of the license agreement and will be prosecuted by
 * civil and criminal law.
 *
 * @link      http://www.oxid-esales.com
 * @copyright (C) OXID eSales AG 2003-2016
 * @version   OXID eShop EE
 */

namespace OxidEsales\EshopEnterprise\Tests\Unit\Core;

use oxDb;
use OxidEsales\EshopEnterprise\Core\AdminRights;
use OxidEsales\TestingLibrary\UnitTestCase;

class SuperConfigTest extends UnitTestCase
{
    protected function tearDown()
    {
        $this->cleanUpTable('oxobjectrights');

        return parent::tearDown();
    }

    public function testGetRightsRRisOff()
    {
        $this->getConfig()->setConfigParam('blUseRightsRoles', 0);

        $oCfg = oxNew('oxSuperCfg');
        $this->assertNull($oCfg->getRights());
    }

    public function testGetRightsAdminRR()
    {
        $this->getConfig()->setConfigParam('blUseRightsRoles', 1);

        $oCfg = oxNew('oxSuperCfg');
        $oCfg->setAdminMode(true);

        $this->assertTrue($oCfg->getRights() instanceof AdminRights);
    }

    public function testGetRightsShopRR()
    {
        $this->getConfig()->setConfigParam('blUseRightsRoles', 2);

        // empty demo data..
        $oCfg = oxNew('oxSuperCfg');
        $oCfg->setAdminMode(false);
        $this->assertFalse($oCfg->getRights());

        // inserting test record..
        $sQ = "insert into oxobjectrights (oxid,oxobjectid,oxgroupidx,oxoffset,oxaction) values ('_testId','_testObjectId','0','0','0')";
        oxDb::getDb()->execute($sQ);

        // testing
        $oCfg = oxNew('oxSuperCfg');
        $oCfg->setAdminMode(false);
        $rightsClass = get_class(oxNew('oxRights'));
        $this->assertTrue($oCfg->getRights() instanceof $rightsClass);
    }

    public function testSetRightsAndGetRights()
    {
        $oCfg = oxNew('oxSuperCfg');
        $oCfg->setRights('xxx');
        $this->assertEquals('xxx', $oCfg->getRights());
    }
}
