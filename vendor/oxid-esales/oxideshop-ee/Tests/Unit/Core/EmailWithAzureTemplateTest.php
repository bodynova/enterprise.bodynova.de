<?php
/**
 * This Software is the property of OXID eSales and is protected
 * by copyright law - it is NOT Freeware.
 *
 * Any unauthorized use of this software without a valid license key
 * is a violation of the license agreement and will be prosecuted by
 * civil and criminal law.
 *
 * @link      http://www.oxid-esales.com
 * @copyright (C) OXID eSales AG 2003-2016
 * @version   OXID eShop EE
 */

namespace OxidEsales\EshopEnterprise\Tests\Unit\Core;

use oxDb;
use oxEmail;
use oxField;
use OxidEsales\TestingLibrary\UnitTestCase;
use oxRegistry;
use oxShop;
use PHPUnit_Framework_MockObject_MockObject;

class EmailWithAzureTemplateTest extends UnitTestCase
{
    /**
     * Initialize the fixture.
     */
    protected function setUp()
    {
        parent::setUp();

        // reload smarty
        oxRegistry::get("oxUtilsView")->getSmarty(true);
    }

    /**
     * Tear down the fixture.
     */
    protected function tearDown()
    {
        // reload smarty
        oxRegistry::get("oxUtilsView")->getSmarty(true);

        $oActShop = $this->getConfig()->getActiveShop();
        $oActShop->setLanguage(0);
        oxRegistry::getLang()->setBaseLanguage(0);

        parent::tearDown();
    }

    /**
     * Test sending forgot password to not existing user
     */
    public function testSendForgotPwdEmailToUserInAnotherShopMallUsers()
    {
        $this->getConfig()->setConfigParam('blMallUsers', 1);
        oxDb::getDb()->execute(
            "INSERT INTO  `oxuser` (
                    `OXID`, `OXACTIVE`, `OXRIGHTS`, `OXSHOPID`, `OXUSERNAME`,
                    `OXPASSWORD`, `OXPASSSALT`, `OXCUSTNR`, `OXUSTID`, `OXUSTIDSTATUS`,
                    `OXCOMPANY`, `OXFNAME`, `OXLNAME`, `OXSTREET`, `OXSTREETNR`, `OXADDINFO`,
                    `OXCITY`, `OXCOUNTRYID`, `OXZIP`, `OXFON`, `OXFAX`, `OXSAL`,
                    `OXBONI`, `OXCREATE`, `OXREGISTER`, `OXPRIVFON`, `OXMOBFON`, `OXBIRTHDATE`,
                    `OXURL`, `OXLDAPKEY`, `OXWRONGLOGINS`, `OXUPDATEKEY`, `OXUPDATEEXP`
                )
                VALUES (
                    '_test_another', '1',  'user', '5', 'test@testforsubs.com',
                    'zzz', '', '0', '', '0',
                     '', 'testUserFName', 'testUserLName', '', '', '',
                     '', '', '', '', '', '',
                     '0', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', '', '0000-00-00',
                     '', '', '0', '', '0'
                );
            "
        );

        /** @var oxEmail|PHPUnit_Framework_MockObject_MockObject $email */
        $email = $this->getMock('oxEmail', array("_sendMail", "_getShop", "_getUseInlineImages"));
        $email->expects($this->once())->method('_sendMail')->will($this->returnValue(true));
        $email->expects($this->any())->method('_getShop')->will($this->returnValue($this->getShop()));
        $email->expects($this->any())->method('_getUseInlineImages')->will($this->returnValue(true));

        $result = $email->sendForgotPwdEmail('test@testforsubs.com');
        $this->assertTrue($result, 'Forgot password email was not sent');

        // check mail fields
        $fields['sRecipient'] = 'test@testforsubs.com';
        $fields['sRecipientName'] = 'testUserFName testUserLName';
        $fields['sSubject'] = 'testUserFogotPwdSubject';
        $fields['sFrom'] = 'orderemail@orderemail.nl';
        $fields['sFromName'] = 'testShopName';
        $fields['sReplyTo'] = 'orderemail@orderemail.nl';
        $fields['sReplyToName'] = 'testShopName';

        $this->checkMailFields($fields, $email);
        $this->checkMailBody('testSendForgotPwdEmail', $email->getBody());
    }

    /**
     * @param array   $fields
     * @param oxEmail $email
     */
    protected function checkMailFields($fields, $email)
    {
        if ($fields['sRecipient']) {
            $recipient = $email->getRecipient();
            $this->assertEquals($fields['sRecipient'], $recipient[0][0], 'Incorrect mail recipient');
        }

        if ($fields['sRecipientName']) {
            $recipient = $email->getRecipient();
            $this->assertEquals($fields['sRecipientName'], $recipient[0][1], 'Incorrect mail recipient name');
        }

        if ($fields['sSubject']) {
            $this->assertEquals($fields['sSubject'], $email->getSubject(), 'Incorrect mail subject');
        }

        if ($fields['sFrom']) {
            $from = $email->getFrom();
            $this->assertEquals($fields['sFrom'], $from, 'Incorrect mail from address');
        }

        if ($fields['sFromName']) {
            $fromName = $email->getFromName();
            $this->assertEquals($fields['sFromName'], $fromName, 'Incorrect mail from name');
        }

        if ($fields['sReplyTo']) {
            $replyTo = $email->getReplyTo();
            $this->assertEquals($fields['sReplyTo'], $replyTo[0][0], 'Incorrect mail reply to address');
        }

        if ($fields['sReplyToName']) {
            $replyTo = $email->getReplyTo();
            $this->assertEquals($fields['sReplyToName'], $replyTo[0][1], 'Incorrect mail reply to name');
        }

        if ($fields['sBody']) {
            $this->assertEquals($fields['sBody'], $email->getBody(), 'Incorrect mail body');
        }
    }

    /**
     * @param string $function
     * @param string $body
     * @param bool   $writeToTestFile
     */
    protected function checkMailBody($function, $body, $writeToTestFile = false)
    {
        // uncomment line to generate template for checking mail body
        // file_put_contents(__DIR__ ."/../testData/email_templates/azure/$function.html", $body);

        $utfMode = $this->getConfig()->isUtf() ? '_utf8' : '';

        $path = __DIR__ .'/../TestData/email_templates/azure/' . $function . $utfMode . '.html';
        if (!($expectedBody = file_get_contents($path))) {
            $this->fail("Template '$path' was not found!");
        }

        //remove <img src="cid:1192193298470f6d12383b8" ... from body, because it is everytime different
        $expectedBody = preg_replace("/cid:[0-9a-zA-Z]+\"/", "cid:\"", $expectedBody);

        //replacing test shop id to good one
        $expectedBody = preg_replace("/shp\=testShopId/", "shp=1", $expectedBody);

        $body = preg_replace("/cid:[0-9a-zA-Z]+\"/", "cid:\"", $body);

        // A. very special case for user password reminder
        if ($function == 'testSendForgotPwdEmail') {
            $expectedBody = preg_replace("/uid=[0-9a-zA-Z]+\&amp;/", "", $expectedBody);
            $body = preg_replace("/uid=[0-9a-zA-Z]+\&amp;/", "", $body);
        }

        $expectedBody = preg_replace("/\s+/", " ", $expectedBody);
        $body = preg_replace("/\s+/", " ", $body);

        $expectedBody = str_replace("> <", "><", $expectedBody);
        $body = str_replace("> <", "><", $body);

        $expectedShopUrl = "http://eshop/";
        $shopUrl = $this->getConfig()->getConfigParam('sShopURL');

        //remove shop url base path from links
        $body = str_replace($shopUrl, $expectedShopUrl, $body);

        if ($writeToTestFile) {
            file_put_contents(__DIR__ .'/../testData/email_templates/azure/' . $function . '_test_expecting.html', $expectedBody);
            file_put_contents(__DIR__ .'/../testData/email_templates/azure/' . $function . '_test_result.html', $body);
        }

        $this->assertEquals(strtolower(trim($expectedBody)), strtolower(trim($body)), "Incorect mail body");
    }

    /**
     * @return oxShop
     */
    protected function getShop()
    {
        $shop = oxNew("oxShop");
        $shop->load($this->getConfig()->getShopId());
        $shop->oxshops__oxorderemail = new oxField('orderemail@orderemail.nl', oxField::T_RAW);
        $shop->oxshops__oxordersubject = new oxField('testOrderSubject', oxField::T_RAW);
        $shop->oxshops__oxsendednowsubject = new oxField('testSendedNowSubject', oxField::T_RAW);
        $shop->oxshops__oxname = new oxField('testShopName', oxField::T_RAW);
        $shop->oxshops__oxowneremail = new oxField('shopOwner@shopOwnerEmail.nl', oxField::T_RAW);
        $shop->oxshops__oxinfoemail = new oxField('shopInfoEmail@shopOwnerEmail.nl', oxField::T_RAW);
        $shop->oxshops__oxsmtp = new oxField('127.0.0.1', oxField::T_RAW);
        $shop->oxshops__oxsmtpuser = new oxField('testSmtpUser', oxField::T_RAW);
        $shop->oxshops__oxsmtppwd = new oxField('testSmtpPassword', oxField::T_RAW);
        $shop->oxshops__oxregistersubject = new oxField('testUserRegistrationSubject', oxField::T_RAW);
        $shop->oxshops__oxforgotpwdsubject = new oxField('testUserFogotPwdSubject', oxField::T_RAW);

        return $shop;
    }
}
