<?php
/**
 * This Software is the property of OXID eSales and is protected
 * by copyright law - it is NOT Freeware.
 *
 * Any unauthorized use of this software without a valid license key
 * is a violation of the license agreement and will be prosecuted by
 * civil and criminal law.
 *
 * @link      http://www.oxid-esales.com
 * @copyright (C) OXID eSales AG 2003-2016
 * @version   OXID eShop EE
 */

namespace OxidEsales\EshopEnterprise\Tests\Unit\Core;

use OxidEsales\TestingLibrary\UnitTestCase;
use oxField;

class UtilsObjectTest extends UnitTestCase
{
    /**
     * Tear down the fixture.
     *
     * @return null
     */
    public function tearDown()
    {
        $oShop = oxNew('oxShop');
        $oShop->delete(2);

        parent::tearDown();
    }

    public function testIsDerivedFromParentShopIsDerived()
    {
        // first insert a new shop
        $oShop = oxNew('oxShop');
        $oShop->setId(2);
        $oShop->oxshops__oxparentid = new oxField(1, oxField::T_RAW);
        $oShop->save();

        $oArticle = oxNew('oxArticle');
        $oArticle->setId('testArticle');
        $oArticle->oxarticles__oxshopid = new oxField(1, oxField::T_RAW);
        $oArticle->save();

        /** @var oxShopIdCalculator|PHPUnit_Framework_MockObject_MockObject $shopIdCalculator */
        $shopIdCalculator = $this->getMock('oxShopIdCalculator', array('getShopId'), array(), '', false);
        $shopIdCalculator->expects($this->once())->method('getShopId')->will($this->returnValue(2));

        $oUtilsObject = oxNew('oxUtilsObject', null, null, $shopIdCalculator);

        $this->assertTrue($oUtilsObject->isDerivedFromParentShop('testArticle', "oxarticles"));
    }

    public function testIsDerivedFromParentShopIsNotDerived()
    {
        // first insert a new shop
        $oShop = oxNew('oxShop');
        $oShop->setId(2);
        $oShop->oxshops__oxparentid = new oxField(1, oxField::T_RAW);
        $oShop->save();

        $oArticle = oxNew('oxArticle');
        $oArticle->setId('testArticle');
        $oArticle->oxarticles__oxshopid = new oxField(1, oxField::T_RAW);
        $oArticle->save();

        $oConfig = $this->getMock('oxconfig', array('getShopId'));
        $oConfig->expects($this->any())->method('getShopId')->will($this->returnValue(3));

        $oUtilsObject = $this->getMock('oxUtilsObject', array('getConfig'));
        $oUtilsObject->expects($this->any())->method('getConfig')->will($this->returnValue($oConfig));

        $this->assertFalse($oUtilsObject->isDerivedFromParentShop('testArticle', "oxarticles"));
    }

    public function testIsDerivedFromParentShopIsNotDerivedSameShop()
    {
        $oArticle = oxNew('oxArticle');
        $oArticle->setId('testArticle');
        $oArticle->oxarticles__oxshopid = new oxField(1, oxField::T_RAW);
        $oArticle->save();

        $oConfig = $this->getMock('oxconfig', array('getShopId'));
        $oConfig->expects($this->any())->method('getShopId')->will($this->returnValue(1));

        $oUtilsObject = $this->getMock('oxUtilsObject', array('getConfig'));
        $oUtilsObject->expects($this->any())->method('getConfig')->will($this->returnValue($oConfig));

        $this->assertFalse($oUtilsObject->isDerivedFromParentShop('testArticle', "oxarticles"));
    }
}
