<?php

namespace OxidEsales\EshopEnterprise\Tests\Unit\Core;

use OxidEsales\EshopProfessional\Core\Serial;
use \oxRegistry;
use \oxDb;
use \oxField;

/**
 * This Software is the property of OXID eSales and is protected
 * by copyright law - it is NOT Freeware.
 *
 * Any unauthorized use of this software without a valid license key
 * is a violation of the license agreement and will be prosecuted by
 * civil and criminal law.
 *
 * @link      http://www.oxid-esales.com
 * @copyright (C) OXID eSales AG 2003-2016
 * @version   OXID eShop EE
 */
class _oxSerial extends Serial
{

    public function charShift($cIn, $sShift)
    {
        return parent::_charShift($cIn, $sShift);
    }

    public function hashName($sName = "")
    {
        return parent::_hashName($sName);
    }

    public function getChecksum($sIn)
    {
        return parent::_getChecksum($sIn);
    }

    public function mangleSerial($sUnmangledSerial = "")
    {
        return parent::_mangleSerial($sUnmangledSerial);
    }

    public function unmangleSerial($sMangledSerial = "")
    {
        return parent::_unmangleSerial($sMangledSerial);
    }

    public function getBlankSerial()
    {
        return parent::_getBlankSerial();
    }

    public function getSSerial()
    {
        return $this->sSerial;
    }

    public function getSName()
    {
        return $this->_sName;
    }

    public function UNITgetClassVar($sVarName)
    {
        return $this->$sVarName;
    }
}

/**
 * License key managing class.
 *
 * @package core
 */
class SerialTest extends \oxUnitTestCase
{
    private $_oSerial = null;

    public function setUp()
    {
        parent::setUp();

        $this->_oSerial = new _oxSerial();
        $myConfig = $this->getConfig();

        $this->aConfig = array();
        $this->aConfig['blShopStopped'] = oxDb::getDb(oxDb::FETCH_MODE_ASSOC)->getRow("select * from oxconfig where oxvarname='blShopStopped'");

        $myConfig->saveShopConfVar('bool', 'blShopStopped', 'false', $myConfig->getBaseShopId());
        $myConfig->saveShopConfVar('str', 'sBackTag', '', $myConfig->getBaseShopId());
        $myConfig->saveShopConfVar('bool', 'blExpirationEmailSent', 'false', $myConfig->getBaseShopId());
    }

    /**
     * Tear down the fixture.
     *
     * @return null
     */
    protected function tearDown()
    {
        $oDBRestore = self::_getDbRestore();
        $oDBRestore->restoreTable('oxconfig');

        parent::tearDown();
    }

    public function testIsValidSerial()
    {
        $this->assertFalse($this->_oSerial->isValidSerial('47YUR-RQX6N-DAMBH-LBUU6-RMA9P-QZKDN'));

        $this->assertTrue($this->_oSerial->isValidSerial('U7YUR-RQX6N-DAMBH-LBUU6-RMA9P-QZKDM'));
        $this->assertTrue($this->_oSerial->isValidSerial('VFTJR-CF9KF-BRLTQ-RMUJC-F5AN7-AUKGN'));
    }

    /**
     * Tests Beta key.
     */
    public function testBetaKey()
    {
        $sBetaKey = 'FLNBR-LTGNQ-SKN8E-CK759-M7MAM-B9PKG';

        $oSerial = oxNew('oxSerial');

        $this->assertFalse($oSerial->isUnlicensedSerial($sBetaKey));
        $this->assertEquals(4, $oSerial->detectVersion($sBetaKey));
    }

    public function testValidateShop_MandateCountIncorrect_ShopUnlicensed()
    {
        $oSerial = $this->_getSerial(false, true);
        $this->getConfig()->setConfigParam('IMS', 0);

        $this->assertFalse($oSerial->validateShop());
        $this->assertEquals('incorrect_mandate_amount', $oSerial->getValidationMessage());
    }

    private function _getSerial($isUnlicensed = false, $blGracePeriodExpired = false, $blBetaSerial = false, $blBetaShopVersion = false)
    {
        $oSerial = $this->getMock('oxSerial', array('isUnlicensedSerial', 'isGracePeriodExpired', 'isGracePeriodStarted', 'isBetaSerial', 'getEmail'));
        $oSerial->expects($this->any())->method('isUnlicensedSerial')->will($this->returnValue($isUnlicensed));
        $oSerial->expects($this->any())->method('isGracePeriodStarted')->will($this->returnValue(true));
        $oSerial->expects($this->any())->method('isGracePeriodExpired')->will($this->returnValue($blGracePeriodExpired));
        $oSerial->expects($this->any())->method('isBetaSerial')->will($this->returnValue($blBetaSerial));
        if ($blBetaShopVersion) {
            $this->getConfig()->getActiveShop()->oxshops__oxversion = new oxField('5.0.0_beta1');
        } else {
            $this->getConfig()->getActiveShop()->oxshops__oxversion = new oxField('5.0.0');
        }
        $oSerial->expects($this->any())->method('getEmail')->will($this->returnValue($this->_getMockedExpirationEmailBuilder()));

        return $oSerial;
    }

    /**
     * Function mocks oxEmail and oxExpirationEmailBuilder.
     *
     * @param string $sSendExpectsToBeCalled defines how much time should be called send function.
     *
     * @return oxExpirationEmailBuilder
     */
    private function _getMockedExpirationEmailBuilder($sSendExpectsToBeCalled = 'any')
    {
        $oEmail = $this->getMock('oxEmail', array('send'));
        $oEmail->expects($this->$sSendExpectsToBeCalled())->method('send');

        $oEmailBuilder = $this->getMock('oxExpirationEmailBuilder', array('build'));
        $oEmailBuilder->expects($this->any())->method('build')->will($this->returnValue($oEmail));

        return $oEmailBuilder;
    }
}
