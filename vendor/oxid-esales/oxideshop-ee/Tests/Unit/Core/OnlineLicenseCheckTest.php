<?php
/**
 * This Software is the property of OXID eSales and is protected
 * by copyright law - it is NOT Freeware.
 *
 * Any unauthorized use of this software without a valid license key
 * is a violation of the license agreement and will be prosecuted by
 * civil and criminal law.
 *
 * @link      http://www.oxid-esales.com
 * @copyright (C) OXID eSales AG 2003-2016
 * @version   OXID eShop EE
 */

namespace OxidEsales\EshopEnterprise\Tests\Unit\Core;

use OxidEsales\EshopEnterprise\Core\OnlineLicenseCheck;

class OnlineLicenseCheckTest extends \oxUnitTestCase
{
    public function testShopGoesToGracePeriodWhenShopSerialInvalid()
    {
        $config = $this->getConfig();
        $config->setConfigParam('blShopStopped', false);
        $config->setConfigParam('sShopVar', '');

        $response = oxNew('oxOnlineLicenseCheckResponse');
        $response->code = '1';
        $response->message = 'NACK';

        $caller = $this->getMock('oxOnlineLicenseCheckCaller', array('doRequest'), array(), '', false);
        $caller->expects($this->once())->method('doRequest')->will($this->returnValue($response));
        /** @var oxOnlineLicenseCheckCaller $caller */

        $licenseCheck = oxNew('oxOnlineLicenseCheck', $caller);
        $licenseCheck->validateShopSerials();

        $this->assertTrue($this->getConfig()->getConfigParam('blShopStopped'));
        $this->assertEquals('unlc', $this->getConfig()->getConfigParam('sShopVar'));
    }

    public function testShopDoesNotGoToGracePeriodWhenLicensingServerIsDown()
    {
        $config = $this->getConfig();
        $config->setConfigParam('blShopStopped', false);
        $config->setConfigParam('sShopVar', '');

        $exception = oxNew('oxException');

        $caller = $this->getMock('oxOnlineLicenseCheckCaller', array('doRequest'), array(), '', false);
        $caller->expects($this->once())->method('doRequest')->will($this->throwException($exception));
        /** @var oxOnlineLicenseCheckCaller $caller */

        $licenseCheck = oxNew('oxOnlineLicenseCheck', $caller);
        $licenseCheck->validateShopSerials();

        $this->assertFalse($config->getConfigParam('blShopStopped'));
        $this->assertEquals('', $config->getConfigParam('sShopVar'));
    }

    public function testShopDoesNotGoToGracePeriodWhenSerialValid()
    {
        $config = $this->getConfig();
        $config->setConfigParam('blShopStopped', false);
        $config->setConfigParam('sShopVar', '');

        $response = oxNew('oxOnlineLicenseCheckResponse');
        $response->code = '0';
        $response->message = 'ACK';

        $caller = $this->getMock('oxOnlineLicenseCheckCaller', array('doRequest'), array(), '', false);
        $caller->expects($this->once())->method('doRequest')->will($this->returnValue($response));
        /** @var oxOnlineLicenseCheckCaller $caller */

        $licenseCheck = oxNew('oxOnlineLicenseCheck', $caller);
        $licenseCheck->validateShopSerials();

        $this->assertFalse($this->getConfig()->getConfigParam('blShopStopped'));
        $this->assertEquals('', $this->getConfig()->getConfigParam('sShopVar'));
    }
}
