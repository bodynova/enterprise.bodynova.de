<?php
/**
 * This Software is the property of OXID eSales and is protected
 * by copyright law - it is NOT Freeware.
 *
 * Any unauthorized use of this software without a valid license key
 * is a violation of the license agreement and will be prosecuted by
 * civil and criminal law.
 *
 * @link      http://www.oxid-esales.com
 * @copyright (C) OXID eSales AG 2003-2016
 * @version   OXID eShop EE
 */

namespace OxidEsales\EshopEnterprise\Tests\Unit\Core\Smarty\Plugin;

use OxidEsales\EshopCommunity\Core\Edition\EditionPathProvider;
use OxidEsales\EshopCommunity\Core\Edition\EditionRootPathProvider;
use OxidEsales\EshopCommunity\Core\Edition\EditionSelector;
use OxidEsales\Eshop\Core\Registry;
use OxidEsales\TestingLibrary\UnitTestCase;
use oxRegistry;
use Smarty;

class PluginsSmartyIncludeWidgetTest extends UnitTestCase
{
    /**
     * Include tested smarty plugin.
     */
    public static function setUpBeforeClass()
    {
        $pathSelector = new EditionPathProvider(new EditionRootPathProvider(new EditionSelector()));
        require_once $pathSelector->getSmartyPluginsDirectory() . 'function.oxid_include_widget.php';
    }

    public function testIncludeIfHTMLCachingIsOn()
    {
        $reverseProxyBackend = $this->getMock("oxReverseProxyBackend", array("isActive"));
        $reverseProxyBackend->expects($this->any())->method("isActive")->will($this->returnValue(true));

        Registry::set("oxReverseProxyBackend", $reverseProxyBackend);

        $config = $this->getMock('oxConfig');
        $config->expects($this->atLeastOnce())
            ->method('getWidgetUrl')
            ->with(
                $this->identicalTo(null),
                $this->identicalTo(null),
                $this->identicalTo(array('cl' => strtolower('oxwInformation')))
            )
            ->will($this->returnValue('widget_url'));
        Registry::set('oxConfig', $config);

        $smarty = new Smarty();
        $output = "<esi:include src='widget_url'/>";
        $this->assertEquals($output, smarty_function_oxid_include_widget(array('cl' => 'oxwInformation'), $smarty));
    }
}
