<?php
/**
 * This Software is the property of OXID eSales and is protected
 * by copyright law - it is NOT Freeware.
 *
 * Any unauthorized use of this software without a valid license key
 * is a violation of the license agreement and will be prosecuted by
 * civil and criminal law.
 *
 * @link      http://www.oxid-esales.com
 * @copyright (C) OXID eSales AG 2003-2016
 * @version   OXID eShop EE
 */

namespace OxidEsales\EshopEnterprise\Tests\Unit\Core;

use OxidEsales\TestingLibrary\UnitTestCase;

class I18nTest extends UnitTestCase
{
    /**
     * base test
     */
    public function testGetViewName()
    {
        $i18nObject = oxNew('oxi18n');
        $i18nObject->init('oxarticles');

        $i18nObject->setLanguage(1);
        $i18nObject->setEnableMultilang(false);

        $i18nObject->setForceCoreTableUsage(true);
        $this->assertEquals(getViewName('oxarticles', -1, -1), $i18nObject->getViewName());
        $this->assertEquals(getViewName('oxarticles', -1, 1), $i18nObject->getViewName(0));
        $this->assertEquals(getViewName('oxarticles', -1, -1), $i18nObject->getViewName(1));
        $this->assertEquals(getViewName('oxarticles', -1, -1), $i18nObject->getViewName());

        $i18nObject->setEnableMultilang(true);
        $this->assertEquals(getViewName('oxarticles', 1, -1), $i18nObject->getViewName());
        $this->assertEquals(getViewName('oxarticles', 1, -1), $i18nObject->getViewName(1));
        $this->assertEquals(getViewName('oxarticles', 1, 1), $i18nObject->getViewName(0));
        $this->assertEquals(getViewName('oxarticles', 1, -1), $i18nObject->getViewName());
    }

    /**
     * This test is for bugfix #4536.
     *
     * Tests if item is loaded from another subshop.
     * This test is important for certain cases when item is loaded from different subshops
     */
    public function testLoadItemFromAnyShop()
    {
        $this->getSession()->setVariable("actshop", 16);

        $oBaseObject = oxNew("oxBase");
        $oBaseObject->init("oxarticles");
        $oBaseObject->load('1126');

        $this->assertEquals("Bar-Set ABSINTH", $oBaseObject->oxarticles__oxtitle->value);

    }
}
