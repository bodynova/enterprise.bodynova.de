<?php
/**
 * This Software is the property of OXID eSales and is protected
 * by copyright law - it is NOT Freeware.
 *
 * Any unauthorized use of this software without a valid license key
 * is a violation of the license agreement and will be prosecuted by
 * civil and criminal law.
 *
 * @link      http://www.oxid-esales.com
 * @copyright (C) OXID eSales AG 2003-2016
 * @version   OXID eShop EE
 */

namespace OxidEsales\EshopEnterprise\Tests\Unit\Core;

use oxConfig;
use OxidEsales\EshopEnterprise\Core\Cache\ReverseProxy\ReverseProxyBackend;
use OxidEsales\TestingLibrary\UnitTestCase;
use oxView;
use PHPUnit_Framework_MockObject_MockObject;
use stdClass;
use oxRegistry;
use oxField;
use oxUtilsHelper;

require_once TEST_LIBRARY_HELPERS_PATH . 'oxUtilsHelper.php';

class ViewTest extends UnitTestCase
{
    /*
     * Test adding global params to view data
     */
    public function testAddGlobalParams()
    {
        $oView = oxNew('oxView');
        $this->assertTrue($oView->isMall());
    }

    /**
     * oxView::executeFunction() test case
     *
     * @return null
     */
    public function testExecuteFunctionEE()
    {
        $oRights = $this->getMock('oxRights', array('processView'));
        $oRights->expects($this->once())->method('processView')->will($this->returnValue(new stdClass()));

        $oCmp = $this->getMock('oxcmp_categories', array('xxx', 'getRights'));
        $oCmp->expects($this->once())->method('xxx');
        $oCmp->expects($this->once())->method('getRights')->will($this->returnValue($oRights));

        $oCmp->executeFunction('xxx');
    }

    /**
     * oxView::_executeNewAction() test case
     *
     * @return null
     */
    public function testExecuteNewActionSslIsAdminEE()
    {
        $this->getSession()->setId('SID');

        oxAddClassModule("oxUtilsHelper", "oxUtils");

        /** @var oxConfig|PHPUnit_Framework_MockObject_MockObject $config */
        $config = $this->getMock('oxConfig', array('isSsl', 'getSslShopUrl', 'getShopUrl'));
        $config->expects($this->once())->method('isSsl')->will($this->returnValue(true));
        $config->expects($this->once())->method('getSslShopUrl')->will($this->returnValue('SSLshopurl/'));
        $config->expects($this->never())->method('getShopUrl');
        $config->setConfigParam('sAdminDir', 'admin');

        /** @var ReverseProxyBackend|PHPUnit_Framework_MockObject_MockObject $cache */
        $cache = $this->getMock('oxReverseProxyBackend', array('isEnabled', 'execute'));
        $cache->expects($this->once())->method('isEnabled')->will($this->returnValue(true));
        $cache->expects($this->once())->method('execute');

        /** @var oxView|PHPUnit_Framework_MockObject_MockObject $view */
        $view = $this->getMock('oxView', array('getConfig', 'isAdmin', '_getReverseProxyBackend'));
        $view->expects($this->once())->method('getConfig')->will($this->returnValue($config));
        $view->expects($this->once())->method('isAdmin')->will($this->returnValue(true));
        $view->expects($this->once())->method('_getReverseProxyBackend')->will($this->returnValue($cache));
        $view->UNITexecuteNewAction("details?fnc=somefnc&anid=someanid");
        $sRedirectUrl = oxUtilsHelper::$sRedirectUrl;

        $this->assertEquals('SSLshopurl/admin/index.php?cl=details&fnc=somefnc&anid=someanid&' . $this->getSession()->sid(), $sRedirectUrl);
    }

    public function testIsCacheable()
    {
        $oView = oxNew('oxView');
        $this->assertTrue($oView->isCacheable());
    }

    public function testSetIsCacheable()
    {
        $oView = oxNew('oxView');
        $oView->setIsCacheable(false);
        $this->assertFalse($oView->isCacheable());
    }

    /**
     * Tests oxView::getCacheLifeTime() with all tables used in function
     */
    public function testGetCacheLifeTime_FromAllTables()
    {
        $this->_mockUtilsDateTime(oxRegistry::get("oxUtilsDate")->getTime());

        $sActiveFrom = date('Y-m-d H:i:s', oxRegistry::get("oxUtilsDate")->getTime() + 5);
        $oDiscount = oxNew('oxDiscount');
        $oDiscount->setId('_testDiscount');
        $oDiscount->oxdiscount__oxactivefrom = new oxField($sActiveFrom);
        $oDiscount->save();

        $sActiveFrom = date('Y-m-d H:i:s', oxRegistry::get("oxUtilsDate")->getTime() + 4);
        $sUpdatePriceTime = date('Y-m-d H:i:s', oxRegistry::get("oxUtilsDate")->getTime() + 3);
        $oArticle = oxNew('oxArticle');
        $oArticle->setId('_testArticle');
        $oArticle->oxarticles__oxactivefrom = new oxField($sActiveFrom);
        $oArticle->oxarticles__oxupdatepricetime = new oxField($sUpdatePriceTime);
        $oArticle->save();

        $sUpdatePriceTime = date('Y-m-d H:i:s', oxRegistry::get("oxUtilsDate")->getTime() + 6);
        $oField2Shop = oxNew('oxField2Shop');
        $oField2Shop->setId('_testDiscount');
        $oField2Shop->oxfield2shop__oxupdatepricetime = new oxField($sUpdatePriceTime);
        $oField2Shop->save();

        $iTime = oxRegistry::get('oxUtilsDate')->getTime() - 5;
        $oUserBasket = oxNew('oxUserBasket');
        $oUserBasket->setId('_testBasket');
        $oUserBasket->oxuserbaskets__oxuserid = new oxField(1, oxField::T_RAW);
        $oUserBasket->oxuserbaskets__oxtitle = new oxField('not_reservations', oxField::T_RAW);
        $oUserBasket->save();
        $oUserBasket->load('_testBasket');
        $oUserBasket->oxuserbaskets__oxupdate = new oxField($iTime);
        $oUserBasket->save();

        $this->getConfig()->setConfigParam("iLayoutCacheLifeTime", 10);
        $oView = oxNew('oxView');
        $iExpectedCacheLifeTime = 3;
        $this->assertEquals($iExpectedCacheLifeTime, $oView->getCacheLifeTime());
        $this->cleanUpTable('oxdiscount');
        $this->cleanUpTable('oxarticles');
        $this->cleanUpTable('oxfield2shop');
        $this->cleanUpTable('oxuserbaskets');
    }

    /**
     * Tests oxView::getCacheLifeTime() with actions.
     */
    public function testGetCacheLifeTime_FromActions_WhenActionEndsBeforeCacheLifeTime()
    {
        $this->_mockUtilsDateTime(oxRegistry::get("oxUtilsDate")->getTime());

        $activeFrom = date('Y-m-d H:i:s', oxRegistry::get("oxUtilsDate")->getTime() + 4);
        $updatePriceTime = date('Y-m-d H:i:s', oxRegistry::get("oxUtilsDate")->getTime() + 3);

        $actions = oxNew('oxActions');
        $actions->setId('_testArticle');
        $actions->oxactions__oxactiveto = new oxField($activeFrom);
        $actions->oxactions__oxactiveto = new oxField($updatePriceTime);
        $actions->save();

        $this->getConfig()->setConfigParam("iLayoutCacheLifeTime", 10);
        $oView = oxNew('oxView');
        $iExpectedCacheLifeTime = 3;
        $this->assertEquals($iExpectedCacheLifeTime, $oView->getCacheLifeTime());
        $this->cleanUpTable('oxactions');
    }

    /**
     * Tests oxView::getCacheLifeTime() with actions.
     */
    public function testGetCacheLifeTime_FromActions_WhenActionEndsAfterCacheLifeTime()
    {
        $this->_mockUtilsDateTime(oxRegistry::get("oxUtilsDate")->getTime());

        $activeFrom = date('Y-m-d H:i:s', oxRegistry::get("oxUtilsDate")->getTime() + 4);
        $updatePriceTime = date('Y-m-d H:i:s', oxRegistry::get("oxUtilsDate")->getTime() + 30);

        $actions = oxNew('oxActions');
        $actions->setId('_testArticle');
        $actions->oxactions__oxactiveto = new oxField($activeFrom);
        $actions->oxactions__oxactiveto = new oxField($updatePriceTime);
        $actions->save();

        $this->getConfig()->setConfigParam("iLayoutCacheLifeTime", 10);
        $oView = oxNew('oxView');
        $iExpectedCacheLifeTime = 10;
        $this->assertEquals($iExpectedCacheLifeTime, $oView->getCacheLifeTime());
        $this->cleanUpTable('oxactions');
    }

    /**
     * Tests oxView::getCacheLifeTime() when ir returns default lifetime- 3600
     */
    public function testGetCacheLifeTime_DefaultTime()
    {
        $this->getConfig()->setConfigParam("iLayoutCacheLifeTime", null);
        $oView = oxNew('oxView');
        $this->assertEquals(3600, $oView->getCacheLifeTime());
    }

    /**
     * Tests oxView::getCacheLifeTime() when reservation is enabled, but time is taken from discount table
     */
    public function testgetCacheLifeTime_ReservationEnabled_FromDiscount()
    {
        $this->_mockUtilsDateTime(oxRegistry::get('oxUtilsDate')->getTime());

        $iTime = oxRegistry::get('oxUtilsDate')->getTime() - 100;
        $oUserBasket = oxNew('oxUserBasket');
        $oUserBasket->setId('_testBasket');
        $oUserBasket->oxuserbaskets__oxuserid = new oxField(1, oxField::T_RAW);
        $oUserBasket->oxuserbaskets__oxtitle = new oxField('reservations', oxField::T_RAW);
        $oUserBasket->save();
        $oUserBasket->load('_testBasket');
        $oUserBasket->oxuserbaskets__oxupdate = new oxField($iTime);
        $oUserBasket->save();

        $sActiveFrom = date('Y-m-d H:i:s', oxRegistry::get("oxUtilsDate")->getTime() + 5);
        $oDiscount = oxNew('oxDiscount');
        $oDiscount->setId('_testDiscount');
        $oDiscount->oxdiscount__oxactivefrom = new oxField($sActiveFrom);
        $oDiscount->save();

        $this->getConfig()->setConfigParam('blPsBasketReservationEnabled', true);
        $this->getConfig()->setConfigParam('iPsBasketReservationTimeout', 110);
        $oView = oxNew('oxView');
        $iExpectedCacheLifeTime = 5;
        $this->assertEquals($iExpectedCacheLifeTime, $oView->getCacheLifeTime());
        $this->cleanUpTable('oxuserbaskets');
        $this->cleanUpTable('oxdiscount');
    }

    /**
     * Tests oxView::getCacheLifeTime() when reservation is enabled and time is taken from userbaskets table
     */
    public function testGetCacheLifeTime_ReservationEnabled_FromUserbaskets()
    {
        $this->_mockUtilsDateTime(oxRegistry::get('oxUtilsDate')->getTime());

        $iTime = oxRegistry::get('oxUtilsDate')->getTime() - 109;
        $oUserBasket = oxNew('oxUserBasket');
        $oUserBasket->setId('_testBasket');
        $oUserBasket->oxuserbaskets__oxuserid = new oxField(1, oxField::T_RAW);
        $oUserBasket->oxuserbaskets__oxtitle = new oxField('reservations', oxField::T_RAW);
        $oUserBasket->save();
        $oUserBasket->load('_testBasket');
        $oUserBasket->oxuserbaskets__oxupdate = new oxField($iTime);
        $oUserBasket->save();

        $sActiveFrom = date('Y-m-d H:i:s', oxRegistry::get("oxUtilsDate")->getTime() + 5);
        $oDiscount = oxNew('oxDiscount');
        $oDiscount->setId('_testDiscount');
        $oDiscount->oxdiscount__oxactivefrom = new oxField($sActiveFrom);
        $oDiscount->save();

        $this->getConfig()->setConfigParam('blPsBasketReservationEnabled', true);
        $this->getConfig()->setConfigParam('iPsBasketReservationTimeout', 110);
        $oView = oxNew('oxView');
        $iExpectedCacheLifeTime = 1;
        $this->assertEquals($iExpectedCacheLifeTime, $oView->getCacheLifeTime());
        $this->cleanUpTable('oxuserbaskets');
        $this->cleanUpTable('oxdiscount');
    }

    /**
     * Mocks time and uses it in oxView::getCacheLifeTime()
     *
     * @param $sTime
     */
    protected function _mockUtilsDateTime($sTime)
    {
        $oUtilsDate = $this->getMock('oxUtilsDate', array('getTime'));
        $oUtilsDate->expects($this->any())->method('getTime')->will($this->returnValue($sTime));
        oxRegistry::set('oxUtilsDate', $oUtilsDate);
    }

    public function testSetGetAllowCacheInvalidating()
    {
        $oView = oxNew("oxView");
        // default is now false
        $this->assertEquals(true, $oView->getAllowCacheInvalidating());

        $oView->setAllowCacheInvalidating(false);
        $this->assertEquals(false, $oView->getAllowCacheInvalidating());

        $oView->setAllowCacheInvalidating(true);
        $this->assertEquals(true, $oView->getAllowCacheInvalidating());
    }
}
