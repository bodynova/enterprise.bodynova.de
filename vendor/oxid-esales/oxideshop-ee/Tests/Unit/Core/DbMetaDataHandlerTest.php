<?php
/**
 * This Software is the property of OXID eSales and is protected
 * by copyright law - it is NOT Freeware.
 *
 * Any unauthorized use of this software without a valid license key
 * is a violation of the license agreement and will be prosecuted by
 * civil and criminal law.
 *
 * @link      http://www.oxid-esales.com
 * @copyright (C) OXID eSales AG 2003-2016
 * @version   OXID eShop EE
 */

namespace OxidEsales\EshopEnterprise\Tests\Unit\Core;

use OxidEsales\EshopEnterprise\Core\DbMetaDataHandler;
use \oxDb;
use \oxTestModules;

class DbMetaDataHandlerTest extends \oxUnitTestCase
{
    /**
     * Tear down the fixture.
     *
     * @return null
     */
    protected function tearDown()
    {
        $this->cleanUpTable('oxactions');

        //dropping test table
        oxDb::getDb()->execute("DROP TABLE IF EXISTS `testDbMetaDataHandler`");

        parent::tearDown();
    }

    /*
     * Test table
     */
    protected function _createTestTable()
    {
        $sSql = " CREATE TABLE `testDbMetaDataHandler` (
                    `OXID` char(32) NOT NULL,
                    `OXTITLE` varchar(255) NOT NULL,
                    `OXTITLE_1` varchar(255) NOT NULL,
                    `OXLONGDESC` text NOT NULL,
                    `OXLONGDESC_1` text NOT NULL,
                     PRIMARY KEY (`OXID`),
                     KEY `OXTITLE` (`OXTITLE`),
                     KEY `OXTITLE_1` (`OXTITLE_1`),
                     FULLTEXT KEY `OXLONGDESC` (`OXLONGDESC`),
                     FULLTEXT KEY `OXLONGDESC_1` (`OXLONGDESC_1`)
                  ) ENGINE = MyISAM";

        oxDb::getDb()->execute($sSql);
    }

    /**
     * Test getting all db tables list (except views)
     */
    public function testGetAllTables()
    {
        $aTables = oxDb::getDb()->getAll("show tables");

        $aTablesList = array();
        foreach ($aTables as $aTableInfo) {
            if (strpos($aTableInfo[0], "oxv_") !== 0) {
                $aTablesList[] = $aTableInfo[0];
            }
        }

        /** @var DbMetaDataHandler $oDbMeta */
        $oDbMeta = oxNew("oxDbMetaDataHandler");
        $this->assertTrue(count($aTablesList) > 1);
        $this->assertEquals($aTablesList, $oDbMeta->getAllTables());
    }

    /**
     * Test if method call another method which creates sql's with correct params
     */
    public function testAddNewLangToDb()
    {
        $aTables = oxDb::getDb()->getAll("show tables");

        $aTablesList = array();
        foreach ($aTables as $aTableInfo) {
            if (strpos($aTableInfo[0], "oxv_") !== 0) {
                $aTablesList[] = $aTableInfo[0];
            }
        }

        /** @var DbMetaDataHandler|\PHPUnit_Framework_MockObject_MockObject $oDbMeta */
        $oDbMeta = $this->getMock('\OxidEsales\EshopEnterprise\Core\DbMetaDataHandler', array('addNewMultilangField'));

        $iIndex = 0;
        foreach ($aTablesList as $sTableName) {
            $oDbMeta->expects($this->at($iIndex++))->method('addNewMultilangField')->with($this->equalTo($sTableName));
        }

        $oDbMeta->addNewLangToDb();
    }

    /*
     * Test if method call views updater
     */
    public function testAddNewLangToDb_upatingViews()
    {
        /** @var DbMetaDataHandler|\PHPUnit_Framework_MockObject_MockObject $oDbMeta */
        $oDbMeta = $this->getMock('\OxidEsales\EshopEnterprise\Core\DbMetaDataHandler', array('addNewMultilangField', 'updateViews'));
        $oDbMeta->expects($this->any())->method('addNewMultilangField');
        $oDbMeta->expects($this->once())->method('updateViews');

        $oDbMeta->addNewLangToDb();
    }

    public function testUpdateViews()
    {
        // saving parameters
        oxTestModules::addFunction("oxshop", "generateViews", "{ \$oConfig = \$this->getConfig(); \$oConfig->setConfigParam( 'testUpdateViewsDebugData', array( \$aA[0], \$aA[1] ) ); return true; }");
        $myConfig = $this->getConfig();

        /** @var DbMetaDataHandler $oDbMeta */
        $oDbMeta = oxNew("oxDbMetaDataHandler");
        $this->assertTrue($oDbMeta->updateViews());

        $aExpTestData = array(false, array_fill_keys($myConfig->getConfigParam('aMultiShopTables'), false));

        $aTestData = $myConfig->getConfigParam('testUpdateViewsDebugData');
        $this->assertNotNull($aTestData);
        $this->assertEquals($aExpTestData, $aTestData);
    }

    public function testUpdateViewsForOxarticleTable()
    {
        // saving parameters
        oxTestModules::addFunction("oxshop", "generateViews", "{ \$oConfig = \$this->getConfig(); \$oConfig->setConfigParam( 'testUpdateViewsDebugData', array( \$aA[0], \$aA[1] ) ); return true; }");
        $myConfig = $this->getConfig();

        /** @var DbMetaDataHandler $oDbMeta */
        $oDbMeta = oxNew("oxDbMetaDataHandler");
        $this->assertTrue($oDbMeta->updateViews(array("oxarticles")));

        $aExpTestData = array(false, array("oxarticles" => false));

        $aTestData = $myConfig->getConfigParam('testUpdateViewsDebugData');
        $this->assertNotNull($aTestData);
        $this->assertEquals($aExpTestData, $aTestData);
    }
}
