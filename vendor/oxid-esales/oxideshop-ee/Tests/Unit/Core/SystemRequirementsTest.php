<?php
/**
 * This Software is the property of OXID eSales and is protected
 * by copyright law - it is NOT Freeware.
 *
 * Any unauthorized use of this software without a valid license key
 * is a violation of the license agreement and will be prosecuted by
 * civil and criminal law.
 *
 * @link      http://www.oxid-esales.com
 * @copyright (C) OXID eSales AG 2003-2016
 * @version   OXID eShop EE
 */

namespace OxidEsales\EshopEnterprise\Tests\Unit\Core;

use OxidEsales\TestingLibrary\UnitTestCase;

class SystemRequirementsTest extends UnitTestCase
{
    /**
     * Provides different server configuration to check memory limit.
     *
     * @return array
     */
    public function providerCheckMemoryLimit()
    {
        $aMemoryLimitsWithExpectedSystemHealth = array(
            array('8M', 0),
            array('32M', 1),
            array('60M', 2),
            array('-1', 2),
        );

        return $aMemoryLimitsWithExpectedSystemHealth;
    }

    /**
     * Testing oxSysRequirements::checkMemoryLimit()
     *
     * @param string $sMemoryLimit    how much memory allocated.
     * @param int    $iExpectedResult if fits system requirements.
     *
     * @dataProvider providerCheckMemoryLimit
     *
     * @return null
     */
    public function testCheckMemoryLimit($sMemoryLimit, $iExpectedResult)
    {
        $oSysReq = oxNew('oxSysRequirements');
        $this->assertEquals($iExpectedResult, $oSysReq->checkMemoryLimit($sMemoryLimit));
    }

    /**
     * Testing oxSysRequirements::checkMysqlVersion()
     *
     * @return null
     */
    public function testCheckMysqlVersionForEnterpriseEdition()
    {
        $oSysReq = oxNew('oxSysRequirements');
        $this->assertEquals(0, $oSysReq->checkMysqlVersion('5.6'));
        $this->assertEquals(0, $oSysReq->checkMysqlVersion('5.6.0'));
        $this->assertEquals(0, $oSysReq->checkMysqlVersion('5.6.30-0ubuntu0.14.04.1'));
    }
}
