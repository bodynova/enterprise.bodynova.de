<?php
/**
 * This Software is the property of OXID eSales and is protected
 * by copyright law - it is NOT Freeware.
 *
 * Any unauthorized use of this software without a valid license key
 * is a violation of the license agreement and will be prosecuted by
 * civil and criminal law.
 *
 * @link      http://www.oxid-esales.com
 * @copyright (C) OXID eSales AG 2003-2016
 * @version   OXID eShop EE
 */

namespace OxidEsales\EshopEnterprise\Tests\Unit\Core;

use OxidEsales\EshopEnterprise\Core\UtilsCount;
use \oxRegistry;
use \oxField;
use \oxArticle;
use \oxCategory;
use \oxDb;

class UtilsCountTest extends \oxUnitTestCase
{
    /**
     * Initialize the fixture.
     *
     * @return null
     */
    protected function setUp()
    {
        parent::setUp();
        $oPriceCat = new oxCategory();
        $oPriceCat->oxcategories__oxactive = new oxField(1, oxField::T_RAW);
        $oPriceCat->oxcategories__oxparentid = new oxField("oxrootid", oxField::T_RAW);
        $oPriceCat->oxcategories__oxshopid = new oxField($this->getConfig()->getBaseShopId(), oxField::T_RAW);
        $oPriceCat->oxcategories__oxtitle = new oxField("Price Cat 1", oxField::T_RAW);
        $oPriceCat->oxcategories__oxpricefrom = new oxField(1, oxField::T_RAW);
        $oPriceCat->oxcategories__oxpriceto = new oxField(100, oxField::T_RAW);
        $oPriceCat->save();

        $this->aCats[$oPriceCat->getId()] = $oPriceCat;

        $oPriceCat = new oxCategory();
        $oPriceCat->oxcategories__oxactive = new oxField(1, oxField::T_RAW);
        $oPriceCat->oxcategories__oxparentid = new oxField("oxrootid", oxField::T_RAW);
        $oPriceCat->oxcategories__oxshopid = new oxField($this->getConfig()->getBaseShopId(), oxField::T_RAW);
        $oPriceCat->oxcategories__oxtitle = new oxField("Price Cat 2", oxField::T_RAW);
        $oPriceCat->oxcategories__oxpricefrom = new oxField(1, oxField::T_RAW);
        $oPriceCat->oxcategories__oxpriceto = new oxField(100, oxField::T_RAW);
        $oPriceCat->save();

        $this->aCats[$oPriceCat->getId()] = $oPriceCat;

        $this->getConfig()->setGlobalParameter('aLocalVendorCache', null);
        oxRegistry::getUtils()->toFileCache('aLocalVendorCache', '');
        oxRegistry::getUtils()->toFileCache('aLocalCatCache', '');

        $oCache = oxNew('oxcache');
        $oCache->reset();
    }

    protected function tearDown()
    {
        foreach ($this->aCats as $oCat) {
            $oCat->delete();
        }

        $this->getConfig()->setGlobalParameter('aLocalVendorCache', null);
        oxRegistry::getUtils()->toFileCache('aLocalVendorCache', '');
        oxRegistry::getUtils()->toFileCache('aLocalCatCache', '');

        oxRegistry::getUtils()->oxResetFileCache();

        $oCache = oxNew('oxcache');
        $oCache->reset();

        // deleting test articles
        $oArticle = new oxarticle;
        $oArticle->delete('testarticle1');
        $oArticle->delete('testarticle2');
        $oArticle->delete('_testArticle');

        $this->cleanUpTable('oxarticles');

        parent::tearDown();
    }


    public function testSetPriceCatArticleCountWhenPriceFrom0To1AndDbContainsProductWhichPriceIs0()
    {
        $oArticle = new oxArticle();
        $oArticle->setId("_testArticle");
        $oArticle->oxarticles__oxshopid = new oxField($this->getConfig()->getBaseShopId());
        $oArticle->oxarticles__oxactive = new oxField(1);
        $oArticle->oxarticles__oxvarminprice = new oxField(0);
        $oArticle->save();

        $oUtilsCount = new UtilsCount();

        $this->assertEquals(1, $oUtilsCount->setPriceCatArticleCount(array(), 'xxx', 'xxx', 0, 1));
    }

    public function testGetCatArticleCount()
    {
        $this->assertEquals('0', oxRegistry::get("oxUtilsCount")->GetCatArticleCount('', true));
        $this->assertEquals('1', oxRegistry::get("oxUtilsCount")->GetCatArticleCount('30e44ab8338d7bf06.79655612', true));

        oxRegistry::getUtils()->oxResetFileCache();
        $this->assertEquals('0', oxRegistry::get("oxUtilsCount")->GetCatArticleCount('', true));
        $this->assertEquals('1', oxRegistry::get("oxUtilsCount")->GetCatArticleCount('30e44ab8338d7bf06.79655612', true));
    }

    public function testGetVendorArticleCount()
    {
        $myUtilsTest = new UtilsCount();

        $aCache = $myUtilsTest->UNITgetVendorCache();

        $sRet = oxRegistry::get("oxUtilsCount")->setVendorArticleCount($aCache, 'd2e44d9b32fd2c224.65443178', $myUtilsTest->UNITgetUserViewId(), true);
        $sCount = oxRegistry::get("oxUtilsCount")->getVendorArticleCount('d2e44d9b32fd2c224.65443178', true, true);

        $this->assertEquals($sRet, $sCount);
        $this->assertTrue($sRet > 0);
    }

    public function testGetManufacturerArticleCount()
    {
        $myUtilsTest = new UtilsCount();

        $aCache = $myUtilsTest->UNITgetManufacturerCache();

        $sRet = oxRegistry::get("oxUtilsCount")->setManufacturerArticleCount($aCache, '2536d76675ebe5cb777411914a2fc8fb', $myUtilsTest->UNITgetUserViewId(), true);
        $sCount = oxRegistry::get("oxUtilsCount")->getManufacturerArticleCount('2536d76675ebe5cb777411914a2fc8fb', true, true);

        $this->assertEquals($sRet, $sCount);
        $this->assertTrue($sRet > 0);
    }

    public function testSetCatArticleCount()
    {
        $myUtilsTest = new UtilsCount();
        $sRetSet = oxRegistry::get("oxUtilsCount")->setCatArticleCount(array(), '30e44ab8338d7bf06.79655612', $myUtilsTest->UNITgetUserViewId(), true);
        $sRetGet = oxRegistry::get("oxUtilsCount")->getCatArticleCount('30e44ab8338d7bf06.79655612', true);

        $this->assertEquals($sRetSet, $sRetGet);
        $this->assertEquals($sRetSet, 1);
    }

    public function testSetPriceCatArticleCount()
    {
        $myUtilsTest = new UtilsCount();

        $sRetSet = oxRegistry::get("oxUtilsCount")->setPriceCatArticleCount(array(), '30e44ab8338d7bf06.79655612', $myUtilsTest->UNITgetUserViewId(), 10, 100);
        $sRetGet = oxRegistry::get("oxUtilsCount")->getPriceCatArticleCount('30e44ab8338d7bf06.79655612', 10, 100, true);
        $this->assertEquals($sRetSet, $sRetGet);
        $this->assertEquals(48, $sRetSet);
    }

    public function testSetVendorArticleCount()
    {
        $myUtilsTest = new UtilsCount();
        $aCache = null;
        $sCatId = 'root';
        $sActIdent = null;

        // always return 0 if $sCatId ='root'
        $this->assertEquals(oxRegistry::get("oxUtilsCount")->setVendorArticleCount($aCache, $sCatId, $sActIdent, true), 0);
        oxRegistry::getUtils()->oxResetFileCache();

        $aCache = $myUtilsTest->UNITgetVendorCache();
        $sVendorID = 'd2e44d9b31fcce448.08890330'; //Hersteller 1 from Demodata
        $sCatId = $sVendorID;
        $sActIdent = $myUtilsTest->UNITgetUserViewId();
        //echo "\n->".setVendorArticleCount($aCache, $sCatId, $sActIdent)."<-";
        $this->assertEquals(oxRegistry::get("oxUtilsCount")->setVendorArticleCount($aCache, $sCatId, $sActIdent, true), 14);
    }

    public function testSetManufacturerArticleCount()
    {
        $myUtilsTest = new UtilsCount();
        $aCache = null;
        $sCatId = 'root';
        $sActIdent = null;

        // always return 0 if $sCatId ='root'
        $this->assertEquals(oxRegistry::get("oxUtilsCount")->setManufacturerArticleCount($aCache, $sCatId, $sActIdent, true), 0);
        oxRegistry::getUtils()->oxResetFileCache();

        $aCache = $myUtilsTest->UNITgetManufacturerCache();
        $sManufacturerID = '88a996f859f94176da943f38ee067984'; //Hersteller 1 from Demodata
        $sCatId = $sManufacturerID;
        $sActIdent = $myUtilsTest->UNITgetUserViewId();
        //echo "\n->".setManufacturerArticleCount($aCache, $sCatId, $sActIdent)."<-";
        $this->assertEquals(oxRegistry::get("oxUtilsCount")->setManufacturerArticleCount($aCache, $sCatId, $sActIdent, true), 14);
    }

    public function testZeroArtManufaturerCache()
    {
        $myUtilsTest = $this->getMock('\OxidEsales\EshopEnterprise\Core\UtilsCount', array('_setManufacturerCache'));
        $myUtilsTest->expects($this->once())->method('_setManufacturerCache')->with(
            $this->equalTo(
                array(
                    '_testManufacturerId' =>
                        array(
                            '2fb5911b89dddda329c256f56d1f60c5' => '0',
                        ),
                )
            )
        );

        oxRegistry::getUtils()->oxResetFileCache();
        $oDb = oxDb::getDb();
        $oDb->execute('replace INTO `oxmanufacturers` (`OXID`, `OXSHOPID`) VALUES ("_testManufacturerId", 1);');

        $sActIdent = $myUtilsTest->UNITgetUserViewId();
        $iCount = $myUtilsTest->setManufacturerArticleCount(array(), '_testManufacturerId', $sActIdent);

        $this->assertSame(0, $iCount);
    }
}
