<?php
/**
 * This Software is the property of OXID eSales and is protected
 * by copyright law - it is NOT Freeware.
 *
 * Any unauthorized use of this software without a valid license key
 * is a violation of the license agreement and will be prosecuted by
 * civil and criminal law.
 *
 * @link      http://www.oxid-esales.com
 * @copyright (C) OXID eSales AG 2003-2016
 * @version   OXID eShop EE
 */

namespace OxidEsales\EshopEnterprise\Tests\Unit\Core;

use \oxRegistry;

class SessionTest extends \oxUnitTestCase
{
    /**
     * Test if add subshop ID to URL.
     */
    function testSidInAdmin()
    {
        $session = $this->getMock('oxSession', array('_getCookieSid', 'isAdmin', 'getSessionChallengeToken'));
        $session->expects($this->any())->method('getSessionChallengeToken')->will($this->returnValue('stok'));
        $session->expects($this->any())->method('_getCookieSid')->will($this->returnValue('admin_sid'));
        $session->expects($this->any())->method('isAdmin')->will($this->returnValue(true));
        $session->UNITsetSessionId('testSid');

        $urlPartWithSid = 'stoken=stok&amp;shp=' . $this->getConfig()->getShopId();

        $this->assertEquals($urlPartWithSid, $session->sid());
    }

    /**
     * Headers should not be sent to browser when Reverse Proxy is caching as headers affect caching.
     */
    public function testIfSetHeaderWhenReverseProxyActive()
    {
        $reverseProxyBackend = $this->getMock('oxReverseProxyBackend');
        $reverseProxyBackend->expects($this->any())->method('isActive')->will($this->returnValue(true));
        oxRegistry::set('oxReverseProxyBackend', $reverseProxyBackend);

        $session = oxNew('oxSession');
        $this->assertFalse($session->needToSetHeaders());
    }

    /**
     * Headers should be sent to browser in usual case.
     */
    public function testIfSetHeaderWhenReverseProxyNotActive()
    {
        $reverseProxyBackend = $this->getMock('oxReverseProxyBackend');
        $reverseProxyBackend->expects($this->any())->method('isActive')->will($this->returnValue(false));
        oxRegistry::set('oxReverseProxyBackend', $reverseProxyBackend);

        $session = oxNew('oxSession');
        $this->assertTrue($session->needToSetHeaders());
    }
}
