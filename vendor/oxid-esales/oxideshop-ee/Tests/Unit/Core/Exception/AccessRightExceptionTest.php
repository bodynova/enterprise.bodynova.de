<?php
/**
 * This Software is the property of OXID eSales and is protected
 * by copyright law - it is NOT Freeware.
 *
 * Any unauthorized use of this software without a valid license key
 * is a violation of the license agreement and will be prosecuted by
 * civil and criminal law.
 *
 * @link      http://www.oxid-esales.com
 * @copyright (C) OXID eSales AG 2003-2016
 * @version   OXID eShop EE
 */
namespace OxidEsales\EshopEnterprise\Tests\Unit\Core\Exception;

use \PHPUnit_Framework_MockObject_MockObject as MockObject;

class AccessRightExceptionTest extends \oxUnitTestCase
{
    /**
     * Test set/get object name.
     *
     * @return null
     */
    public function testSetGetObjectName()
    {
        $sClassName = 'sClassName';
        $oTestObject = oxNew('oxAccessRightException');
        $oTestObject->setObjectName($sClassName);
        $this->assertEquals($sClassName, $oTestObject->getObjectName());
    }

    /**
     * Test error message.
     *
     * We check on class name and message only - rest is not checked yet
     *
     * @return null
     */
    public function testToString()
    {
        $sMsg = 'Erik was here..';
        $oTestObject = oxNew('oxAccessRightException', $sMsg);
        $this->assertEquals('OxidEsales\EshopEnterprise\Core\Exception\AccessRightException', get_class($oTestObject));

        $sClassName = 'ClassName';
        $oTestObject->setObjectName($sClassName);
        $sStringOut = $oTestObject->getString();
        $this->assertContains($sMsg, $sStringOut); // Message
        $this->assertContains('OxidEsales\EshopEnterprise\Core\Exception\AccessRightException', $sStringOut); // Exception class name
        $this->assertContains($sClassName, $sStringOut); // Object name
    }

    /**
     * Test get values.
     *
     * @return null
     */
    public function testGetValues()
    {
        $oTestObject = oxNew('oxAccessRightException');
        $sClassName = 'sClassName';
        $oTestObject->setObjectName($sClassName);
        $aRes = $oTestObject->getValues();
        $this->assertArrayHasKey('object', $aRes);
        $this->assertTrue($sClassName === $aRes['object']);
    }
}
