<?php
/**
 * This Software is the property of OXID eSales and is protected
 * by copyright law - it is NOT Freeware.
 *
 * Any unauthorized use of this software without a valid license key
 * is a violation of the license agreement and will be prosecuted by
 * civil and criminal law.
 *
 * @link      http://www.oxid-esales.com
 * @copyright (C) OXID eSales AG 2003-2016
 * @version   OXID eShop EE
 */

namespace OxidEsales\EshopEnterprise\Tests\Unit\Core;

use \oxDb;
use \oxField;
use \oxTestModules;
use \oxRegistry;

class EmailTest extends \oxUnitTestCase
{
    /**
     * Initialize the fixture.
     *
     * @return null
     */
    protected function setUp()
    {
        parent::setUp();

        // set shop params for testing
        $this->_oShop = oxNew("oxshop");
        $this->_oShop->load($this->getConfig()->getShopId());
        $this->_oShop->oxshops__oxorderemail = new oxField('orderemail@orderemail.nl', oxField::T_RAW);
        $this->_oShop->oxshops__oxordersubject = new oxField('testOrderSubject', oxField::T_RAW);
        $this->_oShop->oxshops__oxsendednowsubject = new oxField('testSendedNowSubject', oxField::T_RAW);
        $this->_oShop->oxshops__oxname = new oxField('testShopName', oxField::T_RAW);
        $this->_oShop->oxshops__oxowneremail = new oxField('shopOwner@shopOwnerEmail.nl', oxField::T_RAW);
        $this->_oShop->oxshops__oxinfoemail = new oxField('shopInfoEmail@shopOwnerEmail.nl', oxField::T_RAW);
        //$this->_oShop->oxshops__oxsmtp = new oxField('localhost', oxField::T_RAW);
        $this->_oShop->oxshops__oxsmtp = new oxField('127.0.0.1', oxField::T_RAW);
        $this->_oShop->oxshops__oxsmtpuser = new oxField('testSmtpUser', oxField::T_RAW);
        $this->_oShop->oxshops__oxsmtppwd = new oxField('testSmtpPassword', oxField::T_RAW);
        $this->_oShop->oxshops__oxregistersubject = new oxField('testUserRegistrationSubject', oxField::T_RAW);
        $this->_oShop->oxshops__oxforgotpwdsubject = new oxField('testUserFogotPwdSubject', oxField::T_RAW);
    }

    /**
     * Tear down the fixture.
     *
     * @return null
     */
    protected function tearDown()
    {
        $oActShop = $this->getConfig()->getActiveShop();
        $oActShop->setLanguage(0);
        oxRegistry::getLang()->setBaseLanguage(0);

        parent::tearDown();
    }

    /**
     * Test sending forgot password to not existing user
     */
    public function testSendForgotPwdEmailToUserInAnotherShopNoMallUsers()
    {
        $this->getConfig()->setConfigParam('blMallUsers', 0);

        oxDb::getDb()->execute(
            "INSERT INTO  `oxuser` (
                                            `OXID` , `OXACTIVE` , `OXRIGHTS` , `OXSHOPID` , `OXUSERNAME` ,
                                            `OXPASSWORD` , `OXPASSSALT` , `OXCUSTNR` , `OXUSTID` , `OXUSTIDSTATUS` ,
                                            `OXCOMPANY` , `OXFNAME` , `OXLNAME` , `OXSTREET` , `OXSTREETNR` ,`OXADDINFO` ,
                                            `OXCITY` ,`OXCOUNTRYID` ,`OXZIP` , `OXFON` , `OXFAX` , `OXSAL` ,
                                            `OXBONI` , `OXCREATE` , `OXREGISTER` , `OXPRIVFON` , `OXMOBFON` , `OXBIRTHDATE` ,
                                            `OXURL`, `OXLDAPKEY` , `OXWRONGLOGINS` , `OXUPDATEKEY` ,
                                            `OXUPDATEEXP`
                                            )
                                            VALUES (
                                            '_test_another',  '1',  'user',  '5',  'test@testforsubs.com',  'zzz',  '',  '0',  '',  '0',  '',  '',  '',  '',  '',  '',  '',  '',  '',  '',  '',  '',  '0',  '0000-00-00 00:00:00',  '0000-00-00 00:00:00',  '',  '',  '0000-00-00', '',  '',  '0',  '',  '0'
                                            );
                                            "
        );

        $oEmail = $this->getMock('oxEmail', array("_sendMail", "_getShop"));
        $oEmail->expects($this->never())->method('_sendMail');
        $oEmail->expects($this->any())->method('_getShop')->will($this->returnValue($this->_oShop));

        $blRet = $oEmail->sendForgotPwdEmail('test@testforsubs.com');
        $this->assertFalse($blRet, 'Mail was sent to user from another shop');
    }
}
