<?php
/**
 * This Software is the property of OXID eSales and is protected
 * by copyright law - it is NOT Freeware.
 *
 * Any unauthorized use of this software without a valid license key
 * is a violation of the license agreement and will be prosecuted by
 * civil and criminal law.
 *
 * @link      http://www.oxid-esales.com
 * @copyright (C) OXID eSales AG 2003-2016
 * @version   OXID eShop EE
 */

namespace OxidEsales\EshopEnterprise\Tests\Unit\Core;

use OxidEsales\TestingLibrary\UnitTestCase;

/**
 * Tests online request.
 *
 * @covers oxOnlineRequest
 */
class OnlineRequestTest extends UnitTestCase
{
    public function testClusterIdTakenFromBaseShopInsteadOfSubShop()
    {
        $config = $this->getConfig();
        $config->saveShopConfVar("str", 'sClusterId', 'generated_unique_cluster_id', 1);

        $this->setShopId(9);
        $config->setConfigParam('sClusterId', '');
        $request = oxNew('oxOnlineRequest');
        $this->assertSame('generated_unique_cluster_id', $request->clusterId);
    }
}
