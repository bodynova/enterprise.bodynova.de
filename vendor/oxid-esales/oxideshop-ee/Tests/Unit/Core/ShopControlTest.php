<?php
/**
 * This Software is the property of OXID eSales and is protected
 * by copyright law - it is NOT Freeware.
 *
 * Any unauthorized use of this software without a valid license key
 * is a violation of the license agreement and will be prosecuted by
 * civil and criminal law.
 *
 * @link      http://www.oxid-esales.com
 * @copyright (C) OXID eSales AG 2003-2016
 * @version   OXID eShop EE
 */

namespace OxidEsales\EshopEnterprise\Tests\Unit\Core;

use Exception;
use OxidEsales\EshopEnterprise\Core\DatabaseProvider;
use OxidEsales\EshopEnterprise\Core\Cache\DynamicContent\ContentCache;
use oxOutput;
use oxTestModules;

class ShopControlTest extends \oxUnitTestCase
{
    /**
     * Testing oxShopControl::start()
     *
     * @return null
     */
    public function testStartAccessRightException()
    {
        $this->setRequestParameter('cl', null);
        $this->setRequestParameter('fnc', "testFnc");
        $this->getSession()->setVariable('actshop', null);
        oxTestModules::addFunction('oxUtilsView', 'addErrorToDisplay', '{}');
        oxTestModules::addFunction('oxUtils', 'redirect', '{ throw new Exception("oxAccessRightException"); }');

        $oConfig = $this->getMock("oxConfig", array("isMall", "getConfigParam", "getShopId", "getShopHomeUrl"));
        $oConfig->expects($this->at(0))->method('isMall')->will($this->returnValue(true));
        $oConfig->expects($this->at(1))->method('getShopId')->will($this->returnValue(999));
        $oConfig->expects($this->at(2))->method('getShopHomeUrl');

        $oControl = $this->getMock("oxShopControl", array("getConfig", "_runOnce", "isAdmin", "_process", '_isDebugMode'), array(), '', false);
        $oControl->expects($this->atLeastOnce())->method('getConfig')->will($this->returnValue($oConfig));
        $oControl->expects($this->once())->method('_runOnce');
        $oControl->expects($this->atLeastOnce())->method('isAdmin')->will($this->returnValue(false));
        $oControl->expects($this->once())->method('_process')->with($this->equalTo("start"), $this->equalTo("testFnc"))->will($this->throwException(oxNew('oxSystemComponentException')));
        $oControl->expects($this->any())->method('_isDebugMode')->will($this->returnValue(false));

        try {
            $oControl->start();
        } catch (Exception $oExcp) {
            $this->assertEquals("oxAccessRightException", $oExcp->getMessage(), "Error while executing testStartAccessRightException()");

            return;
        }
        $this->fail("Error while executing testStartAccessRightException()");
    }

    /**
     * Testing oxShopControl::_process()
     */
    public function testProcess()
    {
        oxTestModules::addFunction('oxCache', 'isViewCacheable', '{ return true; }');
        oxTestModules::addFunction('oxCache', 'get', '{ return false; }');
        oxTestModules::addFunction('oxReverseProxyHeader', 'sendHeader', '{}');
        oxTestModules::addFunction('oxUtils', 'isSearchEngine', '{ return false; }');
        oxTestModules::addFunction('oxUtils', 'setHeader', '{}');

        $sTplPath = $this->getConfig()->getConfigParam('sShopDir') . "/Application/views/";
        $sTplPath .= $this->getConfig()->getConfigParam('sTheme') . "/tpl/page/checkout/basket.tpl";

        $oConfig = $this->getMock("oxConfig", array("getTemplatePath", "getConfigParam", "pageClose"));
        $map = [
            ['blLogging', null, true],
            ['blUseContentCaching', null, true]
        ];
        $oConfig->expects($this->any())->method('getConfigParam')->will($this->returnValueMap($map));
        $oConfig->expects($this->any())->method('getTemplatePath')->will($this->returnValue($sTplPath));

        $aTasks = array("isAdmin", "_log", "_startMonitor", "getConfig", "_stopMonitor", '_getOutputManager', '_executeMaintenanceTasks');

        $oRights = $this->getMock("oxRights", array("processView"));
        $oRights->expects($this->once())->method('processView');
        $aTasks[] = "getRights";

        $oOut = $this->getMock("oxOutput", array('output', 'flushOutput', 'sendHeaders'));
        $oOut->expects($this->once())->method('output')->with($this->equalTo('content'));
        $oOut->expects($this->once())->method('flushOutput')->will($this->returnValue(null));
        $oOut->expects($this->once())->method('sendHeaders')->will($this->returnValue(null));

        $oSmarty = $this->getMock("Smarty", array('fetch'));
        $oSmarty->expects($this->once())->method('fetch')->with($this->equalTo("page/info/content.tpl"));

        $oUtilsView = $this->getMock("oxUtilsView", array('getSmarty'));
        $oUtilsView->expects($this->any())->method('getSmarty')->will($this->returnValue($oSmarty));
        oxTestModules::addModuleObject('oxUtilsView', $oUtilsView);

        $oControl = $this->getMock("oxShopControl", $aTasks, array(), '', false);
        $oControl->expects($this->any())->method('getConfig')->will($this->returnValue($oConfig));
        $oControl->expects($this->any())->method('isAdmin')->will($this->returnValue(false));
        $oControl->expects($this->any())->method('_getOutputManager')->will($this->returnValue($oOut));
        $oControl->expects($this->atLeastOnce())->method('_executeMaintenanceTasks');

        $oControl->expects($this->once())->method('getRights')->will($this->returnValue($oRights));

        $oControl->UNITprocess("content", null);
    }

    public function testProcessJson()
    {
        oxTestModules::addFunction('oxcache', 'isViewCacheable', '{ return true; }');
        oxTestModules::addFunction('oxcache', 'get', '{ return false; }');
        oxTestModules::addFunction('oxReverseProxyHeader', 'sendHeader', '{}');
        oxTestModules::addFunction('oxUtils', 'isSearchEngine', '{ return false; }');
        oxTestModules::addFunction('oxUtils', 'setHeader', '{}');

        $this->setRequestParameter('renderPartial', 'asd');

        $sTplPath = $this->getConfig()->getConfigParam('sShopDir') . "/Application/views/";
        $sTplPath .= $this->getConfig()->getConfigParam('sTheme') . "/tpl/page/checkout/basket.tpl";

        $oConfig = $this->getMock("oxConfig", array("getTemplatePath", "getConfigParam", "pageClose"));
        $map = [
            ['blLogging', null, true],
            ['blUseContentCaching', null, true]
        ];
        $oConfig->expects($this->any())->method('getConfigParam')->will($this->returnValueMap($map));
        $oConfig->expects($this->any())->method('getTemplatePath')->will($this->returnValue($sTplPath));

        $aTasks = array("isAdmin", "_log", "_startMonitor", "getConfig", "_stopMonitor", '_getOutputManager', '_getErrors', '_executeMaintenanceTasks');

        $oRights = $this->getMock("oxRights", array("processView"));
        $oRights->expects($this->once())->method('processView');
        $aTasks[] = "getRights";

        $oOut = $this->getMock("oxOutput", array('output', 'flushOutput', 'sendHeaders', 'setOutputFormat'));
        $oOut->expects($this->at(0))->method('setOutputFormat')->with($this->equalTo(oxOutput::OUTPUT_FORMAT_JSON));
        $oOut->expects($this->at(1))->method('sendHeaders')->will($this->returnValue(null));
        $oOut->expects($this->at(3))->method('output')->with($this->equalTo('content'), $this->anything());
        $oOut->expects($this->at(4))->method('flushOutput')->will($this->returnValue(null));

        $oSmarty = $this->getMock("Smarty", array('fetch'));
        $oSmarty->expects($this->once())->method('fetch')->with($this->equalTo("page/info/content.tpl"));

        $oUtilsView = $this->getMock("oxUtilsView", array('getSmarty'));
        $oUtilsView->expects($this->any())->method('getSmarty')->will($this->returnValue($oSmarty));
        oxTestModules::addModuleObject('oxUtilsView', $oUtilsView);

        $oControl = $this->getMock("oxShopControl", $aTasks, array(), '', false);
        $oControl->expects($this->any())->method('getConfig')->will($this->returnValue($oConfig));
        $oControl->expects($this->any())->method('isAdmin')->will($this->returnValue(false));
        $oControl->expects($this->any())->method('_getOutputManager')->will($this->returnValue($oOut));
        $oControl->expects($this->any())->method('_getErrors')->will($this->returnValue(array()));
        $oControl->expects($this->atLeastOnce())->method('_executeMaintenanceTasks');
        $oControl->expects($this->once())->method('getRights')->will($this->returnValue($oRights));

        $oControl->UNITprocess("content", null);
    }


    public function testProcessJsonWithErrors()
    {
        oxTestModules::addFunction('oxcache', 'isViewCacheable', '{ return true; }');
        oxTestModules::addFunction('oxcache', 'get', '{ return false; }');
        oxTestModules::addFunction('oxReverseProxyHeader', 'sendHeader', '{}');
        oxTestModules::addFunction('oxUtils', 'isSearchEngine', '{ return false; }');
        oxTestModules::addFunction('oxUtils', 'setHeader', '{}');

        $this->setRequestParameter('renderPartial', 'asd');

        $sTplPath = $this->getConfig()->getConfigParam('sShopDir') . "/Application/views/";
        $sTplPath .= $this->getConfig()->getConfigParam('sTheme') . "/tpl/page/checkout/basket.tpl";

        $oConfig = $this->getMock("oxConfig", array("getTemplatePath", "getConfigParam", "pageClose"));
        $map = [
            ['blLogging', null, true],
            ['blUseContentCaching', null, true]
        ];
        $oConfig->expects($this->any())->method('getConfigParam')->will($this->returnValueMap($map));
        $oConfig->expects($this->any())->method('getTemplatePath')->will($this->returnValue($sTplPath));

        $aTasks = array("isAdmin", "_log", "_startMonitor", "getConfig", "_stopMonitor", '_getOutputManager', '_getErrors', '_executeMaintenanceTasks');
        $oRights = $this->getMock("oxRights", array("processView"));
        $oRights->expects($this->once())->method('processView');
        $aTasks[] = "getRights";

        $oOut = $this->getMock("oxOutput", array('output', 'flushOutput', 'sendHeaders', 'setOutputFormat'));
        $oOut->expects($this->at(0))->method('setOutputFormat')->with($this->equalTo(oxOutput::OUTPUT_FORMAT_JSON));
        $oOut->expects($this->at(1))->method('output')->with(
            $this->equalTo('errors'),
            $this->equalTo(
                array(
                    'other' => array('test1', 'test3'),
                    'default' => array('test2', 'test4'),
                )
            )
        );
        $oOut->expects($this->at(2))->method('sendHeaders')->will($this->returnValue(null));
        $oOut->expects($this->at(3))->method('output')->with($this->equalTo('content'), $this->anything());
        $oOut->expects($this->at(4))->method('flushOutput')->will($this->returnValue(null));

        $oSmarty = $this->getMock("Smarty", array('fetch'));
        $oSmarty->expects($this->once())->method('fetch')->with($this->equalTo("page/info/content.tpl"));

        $oUtilsView = $this->getMock("oxUtilsView", array('getSmarty'));
        $oUtilsView->expects($this->any())->method('getSmarty')->will($this->returnValue($oSmarty));
        oxTestModules::addModuleObject('oxUtilsView', $oUtilsView);

        $oControl = $this->getMock("oxShopControl", $aTasks, array(), '', false);
        $oControl->expects($this->any())->method('getConfig')->will($this->returnValue($oConfig));
        $oControl->expects($this->any())->method('isAdmin')->will($this->returnValue(false));
        $oControl->expects($this->any())->method('_getOutputManager')->will($this->returnValue($oOut));
        $oControl->expects($this->atLeastOnce())->method('_executeMaintenanceTasks');
        $aErrors = array();
        $oDE = oxNew('oxDisplayError');
        $oDE->setMessage('test1');
        $aErrors['other'][] = serialize($oDE);
        $oDE->setMessage('test2');
        $aErrors['default'][] = serialize($oDE);
        $oDE->setMessage('test3');
        $aErrors['other'][] = serialize($oDE);
        $oDE->setMessage('test4');
        $aErrors['default'][] = serialize($oDE);

        $oControl->expects($this->any())->method('_getErrors')->will($this->returnValue($aErrors));
        $oControl->expects($this->once())->method('getRights')->will($this->returnValue($oRights));

        $oControl->UNITprocess("content", null);
    }

    /**
     * Testing oxShopControl::_process()
     */
    public function testProcessCanCacheIsChecked()
    {
        oxTestModules::addFunction('oxcache', 'isViewCacheable', '{ return true; }');
        oxTestModules::addFunction('content', 'canCache', '{ return true; }');
        oxTestModules::addFunction('content', 'setIsCallForCache', '{ throw new Exception("setIsCallForCache"); }');
        oxTestModules::addFunction('content', 'init', '{ throw new Exception("init"); }');

        oxTestModules::addFunction('oxUtils', 'isSearchEngine', '{ return false; }');
        oxTestModules::addFunction('oxUtils', 'setHeader', '{}');

        $sTplPath = $this->getConfig()->getConfigParam('sShopDir') . "/Application/views/";
        $sTplPath .= $this->getConfig()->getConfigParam('sTheme') . "/tpl/help.tpl";

        $oConfig = $this->getMock("oxConfig", array("getTemplatePath", "getConfigParam", "pageClose"));
        $map = [
            ['blLogging', null, true],
            ['blUseContentCaching', null, true]
        ];
        $oConfig->expects($this->any())->method('getConfigParam')->will($this->returnValueMap($map));
        $oConfig->expects($this->any())->method('getTemplatePath')->will($this->returnValue($sTplPath));

        $aTasks = array("isAdmin", "_log", "_startMonitor", "getConfig", "_stopMonitor", '_executeMaintenanceTasks');
        $oRights = $this->getMock("oxRights", array("processView"));
        $oRights->expects($this->any())->method('processView');
        $aTasks[] = "getRights";

        $oControl = $this->getMock("oxShopControl", $aTasks, array(), '', false);
        $oControl->expects($this->any())->method('getConfig')->will($this->returnValue($oConfig));
        $oControl->expects($this->any())->method('isAdmin')->will($this->returnValue(false));
        $oControl->expects($this->any())->method('getRights')->will($this->returnValue($oRights));
        $oControl->expects($this->atLeastOnce())->method('_executeMaintenanceTasks');

        $this->setExpectedException('Exception', 'setIsCallForCache');
        $oControl->UNITprocess("content", null);
    }

    /**
     * Testing oxShopControl::_process()
     */
    public function testProcessDoNotCacheOnError()
    {
        oxTestModules::addFunction('oxcache', 'isViewCacheable', '{ return true; }');
        oxTestModules::addFunction('content', 'canCache', '{ return true; }');
        oxTestModules::addFunction('content', 'getIsCallForCache', '{ return true; }');
        // Method setIsCallForCache() must be called with false.
        oxTestModules::addFunction('content', 'setIsCallForCache($blCache)', '{ if(!$blCache) { throw new Exception("setIsCallForCache"); } }');
        oxTestModules::addFunction('content', 'initCacheableComponents', '{ throw new Exception("initCacheableComponents"); }');
        oxTestModules::addFunction('content', 'render', '{ throw new Exception("render"); }');
        oxTestModules::addFunction('content', 'init', '{ $this->getSession()->setVariable("Errors", array("default"=>"test")); }');

        oxTestModules::addFunction('oxUtils', 'isSearchEngine', '{ return false; }');
        oxTestModules::addFunction('oxUtils', 'setHeader', '{}');

        $sTplPath = $this->getConfig()->getConfigParam('sShopDir') . "/Application/views/";
        $sTplPath .= $this->getConfig()->getConfigParam('sTheme') . "/tpl/help.tpl";

        $oConfig = $this->getMock("oxConfig", array("getTemplatePath", "getConfigParam", "pageClose"));
        $map = [
            ['blLogging', null, true],
            ['blUseContentCaching', null, true]
        ];
        $oConfig->expects($this->any())->method('getConfigParam')->will($this->returnValueMap($map));
        $oConfig->expects($this->any())->method('getTemplatePath')->will($this->returnValue($sTplPath));

        $aTasks = array("isAdmin", "_log", "_startMonitor", "getConfig", "_stopMonitor", '_executeMaintenanceTasks');
        $oRights = $this->getMock("oxRights", array("processView"));
        $oRights->expects($this->any())->method('processView');
        $aTasks[] = "getRights";

        $oControl = $this->getMock("oxShopControl", $aTasks, array(), '', false);
        $oControl->expects($this->any())->method('getConfig')->will($this->returnValue($oConfig));
        $oControl->expects($this->any())->method('isAdmin')->will($this->returnValue(false));
        $oControl->expects($this->any())->method('getRights')->will($this->returnValue($oRights));
        $oControl->expects($this->atLeastOnce())->method('_executeMaintenanceTasks');

        try {
            $oControl->UNITprocess("content", null);
            $this->fail("");
        } catch (Exception $e) {
            // cachable
            $this->assertEquals('setIsCallForCache', $e->getMessage(), $e->getTraceAsString());
        }
    }

    public function testGetCacheManager()
    {
        $oControl = oxNew('oxShopControl');
        $oOut = $oControl->UNITgetCacheManager();
        $this->assertTrue($oOut instanceof ContentCache);
        $oOut1 = $oControl->UNITgetCacheManager();
        $this->assertSame($oOut, $oOut1);
    }

    public function testSetGetAllowCacheInvalidating()
    {
        $oControl = oxNew("oxShopControl");

        $this->assertEquals(true, $oControl->getAllowCacheInvalidating());

        $oControl->setAllowCacheInvalidating(false);
        $this->assertEquals(false, $oControl->getAllowCacheInvalidating());

        $oControl->setAllowCacheInvalidating(true);
        $this->assertEquals(true, $oControl->getAllowCacheInvalidating());
    }

    /**
     * Testing oxShopControl::start()
     */
    public function testStart()
    {
        $this->setRequestParameter('cl', null);
        $this->setRequestParameter('fnc', "testFnc");

        $map = array(
            array('sMallShopURL', null, false),
            array('iMallMode', null, 1),
        );
        $config = $this->getMock("oxConfig", array("isMall", "getConfigParam"));
        $config->expects($this->any())->method('isMall')->will($this->returnValue(true));
        $config->expects($this->any())->method('getConfigParam')->will($this->returnValueMap($map));

        $control = $this->getMock("oxShopControl", array("getConfig", "_runOnce", "isAdmin", "_process", 'fetchActiveShopCount'), array(), '', false);
        $control->expects($this->any())->method('getConfig')->will($this->returnValue($config));
        $control->expects($this->any())->method('isAdmin')->will($this->returnValue(false));
        $control->expects($this->any())->method('fetchActiveShopCount')->will($this->returnValue(2));
        $control->expects($this->once())->method('_process')->with($this->equalTo("mallstart"), $this->equalTo("testFnc"));

        $control->start();
    }
}
