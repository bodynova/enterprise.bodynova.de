<?php
/**
 * This Software is the property of OXID eSales and is protected
 * by copyright law - it is NOT Freeware.
 *
 * Any unauthorized use of this software without a valid license key
 * is a violation of the license agreement and will be prosecuted by
 * civil and criminal law.
 *
 * @link      http://www.oxid-esales.com
 * @copyright (C) OXID eSales AG 2003-2016
 * @version   OXID eShop EE
 */

namespace OxidEsales\EshopEnterprise\Tests\Unit\Core;

use OxidEsales\EshopCommunity\Core\Edition\EditionPathProvider;
use OxidEsales\EshopCommunity\Core\Edition\EditionRootPathProvider;
use OxidEsales\EshopCommunity\Core\Edition\EditionSelector;

class UtilsViewTest extends \oxUnitTestCase
{
    /**
     * Initialize the fixture.
     *
     * @return null
     */

    public function testFillCommonSmartyPropertiesANDSmartyCompileCheckDemoShop()
    {
        $config = oxNew('oxConfig');

        $config->setConfigParam('iDebug', 1);
        $config->setConfigParam('blDemoShop', 1);

        $tplDir = $config->getTemplateDir($config->isAdmin());

        $templatesDirectories = array();
        if ($tplDir) {
            $templatesDirectories[] = $tplDir;
        }

        $tplDir = $config->getOutDir() . $config->getConfigParam('sTheme') . "/tpl/";
        if ($tplDir && !in_array($tplDir, $templatesDirectories)) {
            $templatesDirectories[] = $tplDir;
        }

        $vfsStreamWrapper = $this->getVfsStreamWrapper();
        $vfsStreamWrapper->createStructure(array('tmp_directory' => array()));
        $compileDirectory = $vfsStreamWrapper->getRootPath().'tmp_directory';
        $config->setConfigParam('sCompileDir', $compileDirectory);


        $smarty = $this->getMock('\Smarty', array('register_resource', 'register_prefilter'));
        $smarty->expects($this->once())->method('register_resource')
            ->with(
                $this->equalTo('ox'),
                $this->equalTo(
                    array(
                        'ox_get_template',
                        'ox_get_timestamp',
                        'ox_get_secure',
                        'ox_get_trusted',
                    )
                )
            );
        $smarty->expects($this->once())->method('register_prefilter')
            ->with($this->equalTo('smarty_prefilter_oxblock'));

        $utilsView = oxNew('oxUtilsView');
        $utilsView->setConfig($config);
        $utilsView->UNITfillCommonSmartyProperties($smarty);
        $utilsView->UNITsmartyCompileCheck($smarty);

        $smarty = new \smarty();
        $mockedConfig = $this->getMock('oxConfig', array('isProductiveMode'));
        $mockedConfig->expects($this->once())->method('isProductiveMode')->will($this->returnValue(true));
        $utilsView = oxNew('oxUtilsView');
        $utilsView->setConfig($mockedConfig);
        $utilsView->UNITsmartyCompileCheck($smarty);
        $this->assertFalse($smarty->compile_check);
    }

    public function testGetEditionTemplateDirs()
    {
        $config = $this->getConfig();
        $shopPath = rtrim($config->getConfigParam('sShopDir'), '/') . '/';

        $enterprisePathProvider = new EditionPathProvider(new EditionRootPathProvider(new EditionSelector(EditionSelector::ENTERPRISE)));
        $professionalPathProvider = new EditionPathProvider(new EditionRootPathProvider(new EditionSelector(EditionSelector::PROFESSIONAL)));
        $dirs = array(
            $enterprisePathProvider->getViewsDirectory() . 'azure/tpl/',
            $professionalPathProvider->getViewsDirectory() . 'azure/tpl/',
            $shopPath . 'Application/views/azure/tpl/',
            $shopPath . 'out/azure/tpl/',
        );

        $utilsView = $this->getMock('oxUtilsView', array('isAdmin'));
        $utilsView->expects($this->any())->method('isAdmin')->will($this->returnValue(false));
        $this->assertEquals($dirs, $utilsView->getTemplateDirs());
    }

    public function testGetEditionTemplateDirsForAdmin()
    {
        $config = $this->getConfig();
        $shopPath = rtrim($config->getConfigParam('sShopDir'), '/') . '/';

        $enterprisePathProvider = new EditionPathProvider(new EditionRootPathProvider(new EditionSelector(EditionSelector::ENTERPRISE)));
        $professionalPathProvider = new EditionPathProvider(new EditionRootPathProvider(new EditionSelector(EditionSelector::PROFESSIONAL)));
        $dirs = array(
            $enterprisePathProvider->getViewsDirectory() . 'admin/tpl/',
            $professionalPathProvider->getViewsDirectory() . 'admin/tpl/',
            $shopPath . 'Application/views/admin/tpl/',
        );

        $utilsView = $this->getMock('oxUtilsView', array('isAdmin'));
        $utilsView->expects($this->any())->method('isAdmin')->will($this->returnValue(true));
        $this->assertEquals($dirs, $utilsView->getTemplateDirs());
    }

    /**
     * oxUtilsView::setTemplateDir() test case
     *
     * @return null
     */
    public function testSetTemplateDir()
    {
        $config = $this->getConfig();
        $shopPath = rtrim($config->getConfigParam('sShopDir'), '/') . '/';

        $enterprisePathProvider = new EditionPathProvider(new EditionRootPathProvider(new EditionSelector(EditionSelector::ENTERPRISE)));
        $professionalPathProvider = new EditionPathProvider(new EditionRootPathProvider(new EditionSelector(EditionSelector::PROFESSIONAL)));
        $dirs = array(
            'testDir1',
            'testDir2',
            $enterprisePathProvider->getViewsDirectory() . 'azure/tpl/',
            $professionalPathProvider->getViewsDirectory() . 'azure/tpl/',
            $shopPath . 'Application/views/azure/tpl/',
            $shopPath . 'out/azure/tpl/',
        );

        //
        $utilsView = $this->getMock("oxUtilsView", array("isAdmin"));
        $utilsView->expects($this->any())->method('isAdmin')->will($this->returnValue(false));
        $utilsView->setTemplateDir("testDir1");
        $utilsView->setTemplateDir("testDir2");
        $utilsView->setTemplateDir("testDir1");

        $this->assertEquals($dirs, $utilsView->getTemplateDirs());
    }

    // non demo mode
    public function testFillCommonSmartyPropertiesANDSmartyCompileCheck()
    {
        $config = oxNew('oxConfig');

        $config->setConfigParam('iDebug', 1);
        $config->setConfigParam('blDemoShop', 0);

        $enterprisePathProvider = new EditionPathProvider(new EditionRootPathProvider(new EditionSelector(EditionSelector::ENTERPRISE)));
        $professionalPathProvider = new EditionPathProvider(new EditionRootPathProvider(new EditionSelector(EditionSelector::PROFESSIONAL)));
        $shopPath = rtrim($config->getConfigParam('sShopDir'), '/') . '/';
        $templatesDirs = array(
            $enterprisePathProvider->getViewsDirectory() . 'azure/tpl/',
            $professionalPathProvider->getViewsDirectory() . 'azure/tpl/',
            $shopPath . 'Application/views/azure/tpl/',
            $shopPath . 'out/azure/tpl/',
        );

        $vfsStreamWrapper = $this->getVfsStreamWrapper();
        $vfsStreamWrapper->createStructure(array('tmp_directory' => array()));
        $compileDirectory = $vfsStreamWrapper->getRootPath().'tmp_directory';
        $config->setConfigParam('sCompileDir', $compileDirectory);

        $plugins = array(
            $enterprisePathProvider->getSmartyPluginsDirectory(),
            $professionalPathProvider->getSmartyPluginsDirectory(),
            $this->getConfigParam('sCoreDir') . 'Smarty/Plugin',
            'plugins'
        );

        $check = array(
            'security'        => false,
            'php_handling'    => (int) $config->getConfigParam('iSmartyPhpHandling'),
            'left_delimiter'  => '[{',
            'right_delimiter' => '}]',
            'caching'         => false,
            'compile_dir'     => $compileDirectory . "/smarty/",
            'cache_dir'       => $compileDirectory . "/smarty/",
            'template_dir'    => $templatesDirs,
            'debugging'       => true,
            'compile_check'   => true,
            'plugins_dir'     => $plugins
        );


        $smarty = $this->getMock('\Smarty', array('register_resource'));
        $smarty->expects($this->once())->method('register_resource');

        $utilsView = oxNew('oxutilsview');
        $utilsView->setConfig($config);
        $utilsView->UNITfillCommonSmartyProperties($smarty);
        $utilsView->UNITsmartyCompileCheck($smarty);

        foreach ($check as $varName => $varValue) {
            $this->assertTrue(isset($smarty->$varName));
            $this->assertEquals($varValue, $smarty->$varName, $varName);
        }
    }
}
