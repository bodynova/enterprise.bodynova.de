<?php
/**
 * This Software is the property of OXID eSales and is protected
 * by copyright law - it is NOT Freeware.
 *
 * Any unauthorized use of this software without a valid license key
 * is a violation of the license agreement and will be prosecuted by
 * civil and criminal law.
 *
 * @link      http://www.oxid-esales.com
 * @copyright (C) OXID eSales AG 2003-2016
 * @version   OXID eShop EE
 */

namespace OxidEsales\EshopEnterprise\Tests\Unit\Core;

use \oxConfig;
use \oxLang;
use PHPUnit_Framework_MockObject_MockObject as MockObject;
use \oxTestModules;

class Unit_Core_oxViewNameGeneratorTest extends \oxUnitTestCase
{
    public function testMultiShopTableViewNameGenerationWhenDefaultShopIdIsUsed()
    {
        /** @var oxConfig|MockObject $config */
        $config = $this->getMock('oxConfig', array('getShopId', 'isMall'));
        $config->expects($this->any())->method('getShopId')->will($this->returnValue(2));
        $config->expects($this->any())->method('isMall')->will($this->returnValue(true));
        $config->setConfigParam('aMultiShopTables', array('test_table1', 'test_table2'));

        /** @var oxLang|MockObject $language */
        $language = $this->getMock('oxLang', array('getMultiLangTables', 'getBaseLanguage', 'getLanguageAbbr'));
        $language->expects($this->any())->method('getMultiLangTables')->will($this->returnValue(array()));

        $viewNameGenerator = oxNew('oxTableViewNameGenerator', $config, $language);
        $this->assertEquals('oxv_test_table1_2', $viewNameGenerator->getViewName('test_table1'));
    }

    public function testLanguageTableViewNameGenerationWhenLanguageIsPassed()
    {
        /** @var oxConfig|MockObject $config */
        $config = $this->getMock('oxConfig', array('getShopId', 'isMall'));
        $config->expects($this->any())->method('getShopId')->will($this->returnValue(1));
        $config->expects($this->any())->method('isMall')->will($this->returnValue(true));
        $config->setConfigParam('aMultiShopTables', array('test_table1', 'test_table2'));

        /** @var oxLang|MockObject $language */
        $language = $this->getMock('oxLang', array('getMultiLangTables', 'getBaseLanguage', 'getLanguageAbbr'));
        $language->expects($this->any())->method('getMultiLangTables')->will($this->returnValue(array()));

        $viewNameGenerator = oxNew('oxTableViewNameGenerator', $config, $language);
        $this->assertEquals('oxv_test_table1_2', $viewNameGenerator->getViewName('test_table1', 1, 2));
    }
}
