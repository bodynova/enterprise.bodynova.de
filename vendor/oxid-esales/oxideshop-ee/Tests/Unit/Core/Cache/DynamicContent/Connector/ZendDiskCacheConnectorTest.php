<?php
/**
 * This Software is the property of OXID eSales and is protected
 * by copyright law - it is NOT Freeware.
 *
 * Any unauthorized use of this software without a valid license key
 * is a violation of the license agreement and will be prosecuted by
 * civil and criminal law.
 *
 * @link      http://www.oxid-esales.com
 * @copyright (C) OXID eSales AG 2003-2016
 * @version   OXID eShop EE
 */
/**
 * Register functions to global namespace.
 */
namespace {
    function zend_disk_cache_fetch($sId)
    {
        return "zend_disk_cache_fetch($sId)";
    }

    function zend_disk_cache_store($sId, $sContent, $iTtl)
    {
        return "zend_disk_cache_store( $sId, $sContent, $iTtl )";
    }

    function zend_disk_cache_delete($sId)
    {
        return "zend_disk_cache_delete( $sId )";
    }

    function zend_disk_cache_clear()
    {
        throw new Exception("zend_disk_cache_clear( )");
    }
}

/**
 * Actual test case namespace.
 */
namespace OxidEsales\EshopEnterprise\Tests\Unit\Core\Cache\DynamicContent\Connector {

    use OxidEsales\EshopEnterprise\Application\Model\Contract\CacheBackendInterface;
    use OxidEsales\EshopEnterprise\Core\Cache\DynamicContent\Connector\ZendDiskCacheConnector;
    use \Exception;

    use \PHPUnit_Framework_MockObject_MockObject as MockObject;
    use \oxTestModules;

    class ZendDiskCacheConnectorTest extends \oxUnitTestCase
    {
        public function testCacheSetTTL()
        {
            oxTestModules::addFunction('oxCacheBackendZSDisk', 'getVar($var)', '{return $this->$var;}');
            $o = oxNew('oxCacheBackendZSDisk');
            $o->cacheSetTTL(64);
            $this->assertEquals(64, $o->getVar('_iTtl'));
        }

        public function testCachePut()
        {
            $o = oxNew('oxCacheBackendZSDisk');
            $o->cacheSetTTL(61);
            $this->assertEquals('zend_disk_cache_store( $sId, $sContent, 61 )', $o->cachePut('$sId', '$sContent'));
        }

        public function testCacheGet()
        {
            $o = oxNew('oxCacheBackendZSDisk');
            $o->cacheSetTTL(61);
            $this->assertEquals('zend_disk_cache_fetch($sId)', $o->cacheGet('$sId'));
        }

        public function testCacheRemoveKey()
        {
            $o = oxNew('oxCacheBackendZSDisk');
            $o->cacheSetTTL(61);
            $this->assertEquals('zend_disk_cache_delete( $sId )', $o->cacheRemoveKey('$sId'));
        }

        public function testCacheClear()
        {
            $o = oxNew('oxCacheBackendZSDisk');
            $o->cacheSetTTL(61);
            try {
                $this->assertEquals(null, $o->cacheClear());
            } catch (Exception $e) {
                $this->assertEquals('zend_disk_cache_clear( )', $e->getMessage());

                return;
            }
            $this->fail("exception lost");
        }

        public function testisAvailable()
        {
            $this->assertTrue(ZendDiskCacheConnector::isAvailable());
        }

        public function testisRightInstance()
        {
            $o = new ZendDiskCacheConnector();
            $this->assertTrue($o instanceof CacheBackendInterface);
        }
    }
}
