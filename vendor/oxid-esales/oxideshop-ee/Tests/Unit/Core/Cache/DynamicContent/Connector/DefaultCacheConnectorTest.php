<?php
/**
 * This Software is the property of OXID eSales and is protected
 * by copyright law - it is NOT Freeware.
 *
 * Any unauthorized use of this software without a valid license key
 * is a violation of the license agreement and will be prosecuted by
 * civil and criminal law.
 *
 * @link      http://www.oxid-esales.com
 * @copyright (C) OXID eSales AG 2003-2016
 * @version   OXID eShop EE
 */
namespace OxidEsales\EshopEnterprise\Tests\Unit\Core\Cache\DynamicContent\Connector;

use OxidEsales\EshopEnterprise\Application\Model\Contract\CacheBackendInterface;
use OxidEsales\EshopEnterprise\Core\Cache\DynamicContent\Connector\DefaultCacheConnector;
use \oxDb;

use \PHPUnit_Framework_MockObject_MockObject as MockObject;
use \oxTestModules;

class DefaultCacheConnectorTest extends \oxUnitTestCase
{

    protected function setUp()
    {
        parent::setUp();
    }

    /**
     * Tear down the fixture.
     *
     * @return null
     */
    protected function tearDown()
    {
        // cleaning up cache table
        oxDb::getDb()->execute('delete from oxcache');

        // removing cache files
        $sFilePath = $this->getConfig()->getConfigParam('sCompileDir') . "/_*.cache";
        $aPathes = glob($sFilePath);
        if (is_array($aPathes)) {
            foreach ($aPathes as $sFilename) {
                // delete all the files
                @unlink($sFilename);
            }
        }
        parent::tearDown();
    }

    /**
     * test if the cache getter works right. if the cache key doesn't exist,
     * no value have to returned.
     *
     * @return null
     */
    public function testCachePutAndCacheGet()
    {
        $oCache = new DefaultCacheConnector();
        $oCache->cachePut('xxx', 'yyy');

        $this->assertFalse($oCache->cacheGet('yyy'));
        $this->assertEquals('yyy', $oCache->cacheGet('xxx'));
    }

    /**
     * test if cache remover works fine.
     *
     * @return null
     */
    public function testCacheRemoveKey()
    {
        $oCache = new DefaultCacheConnector();
        $oCache->cachePut('xxx', 'yyy');

        $this->assertEquals('yyy', $oCache->cacheGet('xxx'));

        $oCache->cacheRemoveKey('xxx');
        $this->assertFalse($oCache->cacheGet('xxx'));
    }

    /**
     * test if cache is available.
     *
     * @return null
     */
    public function testisAvailable()
    {
        $this->assertTrue(DefaultCacheConnector::isAvailable());
    }

    /**
     * test if an right instance will be created.
     *
     * @return null
     */
    public function testisRightInstance()
    {
        $o = new DefaultCacheConnector();
        $this->assertTrue($o instanceof CacheBackendInterface);
    }
}
