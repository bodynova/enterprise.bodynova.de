<?php
/**
 * This Software is the property of OXID eSales and is protected
 * by copyright law - it is NOT Freeware.
 *
 * Any unauthorized use of this software without a valid license key
 * is a violation of the license agreement and will be prosecuted by
 * civil and criminal law.
 *
 * @link      http://www.oxid-esales.com
 * @copyright (C) OXID eSales AG 2003-2016
 * @version   OXID eShop EE
 */
namespace OxidEsales\EshopEnterprise\Tests\Unit\Core\Cache\Generic;

use \PHPUnit_Framework_MockObject_MockObject as MockObject;
use \oxTestModules;

/**
 * Tests for oxReverseProxyHeader class
 */
class ReverseProxyHeaderTest extends \oxUnitTestCase
{
    /**
     * oxReverseProxyHeader::setCacheAge() test case.
     * test if cache age is set correctly.
     *
     * @return null
     */
    public function testSetCacheAge()
    {
        $oHeader = oxNew('oxReverseProxyHeader');
        $oHeader->setCacheAge(59);
        $this->assertEquals(array("Cache-Control: s-maxage=59;" . "\r\n"), $oHeader->getHeader(), 'Cache age header  was NOT formated correctly.');
    }

    /**
     * oxReverseProxyHeader::setNonCacheable() test case.
     * test if no cache header formated correctly.
     *
     * @return null
     */
    public function testSetNonCacheable()
    {
        $oHeader = oxNew('oxReverseProxyHeader');
        $oHeader->setNonCacheable();
        $this->assertEquals(array("Cache-Control: no-cache;" . "\r\n"), $oHeader->getHeader(), 'Cache header was NOT formated correctly.');
    }

    /**
     * oxReverseProxyHeader::setNoStaleData() test case.
     * test if no stale data header formated correctly.
     *
     * @return null
     */
    public function testSetNoStaleData()
    {
        $oHeader = oxNew('oxReverseProxyHeader');
        $oHeader->setNoStaleData();
        $this->assertEquals(array("x-no-stale-data: 1;" . "\r\n"), $oHeader->getHeader(), 'No stale data header was NOT formated correctly.');
    }

    /**
     * oxReverseProxyHeader::setSurrogateHeader() test case.
     * test if header for surrogate is formated correctly.
     *
     * @return null
     */
    public function testSetSurrogateHeader()
    {
        $oHeader = oxNew('oxReverseProxyHeader');
        $oHeader->setSurrogateHeader();
        $this->assertEquals(array('Surrogate-Control: content="ESI/1.0;"' . "\r\n"), $oHeader->getHeader(), 'Surrogate header was NOT formated correctly.');
    }

    /**
     * oxReverseProxyHeader::setSurrogateHeader() test case.
     * test if header for surrogate is formated correctly.
     *
     * @return null
     */
    public function testSetVaryLanguageHeader()
    {
        $oHeader = oxNew('oxReverseProxyHeader');
        $oHeader->setVaryLanguageHeader();
        $this->assertEquals(array("Vary: X-UA-Language" . "\r\n"), $oHeader->getHeader());
    }

}
