<?php
/**
 * This Software is the property of OXID eSales and is protected
 * by copyright law - it is NOT Freeware.
 *
 * Any unauthorized use of this software without a valid license key
 * is a violation of the license agreement and will be prosecuted by
 * civil and criminal law.
 *
 * @link      http://www.oxid-esales.com
 * @copyright (C) OXID eSales AG 2003-2016
 * @version   OXID eShop EE
 */
namespace OxidEsales\EshopEnterprise\Tests\Unit\Core\Cache\Generic;

use \oxRegistry;

use \PHPUnit_Framework_MockObject_MockObject as MockObject;
use \oxTestModules;

class ReverseProxyUrlPartsToFlushTest extends \oxUnitTestCase
{
    public function testGetsHostForActiveSubshop()
    {
        $shopHost = 'testHost';
        $this->createAndSetToRegistryUtilsUrlMock($shopHost);

        $this->setConfigParam('reverseProxyFlushStrategy', 1);
        $urlPartsToFlush = oxNew('oxReverseProxyUrlPartsToFlush');

        $this->assertEquals($shopHost, $urlPartsToFlush->getHost());
    }

    public function testGetsHostWhenNeedToFlushAllSubshops()
    {
        $shopHost = 'testHost';
        $this->createAndSetToRegistryUtilsUrlMock($shopHost);
        $this->setConfigParam('reverseProxyFlushStrategy', 0);
        $urlPartsToFlush = oxNew('oxReverseProxyUrlPartsToFlush');

        $this->assertEquals('.*', $urlPartsToFlush->getHost());
    }

    /**
     * @param string $shopHost
     *
     * @return string
     */
    private function createAndSetToRegistryUtilsUrlMock($shopHost)
    {
        $oxUtilsURL = $this->getMock('oxUtilsUrl');
        $oxUtilsURL->expects($this->any())->method('getActiveShopHost')->will($this->returnValue($shopHost));

        oxRegistry::set('oxUtilsUrl', $oxUtilsURL);
    }

    public function testGetsPathForActiveSubshop()
    {
        $shopPath = 'testPath';
        $this->createAndSetToRegistryUtilsUrlMockWithUrlPath($shopPath);

        $this->setConfigParam('reverseProxyFlushStrategy', 1);
        $urlPartsToFlush = oxNew('oxReverseProxyUrlPartsToFlush');

        $this->assertEquals($shopPath, $urlPartsToFlush->getPath());
    }

    public function testGetsPathWhenNeedToFlushAllSubshops()
    {
        $shopPath = 'testPath';
        $this->createAndSetToRegistryUtilsUrlMockWithUrlPath($shopPath);
        $this->setConfigParam('reverseProxyFlushStrategy', 0);
        $urlPartsToFlush = oxNew('oxReverseProxyUrlPartsToFlush');

        $this->assertEquals('', $urlPartsToFlush->getPath());
    }

    /**
     * @param string $urlPath
     *
     * @return string
     */
    private function createAndSetToRegistryUtilsUrlMockWithUrlPath($urlPath)
    {
        $oxUtilsURL = $this->getMock('oxUtilsUrl', array('getActiveShopUrlPath'));
        $oxUtilsURL->expects($this->any())->method('getActiveShopUrlPath')->will($this->returnValue($urlPath));

        oxRegistry::set('oxUtilsUrl', $oxUtilsURL);
    }
}
