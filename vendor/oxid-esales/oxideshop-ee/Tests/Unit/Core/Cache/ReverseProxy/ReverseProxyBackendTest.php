<?php
/**
 * This Software is the property of OXID eSales and is protected
 * by copyright law - it is NOT Freeware.
 *
 * Any unauthorized use of this software without a valid license key
 * is a violation of the license agreement and will be prosecuted by
 * civil and criminal law.
 *
 * @link      http://www.oxid-esales.com
 * @copyright (C) OXID eSales AG 2003-2016
 * @version   OXID eShop EE
 */
namespace OxidEsales\EshopEnterprise\Tests\Unit\Core\Cache\Generic;

use OxidEsales\EshopEnterprise\Core\Cache\ReverseProxy\ReverseProxyBackend;
use OxidEsales\EshopEnterprise\Core\Cache\ReverseProxy\ReverseProxyUrlGenerator;
use OxidEsales\EshopEnterprise\Core\Cache\ReverseProxy\ReverseProxyUrlPartsToFlush;
use OxidEsales\EshopEnterprise\Core\Cache\Generic\Connector\ReverseProxyCacheConnector;
use OxidEsales\EshopEnterprise\Core\Config;
use \oxSerial;

use \PHPUnit_Framework_MockObject_MockObject as MockObject;
use \oxTestModules;

/**
 * Tests oxReverseProxyBackEnd
 */
class ReverseProxyBackendTest extends \oxUnitTestCase
{
    /**
     * Sets up oxReverseProxyAccess mock
     *
     * @return null|void
     */
    public function setUp()
    {
        $this->setConfigParam("blReverseProxyActive", true);

        return parent::setUp();
    }

    /**
     * Testing cache connector setting and getting
     */
    public function testSetAndGetConnector()
    {
        $oBackend = oxNew('oxReverseProxyBackend');
        $oBackend->setConnector('connector');
        $this->assertEquals('connector', $oBackend->getConnector());
    }

    /**
     * Test for setting url to be url pool and getting (unique urls)
     */
    public function testSetUrls()
    {
        $oBackend = oxNew('oxReverseProxyBackend');

        $oBackend->set('url1');
        $oBackend->set(array('url2', 'url3'));
        $oBackend->set('url3');
        $oBackend->set(array('url4', 'url2', 'url5'));

        $aUrlPool = $oBackend->getUrlPool();
        $this->assertEquals(5, count($aUrlPool));

        $aExpectedUrls = array('url1', 'url2', 'url3', 'url4', 'url5');

        foreach ($aExpectedUrls as $sUrl) {
            $this->assertTrue(in_array($sUrl, $aUrlPool));
        }
    }

    public function testExecuteSetsHostForFlushing()
    {
        $hostNamesFromUrlPartsToFlush = 'testHostName';

        $urlPartsToFlush = $this->getMock('oxReverseProxyUrlPartsToFlush');
        $urlPartsToFlush->expects($this->any())->method('getHost')->will($this->returnValue($hostNamesFromUrlPartsToFlush));

        /** @var ReverseProxyCacheConnector|MockObject $connector */
        $connector = $this->getMock('oxReverseProxyConnector', array(), array(), '', false);
        $connector->expects($this->atLeastOnce())->method('setShopHost')->with($this->identicalTo($hostNamesFromUrlPartsToFlush));

        $backend = oxNew('oxReverseProxyBackend');
        $backend->setConnector($connector);
        $backend->setUrlPartsToFlush($urlPartsToFlush);
        $backend->set('url1');

        $backend->execute();
    }

    /**
     * Test invalidation execution invalidates all urls.
     */
    public function testInvalidateAllUrls()
    {
        $urlQuery = '.*';
        $urlPath = 'url_path';
        $concatenatedUrl = $urlPath . $urlQuery;

        /** @var ReverseProxyUrlPartsToFlush|MockObject $urlPartsToFlush */
        $urlPartsToFlush = $this->getMock('oxReverseProxyUrlPartsToFlush');
        $urlPartsToFlush->expects($this->atLeastOnce())->method('getPath')->will($this->returnValue($urlPath));

        /** @var ReverseProxyUrlGenerator|MockObject $urlGenerator */
        $urlGenerator = $this->getMock('oxReverseProxyUrlGenerator');
        $urlGenerator->expects($this->atLeastOnce())
            ->method('prependPathToUrlQuery')
            ->with($this->identicalTo($urlPath, $urlQuery))
            ->will($this->returnValue($concatenatedUrl));

        /** @var ReverseProxyCacheConnector|MockObject $connector */
        $connector = $this->getMock('oxReverseProxyConnector', array('invalidate'), array(), '', false);
        $connector->expects($this->atLeastOnce())->method('invalidate')->with($this->identicalTo($concatenatedUrl));

        $backend = oxNew('oxReverseProxyBackend');
        $backend->setConnector($connector);
        $backend->setUrlGenerator($urlGenerator);
        $backend->setUrlPartsToFlush($urlPartsToFlush);
        $backend->setFlush();
        $backend->set('any_different_url');
        $backend->execute();
    }

    public function testExecuteSetsPathForFlushing()
    {
        $urlPath = 'url_path';
        $urlQuery = 'url_query';
        $concatenatedUrl = 'concatenated_url';

        /** @var ReverseProxyUrlPartsToFlush|MockObject $urlPartsToFlush */
        $urlPartsToFlush = $this->getMock('oxReverseProxyUrlPartsToFlush');
        $urlPartsToFlush->expects($this->atLeastOnce())->method('getPath')->will($this->returnValue($urlPath));

        /** @var ReverseProxyUrlGenerator|MockObject $urlGenerator */
        $urlGenerator = $this->getMock('oxReverseProxyUrlGenerator');
        $urlGenerator->expects($this->atLeastOnce())
            ->method('prependPathToUrlQuery')
            ->with($this->identicalTo($urlPath, $urlQuery))
            ->will($this->returnValue($concatenatedUrl));

        /** @var ReverseProxyCacheConnector|MockObject $urlGenerator $connector */
        $connector = $this->getMock('oxReverseProxyConnector', array(), array(), '', false);
        $connector->expects($this->atLeastOnce())->method('invalidate')->with($this->identicalTo($concatenatedUrl));

        $backend = oxNew('oxReverseProxyBackend');
        $backend->setConnector($connector);
        $backend->setUrlPartsToFlush($urlPartsToFlush);
        $backend->setUrlGenerator($urlGenerator);
        $backend->set($urlQuery);

        $backend->execute();
    }

    public function testGetUrlPartsToFlush()
    {
        $oBackend = oxNew('oxReverseProxyBackend');
        $urlPartsToFlush = $oBackend->getUrlPartsToFlush();

        $this->assertInstanceOf('OxidEsales\EshopEnterprise\Core\Cache\ReverseProxy\ReverseProxyUrlPartsToFlush', $urlPartsToFlush);
    }

    public function testGetUrlGenerator()
    {
        $oBackend = oxNew('oxReverseProxyBackend');
        $urlGenerator = $oBackend->getUrlGenerator();

        $this->assertInstanceOf('OxidEsales\EshopEnterprise\Core\Cache\ReverseProxy\ReverseProxyUrlGenerator', $urlGenerator);
    }

    /**
     * Data provider for oxReverseProxyBackEnd::isEnabled().
     */
    public function isEnabled_dataProviderRP()
    {
        $oSerial_enabled = $this->getMock('oxSerial', array("isFlagEnabled"));
        $oSerial_enabled->expects($this->any())->method('isFlagEnabled')->with($this->equalTo("reverse_proxy"))->will($this->returnValue(true));

        $oSerial_disabled = $this->getMock('oxSerial', array("isFlagEnabled"));
        $oSerial_disabled->expects($this->any())->method('isFlagEnabled')->with($this->equalTo("reverse_proxy"))->will($this->returnValue(false));

        return array(
            array($oSerial_enabled, true, true),
            array($oSerial_enabled, false, false),
            array($oSerial_disabled, true, false),
            array($oSerial_disabled, false, false),
        );
    }

    /**
     * oxReverseProxyBackEnd::isEnabled() test case.
     * Check if Reverse Proxy is turned on by configs.
     *
     * @dataProvider isEnabled_dataProviderRP
     *
     * @param bool $oSerial              Serial
     * @param bool $blReverseProxyActive Is reverse proxy active
     * @param bool $blResult             Expected Result
     */
    public function testIsRPEnabled($oSerial, $blReverseProxyActive, $blResult)
    {
        /** @var Config|MockObject $oConfig */
        $oConfig = $this->getMock('oxConfig', array("getSerial"));
        $oConfig->expects($this->any())->method('getSerial')->will($this->returnValue($oSerial));
        $oConfig->getConfigParam("blReverseProxyActive");
        $oConfig->setConfigParam("blReverseProxyActive", $blReverseProxyActive);

        /** @var ReverseProxyBackend|MockObject $oSubj */
        $oSubj = $this->getMock('oxReverseProxyBackend', array("_getConfig"), array(), '', false);
        $oSubj->expects($this->any())->method('_getConfig')->will($this->returnValue($oConfig));

        $this->assertEquals($blResult, $oSubj->isEnabled());
    }

    /**
     * Data provider for oxReverseProxyBackEnd::isActive().
     */
    public function isActive_dataProviderRP()
    {
        return array(
            array(true, true, true),
            array(true, false, false),
            array(false, true, false),
            array(false, false, false),
        );
    }

    /**
     * oxReverseProxyBackEnd::isActive() test case.
     * Check if Reverse Proxy is turned on by configs and esi capability.
     *
     * @dataProvider isActive_dataProviderRP
     *
     * @param bool $blEnabled             Is enabled
     * @param bool $blWithSurrogateHeader Use surrogateHeader
     * @param bool $blResult              Expected result
     */
    public function testIsRPActive($blEnabled, $blWithSurrogateHeader, $blResult)
    {
        /** @var ReverseProxyBackend|MockObject $oSubj */
        $oSubj = $this->getMock('oxReverseProxyBackend', array("isEnabled"), array(), '', false);

        if ($blWithSurrogateHeader) {
            $oSubj->setReverseProxyCapableDoEsi(true);
            $oSubj->expects($this->once())->method('isEnabled')->will($this->returnValue($blEnabled));
        } elseif (isset($_SERVER["HTTP_SURROGATE_CAPABILITY"])) {
            $oSubj->setReverseProxyCapableDoEsi(false);
            $oSubj->expects($this->never())->method('isEnabled');
        }

        $this->assertEquals($blResult, $oSubj->isActive());
    }

    /**
     * oxReverseProxyBackEnd::isActive() test case.
     * Check if value is cached
     */
    public function testIsRPActive_cachingValue()
    {
        /** @var oxSerial|MockObject $serial */
        $serial = $this->getMock('oxSerial', array("isFlagEnabled"));
        $serial->expects($this->once())->method('isFlagEnabled')->with($this->equalTo("reverse_proxy"))->will($this->returnValue(true));

        /** @var Config|MockObject $config */
        $config = $this->getMock('oxConfig', array("getSerial"));
        $config->expects($this->once())->method('getSerial')->will($this->returnValue($serial));
        $config->setConfigParam("blReverseProxyActive", true);

        /** @var ReverseProxyBackend|MockObject $backend */
        $backend = $this->getMock('oxReverseProxyBackend', array("_getConfig"));
        $backend->expects($this->any())->method('_getConfig')->will($this->returnValue($config));

        $backend->setReverseProxyCapableDoEsi(true);

        $this->assertTrue($backend->isActive());

        // value should be cached - no additional call to oxConfig::getSerial
        $this->assertTrue($backend->isActive());
    }

    /**
     * oxReverseProxyBackEnd::setNotActive() test case.
     * Check if value is not active even if otherwise would be active.
     */
    public function  testSetNotActive()
    {
        $this->setConfigParam("blReverseProxyActive", true);

        $oConfig = $this->getMock('oxConfig', array("getSerial"));

        /** @var ReverseProxyBackend|MockObject $oSubj */
        $oSubj = $this->getMock('oxReverseProxyBackend', array("_getConfig"), array(), '', false);
        $oSubj->setReverseProxyCapableDoEsi(true);
        $oSubj->setNotActive();

        $this->assertEquals(false, $oSubj->isActive());
    }

    /**
     * Test invalidation execution set urls
     */
    public function testSetShopHost()
    {
        $oBackend = oxNew('oxReverseProxyBackend');
        $oBackend->setShopHost("testShopHost");
        $this->assertEquals("testShopHost", $oBackend->getConnector()->getShopHost());
    }

    /**
     * oxReverseProxyBackEnd::isReverseProxyCapableDoEsi test case.
     * Check if getter returns what was set in setter.
     */
    public function testIsReverseProxyCapableDoEsi()
    {
        $oSubj = oxNew('oxReverseProxyBackend');
        $oSubj->setReverseProxyCapableDoEsi(true);
        $this->assertEquals(true, $oSubj->isReverseProxyCapableDoEsi());
        $oSubj->setReverseProxyCapableDoEsi(false);
        $this->assertEquals(false, $oSubj->isReverseProxyCapableDoEsi());
    }

    /**
     * oxReverseProxyBackend::setEnvironmentKey test case.
     * test if environment key cookie formed correctly.
     *
     * @return null
     */
    public function testSetEnvironmentKey()
    {
        // setting language, to check if it will not be included in environment key
        $oLang = $this->getMock('oxLang', array('getBaseLanguage'));
        $oLang->expects($this->any())->method('getBaseLanguage')->will($this->returnValue(3));
        oxTestModules::addModuleObject('oxLang', $oLang);

        $this->setSessionParam("currency", 2);
        $this->setSessionParam("ldtype", "line");
        $this->setSessionParam("_artperpage", 16);
        $this->setSessionParam("aSorting", array("key" => "value"));
        $this->setSessionParam("session_attrfilter", array("key2" => "value2"));
        $this->setSessionParam("_newitem", 1);

        /** @var ReverseProxyBackend|MockObject $oShopControl */
        $oShopControl = $this->getMock("oxReverseProxyBackend", array("getUser"));
        $oShopControl->expects($this->once())->method("getUser")->will($this->returnValue(oxNew('oxUser')));

        $aEnvKey = array("#1#2", "#3#line", "#4#16", '#5#a:1:{s:3:"key";s:5:"value";}', '#6#a:1:{s:4:"key2";s:6:"value2";}', "#7#1", "#8#1");
        $sEnvKey = md5(implode('|', $aEnvKey));

        // setting expected oxenv_key value in cookie
        $oUtilsServer = $this->getMock('oxUtilsServer', array('setOxCookie'));
        $oUtilsServer->expects($this->once())->method('setOxCookie')->with($this->equalTo("oxenv_key"), $this->equalTo($sEnvKey));
        oxTestModules::addModuleObject('oxUtilsServer', $oUtilsServer);

        $oShopControl->setEnvironmentKey();
    }

    /**
     * oxReverseProxyBackend::setEnvironmentKey test case.
     * No environment key should be set if values related with it are default
     *
     * @return null
     */
    public function testSetEnvironmentKey_emptyKey()
    {
        $oShopControl = oxNew('oxReverseProxyBackend');

        $oShopControl->setEnvironmentKey();
        $this->assertNull($oShopControl->UNITgetEnvironmentKey());
    }

    /**
     * oxReverseProxyBackend::_getEnvironmentKey test case.
     *
     * @return null
     */
    public function test_SetGet_EnvironmentKey()
    {
        $oShopControl = oxNew('oxReverseProxyBackend');

        // key is empty
        $this->assertEquals(null, $oShopControl->UNITgetEnvironmentKey(null, null));

        // adding first param to key
        $oShopControl->UNITaddParamToEnvironmentKey("testValue", "#1#");
        $this->assertEquals(md5("#1#testValue"), $oShopControl->UNITgetEnvironmentKey());

        // adding second param
        $oShopControl->UNITaddParamToEnvironmentKey("testValue2", "#2#");
        $this->assertEquals(md5("#1#testValue|#2#testValue2"), $oShopControl->UNITgetEnvironmentKey());

        // adding empty value, no changes should be in key
        $oShopControl->UNITaddParamToEnvironmentKey("", "#3#");
        $this->assertEquals(md5("#1#testValue|#2#testValue2"), $oShopControl->UNITgetEnvironmentKey());

        $oShopControl->UNITaddParamToEnvironmentKey(0, "#3#");
        $this->assertEquals(md5("#1#testValue|#2#testValue2"), $oShopControl->UNITgetEnvironmentKey());

        $oShopControl->UNITaddParamToEnvironmentKey("", "#3#");
        $this->assertEquals(md5("#1#testValue|#2#testValue2"), $oShopControl->UNITgetEnvironmentKey());

        $oShopControl->UNITaddParamToEnvironmentKey(array(), "#3#");
        $this->assertEquals(md5("#1#testValue|#2#testValue2"), $oShopControl->UNITgetEnvironmentKey());

        // adding param as array
        $oShopControl->UNITaddParamToEnvironmentKey(array("one"), "#3#");
        $this->assertEquals(md5("#1#testValue|#2#testValue2|#3#" . serialize(array("one"))), $oShopControl->UNITgetEnvironmentKey());
    }

    /**
     * oxReverseProxyBackend::_addListDisplayTypeToEnvironmentKey test case.
     *
     * @return null
     */
    public function testAddListDisplayTypeToEnvironmentKey()
    {
        $oShopControl = oxNew('oxReverseProxyBackend');

        // checking if adding config param value, which is same
        // as default, will not be added
        $oShopControl->UNITaddListDisplayTypeToEnvironmentKey("infogrid", "#3#");
        $this->assertEquals(null, $oShopControl->UNITgetEnvironmentKey());

        // adding param which is different from default value
        $oShopControl->UNITaddListDisplayTypeToEnvironmentKey("line", "#3#");
        $this->assertEquals(md5("#3#line"), $oShopControl->UNITgetEnvironmentKey());
    }

    /**
     * oxReverseProxyBackend::_addNrOfCatArticlesToEnvironmentKey test case.
     *
     * @return null
     */
    public function testAddNrOfCatArticlesToEnvironmentKey()
    {
        $oShopControl = oxNew('oxReverseProxyBackend');

        // no parameters
        $oShopControl->UNITaddNrOfCatArticlesToEnvironmentKey(null, null, null);
        $this->assertEquals(null, $oShopControl->UNITgetEnvironmentKey());

        $oShopControl->UNITaddNrOfCatArticlesToEnvironmentKey(0, grid, null);
        $this->assertEquals(null, $oShopControl->UNITgetEnvironmentKey());

        // no number parameter
        $oShopControl->UNITaddNrOfCatArticlesToEnvironmentKey(null, "grid", "#4#");
        $this->assertEquals(null, $oShopControl->UNITgetEnvironmentKey());

        // adding param which is same as default value for selected display list type
        $oShopControl->UNITaddNrOfCatArticlesToEnvironmentKey("10", "infogrid", "#4#");
        $this->assertEquals(null, $oShopControl->UNITgetEnvironmentKey());

        // adding param which is same as default value for selected display list type
        $oShopControl->UNITaddNrOfCatArticlesToEnvironmentKey("12", "grid", "#4#");
        $this->assertEquals(null, $oShopControl->UNITgetEnvironmentKey());

        // adding param which is different from default selected display list type
        $oShopControl->UNITaddNrOfCatArticlesToEnvironmentKey("14", "grid", "#4#");
        $this->assertEquals((md5("#4#14")), $oShopControl->UNITgetEnvironmentKey());

        // adding param which is different from default selected display list type
        // grid type is not selected so default one is used
        $oShopControl = oxNew('oxReverseProxyBackend');
        $oShopControl->UNITaddNrOfCatArticlesToEnvironmentKey("16", null, "#4#");
        $this->assertEquals((md5("#4#16")), $oShopControl->UNITgetEnvironmentKey());
    }
}
