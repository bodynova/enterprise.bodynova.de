<?php
/**
 * This Software is the property of OXID eSales and is protected
 * by copyright law - it is NOT Freeware.
 *
 * Any unauthorized use of this software without a valid license key
 * is a violation of the license agreement and will be prosecuted by
 * civil and criminal law.
 *
 * @link      http://www.oxid-esales.com
 * @copyright (C) OXID eSales AG 2003-2016
 * @version   OXID eShop EE
 */
namespace OxidEsales\EshopEnterprise\Tests\Unit\Core\Cache\Generic\Connector;

class FileCacheConnectorTest extends \oxUnitTestCase
{
    /**
     * Calling parent constructor, to fix possible problems with dataprovider
     *
     * @param  string $name
     * @param  array  $data
     * @param  string $dataName
     */
    public function __construct($name = null, array $data = array(), $dataName = '')
    {
        parent::__construct($name, $data, $dataName);
    }

    public static function cacheDataProvider()
    {
        // key and value
        return array(
            array('key_1', 1),
            array('key_2', 'testString'),
            array('key_3', 0.13),
            array('key_4', array(1, 2.01, 'testString'))
        );
    }

    /**
     * Test for data storing to and getting from cache
     *
     * @dataProvider cacheDataProvider
     *
     * @param string $sKey   Key
     * @param mixed  $mValue Value
     */
    public function testSettingAndGettingData($sKey, $mValue)
    {
        $oCacheConnector = oxNew('oxFileCacheConnector');
        $oCacheItem = oxNew('oxCacheItem');
        $oCacheItem->setData($mValue);
        $oCacheConnector->set($sKey, $oCacheItem);
        $this->assertEquals($mValue, $oCacheConnector->get($sKey)->getData());
    }

    /**
     * Test get expired
     */
    public function testGetExpired()
    {
        $oCacheConnector = oxNew('oxFileCacheConnector');
        $oCacheItem = oxNew('oxCacheItem');
        $oCacheConnector->set('expired_key', $oCacheItem, -3600);
        $this->assertNull($oCacheConnector->get('expired_key'));
    }

    /**
     * Test get not expired
     */
    public function testGetNotExpired()
    {
        $oCacheConnector = oxNew('oxFileCacheConnector');
        $oCacheItem = oxNew('oxCacheItem');
        $oCacheConnector->set('non_expired_key', $oCacheItem, 3600);
        $this->assertEquals($oCacheItem, $oCacheConnector->get('non_expired_key'));
    }

    /**
     * Test get  not existing data
     */
    public function testGetNotExistingData()
    {
        $oCacheConnector = oxNew('oxFileCacheConnector');
        $this->assertNull($oCacheConnector->get('not_exist_key'));
    }

    /**
     * Test Set and get cache directory
     */
    public function testSetAndGetCacheDirectory()
    {
        $oCacheConnector = oxNew('oxFileCacheConnector');
        $oCacheConnector->setCacheDir('tmp');
        $this->assertEquals('tmp', $oCacheConnector->getCacheDir());
    }

    /**
     * Test invalidate cache
     */
    public function testInvalidateFromCache()
    {
        $oCacheConnector = oxNew('oxFileCacheConnector');
        $oCacheConnector->flush();
        $oCacheItem = oxNew('oxCacheItem');
        $oCacheItem->setData('del_data');
        $oCacheConnector->set('del_key', $oCacheItem);
        $oCacheItem->setData('stay_data');
        $oCacheConnector->set('stay_key', $oCacheItem);

        $oCacheConnector->invalidate('del_key');

        $this->assertEquals('stay_data', $oCacheConnector->get('stay_key')->getData());
        $this->assertNull($oCacheConnector->get('del_key'));
    }

    /**
     * Test for cache flush
     */
    public function testFlushCache()
    {
        $oCacheConnector = oxNew('oxFileCacheConnector');
        $oCacheItem = oxNew('oxCacheItem');
        $oCacheItem->setData('data');
        $oCacheConnector->set('key1', $oCacheItem);
        $oCacheItem->setData('data');
        $oCacheConnector->set('key2', $oCacheItem);

        $oCacheConnector->flush();

        $this->assertNull($oCacheConnector->get('key1'));
        $this->assertNull($oCacheConnector->get('key2'));

        $aFiles = glob(getShopBasePath() . $oCacheConnector->getCacheDir() . '/*');
        $this->assertEquals(0, count($aFiles));
    }
}
