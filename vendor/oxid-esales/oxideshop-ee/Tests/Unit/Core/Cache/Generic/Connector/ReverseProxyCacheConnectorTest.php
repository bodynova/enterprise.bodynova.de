<?php
/**
 * This Software is the property of OXID eSales and is protected
 * by copyright law - it is NOT Freeware.
 *
 * Any unauthorized use of this software without a valid license key
 * is a violation of the license agreement and will be prosecuted by
 * civil and criminal law.
 *
 * @link      http://www.oxid-esales.com
 * @copyright (C) OXID eSales AG 2003-2016
 * @version   OXID eShop EE
 */
namespace OxidEsales\EshopEnterprise\Tests\Unit\Core\Cache\Generic\Connector;

use OxidEsales\EshopEnterprise\Core\Cache\Generic\Connector\ReverseProxyCacheConnector;
use \oxRegistry;

use \PHPUnit_Framework_MockObject_MockObject as MockObject;

/**
 * Tests ReverseProxyCacheConnector
 */
class ReverseProxyCacheConnectorTest extends \oxUnitTestCase
{
    /**
     * Test invalidation
     */
    public function testInvalidate()
    {
        $aHeaders = array(
            'x-ban-url: testUrl',
            'x-ban-host: .*',
        );

        $curl = $this->getMock('oxCurl');
        /** @var ReverseProxyCacheConnector|MockObject $oConnector */
        $oConnector = $this->getMock('oxReverseProxyConnector', array('getReverseProxyUrl', 'ban', 'purge'), array($curl));
        $oConnector->expects($this->once())->method('getReverseProxyUrl')->will($this->returnValue('testShop'));
        $oConnector->expects($this->never())->method('ban');
        $oConnector->expects($this->once())->method('purge')->with($this->equalTo('testShop'), $this->equalTo($aHeaders));
        $oConnector->setShopHost('.*');

        $this->assertNull($oConnector->invalidate('testUrl'));
    }

    /**
     * Test invalidation
     */
    public function testInvalidateDomain()
    {
        $aHeaders = array(
            'x-ban-url: /',
            'x-ban-host: .*',
        );

        $curl = $this->getMock('oxCurl');

        /** @var ReverseProxyCacheConnector|MockObject $oConnector */
        $oConnector = $this->getMock('oxReverseProxyConnector', array('getReverseProxyUrl', 'ban', 'purge'), array($curl));
        $oConnector->expects($this->once())->method('getReverseProxyUrl')->will($this->returnValue('testShop'));
        $oConnector->expects($this->once())->method('ban')->with($this->equalTo('testShop'), $this->equalTo($aHeaders));
        $oConnector->expects($this->never())->method('purge');
        $oConnector->setShopHost('.*');

        $this->assertNull($oConnector->invalidate('/'));
    }

    /**
     * Test invalidation
     */
    public function testInvalidateNoUrlIsSet()
    {
        $curl = $this->getMock('oxCurl');

        /** @var ReverseProxyCacheConnector|MockObject $oConnector */
        $oConnector = $this->getMock('oxReverseProxyConnector', array('getReverseProxyUrl', 'ban', 'purge'), array($curl));
        $oConnector->expects($this->never())->method('getReverseProxyUrl');
        $oConnector->expects($this->never())->method('ban');
        $oConnector->expects($this->never())->method('purge');

        $this->assertNull($oConnector->invalidate(null));
    }

    /**
     * Test invalidation
     */
    public function testInvalidateDifferentHost()
    {
        $aHeaders = array(
            'x-ban-url: /',
            'x-ban-host: testHost',
        );

        $curl = $this->getMock('oxCurl');

        /** @var ReverseProxyCacheConnector|MockObject $oConnector */
        $oConnector = $this->getMock('oxReverseProxyConnector', array('getReverseProxyUrl', 'ban', 'purge'), array($curl));
        $oConnector->expects($this->once())->method('getReverseProxyUrl')->will($this->returnValue('testShop'));
        $oConnector->expects($this->once())->method('ban')->with($this->equalTo('testShop'), $this->equalTo($aHeaders));
        $oConnector->expects($this->never())->method('purge');
        $oConnector->setShopHost('testHost');

        $this->assertNull($oConnector->invalidate('/'));
    }

    public function providerInvalidateHomePage()
    {
        return array(
            array('', '/'),
            array('/', '/'),
            array('/home_page', '/home_page/'),
            array('/home_page/', '/home_page/'),
        );
    }

    /**
     * @param string $activeShopUrlPath
     * @param string $urlToBan
     *
     * @dataProvider providerInvalidateHomePage
     */
    public function testInvalidateHomePage($activeShopUrlPath, $urlToBan)
    {
        $oxUtilsUrl = $this->getMock('oxUtilsUrl');
        $oxUtilsUrl->expects($this->any())->method('getActiveShopUrlPath')->will($this->returnValue($activeShopUrlPath));
        oxRegistry::set('oxUtilsUrl', $oxUtilsUrl);

        $host = 'test_host';
        $aHeaders = array(
            'x-ban-url: ' . $urlToBan,
            'x-ban-host: ' . $host,
        );

        $curl = $this->getMock('oxCurl');

        /** @var ReverseProxyCacheConnector|MockObject $oConnector */
        $oConnector = $this->getMock('oxReverseProxyConnector', array('getReverseProxyUrl', 'ban', 'purge'), array($curl));
        $oConnector->expects($this->once())->method('getReverseProxyUrl')->will($this->returnValue('testShop'));
        $oConnector->expects($this->once())->method('ban')->with($this->equalTo('testShop'), $this->equalTo($aHeaders));
        $oConnector->expects($this->never())->method('purge');
        $oConnector->setShopHost($host);

        $this->assertNull($oConnector->invalidate($urlToBan));
    }

    public function providerInvalidateNotHomePage()
    {
        return array(
            array('/', '/kites/'),
            array('', 'kites'),
            array('/home_page/', '/home_page/kites/'),
            array('/home_page_kites/', 'kites'),
            array('/home_page_kites/all_about_us/', 'kites'),
        );
    }

    /**
     * @param string $activeShopUrlPath
     * @param string $urlToBan
     *
     * @dataProvider providerInvalidateNotHomePage
     */
    public function testInvalidateNotHomePage($activeShopUrlPath, $urlToBan)
    {
        $oxUtilsUrl = $this->getMock('oxUtilsUrl');
        $oxUtilsUrl->expects($this->any())->method('getActiveShopUrlPath')->will($this->returnValue($activeShopUrlPath));
        oxRegistry::set('oxUtilsUrl', $oxUtilsUrl);

        $host = 'test_host';
        $aHeaders = array(
            'x-ban-url: ' . $urlToBan,
            'x-ban-host: ' . $host,
        );

        $curl = $this->getMock('oxCurl');

        /** @var ReverseProxyCacheConnector|MockObject $oConnector */
        $oConnector = $this->getMock('oxReverseProxyConnector', array('getReverseProxyUrl', 'ban', 'purge'), array($curl));
        $oConnector->expects($this->once())->method('getReverseProxyUrl')->will($this->returnValue('testShop'));
        $oConnector->expects($this->never())->method('ban');
        $oConnector->expects($this->once())->method('purge')->with($this->equalTo('testShop'), $this->equalTo($aHeaders));
        $oConnector->setShopHost($host);

        $this->assertNull($oConnector->invalidate($urlToBan));
    }

    /**
     * Testing reverse proxy url setting and getting
     */
    public function testSetAndGetReverseProxyUrl()
    {
        $curl = oxNew('oxCurl');

        $connector = oxNew('oxReverseProxyConnector', $curl);
        $connector->setReverseProxyUrl('testUrl');
        $this->assertEquals('testUrl', $connector->getReverseProxyUrl());
    }

    /**
     * Testing reverse proxy url setting and getting
     */
    public function testSetAndGetShopHost()
    {
        $curl = oxNew('oxCurl');

        $connector = oxNew('oxReverseProxyConnector', $curl);
        $connector->setShopHost('testHost');
        $this->assertEquals('testHost', $connector->getShopHost());
    }
}
