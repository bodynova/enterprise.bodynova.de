<?php
/**
 * This Software is the property of OXID eSales and is protected
 * by copyright law - it is NOT Freeware.
 *
 * Any unauthorized use of this software without a valid license key
 * is a violation of the license agreement and will be prosecuted by
 * civil and criminal law.
 *
 * @link      http://www.oxid-esales.com
 * @copyright (C) OXID eSales AG 2003-2016
 * @version   OXID eShop EE
 */
namespace OxidEsales\EshopEnterprise\Tests\Unit\Core\Cache\Generic;

require_once TEST_LIBRARY_HELPERS_PATH . 'oxTestCacheConnector.php';

use \oxTestCacheConnector;
use OxidEsales\EshopEnterprise\Core\Cache\Generic\Cache;
use OxidEsales\EshopEnterprise\Core\Cache\Generic\Connector\FileCacheConnector;

use \PHPUnit_Framework_MockObject_MockObject as MockObject;

/**
 * Test cache connector.
 * @package OxidEsales\Tests\Unit\Enterprise\Core\Cache\Generic
 */
class TestCacheConnector extends oxTestCacheConnector
{
    public function get($mKey)
    {
        $oData = oxNew('oxCacheItem');
        $oData->setData('dummy_' . $mKey);

        return $oData;
    }
}

/**
 * Test unavailable cache connector.
 * @package OxidEsales\Tests\Unit\Enterprise\Core\Cache\Generic
 */
class TestUnavailableCacheConnector extends oxTestCacheConnector
{
    public static function isAvailable()
    {
        return false;
    }
}

/**
 * Actual test class
 * @package OxidEsales\Tests\Unit\Enterprise\Core\Cache\Generic
 */
class CacheTest extends \oxUnitTestCase
{
    /** @var Cache */
    public $oCacheBk;

    /** @var  oxTestCacheConnector */
    public $oCacheCn;

    /**
     * Initialize the fixture.
     *
     * @return null
     */
    protected function setUp()
    {
        $this->oCacheCn = new oxTestCacheConnector();
        $this->oCacheBk = oxNew('oxCacheBackend');
        $this->oCacheBk->registerConnector($this->oCacheCn);
        $this->oCacheBk->flush();
        parent::setUp();
    }

    /**
     * Tear down the fixture.
     *
     * @return null
     */
    protected function tearDown()
    {
        parent::tearDown();
        if ($this->oCacheBk) {
            $this->oCacheBk->flush();
        }
    }

    /**
     * Data provider for oxCacheBackEnd::isActive().
     *
     * @return array
     */
    public static function isActive_dataProvider()
    {
        $oTestConnectorAvailable = new oxTestCacheConnector();
        $oTestConnectorUnavailable = new TestUnavailableCacheConnector();

        return array(
            array($oTestConnectorAvailable, true, true),
            array($oTestConnectorAvailable, false, false),
            array($oTestConnectorUnavailable, true, false),
            array($oTestConnectorUnavailable, false, false),
        );
    }

    /**
     * oxCacheBackEnd::isActive() test case.
     * Check if Memcached connector is turned on.
     *
     * @dataProvider isActive_dataProvider
     *
     * @param oxTestCacheConnector $oConnector    Connector
     * @param bool                 $blCacheActive Cache Active
     * @param bool                 $blResult      Result
     *
     * @return null
     */
    public function testIsActive($oConnector, $blCacheActive, $blResult)
    {
        $this->setConfigParam("blCacheActive", $blCacheActive);

        $oCache = oxNew('oxCacheBackend');
        $oCache->registerConnector($oConnector);

        $this->assertEquals($blResult, $oCache->isActive());
        $this->assertEquals($blResult, $oCache->isActive());
    }

    /**
     * Test register Connector
     */
    public function testRegisterConnector()
    {
        $sKey = 'testKey';
        $oData = oxNew('oxCacheItem');
        $oData->setData(array('register', 'connector'));

        $this->oCacheBk->set($sKey, $oData);
        $this->assertEquals($this->oCacheBk->get($sKey), $oData);

        $oDummyCn = new TestCacheConnector();
        $this->oCacheBk->registerConnector($oDummyCn);
        $this->assertEquals($this->oCacheBk->get($sKey)->getData(), 'dummy_' . $sKey);
    }

    /**
     * Test register unavailable connector
     */
    public function testRegisterUnavailableConnector()
    {
        $sKey = 'testKey';
        $oData = oxNew('oxCacheItem');
        $oData->setData(array('register', 'connector'));

        $this->oCacheBk->set($sKey, $oData);
        $this->assertEquals($this->oCacheBk->get($sKey), $oData);

        $oUnavailableCn = new TestUnavailableCacheConnector();
        $this->oCacheBk->registerConnector($oUnavailableCn);
        $this->assertNull($this->oCacheBk->get($sKey));
        $this->assertFalse($this->oCacheBk->isActive());
    }

    /**
     * Test default connector.
     */
    public function testRegisterDefaultConnector()
    {
        $this->setConfigParam('sDefaultCacheConnector', 'oxFileCacheConnector');

        $oCacheBk = oxNew('oxCacheBackend');
        $this->assertTrue($oCacheBk->UNITgetConnector() instanceof FileCacheConnector);
        $oCacheBk->get('testKey');
    }

    /**
     * Test default connector from config.
     */
    public function testRegisterDefaultConfigConnector()
    {
        $this->setConfigParam('sDefaultCacheConnector', 'oxTestCacheConnector');

        $oCacheBk = oxNew('oxCacheBackend');
        //$this->assertInstanceOf('oxTestCacheConnector', $oCacheBk->UNITgetConnector());
        $this->assertTrue($oCacheBk->UNITgetConnector() instanceof oxTestCacheConnector);
        $oCacheBk->get('testKey');
    }

    /**
     * Test default unavailable connector from config.
     */
    public function testRegisterDefaultUnavailableConnector()
    {
        $this->setConfigParam('sDefaultCacheConnector', '\OxidEsales\EshopEnterprise\Tests\Unit\Core\Cache\Generic\TestUnavailableCacheConnector');

        $oCacheBk = oxNew('oxCacheBackend');
        $this->assertNull($oCacheBk->UNITgetConnector());
        $oCacheBk->get('testKey');
    }

    /**
     * Test set() and get().
     */
    public function testSetGet()
    {
        $sKey = 'testKey';
        $oData = oxNew('oxCacheItem');
        $oData->setData(array('set', 'get', 'non', 'pooled'));

        $this->oCacheBk->set($sKey, $oData);

        $this->assertEquals($this->oCacheCn->get($sKey), $oData);
        $this->assertEquals($this->oCacheBk->get($sKey), $oData);
    }

    /**
     * Test set() and get() pooled.
     */
    public function testSetGetPooled()
    {
        $sKey = 'testKey';
        $oData = oxNew('oxCacheItem');
        $oData->setData(array('set', 'get', 'pooled'));

        /** @var oxTestCacheConnector|MockObject $oCacheCn */
        $oCacheCn = $this->getMock("oxTestCacheConnector", array("get"));
        $oCacheCn->expects($this->never())->method('get')->will($this->returnValue($oData));

        $oCacheBk = oxNew('oxCacheBackend');
        $oCacheBk->registerConnector($oCacheCn);
        $oCacheBk->set($sKey, $oData);

        $this->assertEquals($oCacheBk->get($sKey), $oData);
        $this->assertEquals($oCacheBk->get($sKey), $oData);
    }

    /**
     * Test set() and get() non pooled, use setter in connector.
     */
    public function testSetGetNonPooled()
    {
        $sKey = 'testKey';
        $oData = oxNew('oxCacheItem');
        $oData->setData(array('set', 'get', 'pooled'));

        /** @var oxTestCacheConnector|MockObject $oCacheCn */
        $oCacheCn = $this->getMock("oxTestCacheConnector", array("get"));
        $oCacheCn->expects($this->once())->method('get')->will($this->returnValue($oData));

        $oCacheBk = oxNew('oxCacheBackend');
        $oCacheBk->registerConnector($oCacheCn);
        $oCacheCn->set($sKey, $oData);

        $this->assertEquals($oCacheBk->get($sKey), $oData);
        $this->assertEquals($oCacheBk->get($sKey), $oData);
    }


    /**
     * Test set() and get() pool only.
     */
    public function testSetGetPooledOnly()
    {
        $sKey = 'testKey';
        $oData = oxNew('oxCacheItem');
        $oData->setData(array('set', 'get', 'pooled', 'only'));

        $this->setConfigParam('sDefaultCacheConnector', null);

        $oCacheBk = oxNew('oxCacheBackend');
        $oCacheBk->set($sKey, $oData);

        $this->assertEquals($oCacheBk->get($sKey), $oData);
    }

    /**
     * Test set() and invalidate() pool only.
     */
    public function testSetInvalidatePooledOnly()
    {
        $sKey = 'testKey';
        $oData = oxNew('oxCacheItem');
        $oData->setData(array('set', 'invalidate', 'pooled', 'only'));

        $this->setConfigParam('sDefaultCacheConnector', null);

        $oCacheBk = oxNew('oxCacheBackend');
        $oCacheBk->set($sKey, $oData);
        $oCacheBk->invalidate($sKey);

        $this->assertNull($oCacheBk->get($sKey));
    }

    /**
     * Test set() and get() with arrays.
     */
    public function testSetGetArrays()
    {

        $oData = oxNew('oxCacheItem');
        $oData->setData(array('set', 'get', 'non', 'pooled'));

        $aData = array('TestKey1' => $oData, 'TestKey2' => $oData);
        $aKeys = array_keys($aData);

        $this->oCacheBk->set($aData);

        $this->assertEquals($this->oCacheCn->get($aKeys), $aData);
        $this->assertEquals($this->oCacheBk->get($aKeys), $aData);
    }

    /**
     * Test set() and get() with array non pooled.
     */
    public function testSetGetArrayNonPooled()
    {

        $oData = oxNew('oxCacheItem');
        $oData->setData(array('set', 'get', 'non', 'pooled'));

        $aData = array('TestKey1' => $oData, 'TestKey2' => $oData);
        $aKeys = array_keys($aData);

        $this->oCacheBk->set($aData);
        $this->oCacheBk->UNITpoolInvalidate('TestKey1');
        $this->oCacheBk->UNITpoolInvalidate('TestKey2');

        $this->assertEquals($this->oCacheCn->get($aKeys), $aData);
        $this->assertEquals($this->oCacheBk->get($aKeys), $aData);
    }

    /**
     * Test set() and invalidate() with arrays.
     */
    public function testSetInvalidateArrays()
    {
        $oData = oxNew('oxCacheItem');
        $oData->setData(array('set', 'get', 'non', 'pooled'));

        $aData = array('TestKey1' => $oData, 'TestKey2' => $oData);
        $aKeys = array_keys($aData);

        $this->oCacheBk->set($aData);
        $this->oCacheBk->invalidate($aKeys);

        $this->assertEquals($this->oCacheCn->get($aKeys), array());
        $this->assertEquals($this->oCacheBk->get($aKeys), array());
    }

    /**
     * Test set() and flush() pool only.
     */
    public function testSetFlushPooledOnly()
    {
        $sKey = 'testKey';
        $oData = oxNew('oxCacheItem');
        $oData->setData(array('set', 'flush', 'pooled', 'only'));

        $this->setConfigParam('sDefaultCacheConnector', null);

        $oCacheBk = oxNew('oxCacheBackend');
        $oCacheBk->set($sKey, $oData);
        $oCacheBk->invalidate($sKey);

        $this->assertNull($oCacheBk->get($sKey));
    }

    /**
     * Test set() for existing.
     */
    public function testSetExisting()
    {
        $sKey = 'testKey';

        $oData1 = oxNew('oxCacheItem');
        $oData1->setData(array('set', 'existing'));

        $this->oCacheBk->set($sKey, $oData1);
        $this->assertEquals($this->oCacheBk->get($sKey), $oData1);

        $oData2 = oxNew('oxCacheItem');
        $oData2->setData(array('set', 'existing', 'again'));

        $this->oCacheBk->set($sKey, $oData2);
        $this->assertEquals($this->oCacheBk->get($sKey), $oData2);
    }

    /**
     * Test get() for non existing.
     */
    public function testGetNonExisting()
    {
        $sKey = 'testKey';

        $this->assertNull($this->oCacheCn->get($sKey));
        $this->assertNull($this->oCacheBk->get($sKey));
    }

    /**
     * Test set(), invalidate(), get().
     */
    public function testSetInvalidateGet()
    {
        $sKey = 'testKey';
        $oData = oxNew('oxCacheItem');
        $oData->setData(array('set', 'invalidate', 'get'));

        $this->oCacheBk->set($sKey, $oData);
        $this->assertEquals($this->oCacheBk->get($sKey), $oData);

        $this->oCacheBk->invalidate($sKey);
        $this->assertNull($this->oCacheBk->get($sKey));
    }

    /**
     * Test set(), set(), flush().
     */
    public function testSetSetFlush()
    {
        $sKey1 = 'testKey1';
        $sKey2 = 'testKey1';

        $oData = oxNew('oxCacheItem');
        $oData->setData(array('set', 'set', 'flush'));

        $this->oCacheBk->set($sKey1, $oData);
        $this->oCacheBk->set($sKey2, $oData);

        $this->assertEquals($this->oCacheBk->get($sKey1), $oData);
        $this->assertEquals($this->oCacheBk->get($sKey2), $oData);

        $this->oCacheBk->flush();
        $this->assertNull($this->oCacheBk->get($sKey1));
        $this->assertNull($this->oCacheBk->get($sKey2));
    }

    /**
     * #0005761
     */
    public function testUseDefaultTtl_defined()
    {
        $iDefaultTtl = 34;
        $this->getConfig()->setConfigParam('iDefaultCacheTTL', $iDefaultTtl);

        $oCacheItem = oxNew('oxCacheItem');
        $oCacheItem->setData('value');

        $oCacheConnector = $this->getMock("oxFileCacheConnector", array("set"));
        $oCacheConnector->expects($this->once())->method('set')->with($this->equalTo('key'), $this->equalTo($oCacheItem), $this->equalTo($iDefaultTtl));

        /** @var Cache|MockObject $oCacheBackend */
        $oCacheBackend = $this->getMock("oxCacheBackend", array("_getConnector"));
        $oCacheBackend->expects($this->any())->method('_getConnector')->will($this->returnValue($oCacheConnector));

        $oCacheBackend->set('key', $oCacheItem);
    }

    /**
     * #0005761
     */
    public function testUseDefaultTtl_notDefined()
    {
        $oCacheItem = oxNew('oxCacheItem');
        $oCacheItem->setData('value');

        $oCacheConnector = $this->getMock("oxFileCacheConnector", array("set"));
        $oCacheConnector->expects($this->once())->method('set')->with($this->equalTo('key'), $this->equalTo($oCacheItem), $this->equalTo(0));

        /** @var Cache|MockObject $oCacheBackend */
        $oCacheBackend = $this->getMock("oxCacheBackend", array("_getConnector"));
        $oCacheBackend->expects($this->any())->method('_getConnector')->will($this->returnValue($oCacheConnector));

        $oCacheBackend->set('key', $oCacheItem);
    }

}
