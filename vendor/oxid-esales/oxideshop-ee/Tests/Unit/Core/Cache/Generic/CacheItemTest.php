<?php
/**
 * This Software is the property of OXID eSales and is protected
 * by copyright law - it is NOT Freeware.
 *
 * Any unauthorized use of this software without a valid license key
 * is a violation of the license agreement and will be prosecuted by
 * civil and criminal law.
 *
 * @link      http://www.oxid-esales.com
 * @copyright (C) OXID eSales AG 2003-2016
 * @version   OXID eShop EE
 */
namespace OxidEsales\EshopEnterprise\Tests\Unit\Core\Cache\Generic;

class CacheItemTest extends \oxUnitTestCase
{
    public function setDataProvider()
    {
        return array(
            array(1),
            array('testString'),
            array(0.13),
            array(array(1, 2.01, 'testString'))
        );
    }

    /**
     * testSettingAndGettingDataForCache
     * Test for oxCacheItem::setData(), oxCacheItem::getData()
     *
     * @dataProvider setDataProvider
     *
     * @param mixed $mData Data
     *
     */
    public function testSettingAndGettingDataForCache($mData)
    {
        $oCacheItem = oxNew('oxCacheItem');
        $oCacheItem->setData($mData);

        $this->assertEquals($mData, $oCacheItem->getData());
    }
}
