<?php
/**
 * This Software is the property of OXID eSales and is protected
 * by copyright law - it is NOT Freeware.
 *
 * Any unauthorized use of this software without a valid license key
 * is a violation of the license agreement and will be prosecuted by
 * civil and criminal law.
 *
 * @link      http://www.oxid-esales.com
 * @copyright (C) OXID eSales AG 2003-2016
 * @version   OXID eShop EE
 */

namespace OxidEsales\EshopEnterprise\Tests\Unit\Core;

use OxidEsales\TestingLibrary\UnitTestCase;

class FunctionsTest extends UnitTestCase
{
    public function testGetViewName()
    {
        $this->getConfig()->setConfigParam('aMultiShopTables', array('xxx', 'yyy'));

        $this->assertEquals('oxv_xxx_' . $this->getConfig()->getBaseShopId(), getViewName('xxx'));
        $this->assertEquals('zzz', getViewName('zzz'));

        $this->getConfig()->setConfigParam('blSkipViewUsage', true);
        $this->assertEquals('xxx', getViewName('xxx', 'xxx'));
    }
}
