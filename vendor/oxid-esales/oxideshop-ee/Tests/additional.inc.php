<?php

$serviceCaller = new \OxidEsales\TestingLibrary\ServiceCaller();
$testConfig = new \OxidEsales\TestingLibrary\TestConfig();

if ($testConfig->getShopEdition() !== 'EE') {
    exit;
}

$serviceCaller = new \OxidEsales\TestingLibrary\ServiceCaller();
$serviceCaller->setParameter('importSql', '@'. __DIR__ .'/Fixtures/testdata.sql');
$serviceCaller->callService('ShopPreparation', 1);

define('oxADMIN_LOGIN', oxDb::getDb()->getOne("select OXUSERNAME from oxuser where oxid='oxdefaultadmin'"));
define('oxADMIN_PASSWD', getenv('oxADMIN_PASSWD') ? getenv('oxADMIN_PASSWD') : 'admin');
