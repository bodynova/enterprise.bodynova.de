<?php
/**
 * This Software is the property of OXID eSales and is protected
 * by copyright law - it is NOT Freeware.
 *
 * Any unauthorized use of this software without a valid license key
 * is a violation of the license agreement and will be prosecuted by
 * civil and criminal law.
 *
 * @link      http://www.oxid-esales.com
 * @copyright (C) OXID eSales AG 2003-2016
 * @version   OXID eShop EE
 */

namespace OxidEsales\EshopEnterprise\Tests\Integration\Core\Database;

use OxidEsales\Eshop\Core\Database\Adapter\DatabaseInterface;
use OxidEsales\Eshop\Core\Database\Adapter\ResultSetInterface;
use OxidEsales\EshopEnterprise\Core\DatabaseProvider;
use OxidEsales\TestingLibrary\UnitTestCase;

/**
 * Class MasterSlaveTest
 *
 * @group  database-adapter
 * @group  master-slave
 * @covers OxidEsales\EshopCommunity\Core\Database
 *
 * Quote form the Doctrine API documentation:
 *
 * Important for the understanding of this connection should be how and when it picks the slave or master.
 *
 * 1. Master picked when 'exec', 'executeUpdate', 'insert', 'delete', 'update', 'createSavepoint', 'releaseSavepoint', 'beginTransaction', 'rollback', 'commit', 'query' or 'prepare' is called.
 * 2. Slave if master was never picked before and ONLY if 'getWrappedConnection' or 'executeQuery' is used.
 * 3. If master was picked once during the lifetime of the connection it will always get picked afterwards.
 * 4. One slave connection is randomly picked ONCE during a request.
 *
 * This integration test suite covers all methods of \OxidEsales\EshopCommunity\Core\Database\Adapter\Doctrine\Database that are using
 * one of the before mentioned methods of \Doctrine\DBAL\Connection under the hood. Their behavior is determined by the
 * doctrine functions and they should react in the same way.
 *
 * To test the master slave behaviour, 3 database connections are established:
 * - a connection to the slave database
 * - a connection to the master database
 * - a master-slave connection, which is expected to switch between master and slave according the rules mentioned before .
 *
 * All tests are completely independent from each other and always start with a fresh database table and fresh connections.
 *
 * \Doctrine\DBAL\Connection::executeQuery
 */
class MasterSlaveTest extends UnitTestCase
{

    /**
     * @var DatabaseInterface
     */
    private $masterSlaveConnection;

    /**
     * @var \mysqli
     */
    private $masterConnection;

    /**
     * @var \mysqli
     */
    private $slaveConnection;

    private $expectedMasterResult;

    private $isTestableMasterSlaveSetup = false;

    /**
     * @inheritdoc
     */
    public function setUp()
    {
        parent::setUp();

        $this->setTestableMasterSlaveSetup();

        if ($this->isTestableMasterSlaveSetup) {
            /**
             * Setup the different connections
             */
            $this->masterSlaveConnection = $this->getConnectionMasterSlave();
            $this->masterConnection = $this->getConnectionMaster();
            $this->slaveConnection = $this->getConnectionSlave();

            /**
             * Drop the test table before each test if it exists
             */
            $this->dropMasterSlaveTable($this->masterConnection);
            /**
             * Create a test table before each test
             */
            $this->createMasterSlaveTable($this->masterConnection);

            $this->expectedMasterResult = '1';

            /** Insert record to the master database */
            $this->populateTestTable($this->expectedMasterResult);
        }
    }

    /**
     * @inheritdoc
     */
    public function tearDown()
    {
        parent::tearDown();

        if ($this->isTestableMasterSlaveSetup) {
            $this->masterSlaveConnection->closeConnection();
            /** Set db property of Database instance to null to enforce a fresh connection after closing the connection */
            $this->setProtectedClassProperty(DatabaseProvider::getInstance(), 'db', null);

            $this->masterConnection->close();
            $this->slaveConnection->close();

            $this->masterSlaveConnection = null;
            $this->masterConnection = null;
            $this->slaveConnection = null;
        }
    }

    /**
     * This test will fail if the connection could not be established
     */
    public function testConnectionMaster()
    {
        $this->markTestSkippedIfNotMasterSlave();

        $this->getConnectionMaster();
    }

    /**
     * This test will fail if the connection could not be established
     */
    public function testConnectionSlave()
    {
        $this->markTestSkippedIfNotMasterSlave();

        $this->getConnectionSlave();
    }

    /**
     * Assure that replication works at all
     */
    public function testBasicMasterSlaveFunctionality()
    {
        $this->markTestSkippedIfNotMasterSlave();

        /** $resultSet Read directly from slave database */
        $actualSlaveResult = $this->readValueFromSlaveDb();

        /** $resultSet Read directly from master database */
        $actualMasterResult = $this->readValueFromMasterDb();

        $this->assertSame($this->expectedMasterResult, $actualSlaveResult, 'Slave connection retrieves record inserted into master database');
        $this->assertSame($this->expectedMasterResult, $actualMasterResult, 'Master connection retrieves record inserted into master database');
    }

    /**
     * Test Doctrine behavior:
     * "Use slave if master was never picked before and ONLY if 'getWrappedConnection' or 'executeQuery' is used."
     *  \OxidEsales\EshopCommunity\Core\Database\Adapter\Doctrine\Database::getAll uses executeQuery under the hood to retrieve the results.
     *
     * @covers \OxidEsales\EshopCommunity\Core\Database\Adapter\Doctrine\Database::getAll
     */
    public function testMasterSlaveConnectionReadsFromSlaveOnGetAll()
    {
        $this->markTestSkippedIfNotMasterSlave();

        $expectedSlaveResult = '100';

        /** Modify record directly on the slave database, bypassing master-slave replication */
        $this->slaveConnection->query('UPDATE `master_slave_table` SET `column1` = ' . $expectedSlaveResult . ' WHERE id = 1');

        /** Read directly from slave database */
        $actualSlaveResult = $this->readValueFromSlaveDb();

        /** Read directly from master database */
        $actualMasterResult = $this->readValueFromMasterDb();

        $actualMasterSlaveResult = $this->masterSlaveConnection->getAll('SELECT column1 FROM `master_slave_table` WHERE id = 1')[0]['column1'];

        $this->assertSame($expectedSlaveResult, $actualSlaveResult, 'Slave connection retrieves modified value ' . $expectedSlaveResult);
        $this->assertSame($this->expectedMasterResult, $actualMasterResult, 'Master connection retrieves unmodified value ' . $expectedSlaveResult);
        $this->assertSame($expectedSlaveResult, $actualMasterSlaveResult, 'Master-Slave connection reads from slave database when using getAll()');
    }

    /**
     * Test Doctrine behavior:
     * "Use slave if master was never picked before and ONLY if 'getWrappedConnection' or 'executeQuery' is used."
     *  \OxidEsales\EshopCommunity\Core\Database\Adapter\Doctrine\Database::getCol uses executeQuery under the hood to retrieve the results.
     *
     * @covers \OxidEsales\EshopCommunity\Core\Database\Adapter\Doctrine\Database::getCol
     */
    public function testMasterSlaveConnectionReadsFromSlaveOnGetCol()
    {
        $this->markTestSkippedIfNotMasterSlave();

        $expectedSlaveResult = '100';

        /** Modify record directly on the slave database, bypassing master-slave replication */
        $this->slaveConnection->query('UPDATE `master_slave_table` SET `column1` = ' . $expectedSlaveResult . ' WHERE id = 1');

        /** Read directly from slave database */
        $actualSlaveResult = $this->readValueFromSlaveDb();

        /** Read directly from master database */
        $actualMasterResult = $this->readValueFromMasterDb();

        $actualMasterSlaveResult = $this->masterSlaveConnection->getCol('SELECT column1 FROM `master_slave_table` WHERE id = 1')[0];

        $this->assertSame($expectedSlaveResult, $actualSlaveResult, 'Slave connection retrieves modified value ' . $expectedSlaveResult);
        $this->assertSame($this->expectedMasterResult, $actualMasterResult, 'Master connection retrieves unmodified value ' . $expectedSlaveResult);
        $this->assertSame($expectedSlaveResult, $actualMasterSlaveResult, 'Master-Slave connection reads from slave database when using getCol()');
    }

    /**
     * Test Doctrine behavior:
     * "Use slave if master was never picked before and ONLY if 'getWrappedConnection' or 'executeQuery' is used."
     *  \OxidEsales\EshopCommunity\Core\Database\Adapter\Doctrine\Database::getOne uses executeQuery under the hood to retrieve the results.
     *
     * @covers \OxidEsales\EshopCommunity\Core\Database\Adapter\Doctrine\Database::getOne
     */
    public function testMasterSlaveConnectionReadsFromSlaveOnGetOne()
    {
        $this->markTestSkippedIfNotMasterSlave();

        $expectedSlaveResult = '100';

        /** Modify record directly on the slave database, bypassing master-slave replication */
        $this->slaveConnection->query('UPDATE `master_slave_table` SET `column1` = ' . $expectedSlaveResult . ' WHERE id = 1');

        /** Read directly from slave database */
        $actualSlaveResult = $this->readValueFromSlaveDb();

        /** Read directly from master database */
        $actualMasterResult = $this->readValueFromMasterDb();

        $actualMasterSlaveResult = $this->masterSlaveConnection->getOne('SELECT column1 FROM `master_slave_table` WHERE id = 1');

        $this->assertSame($expectedSlaveResult, $actualSlaveResult, 'Slave connection retrieves modified value ' . $expectedSlaveResult);
        $this->assertSame($this->expectedMasterResult, $actualMasterResult, 'Master connection retrieves unmodified value ' . $expectedSlaveResult);
        $this->assertSame($expectedSlaveResult, $actualMasterSlaveResult, 'Master-Slave connection reads from slave database when using getOne()');
    }

    /**
     * Test Doctrine behavior:
     * "Use slave if master was never picked before and ONLY if 'getWrappedConnection' or 'executeQuery' is used."
     *  \OxidEsales\EshopCommunity\Core\Database\Adapter\Doctrine\Database::getRow uses executeQuery under the hood to retrieve the results.
     *
     * @covers \OxidEsales\EshopCommunity\Core\Database\Adapter\Doctrine\Database::getRow
     */
    public function testMasterSlaveConnectionReadsFromSlaveOnGetRow()
    {
        $this->markTestSkippedIfNotMasterSlave();

        $expectedSlaveResult = '100';

        /** Modify record directly on the slave database, bypassing master-slave replication */
        $this->slaveConnection->query('UPDATE `master_slave_table` SET `column1` = ' . $expectedSlaveResult . ' WHERE id = 1');

        /** Read directly from slave database */
        $actualSlaveResult = $this->readValueFromSlaveDb();

        /** Read directly from master database */
        $actualMasterResult = $this->readValueFromMasterDb();

        $actualMasterSlaveResult = $this->masterSlaveConnection->getRow('SELECT column1 FROM `master_slave_table` WHERE id = 1')['column1'];

        $this->assertSame($expectedSlaveResult, $actualSlaveResult, 'Slave connection retrieves modified value ' . $expectedSlaveResult);
        $this->assertSame($this->expectedMasterResult, $actualMasterResult, 'Master connection retrieves unmodified value ' . $expectedSlaveResult);
        $this->assertSame($expectedSlaveResult, $actualMasterSlaveResult, 'Master-Slave connection reads from slave database when using getRow()');
    }

    /**
     * Test Doctrine behavior:
     * "Use slave if master was never picked before and ONLY if 'getWrappedConnection' or 'executeQuery' is used."
     *  \OxidEsales\EshopCommunity\Core\Database\Adapter\Doctrine\Database::select uses executeQuery under the hood.
     *  \OxidEsales\EshopCommunity\Core\Database\Adapter\Doctrine\Database::selectLimit uses select under the hood.
     *
     * @covers \OxidEsales\EshopCommunity\Core\Database\Adapter\Doctrine\Database::select
     * @covers \OxidEsales\EshopCommunity\Core\Database\Adapter\Doctrine\Database::selectLimit
     */
    public function testMasterSlaveConnectionReadsFromSlaveOnSelect()
    {
        $this->markTestSkippedIfNotMasterSlave();

        $expectedSlaveResult = '100';

        /** Modify record directly on the slave database, bypassing master-slave replication */
        $this->slaveConnection->query('UPDATE `master_slave_table` SET `column1` = ' . $expectedSlaveResult . ' WHERE id = 1');

        /** Read directly from slave database */
        $actualSlaveResult = $this->readValueFromSlaveDb();

        /** Read directly from master database */
        $actualMasterResult = $this->readValueFromMasterDb();

        /** @var ResultSetInterface $resultSet */
        $resultSet = $this->masterSlaveConnection->select('SELECT column1 FROM `master_slave_table` WHERE id = 1');
        $actualMasterSlaveResult = $resultSet->fields['column1'];

        $this->assertSame($expectedSlaveResult, $actualSlaveResult, 'Slave connection retrieves modified value ' . $expectedSlaveResult);
        $this->assertSame($this->expectedMasterResult, $actualMasterResult, 'Master connection retrieves unmodified value ' . $expectedSlaveResult);
        $this->assertSame($expectedSlaveResult, $actualMasterSlaveResult, 'Master-Slave connection reads from slave database when using select()');
    }

    /**
     * Test Doctrine behavior:
     * "Master is picked when 'exec', 'executeUpdate', 'insert', 'delete', 'update', 'createSavepoint', 'releaseSavepoint', 'beginTransaction', 'rollback', 'commit', 'query' or 'prepare' is called."
     * "Use slave if master was never picked before and ONLY if 'getWrappedConnection' or 'executeQuery' is used."
     *
     *  \OxidEsales\EshopCommunity\Core\Database\Adapter\Doctrine\Database::getAll uses executeQuery under the hood to retrieve the results.
     * Now picking the master is forced by using 'executeUpdate' and then the master should also be also used for
     * \OxidEsales\EshopCommunity\Core\Database\Adapter\Doctrine\Database::getAll
     *
     * @covers \OxidEsales\EshopCommunity\Core\Database\Adapter\Doctrine\Database::getAll
     */
    public function testMasterSlaveConnectionReadsFromMasterOnGetAll()
    {
        $this->markTestSkippedIfNotMasterSlave();

        $expectedSlaveResult = '100';

        /** Modify record directly on the slave database, bypassing master-slave replication */
        $this->slaveConnection->query('UPDATE `master_slave_table` SET `column1` = ' . $expectedSlaveResult . ' WHERE id = 1');

        /** Read directly from slave database */
        $actualSlaveResult = $this->readValueFromSlaveDb();

        /** Read directly from master database */
        $actualMasterResult = $this->readValueFromMasterDb();

        /** Force picking the master by doing an execute here. Doctrine:executeUpdate is called under the hood */
        $this->masterSlaveConnection->execute('INSERT INTO `master_slave_table` (`column1`, `column2`) VALUES (2, 3)');
        $actualMasterSlaveResult = $this->masterSlaveConnection->getAll('SELECT column1 FROM `master_slave_table` WHERE id = 1')[0]['column1'];

        $this->assertSame($expectedSlaveResult, $actualSlaveResult, 'Slave connection retrieves modified value ' . $expectedSlaveResult);
        $this->assertSame($this->expectedMasterResult, $actualMasterResult, 'Master connection retrieves unmodified value ' . $expectedSlaveResult);
        $this->assertSame($this->expectedMasterResult, $actualMasterSlaveResult, 'Master-Slave connection reads from master database when using getAll() after execute()');
    }

    /**
     * Test Doctrine behavior:
     * "Master is picked when 'exec', 'executeUpdate', 'insert', 'delete', 'update', 'createSavepoint', 'releaseSavepoint', 'beginTransaction', 'rollback', 'commit', 'query' or 'prepare' is called."
     * "Use slave if master was never picked before and ONLY if 'getWrappedConnection' or 'executeQuery' is used."
     *
     * \OxidEsales\EshopCommunity\Core\Database\Adapter\Doctrine\Database::getCol uses executeQuery under the hood to retrieve the results.
     * Now picking the master is forced by using 'executeUpdate' and then the master should also be also used for
     * \OxidEsales\EshopCommunity\Core\Database\Adapter\Doctrine\Database::getCol
     *
     * @covers \OxidEsales\EshopCommunity\Core\Database\Adapter\Doctrine\Database::getCol
     */
    public function testMasterSlaveConnectionReadsFromMasterOnGetCol()
    {
        $this->markTestSkippedIfNotMasterSlave();

        $expectedSlaveResult = '100';

        /** Modify record directly on the slave database, bypassing master-slave replication */
        $this->slaveConnection->query('UPDATE `master_slave_table` SET `column1` = ' . $expectedSlaveResult . ' WHERE id = 1');

        /** Read directly from slave database */
        $actualSlaveResult = $this->readValueFromSlaveDb();

        /** Read directly from master database */
        $actualMasterResult = $this->readValueFromMasterDb();

        /** Force picking the master by doing an execute here. Doctrine:executeUpdate is called under the hood */
        $this->masterSlaveConnection->execute('INSERT INTO `master_slave_table` (`column1`, `column2`) VALUES (2, 3)');
        $actualMasterSlaveResult = $this->masterSlaveConnection->getCol('SELECT column1 FROM `master_slave_table` WHERE id = 1')[0];

        $this->assertSame($expectedSlaveResult, $actualSlaveResult, 'Slave connection retrieves modified value ' . $expectedSlaveResult);
        $this->assertSame($this->expectedMasterResult, $actualMasterResult, 'Master connection retrieves unmodified value ' . $expectedSlaveResult);
        $this->assertSame($this->expectedMasterResult, $actualMasterSlaveResult, 'Master-Slave connection reads from master database when using getCol() after execute()');
    }

    /**
     * Test Doctrine behavior:
     * "Master is picked when 'exec', 'executeUpdate', 'insert', 'delete', 'update', 'createSavepoint', 'releaseSavepoint', 'beginTransaction', 'rollback', 'commit', 'query' or 'prepare' is called."
     * "Use slave if master was never picked before and ONLY if 'getWrappedConnection' or 'executeQuery' is used."
     *
     * \OxidEsales\EshopCommunity\Core\Database\Adapter\Doctrine\Database::getOne uses executeQuery under the hood to retrieve the results.
     * Now picking the master is forced by using 'executeUpdate' and then the master should also be also used for
     * \OxidEsales\EshopCommunity\Core\Database\Adapter\Doctrine\Database::getOne
     *
     * @covers \OxidEsales\EshopCommunity\Core\Database\Adapter\Doctrine\Database::getOne
     */
    public function testMasterSlaveConnectionReadsFromMasterOnGetOne()
    {
        $this->markTestSkippedIfNotMasterSlave();

        $expectedSlaveResult = '100';

        /** Modify record directly on the slave database, bypassing master-slave replication */
        $this->slaveConnection->query('UPDATE `master_slave_table` SET `column1` = ' . $expectedSlaveResult . ' WHERE id = 1');

        /** Read directly from slave database */
        $actualSlaveResult = $this->readValueFromSlaveDb();

        /** Read directly from master database */
        $actualMasterResult = $this->readValueFromMasterDb();

        /** Force picking the master by doing an execute here. Doctrine:executeUpdate is called under the hood */
        $this->masterSlaveConnection->execute('INSERT INTO `master_slave_table` (`column1`, `column2`) VALUES (2, 3)');
        $actualMasterSlaveResult = $this->masterSlaveConnection->getOne('SELECT column1 FROM `master_slave_table` WHERE id = 1');

        $this->assertSame($expectedSlaveResult, $actualSlaveResult, 'Slave connection retrieves modified value ' . $expectedSlaveResult);
        $this->assertSame($this->expectedMasterResult, $actualMasterResult, 'Master connection retrieves unmodified value ' . $expectedSlaveResult);
        $this->assertSame($this->expectedMasterResult, $actualMasterSlaveResult, 'Master-Slave connection reads from master database when using getOne() after execute()');
    }

    /**
     * Test Doctrine behavior:
     * "Master is picked when 'exec', 'executeUpdate', 'insert', 'delete', 'update', 'createSavepoint', 'releaseSavepoint', 'beginTransaction', 'rollback', 'commit', 'query' or 'prepare' is called."
     * "Use slave if master was never picked before and ONLY if 'getWrappedConnection' or 'executeQuery' is used."
     *
     * \OxidEsales\EshopCommunity\Core\Database\Adapter\Doctrine\Database::getRow uses executeQuery under the hood to retrieve the results.
     * Now picking the master is forced by using 'executeUpdate' and then the master should also be also used for
     * \OxidEsales\EshopCommunity\Core\Database\Adapter\Doctrine\Database::getRow
     *
     * @covers \OxidEsales\EshopCommunity\Core\Database\Adapter\Doctrine\Database::getRow
     */
    public function testMasterSlaveConnectionReadsFromMasterOnGetRow()
    {
        $this->markTestSkippedIfNotMasterSlave();

        $expectedSlaveResult = '100';

        /** Modify record directly on the slave database, bypassing master-slave replication */
        $this->slaveConnection->query('UPDATE `master_slave_table` SET `column1` = ' . $expectedSlaveResult . ' WHERE id = 1');

        /** Read directly from slave database */
        $actualSlaveResult = $this->readValueFromSlaveDb();

        /** Read directly from master database */
        $actualMasterResult = $this->readValueFromMasterDb();

        /** Force picking the master by doing an execute here. Doctrine:executeUpdate is called under the hood */
        $this->masterSlaveConnection->execute('INSERT INTO `master_slave_table` (`column1`, `column2`) VALUES (2, 3)');
        $actualMasterSlaveResult = $this->masterSlaveConnection->getRow('SELECT column1 FROM `master_slave_table` WHERE id = 1')['column1'];

        $this->assertSame($expectedSlaveResult, $actualSlaveResult, 'Slave connection retrieves modified value ' . $expectedSlaveResult);
        $this->assertSame($this->expectedMasterResult, $actualMasterResult, 'Master connection retrieves unmodified value ' . $expectedSlaveResult);
        $this->assertSame($this->expectedMasterResult, $actualMasterSlaveResult, 'Master-Slave connection reads from master database when using getRow() after execute()');
    }

    /**
     * Test Doctrine behavior:
     * "Master is picked when 'exec', 'executeUpdate', 'insert', 'delete', 'update', 'createSavepoint', 'releaseSavepoint', 'beginTransaction', 'rollback', 'commit', 'query' or 'prepare' is called."
     * "Use slave if master was never picked before and ONLY if 'getWrappedConnection' or 'executeQuery' is used."
     *
     *  \OxidEsales\EshopCommunity\Core\Database\Adapter\Doctrine\Database::select uses executeQuery to retrieve the results.
     * Now picking the master is forced by using 'executeUpdate' and then the master should also be also used for
     * \OxidEsales\EshopCommunity\Core\Database\Adapter\Doctrine\Database::select
     *
     * @covers \OxidEsales\EshopCommunity\Core\Database\Adapter\Doctrine\Database::select
     */
    public function testMasterSlaveConnectionReadsFromMasterOnSelect()
    {
        $this->markTestSkippedIfNotMasterSlave();

        $expectedSlaveResult = '100';

        /** Modify record directly on the slave database, bypassing master-slave replication */
        $this->slaveConnection->query('UPDATE `master_slave_table` SET `column1` = ' . $expectedSlaveResult . ' WHERE id = 1');

        /** Read directly from slave database */
        $actualSlaveResult = $this->readValueFromSlaveDb();

        /** Read directly from master database */
        $actualMasterResult = $this->readValueFromMasterDb();

        /** Force picking the master by doing an execute here. Doctrine:executeUpdate is called under the hood */
        $this->masterSlaveConnection->execute('INSERT INTO `master_slave_table` (`column1`, `column2`) VALUES (2, 3)');
        /** @var ResultSetInterface $resultSet */
        $resultSet = $this->masterSlaveConnection->select('SELECT column1 FROM `master_slave_table` WHERE id = 1');
        $actualMasterSlaveResult = $resultSet->fields['column1'];

        $this->assertSame($expectedSlaveResult, $actualSlaveResult, 'Slave connection retrieves modified value ' . $expectedSlaveResult);
        $this->assertSame($this->expectedMasterResult, $actualMasterResult, 'Master connection retrieves unmodified value ' . $this->expectedMasterResult);
        $this->assertSame($this->expectedMasterResult, $actualMasterSlaveResult, 'Master-Slave connection reads from master database when using select() after execute()');
    }

    /**
     * Test Doctrine behavior:
     * "Master is picked when 'exec', 'executeUpdate', 'insert', 'delete', 'update', 'createSavepoint', 'releaseSavepoint', 'beginTransaction', 'rollback', 'commit', 'query' or 'prepare' is called."
     *
     *  \OxidEsales\EshopCommunity\Core\Database\Adapter\Doctrine\Database::startTransaction uses beginTransaction under the hood.
     *
     * @covers \OxidEsales\EshopCommunity\Core\Database\Adapter\Doctrine\Database::startTransaction
     */
    public function testMasterSlaveConnectionReadsFromMasterDuringTransaction()
    {
        $this->markTestSkippedIfNotMasterSlave();

        $expectedSlaveResult = '100';

        /** Modify record directly on the slave database, bypassing master-slave replication */
        $this->slaveConnection->query('UPDATE `master_slave_table` SET `column1` = ' . $expectedSlaveResult . ' WHERE id = 1');

        /** Read directly from slave database */
        $actualSlaveResult = $this->readValueFromSlaveDb();

        /** Read directly from master database */
        $actualMasterResult = $this->readValueFromMasterDb();

        /**
         * Start a transaction and read from the master-slave connection.
         * In this case the modifications on the slave should be invisible
         */
        $this->masterSlaveConnection->startTransaction();
        $actualMasterSlaveResult = $this->masterSlaveConnection->getOne('SELECT column1 FROM `master_slave_table` WHERE id = 1');
        $this->masterSlaveConnection->commitTransaction();

        $this->assertSame($expectedSlaveResult, $actualSlaveResult, 'Slave connection retrieves modified value ' . $expectedSlaveResult);
        $this->assertSame($this->expectedMasterResult, $actualMasterResult, 'Master connection retrieves unmodified value ' . $this->expectedMasterResult);
        $this->assertSame($this->expectedMasterResult, $actualMasterSlaveResult, 'Master-Slave connection reads from master database after using startTransaction()');
    }

    /**
     * Test Doctrine behavior:
     * "Master is picked when 'exec', 'executeUpdate', 'insert', 'delete', 'update', 'createSavepoint', 'releaseSavepoint', 'beginTransaction', 'rollback', 'commit', 'query' or 'prepare' is called."
     *
     *  \OxidEsales\EshopCommunity\Core\Database\Adapter\Doctrine\Database::startTransaction uses beginTransaction under the hood.
     *
     * @covers \OxidEsales\EshopCommunity\Core\Database\Adapter\Doctrine\Database::commitTransaction
     */
    public function testMasterSlaveConnectionReadsFromMasterAfterCommitTransaction()
    {
        $this->markTestSkippedIfNotMasterSlave();

        $expectedSlaveResult = '100';

        /** Start a transaction, do an update and commit it. */
        $this->masterSlaveConnection->startTransaction();
        $this->masterSlaveConnection->execute('INSERT INTO `master_slave_table` (`column1`, `column2`) VALUES (2, 3)');
        $this->masterSlaveConnection->commitTransaction();

        /** Modify record directly on the slave database, bypassing master-slave replication */
        $this->slaveConnection->query('UPDATE `master_slave_table` SET `column1` = ' . $expectedSlaveResult . ' WHERE id = 1');

        /** Read directly from slave database */
        $actualSlaveResult = $this->readValueFromSlaveDb();

        /** Read directly from master database */
        $actualMasterResult = $this->readValueFromMasterDb();

        /**
         * Read from the master-slave connection. In this case the modifications on the slave should be invisible
         */
        $actualMasterSlaveResult = $this->masterSlaveConnection->getOne('SELECT column1 FROM `master_slave_table` WHERE id = 1');

        $this->assertSame($expectedSlaveResult, $actualSlaveResult, 'Slave connection retrieves modified value ' . $expectedSlaveResult);
        $this->assertSame($this->expectedMasterResult, $actualMasterResult, 'Master connection retrieves unmodified value ' . $this->expectedMasterResult);
        $this->assertSame($this->expectedMasterResult, $actualMasterSlaveResult, 'Master-Slave connection reads from master database after using commitTransaction()');
    }

    /**
     * Test Doctrine behavior:
     * "Master is picked when 'exec', 'executeUpdate', 'insert', 'delete', 'update', 'createSavepoint', 'releaseSavepoint', 'beginTransaction', 'rollback', 'commit', 'query' or 'prepare' is called."
     *
     *  \OxidEsales\EshopCommunity\Core\Database\Adapter\Doctrine\Database::execute uses executeUpdate under the hood.
     *
     * In this test the slave database is use first for reading.
     * The and update is made and the master database should be used for writing
     *
     * @covers \OxidEsales\EshopCommunity\Core\Database\Adapter\Doctrine\Database::execute
     */
    public function testMasterSlaveConnectionWritesToMasterOnExecute()
    {
        $this->markTestSkippedIfNotMasterSlave();

        $expectedSlaveResult = '100';
        $expectedMasterResultAfterExecute = '2';

        /** Modify record directly on the slave database, bypassing master-slave replication */
        $this->slaveConnection->query('UPDATE `master_slave_table` SET `column1` = ' . $expectedSlaveResult . ' WHERE id = 1');

        /** Read directly from slave database */
        $actualSlaveResult = $this->readValueFromSlaveDb();

        /** Read directly from master database */
        $actualMasterResult = $this->readValueFromMasterDb();

        /** @var ResultSetInterface $resultSet */
        $resultSet = $this->masterSlaveConnection->select('SELECT column1 FROM `master_slave_table` WHERE id = 1');
        $actualMasterSlaveResult = $resultSet->fields['column1'];

        $this->masterSlaveConnection->execute('INSERT INTO `master_slave_table` (`column1`, `column2`) VALUES (' . $expectedMasterResultAfterExecute . ', 3)');

        /** Read directly from master database */
        $resultSet = $this->masterConnection->query('SELECT column1 FROM `master_slave_table` WHERE id = 2');
        $row = $resultSet->fetch_assoc();
        $actualMasterResultAfterExecute = $row['column1'];

        $this->assertSame($expectedSlaveResult, $actualSlaveResult, 'Slave connection retrieves modified value ' . $expectedSlaveResult);
        $this->assertSame($this->expectedMasterResult, $actualMasterResult, 'Master connection retrieves unmodified value ' . $expectedSlaveResult);
        $this->assertSame($expectedSlaveResult, $actualMasterSlaveResult, 'Master-Slave connection retrieves modified value ' . $expectedSlaveResult . ' from slave database on select');
        $this->assertSame($expectedMasterResultAfterExecute, $actualMasterResultAfterExecute, 'Master-Slave connection writes new value ' . $this->expectedMasterResult . ' to master database using execute');
    }

    /**
     * @covers \OxidEsales\EshopCommunity\Core\Database\Adapter\Doctrine\Database::metaColumns
     */
    public function testMetaColumnsReadsFromMaster()
    {
        $this->markTestSkippedIfNotMasterSlave();

        /** Alter the table schema on the slave bypassing master-slave connection  */
        $this->slaveConnection->query('ALTER TABLE `master_slave_table`	ADD COLUMN `columnSlave` INT NULL AFTER `column2`;');

        /** Read column meta information for the table. This should happen on the master database */
        $expectedResult = $this->masterSlaveConnection->metaColumns('master_slave_table');

        /** Force pick the master connection and read column meta information again */
        $this->masterSlaveConnection->execute('UPDATE `master_slave_table` SET `column1` = 100 WHERE id = 1');

        $actualResultMaster = $this->masterSlaveConnection->metaColumns('master_slave_table');

        $this->assertEquals($expectedResult, $actualResultMaster, 'metaColumns retrieves data from master connection');
    }

    /**
     * sql_mode is set to an empty string in the session scope during Database::getDb via \OxidEsales\EshopCommunity\Core\Database::setSqlMode.
     * Test, that the initial values on master and on slave is an empty string.
     * There is still the possibility, that the sql_mode is not set by setSqlMode(), but is an empty string for other reasons.
     * The test self::testSetSqlModeAffectsMasterAndSlave excludes this possibility.
     *
     * See http://dev.mysql.com/doc/refman/5.7/en/sql-mode.html for details
     *
     * @covers \OxidEsales\EshopCommunity\Core\DatabaseProvider::setSqlMode
     */
    public function testSetSqlModeInitialValueIsEmptyString()
    {
        $this->markTestSkippedIfNotMasterSlave();

        $query = 'show variables like "sql_mode"';

        /**
         * sqlMode is set to an empty string during Database::getDb via \OxidEsales\EshopCommunity\Core\Database::setSqlMode,
         * so we expect the result of the query to be ''
         */
        $expectedResultSqlMode = '';

        /** At this moment the connection is still pointing to the slave database */
        $actualResultSlaveSqlMode = $this->masterSlaveConnection->getRow($query)['Value'];

        /** Force picking the master connection and execute the query again */
        $this->masterSlaveConnection->execute('ALTER TABLE `master_slave_table`	ADD COLUMN `column3` INT NULL AFTER `column2`;');
        $actualResultMasterSqlMode = $this->masterSlaveConnection->getRow($query)['Value'];

        $this->assertSame($expectedResultSqlMode, $actualResultSlaveSqlMode, 'The initial sql_mode on the slave is an empty string');
        $this->assertSame($expectedResultSqlMode, $actualResultMasterSqlMode, 'The sql_mode on the master is an empty string');
    }

    /**
     * After master connection was picked, Doctrine would never pick the slave connection. forceSlaveConnection() brings
     * us back to the slave for read accesses.
     *
     * @covers \OxidEsales\EshopEnterprise\Core\Database\Adapter\Doctrine\Database::forceSlaveConnection
     */
    public function testForceSlaveConnectionReadsFromSlaveAfterMasterWasPicked()
    {
        if (!$this->isTestableMasterSlaveSetup) {
            $this->markTestSkipped('This test needs a real master-slave setup with exactly one master and one slave in order to work');
        }

        $expectedMasterResult = '10';
        $expectedSlaveResult = '100';


        /** A new value for column1 is set on the master slave connection. The master is picked and this value will be replicated */
        $this->masterSlaveConnection->execute('UPDATE `master_slave_table` SET `column1` = ' . $expectedMasterResult . ' WHERE id = 1');

        $actualMasterSlaveResultOnMaster = $this->masterSlaveConnection->getOne('SELECT column1 FROM `master_slave_table` WHERE id = 1');

        /** Modify record directly on the slave database, bypassing master-slave replication */
        $this->slaveConnection->query('UPDATE `master_slave_table` SET `column1` = ' . $expectedSlaveResult . ' WHERE id = 1');

        /** Force the slave connection to be picked */
        $this->masterSlaveConnection->forceSlaveConnection();
        $actualMasterSlaveResultOnSlave = $this->masterSlaveConnection->getOne('SELECT column1 FROM `master_slave_table` WHERE id = 1');


        $this->assertSame($expectedMasterResult, $actualMasterSlaveResultOnMaster, 'Master-Slave connection reads from master database after update');
        $this->assertSame($expectedSlaveResult, $actualMasterSlaveResultOnSlave, 'Master-Slave connection reads from slave database after forceSlaveConnection()');
    }

    /**
     * After forceSlaveConnection() the slave connection is picked for read accesses. forceMasterConnection() brings us
     * back to the the master for read accesses.
     *
     * @covers \OxidEsales\EshopEnterprise\Core\Database\Adapter\Doctrine\Database::forceSlaveConnection
     * @covers \OxidEsales\EshopEnterprise\Core\Database\Adapter\Doctrine\Database::forceMasterConnection
     */
    public function testForceMasterConnectionReadsFromMasterAfterSlaveWasPicked()
    {
        if (!$this->isTestableMasterSlaveSetup) {
            $this->markTestSkipped('This test needs a real master-slave setup with exactly one master and one slave in order to work');
        }

        $expectedSlaveResult = '100';

        /** Modify record directly on the slave database, bypassing master-slave replication */
        $this->slaveConnection->query('UPDATE `master_slave_table` SET `column1` = ' . $expectedSlaveResult . ' WHERE id = 1');

        /** Force the slave connection to be picked */
        $this->masterSlaveConnection->forceSlaveConnection();
        $actualMasterSlaveResultOnSlave = $this->masterSlaveConnection->getOne('SELECT column1 FROM `master_slave_table` WHERE id = 1');

        /** Force the master connection to be picked */
        $this->masterSlaveConnection->forceMasterConnection();
        $actualMasterSlaveResultOnMaster = $this->masterSlaveConnection->getOne('SELECT column1 FROM `master_slave_table` WHERE id = 1');

        $this->assertSame($expectedSlaveResult, $actualMasterSlaveResultOnSlave, 'Master-Slave connection reads from slave database after forceSlaveConnection()');
        $this->assertSame($this->expectedMasterResult, $actualMasterSlaveResultOnMaster, 'Master-Slave connection reads from master database after forceMasterConnection()');
    }

    /**
     * Right after database bootstrap the slave connection should be picked.
     */
    public function testInitiallyTheSlaveConnectionIsPicked()
    {
        if (!$this->isTestableMasterSlaveSetup) {
            $this->markTestSkipped('This test needs a real master-slave setup with exactly one master and one slave in order to work');
        }

        $expectedSlaveResult = '100';

        /** Modify record directly on the slave database, bypassing master-slave replication */
        $this->slaveConnection->query('UPDATE `master_slave_table` SET `column1` = ' . $expectedSlaveResult . ' WHERE id = 1');

        $actualMasterSlaveResultOnSlave = $this->masterSlaveConnection->getOne('SELECT column1 FROM `master_slave_table` WHERE id = 1');

        $this->assertSame($expectedSlaveResult, $actualMasterSlaveResultOnSlave, 'Master-Slave connection reads from slave database after database bootstrap');
    }

    /**
     * Get a dedicated database connection to the master
     *
     * @return \mysqli Returns an object which represents the connection to a MySQL Server.
     */
    protected function getConnectionMaster()
    {
        $configKeyDatabaseHost = 'dbHost';
        $configKeyDatabasePort = 'dbPort';
        $configKeyDatabaseName = 'dbName';
        $configKeyDatabaseUser = 'dbUser';
        $configKeyDatabasePassword = 'dbPwd';

        $mysqli = $this->getDatabaseConnection($configKeyDatabaseHost, $configKeyDatabasePort, $configKeyDatabaseName, $configKeyDatabaseUser, $configKeyDatabasePassword);

        return $mysqli;
    }

    /**
     * Get a dedicated database connection to the slave
     *
     * @return \mysqli Returns an object which represents the connection to a MySQL Server.
     */
    protected function getConnectionSlave()
    {
        $configKeyDatabaseHost = 'aSlaveHosts';
        $configKeyDatabasePort = 'dbPort';
        $configKeyDatabaseName = 'dbName';
        $configKeyDatabaseUser = 'dbUser';
        $configKeyDatabasePassword = 'dbPwd';

        $mysqli = $this->getDatabaseConnection($configKeyDatabaseHost, $configKeyDatabasePort, $configKeyDatabaseName, $configKeyDatabaseUser, $configKeyDatabasePassword);

        return $mysqli;
    }

    /**
     * Get a master slave database connection
     */
    protected function getConnectionMasterSlave()
    {
        return DatabaseProvider::getDb(DatabaseProvider::FETCH_MODE_ASSOC);
    }

    /**
     * @param string $configKeyDatabaseHost
     * @param string $configKeyDatabasePort
     * @param string $configKeyDatabaseName
     * @param string $configKeyDatabaseUser
     * @param string $configKeyDatabasePassword
     *
     * @return \mysqli Returns an object which represents the connection to a MySQL Server.
     */
    protected function getDatabaseConnection($configKeyDatabaseHost, $configKeyDatabasePort, $configKeyDatabaseName, $configKeyDatabaseUser, $configKeyDatabasePassword)
    {
        /** @var string $host Host name or IP address.
         * The config param for $configKeyDatabaseHost might be array in
         * case of a slave connection. In this case the first value of the array is chosen.
         */
        $host = is_array($this->getConfigParam($configKeyDatabaseHost)) ? $this->getConfigParam($configKeyDatabaseHost)[0] : $this->getConfigParam($configKeyDatabaseHost);
        $port = $this->getConfigParam($configKeyDatabasePort) ? $this->getConfigParam($configKeyDatabasePort) : 3306;
        $database = $this->getConfigParam($configKeyDatabaseName);
        $user = $this->getConfigParam($configKeyDatabaseUser);
        $password = $this->getConfigParam($configKeyDatabasePassword);

        $mysqli = new \mysqli($host, $user, $password, $database, $port);
        if ($mysqli->connect_error) {
            $this->fail(
                'Connect Error (' . $mysqli->connect_errno . ') '
                . $mysqli->connect_error
            );
        }

        return $mysqli;
    }

    /**
     * @param \mysqli $masterConnection
     *
     * @return string
     */
    protected function createMasterSlaveTable(\mysqli $masterConnection)
    {
        /** Create `master_slave_table` */
        $query = <<<EOT
                CREATE TABLE `master_slave_table` (
                    `id` INT(11) NOT NULL AUTO_INCREMENT,
                    `column1` INT(11) NULL DEFAULT NULL,
                    `column2` INT(11) NULL DEFAULT NULL,
                    PRIMARY KEY (`id`)
                )
                COMMENT='Temporary table to test master slave behaviour'
                COLLATE='latin1_general_ci'
                ENGINE=InnoDB
                ;
EOT;
        $masterConnection->query($query);

        return $query;
    }

    /**
     * @param \mysqli $masterConnection
     *
     * @return string
     */
    protected function dropMasterSlaveTable(\mysqli $masterConnection)
    {
        /** Remove `master_slave_table` */
        $query = "DROP TABLE IF EXISTS `master_slave_table`";
        $masterConnection->query($query);
    }

    /**
     * @return string
     */
    protected function readValueFromSlaveDb()
    {
        $resultSet = $this->slaveConnection->query('SELECT column1 FROM `master_slave_table` WHERE id = 1');
        $row = $resultSet->fetch_assoc();
        $value = $row['column1'];

        return $value;
    }

    /**
     * @return string
     */
    protected function readValueFromMasterDb()
    {
        $resultSet = $this->masterConnection->query('SELECT column1 FROM `master_slave_table` WHERE id = 1');
        $row = $resultSet->fetch_assoc();
        $value = $row['column1'];

        return $value;
    }

    /**
     * @param $expectedMasterResult
     */
    protected function populateTestTable($expectedMasterResult)
    {

        $this->masterConnection->query('INSERT INTO `master_slave_table` (`column1`, `column2`) VALUES (' . $expectedMasterResult . ', 2)');
        /** Pause to let replication take place */
        sleep(2);
    }

    private function setTestableMasterSlaveSetup()
    {
        $slaveHosts = $this->getConfig()->getConfigParam('aSlaveHosts');
        $masterHostname = $this->getConfig()->getConfigParam('dbHost');
        if (is_array($slaveHosts) &&
            count($slaveHosts) === 1 &&
            $masterHostname != $slaveHosts[0]
        ) {
            $this->isTestableMasterSlaveSetup = true;
        }
    }

    /**
     * Mark this test as skipped, if we are not on a master/slave setup.
     */
    protected function markTestSkippedIfNotMasterSlave()
    {
        if (!$this->isTestableMasterSlaveSetup) {
            $this->markTestSkipped('This test needs a real master-slave setup with exactly one master and one slave in order to work');
        }
    }

}
