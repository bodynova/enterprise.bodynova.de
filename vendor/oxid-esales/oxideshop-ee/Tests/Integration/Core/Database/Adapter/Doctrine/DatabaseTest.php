<?php
/**
 * This Software is the property of OXID eSales and is protected
 * by copyright law - it is NOT Freeware.
 *
 * Any unauthorized use of this software without a valid license key
 * is a violation of the license agreement and will be prosecuted by
 * civil and criminal law.
 *
 * @link      http://www.oxid-esales.com
 * @copyright (C) OXID eSales AG 2003-2016
 * @version   OXID eShop EE
 */

namespace OxidEsales\EshopEnterprise\Tests\Integration\Core\Database\Adapter\Doctrine;

use PDO;
use OxidEsales\EshopEnterprise\Core\Database\Adapter\Doctrine\Database as DatabaseAdapter;
use OxidEsales\EshopEnterprise\Core\DatabaseProvider;
use OxidEsales\TestingLibrary\UnitTestCase;

/**
 * Class DatabaseTest
 *
 * @covers OxidEsales\EshopEnterprise\Core\Database\Adapter\Doctrine\Database
 */
class DatabaseTest extends UnitTestCase
{

    /**
     * @var DatabaseAdapter The database to test.
     */
    protected $database = null;

    /**
     * Initialize database
     */
    public function setUp()
    {
        parent::setUp();

        $this->initializeDatabase();
        $this->setProtectedClassProperty($this->database, 'connectionParameters', array());
    }

    /**
     * Empty database table after every test
     */
    public function tearDown()
    {
        $this->database->closeConnection();
        parent::tearDown();
    }

    /**
     * Provides an error handler
     *
     * @param integer $errorLevel   Error number as defined in http://php.net/manual/en/errorfunc.constants.php
     * @param string  $errorMessage Error message
     * @param string  $errorFile    Error file
     * @param integer $errorLine    Error line
     * @param array   $errorContext Error context
     */
    public function errorHandler($errorLevel, $errorMessage, $errorFile, $errorLine, $errorContext)
    {
        $this->errors[] = compact(
            "errorLevel",
            "errorMessage",
            "errorFile",
            "errorLine",
            "errorContext"
        );
    }

    /**
     * Create the database, we want to test.
     */
    protected function initializeDatabase()
    {
        $this->database = DatabaseProvider::getDb();
    }

    /*
     * Use Case: All possible parameters for db connection are set
     */
    public function testSetConnectionParametersAllParametersSet()
    {
        $this->setProtectedClassProperty($this->database, 'connectionParameters', array());
        $connectionParametersFromConfigInc = array(
            'default' => array(
                'databaseHost'     => 'myMasterDatabaseHost',
                'databaseName'     => 'myMasterDatabaseName',
                'databaseUser'     => 'myMasterDatabaseUser',
                'databasePassword' => 'myMasterDatabasePassword',
                'databasePort'     => 'myMasterDatabasePort'
            ),
            'slaves'  => array(
                array(
                    'databaseHost'     => 'mySlave1DatabaseHost',
                    'databasePort'     => 'mySlave1DatabasePort',
                    'databaseName'     => 'mySlave1DatabaseName',
                    'databaseUser'     => 'mySlave1DatabaseUser',
                    'databasePassword' => 'mySlave1DatabasePassword',
                ),
                array(
                    'databaseHost'     => 'mySlave2DatabaseHost',
                    'databasePort'     => 'mySlave2DatabasePort',
                    'databaseName'     => 'mySlave2DatabaseName',
                    'databaseUser'     => 'mySlave2DatabaseUser',
                    'databasePassword' => 'mySlave2DatabasePassword',
                )
            )
        );

        $this->database->setConnectionParameters($connectionParametersFromConfigInc);

        $expectedConnectionParameters = array(
            'wrapperClass' => 'Doctrine\DBAL\Connections\MasterSlaveConnection',
            'keepSlave'    => true,
            'driver'       => 'pdo_mysql',
            'master'       => array(
                'host'     => 'myMasterDatabaseHost',
                'dbname'   => 'myMasterDatabaseName',
                'user'     => 'myMasterDatabaseUser',
                'password' => 'myMasterDatabasePassword',
                'port'     => 'myMasterDatabasePort'
            ),
            'slaves'       => array(
                array(
                    'host'     => 'mySlave1DatabaseHost',
                    'port'     => 'mySlave1DatabasePort',
                    'dbname'   => 'mySlave1DatabaseName',
                    'user'     => 'mySlave1DatabaseUser',
                    'password' => 'mySlave1DatabasePassword'
                ),
                array(
                    'host'     => 'mySlave2DatabaseHost',
                    'port'     => 'mySlave2DatabasePort',
                    'dbname'   => 'mySlave2DatabaseName',
                    'user'     => 'mySlave2DatabaseUser',
                    'password' => 'mySlave2DatabasePassword'
                )
            ),
            'driverOptions' => array(
                PDO::MYSQL_ATTR_INIT_COMMAND => "SET @@SESSION.sql_mode=''"
            )
        );

        $this->assertEquals(
            $expectedConnectionParameters,
            $this->getProtectedClassProperty($this->database, 'connectionParameters')
        );
    }

    /*
     * Use Case: All possible parameters for db connection are set
     */
    public function testSetConnectionParametersNoParameters()
    {
        $this->setProtectedClassProperty($this->database, 'connectionParameters', array());
        $connectionParametersFromConfigInc = array();
        $this->database->setConnectionParameters($connectionParametersFromConfigInc);
        $this->assertEquals(
            array(),
            $this->getProtectedClassProperty($this->database, 'connectionParameters'),
            "There can be no parameters in the array with no input parameters."
        );
    }

}
