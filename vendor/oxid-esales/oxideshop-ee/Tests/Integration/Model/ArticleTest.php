<?php
/**
 * This Software is the property of OXID eSales and is protected
 * by copyright law - it is NOT Freeware.
 *
 * Any unauthorized use of this software without a valid license key
 * is a violation of the license agreement and will be prosecuted by
 * civil and criminal law.
 *
 * @link      http://www.oxid-esales.com
 * @copyright (C) OXID eSales AG 2003-2016
 * @version   OXID eShop EE
 */

namespace OxidEsales\EshopEnterprise\Tests\Integration\Model;

use oxDb;
use oxField;
use oxRegistry;


/**
 * oxArticle integration test
 */
class ArticleTest extends \oxUnitTestCase
{

    /**
     * Test setup
     */
    public function setUp()
    {
        parent::setUp();

        $id = $this->getShopId();
        $this->addToDatabase("Insert into oxarticles (oxid, oxshopid, oxtitle, oxprice) values ('_testid', '{$id}', '_testArticle', '125')", 'oxarticles', array($id, 2, 3, 4, 5));
        $this->addToDatabase("Insert into oxarticles (oxid, oxparentid, oxshopid, oxtitle, oxprice) values ('_testidvariant', '_testid', '{$id}', '_testArticleVariant', '1250')", 'oxarticles');
        $this->addTableForCleanup('oxarticles');
    }

    /**
     * Integration test that checks if after deleting article, maps are deleted from subshops
     */
    public function testDeleteFromMaps()
    {
        $article = oxNew('oxArticle');
        $article->load('_testid');

        $mapId = oxDb::getDb()->getOne("select oxmapid from oxarticles where oxid = '_testid'");
        $count = (int) oxDb::getDb()->getOne("select count(*) from oxarticles2shop where oxmapobjectid={$mapId}");
        $this->assertEquals(5, $count);
        $this->assertEquals('_testid', $article->getId());

        $article->delete('_testid');
        $count = (int) oxDb::getDb()->getOne("select count(*) from oxarticles2shop where oxmapobjectid={$mapId}");
        $this->assertEquals(0, $count);

    }

    /**
     * Check whether assigning article to subshop and changing it's price there and then unassigning it
     * leaves oxfield2shop value
     *
     * Test for bug #5461
     */
    public function testInheritedArticlePriceChangeAndUnassignAfterFromShop()
    {
        $subShop = oxNew('oxShop');
        $subShop->setEnableMultilang(true);
        $subShop->oxshops__oxisinherited = new oxField(0);
        $subShop->oxshops__oxparentid = new oxField(1);
        $subShop->save();

        $subShopId = $subShop->getId();

        $config = oxRegistry::getConfig();
        $langParams = $config->getConfigParam('aLanguageParams');
        $languages = $config->getConfigParam('aLanguages');
        $config->saveShopConfVar('aarr', 'aLanguageParams', $langParams, $subShopId);
        $config->saveShopConfVar('aarr', 'aLanguages', $languages, $subShopId);
        $subShop->generateViews();

        $article = oxNew('oxArticle');
        $article->load("_testid");
        $article->assignToShop($subShopId);

        $config->setShopId($subShopId);

        $article = oxNew('oxArticle');
        $article->load("_testid");
        $article->oxarticles__oxprice = new oxField(250);
        $article->save();

        $config->setShopId(1);

        $article = oxNew('oxArticle');
        $article->load("_testid");
        $this->assertEquals(125, $article->getPrice()->getPrice(), "price of article is incorrct in parent shop");

        $config->setShopId($subShopId);

        $article = oxNew('oxArticle');
        $article->load("_testid");
        $this->assertEquals(250, $article->getPrice()->getPrice(), "price of article is incorrect in subshop");

        $article->unassignFromShop(array($subShopId));
        $count = oxDb::getDb()->getOne("SELECT count(*) FROM `oxfield2shop` WHERE `oxartid` = '_testid' AND `oxshopid` = {$subShopId}");
        $this->assertEquals(0, $count, 'after unassigning article from subshop, oxfield2shop values remain');

        $config->setShopId(1);

        $article = oxNew('oxArticle');
        $article->load("_testid");
        $this->assertEquals(125, $article->getPrice()->getPrice(), "price of article should not change in parent shop");

        $article->assignToShop($subShopId);
        $count = oxDb::getDb()->getOne("SELECT count(*) FROM `oxfield2shop` WHERE `oxartid` = '_testid' AND `oxshopid` = {$subShopId}");
        $this->assertEquals(0, $count, 'oxfield2shop article values should not be present after the first unassignment');

        $config->setShopId($subShopId);

        $article = oxNew('oxArticle');
        $article->load("_testid");
        $this->assertEquals(125, $article->getPrice()->getPrice(), "price of article should not change in subshop");
    }

    /**
     * Check whether assigning article to subshop and changing it's variant price there and then unassigning it
     * leaves oxfield2shop value
     */
    public function testInheritedArticleVariantPriceChangeAndUnassignParentAfterFromShop()
    {
        $subShop = oxNew('oxShop');
        $subShop->setEnableMultilang(true);
        $subShop->oxshops__oxisinherited = new oxField(0);
        $subShop->oxshops__oxparentid = new oxField(1);
        $subShop->save();

        $subShopId = $subShop->getId();

        $config = oxRegistry::getConfig();
        $langParams = $config->getConfigParam('aLanguageParams');
        $languages = $config->getConfigParam('aLanguages');
        $config->saveShopConfVar('aarr', 'aLanguageParams', $langParams, $subShopId);
        $config->saveShopConfVar('aarr', 'aLanguages', $languages, $subShopId);
        $subShop->generateViews();

        $article = oxNew('oxArticle');
        $article->load("_testid");
        $article->assignToShop($subShopId);

        $articleVariant = oxNew('oxArticle');
        $articleVariant->load('_testidvariant');
        $articleVariant->assignToShop($subShopId);

        $config->setShopId($subShopId);

        $articleVariant = oxNew('oxArticle');
        $articleVariant->load("_testidvariant");
        $articleVariant->oxarticles__oxprice = new oxField(250);
        $articleVariant->save();

        $config->setShopId(1);

        $article = oxNew('oxArticle');
        $article->load("_testid");
        $this->assertEquals(125, $article->getPrice()->getPrice(), "price of article is incorrct in parent shop");

        $articleVariant = oxNew('oxArticle');
        $articleVariant->load("_testidvariant");
        $this->assertEquals(1250, $articleVariant->getPrice()->getPrice(), "price of article variant is incorrct in parent shop");

        $config->setShopId($subShopId);

        $article = oxNew('oxArticle');
        $article->load("_testid");
        $this->assertEquals(125, $article->getPrice()->getPrice(), "price of article is incorrct in subshop");

        $articleVariant = oxNew('oxArticle');
        $articleVariant->load("_testidvariant");
        $this->assertEquals(250, $articleVariant->getPrice()->getPrice(), "price of article variant is incorrct in subshop");

        $article->unassignFromShop(array($subShopId));
        $count = oxDb::getDb()->getOne("SELECT count(*) FROM `oxfield2shop` WHERE `oxartid` = '_testidvariant' AND `oxshopid` = {$subShopId}");
        $this->assertEquals(0, $count, 'after unassigning article from subshop, article varnati oxfield2shop values remain');

        $config->setShopId(1);

        $article = oxNew('oxArticle');
        $article->load("_testid");
        $this->assertEquals(125, $article->getPrice()->getPrice(), "price of article is incorrct in parent shop");

        $articleVariant = oxNew('oxArticle');
        $articleVariant->load("_testidvariant");
        $this->assertEquals(1250, $articleVariant->getPrice()->getPrice(), "price of article variant is incorrct in parent shop");

        $article->assignToShop($subShopId);
        $count = oxDb::getDb()->getOne("SELECT count(*) FROM `oxfield2shop` WHERE `oxartid` = '_testidvariant' AND `oxshopid` = {$subShopId}");
        $this->assertEquals(0, $count, 'oxfield2shop article variant values should not be present after the first unassignment');

        $article = oxNew('oxArticle');
        $article->load("_testid");
        $this->assertEquals(125, $article->getPrice()->getPrice(), "price of article is incorrct in subshop");

        $articleVariant = oxNew('oxArticle');
        $articleVariant->load("_testidvariant");
        $this->assertEquals(1250, $articleVariant->getPrice()->getPrice(), "price of article variant is incorrct in subshop");
    }
}