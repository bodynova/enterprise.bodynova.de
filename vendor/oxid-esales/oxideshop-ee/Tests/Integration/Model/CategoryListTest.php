<?php
/**
 * This Software is the property of OXID eSales and is protected
 * by copyright law - it is NOT Freeware.
 *
 * Any unauthorized use of this software without a valid license key
 * is a violation of the license agreement and will be prosecuted by
 * civil and criminal law.
 *
 * @link      http://www.oxid-esales.com
 * @copyright (C) OXID eSales AG 2003-2016
 * @version   OXID eShop EE
 */

namespace OxidEsales\EshopEnterprise\Tests\Integration\Model;

use \oxField;
use \OxidEsales\TestingLibrary\UnitTestCase;

/**
 * Category tree regeneration test cases:
 * - after addding new category;
 * - after deleting category;
 * - after updating category data:
 *   - updating title etc.;
 *   - adding new article to category;
 *   - removing article from category;
 * - on category visibility change ( article stock goes to 0 );
 * - after adding removing content category;
 * - after updating content category;
 *
 */
class CategoryListTest extends UnitTestCase
{
    /**
     * Initialize the fixture.
     *
     * @return null
     */
    protected function setUp()
    {
        parent::setUp();
        $this->setConfigParam( 'blCacheActive', true );
        $this->setConfigParam( 'sDefaultCacheConnector', 'oxFileCacheConnector' );
    }

    /**
     * Tear down the fixture.
     *
     * @return null
     */
    protected function tearDown()
    {
        $this->cleanUpTable('oxcategories');
        $cache = $this->getCacheBackend();
        $cache->flush();

        parent::tearDown();
    }

    /**
     * After adding / deleting category
     */
    public function testAddOrDeleteCategory()
    {
        $categoryTree = oxNew('oxCategoryList');
        $categoryTree->load();

        //checking count of loaded categories
        $categoryCount = count( $categoryTree );

        //adding new category
        $category = oxNew('oxCategory');
        $category->setId('_testCategory');
        $category->oxcategories__oxparentid = new oxField('oxrootid');
        $category->oxcategories__oxtitle  = new oxField('testCategory');
        $category->save();

        //checking count of loaded categories after adding new category
        $categoryTree->load();
        $this->assertEquals( $categoryCount + 1, count( $categoryTree ) );

        $category->delete();
        //checking count of loaded categories after deleted category
        $categoryTree->load();
        $this->assertEquals( $categoryCount, count( $categoryTree ) );
    }

    /**
     * After updating category
     */
    public function testUpdateCategory()
    {
        //adding new category
        $category = oxNew('oxCategory');
        $category->setId('_testCategory');
        $category->oxcategories__oxparentid = new oxField('oxrootid');
        $category->oxcategories__oxtitle  = new oxField('testCategory');
        $category->save();

        //checking count of loadded categories after adding new category
        $categoryTree = oxNew('oxCategoryList');
        $categoryTree->buildTree(null);
        $this->assertEquals( 'testCategory', $categoryTree['_testCategory']->oxcategories__oxtitle->value );

        //updating category title
        $category->oxcategories__oxtitle  = new oxField('testCategoryEdited');
        $category->save();

        $categoryTree->buildTree(null);
        $this->assertEquals( 'testCategoryEdited', $categoryTree['_testCategory']->oxcategories__oxtitle->value );
    }

    /**
     * After adding / deleting content category
     */
    public function testAddOrDeleteContentCategory()
    {
        $categoryTree = oxNew('oxCategoryList');

        //adding new category
        $category = oxNew('oxCategory');
        $category->setId('_testCategory');
        $category->oxcategories__oxparentid = new oxField('oxrootid');
        $category->oxcategories__oxtitle  = new oxField('testCategory');
        $category->save();

        $categoryTree->buildTree(null);
        $this->assertEquals( 0, count($categoryTree['_testCategory']->getContentCats()) );

        //adding new content category
        $content = oxNew('oxContent');
        $content->setId('_testContent');
        $content->oxcontents__oxtype = new oxField( 2 );
        $content->oxcontents__oxtitle = new oxField( 'testContentCategory' );
        $content->oxcontents__oxactive = new oxField( 1 );
        $content->oxcontents__oxcatid = new oxField( '_testCategory' );
        $content->oxcontents__oxsnippet= new oxField( 0 );
        $content->save();

        //checking count of loadded categories after adding new category
        $categoryTree->buildTree(null);
        $this->assertEquals( 1, count($categoryTree['_testCategory']->getContentCats()) );

        $content->delete();
        $categoryTree->buildTree(null);
        $this->assertEquals( 0, count($categoryTree['_testCategory']->getContentCats()) );

    }

    /**
     * After updating content category
     */
    public function testUpdateContentCategory()
    {
        $categoryTree = oxNew('oxCategoryList');

        //adding new category
        $category = oxNew('oxCategory');
        $category->setId('_testCategory');
        $category->oxcategories__oxparentid = new oxField('oxrootid');
        $category->oxcategories__oxtitle  = new oxField('testCategory');
        $category->save();

        //adding new content category
        $content = oxNew('oxContent');
        $content->setId('_testContent');
        $content->oxcontents__oxtype = new oxField( 2 );
        $content->oxcontents__oxtitle = new oxField( 'testContentCategory' );
        $content->oxcontents__oxactive = new oxField( 1 );
        $content->oxcontents__oxcatid = new oxField( '_testCategory' );
        $content->oxcontents__oxsnippet= new oxField( 0 );
        $content->save();

        //checking count of loadded categories after adding new category
        $categoryTree->buildTree(null);
        $contCats = $categoryTree['_testCategory']->getContentCats();
        $this->assertEquals( 'testContentCategory', $contCats[0]->oxcontents__oxtitle->value );

        //update
        $content->oxcontents__oxtitle = new oxField( 'testContentCategoryEdited' );
        $content->save();

        $categoryTree->buildTree(null);
        $contCats = $categoryTree['_testCategory']->getContentCats();
        $this->assertEquals( 'testContentCategoryEdited', $contCats[0]->oxcontents__oxtitle->value );
    }
}
