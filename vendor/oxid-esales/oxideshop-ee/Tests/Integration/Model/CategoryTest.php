<?php
/**
 * This Software is the property of OXID eSales and is protected
 * by copyright law - it is NOT Freeware.
 *
 * Any unauthorized use of this software without a valid license key
 * is a violation of the license agreement and will be prosecuted by
 * civil and criminal law.
 *
 * @link      http://www.oxid-esales.com
 * @copyright (C) OXID eSales AG 2003-2016
 * @version   OXID eShop EE
 */

namespace OxidEsales\EshopEnterprise\Tests\Integration\Model;

use \oxField;
use \oxDb;
use \OxidEsales\TestingLibrary\UnitTestCase;

/**
 *
 * Article caching in cache backend cases:
 *
 * - key generation;
 * - select: first time from db, second from cache;
 * - update;
 * - delete: cache by ident should be empty;
 * - invalidation keys generation;
 *
 */
class CategoryTest extends UnitTestCase
{
    /**
     * Initialize the fixture.
     *
     * @return null
     */
    protected function setUp()
    {
        parent::setUp();
        $this->setConfigParam( 'blCacheActive', true );
        $this->setConfigParam( 'sDefaultCacheConnector', 'oxFileCacheConnector' );
    }

    /**
     * Tear down the fixture.
     *
     * @return null
     */
    protected function tearDown()
    {
        $this->cleanUpTable('oxcategories');
        $cache = $this->getCacheBackend();
        $cache->flush();

        parent::tearDown();
    }


    /**
     * Data provider: return prepared oxCategory object for testing
     */
    protected function _dpCategoryObject()
    {
        $category = oxNew('oxCategory');
        $category->setId('_testCategory');
        $category->oxcategories__oxparentid = new oxField('oxrootid');
        $category->oxcategories__oxtitle  = new oxField('testCategory');
        $category->save();

        return $category;
    }


    /**
     * Cache key generation
     */
    public function testGetCacheKey()
    {
        $category = $this->_dpCategoryObject();
        $this->assertEquals( 'oxCategory__testCategory_1_de', $category->getCacheKey('_testCategory') );
        $this->assertEquals( 'oxCategory__testCategory_1_de', $category->getCacheKey() );
    }

    /**
     * Select: if info in a cache getting it from cache
     *
     */
    public function testSelect()
    {
        $cache = $this->getCacheBackend();
        $category = $this->_dpCategoryObject();

        // cache empty
        $this->assertNull($cache->get( 'oxCategory__testCategory_1_de' ));

        // load
        $category->load('_testCategory');
        // cache filed
        $this->assertNotNull($cache->get( 'oxCategory__testCategory_1_de' ));

        // change in directly to db
        oxDb::getDb()->execute("UPDATE `oxcategories` SET `oxtitle` = 'testCategoryUpdated' WHERE `oxid` = '_testCategory'");

        //testing if it loaded from cache
        $category = oxNew('oxCategory');
        $category->load('_testCategory');
        $this->assertEquals( 'testCategory', $category->oxcategories__oxtitle->value );
    }

    /**
     * delete: after deletion cache with this key empty
     *
     */
    public function testDelete()
    {
        $cache = $this->getCacheBackend();
        $category = $this->_dpCategoryObject();

        // cache empty
        $this->assertNull($cache->get( 'oxCategory__testCategory_1_de' ));

        // load
        $category->load('_testCategory');

        // cache filed
        $this->assertNotNull($cache->get( 'oxCategory__testCategory_1_de' ));

        // delete
        $category->delete();

        // cache empty
        $this->assertNull( $cache->get( 'oxCategory__testCategory_1_de' ) );

        //testing if it is deleted
        $category = oxNew('oxCategory');
        $this->assertFalse( $category->load('_testCategory') );
    }

    /**
     * Update: after we excpect that cache is invalidated
     *
     */
    public function testUpdate()
    {
        $cache = $this->getCacheBackend();
        $category = $this->_dpCategoryObject();

        // cache empty
        $this->assertNull($cache->get( 'oxCategory__testCategory_1_de' ));

        // load
        $category->load('_testCategory');

        // cache filed
        $this->assertNotNull($cache->get( 'oxCategory__testCategory_1_de' ));

        // update
        $category->oxcategories__oxtitle = new oxField( 'testCategoryUpdated' );
        $category->save();

        // cache empty
        $this->assertNull( $cache->get( 'oxCategory__testCategory_1_de' ) );

        //testing update
        $category = oxNew('oxCategory');
        $category->load('_testCategory');
        $this->assertEquals( 'testCategoryUpdated', $category->oxcategories__oxtitle->value );
    }
}
