<?php
/**
 * This Software is the property of OXID eSales and is protected
 * by copyright law - it is NOT Freeware.
 *
 * Any unauthorized use of this software without a valid license key
 * is a violation of the license agreement and will be prosecuted by
 * civil and criminal law.
 *
 * @link      http://www.oxid-esales.com
 * @copyright (C) OXID eSales AG 2003-2016
 * @version   OXID eShop EE
 */

namespace OxidEsales\EshopEnterprise\Tests\Integration\Model;

use \oxField;
use \oxDb;
use OxidEsales\TestingLibrary\UnitTestCase;

/**
 *
 * Content caching in cache backend cases:
 *
 * - key generation: base is loadid, not oxid;
 * - select from cache;
 * - update: cache invalidation;
 * - delete: cache by ident should be empty;
 *
 */
class ContentTest extends UnitTestCase
{
    /**
     * Initialize the fixture.
     *
     * @return null
     */
    protected function setUp()
    {
        parent::setUp();
        $this->setConfigParam( 'blCacheActive', true );
        $this->setConfigParam( 'sDefaultCacheConnector', 'oxFileCacheConnector' );
    }

    /**
     * Tear down the fixture.
     *
     * @return null
     */
    protected function tearDown()
    {
        $this->cleanUpTable('oxcontents');
        $cache = $this->getCacheBackend();
        $cache->flush();

        parent::tearDown();
    }

    /**
     * Data provider: return prepared oxContent object for testing
     */
    protected function _dpContentObject()
    {
        $content = oxNew('oxContent');
        $content->setId('_testContent');
        $content->oxcontents__oxloadid = new oxField( '_testLoadId' );
        $content->oxcontents__oxtype = new oxField( 2 );
        $content->oxcontents__oxtitle = new oxField( 'testContentCategory' );
        $content->oxcontents__oxactive = new oxField( 1 );
        $content->oxcontents__oxsnippet= new oxField( 0 );
        $content->save();

        return $content;
    }


    /**
     * Cache key generation
     */
    public function testGetCacheKey()
    {
        $content = $this->_dpContentObject();

        $this->assertEquals( 'oxContent__testLoadId_1_de', $content->getCacheKey('_testLoadId') );
        $this->assertEquals( 'oxContent__testLoadId_1_de', $content->getCacheKey() );

        //Setting different load id
        $content->oxcontents__oxloadid = new oxField( '_testLoadIdDifferet' );
        $this->assertEquals( 'oxContent__testLoadIdDifferet_1_de', $content->getCacheKey('_testLoadIdDifferet') );

        // must get from db, if load id not passed
        $this->assertEquals( 'oxContent__testLoadId_1_de', $content->getCacheKey() );
    }

    /**
     * Update content: after we excpect that cache is invalidated
     *
     */
    public function testUpdate()
    {
        $cache = $this->getCacheBackend();
        $content = $this->_dpContentObject();

        // cache empty
        $this->assertNull($cache->get( 'oxContent__testLoadId_1_de' ));

        // load
        $content->loadByIdent('_testLoadId');

        // cache filed
        $this->assertNotNull($cache->get( 'oxContent__testLoadId_1_de' ));

        // update
        $content->oxcontents__oxtitle = new oxField( 'testContentCategoryUpdated' );
        $content->save();

        // cache empty
        $this->assertNull( $cache->get( 'oxContent__testLoadId_1_de' ) );

        //testing update
        $content = oxNew('oxContent');
        $content->loadByIdent('_testLoadId');
        $this->assertEquals( 'testContentCategoryUpdated', $content->oxcontents__oxtitle->value );
    }

    /**
     * delete content: after deletion cache with this key empty
     *
     */
    public function testDelete()
    {
        $cache = $this->getCacheBackend();
        $content = $this->_dpContentObject();

        // cache empty
        $this->assertNull($cache->get( 'oxContent__testLoadId_1_de' ));

        // load
        $content->loadByIdent('_testLoadId');

        // cache filed
        $this->assertNotNull($cache->get( 'oxContent__testLoadId_1_de' ));

        // delete
        $content->delete();

        // cache empty
        $this->assertNull( $cache->get( 'oxContent__testLoadId_1_de' ) );

        //testing if it is deleted
        $content = oxNew('oxContent');
        $this->assertFalse( $content->loadByIdent('_testLoadId') );
    }

    /**
     * Select content: if info in a cache getting it from cache
     *
     */
    public function testSelect()
    {
        $cache = $this->getCacheBackend();
        $content = $this->_dpContentObject();

        // cache empty
        $this->assertNull($cache->get( 'oxContent__testLoadId_1_de' ));

        // load
        $content->loadByIdent('_testLoadId');
        // cache filed
        $this->assertNotNull($cache->get( 'oxContent__testLoadId_1_de' ));

        // change in directly to db
        oxDb::getDb()->execute("UPDATE `oxcontents` SET `oxtitle` = 'testContentCategoryUpdated' WHERE `oxid` = '_testLoadId'");

        //testing if it loaded from cache
        $content = oxNew('oxContent');
        $content->loadByIdent('_testLoadId');
        $this->assertEquals( 'testContentCategory', $content->oxcontents__oxtitle->value );
    }
}
