<?php
/**
 * This Software is the property of OXID eSales and is protected
 * by copyright law - it is NOT Freeware.
 *
 * Any unauthorized use of this software without a valid license key
 * is a violation of the license agreement and will be prosecuted by
 * civil and criminal law.
 *
 * @link      http://www.oxid-esales.com
 * @copyright (C) OXID eSales AG 2003-2016
 * @version   OXID eShop EE
 */
namespace OxidEsales\EshopEnterprise\Tests\Integration\Cache\ReverseProxy\Invalidation;

use oxCurl;
use OxidEsales\TestingLibrary\UnitTestCase;
use PHPUnit_Framework_MockObject_MockObject;

class ParametersToFlushTest extends UnitTestCase
{
    /** @var string */
    private $urlPath = 'url_path';

    /** @var string */
    private $urlQuery = 'url_query';

    /** @var string */
    private $shopDomain = 'www.oxid-esales.dev';

    /**
     * @return array
     */
    public function providerFlushingStrategy()
    {
        $urlPath = $this->urlPath;
        $urlQuery = $this->urlQuery;
        $shopDomain = $this->shopDomain;

        return array(
            array(1, $shopDomain, $urlPath . '.*'. $urlQuery),
            array(0, '.*', $urlQuery),
        );
    }

    /**
     * Checks if URL parameters are formed correctly dependently on flushing strategy.
     *
     * @param int    $strategy
     * @param string $expectedShopDomain
     * @param string $expectedConcatenatedUrl
     *
     * @dataProvider providerFlushingStrategy
     */
    public function testFlushingStrategy($strategy, $expectedShopDomain, $expectedConcatenatedUrl)
    {
        $urlPath = $this->urlPath;
        $shopDomain = $this->shopDomain;
        $shopUrl = 'http://' . $shopDomain . '/' . $urlPath;

        $urlQuery = $this->urlQuery;

        $headerToFlush = array(
            'x-ban-url: ' . $expectedConcatenatedUrl,
            'x-ban-host: ' . $expectedShopDomain,
        );

        $this->setConfigParam('reverseProxyFlushStrategy', $strategy);
        $this->setConfigParam('sShopURL', $shopUrl);

        /** @var oxCurl|PHPUnit_Framework_MockObject_MockObject $oxCurl */
        $oxCurl = $this->getMock('oxCurl');
        $oxCurl->expects($this->atLeastOnce())->method('setHeader')->with($this->identicalTo($headerToFlush));

        $connector = oxNew('oxReverseProxyConnector', $oxCurl);
        $urlPartsToFlush = oxNew('oxReverseProxyUrlPartsToFlush');
        $urlGenerator = oxNew('oxReverseProxyUrlGenerator');

        $backend = oxNew('oxReverseProxyBackend');
        $backend->setConnector($connector);
        $backend->setUrlPartsToFlush($urlPartsToFlush);
        $backend->setUrlGenerator($urlGenerator);
        $backend->set($urlQuery);

        $backend->execute();
    }
}
