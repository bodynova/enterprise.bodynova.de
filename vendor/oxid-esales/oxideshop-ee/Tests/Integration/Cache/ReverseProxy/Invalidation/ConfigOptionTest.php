<?php
/**
 * This Software is the property of OXID eSales and is protected
 * by copyright law - it is NOT Freeware.
 *
 * Any unauthorized use of this software without a valid license key
 * is a violation of the license agreement and will be prosecuted by
 * civil and criminal law.
 *
 * @link      http://www.oxid-esales.com
 * @copyright (C) OXID eSales AG 2003-2016
 * @version   OXID eShop EE
 */
namespace OxidEsales\EshopEnterprise\Tests\Integration\Cache\ReverseProxy\Invalidation;

use oxDb;
use OxidEsales\TestingLibrary\UnitTestCase;
use oxRegistry;

/**
 *
 */
class ConfigOptionTest extends UnitTestCase
{
    /**
     * Initialize the fixture.
     */
    protected function setUp()
    {
        if (!$this->getTestConfig()->shouldEnableVarnish()) {
            $this->markTestSkipped("Varnish must be turned on to run these tests.");
        }
        parent::setUp();
        $this->setConfigParam('blReverseProxyActive', true);
        $this->prepareSeoTable();
    }

    /**
     * Adds records needed for testing to seo table.
     */
    protected function prepareSeoTable()
    {
        $database = oxDb::getDb();
        $database->execute('TRUNCATE TABLE `oxseo`');
        $sqlTail = ", `oxlang`=0, `oxshopid`=1";
        $sql = "INSERT INTO `oxseo` SET `oxseourl` = 'seo/page1', `oxtype` = 'static', `oxstdurl` = 'index.php?cl=page1', `oxobjectid`='page1', `oxident`=1" . $sqlTail;
        $database->execute($sql);
        $sql = "INSERT INTO `oxseo` SET `oxseourl` = 'seo/page2', `oxtype` = 'static', `oxstdurl` = 'index.php?cl=page2', `oxobjectid`='page2', `oxident`=2" . $sqlTail;
        $database->execute($sql);
        $sql = "INSERT INTO `oxseo` SET `oxseourl` = 'seo/article', `oxtype` = 'oxarticle', `oxobjectid`='oxarticleid', `oxident`=3" . $sqlTail;
        $database->execute($sql);
        $sql = "INSERT INTO `oxseo` SET `oxseourl` = 'seo/content', `oxtype` = 'oxcontent', `oxobjectid`='oxcontentid', `oxident`=4" . $sqlTail;
        $database->execute($sql);
        $sql = "INSERT INTO `oxseo` SET `oxseourl` = 'seo/category', `oxtype` = 'oxcategory', `oxobjectid`='oxcategoryid', `oxident`=5". $sqlTail;
        $database->execute($sql);
        $sql = "INSERT INTO `oxseo` SET `oxseourl` = 'seo/manufacturer', `oxtype` = 'oxmanufacturer', `oxobjectid`='oxmanufacturerid', `oxident`=6" . $sqlTail;
        $database->execute($sql);
        $sql = "INSERT INTO `oxseo` SET `oxseourl` = 'seo/vendor', `oxtype` = 'oxvendor', `oxobjectid`='oxvendorid', `oxident`=7" . $sqlTail;
        $database->execute($sql);
        $sql = "INSERT INTO `oxseo` SET `oxseourl` = 'seo/article', `oxtype` = 'oxarticle', `oxobjectid`='oxarticleid2', `oxident`=8" . $sqlTail;
        $database->execute($sql);
        $sql = "INSERT INTO `oxseo` SET `oxseourl` = 'seo/category', `oxtype` = 'oxcategory', `oxobjectid`='oxcategoryid2', `oxident`=9". $sqlTail;
        $database->execute($sql);
        $sql = "INSERT INTO `oxseo` SET `oxseourl` = 'seo/register', `oxstdurl` = 'index.php?cl=register', `oxtype` = 'static', `oxobjectid`='registerid', `oxident`=10". $sqlTail;
        $database->execute($sql);
        $sql = "INSERT INTO `oxseo` SET `oxseourl` = 'seo/new', `oxstdurl` = 'index.php?cl=news', `oxtype` = 'static', `oxobjectid`='newsid', `oxident`=11". $sqlTail;
        $database->execute($sql);
        $sql = "INSERT INTO `oxseo` SET `oxseourl` = 'seo/home', `oxstdurl` = 'index.php?cl=start', `oxtype` = 'static', `oxobjectid`='startid', `oxident`=12". $sqlTail;
        $database->execute($sql);
        $sql = "INSERT INTO `oxseo` SET `oxseourl` = 'seo/links', `oxstdurl` = 'index.php?cl=links', `oxtype` = 'static', `oxobjectid`='linksid', `oxident`=13". $sqlTail;
        $database->execute($sql);
    }

    /**
     * Forms and returns array of expected cache invalidation.
     *
     * @return array
     */
    protected function getExpectationArray()
    {
        $expectedResult = array (
            'startPage' => array(
                '/',
                '/seo/home.*',
            ),
            'details' => array(
                '/seo/article.*'
            ),
            'startpage_datails' => array(
                '/',
                '/seo/home.*',
                '/seo/article.*'
            ),
            'startpage_datails_lists' => array(
                '/',
                '/seo/home.*',
                '/seo/article.*',
                '/seo/category.*',
                '/seo/manufacturer.*',
                '/seo/vendor.*'
            ),
            'lists' => array(
                '/seo/category.*',
                '/seo/manufacturer.*',
                '/seo/vendor.*'
            ),
            'all' => array (),
            'bl_perfShowActionCatArticleCnt' => array(
                '/seo/category.*',
                '/seo/manufacturer.*',
                '/seo/vendor.*',
                '/widget.php?.*cl=oxwcategorytree.*'
            ),
            'blSearchUseAND' => array(
                '/index.php?.*cl=search.*'
            ),
            'blEnableDownloads' => array(
                '/widget.php?.*cl=oxwservicelist.*',
                '/widget.php?.*cl=oxwservicemenu.*'
            ),
            'aMustFillFields' => array(
                '/seo/register.*'
            ),
            'aCurrencies' => array(
                '/widget.php?.*cl=oxwcurrencylist.*'
            ),
            'bl_rssRecommLists' => array(
                '/index.php?.*cl=recommlist.*'
            ),
            'bl_rssRecommListArts' => array(
                '/index.php?.*cl=recommlist.*'
            ),
            'blShowRememberMe' => array(
                '/widget.php?.*cl=oxwservicemenu.*'
            ),
            'blDontShowEmptyCategories' => array(
                '/widget.php?.*cl=oxwcategorytree.*'
            ),
            'bl_perfLoadManufacturerTree' => array(
                '/seo/manufacturer.*',
                '/widget.php?.*cl=oxwmanufacturerlist.*'
            ),
            'bl_perfLoadCurrency' => array(
                '/widget.php?.*cl=oxwcurrencylist.*'
            ),
            'bl_perfLoadLanguages' => array(
                '/widget.php?.*cl=oxwlanguagelist.*'
            ),
            'sRDFaBusinessEntityLoc' => array(
                '/seo/content.*'
            ),
            'sRDFaPaymentChargeSpecLoc' => array(
                '/seo/content.*'
            ),
            'sRDFaDeliveryChargeSpecLoc' => array(
                '/seo/content.*'
            ),
            'blInvitationsEnabled' => array(
                '/widget.php?.*cl=oxwservicelist.*'
            ),
        );

        return $expectedResult;
    }

    /**
     * @return array
     */
    public function providerConfigOptions()
    {
        return array(
            array( 'blOverrideZeroABCPrices', 'startpage_datails_lists'),
            array('blNewArtByInsert', 'startpage_datails_lists'),
            array('dDefaultVAT', 'startpage_datails_lists'),
            array('blEnterNetPrice', 'startpage_datails_lists'),
            array('blShowNetPrice', 'startpage_datails_lists'),
            array('sCntOfNewsLoaded', 'startPage'),
            array('bl_rssTopShop', 'startPage'),
            array('bl_rssBargain', 'startPage'),
            array('bl_rssNewest', 'startPage'),
            array('iTop5Mode', 'startPage'),
            array('iNewestArticlesMode', 'startPage'),
            array('bl_perfLoadAktion', 'startPage'),
            array('bl_perfLoadPriceForAddList', 'startPage'),
            array('bl_perfLoadNews', 'startPage'),
            array('bl_perfLoadNewsOnlyStart', 'startPage'),
            array('blShowSorting', 'lists'),
            array('aSortCols', 'lists'),
            array('bl_rssCategories', 'lists'),
            array('blLoadVariants', 'lists'),
            array('bl_perfLoadSelectListsInAList', 'lists'),
            array('blUseStock', 'details'),
            array('sStockWarningLimit', 'details'),
            array('blStockOnDefaultMessage', 'details'),
            array('blStockOffDefaultMessage', 'details'),
            array('iNrofSimilarArticles', 'details'),
            array('iNrofCustomerWhoArticles', 'details'),
            array('iNrofNewcomerArticles', 'details'),
            array('iNrofCrossellArticles', 'details'),
            array('blBidirectCross', 'details'),
            array('iRatingLogsTimeout', 'details'),
            array('blVariantParentBuyable', 'details'),
            array('blVariantInheritAmountPrice', 'details'),
            array('blShowVariantReviews', 'details'),
            array('blUseMultidimensionVariants', 'details'),
            array('iAttributesPercent', 'details'),
            array('bl_perfLoadReviews', 'details'),
            array('bl_perfLoadCrossselling', 'details'),
            array('bl_perfLoadAccessoires', 'details'),
            array('bl_perfLoadCustomerWhoBoughtThis', 'details'),
            array('bl_perfLoadSimilar', 'details'),
            array( 'bl_perfLoadSelectLists', 'details'),
            array('bl_perfUseSelectlistPrice', 'details'),
            array('bl_perfParseLongDescinSmarty', 'details'),
            array('blRDFaEmbedding', 'details'),
            array('blShowCookiesNotification', 'all'),
            array('iSmartyPhpHandling', 'all'),
            array('blUseTimeCheck', 'all'),
            array('bl_perfLoadPrice', 'all'),
            array('bl_perfShowActionCatArticleCnt', 'bl_perfShowActionCatArticleCnt'),
            array('blSearchUseAND', 'blSearchUseAND'),
            array('blEnableDownloads', 'blEnableDownloads'),
            array('aMustFillFields', 'aMustFillFields'),
            array('aCurrencies', 'aCurrencies'),
            array('bl_rssRecommLists', 'bl_rssRecommLists'),
            array('bl_rssRecommListArts', 'bl_rssRecommListArts'),
            array('blShowRememberMe', 'blShowRememberMe'),
            array('blDontShowEmptyCategories', 'blDontShowEmptyCategories'),
            array('bl_perfLoadManufacturerTree', 'bl_perfLoadManufacturerTree'),
            array('bl_perfLoadCurrency', 'bl_perfLoadCurrency'),
            array('bl_perfLoadLanguages', 'bl_perfLoadLanguages'),
            array('sRDFaBusinessEntityLoc', 'sRDFaBusinessEntityLoc'),
            array('sRDFaPaymentChargeSpecLoc', 'sRDFaPaymentChargeSpecLoc'),
            array('sRDFaDeliveryChargeSpecLoc', 'sRDFaDeliveryChargeSpecLoc'),
            array('blInvitationsEnabled', 'blInvitationsEnabled'),
        );
    }

    /**
     * Test case for config option which invalidates start page
     *
     * @dataProvider providerConfigOptions
     *
     * @param string $configOptionName
     * @param string $expectedArrayIndex
     */
    public function testInvalidation($configOptionName, $expectedArrayIndex)
    {
        $reverseProxyBackend = oxRegistry::get('oxReverseProxyBackend');
        // Pretend that there is reverse proxy header.
        $reverseProxyBackend->setReverseProxyCapableDoEsi(true);

        $expectedResult = $this->getExpectationArray();

        $config = $this->getConfig();
        $config->executeDependencyEvent($configOptionName);

        $urls = $reverseProxyBackend->getUrlPool();

        sort($urls);
        sort($expectedResult[$expectedArrayIndex]);

        if ($expectedArrayIndex == 'all') {
            $this->assertTrue($reverseProxyBackend->isFlushSet());
        } else {
            $this->assertFalse($reverseProxyBackend->isFlushSet());
        }

        $this->assertEquals($expectedResult[$expectedArrayIndex], $urls);
    }
}
