<?php
/**
 * This Software is the property of OXID eSales and is protected
 * by copyright law - it is NOT Freeware.
 *
 * Any unauthorized use of this software without a valid license key
 * is a violation of the license agreement and will be prosecuted by
 * civil and criminal law.
 *
 * @link      http://www.oxid-esales.com
 * @copyright (C) OXID eSales AG 2003-2016
 * @version   OXID eShop EE
 */
namespace OxidEsales\EshopEnterprise\Tests\Integration\Cache\ReverseProxy\Invalidation;

use OxidEsales\TestingLibrary\UnitTestCase;
use oxRegistry;

/**
 * Reverse proxy caching invalidation on modules activation / deactivation :
 *
 *   - When HIDDEN config option blOnModuleEventDoNotFlushCache
 *         - is undefined, should flush all
 *         - is false, should flush all
 *         - is true, should NOT flush
 *   - on activating or deactivating module
 *
 */
class ModuleTest extends UnitTestCase
{
    /**
     * Initialize the fixture
     */
    protected function setUp()
    {
        if (!$this->getTestConfig()->shouldEnableVarnish()) {
            $this->markTestSkipped("Varnish must be turned on to run these tests.");
        }
        parent::setUp();
        $this->setConfigParam('blReverseProxyActive', true);
    }

    /**
     * Module data provider
     *
     * @return array
     */
    public function providerModuleEvents()
    {
        return array(
            array("oxModule",       "activate",      true,  null),
            array("oxModule",       "activate",      true,  false),
            array("oxModule",       "activate",      false, true),
            array("oxModule",       "deactivate",    true,  null),
            array("oxModule",       "deactivate",    true,  false),
            array("oxModule",       "deactivate",    false, true),
            array("module_config",   "saveConfVars", true,  null),
            array("module_config",   "saveConfVars", true,  false),
            array("module_config",   "saveConfVars", false, true),
            array("module_sortlist", "save",         true,  null),
            array("module_sortlist", "save",         true,  false),
            array("module_sortlist", "save",         false, true),
        );
    }

    /**
     * Test flushing on module activate/deactivate (having in mind hidden config option blOnModuleEventDoNotFlushCache)
     *
     * @dataProvider providerModuleEvents
     *
     * @param string $className
     * @param string $action
     * @param bool   $expectedResult
     * @param bool   $onModuleEventDoNotFlushCache
     */
    public function testModuleEvents($className, $action, $expectedResult, $onModuleEventDoNotFlushCache)
    {
        $reverseProxyBackend = oxRegistry::get('oxReverseProxyBackend');
        // Pretend that there is reverse proxy header.
        $reverseProxyBackend->setReverseProxyCapableDoEsi(true);

        $moduleInformation = array(
                'id' => 'myorder',
                'title' => 'My Order',
                'extend' => array('oxorder' => 'myorder'),
        );

        if (!is_null($onModuleEventDoNotFlushCache)) {
            $this->getConfig()->setConfigParam("blOnModuleEventDoNotFlushCache", $onModuleEventDoNotFlushCache);
        }

        $module = oxNew('oxModule');
        $module->setModuleData($moduleInformation);
        
        if ($className != "oxModule") {
            $object = oxNew($className);
            $object->$action();
        } else {
            $moduleCache = oxNew('oxModuleCache', $module);
            $moduleInstaller = oxNew('oxModuleInstaller', $moduleCache);

            $moduleInstaller->activate($module);
        }

        $this->assertEquals($expectedResult, $reverseProxyBackend->isFlushSet());
    }
}
