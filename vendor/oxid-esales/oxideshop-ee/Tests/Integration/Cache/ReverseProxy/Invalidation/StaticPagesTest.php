<?php
/**
 * This Software is the property of OXID eSales and is protected
 * by copyright law - it is NOT Freeware.
 *
 * Any unauthorized use of this software without a valid license key
 * is a violation of the license agreement and will be prosecuted by
 * civil and criminal law.
 *
 * @link      http://www.oxid-esales.com
 * @copyright (C) OXID eSales AG 2003-2016
 * @version   OXID eShop EE
 */
namespace OxidEsales\EshopEnterprise\Tests\Integration\Cache\ReverseProxy\Invalidation;

use oxDb;
use OxidEsales\TestingLibrary\UnitTestCase;
use oxRegistry;

/**
 * Testing invalidation url generation on main objects SAVE and DELETE events
 */
class StaticPagesTest extends UnitTestCase
{
    /**
     * Initialize the fixture.
     */
    protected function setUp()
    {
        if (!$this->getTestConfig()->shouldEnableVarnish()) {
            $this->markTestSkipped("Varnish must be turned on to run these tests.");
        }
        parent::setUp();
        $this->setConfigParam('blReverseProxyActive', true);
        $this->prepareSeoTable();
    }

    /**
     * Cleans up database tables.
     */
    public function tearDown()
    {
        $this->setConfigParam("blReverseProxyActive", false);
        $this->cleanUpTable("oxseo", "oxident");
        parent::tearDown();
    }

    /**
     * Test case on deleteStaticUrl event
     *
     */
    public function testOnDeleteStaticUrl()
    {
        $expectedResult = array('/seo/page1.*');
        $reverseProxyBackend = oxRegistry::get('oxReverseProxyBackend');
        // Pretend that there is reverse proxy header.
        $reverseProxyBackend->setReverseProxyCapableDoEsi(true);

        $this->setRequestParameter('aStaticUrl', array( "oxseo__oxobjectid" => "page1" ));
        $this->setRequestParameter('oxid', "1");

        $view = oxNew('Shop_Seo');
        $view->deleteStaticUrl();

        $urls = $reverseProxyBackend->getUrlPool();
        sort($urls);
        sort($expectedResult);

        $this->assertEquals($expectedResult, $urls);
        $this->assertFalse($reverseProxyBackend->isFlushSet());
    }

    /**
     * Adds records needed for testing to seo table.
     */
    protected function prepareSeoTable()
    {
        $database = oxDb::getDb();
        $queryTail = ", `oxlang`=0, `oxshopid`=1";
        $query = "INSERT INTO `oxseo` SET `oxseourl` = 'seo/page1', `oxtype` = 'static', `oxstdurl` = 'index.php?cl=page1', `oxobjectid`='page1', `oxident`='_test1'" . $queryTail;
        $database->execute($query);
    }
}
