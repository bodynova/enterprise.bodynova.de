<?php
/**
 * This Software is the property of OXID eSales and is protected
 * by copyright law - it is NOT Freeware.
 *
 * Any unauthorized use of this software without a valid license key
 * is a violation of the license agreement and will be prosecuted by
 * civil and criminal law.
 *
 * @link      http://www.oxid-esales.com
 * @copyright (C) OXID eSales AG 2003-2016
 * @version   OXID eShop EE
 */
namespace OxidEsales\EshopEnterprise\Tests\Integration\Cache\ReverseProxy\Invalidation;

use oxDb;
use oxField;
use OxidEsales\TestingLibrary\UnitTestCase;
use oxRegistry;

/**
 * Testing invalidation url generation on main objects SAVE and DELETE events
 */
class RecommListTest extends UnitTestCase
{
    /**
     * Initialize the fixture.
     */
    protected function setUp()
    {
        if (!$this->getTestConfig()->shouldEnableVarnish()) {
            $this->markTestSkipped("Varnish must be turned on to run these tests.");
        }
        parent::setUp();
        $this->setConfigParam('blReverseProxyActive', true);
    }

    /**
     * Tear down the fixture.
     */
    protected function tearDown()
    {
        $this->cleanUpTable("oxrecommlists");
        $this->cleanUpTable("oxseo", "oxident");
        parent::tearDown();
    }

    /**
     * Test case on SAVE event
     */
    public function testOnSave()
    {
        $reverseProxyBackend = oxRegistry::get('oxReverseProxyBackend');
        // Pretend that there is reverse proxy header.
        $reverseProxyBackend->setReverseProxyCapableDoEsi(true);

        $query = "INSERT INTO `oxseo` SET `oxseourl` = 'seo/recommlist', `oxstdurl` = 'index.php?cl=recommlist', `oxtype` = 'dynamic', `oxobjectid`='_testId', `oxident`='_test1', `oxlang`=0, `oxshopid`=1";
        oxDb::getDb()->execute($query);

        $expectedResult = array( '/widget.php?.*cl=oxwrecommendation.*',
                                  '/seo/recommlist.*');
        $object = oxNew('oxRecommList');
        $object->setId('_testId');
        $object->oxrecommlists__oxtitle = new oxField('testRecomm');
        $object->save();

        $urls = $reverseProxyBackend->getUrlPool();
        sort($urls);
        sort($expectedResult);

        $this->assertEquals($expectedResult, $urls);
        $this->assertFalse($reverseProxyBackend->isFlushSet());
    }

    /**
     * Test case on DELETE event
     */
    public function testOnDelete()
    {
        $reverseProxyBackend = oxRegistry::get('oxReverseProxyBackend');
        // Pretend that there is reverse proxy header.
        $reverseProxyBackend->setReverseProxyCapableDoEsi(true);

        $database = oxDb::getDb();
        $database->execute("insert into oxrecommlists (oxid, oxtitle) values ('_testId', 'testTitle')");

        $query = "INSERT INTO `oxseo` SET `oxseourl` = 'seo/recommlist', `oxstdurl` = 'index.php?cl=recommlist', `oxtype` = 'dynamic', `oxobjectid`='_testId', `oxident`='_test1', `oxlang`=0, `oxshopid`=1";
        $database->execute($query);

        $expectedResult = array( '/widget.php?.*cl=oxwrecommendation.*',
                                  '/seo/recommlist.*');
        $object = oxNew('oxRecommList');
        $object->delete('_testId');

        $urls = $reverseProxyBackend->getUrlPool();
        sort($urls);
        sort($expectedResult);

        $this->assertEquals($expectedResult, $urls);
        $this->assertFalse($reverseProxyBackend->isFlushSet());
    }
}
