<?php
/**
 * This Software is the property of OXID eSales and is protected
 * by copyright law - it is NOT Freeware.
 *
 * Any unauthorized use of this software without a valid license key
 * is a violation of the license agreement and will be prosecuted by
 * civil and criminal law.
 *
 * @link      http://www.oxid-esales.com
 * @copyright (C) OXID eSales AG 2003-2016
 * @version   OXID eShop EE
 */
namespace OxidEsales\EshopEnterprise\Tests\Integration\Cache\ReverseProxy\Cache;

/**
 * Helper class for testing reverse proxy functionality.
 */
class ReverseProxyPage
{
    /** @var string Path and name to cookies file. */
    private $tempDirectory = '/tmp/reverseProxy/';

    /** @var string Page url to call. */
    private $pageUrl = null;

    /** @var array Parameters to pass to server via POST. */
    private $parameters = null;

    /** @var string File name to store cookies information in. File will be created in temp folder. */
    private $cookieName = null;

    /** @var string Page content. Will be set after every execute() run. */
    private $content = null;

    /** @var string CURL error message. */
    private $errorMessage = null;

    /** @var string Times to retry. */
    private $timesToRetry = 3;

    /** @var int Times already retried. */
    private $retryCount = 0;

    /** @var array List of server errors, on which execution will be retired. */
    private $errorsToRetryOn = array(
        '500 Internal Server Error',
        '501 Not Implemented',
        '502 Bad Gateway',
        '503 Service Unavailable',
        '504 Gateway Timeout'
    );

    /**
     * Sets page url and parameters, which will be used for executing the call.
     *
     * @param string $url        Page url
     * @param array  $parameters Call parameters (if set, parameters will be past by POST)
     */
    public function __construct($url, $parameters = null)
    {
        $this->pageUrl = $url;
        $this->parameters = $parameters;
    }

    /**
     * Returns current page url.
     *
     * @return string
     */
    public function getUrl()
    {
        return $this->pageUrl;
    }

    /**
     * Returns current page call parameters.
     *
     * @return array
     */
    public function getParameters()
    {
        return $this->parameters;
    }

    /**
     * Sets cookie name, which is used for storing cookie information.
     *
     * @param string $cookieName
     */
    public function setCookieName($cookieName)
    {
        $this->cookieName = $cookieName;
    }

    /**
     * Returns cookie name, which is used for storing cookie information.
     *
     * @return string
     */
    public function getCookieName()
    {
        return $this->cookieName;
    }

    /**
     * Sets temporary directory path for storing cookies and page information.
     *
     * @param string $tmpDir
     */
    public function setTempDirectory($tmpDir)
    {
        $this->tempDirectory = $tmpDir;
    }

    /**
     * Returns temporary directory path, which is used for storing cookies and page information.
     *
     * @return string
     */
    public function getTempDirectory()
    {
        if (!file_exists($this->tempDirectory)) {
            mkdir($this->tempDirectory, 0777, true);
        }
        return $this->tempDirectory;
    }

    /**
     * Returns CURL error message.
     *
     * @return string
     */
    public function getErrorMessage()
    {
        return $this->errorMessage;
    }

    /**
     * Returns page content.
     *
     * @return string|false Returns false if page call was unsuccessful.
     */
    public function getContent()
    {
        if (is_null($this->content)) {
            $this->execute();
        }
        return $this->content;
    }

    /**
     * Deletes cookie file if it exists.
     */
    public function deleteCookies()
    {
        if (file_exists($this->_getCookieFilePath())) {
            unlink($this->_getCookieFilePath());
        }
    }

    /**
     * Get page content with curl.
     * Cookies file need for two connection as environment key changes regenerate cache.
     *
     * @return bool Whether call was successful.
     */
    public function execute()
    {
        $ch = curl_init();

        curl_setopt($ch, CURLOPT_URL, $this->getUrl());
        // Add bigger timeout (server has 30).
        curl_setopt($ch, CURLOPT_TIMEOUT, 31);
        // Return result as string.
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        $requestParameters = $this->getParameters();
        if ($requestParameters) {
            curl_setopt($ch, CURLOPT_POST, 1);

            if ($requestParameters && count($requestParameters) > 0) {
                $formedParameters = '';
                foreach ($requestParameters as $paramKey => $paramValue) {
                    $formedParameters .= $paramKey ."=". $paramValue ."&";
                }
                $formedParameters = substr($formedParameters, 0, -1);

                curl_setopt($ch, CURLOPT_POSTFIELDS, $formedParameters);
            } else {
                curl_setopt($ch, CURLOPT_POSTFIELDS, "params=0");
            }
        } else {
            curl_setopt($ch, CURLOPT_HTTPGET, 1);
        }
        // Will include headers in to output.
        curl_setopt($ch, CURLOPT_HEADER, 1);
        // For reverse proxy log to see who called.
        curl_setopt($ch, CURLOPT_USERAGENT, "OXID-TEST");
        // Where cookies should be stored.
        curl_setopt($ch, CURLOPT_COOKIEJAR, $this->_getCookieFilePath());
        // From where cookies will be send to server.
        curl_setopt($ch, CURLOPT_COOKIEFILE, $this->_getCookieFilePath());
        // Force close connection, no cache.
        curl_setopt($ch, CURLOPT_FORBID_REUSE, true);
        // Make cURL follow a redirect. Some pages do redirect from none SEO to SEO pages.
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);

        $this->content = curl_exec($ch);

        $this->_checkContent();

        $result = $this->content !== false;

        if (!$result) {
            $this->errorMessage = curl_error($ch);
        }
        curl_close($ch);

        return $result;
    }

    /**
     * Returns render's date value by tag name.
     *
     * @param string $tag tag name to get valye by.
     *
     * @return int
     */
    public function getRenderDateFromString($tag)
    {
        $stringToFind = '/<div id=\''. $tag .'_timestamp' .'\'>Timestamp: (\d+\.?\d*)/';
        preg_match($stringToFind, $this->getContent(), $result);

        return $result[1];
    }

    /**
     * Returns whether text exists in page.
     *
     * @param string $text Text to search for.
     *
     * @return bool
     */
    public function isTextPresent($text)
    {
        $text = preg_quote($text);
        $value = preg_match('/'.$text.'/', $this->getContent());

        return (bool) $value;
    }

    /**
     * Saves page content of the page to file.
     *
     * @param string $fileName   File name where to save file content.
     * @param string $folderName Folder in temp dir where to put created file (default is temp folder).
     */
    public function saveToFile($fileName, $folderName = null)
    {
        $path = $this->getTempDirectory();
        if (!empty($folderName)) {
            $path = $path . $folderName;
        }
        $path = rtrim($path, '/').'/';

        if (!file_exists($path)) {
            mkdir($path, 0777, true);
        }
        $fullFilePath = $path . $fileName;
        $fc = fopen($fullFilePath, 'w');
        fwrite($fc, $this->getContent());
        fclose($fc);
    }

    /**
     * Forms and returns cookie file path.
     *
     * @return string
     */
    private function _getCookieFilePath()
    {
        return $this->getTempDirectory().'/'.$this->getCookieName();
    }

    /**
     * Checks content for errors and retries execution on error.
     */
    private function _checkContent()
    {
        if ($this->_isInternalError() && $this->retryCount < $this->timesToRetry) {
            $this->retryCount++;
            $this->execute();
        } else {
            $this->retryCount = 0;
        }
    }

    /**
     * Checks whether there is internal server error in page content.
     *
     * @return bool
     */
    private function _isInternalError()
    {
        $content = $this->getContent();
        foreach ($this->errorsToRetryOn as $error) {
            if (strpos($content, $error) !== false) {
                return true;
            }
        }

        return false;
    }
}
