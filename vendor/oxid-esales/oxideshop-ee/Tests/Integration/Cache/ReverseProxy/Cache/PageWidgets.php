<?php
/**
 * This Software is the property of OXID eSales and is protected
 * by copyright law - it is NOT Freeware.
 *
 * Any unauthorized use of this software without a valid license key
 * is a violation of the license agreement and will be prosecuted by
 * civil and criminal law.
 *
 * @link      http://www.oxid-esales.com
 * @copyright (C) OXID eSales AG 2003-2016
 * @version   OXID eShop EE
 */
namespace OxidEsales\EshopEnterprise\Tests\Integration\Cache\ReverseProxy\Cache;

/**
 * List of widgets, which should be loaded in shop.
 */
class PageWidgets
{
    /** @var array All start page widgets. */
    public $startWidgets = array(
        "oxwcookienote",
        "oxwlanguagelist",
        "oxwcurrencylist",
        "oxwservicemenu",
        "oxwcategorytree",
        "oxwminibasket",
        "oxwcategorytree",
        "oxwservicelist",
        "oxwinformation",
        "oxwmanufacturerlist" ,
        "oxwcategorytree",
        "oxwarticlebox");

    /** @var array All list page widgets. */
    public $listWidgets = array(
        "oxwcookienote",
        "oxwlanguagelist",
        "oxwcurrencylist",
        "oxwservicemenu",
        "oxwcategorytree",
        "oxwminibasket",
        "oxwcategorytree",
        "oxwservicelist",
        "oxwinformation",
        "oxwmanufacturerlist",
        "oxwcategorytree",
        "oxwarticlebox");

    /** @var array All details page widgets. */
    public $detailsWidgets = array(
        "oxwcookienote",
        "oxwlanguagelist",
        "oxwcurrencylist",
        "oxwservicemenu",
        "oxwcategorytree",
        "oxwminibasket",
        "oxwcategorytree",
        "oxwservicelist",
        "oxwinformation",
        "oxwmanufacturerlist",
        "oxwcategorytree",
        "oxwarticledetails",
        "oxwarticlebox",
        "oxwreview",
        "oxwrating");

    /** @var array All compare page widgets. */
    public $compareWidgets = array(
        "oxwcookienote",
        "oxwlanguagelist",
        "oxwcurrencylist",
        "oxwservicemenu",
        "oxwcategorytree",
        "oxwminibasket",
        "oxwcategorytree",
        "oxwservicelist",
        "oxwinformation",
        "oxwmanufacturerlist",
        "oxwcategorytree");

    /** @var array All account page widgets. */
    public $accountWidgets = array(
        "oxwcookienote",
        "oxwlanguagelist",
        "oxwcurrencylist",
        "oxwservicemenu",
        "oxwcategorytree",
        "oxwminibasket",
        "oxwcategorytree",
        "oxwservicelist",
        "oxwinformation",
        "oxwmanufacturerlist",
        "oxwcategorytree");

    /** @var array All not existing page widgets. */
    public $notFoundWidgets = array(
        "oxwcookienote",
        "oxwlanguagelist",
        "oxwcurrencylist",
        "oxwservicemenu",
        "oxwcategorytree",
        "oxwminibasket",
        "oxwcategorytree",
        "oxwservicelist",
        "oxwinformation",
        "oxwmanufacturerlist",
        "oxwcategorytree");
}
