<?php
/**
 * This Software is the property of OXID eSales and is protected
 * by copyright law - it is NOT Freeware.
 *
 * Any unauthorized use of this software without a valid license key
 * is a violation of the license agreement and will be prosecuted by
 * civil and criminal law.
 *
 * @link      http://www.oxid-esales.com
 * @copyright (C) OXID eSales AG 2003-2016
 * @version   OXID eShop EE
 */
namespace OxidEsales\EshopEnterprise\Tests\Integration\Cache\ReverseProxy\Cache;

/**
 * Tests if reverse proxy is working
 */
abstract class ReverseProxyTestCase extends \OxidEsales\TestingLibrary\UnitTestCase
{
    /** @var string Test name. */
    protected $testName = '';

    /**
     * Defines file name where cookies are stored.
     * All tests share same cookies as they imitate user browsing.
     * We use static as same file name must be used for all tests.
     *
     * @var string
     */
    private static $startTime = null;

    /**
     * Initialize the fixture.
     */
    protected function setUp()
    {
        if (!$this->getTestConfig()->shouldEnableVarnish()) {
            $this->markTestSkipped("Varnish must be turned on to run these tests.");
        }
        parent::setUp();
        $this->getConfig()->saveShopConfVar('bool', 'blReverseProxyActive', true);
        $this->getConfig()->saveShopConfVar('str', 'iLayoutCacheLifeTime', 3000);
    }

    /**
     * Tear down the fixture.
     */
    protected function tearDown()
    {
        $this->getConfig()->saveShopConfVar('bool', 'blReverseProxyActive', false);
        parent::tearDown();
    }

    /**
     * Create result file and mark test as failed.
     *
     * @param string           $testName Failed test name, used to form file name
     * @param ReverseProxyPage $page1    Result (page content)
     * @param ReverseProxyPage $page2    Result (page content)
     * @param string           $message  Failure message
     */
    public function __markTestFailed($testName, $page1, $page2, $message)
    {
        $page1->saveToFile($this->formPageFileName($page1->getUrl(), $testName, '1'), $this->getTestName());
        $page2->saveToFile($this->formPageFileName($page2->getUrl(), $testName, '2'), $this->getTestName());

        $this->fail($message);
    }

    /**
     * Define unique name for this test run.
     * Can be used to define unique directory structure or cookie file name.
     *
     * @return string
     */
    protected function getStartTime()
    {
        if (is_null(self::$startTime)) {
            self::$startTime = time();
        }
        return self::$startTime;
    }

    /**
     * Returns test run name
     *
     * @return string
     */
    protected function getTestName()
    {
        return $this->testName;
    }

    /**
     * Returns shop url
     *
     * @return string
     */
    protected function getShopUrl()
    {
        return $this->getConfigParam('sShopURL');
    }

    /**
     * Check if element is cached. If not output error message.
     *
     * @param array            $cachedElements
     * @param ReverseProxyPage $page1
     * @param ReverseProxyPage $page2
     */
    protected function checkIfCached($cachedElements, $page1, $page2)
    {
        foreach ($cachedElements as $cachedElement) {
            $dateFirstHit = $page1->getRenderDateFromString($cachedElement);
            $dateSecondHit = $page2->getRenderDateFromString($cachedElement);

            // Check if element is in both pages. If not something went wrong. Put result to temp file and mark as incomplete.
            if (!$dateFirstHit || !$dateSecondHit) {
                $this->__markTestFailed($this->getTestName() . '_cachedNotFound', $page1, $page2, 'Element ' . $cachedElement . ' missing!');
            }

            // Check if time is correct.
            if ($dateFirstHit !== $dateSecondHit) {
                $message = "Checking element: " . $cachedElement . " in page " . $page1->getUrl() . ". Date on first hit
                    (" . date('Y-m-d H:i:s', $dateFirstHit) . ")[" . $dateFirstHit . "] must be same as in second hit
                    (" . date('Y-m-d H:i:s', $dateSecondHit) . ")[" . $dateSecondHit . "],
                    as date in second hit comes from rendered page which should be previously cached.";

                $testName = $this->getTestName() . '_cachedExpected';
                $this->__markTestFailed($testName, $page1, $page2, $message);
            }
        }
    }

    /**
     * Check if element is not cached. If cached output error message.
     *
     * @param array            $notCachedElements
     * @param ReverseProxyPage $page1
     * @param ReverseProxyPage $page2
     */
    protected function checkIfNotCached($notCachedElements, $page1, $page2)
    {
        foreach ($notCachedElements as $notCachedElement) {
            $dateFirstHit = $page1->getRenderDateFromString($notCachedElement);
            $dateSecondHit = $page2->getRenderDateFromString($notCachedElement);

            // Check if element is in both pages. If not something went wrong. Put result to temp file and mark as incomplete.
            if (!$dateFirstHit || !$dateSecondHit) {
                $this->__markTestFailed($this->getTestName() . '_cachedFound', $page1, $page2, 'Element ' . $notCachedElement . ' missing!');
            }

            // Check if time is correct.
            if ($dateFirstHit >= $dateSecondHit) {
                $message = "Checking element: " . $notCachedElement . " in page " . $page1->getUrl() . ". Date on first hit
                    (" . date('Y-m-d H:i:s', $dateFirstHit) . ")[" . $dateFirstHit . "] must be lower then date on second hit
                    (" . date('Y-m-d H:i:s', $dateSecondHit) . ")[" . $dateSecondHit . "].";

                $testName = $this->getTestName() . '_cachedNotExpected';
                $this->__markTestFailed($testName, $page1, $page2, $message);
            }
        }
    }

    /**
     * @param string $url
     * @param array  $params
     *
     * @return ReverseProxyPage
     */
    protected function createReverseProxyPage($url, $params = null)
    {
        $page = new ReverseProxyPage($url, $params);
        $page->setTempDirectory('/tmp/reverseProxy/'.date("Y-m-d_h-i-s", $this->getStartTime()).'/');
        $page->setCookieName('rpcookies_'.$this->getTestName());

        return $page;
    }

    /**
     * @param string $pageUrl
     * @param string $prefix
     * @param string $postfix
     *
     * @return string
     */
    protected function formPageFileName($pageUrl, $prefix = '', $postfix = '')
    {
        return $prefix.'-'.preg_replace('/[^\w\-~_\.]+/u', '-', $pageUrl).'-'.$postfix.'.html';
    }
}
