<?php
/**
 * This Software is the property of OXID eSales and is protected
 * by copyright law - it is NOT Freeware.
 *
 * Any unauthorized use of this software without a valid license key
 * is a violation of the license agreement and will be prosecuted by
 * civil and criminal law.
 *
 * @link      http://www.oxid-esales.com
 * @copyright (C) OXID eSales AG 2003-2016
 * @version   OXID eShop EE
 */
namespace OxidEsales\EshopEnterprise\Tests\Integration\Cache\Backend;

use oxDb;
use oxField;
use OxidEsales\EshopEnterprise\Application\Model\Article;
use OxidEsales\TestingLibrary\UnitTestCase;

/**
 * Article caching in cache backend cases:
 *
 * - key generation;
 * - select: first time from db, second from cache;
 * - update:
 *   - spec case on stock update;
 *   - parent updates on variant changes;
 *   - reviews and ratings;
 * - delete: cache by identifier should be empty;
 * - invalidation keys generation;
 *
 */
class ArticleTest extends UnitTestCase
{
    /**
     * Initialize the fixture.
     */
    protected function setUp()
    {
        parent::setUp();
        $this->setConfigParam('blCacheActive', true);
        $this->setConfigParam('sDefaultCacheConnector', 'oxFileCacheConnector');
    }

    /**
     * Tear down the fixture.
     */
    protected function tearDown()
    {
        $this->cleanUpTable('oxarticles');
        $cache = $this->getCacheBackend();
        $cache->flush();

        parent::tearDown();
    }

    /**
     * Cache key generation
     */
    public function testGetCacheKey()
    {
        $article = $this->createArticle();
        $this->assertEquals('oxArticle__testArticle_1_de', $article->getCacheKey('_testArticle'));
        $this->assertEquals('oxArticle__testArticle_1_de', $article->getCacheKey());
    }

    /**
     * Select: if info in a cache getting it from cache
     *
     */
    public function testSelect()
    {
        $cache = $this->getCacheBackend();
        $article = $this->createArticle();

        // cache empty
        $this->assertNull($cache->get('oxArticle__testArticle_1_de'));

        // load
        $article->load('_testArticle');
        // cache filed
        $this->assertNotNull($cache->get('oxArticle__testArticle_1_de'));

        // change in directly to db
        oxDb::getDb()->execute("UPDATE `oxarticles` SET `oxtitle` = 'testArticleUpdated' WHERE `oxid` = '_testArticle'");

        //testing if it loaded from cache
        $article = oxNew('oxArticle');
        $article->load('_testArticle');
        $this->assertEquals('testArticle', $article->oxarticles__oxtitle->value);
    }

     /**
     * delete: after delete cache with this key empty
     *
     */
    public function testDelete()
    {
        $cache = $this->getCacheBackend();
        $article = $this->createArticle();

        // cache empty
        $this->assertNull($cache->get('oxArticle__testArticle_1_de'));

        // load
        $article->load('_testArticle');

        // cache filed
        $this->assertNotNull($cache->get('oxArticle__testArticle_1_de'));

        // delete
        $article->delete();

        // cache empty
        $this->assertNull($cache->get('oxArticle__testArticle_1_de'));

        //testing if it is deleted
        $article = oxNew('oxArticle');
        $this->assertFalse($article->load('_testArticle'));
    }

    /**
     * Update: after we expect that cache is invalidated
     *
     */
    public function testUpdate()
    {
        $cache = $this->getCacheBackend();
        $article = $this->createArticle();

        // cache empty
        $this->assertNull($cache->get('oxArticle__testArticle_1_de'));

        // load
        $article->load('_testArticle');

        // cache filed
        $this->assertNotNull($cache->get('oxArticle__testArticle_1_de'));

        // update
        $article->oxarticles__oxtitle = new oxField('testArticleUpdated');
        $article->save();

        // cache empty
        $this->assertNull($cache->get('oxArticle__testArticle_1_de'));

        //testing update
        $article = oxNew('oxArticle');
        $article->load('_testArticle');
        $this->assertEquals('testArticleUpdated', $article->oxarticles__oxtitle->value);
    }

    /**
     * Update Variant: after update variant and parent cache empty
     */
    public function testUpdateVariant()
    {
        $cache = $this->getCacheBackend();
        $article = $this->createArticle();
        $variant = $this->createArticleVariant();

        // cache empty
        $this->assertNull($cache->get('oxArticle__testArticle_1_de'));
        $this->assertNull($cache->get('oxArticle__testVariantArticle_1_de'));

        $article->load('_testArticle');
        $article->load('_testVariantArticle');

        // cache filed
        $this->assertNotNull($cache->get('oxArticle__testArticle_1_de'));
        $this->assertNotNull($cache->get('oxArticle__testVariantArticle_1_de'));

        // update variant
        $variant->oxarticles__oxtitle = new oxField('testVariantArticleUpdated');
        $variant->save();

        // cache empty
        $this->assertNull($cache->get('oxArticle__testArticle_1_de'));
        $this->assertNull($cache->get('oxArticle__testVariantArticle_1_de'));
    }

    /**
     * Update Rating: after we expect that cache is invalidated
     *
     */
    public function testUpdateRating()
    {
        $cache = $this->getCacheBackend();
        $article = $this->createArticle();

        // cache empty
        $this->assertNull($cache->get('oxArticle__testArticle_1_de'));

        // load
        $article->load('_testArticle');

        // cache filed
        $this->assertNotNull($cache->get('oxArticle__testArticle_1_de'));

        // update
        $article->addToRatingAverage(5);

        // cache empty
        $this->assertNull($cache->get('oxArticle__testArticle_1_de'));
    }


    /**
     * Update Stock: after we expect that cache is invalidated
     *
     */
    public function testUpdateStock()
    {
        $cache = $this->getCacheBackend();
        $article = $this->createArticle();

        // cache empty
        $this->assertNull($cache->get('oxArticle__testArticle_1_de'));

        // load
        $article->load('_testArticle');

        // cache filed
        $this->assertNotNull($cache->get('oxArticle__testArticle_1_de'));

        // update
        $article->reduceStock(5);

        // cache empty
        $this->assertNull($cache->get('oxArticle__testArticle_1_de'));
    }

    /**
     * Update price: after we expect that cache is invalidated
     *
     */
    public function testUpdatePrice()
    {
        $cache = $this->getCacheBackend();
        $article = $this->createArticle();

        // cache empty
        $this->assertNull($cache->get('oxArticle__testArticle_1_de'));

        // load
        $article->load('_testArticle');

        // cache filed
        $this->assertNotNull($cache->get('oxArticle__testArticle_1_de'));

        // change directly to db
        oxDb::getDb()->execute("UPDATE `oxarticles` SET `oxupdateprice` = '600', `oxupdatepricetime` = '2000-01-01 12:00:00' WHERE `oxid` = '_testArticle'");

        // Execute upcoming price update
        $list = oxNew("oxArticleList");
        $list->updateUpcomingPrices(true);

        // cache empty
        $this->assertNull($cache->get('oxArticle__testArticle_1_de'));
    }

    /**
     * Data provider: return prepared oxArticle object for testing
     *
     * @return Article
     */
    protected function createArticle()
    {
        $article = oxNew('oxArticle');
        $article->setId('_testArticle');
        $article->oxarticles__oxprice = new oxField(15.5);
        $article->oxarticles__oxshopid = new oxField($this->getConfig()->getBaseShopId());
        $article->oxarticles__oxtitle = new oxField("testArticle");
        $article->save();

        return $article;
    }

    /**
     * Data provider: return prepared oxArticle object for testing
     *
     * @return Article
     */
    protected function createArticleVariant()
    {
        $article = oxNew('oxArticle');
        $article->setId('_testVariantArticle');
        $article->oxarticles__oxprice = new oxField(15.5);
        $article->oxarticles__oxshopid = new oxField($this->getConfig()->getBaseShopId());
        $article->oxarticles__oxtitle = new oxField("testVariantArticle");
        $article->oxarticles__oxparentid = new oxField('_testArticle');
        $article->save();

        return $article;
    }
}
