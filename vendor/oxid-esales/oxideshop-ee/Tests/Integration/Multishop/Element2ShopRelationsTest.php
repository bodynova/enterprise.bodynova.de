<?php
/**
 * This Software is the property of OXID eSales and is protected
 * by copyright law - it is NOT Freeware.
 *
 * Any unauthorized use of this software without a valid license key
 * is a violation of the license agreement and will be prosecuted by
 * civil and criminal law.
 *
 * @link      http://www.oxid-esales.com
 * @copyright (C) OXID eSales AG 2003-2016
 * @version   OXID eShop EE
 */

namespace OxidEsales\EshopEnterprise\Tests\Integration\Multishop;

/**
 * Element2ShopRelations integration test
 */
class Element2ShopRelationsTest extends MultishopTestCase
{

    /**
     * Test case directory array
     *
     * @var array
     */
    protected $testCaseDir = array(
        'TestCases/Element2ShopRelations',
    );

    /**
     * @var array
     */
    protected $fixtureTemplate = array(
        'shops'    => array(),
        'articles' => array(),
        'setup'    => array(
            'articles2shop' => array(),
        ),
        'actions'  => array(
            'add_to_shop'                => array(),
            'remove_from_shop'           => array(),
            'remove_from_all_shops'      => array(),
            'copy_inheritance'           => array(),
            'inherit_from_shop'          => array(),
            'remove_inherited_from_shop' => array(),
        ),
        'expected' => array(
            'article_in_shop'     => array(),
            'article_not_in_shop' => array(),
        ),
    );

    /**
     * Sets up the fixture.
     *
     * @param array $testCase Test cases with expected results.
     */
    protected function _setupFixture($testCase)
    {
        parent::_setupFixture($testCase);

        foreach ($testCase['articles'] as $data) {
            $this->_createArticle($data);
        }

        $this->getConfig()->setShopId(1);

        $setup = $testCase['setup'];

        $shopRelations = oxNew('oxElement2ShopRelations', null);

        foreach ($setup['articles2shop'] as $articleId => $shopIds) {
            $article = $this->_getArticleById($articleId);

            $shopRelations->setShopIds($shopIds);
            $shopRelations->setItemType($article->getCoreTableName());
            $shopRelations->addToShop($article->getId());
        }
    }

    /**
     * Checks result.
     *
     * @param array $testCase Test cases with expected results.
     */
    protected function _checkResults($testCase)
    {
        $expected = $testCase['expected'];

        $shopRelations = oxNew('oxElement2ShopRelations', null);

        foreach ($expected['article_in_shop'] as $articleId => $expectedShopIds) {
            $article = $this->_getArticleById($articleId);

            $shopRelations->setItemType($article->getCoreTableName());

            $shopIdsFromShopRelations = $shopRelations->getItemAssignedShopIds($article->getId());

            $expectedShopIdsBuf = $expectedShopIds;
            sort($expectedShopIdsBuf);
            sort($shopIdsFromShopRelations);

            $this->assertEquals(
                $expectedShopIdsBuf,
                $shopIdsFromShopRelations,
                "Article {$article->getId()} is expected to be assigned to shops " . implode(', ', $expectedShopIds)
                . " but got " . implode(', ', $shopIdsFromShopRelations)
            );

            foreach ($expectedShopIds as $shopId) {
                $shopRelations->setShopIds($shopId);
                $this->assertTrue(
                    $shopRelations->isInShop($article->getId()),
                    "Article {$article->getId()} is expected to be assigned to shop $shopId."
                );
            }
        }

        foreach ($expected['article_not_in_shop'] as $articleId => $expectedShopIds) {
            $article = $this->_getArticleById($articleId);
            $shopRelations->setItemType($article->getCoreTableName());

            foreach ($expectedShopIds as $shopId) {
                $shopRelations->setShopIds($shopId);
                $this->assertFalse(
                    $shopRelations->isInShop($article->getId()),
                    "Article {$article->getId()} is not expected to be assigned to shop $shopId."
                );
            }
        }
    }

    /**
     * Tears down the fixture.
     */
    protected function tearDown()
    {
        $this->_deleteArticles();
        $this->_deleteShops();

        parent::tearDown();
    }


    /**
     * Data fixture and expected results.
     *
     * @return array
     */
    public function dpData()
    {
        return $this->_getTestCases($this->testCaseDir);
    }

    /**
     * Tests add item to shop or list of shops.
     *
     * @param array $testCase Test cases with expected results.
     *
     * @dataProvider dpData
     */
    public function testElement2ShopRelations($testCase)
    {
        $this->_setupFixture($testCase);

        // perform actions to test

        $actions = $testCase['actions'];

        $shopRelations = oxNew('oxElement2ShopRelations', null);
        foreach ($actions['add_to_shop'] as $articleId => $shopIds) {
            $article = $this->_getArticleById($articleId);
            $shopRelations->setItemType($article->getCoreTableName());

            $shopRelations->setShopIds($shopIds);
            $shopRelations->addToShop($article->getId());
        }

        $shopRelations = oxNew('oxElement2ShopRelations', null);
        foreach ($actions['remove_from_shop'] as $articleId => $shopIds) {
            $article = $this->_getArticleById($articleId);
            $shopRelations->setItemType($article->getCoreTableName());

            $shopRelations->setShopIds($shopIds);
            $shopRelations->removeFromShop($article->getId());
        }

        $shopRelations = oxNew('oxElement2ShopRelations', null);
        foreach ($actions['remove_from_all_shops'] as $articleId) {
            $article = $this->_getArticleById($articleId);
            $shopRelations->setItemType($article->getCoreTableName());

            $shopRelations->removeFromAllShops($article->getId());
        }

        $shopRelations = oxNew('oxElement2ShopRelations', null);
        foreach ($actions['copy_inheritance'] as $articleId => $sourceArticleId) {
            $article = $this->_getArticleById($articleId);
            $shopRelations->setItemType($article->getCoreTableName());
            $sourceArticle = $this->_getArticleById($sourceArticleId);

            $shopRelations->copyInheritance(
                $sourceArticle->getId(),
                $article->getId()
            );
        }

        $shopRelations = oxNew('oxElement2ShopRelations', null);
        foreach ($actions['inherit_from_shop'] as $shopId => $parentShopId) {
            $shopRelations->setShopIds($shopId);
            $shopRelations->setItemType('oxarticles');
            $shopRelations->inheritFromShop($parentShopId);
        }

        $shopRelations = oxNew('oxElement2ShopRelations', null);
        foreach ($actions['remove_inherited_from_shop'] as $shopId => $parentShopId) {
            $shopRelations->setShopIds($shopId);
            $shopRelations->setItemType('oxarticles');
            $shopRelations->removeInheritedFromShop($parentShopId);
        }

        $this->_checkResults($testCase);
    }

    /**
     * Tests OxShopRelation::isInShop() getter
     */
    public function testInShopDemodata()
    {
        $shopRelations = oxNew('oxElement2ShopRelations', "oxarticles");
        $shopRelations->setShopIds(1);
        $this->assertFalse($shopRelations->isInShop("nonExistingTestId"));
        $sql = "INSERT INTO oxarticles (oxid, oxtitle) values ('_test123', '_testArticle');";
        $this->addToDatabase($sql, 'oxarticles');
        $this->assertTrue($shopRelations->isInShop("_test123"));
    }

}
