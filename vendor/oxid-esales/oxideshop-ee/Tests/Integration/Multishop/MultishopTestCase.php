<?php
/**
 * This Software is the property of OXID eSales and is protected
 * by copyright law - it is NOT Freeware.
 *
 * Any unauthorized use of this software without a valid license key
 * is a violation of the license agreement and will be prosecuted by
 * civil and criminal law.
 *
 * @link      http://www.oxid-esales.com
 * @copyright (C) OXID eSales AG 2003-2016
 * @version   OXID eShop EE
 */

namespace OxidEsales\EshopEnterprise\Tests\Integration\Multishop;

use OxidEsales\TestingLibrary\UnitTestCase;
use RecursiveRegexIterator;
use RegexIterator;
use RecursiveDirectoryIterator;
use RecursiveIteratorIterator;
use oxDb;
use oxRegistry;

abstract class MultishopTestCase extends UnitTestCase
{

    /**
     * @var string
     */
    protected $testDir = __DIR__;

    /**
     * Test case directory array
     *
     * @var array
     */
    protected $testCaseDir = array(
        'DataProvilers',
    );

    /**
     * @var array
     */
    protected $fixtureTemplate = array(
        'shops' => array(),
    );

    /**
     * List of test shops.
     *
     * @var array
     */
    protected $shops = array();

    /**
     * List of test articles.
     *
     * @var array
     */
    protected $articles = array();

    /**
     * List of test categories.
     *
     * @var array
     */
    protected $categories = array();

    /**
     * List of product to category assignments.
     *
     * @var array
     */
    protected $productCategoryRelations = array();

    /**
     * Creates shop for test.
     *
     * @param array $data Shop data.
     *
     * @return oxShop
     */
    protected function _createShop($data)
    {
        $params = array();
        foreach ($data as $key => $value) {
            $field = "oxshops__{$key}";
            $params[$field] = $value;
        }

        $shop = oxNew('oxShop');
        $shop->assign($params);
        $shop->save();

        //copy main configuration options
        $this->_createConfigOptions($shop);

        if ($shop->oxshops__oxisinherited->value) {
            $this->_setShopInheritance($shop, true);
        }


        $this->shops[$shop->getId()] = $shop;

        return $shop;
    }

    /**
     * Copy config options from base shop to current shop
     *
     * @param $shop
     */
    protected function _createConfigOptions($shop)
    {
        $database = oxDb::getDb();
        $config = oxRegistry::getConfig();
        $utilsObject = oxRegistry::get("oxUtilsObject");

        $copyVars = array(
            "aLanguages"
        );

        $copyVarsPrepared = '';
        if (count($copyVars)) {
            $copyVarsPrepared = " and oxvarname in ( '" . join("', '", $copyVars) . "')";
        }

        $select = "select oxvarname, oxvartype, DECODE( oxvarvalue, " . $database->quote($config->getConfigParam('sConfigKey')) . ") as oxvarvalue, oxmodule from oxconfig where oxshopid = '1' $copyVarsPrepared ";
        $rs = $database->select($select);
        if ($rs != false && $rs->count() > 0) {
            while (!$rs->EOF) {
                $id = $utilsObject->generateUID();
                $insertSql = "insert into oxconfig (oxid, oxshopid, oxvarname, oxvartype, oxvarvalue, oxmodule) values ( '$id', " . $database->quote($shop->getId())
                           . ", " . $database->quote($rs->fields[0])
                           . ", " . $database->quote($rs->fields[1])
                           . ",  ENCODE( " . $database->quote($rs->fields[2])
                           . ", '" . $config->getConfigParam('sConfigKey')
                           . "')"
                           . ", " . $database->quote($rs->fields[3]) . " )";
                $database->execute($insertSql);
                $rs->fetchRow();
            }
        }
    }

    /**
     * Sets "Inherit All" option for subshop
     *
     * @param oxShop $shop        Shop object
     * @param bool   $inheritAll "Inherit All" value
     */
    protected function _setShopInheritance($shop, $inheritAll)
    {
        $config = oxRegistry::getConfig();
        $inheritAll = $inheritAll ? "true" : "false";
        $multiShopTables = $config->getConfigParam('aMultiShopTables');
        foreach ($multiShopTables as $oneMultishopTable) {
            $config->saveShopConfVar("bool", 'blMallInherit_' . strtolower($oneMultishopTable), $inheritAll, $shop->oxshops__oxid->value);
        }
    }

    /**
     * Deletes shops for test.
     */
    protected function _deleteShops()
    {
        /** @var oxShop $shop */
        foreach ($this->shops as $shop) {
            $shop->delete();

            //remove config options
            $query = "delete from oxconfig where oxshopid = " . $shop->getId() . " ";
            oxDb::getDb()->execute($query);
        }
    }

    /**
     * Creates article for test.
     *
     * @param array $data Article data.
     *
     * @return oxArticle
     */
    protected function _createArticle($data)
    {
        $params = array();
        $shopIds = array();
        foreach ($data as $key => $value) {
            if ($key == "oxinheritedshopids") {
                $shopIds = $value;
            } else {
                $field = "oxarticles__{$key}";
                $params[$field] = $value;
            }
        }

        if (isset($params['oxarticles__oxshopid']) && !empty($params['oxarticles__oxshopid'])) {
            $this->getConfig()->setShopId($params['oxarticles__oxshopid']);
        } else {
            $this->getConfig()->setShopId(1);
        }

        $article = oxNew('oxArticle');
        $article->assign($params);
        $article->save();

        if (!empty($shopIds)) {
            foreach ($shopIds as $shopId) {
                $article->assignToShop($shopId);
            }
        }

        $this->articles[$article->getId()] = $article;

        return $article;
    }

    /**
     * Creates category for test.
     *
     * @param array $data Input data.
     *
     * @return oxCategory
     */
    protected function _createCategory($data)
    {
        $params = array();
        foreach ($data as $key => $value) {
            $field = "oxcategories__{$key}";
            $params[$field] = $value;
        }

        if (isset($params['oxcategories__oxshopid']) && !empty($params['oxcategories__oxshopid'])) {
            $this->getConfig()->setShopId($params['oxcategories__oxshopid']);
        } else {
            $this->getConfig()->setShopId(1);
        }

        $category = oxNew('oxCategory');
        $category->assign($params);
        $category->save();

        $this->categories[$category->getId()] = $category;

        return $category;
    }

    /**
     * Creates object 2 category relation  for test.
     *
     * @param array $data Input data.
     *
     * @return oxObject2Category
     */
    protected function _createObject2Category($data)
    {
        $params = array();
        foreach ($data as $key => $value) {
            $field = "oxobject2category__{$key}";
            $params[$field] = $value;
        }

        $object2Category = oxNew('oxObject2Category');
        $object2Category->assign($params);
        $object2Category->save();

        $this->productCategoryRelations[$object2Category->getId()] = $object2Category;

        return $object2Category;
    }

    /**
     * Deletes articles for test.
     */
    protected function _deleteArticles()
    {
        /** @var oxArticle $article */
        foreach ($this->articles as $article) {
            $article->delete();
        }
    }

    /**
     * Deletes categories for test.
     */
    protected function _deleteCategories()
    {
        /** @var oxCategory $category */
        foreach ($this->categories as $category) {
            $category->delete();
        }
    }

    /**
     * Deletes for test.
     */
    protected function _deleteO2C()
    {
        foreach ($this->productCategoryRelations as $object2Category) {
            $object2Category->delete();
        }
    }

    /**
     * Deletes created test date
     */
    protected function _deleteFixture()
    {
        $this->_deleteO2C();
        $this->_deleteCategories();
        $this->_deleteArticles();
        $this->_deleteShops();
    }

    /**
     * Gets article created in test.
     *
     * @param string $articleId Article ID.
     *
     * @return oxArticle
     */
    protected function _getArticleById($articleId)
    {
        return $this->articles[$articleId];
    }

    /**
     * Gets shop created in test.
     *
     * @param string $shopId Shop ID.
     *
     * @return oxShop
     */
    protected function _getShopById($shopId)
    {
        return $this->shops[$shopId];
    }

    /**
     * Updates test data with missing template information.
     *
     * @param array $data Test case data.
     *
     * @return array
     */
    protected function _updateTemplate($data)
    {
        foreach ($this->fixtureTemplate as $item => $itemTemplate) {
            if (!isset($data[$item])) {
                $data[$item] = $itemTemplate;
            } else {
                foreach ($itemTemplate as $subItem => $subItemTemplate) {
                    if (!isset($data[$item][$subItem])) {
                        $data[$item][$subItem] = $subItemTemplate;
                    }
                }
            }
        }

        return $data;
    }

    /**
     * Sets up the fixture.
     *
     * @param array $testCase Test cases with expected results.
     */
    protected function _setupFixture($testCase)
    {
        foreach ($testCase['shops'] as $data) {
            $this->_createShop($data);
        }
    }

    /**
     * Generates fixtures and gets test cases.
     *
     * @param array $directories Test case directory array
     *
     * @return array
     */
    protected function _getTestCases($directories)
    {
        $global = array();

        foreach ($directories as $directory) {
            $path = "{$this->testDir}/$directory/";

            print("Scanning dir $path" . PHP_EOL);

            $regexIterator = new RegexIterator(
                new RecursiveIteratorIterator(
                    new RecursiveDirectoryIterator($path)
                ),
                '/^.+\.php$/i',
                RecursiveRegexIterator::GET_MATCH
            );

            foreach ($regexIterator as $files) {
                $filename = $files[0];

                $data = include $filename;
                if ($data) {
                    $global[$filename] = array($this->_updateTemplate($data));
                }
            }
        }

        return $global;
    }

    /**
     * Updates DB views
     */
    protected function _updateViews()
    {
        $metaData = oxNew('oxDbMetaDataHandler');
        $metaData->updateViews();
    }

}