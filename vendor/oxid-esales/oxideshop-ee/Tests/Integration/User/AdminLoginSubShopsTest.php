<?php
/**
 * This Software is the property of OXID eSales and is protected
 * by copyright law - it is NOT Freeware.
 *
 * Any unauthorized use of this software without a valid license key
 * is a violation of the license agreement and will be prosecuted by
 * civil and criminal law.
 *
 * @link      http://www.oxid-esales.com
 * @copyright (C) OXID eSales AG 2003-2016
 * @version   OXID eShop EE
 */
namespace OxidEsales\EshopEnterprise\Tests\Integration\User;

/**
 * Class Integration_User_AdminLoginSubShopsTest
 */
class AdminLoginSubShopsTest extends UserTestCase
{
    /**
     * Initialize the fixture.
     * Admin login must have at least one cookie, otherwise login fails with no cookie exception.
     */
    public function setUp()
    {
        parent::setUp();
        $this->setAdminMode(true);
        $_COOKIE['someAdminCookie'] = 'somevalue';
    }

    /**
     * Disables admin mode.
     */
    public function tearDown()
    {
        $this->setAdminMode(false);
        parent::tearDown();
    }

    /**
     * @return array
     */
    public function providerSuccessfulSubShopAdminLogin()
    {
        return array(
            // First sub shop admin logs to shop.
            array('oxidadmin', 1),
            // Second sub shop admin logs to shop.
            array('oxidadmin', 2),
            // First sub shop mall admin logs to shop.
            array('malladmin', 1),
            // Second sub shop mall admin logs to shop.
            array('malladmin', 2),
        );
    }

    /**
     * @param string $rights
     * @param int    $subShopId
     *
     * @dataProvider providerSuccessfulSubShopAdminLogin
     */
    public function testSuccessfulSubShopAdminLogin($rights, $subShopId)
    {
        $this->createSubShop();
        $this->setLoginParametersToRequest();
        $this->createDefaultUser($rights, $subShopId);

        $login = oxNew('Login');
        $this->assertSame("admin_start", $login->checklogin());
    }

    /**
     * @return array
     */
    public function providerFailureSubShopAdminLogin()
    {
        return array(
            // User from first sub shop tries to login as admin.
            array('user', 1),
            // User from second sub shop tries to login as admin.
            array('user', 2),
        );
    }

    /**
     * @param string $rights
     * @param int    $subShopId
     *
     * @dataProvider providerFailureSubShopAdminLogin
     */
    public function testFailureSubShopAdminLogin($rights, $subShopId)
    {
        $this->createSubShop();
        $this->setLoginParametersToRequest();
        $this->createDefaultUser($rights, $subShopId);

        $login = oxNew('Login');
        $this->assertEmpty($login->checklogin(), 'Get this result: ' . $login->checklogin());
    }

    /**
     * Adds login data to request.
     *
     * @param string $userName
     * @param string $userPassword
     */
    protected function setLoginParametersToRequest($userName = null, $userPassword = null)
    {
        $userName = !is_null($userName) ? $userName : $this->_sDefaultUserName;
        $userPassword = !is_null($userPassword) ? $userPassword : $this->_sDefaultUserPassword;

        $this->setRequestParameter('user', $userName);
        $this->setRequestParameter('pwd', $userPassword);
    }
}
