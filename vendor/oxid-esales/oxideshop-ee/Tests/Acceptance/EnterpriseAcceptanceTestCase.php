<?php
/**
 * This Software is the property of OXID eSales and is protected
 * by copyright law - it is NOT Freeware.
 *
 * Any unauthorized use of this software without a valid license key
 * is a violation of the license agreement and will be prosecuted by
 * civil and criminal law.
 *
 * @link      http://www.oxid-esales.com
 * @copyright (C) OXID eSales AG 2003-2016
 * @version   OXID eShop EE
 */

namespace OxidEsales\EshopEnterprise\Tests\Acceptance;

use OxidEsales\TestingLibrary\AcceptanceTestCase;

/**
 * oxArticle integration test
 */
class EnterpriseAcceptanceTestCase extends AcceptanceTestCase
{
    /**
     * Run this tests only on Enterprise configuration
     */
    protected function setUp()
    {
        parent::setUp();

        $testConfig = $this->getTestConfig();
        if ($testConfig->getShopEdition() !== 'EE') {
            $this->markTestSkipped('This test can be run only on Enterprise edition');
        }
    }
}