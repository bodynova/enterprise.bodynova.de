<?php
/**
 * This Software is the property of OXID eSales and is protected
 * by copyright law - it is NOT Freeware.
 *
 * Any unauthorized use of this software without a valid license key
 * is a violation of the license agreement and will be prosecuted by
 * civil and criminal law.
 *
 * @link      http://www.oxid-esales.com
 * @copyright (C) OXID eSales AG 2003-2016
 * @version   OXID eShop EE
 */

namespace OxidEsales\EshopEnterprise\Tests\Acceptance\Frontend;

use OxidEsales\EshopEnterprise\Tests\Acceptance\EnterpriseTestCase;

/** Varnish related tests. */
class VarnishFrontendChangeLanguageTest extends EnterpriseTestCase
{
    const ENGLISH_LANGUAGE_ID = 1;

    const GERMAN_LANGUAGE_ID = 0;

    const HOME_PAGE = 'home';

    const CATEGORY_PAGE = 'category';

    /** @var bool */
    protected $_blStartMinkSession = false;

    /** @var int */
    private $languageId;

    /** @var string */
    private $pageName;

    /**
     * @group varnish
     *
     * @return null
     */
    public function testCheckPageContentTranslatedCorrectly()
    {
        // Test only with Main Shop as it should cover both cases.
        // Testing with Subshop leads to fatal error as oxMinkWrapper::selectFrame
        // has line $this->selectWindow( null ) which is not supported by goutte driver.
        if (isSUBSHOP) {
            return;
        }

        $this->startMinkSession('goutte');

        $shopUrl = $this->getTestConfig()->getShopUrl();

        // English pages.
        $this->setLanguageId(static::ENGLISH_LANGUAGE_ID);
        $this->setLanguageIdToTranslator();

        $this->setPageName(static::HOME_PAGE);
        $this->openNewWindow($shopUrl);
        $this->checkPageContentLanguage();
        $this->openNewWindow($shopUrl);
        $this->checkPageContentLanguage();

        $this->setPageName(static::CATEGORY_PAGE);
        $this->openNewWindow($shopUrl . 'en/Kiteboarding/Harnesses/');
        $this->checkPageContentLanguage();
        $this->openNewWindow($shopUrl . 'en/Kiteboarding/Harnesses/');
        $this->checkPageContentLanguage();

        // German pages.
        $this->setLanguageId(static::GERMAN_LANGUAGE_ID);
        $this->setLanguageIdToTranslator();

        $this->setPageName(static::HOME_PAGE);
        $this->openNewWindow($shopUrl . 'startseite');
        $this->checkPageContentLanguage();
        $this->openNewWindow($shopUrl . 'startseite');
        $this->checkPageContentLanguage();

        $this->setPageName(static::CATEGORY_PAGE);
        $this->openNewWindow($shopUrl . 'Kiteboarding/Trapeze/');
        $this->checkPageContentLanguage();
        $this->openNewWindow($shopUrl . 'Kiteboarding/Trapeze/');
        $this->checkPageContentLanguage();
    }

    /**
     * Check if page widgets contains of words translated in correct language.
     * Check if page widgets does not contains of words in incorrect language.
     */
    private function checkPageContentLanguage()
    {
        $translator = $this->getTranslator();

        $language = $this->getLanguageId();
        if ($language == static::ENGLISH_LANGUAGE_ID) {
            $languageTrigger = "English";
            $productName = "Harness SOL KITE";
            $services = "Contact";
            $information = "About Us";
            $manufacturers = "All brands";
            $categories = "Categories";
            $moreDifferentLanguage = "Mehr";
        } else {
            $languageTrigger = "Deutsch";
            $productName = "Trapez ION SOL KITE 2011";
            $services = "Kontakt";
            $information = "Impressum";
            $manufacturers = "Alle Marken";
            $categories = "Kategorien";
            $more = "Mehr";
            $moreDifferentLanguage = "More";
        }

        $activePage = $this->getPageName();
        if ($activePage === static::HOME_PAGE) {
            $productNamePlace = 'newItems_4';
        } else {
            $productNamePlace = 'productList';
        }

        $this->assertElementPresentInCorrectLanguage("//p[@id='languageTrigger']//*[text()='" . $languageTrigger . "']", "Language list");
        $this->assertExactContainsCorrectLanguage($translator->translate("%LOGIN%"), 'loginBoxOpener', "Service menu");
        $this->assertContainsCorrectLanguage($productName, $productNamePlace, "Article box");
        $this->assertContainsCorrectLanguage($translator->translate("%MORE%"), 'navigation', "Category tree");
        $this->assertContainsCorrectLanguage($services, 'footerServices', "Services");
        $this->assertContainsCorrectLanguage($information, 'footerInformation', "Information");
        $this->assertContainsCorrectLanguage($manufacturers, 'footerManufacturers', "Manufacturer");
        $this->assertContainsCorrectLanguage($categories, 'footerCategories', "Category");

        $this->assertNotContainsCorrectLanguage($moreDifferentLanguage, 'sidebar', "Sidebar");
    }

    /**
     * @return int
     */
    protected function getLanguageId()
    {
        return $this->languageId;
    }

    /**
     * @param int $languageId
     */
    protected function setLanguageId($languageId)
    {
        $this->languageId = $languageId;
    }

    /**
     * @return string
     */
    protected function getPageName()
    {
        return $this->pageName;
    }

    /**
     * @param string $pageName
     */
    protected function setPageName($pageName)
    {
        $this->pageName = $pageName;
    }

    /**
     * Checks if element contains expected content and forms error message if does not.
     *
     * @param string $translation
     * @param string $place
     * @param string $widgetName
     */
    private function assertContainsCorrectLanguage($translation, $place, $widgetName)
    {
        $message = $this->formErrorMessage($widgetName);
        $this->assertContains($translation, $this->getText($place), $message);
    }

    /**
     * Checks if element does not contains unexpected content and forms error message if does.
     *
     * @param string $translation
     * @param string $place
     * @param string $widgetName
     */
    private function assertNotContainsCorrectLanguage($translation, $place, $widgetName)
    {
        $message = $this->formErrorMessage($widgetName);
        $this->assertNotContains($translation, $this->getText($place), $message);
    }

    /**
     * @param string $translation
     * @param string $place
     * @param string $widgetName
     */
    private function assertExactContainsCorrectLanguage($translation, $place, $widgetName)
    {
        $message = $this->formErrorMessage($widgetName);
        $this->assertEquals($translation, $this->getText($place), $message);
    }

    /**
     * Checks if element is visible and forms error message if not.
     *
     * @param string $translation
     * @param string $widgetName
     */
    private function assertElementPresentInCorrectLanguage($translation, $widgetName)
    {
        $message = $this->formErrorMessage($widgetName);
        $this->assertElementPresent($translation, $message);
    }

    /**
     * Forms Error message in which widget and in which language content is not as expected.
     *
     * @param string $widgetName
     *
     * @return string
     */
    private function formErrorMessage($widgetName)
    {
        $message = $widgetName . " widget content is not as expected in language: " . $this->getLanguageId();
        return $message;
    }

    /**
     * Set translator to translate in particular language.
     */
    private function setLanguageIdToTranslator()
    {
        $translator = $this->getTranslator();
        $languageId = $this->getLanguageId();
        $translator->setLanguage($languageId);
    }
}
