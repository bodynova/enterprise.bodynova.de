<?php
/**
 * This Software is the property of OXID eSales and is protected
 * by copyright law - it is NOT Freeware.
 *
 * Any unauthorized use of this software without a valid license key
 * is a violation of the license agreement and will be prosecuted by
 * civil and criminal law.
 *
 * @link      http://www.oxid-esales.com
 * @copyright (C) OXID eSales AG 2003-2016
 * @version   OXID eShop EE
 */

namespace OxidEsales\EshopEnterprise\Tests\Acceptance\Frontend;

use OxidEsales\EshopEnterprise\Tests\Acceptance\EnterpriseTestCase;

/** Varnish related tests. */
class VarnishFrontendTest extends EnterpriseTestCase
{
    /** @var bool */
    protected $_blStartMinkSession = false;

    /**
     * This test case is supposed to test handling of sToken
     * when varnish is enabled, user is not logged in but with started session.
     *
     * Test case:
     * Item is added to the basket and session is created;
     * Page with session is cached in varnish;
     * Cookies is cleared to imitate other user coming to the shop;
     * Item is added to the basket to start new session.
     * Adding item to basket still works when page with session is cached.
     *
     * @group varnish
     *
     * @return null
     */
    public function testAddToBasketWithDifferentUsers()
    {
        // Test only with Main Shop as it should cover both cases.
        // Testing with Subshop leads to fatal error as oxMinkWrapper::selectFrame
        // has line $this->selectWindow( null ) which is not supported by goutte driver.
        if (isSUBSHOP) {
            return;
        }

        $this->startMinkSession('goutte');

        $this->openShop();

        $this->addFirstItemToBasket();

        $this->openShop();

        $this->checkItemsCountInBasket(1);

        $this->clearCookies();

        $this->openShop();

        $this->addFirstItemToBasket();

        $this->openShop();

        $this->addFirstItemToBasket();

        $this->checkItemsCountInBasket(2);
    }

    /**
     * Adds first item found in list to basket.
     */
    private function addFirstItemToBasket()
    {
        $oButton = $this->getElement("//ul[@id='newItems']//div[@class='buttonBox']/button");
        $oButton->click();
    }

    /**
     * Checks items count in basket.
     *
     * @param int $iCount How much items should be in basket.
     */
    private function checkItemsCountInBasket($iCount)
    {
        $oCount = $this->getElement("countValue");
        $this->assertEquals($iCount, $oCount->getText());
    }
}
