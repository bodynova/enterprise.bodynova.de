<?php
/**
 * This Software is the property of OXID eSales and is protected
 * by copyright law - it is NOT Freeware.
 *
 * Any unauthorized use of this software without a valid license key
 * is a violation of the license agreement and will be prosecuted by
 * civil and criminal law.
 *
 * @link      http://www.oxid-esales.com
 * @copyright (C) OXID eSales AG 2003-2016
 * @version   OXID eShop EE
 */

namespace OxidEsales\EshopEnterprise\Tests\Acceptance\Admin;

use OxidEsales\EshopEnterprise\Tests\Acceptance\EnterpriseAcceptanceTestCase;

/**
 * oxArticle integration test
 */
class SearchAndSortingAdminTest extends EnterpriseAcceptanceTestCase
{
    /**
     * Sets default language to English.
     */
    protected function setUp()
    {
        parent::setUp();
        $this->getTranslator()->setLanguage(1);
    }

    /**
     * searching and sorting Admin Roles (EE version only)
     *
     * @group search_sort
     */
    public function testSearchSortAdminRoles()
    {
        $testConfig = $this->getTestConfig();
        if ($testConfig->isSubShop()) {
            $this->executeSql( "UPDATE `oxroles` SET `OXSHOPID` = ".oxSHOPID."  WHERE `OXAREA` = 0;" );
        }

        $this->loginAdmin("Administer Products", "Admin Roles");
        //search
        $this->type("where[oxroles][oxtitle]", "admin");
        $this->clickAndWait("submitit");
        $this->assertElementPresent("link=1 admin role šÄßüл");
        $this->assertElementPresent("link=2 admin role šÄßüл");
        $this->assertElementPresent("link=3 admin role šÄßüл");
        $this->assertElementPresent("link=4 admin role šÄßüл");
        $this->assertElementPresent("link=[last] admin role šÄßüл");
        $this->assertElementNotPresent("//tr[@id='row.6']/td[1]");
        $this->type("where[oxroles][oxtitle]", "4 admin šÄßüл");
        $this->clickAndWait("submitit");
        $this->assertEquals("4 admin role šÄßüл", $this->getText("//tr[@id='row.1']/td[1]"));
        $this->type("where[oxroles][oxtitle]", "");
        $this->clickAndWait("submitit");
        //sorting
        $this->clickAndWait("link=Title");
        $this->assertEquals("1 admin role šÄßüл", $this->getText("//tr[@id='row.1']/td[1]"));
        $this->assertEquals("2 admin role šÄßüл", $this->getText("//tr[@id='row.2']/td[1]"));
        $this->assertEquals("3 admin role šÄßüл", $this->getText("//tr[@id='row.3']/td[1]"));
        //$this->assertEquals("Page 1 / 2", $this->getText("nav.site"));
        $this->assertElementPresent("//a[@id='nav.page.1'][@class='pagenavigation pagenavigationactive']");
        $this->clickAndWait("nav.next");
        //$this->assertEquals("Page 2 / 2", $this->getText("nav.site"));
        $this->assertElementPresent("//a[@id='nav.page.2'][@class='pagenavigation pagenavigationactive']");
        $this->assertEquals("[last] admin role šÄßüл", $this->getText("//tr[@id='row.1']/td[1]"));
        $this->clickAndWait("nav.prev");
        //$this->assertEquals("Page 1 / 2", $this->getText("nav.site"));
        $this->assertElementPresent("//a[@id='nav.page.1'][@class='pagenavigation pagenavigationactive']");
        $this->assertEquals("1 admin role šÄßüл", $this->getText("//tr[@id='row.1']/td[1]"));
        $this->clickAndWait("nav.last");
        $this->assertEquals("[last] admin role šÄßüл", $this->getText("//tr[@id='row.1']/td[1]"));
        //$this->assertEquals("Page 2 / 2", $this->getText("nav.site"));
        $this->assertElementPresent("//a[@id='nav.page.2'][@class='pagenavigation pagenavigationactive']");
        $this->clickAndWait("nav.first");
        $this->assertEquals("1 admin role šÄßüл", $this->getText("//tr[@id='row.1']/td[1]"));
        //$this->assertEquals("Page 1 / 2", $this->getText("nav.site"));
        $this->assertElementPresent("//a[@id='nav.page.1'][@class='pagenavigation pagenavigationactive']");
        //deleting last item to check navigation
        $this->clickAndWait("nav.last");
        $this->assertEquals("[last] admin role šÄßüл", $this->getText("//tr[@id='row.1']/td[1]"));
        $this->clickDeleteListItem(1);
        $this->assertElementNotPresent("nav.page.1");
        $this->assertEquals("1 admin role šÄßüл", $this->getText("//tr[@id='row.1']/td[1]"));
    }

    /**
     * searching and sorting Shop Roles (EE version only)
     *
     * @group search_sort
     */
    public function testSearchSortShopRoles()
    {
        $this->loginAdmin("Administer Products", "Shop Roles");
        //search
        $this->assertElementPresent("link=1 shop role šÄßüл");
        $this->assertElementPresent("link=10 shop role šÄßüл");
        $this->assertElementPresent("link=2 shop role šÄßüл");
        $this->type("where[oxroles][oxtitle]", "1 shop šÄßüл");
        $this->clickAndWait("submitit");
        $this->assertElementPresent("link=1 shop role šÄßüл");
        $this->assertElementPresent("link=10 shop role šÄßüл");
        $this->assertElementNotPresent("link=2 shop role šÄßüл");
        $this->assertEquals("1 shop šÄßüл", $this->getValue("where[oxroles][oxtitle]"));
        $this->type("where[oxroles][oxtitle]", "10 shop");
        $this->clickAndWait("submitit");
        $this->assertEquals("10 shop role šÄßüл", $this->getText("//tr[@id='row.1']/td[1]"));
        $this->assertElementNotPresent("//tr[@id='row.2']/td[1]");
        $this->type("where[oxroles][oxtitle]", "");
        $this->clickAndWait("submitit");
        //sorting
        $this->clickAndWait("link=Title");
        $this->assertEquals("1 shop role šÄßüл", $this->getText("//tr[@id='row.1']/td[1]"));
        $this->assertEquals("10 shop role šÄßüл", $this->getText("//tr[@id='row.2']/td[1]"));
        $this->assertEquals("2 shop role šÄßüл", $this->getText("//tr[@id='row.3']/td[1]"));
        $this->assertEquals("3 shop role šÄßüл", $this->getText("//tr[@id='row.4']/td[1]"));
        $this->assertEquals("Page 1 / 2", $this->getText("nav.site"));
        $this->assertElementPresent("//a[@id='nav.page.1'][@class='pagenavigation pagenavigationactive']");
        $this->clickAndWait("nav.next");
        $this->assertEquals("Page 2 / 2", $this->getText("nav.site"));
        $this->assertElementPresent("//a[@id='nav.page.2'][@class='pagenavigation pagenavigationactive']");
        $this->assertEquals("[last] shop role šÄßüл", $this->getText("//tr[@id='row.1']/td[1]"));
        $this->clickAndWait("nav.prev");
        $this->assertEquals("1 shop role šÄßüл", $this->getText("//tr[@id='row.1']/td[1]"));
        $this->assertEquals("Page 1 / 2", $this->getText("nav.site"));
        $this->assertElementPresent("//a[@id='nav.page.1'][@class='pagenavigation pagenavigationactive']");
        $this->clickAndWait("nav.last");
        $this->assertEquals("[last] shop role šÄßüл", $this->getText("//tr[@id='row.1']/td[1]"));
        $this->assertEquals("Page 2 / 2", $this->getText("nav.site"));
        $this->assertElementPresent("//a[@id='nav.page.2'][@class='pagenavigation pagenavigationactive']");
        $this->clickAndWait("nav.first");
        $this->assertEquals("1 shop role šÄßüл", $this->getText("//tr[@id='row.1']/td[1]"));
        $this->assertEquals("Page 1 / 2", $this->getText("nav.site"));
        $this->assertElementPresent("//a[@id='nav.page.1'][@class='pagenavigation pagenavigationactive']");
        //deleting last element to check navigation
        $this->clickAndWait("nav.last");
        $this->assertEquals("[last] shop role šÄßüл", $this->getText("//tr[@id='row.1']/td[1]"));
        $this->clickDeleteListItem(1);
        $this->assertElementNotPresent("nav.page.1");
        $this->assertEquals("1 shop role šÄßüл", $this->getText("//tr[@id='row.1']/td[1]"));
    }
}