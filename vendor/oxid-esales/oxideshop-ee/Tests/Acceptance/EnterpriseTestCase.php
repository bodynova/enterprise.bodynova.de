<?php
/**
 * This Software is the property of OXID eSales and is protected
 * by copyright law - it is NOT Freeware.
 *
 * Any unauthorized use of this software without a valid license key
 * is a violation of the license agreement and will be prosecuted by
 * civil and criminal law.
 *
 * @link      http://www.oxid-esales.com
 * @copyright (C) OXID eSales AG 2003-2016
 * @version   OXID eShop EE
 *
 * @link http://www.oxid-esales.com
 * @copyright (c) OXID eSales AG 2003-#OXID_VERSION_YEAR#
 */
namespace OxidEsales\EshopEnterprise\Tests\Acceptance;

use OxidEsales\TestingLibrary\AcceptanceTestCase;

/**
 * Base class for acceptance tests.
 */
class EnterpriseTestCase extends AcceptanceTestCase
{
    /**
     * Checking if correct shop version is used for running tests.
     */
    protected function setUp()
    {
        if ($this->getTestConfig()->getShopEdition() !== 'EE') {
            $this->markTestSkipped("This test is for Enterprise editions only.");
        }
        parent::setUp();
    }
}
